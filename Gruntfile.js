"use strict";
module.exports = function (grunt) {
  String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    ngAnnotate: {
      options: {
          singleQuotes: true
      },
      app: {
          files: {
              'js/controllers.js': ['js/controllers.js']
          }
      }
  },
    jslint: {
      server: {
        src: ['Gruntfile.js', 'js/controllers.js'],
        options: {
          errorsOnly: true,
          failOnError: false, // defaults to true
        }
      }
    },
    concat: {
      basic: {		  
        src: [
          'js/sql.js',
          'js/sql-procedure.js',
		  
		  //chamadas
          'js/CtrlChamadas.js',
		  'js/CtrlResumo880x144.js',
		  'js/CtrlResumoPH.js',
          'js/CtrlResumoDD.js',
		  'js/CtrlPerformanceDDDUD.js',
		  'js/CtrlResumoEmpresaDestino.js',
		  'js/CtrlResumoErrosPreRouting.js',
		  'js/CtrlInfFaltLog.js',
		  'js/CtrlLogin.js',
		  'js/CtrlContingenciaRouting.js',
		  'js/CtrlResumoTag.js',
		  'js/CtrlResumoRetencaoLiquida.js',		  
		  
		  //Parâmetros
		  'js/CtrlGra.js',
		  'js/CtrlParamOiTV.js',
		  'js/CtrlGestaoED.js',
		  
		  //Estados de diálogo
          'js/CtrlED.js',
          'js/CtrlConsolidadoED.js',
          
		  //Derivação   
		  'js/CtrlTransferID.js',
		  'js/CtrlDerivacaoTLV.js',		  
		  
		  //Vendas
          'js/CtrlResumoVS.js',
          'js/CtrlVendas.js',
		  'js/CtrlResumoPorPromo.js',
		  'js/CtrlResIncentivo.js',
          'js/CtrlResumoVendasFalhaSucesso.js',
		  'js/CtrlCadastroXRecarga.js',
		  'js/CtrlResCadastroXRecarga.js',
		  'js/CtrlResumoRecargaM4U.js',
		  
		  //Itens de controle  
		  'js/CtrlResumoIC.js',
		  'js/CtrlResumoRaco.js',
		  'js/CtrlResumoAutomacoes.js',
		  
		  //Repetidas 
		  'js/CtrlDerivacoesR.js',
		  'js/CtrlDerivacoesRIC.js',
		  'js/CtrlHistogramaClientes.js',
      'js/CtrlRetencaoLiquida.js',
	  'js/CtrlDerivacoesRTag.js',
	  'js/CtrlMetaURA.js',		  
		  
		  //Falhas e Legados		  
		  'js/CtrlResumoFalhas.js',
		  'js/CtrlResumoFalhasCallLogs.js',
		  
		  //Extratores
		  'js/CtrlExtratores.js',
		  
		  //Administrativo
		  'js/CtrlCadastroUsuarios.js',
		  'js/CtrlUserRequest.js',
		  'js/CtrlImportacaoAudio.js',	
		  
		  //Suporte
		  'js/CtrlAjuda.js',
		  
		  //Monitoração 
		  'js/CtrlMonitorBilhetagem.js',
		  'js/CtrlDashboardMailing.js',
		  'js/CtrlControleConsolidador.js',
		  'js/CtrlMonitorExportacaoMailing.js',
		  'js/CtrlMonitorTeradata.js',
		  'js/CtrlAcessoUsuarios.js',
		  'js/CtrlLogsEmAtraso.js',
		  'js/CtrlMonitorBilhetagemST.js',
		  'js/CtrlMonitorPromptIc.js',
		  'js/CtrlMonitorTesteProcedure.js',
		  'js/CtrlHomologacaoPrompt.js', 
		  
		  //Cradle to grave 2
		  'js/CtrlContGlobalTransf2.js',
		  'js/CtrlResumoContProdXGrupAtend2.js',
		  'js/CtrlResumoEmpresa2.js',
		  'js/CtrlResumoPontoDerGrupo2.js',
		  'js/CtrlChamadasCTG2.js',
		  'js/CtrlResumoUraxECH2.js',		  
		  'js/CtrlResumoTransferenciaPorTag.js',
		  
		  //Tela única	  
		  'js/CtrlResumoDDTelaUnica.js',
		  'js/CtrlResumoDesbloqueioTelaUnica.js',
		  'js/CtrlResumoContestacaoTelaUnica.js',
		  'js/CtrlResumoEPSTelaUnica.js',
		  'js/CtrlResumoTUOTFunilSegVia.js',
		  'js/CtrlResumoTUOTServicosVariaveis.js',
		  'js/CtrlResumoTUOTQtdEntrantes.js',
		  'js/CtrlResumoTUOTFunilDesbloqueio.js',
		  'js/CtrlResumoFrasesTU.js',
		  'js/CtrlAutomacaoOiTotal.js',
		  
		  //Cadastro e Migração
		  'js/CtrlResumoFontes.js',
		  'js/CtrlResumoDerivacoes.js',
		  'js/CtrlResumoCadastro.js',
		  'js/CtrlHigienizacaoCadastro.js',
		  
		  //UraAtiva
		  'js/CtrlUraAtiva.js',
		  
		  //Pesquisa de Satisfação
		  'js/CtrlResumoPesqServ.js',
		  'js/CtrlAtendimentoHumano.js',
		  'js/CtrlOfensoresRepetidas.js',
		  
          'js/controllers-common.js',	
          'js/controllers-common-analitico.js',		  		  
          'js/controllers-common-cache.js',
          'js/controllers-common-cadastro.js',
          'js/controllers-common-data.js',
          'js/controllers-common-filtro.js',
          'js/controllers-common-ftp.js',
          'js/controllers-common-prototipo.js',
          'js/controllers-common-thread.js',
          'js/controllers-common-update.js',
          'js/controllers-common-validacao.js',
          'js/controllers-common-xml.js',	
		  'js/routes.js',
          'js/diretivas.js'
          
        ],
        dest: 'js/controllers.js',
      },
    },
    exec: {
      abreEstalo: {
        cmd: 'estalo.exe'
      },
      uglify : {
        cmd : 'uglifyjs --compress -o js/controllers.js -- js/controllers.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-jslint');
  grunt.loadNpmTasks('grunt-exec');


  grunt.registerTask('default', ['concat', 'jslint', 'exec:abreEstalo']);
  grunt.registerTask('deploy', ['concat', 'exec:uglify','exec:abreEstalo']);
};
