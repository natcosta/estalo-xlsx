

[![Versatil](images/logo-versatil-ti.png)](http://www.versatec.com.br/)

## Cadastro de Usuário

 A tela de cadastro de usuário é responsável pela criação de usuários, alteração de seus dados e liberação de acesso para diversos relatórios. Com ela, também é possível atender as solicitações de acesso dos usuários.

### A Ferramenta
A ferramenta é organizada em algumas seções:
 - O grid principal é responsável por apresentar os usuários já cadastrados.
 - O botão ![Botão de pesquisa](images/search.png) no menu superior é utilizado para realizar pesquisa de usuários referente a um dado apresentado em qualquer um dos campos no grid principal.
 - O botão ![Botão de cadastro](images/addUser.png) está presente no menu superior e é responsável pelo cadastro de novos usuários. Os dados necessários para cadastro são Login, Domínio, Nome, E-mail e Empresa.
 - O botão ![Botão de solicitação de acesso](images/solicitacao.png) no menu superior é utilizado para verificar os usuários que solicitaram acesso ao Estalo e permitir ou negar o acesso à ferramenta.
 - O botão ![Botão de editar](images/editUser.png) no final de cada linha permite a alteração dos usuários e suas permissões.

### Solicitações de cadastro
 Para atender às demandas de solicitação de cadastro, basta verificar o número sobreposto no botão ![Botão de solicitação de acesso](images/solicitacao.png =30x30), equivalente às solicitações pendentes. Clicando nele, é possível verificar a lista de usuários em um modal como na imagem abaixo:
 ![Modal de solicitação de acesso](images/solicitacaoCadastros.png)
 
 Ao clicar no botão de edição ![Botão de editar](images/editUser.png) no modal de solicitações, você acessará o modal de permissões de usuário onde estão marcadas as permissões que o usuário solicitou. O autenticador pode dar e remover permissões antes de aceitar ou recusar o usuário. Confira na imagem:
 ![Modal de liberação de acesso](images/solicitacaoCadastro.png)
 
 ### Registro de novo usuário
 Na tela de cadastro, é possível registrar um novo usuário a partir do botão ![Botão de cadastro](images/addUser.png). Ao clicar nele um modal será exibido:
 
 ![Modal de cadastro](images/newUser.png)
 
 Para realizar o cadastro de um novo usuário, devem ser preenchidos os dados de Login, Domínio, Nome, E-mail e Empresa. Ao confirmar, o usuário será cadastrado com permissões padrão e após o registro será possível efetuar alteração de dados e permissões do usuário.
 
 
 
 