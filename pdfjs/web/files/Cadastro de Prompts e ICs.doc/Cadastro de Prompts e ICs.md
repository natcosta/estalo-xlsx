

[![Versatil](images/logo-versatil-ti.png)](http://www.versatec.com.br/)

# Cadastro de Prompts & IC's

As telas de cadastro de Prompts e IC's foram criadas com o intuito de automatizar o registro dos dados de Prompts e ICs para suas aplicações.

### A ferramenta
 A ferramenta é dividida em 3 passos, que consistem em importação, validação e confirmação dos dados registrados. Ela permite que os dados sejam carregados em diversas aplicações e sugere outras quando a aplicação selecionada for parte de um grupo. O seu funcionamento é baseado em templates "xlsx" criados pela Versátil.
 - [Template de IC's](http://www.versatec.com.br/downloads/bases/TemplatePrompt.xlsx)
 - [Template de Prompt's](http://www.versatec.com.br/downloads/bases/TemplateIC.xlsx) 
 
### O Processo ##
 O processamento dos dados é feito da seguinte forma:
 
 [![Tela de cadastro](images/Interface.png)](http://www.versatec.com.br/)
  - Uma ou mais aplicações são selecionadas
  - A planilha de Prompt ou IC no template da Versátil é carregada

 [![Tela de cadastro](images/linhas.png)](http://www.versatec.com.br/)
  - A planilha é verificada e os seus dados são apresentados na tela
  - O usuário confirma o cadastro dos dados.
 
 [![Tela de cadastro](images/completo.png)](http://www.versatec.com.br/)
  - O sistema realiza o cadastro.

### Aviso!
 A planilha não deve conter:
  - Dados a serem excluídos 
  - Dados repetidos
  - Uma segunda planilha

Os dados devem ser organizados na planilha primária e os ICs e Prompts devem ser carregados de arquivos diferentes. Dados já existentes serão automaticamente substituídos, sem a necessidade de exclusão para a inserção.

Em caso de erro, exceção ou qualquer problema durante o processo de cadastro um aviso será exibido informando o erro.
