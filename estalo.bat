::APAGAR CACHE
@echo off
set caminho=%appdata%
set modified=%caminho:Roaming=Local\Estalo%
@RD /S /Q %modified%
set caminhob=%appdata%
set modifiedb=%caminhob:Roaming=Local\Temp\nw*%
FOR /D %%f in (%modifiedb%) do @rmdir %%f /Q /S
::chcp 65000
@echo off

REM TASKKILL /F /IM node.exe /T
TASKKILL /F /IM tool.exe /T
@echo on
set fuso=tzutil/g
%fuso% > file.txt
set /p fusoUsuario=<file.txt
tzutil /s "%fusoUsuario%_dstoff"
@echo Change time zone...
SET LookForFile2="tool.exe.full"
IF EXIST %LookForFile2% (
del tool.exe
rename tool.exe.full tool.exe
)
start tool.exe
SET LookForFile="dstoff"
:CheckForFile
IF EXIST %LookForFile% GOTO FoundIt
timeout /t 1 /nobreak
GOTO CheckForFile
:FoundIt
ECHO Found: %LookForFile%
timeout /t 5 /nobreak
tzutil /s "%fusoUsuario%"
@echo Time zone fixed %fusoUsuario%
del dstoff
exit /b

