/*
 * CtrlVendas
 */
function CtrlVendas($scope,$globals) {
	
	win.title="Detalhamento de vendas";
	var limite = 50000;
	$scope.versao = versao;
	$scope.rotas = rotas;
	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
	$scope.estadoEnx = false;
	$scope.itemEnx = false;
	$scope.itemEnxAll = false;
	$scope.status = false;
	
	var tempData = "";
    var novaMacro;
	var csvExportado = '';

	
	//09/03/2015 Evitar conflito com função global que popula filtro de segmentos
	$scope.filtros = {
		isHFiltroED: false,
		naoAbordado: false,
		atualizacao: false
	};
	
	travaBotaoFiltro(0, $scope, "#pag-vendas", "Consulta realizada de 5 em 5 minutos  - Limite para exportação xlsx: "+limite+" registros.");
		
	
	$scope.vendas = [];
	$scope.csv = [];
	$scope.ordenacao = ['data_hora','chamador'];
	$scope.decrescente = false;
	$scope.ocultar = {
		tratado: false,
		log_formatado: false,
		log_original: true
	};
	
	$scope.colunas = [];
	$scope.gridDados = {
		data: "vendas",
		rowTemplate: '<div ng-dblclick="formataXML(row.entity)" ng-repeat="col in renderedColumns" ng-class="{\'sucesso\':row.getProperty(\'cod_status\') == 0,\'alerta\':row.getProperty(\'cod_status\') == 2,\'erro\':row.getProperty(\'cod_status\') == 1 }" class="ngCell {{col.cellClass}} {{col.colIndex()}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	
	$scope.venda_atual = undefined;
	$scope.callLog = "";
	$scope.logxml = "";
	$scope.extrator = false;
	
	$scope.log = [];
	
	$scope.filtro_aplicacoes = [];
	
	// Filtro: produto
	$scope.filtro_produtos = [];
	
	// Filtro: status_vendas
	$scope.estatos = status_vendas;
	
	// Filtro: erros_vendas
	$scope.erros = cache.erros;


	colunasExtras = cache.exibicaoCondicionalColunas['pag-vendas'];
	adicionarCtxId = false;
	adicionarOperador = false;
	adicionarCPF = false;
	adicionarContrato = false;
	adicionarExtra = false;
	
	function geraColunas(){
		var array = [];
		array.push({ field: "data_hora_BR", displayName: "Data", width: 143, pinned: true },
		{ field: "chamador", displayName: "Chamador", width: 98, pinned: false });
		$scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));
		if (!$scope.ocultar.tratado) {
			array.push({ field: "tel_digitado", displayName: "Tratado", width: 98, pinned: false });
		}
		var templateWithTooltip = '<div class="ngCellText" ng-class="col.colIndex()"><span title="{{row.getProperty(\'erro\')}}" ng-cell-text>{{row.getProperty(col.field)}}</span></div>';
		array.push({ field: "aplicacao", displayName: "Aplicação", width: 91, pinned: false },
		{ field: "codSegmento", displayName: "Segmento", width: 65, pinned: false });
		
		if($("select.filtro-aplicacao").val().indexOf('NOVAOITV') < 0 && $("select.filtro-aplicacao").val().indexOf('URACAD') < 0){
			array.push({
				field: "tipo", displayName: "Transação", width: 50,	pinned: false });
		}
		
		if($("select.filtro-aplicacao").val().indexOf('URACAD') >= 0){
			array.push({
				field: "tipo", displayName: "Fonte", width: 100, pinned: false });
		}
		
		array.push({ field: "grupo", displayName: "Grupo", width: 82, pinned: false },
		{ field: "produto", displayName: "Produto", width: $scope.filtros.naoAbordado ? 75 : 120, pinned: false });
		if(dominioUsuario.toUpperCase() !== "CONTAX-BR"){
			array.push({ field: "valor", displayName: "Valor", width: 61, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' });
		}
		
		array.push({ field: "status", displayName: "Status", width: 70, pinned: false },
		{ field: "codErro", displayName: "Erro", width: 52,cellClass: "grid-align", pinned: false, cellTemplate: templateWithTooltip });
		array.push({ field: "codOferta", displayName: "idPromo", width: (unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.cpf){ return true}})).toString() || false ? 100 : 50) , pinned: false });
		
		adicionarOperador = false;
		if(unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.operador){ return true}})).toString() || false){
			array.push({ field: "codOperador", displayName: "Operador", width: 54, pinned: false });
			adicionarOperador = true;			
		}
		adicionarCPF = false;
		if(unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.cpf){ return true}})).toString() || false){
			array.push({ field: "cpf", displayName: "CPF", width: 100, pinned: false });
			adicionarCPF = true;
		}
		adicionarContrato = false;
		if(unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.contrato){ return true}})).toString() || false){
			array.push({ field: "contrato", displayName: "Contrato", width: 83, pinned: false });
			adicionarContrato = true;
		}		

		aplicacoes = $("select.filtro-aplicacao").val().toString().split(',')
		adicionarCtxId = false;
		aplicacoes.forEach(function(aplicacao){
			if (colunasExtras[aplicacao] != undefined && colunasExtras[aplicacao].indexOf('CtxID')!= -1){
				adicionarCtxId = true;
			}
		})
		
		if($scope.filtros.naoAbordado){
			
			array.push({ field: "MotivoNaoAbordagem", displayName: "Motivo", width: 120, pinned: false });
		}
		
		
		if (adicionarCtxId){
			array.push({ field: "ctxid", displayName: "CtxId", width: 80, pinned: false });
		}

		//ES-3916 Brunno
		array.push({ field: "codOfertaOrigem", displayName: "idPromoOriginal", width: (unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.cpf){ return true}})).toString() || false ? 100 : 50) , pinned: false });
		//ES-4165
		if($scope.status === true){
		array.push({ field: "status_naotratado", displayName: "Ultimo Status", width: 80, pinned: false });
		}
		
		adicionarExtra = false;
		if($("select.filtro-aplicacao").val().indexOf('URACAD') >= 0){
			adicionarExtra = true;
			array.push(
				{field: "cpf", displayName: "CPF", width: 100, pinned: false},
				{field: "encerramento", displayName: "Encerramento", width: 80, pinned: false},
				{field: "derivacao", displayName: "Derivação", width: 150, pinned: false}				
			);
		}
		
		array.push({ field: "ucid", displayName: "Log", width: 59, cellClass: "grid-align", pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>' }
		);

		if($scope.filtros.atualizacao){
			array.push({
				field: "atualStatus",
				displayName: "Atualizado em",
				width: 143,
				pinned: false
			});
		}
		
		
		
		return array;
	}
	
	// Filtros: data e hora
	var agora = new Date();
	
	//03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();
	
	$scope.periodo = {
		inicio:inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
	};
	
	var $view;
	
	$scope.$on('$viewContentLoaded', function () {
	$view = $("#pag-vendas");
	$(".aba3").css({'position':'fixed','left':'60px','top':'40px'});
	$(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
	
	//{ Bloco responsável pela SEARCHTOOL
	function searchAndHighlight(searchTerm, selector, resultElement) {
		if (searchTerm && !(searchTerm === " ")) {
			var selector = selector;
			$('.highlighted').removeClass('highlighted');
			if (selector == "" || !selector) {
				if ($("div.log-formatado").css('display') == 'block') {
					selector = "div.log-formatado";
				}
				if ($("div.log-original").css('display') == 'block') {
					selector = "div.log-original";
				}
				if ($("div.log-prerouting").css('display') == 'block') {
					selector = "div.log-prerouting";
				}
				if ($("div.log-preroutingret").css('display') == 'block') {
					selector = "div.log-preroutingret";
				}
			}
			console.log("BEFORE BUGGING BEFORE BUGGING Selector: " + selector);


			$(selector).highlight(searchTerm, {
				className: 'match'
			});
			var matches = $('.match');
			//console.log("Matches: "+matches.length+" searchTerm "+searchTerm);
			// Funções dependentes de resultados a seguir
			if (matches != null && matches.length > 0) {

				try {
					resultElement.text(matches.length + ' encontrados'); // Conta a quantidade de elementos encontrados
				} catch (er) {
					console.log("Erro na função de pesquisa " + er);
				}

				// Marca o primeiro elemento MATCH como highlighted ao efetuar uma pesquisa
				$('.match:first').addClass('highlighted');

				var i = 0;

				// Função responsável por "scrollar" e marcar o próximo elemento match como highlighted
				$('.searchtool_next').on('click', function() {

					i++;

					if (i >= $('.match').length) i = 0;

					//Remove o atual e marca o próximo elemento match
					$('.match').removeClass('highlighted');
					$('.match').eq(i).addClass('highlighted');

					// Cálculo final até a posição do elemento marcado highlighted
					var container = $('.modal-body'),
						scrollTo = $('.match').eq(i);
					var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

					container.scrollTop(
						wheretogo - 125
					);
					$('.highlighted').focus();

					// Fim do cálculo e scroll
				});

				// Função para "Scrollar" e "Marcar" o anterior
				$('.searchtool_prev').on('click', function() {

					i--;
					if (i < 0) i = $('.match').length - 1;
					//Remove o atual e marca o elemento anterior
					$('.match').removeClass('highlighted');
					$('.match').eq(i).addClass('highlighted');
					//Cálculo final da posição para o scroll
					var container = $('.modal-body'),
						scrollTo = $('.match').eq(i);
					var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())


					container.scrollTop(
						wheretogo - 125
					);

        stmt += " FROM " + db.prefixo + "VendasURAPromo as VUP"
        stmt += " left outer join " + db.prefixo + "ProdutosURA as PU"
        stmt += " on PU.CodProduto = VUP.CodProduto"
		
		
		if (unique2(cache.apls.map(function(a) {
                if ($("select.filtro-aplicacao").val().indexOf(a.codigo) >= 0 && a.cpf) {
                    return true
                }
            })).toString() || false) {
            stmt += " left outer join CPF_CHAMADA as CC on CC.coducidivr = VUP.CodUCID";
            stmt += " left outer join CONTRATO_CHAMADA as CTC on CTC.CodUcidIvr = VUP.CodUCID";
        }
        stmt += " WHERE 1 = 1" +
            "   AND VUP.DataHora >= '" + formataDataHora(data_ini) + "' AND  VUP.DataHora <= '" + formataDataHora(data_fim) + "'";
        stmt += restringe_consulta("VUP.CodAplicacao", filtro_aplicacoes, true);
        stmt += restringe_consulta("VUP.CodProduto", filtro_produtos, true);
        stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true);
        stmt += restringe_consulta("VUP.status", filtro_status, true);
        stmt += restringe_consulta("CodErro", filtro_erros, true);
        stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
        stmt += restringe_consulta("TipoTransacao", filtro_parceiros, true);

        if ($scope.filtros.naoAbordado) {
            stmt += " UNION ALL " + db.use + "SELECT" + testaLimite($scope.limite_registros) +
				"   I.CodUCID, I.DataHora,NULL AS ATUALIZACAOSTATUS, I.NumANI, I.TelDigitado, I.CodAplicacao, ctxid," +
				" '' as TipoTransacao, '' as GrupoProduto, '' as CodProduto, '' as Valor, Status, '' as CodErro, '' as CodOferta, '' as CodOfertaOrigem," +
				" I.CodSegmento, I.codOperador, '' as TipoVenda"
            stmt += " FROM " + db.prefixo + "ChamadasSemVendaS2S as I, MotivosNaoAbordagemS2S M"
            stmt += " WHERE 1 = 1 AND ISNULL(I.MotivoNaoAbordagem, '0') = M.CodMotivo" +
                "   AND DataHora >= '" + formataDataHora(data_ini) + "' AND  DataHora <= '" + formataDataHora(data_fim) + "'";

            stmt += restringe_consulta("I.CodAplicacao", filtro_aplicacoes, true);
            stmt += restringe_consulta("Status", filtro_nao_abordado, true);
        }


					$('.highlighted').focus();

				});


				if ($('.highlighted:first').length) { //if match found, scroll to where the first one appears
					$(window).scrollTop($('.highlighted:first').position().top);
				}
				return true;
			}
		}
		return false;
	}
	//}
	
	//{ Função responsável por checar atualizações no INPUT text
	$('.searchtool_text').each(function() {
		// ResultElement é o elemento onde a quantidade de resultados é exibida
		var resultElement = $('.search_results');
		var searchElement = $(this);
		search();
		// Save current value of element
		$(this).data('oldVal', $(this));
		
		// Look for changes in the value
		$(this).bind("propertychange keyup input paste", function(event){
			// If value has changed...
			if ($(this).data('oldVal') != $(this).val()) {
				// Updated stored value
				$(this).data('oldVal', $(this).val());
				$("div .log-original").unhighlight({className: 'match'});
				$("div .log-formatado").unhighlight({className: 'match'});
				$("div .log-prerouting").unhighlight({className: 'match'});
				$("div .log-preroutingret").unhighlight({className: 'match'});
				if (!searchAndHighlight($(this).val(), "", resultElement)) {
					resultElement.html('Sem resultados.');
				}
			}
		});
	}).delay(300);

	$(".changeSearchLog").click(function(){
		var resultElement = $('.search_results');
	  clearSearchTool("", resultElement);
	  // console.log("Chamou a função para limpar");
	});

	$("#modal-log").on('hidden', function () {
	  var resultElement = $('.search_results');
	  var searchElement = $('.searchtool_text');
	  clearSearchTool(searchElement, resultElement);
	  // console.log("Chamou a função para limpar");
	});	
	//} Fim do bloco responsável pela searchtool
	
	//19/03/2014
	componenteDataHora ($scope,$view);

	carregaAplicacoes($view,false,true,$scope);// $view, false, true, $scope

	carregaSegmentosPorAplicacao($scope,$view,true);
	// Popula lista de segmentos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});

	carregaGruposProdutoPorAplicacao($scope,$view,true);
	// Popula lista de  grupo de produtos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function(){ 
		carregaGruposProdutoPorAplicacao($scope,$view);
		carregaProdutosPorGrupo($scope,$view);
	});

	carregaProdutosPorGrupo($scope,$view,true);
	// Popula lista de  produtos a partir dos grupo de produtos selecionados
	$view.on("change", "select.filtro-grupo-produto", function(){ carregaProdutosPorGrupo($scope,$view)});


	carregaResultados($view);
	carregaResultadosNaoAbordados($view);
	carregaErros($view,true);
	carregaEmpresasRecarga($view,true);
	  


	//Marcar todas aplicações
	$view.on("click", "#alinkApl", function() {
		marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope, true)
	});

	//Marca todos os produtos
	$view.on("click", "#alinkProd", function() {
		marcaTodosIndependente($('.filtro-produto'), 'produtos')
	});

	// Marca todos os erros
	$view.on("click", "#alinkErr", function() {
		marcaTodosIndependente($('.filtro-erro'), 'erros')
	});

	// Marca todos os resultados
	$view.on("click", "#alinkRes", function() {
		marcaTodosIndependente($('.filtro-status'), 'resultados')
	});

	// Marca todos os nao abordados
	$view.on("click", "#alinkRes", function() {
		marcaTodosIndependente($('.filtro-status-nao-abordado'), 'resultados')
	});

	// Marca todos os parceiros de recarga
	$view.on("click", "#alinkRecarga", function() {
		marcaTodosIndependente($('.filtro-empresaRecarga'), 'resultados')
	});

	// Marca todos os segmentos
	$view.on("click", "#alinkSeg", function() {
		marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
	});


	$view.on("change", "select.filtro-grupo-produto", function() {
		Estalo.filtros.filtro_grupos_produtos = $(this).val() || [];
	});

	$view.on("change", "select.filtro-produto", function() {
		Estalo.filtros.filtro_produtos = $(this).val() || [];
	});

	$view.on("change", "select.filtro-erro", function() {
		Estalo.filtros.filtro_erros = $(this).val() || [];
	});

	$view.on("change", "select.filtro-status", function() {
		Estalo.filtros.filtro_resultados = $(this).val() || [];
	});

	$view.on("change", "select.filtro-status-nao-abordado", function() {
		Estalo.filtros.filtro_nao_abordado = $(this).val() || [];
	});

	$view.on("change", "select.filtro-segmento", function() {
		Estalo.filtros.filtro_segmentos = $(this).val();
	});

	$view.find("select.filtro-aplicacao").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} aplicações',
		showSubtext: true
	});

	$view.find("select.filtro-status").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} resultados',
		showSubtext: true
	});

	$view.find("select.filtro-status-nao-abordado").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} não abordados',
		showSubtext: true
	});

	$view.find("select.filtro-erro").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} erros',
		showSubtext: true
	});

	$view.find("select.filtro-empresaRecarga").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} parceiros',
		showSubtext: true
	});

	$view.find("select.filtro-grupo-produto").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} grupos'
	});

	$view.find("select.filtro-produto").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} produtos'
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function() {
		$("div.data-inicio.input-append.date").data("datetimepicker").hide();
		$("div.data-fim.input-append.date").data("datetimepicker").hide();
		$('div.btn-group.filtro-aplicacao').addClass('open');
		$('div.btn-group.filtro-aplicacao>div>ul').css({
			'max-height': '500px',
			'overflow-y': 'auto',
			'min-height': '1px',
			'max-width': '350px'
		});
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function() {
		$('div.btn-group.filtro-aplicacao').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseover(function() {
		//11/07/2014 não mostrar filtros desabilitados
		if (!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')) {
			$('div.btn-group.filtro-grupo-produto').addClass('open');
		}

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseout(function() {
		$('div.btn-group.filtro-grupo-produto').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-produto').mouseover(function() {
		//11/07/2014 não mostrar filtros desabilitados
		if (!$('div.btn-group.filtro-produto .btn').hasClass('disabled')) {
			$('div.btn-group.filtro-produto').addClass('open');
			$('div.btn-group.filtro-produto>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px'
			});
		}

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-produto').mouseout(function() {
		$('div.btn-group.filtro-produto').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-erro').mouseover(function() {
		$('div.btn-group.filtro-erro').addClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '-30px'
		});
		$('div.btn-group.filtro-erro>div>ul').css({
			'max-width': '450px',
			'max-height': '500px',
			'overflow-y': 'auto',
			'min-height': '1px'
		});

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-erro').mouseout(function() {
		$('div.btn-group.filtro-erro').removeClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '0px'
		});
		$('div.btn-group.filtro-erro>div>ul').css({
			'max-width': '450px',
			'max-height': '500px',
			'overflow-y': 'auto',
			'min-height': '1px'
		});
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseover(function() {
		$('div.btn-group.filtro-empresaRecarga').addClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '-30px'
		});
		$('div.btn-group.filtro-empresaRecarga>div>ul').css({
			'max-width': '450px',
			'max-height': '500px',
			'overflow-y': 'auto',
			'min-height': '1px'
		});

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseout(function() {
		$('div.btn-group.filtro-empresaRecarga').removeClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '0px'
		});
		$('div.btn-group.filtro-empresaRecarga>div>ul').css({
			'max-width': '450px',
			'max-height': '500px',
			'overflow-y': 'auto',
			'min-height': '1px'
		});
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-status').mouseover(function() {
		$('div.btn-group.filtro-status').addClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '-52px'
		});

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-status').mouseout(function() {
		$('div.btn-group.filtro-status').removeClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '0px'
		});
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-status-nao-abordado').mouseover(function() {
		$('div.btn-group.filtro-status-nao-abordado').addClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '-52px'
		});

	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-status-nao-abordado').mouseout(function() {
		$('div.btn-group.filtro-status-nao-abordado').removeClass('open');
		$('div.dropdown-menu.open').css({
			'margin-left': '0px'
		});
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function() {
		//11/07/2014 não mostrar filtros desabilitados
		if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
			$('div.btn-group.filtro-segmento').addClass('open');
			$('div.btn-group.filtro-segmento>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '350px'
			});
		}
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function() {
		$('div.btn-group.filtro-segmento').removeClass('open');
	});
	
	$view.on("dblclick", "div.ng-scope.ngRow", function () {
		$scope.$apply(function () {
			$scope.ocultar.log_original = true;
			$scope.ocultar.log_formatado = false;
		});
		$view.find("#modal").modal('show');
	});

	// Lista vendas conforme filtros
	$view.on("click", ".btn-gerar", function () {
		limpaProgressBar($scope, "#pag-vendas");
		
		//22/03/2014 Testa se data início é maior que a data fim
		var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
		var testedata = testeDataMaisHora(data_ini,data_fim);
		if(testedata!==""){
			setTimeout(function(){
				atualizaInfo($scope,testedata);
				effectNotification();
				$view.find(".btn-gerar").button('reset');
			},500);
			return;
		}
		
		//22/03/2014 Testa se uma aplicação foi selecionada
		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
			setTimeout(function(){
				atualizaInfo($scope,'Selecione uma aplicação');
				effectNotification();
				$view.find(".btn-gerar").button('reset');
			},500);
			return;
		}
		
		$scope.colunas = geraColunas();
		$scope.listaVendas.apply(this);
		
		
	});

	$view.on("click", ".btn-log-original", function (ev) {
		ev.preventDefault();
		$scope.consultaCallLog($scope.venda_atual);
		$scope.$apply(function () {
			$scope.ocultar.log_formatado = true;
			$scope.ocultar.log_original = false;
		});
	});
	
	$view.on("click", ".btn-log-formatado", function (ev) {
		killThreads();
		ev.preventDefault();
		$scope.formataXML($scope.venda_atual);
		$scope.$apply(function () {
			$scope.ocultar.log_original = true;
			$scope.ocultar.log_formatado = false;
		});
	});
	
	$view.on("click", ".btn-exportar", function () {		
		$scope.exportaXLSX.apply(this);
	});
	
	//Limpa filtros 03/03/2014
	$view.on("click", ".btn-limpar-filtros", function () {
		$scope.limparFiltros.apply(this);
	});
	
	$scope.limparFiltros = function () {
		iniciaFiltros();
		componenteDataHora ($scope,$view,true);
		var partsPath = window.location.pathname.split("/");
		var part  = partsPath[partsPath.length-1];
		
		var $btn_limpar = $view.find(".btn-limpar-filtros");
		$btn_limpar.prop("disabled", true);
		$btn_limpar.button('loading');
		
		setTimeout(function(){
			$btn_limpar.button('reset');
		},500);
	}
	
	//Botão agora 03/03/2014
	$view.on("click", ".btn-agora", function () {
		$scope.agora.apply(this);
	});
	
	//02/04/2014
	$scope.agora = function () {
		iniciaAgora($view,$scope);
	}
	$view.on("click", ".btn-exportarCSV-interface", function() {
		var newName = formataDataHoraMilis(new Date()).replace(/[^0-9]/g,'');
		if(csvExportado === ''){
		  fs.appendFileSync(tempDir3() + 'dados_vendas.csv', tempData,'ascii');				
		  fs.renameSync(tempDir3() + 'dados_vendas.csv',	tempDir3() + 'export_'+newName+'.csv');                
				  childProcess.exec("start excel " + tempDir3() + 'export_'+ newName +'.csv', 
				  function (error, stdout, stderr) {
					  if (error) {
						  console.error("exec error: " + error);
						  alert("Parece que o seu sistema não tem o Excel instalado.");
						  childProcess.exec(tempDir3() + 'export_'+newName+'.csv');
						  return;
					  }                        
					  
				  });
				  csvExportado = tempDir3() + 'export_'+newName+'.csv';               
				 
		}else{
		  alert("Dado já foi exportado("+csvExportado+").");
		}
		$(".dropdown-exportar").toggle();
	  });
	
	 // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function() {
            $scope.abortar.apply(this);
			var $btn_exportar = $view.find(".btn-exportar");
			var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
			var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
			$btn_exportar.prop("disabled", false);
			$btn_exportar_csv.prop("disabled", false);
			$btn_exportar_dropdown.prop("disabled", false);	
        });
	
	//23/05/2014
	$scope.abortar = function () {
		abortar($scope, "#pag-vendas");
	}
	
	$view.on("mouseover",".grid",function(){$('div.notification').fadeIn(500)});	
	$view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});
	
	//Lista vendas conforme filtros
	$scope.listaVendas = function () {
		$scope.status =false;
		csvExportado = '';
		
		if (fs.existsSync(tempDir3() + 'dados_vendas.csv')) {
            fs.unlinkSync(tempDir3() + 'dados_vendas.csv');
        }
		var header = '';
		for (var item in $scope.colunas) {
			if(typeof($scope.colunas[item]) === "object"){
				if($scope.colunas[item].field !== "ucid") header += $scope.colunas[item].displayName +';';
				if($scope.colunas[item].field === "codErro") header += 'Descrição erro;'; 
				if($scope.colunas[item].field === "codOferta") header += 'Tipo venda;'; 
				
			}				
		}			
		header += ';\r\n';
		tempData = header;

		var datasComDados = {};
		
		if($scope.extrator){ $scope.limite_registros = 0;}
		$globals.numeroDeRegistros = 0;
		
		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);
		
		var $btn_gerar = $(this);
		$btn_gerar.button('loading');
		
		var data_ini = $scope.periodo.inicio,data_fim = $scope.periodo.fim;
		
		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_produtos = $view.find("select.filtro-produto").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_grupo_produtos = $view.find("select.filtro-grupo-produto").val() || [];
		var filtro_status = $view.find("select.filtro-status").val() || [];
		var filtro_nao_abordado = $view.find("select.filtro-status-nao-abordado").val() || [];
		var filtro_erros = $view.find("select.filtro-erro").val() || [];
		var filtro_parceiros = $view.find("select.filtro-empresaRecarga").val() || [];
		if (filtro_status.indexOf("0") !== -1) { filtro_status.push("20"); }
		
		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
		+ " até " + formataDataHoraBR($scope.periodo.fim);
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_produtos.length > 0) { $scope.filtros_usados += " Produtos: " + filtro_produtos; }
		if (filtro_grupo_produtos.length > 0) { $scope.filtros_usados += " Grupos: " + filtro_grupo_produtos; }
		if (filtro_status.length > 0) { $scope.filtros_usados += " Status: " + filtro_status; }
		if (filtro_nao_abordado.length > 0) { $scope.filtros_usados += " Não Abordados: " + filtro_nao_abordado; }
		if (filtro_erros.length > 0) { $scope.filtros_usados += " Erros: " + filtro_erros; }
		if (filtro_parceiros.length > 0) { $scope.filtros_usados += " Parceiros: " + filtro_parceiros; }
		
		$scope.vendas = [];
		$scope.csv = [];
		
		var stmt = db.use + "SELECT" + testaLimite($scope.limite_registros)
		+ "   CodUCID, DataHora,"+ ($scope.filtros.atualizacao ? "DataAtualizacao AS ATUALIZACAOSTATUS," : "") +" NumANI, TelDigitado, VUP.CodAplicacao, VUP.ctxid as ctxid, ";
		
		stmt += " TipoTransacao, PU.GrupoProduto, VUP.CodProduto, Valor, CAST(Status as varchar(16)) as Status, CodErro, CodOferta, CodOfertaOrigem, codSegmento, codOperador, TipoVenda, '' as MotivoNaoAbordagem, extra";
		if(unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.cpf){ return true}})).toString() || false){
			stmt += " ,CC.cpf,CTC.Contrato";
		}

		stmt += " FROM " + db.prefixo + "VendasURAPromo as VUP"
		stmt += " left outer join " + db.prefixo + "ProdutosURA as PU"
		stmt += " on PU.CodProduto = VUP.CodProduto";
		
		if(unique2(cache.apls.map(function(a){if($("select.filtro-aplicacao").val().indexOf(a.codigo)>=0 && a.cpf){ return true}})).toString() || false){
			stmt += " left outer join CPF_CHAMADA as CC on CC.coducidivr = VUP.CodUCID";
			stmt += " left outer join CONTRATO_CHAMADA as CTC on CTC.CodUcidIvr = VUP.CodUCID";
		}
		stmt += " WHERE 1 = 1"
		+ "   AND DataHora >= '" + formataDataHora(data_ini) + "' AND  DataHora <= '" + formataDataHora(data_fim) + "'";
		stmt += restringe_consulta("VUP.CodAplicacao", filtro_aplicacoes, true);
		stmt += restringe_consulta("VUP.CodProduto", filtro_produtos, true);
		stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true);
		stmt += restringe_consulta("status", filtro_status, true);
		stmt += restringe_consulta("CodErro", filtro_erros, true);
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
		stmt += restringe_consulta("TipoTransacao", filtro_parceiros, true);

		if ($scope.filtros.naoAbordado) {
			stmt += " UNION ALL " + db.use + "SELECT" + testaLimite($scope.limite_registros)
			+ "   I.CodUCID, I.DataHora, "+ ($scope.filtros.atualizacao ? "NULL AS ATUALIZACAOSTATUS," : "") +" I.NumANI, I.TelDigitado, I.CodAplicacao, ctxid, '' as TipoTransacao, '' as GrupoProduto, '' as CodProduto, '' as Valor, Status, '' as CodErro, '' as CodOferta, '' as CodOfertaOrigem, I.CodSegmento, I.codOperador, '' as TipoVenda, M.Descricao as Motivo, '' as extra"
		    // ISNULL(I.MotivoNaoAbordagem, '0')"
			stmt += " FROM " + db.prefixo + "ChamadasSemVendaS2S as I, MotivosNaoAbordagemS2S M"		  
			stmt += " WHERE 1 = 1 AND ISNULL(I.MotivoNaoAbordagem, '0') = M.CodMotivo"		  
			+ "   AND DataHora >= '" + formataDataHora(data_ini) + "' AND  DataHora <= '" + formataDataHora(data_fim) + "'";

			stmt += restringe_consulta("I.CodAplicacao", filtro_aplicacoes, true);
			stmt += restringe_consulta("Status", filtro_nao_abordado, true);
		}

		
		console.log('FILTRO_STATUS = ' + filtro_status);

		stmt = stmt + " order by DataHora, NumANI";
		$scope.extrator ? log("Extrator: "+stmt) : log(stmt);
		var stmtCountRows = stmtContaLinhasVendas(stmt);
		
		//24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			var item1 = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'4": "'+columns[1].value+'0:00'+'|'+columns[1].value+'4:59'+'"}';
			var item2 = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'9": "'+columns[1].value+'5:00'+'|'+columns[1].value+'9:59'+'"}';
			jsonConcat(datasComDados,JSON.parse(item1));	
			jsonConcat(datasComDados,JSON.parse(item2));
            $globals.numeroDeRegistros = columns[0].value;
		}
		
		var s = {};
		
		function executaQuery1(columns) {
			
			executaQuery(columns);
			// Insere no grid
			$scope.vendas.push(s);
			var a = [];		

			var colunas = [];
   
			for (var item in $scope.colunas) {	
				a.push(s[$scope.colunas[item].field]);					
			}
			$scope.csv.push(a);	
			

            if ($scope.vendas.length % 1000 === 0) {		
                fs.appendFileSync(tempDir3() + 'dados_vendas.csv', tempData,'ascii');
                tempData = "";
                $scope.$apply();

            }
		}
		
		function executaQuery2(columns) {
			
			executaQuery(columns);
			if (tempData.length > 240000) {	
				fs.appendFileSync(tempDir3() + 'dados_vendas.csv', tempData,'ascii');
				tempData = "";
			}
		}
		
		function executaQuery(columns) {
			
			s = {};	
			
			var UCID = columns["CodUCID"].value,
			data_hora = formataDataHora(columns["DataHora"].value),
			data_hora_BR = formataDataHoraBR(columns["DataHora"].value),			
			data = columns["DataHora"].value,
			atualStatus = columns["ATUALIZACAOSTATUS"]!==undefined ? (columns["ATUALIZACAOSTATUS"].value !== null ? formataDataHoraBR(columns["ATUALIZACAOSTATUS"].value) : ""): "",
			chamador = columns["NumANI"].value,
			tel_digitado = columns["TelDigitado"].value,
			cod_aplicacao = columns["CodAplicacao"].value,
			aplicacao = obtemNomeAplicacao(cod_aplicacao),
			tipo = columns["TipoTransacao"].value,
			grupo = columns["GrupoProduto"].value,
			produto = obtemNomeProdutoVenda(columns["CodProduto"].value),
			valor = +columns["Valor"].value.toFixed(2),  //valor = toFixed(columns[2].value, 2), //Não ordena
			cod_status = columns["Status"].value,
			status = obtemNomeStatusVenda(cod_status),
			codErro = columns["CodErro"].value !== 0 ? columns["CodErro"].value : 0,
			codOferta = columns["CodOferta"].value,
			codSegmento = obtemNomeSegmento(columns["codSegmento"].value),
			codOperador = columns["codOperador"] !== undefined ? columns["codOperador"].value : undefined,
			tipovenda = columns["TipoVenda"] !== undefined ? columns["TipoVenda"].value : undefined,			
			cpf = columns["cpf"] !== undefined ? columns["cpf"].value : undefined,
			contrato = columns["Contrato"] !== undefined ? columns["Contrato"].value : undefined;
			ctxid = columns["ctxid"].value,
			MotivoNaoAbordagem = columns["MotivoNaoAbordagem"].value,
			codOfertaOrigem = columns["CodOfertaOrigem"].value,
			status_naotratado = cod_status === "-1" || cod_status === "3" || cod_status === "-2"? "Invalido":"";
			
			$scope.status = cod_status === "-1" ||  cod_status === "3"  || $scope.status === true || cod_status === "-2"? true:false
			
			//16/04/2014
			var erro = columns["CodErro"].value !== 0 ? obtemNomeErroVenda(columns["CodErro"].value,retornaGrupoProduto(produto)) : null;
			
			var extra = {};
			
			try{
				extra = JSON.parse(columns["extra"].value);
				if(extra === null) extra = {};
			}catch(ex){
				extra = {};
				console.log(ex);	
			}
			
			
			
			s = {
				UCID: UCID,
				data_hora: data_hora,
				data_hora_BR: data_hora_BR,
				data: data,
				chamador: chamador,
				tel_digitado: tel_digitado,
				cod_aplicacao: cod_aplicacao,
				aplicacao: aplicacao,
				tipo: tipo,
				grupo: grupo,
				produto: produto,
				valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
				cod_status: cod_status,
				status: status,
				icone_status: cod_status !== 1 ? "icon-thumbs-up" : "icon-thumbs-down",
				erro: erro,
				codErro: codErro,
				codOferta: codOferta,
				codSegmento: codSegmento,
				codOperador: codOperador,
				cpf: cpf,
				contrato: contrato,
				tipovenda: tipovenda,
				ctxid: ctxid || ' ',
				atualStatus: atualStatus,
				MotivoNaoAbordagem: MotivoNaoAbordagem,
				codOfertaOrigem: codOfertaOrigem,
				status_naotratado: status_naotratado
			}
			
	
			Object.keys(extra).forEach(function(e){
				if(e === "encerramento"){
					s[e] = extra[e] === 'FIN' ? "Finalizada" : extra[e] === 'ABN' ? "Abandonada" : "Derivada";
				}else{
					s[e] = extra[e];	
				}				
			})	
		
			
			tempData += data_hora_BR + ';' +
			chamador + ';';
			if (!$scope.ocultar.tratado) {
				tempData += tel_digitado + ';';
			}
			tempData += aplicacao + ';' +
			codSegmento + ';';
			try{
				if($("select.filtro-aplicacao").val().indexOf('NOVAOITV') < 0) tempData += tipo + ';';			
			}catch(ex){
				console.log(ex);
			}
			tempData += grupo + ';' +
			produto + ';' +
			valor + ';' +
			status + ';' +
			codErro + ';' +
			(erro !== null ? erro : "ND") + ';' +
			(codOferta !== undefined ? codOferta : "ND") + ';' +
			(tipovenda !== undefined ? tipovenda : "ND") + ';';
			if(adicionarOperador){
				tempData +=	codOperador + ';'; 
			}
			if($scope.filtros.naoAbordado){
				tempData +=(MotivoNaoAbordagem !== undefined ? MotivoNaoAbordagem : "NA") + ';';
			}			
			if(adicionarCPF){
				tempData +=	(cpf !== undefined ? cpf : "NA") + ';';
			}
			if(adicionarContrato){
				tempData += (contrato !== undefined ? contrato : "NA") + ';';
			}
			if (adicionarCtxId){
				tempData += (ctxid !== 'null' ? "'"+ctxid : "NA") + ';';
			}
			tempData += (codOfertaOrigem !== null ? codOfertaOrigem : "ND") + ';';
			if($scope.filtros.atualizacao){
				tempData += (atualStatus !== undefined ? atualStatus : "NA") + ';';
			}
			if(adicionarExtra){
				tempData += (extra["cpf"] !== undefined ? extra["cpf"] : null) + ';';
				tempData += (extra["encerramento"] !== undefined ? extra["encerramento"] : null) +';';	
				tempData += (extra["derivacao"] !== undefined ? extra["derivacao"] : null) +';';	
			}		
			tempData += '\r\n';
			// ES-4165 Brunno
			if($scope.status === true){
				$scope.colunas = geraColunas();
				$scope.status = false;
			}
			
		}

		function retornaQuery(indice){	
			var obj = Object.keys(datasComDados);
			var stmtMod = stmt;
			var ini =  new RegExp(stmtMod.match(/>= '(.*?)'/)[1],'g');
			var fim =  new RegExp(stmtMod.match(/<= '(.*?)'/)[1],'g');
			if(indice > 0)stmtMod = stmtMod.replace(ini,datasComDados[obj[indice]].split('|')[0]);
			if(indice < obj.length-1) stmtMod = stmtMod.replace(fim,datasComDados[obj[indice]].split('|')[1]);
			return stmtMod;
		}		
		
		
		var controle = 0;
		var total = 0;
		
		function proximaHora(stmt){
			
			function comOuSemDados(err, c, txt) {
                $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, Object.keys(datasComDados).length) + " Encontrados: " + total + " "+txt).selectpicker('refresh');
                controle++;
                $scope.$apply();

                //Executa dbquery enquanto array de querys menor que length-1
                if (controle < Object.keys(datasComDados).length) {
                    proximaHora(retornaQuery(controle));
                    if (c === undefined) console.log(controle);
                }

                //Evento fim
                if (controle === Object.keys(datasComDados).length) {
                    if (c === undefined) console.log("fim " + controle);
                    if (err) {
                        retornaStatusQuery(undefined, $scope);
                    } else {
                        retornaStatusQuery($scope.vendas.length, $scope);
                    }

                    $btn_gerar.button('reset');
                    if ($scope.vendas.length > 0) {
                        fs.appendFileSync(tempDir3() + 'dados_vendas.csv', tempData,'ascii');
						tempData = "";
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
                    } else {
                        $btn_exportar.prop("disabled", true);
                        $btn_exportar_csv.prop("disabled", true);
                        $btn_exportar_dropdown.prop("disabled", true);
                    }
                }
            }
			db.query(stmt,($scope.vendas.length < limite ? executaQuery1 : executaQuery2), function(err, num_rows) {
                    console.log("Executando query-> " + stmt + " " + num_rows);
                    num_rows !== undefined ? total += num_rows : total += 0;
                    comOuSemDados(err,undefined,($scope.vendas.length < limite ? "" : "Inserindo em arquivo..."));

                });
	
		}
		
		db.query(stmtCountRows, contaLinhas, function(err, num_rows) {
            console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);

            if ($globals.numeroDeRegistros === 0) {
                retornaStatusQuery(0, $scope);
                $btn_gerar.button('reset');
            } else {
                //Disparo inicial
                proximaHora(retornaQuery(0));
            }

        });
		

	
	$view.on("mouseup", "tr.detalhamento", function () {
		var that = $(this);
		$('tr.detalhamento.marcado').toggleClass('marcado');
		$scope.$apply(function () {
			that.toggleClass('marcado');
		});
	});
	};
	
	// Consulta o log da chamada
  $scope.consultaCallLog = function (chamada) {
    
    // Se a chamada acabou de ser consultada (incluindo o log original), não há nada a fazer
    if (chamada === $scope.venda_atual && $scope.callLog !== "" && $scope.logxml !== "") return true;

    var tabela = "CallLog_";
    if (unique2(cache.apls.map(function (a) {
        if (a.nvp) return a.codigo;
      })).indexOf(chamada.cod_aplicacao) >= 0) {
      tabela = "CallLogNVP_";
    }

    if (chamada.cod_aplicacao === "MENUOI") {
      tabela = "CallLogM4U_";
    }

    if (chamada.cod_aplicacao.match('TELAUNICA')) {
      tabela = "CallLogTelaUnica_";
    }
	
	if (chamada.cod_aplicacao.match('S2S')) {
      tabela = "CruzamentoS2S";
    }
    
    $scope.callLog = '';
    $scope.$apply();
      
    var verificaLogs = "sp_spaceused " + tabela;
    if(tabela != "CruzamentoS2S"){
       verificaLogs+= chamada.data_hora.substr(5, 2);
    }
    mssqlQueryTedious(verificaLogs, function (err, retorno) {
      if (err) console.log(err)
  
      if(retorno[0][1].value == 0) {
        var mesString = formataDataMesString(chamada.data_hora);
        $scope.callLog = 'Não há dados de log original de ' + mesString + ' no banco de dados.'
        $scope.$apply();
      } else {
        if(tabela === "CruzamentoS2S") {
                var stmtCallLog = "SELECT TOP 1 conteudoFinal AS CONTEUDOORIGINAL " 
                + " FROM CruzamentoS2S ";
				
				if(chamada.ctxid.trim() !== ""){
					stmtCallLog += " WHERE CodUCIDORIGINAL = '" + chamada.ctxid + "'";                
				}else{
					stmtCallLog += " WHERE CodUCID = '" + chamada.UCID + "'";   
				}
        }else{
                var stmtCallLog = "SELECT TOP 1 Comprimido AS ARQUIVO "
                + "FROM "+ tabela + ""+ chamada.data_hora.substr(5, 2)
                + " WHERE CodUCID = '" + chamada.UCID + "'"
                + "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'" 
                + " ORDER BY LEN(CodUCID)";
        }
        log(stmtCallLog);

        $scope.chamadaCallLogData = chamada.data;
        $scope.tabelaCallLog = tabela;
        $scope.downloadLog = "";
		
		mssqlQueryTedious(stmtCallLog, function (erro, result) {
			var arquivoB;
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					arquivoB = result[i][0].value;
				}
				if (tabela === "CruzamentoS2S") {
					$scope.$apply(function() {	
						$scope.callLog = arquivoB;
						$scope.logValidado = validaLogGeral($scope.callLog);
						$scope.callLog = mascaraInformacoes($scope.callLog);
						$('#myModalLabel').text("Log da chamada");
						search(".log-original");
					});
					
				}else{
					var baseFileZip = chamada.UCID;
					var buffZip = toBuffer(toArrayBuffer(arquivoB));								
					fs.writeFileSync(baseFileZip +"_temp", buffZip, 'binary');	
					require('zlib').unzip(new Buffer(fs.readFileSync(baseFileZip +"_temp")), function(err, buf) {     
						if (err) console.log(err);
						$scope.$apply(function() {	
							$scope.callLog = buf.toString();
							$scope.logValidado = validaLogGeral($scope.callLog);
							$scope.callLog = mascaraInformacoes($scope.callLog);							
							$('#myModalLabel').text("Log da chamada");
							search(".log-original");
						});								
						try{fs.unlinkSync(baseFileZip +"_temp");}catch(ex){}
					});
				}						
			}
		});	
	
      }
    }, function (err, num_rows){
      console.log("Retornei :) "+num_rows);
      if(err) console.log(err);
    });

    return true;
  };
	
	$scope.infos = [];
	
	// Consulta e formata o XML da chamada
	$scope.formataXML = function (venda, check) {
		
		$('#myModalLabel').text("Log da chamada");
		
		delete cache.aplicacoes[venda.cod_aplicacao].prompts;		

		// Se a chamada acabou de ser consultada, não há nada a fazer
		if (check === undefined) {
			if (venda === $scope.venda_atual && logCarregado) return true;
		}
		
		// Limpar o log original da chamada consultada anteriormente
		$scope.venda_atual = venda;
		$scope.callLog = "";
		$scope.ocultar.log_original = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_formatado = false;
		$scope.logxml = "";

		// Buscar o log em XML
		var stmt = "SELECT XMLIVR,PREROTEAMENTO,PREROTEAMENTORET,RECONHECIMENTOVOZ FROM IVRCDR WHERE CodUCIDIVR = '" + venda.UCID + "'";
		log(stmt);

		var xml = "",
		routing = "",
		routingRet = "",
		recVoz = "";
		
		
		mssqlQueryTedious(stmt, function (erro, result) {			
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){			
					xml = result[i]["XMLIVR"].value;
					routing = result[i]["PREROTEAMENTO"].value;
					routingRet = result[i]["PREROTEAMENTORET"].value;
					parseXMLPrompts(result[i]["XMLIVR"].value, venda);					
					recVoz = result[i]["RECONHECIMENTOVOZ"].value;
				}
			}
		});
		
		var teste2 = setInterval(function () {
			
			if (cache.aplicacoes[venda.cod_aplicacao].hasOwnProperty('prompts') && 
				cache.aplicacoes[venda.cod_aplicacao].hasOwnProperty('itens_controle') && 
				cache.aplicacoes[venda.cod_aplicacao].itens_controle['indice'] !== undefined){
					clearInterval(teste2);
					$('div.modal-header h3').text("Log da chamada");
					$scope.$apply(function () {
						
						// Formatar o log em XML			
						if ($scope.estadoEnx === true) {
							$scope.logxml = parseXML(RemoverEDsExcedentes(xml), venda, $scope.itemEnx, $scope.itemEnxAll);
						}else{
							$scope.logxml = parseXML(xml, venda, $scope.itemEnx, $scope.itemEnxAll);
						}	
				
			  });
			} else {
				$('div.modal-header h3').text("Carregando...");
			}
		}, 500);
	return true;
    };
	
	//Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		
		var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var colunas = [];
        for (i = 0; i < $scope.colunas.length; i++) {
			if($scope.colunas[i].field !== "ucid") colunas.push($scope.colunas[i].displayName);
        }

        // var colunasFinal = [colunas];

        console.log(colunas);

        var dadosExcel = [colunas].concat($scope.csv);

        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

        var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'tvendas_' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);
		
		setTimeout(function() {
            $btn_exportar.button('reset');
        }, 500);		
	}
	
	
  //{* Bloco Responsável pela pesquisa */
	
	function clearSearchTool(searchElement, resultElement){
		
	  // Função responsável por resetar a função de pesquisa
	  //console.log("Limpou a ferramenta de pesquisa de logs");
	  
	  if(resultElement==""||resultElement==null){
		  resultElement = $('.search_results');
	  }
	  $(resultElement).html('');
	  $(searchElement).val('');
	  $(searchElement).attr("placeholder", "Buscar...");
	  $("div .log-original").unhighlight({className: 'match'});
	  $("div .log-formatado").unhighlight({className: 'match'});
	  $("div .log-prerouting").unhighlight({className: 'match'});
	  $("div .log-preroutingret").unhighlight({className: 'match'});
	};
	
	function search(selector){
		var resultElement = $('.search_results');
		var searchElement = $(".searchtool_text");
		clearSearchTool("", resultElement);
		
		// console.log("Search!!!!!!");
		
		if(selector==""||!selector){
		  if( $("div.log-formatado").css('display') == 'block') {
			selector = "div.log-formatado";
		  }
		  if( $("div.log-original").css('display') == 'block') {
			selector = "div.log-original";
		  }
		  if( $("div.log-prerouting").css('display') == 'block') {
			selector = "div.log-prerouting";
		  }
		  if( $("div.log-preroutingret").css('display') == 'block') {
			selector = "div.log-preroutingret";
		  }
		}
		
		if(searchElement.val()!=""&&searchElement.val()!=undefined&&searchElement.val()!=null){
			try{				
				$(selector).highlight(searchElement.val(), {className: 'match'});
				$('.match:first').addClass('highlighted');
				var matches = $(".match");
				resultElement.text(matches.length+' encontrados');
				
			} catch(err){
				console.log("Error: "+err);
			}
		}
	}
  $scope.$watch('callLog', function(newValue, oldValue) {
	  setTimeout(function(){
					search(".log-original");
		}, 1000);
	});	
	$scope.$watch('ocultar.log_formatado', function(newValue, oldValue) {
		if(newValue==false){
			search(".log-formatado");
		}
	});	
		
	$scope.$watch('ocultar.log_original', function(newValue, oldValue) {
			if(newValue==false){
				search(".log-original");
			}
	});
	
  //}* Fim do bloco responsável pelas tarefas de pesquisa */
  	
}
CtrlVendas.$inject = ['$scope','$globals'];
