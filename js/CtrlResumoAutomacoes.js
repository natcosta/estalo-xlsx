function CtrlResumoAutomacoes($scope, $globals) {


    var idView = "pag-resumo-automacoes";
    var titulo = "Resumo de automações";

    win.title = titulo;

    $scope.versao = versao;
    $scope.rotas = rotas;
    $scope.exibirIC = false;
    $scope.category = [];
    travaBotaoFiltro(0, $scope, "#" + idView, titulo);
    $scope.colIC = false;

    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.colunas = [
      { field: "descricao", displayName: "Descrição", width: 350, pinnedLeft: true, category: " " },
      { field: "resultado", displayName: "Resultado", width: 100, pinnedLeft: true, category: " " }
    ];
    $scope.gridDados = {
        data: $scope.dados,
        columnDefs: $scope.dados,
        headerTemplate: base + 'header-template.html',
        category: $scope.category,
        enableRowHeaderSelection: false,
        enableSelectAll: true,
        multiSelect: true,
        enableRowSelection: true,
        enableFullRowSelection: true,
        modifierKeysToMultiSelect: true
    };

    $scope.datas = [];

    var resultados = ["Sucesso", "Insucesso"];

    //$scope.iit = false;

    // Filtros: data e hora
    var agora = new Date();
    var inicio = Estalo.filtros.filtro_data_hora[0] || ontemInicio();
    var fim = Estalo.filtros.filtro_data_hora[1] || ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#" + idView);

        $view.find(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        componenteDataMaisHora($scope, $view);

        // Carregar filtros
        carregaAplicacoes($view, false, false, $scope);
        carregaSites($view);
        carregaSegmentosPorAplicacao($scope, $view, true);

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view);
        });

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} sites/POPs',
            showSubtext: true
        });

        $view.on("change", "select.filtro-site", function () {
            Estalo.filtros.filtro_sites = $(this).val() || [];
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        // Marca todas as aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($view.find('.filtro-aplicacao'), $view, $scope); });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($view.find('.filtro-site'), 'sites'); });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($view.find('.filtro-segmento'), 'segmentos'); });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
            $view.find("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $view.find("div.datahora-fim.input-append.date").data("datetimepicker").hide();
            $view.find('div.btn-group.filtro-aplicacao').addClass('open');
            $view.find('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseout(function () {
            $view.find('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseover(function () {
            $view.find('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseout(function () {
            $view.find('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-segmento').mouseover(function () {
            if (!$view.find('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $view.find('div.btn-group.filtro-segmento').addClass('open');
                $view.find('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-segmento').mouseout(function () {
            $view.find('div.btn-group.filtro-segmento').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#" + idView);

            // Testar se a data inicial é maior que a data final
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            // Testar se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
            }, 500);
        };

        // Botão abortar
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        $scope.abortar = function () {
            abortar($scope, "#" + idView);
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    $scope.listaDados = function () {
        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        $scope.gridDados.data.length = 0;

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

        // Filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if ($scope.exibirIC) {//&&!$scope.colIC

            //$scope.colIC = true;
            //console.log("Nova Coluna Adicionada.");
            $scope.colunas = $scope.colunas.slice(0, 2);
            $scope.colunas.push({ field: "codIC", displayName: "Código IC", width: 100, pinnedLeft: true, category: " " });
        } else {
            $scope.colunas = $scope.colunas.slice(0, 2);
        }
        $scope.dados = [];
        $scope.datas = [];
        $scope.category = [];

        var qtd = "Qtd - QtdIIT - QtdTLV"; // $scope.iit ? "Qtd" : "Qtd - QtdIIT";
        var stmt = db.use
		  + " WITH a AS"
		  + " (SELECT Descricao,"
          + " DataHora,"
          + " ic.CodAutomacao AS CodAutomacao,"
          + " SUM(CASE Sucesso"
                  + " WHEN 1 THEN " + qtd
                  + " ELSE 0"
              + " END) AS QtdSucessos"
	   + " FROM ICAutomacaoSucessoFalha ic"
	   + " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao"
	   + " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao"
	   + " AND ic.CodIC = c.Cod_ItemDeControle"
	   + " WHERE DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "'"
	      + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
          + restringe_consulta("Cod_Site", filtro_sites, true)
          + restringe_consulta("CodSegmento", filtro_segmentos, true)
   + " GROUP BY Descricao,"
            + " DataHora,"
            + " ic.CodAutomacao"
   + " UNION SELECT Descricao,"
                + " DataHora,"
                + " ic.CodAutomacao AS CodAutomacao,"
                + " 0 AS QtdSucessos"
   + " FROM ICAutomacaoSucessoFalha ic"
   + " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao"
   + " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao"
   + " WHERE DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "'"
     + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
     + " AND Sucesso = 1"
     + " AND CodIC NOT IN"
       + " (SELECT cod_itemDecontrole"
        + " FROM consolidaitemcontroled"
        + " WHERE 1 = 1"
          + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
          + " AND DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "')"
   + " GROUP BY Descricao,"
            + " DataHora,"
            + " ic.CodAutomacao),"
     + " b AS"
  + " (SELECT Descricao,"
          + " DataHora,"
          + " ic.CodAutomacao AS CodAutomacao,"
          + " SUM(CASE Sucesso"
                  + " WHEN 0 THEN " + qtd 
                  + " ELSE 0"
              + " END) AS QtdInsucessos"
   + " FROM ICAutomacaoSucessoFalha ic"
   + " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao"
   + " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao"
   + " AND ic.CodIC = c.Cod_ItemDeControle"
   + " WHERE DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "'"
     + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
   + " GROUP BY Descricao,"
            + " DataHora,"
            + " ic.CodAutomacao"
   + " UNION SELECT Descricao,"
                + " DataHora,"
                + " ic.CodAutomacao AS CodAutomacao,"
                + " 0 AS QtdInsucessos"
   + " FROM ICAutomacaoSucessoFalha ic"
   + " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao"
   + " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao"
   + " WHERE DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "'"
     + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
     + " AND Sucesso = 0"
     + " AND CodIC NOT IN"
       + " (SELECT cod_itemDecontrole"
        + " FROM consolidaitemcontroled"
        + " WHERE 1 = 1"
          + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
          + " AND DataHora >= '" + formataDataHora(data_ini) + "' AND DataHora < '" + formataDataHora(data_fim) + "')"
   + " GROUP BY Descricao,"
            + " DataHora,"
            + " ic.CodAutomacao)"
+ " SELECT a.descricao AS Descricao,"
       + " isnull(a.DataHora, b.DataHora) AS DataHora,"
       + " a.qtdsucessos AS QtdSucessos,"
       + " b.qtdinsucessos AS QtdInsucessos"
+ " FROM a"
+ " LEFT JOIN b ON a.codAutomacao = b.codAutomacao"
+ " ORDER BY Descricao,"
         + " DataHora"

        if ($scope.exibirIC) {
          stmt = "WITH a AS " +
            " (SELECT Descricao, " +
            " CodIC AS icsuc, ic.CodAutomacao as CodAutomacao   " +
            " FROM ICAutomacaoSucessoFalha ic " +
            " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao " +
            " AND SUCESSO = 1 " +
            restringe_consulta("CodSegmento", filtro_segmentos, true) +
            restringe_consulta("Cod_Site", filtro_sites, true) +
            restringe_consulta("CodAplicacao", filtro_aplicacoes, true) +
            " GROUP BY Descricao, " +
            " sucesso," +
            " codIC, ic.CodAutomacao), " +
            " b AS " +
            " (SELECT dataHora, " +
            " isnull(SUM(Qtd - QtdIIT - QtdTLV), 0) AS QtdSucessos, " +
            " CodIC AS icsuc" +
            " FROM ICAutomacaoSucessoFalha ic " +
            " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao " +
            " AND ic.CodIC = c.Cod_ItemDeControle " +
            " WHERE DataHora >= '" + formataDataHora(data_ini) + "'" +
            " AND DataHora < '" + formataDataHora(data_fim) + "'" +
            " AND SUCESSO = 1 " +
            restringe_consulta("ic.CodAplicacao", filtro_aplicacoes, true) +
            restringe_consulta("Cod_Site", filtro_sites, true) +
            restringe_consulta("CodSegmento", filtro_segmentos, true) +
            " GROUP BY DataHora, " +
            " sucesso, " +
            " codIC " +
            " UNION SELECT '" + formataDataHora(data_ini) + "' AS datahora, " +
            " 0 AS qtdsucessos, " +
            " CodIC " +
            " FROM ICAutomacaoSucessoFalha " +
            " WHERE 1 = 1 " +
            restringe_consulta("CodAplicacao", filtro_aplicacoes, true) +
            " AND CodIC NOT IN " +
            " (SELECT cod_itemDecontrole " +
            " FROM consolidaitemcontroled " +
            " WHERE 1 = 1 " +
            restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true) +
            " AND datahora = '" + formataDataHora(data_ini) + "') ), " +
            " c AS " +
            " (SELECT Descricao, " +
            " CodIC AS icinsuc, ic.CodAutomacao as CodAutomacao " +
            " FROM ICAutomacaoSucessoFalha ic " +
            " JOIN AutomacaoChamada a ON ic.CodAutomacao = a.CodAutomacao " +
            " AND SUCESSO = 0 " +
            restringe_consulta("ic.CodAplicacao", filtro_aplicacoes, true) +
            restringe_consulta("ic.Cod_Site", filtro_sites, true) +
            restringe_consulta("ic.CodSegmento", filtro_segmentos, true) +
            " GROUP BY Descricao, " +
            " sucesso, " +
            " codIC, ic.CodAutomacao), " +
            " d AS " +
            " (SELECT dataHora, " +
            " isnull(SUM(Qtd - QtdIIT - QtdTLV), 0) AS QtdInsucessos, " +
            " CodIC AS icinsuc " +
            " FROM ICAutomacaoSucessoFalha ic " +
            " JOIN ConsolidaItemControleD c ON ic.CodAplicacao = c.Cod_Aplicacao " +
            " AND ic.CodIC = c.Cod_ItemDeControle " +
            " WHERE 1 = 1 " +
            " AND DataHora >= '" + formataDataHora(data_ini) + "' " +
            " AND DataHora < '" + formataDataHora(data_fim) + "' " +
            " AND SUCESSO = 0 " +
            restringe_consulta("ic.CodAplicacao", filtro_aplicacoes, true) +
            restringe_consulta("Cod_Site", filtro_sites, true) +
            restringe_consulta("CodSegmento", filtro_segmentos, true) +
            " GROUP BY DataHora, " +
            " sucesso, " +
            " codIC " +
            " UNION SELECT '" + formataDataHora(data_ini) + "' as datahora, " +
            " 0 AS qtdinsucessos, " +
            " CodIC " +
            " FROM ICAutomacaoSucessoFalha " +
            " WHERE 1 = 1 " +
            restringe_consulta("CodAplicacao", filtro_aplicacoes, true) +
            " AND CodIC NOT IN " +
            " (SELECT cod_itemDecontrole " +
            " FROM consolidaitemcontroled " +
            " WHERE 1 = 1 " +
            restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true) +
            " AND datahora = '" + formataDataHora(data_ini) + "') ) " +
            " SELECT a.Descricao, " +
            " isnull(b.DataHora, d.Datahora) AS DataHora, " +
            " b.QtdSucessos, " +
            " d.QtdInsucessos, " +
            " a.icsuc, " +
            " c.icinsuc " +
            " FROM a " +
            " LEFT OUTER JOIN b ON a.icsuc = b.icsuc " +
            " LEFT OUTER JOIN c ON a.Descricao = c.Descricao AND a.CodAutomacao = c.CodAutomacao " +
            " LEFT OUTER JOIN d ON c.icinsuc = d.icinsuc " +
            " AND b.DataHora = d.Datahora "
    }

        console.log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);
        var stmts = arrayDiasQuery(stmt, new Date(formataDataHora(data_ini)), new Date(formataDataHora(data_fim)));
        //stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/Select \'.+\' /g,"Select '"  +formataDataHora(new Date(data_fim))+"\' ")];
        stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/DataHora = \'.+\' /g,"datahora = '"  +formataDataHora(new Date(data_fim))+"\' ")];
        
        var controle = 0;
        var total = 0;

        function formataData2(data) {
            var y = data.getFullYear(),
                m = data.getMonth() + 1,
                d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function processaRegistro(columns) {
            var descricao = columns[0].value;
            if (columns[1].value) {
                data = "_" + formataData2(columns[1].value),
                dataBR = formataDataBR(columns[1].value);
            } else {
                data = "_" + formataData2(data_ini),
                    dataBR = formataDataBR(data_ini);
            }
            qtdSucessos = +columns[2].value,
            qtdInsucessos = +columns[3].value;
            if ($scope.exibirIC) {
                codigoIC = columns[4].value || "";
                codigoIC2 = columns[5].value || "";
            }

            var d = $scope.datas[data];
            if (d === undefined) {
                d = { data: data, dataBR: dataBR };
                $scope.datas.push(d);
                $scope.datas[data] = d;
            }

            var impar = true;
            [{ descricao: descricao }, { descricao: "TOTAL", aguardar: true }].forEach(function (o) {
                resultados.forEach(function (resultado) {
                    var chaveIndice = o.descricao + "-x-" + resultado;
                    var dado = $scope.dados[chaveIndice];
                    var qtd = 0;
                    if (dado === undefined) {
                        var codIC;
                        if ($scope.exibirIC) {
                            codIC = resultado.match(/^[Ss]ucesso$/) ? codigoIC : codigoIC2;
                        }
                        dado = criaLinhaGrid(o.descricao, resultado, "", codIC);
                        if (!o.aguardar) {
                            $scope.dados.push(dado);
                        }
                        $scope.dados[chaveIndice] = dado;
                    }

                    qtd = resultado === "Sucesso" ? qtdSucessos : qtdInsucessos;
                    if (qtd) {
                        preencheLinhaGrid(dado, data, qtd);
                    }
                });
            });
        }
        
         function proximaHora(stmt){  
   
           function comOuSemDados(err,c){
            $('.notification .ng-binding').text("Aguarde... "+atualizaProgressExtrator(controle,stmts.length)+ " Encontrados: "+total).selectpicker('refresh');
            controle++;
            //$scope.$apply();

            //Executa dbquery enquanto array de querys menor que length-1
            if(controle < stmts.length - 1){
             proximaHora(stmts[controle].toString());
            }
            
            //Evento fim
            if(controle === stmts.length - 1){
             if(c === undefined) console.log("fim " + controle);
             if(err){
              retornaStatusQuery(undefined, $scope);
             }else{
              //retornaStatusQuery($scope.dados.length, $scope);
              
                    resultados.forEach(function (resultado) {
                        $scope.dados.push($scope.dados["TOTAL" + "-x-" + resultado]);
                    });

                    // Calcular percentuais e preencher com zero valores inexistentes
                    $scope.dados.forEach(function (dado) {
                        var chaveComplemento = dado.descricao + "-x-" + (dado.resultado === "Sucesso" ? "Insucesso" : "Sucesso");
                        var complemento = $scope.dados[chaveComplemento];
                        $scope.datas.forEach(function (d) {
                            var t = dado.totais[d.data];
                            if (t === undefined) {
                                t = criaGrupoColunasGrid();
                                dado.totais[d.data] = t;
                            }
                            var soma = t.qtd + (complemento.totais[d.data] ? complemento.totais[d.data].qtd : 0);
                            calculaPercentual(t, soma);
                        });
                        var soma = dado.totais.geral.qtd + (complemento.totais.geral ? complemento.totais.geral.qtd : 0);
                        calculaPercentual(dado.totais.geral, soma);
                    });

                    // Ordenar e incluir colunas
                    $scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
                    $scope.datas.forEach(function (d) {
                        $scope.category.push({ name: d.dataBR, visible: true });
                        $scope.colunas.push({ name: "totais." + d.data + ".qtd", displayName: "Qtd", width: 100, category: d.dataBR, enablePinning: false, cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' });
                        $scope.colunas.push({ name: "totais." + d.data + ".percent", displayName: "%", width: 100, category: d.dataBR, enablePinning: false, cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' });
                    });
                    $scope.colunas.push({ name: "totais.geral.qtd", displayName: "Qtd", width: 100, category: "TOTAL", enablePinning: false, cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' });
                    $scope.colunas.push({ name: "totais.geral.percent", displayName: "%", width: 100, category: "TOTAL", enablePinning: false, cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' });

                    $($('.ngHeaderContainer')[0]).css('height', '30px');
                    $scope.category.push({ name: "TOTAL", visible: true });
                    $scope.gridDados.category = $scope.category;
                    $scope.gridDados.category.unshift({ name: " ", visible: true });
                    $scope.gridDados.data = $scope.dados;
                    $scope.gridDados.columnDefs = $scope.colunas;
                    console.log($scope.gridDados);
                    $scope.$apply();
                    $btn_gerar.button('reset');
                    $btn_exportar.prop("disabled", false);
                  
                    retornaStatusQuery($scope.dados.length, $scope);
              }
             }
             
             $btn_gerar.button('reset');
             if($scope.dados.length > 0){
              $btn_exportar.prop("disabled", false);
             }else{
              $btn_exportar.prop("disabled", "disabled");
             }
            
           }
          db.query(stmt, processaRegistro, function (err, num_rows) {
              /*userLog(stmt, 'Carrega dados', 2, err)
              console.log(stmt);
              console.log(num_rows + " registros");
              retornaStatusQuery(num_rows, $scope);*/
             console.log("Executando query-> " + stmt + " " + num_rows);             
             num_rows !== undefined ? total += num_rows : total += 0;
             console.log(num_rows + " registros");
             comOuSemDados(err);
              $btn_gerar.button('reset');
              if (num_rows > 0) {//Removido de baixo
                    // Incluir linhas de totais
                } else {
                    $($('.ngHeaderContainer')[0]).css('height', '0px');
                }   
          });
        }
        
        proximaHora(stmts[0].toString());
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho1 = $scope.datas.map(function (d) {
            return { value: d.dataBR, bold: 1, colSpan: 2, hAlign: "center" };
        });

        // Inserir colunas chave: descrição e resultado
        if ($scope.exibirIC) {
            cabecalho1.unshift({ value: "Código IC", bold: 1, rowSpan: 2, hAlign: "center" });
        }
        cabecalho1.unshift({ value: "Resultado", bold: 1, rowSpan: 2, hAlign: "center" });
        cabecalho1.unshift({ value: "Descrição", bold: 1, rowSpan: 2, hAlign: "center" });

        // Inserir colunas de total
        cabecalho1.push({ value: "Total", bold: 1, colSpan: 2, hAlign: "center" });

        var cabecalho2 = $scope.datas.map(function (d) {
            return [{ value: "Qtd", bold: 1, hAlign: "center" }, { value: "%", bold: 1, hAlign: "center" }];
        });

        // Inserir colunas de total
        cabecalho2.push([{ value: "Qtd", bold: 1, hAlign: "center" }, { value: "%", bold: 1, hAlign: "center" }]);

        cabecalho2 = flatten(cabecalho2);

        var linhas = $scope.dados.map(function (dado) {
            var linha = $scope.datas.map(function (d) {
                var t = dado.totais[d.data];
                return [
                  { value: t.qtd || 0, autoWidth: true, hAlign: "right" },
                  { value: t.percent || 0, autoWidth: true, hAlign: "right" }
                ];
            });

            // Inserir colunas chave: descrição e resultado
            if ($scope.exibirIC) {
                linha.unshift([{ value: dado.descricao, autoWidth: true }, { value: dado.resultado, autoWidth: true }, { value: dado.codIC, autoWidth: true }]);
            } else {
                linha.unshift([{ value: dado.descricao, autoWidth: true }, { value: dado.resultado, autoWidth: true }]);
            }

            // Inserir última coluna: total por linha
            linha.push([
              { value: dado.totais.geral.qtd || 0, autoWidth: true, hAlign: "right" },
              { value: dado.totais.geral.percent || 0, autoWidth: true, hAlign: "right" }
            ]);

            return flatten(linha);
        });

        // Inserir cabeçalho
        linhas.unshift(cabecalho2);
        linhas.unshift(cabecalho1);

        console.log(linhas);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'Resumo de automações', data: linhas, table: false }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 2 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');

        var file = 'ResumoAutomacoes_' + formataDataHoraMilis(new Date()) + '.xlsx';
        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

    function criaLinhaGrid(descricao, resultado, impar, codigo) {
        if (descricao == "TOTAL") {
            return { descricao: descricao, resultado: resultado, codIC: "TOTAL", totais: { geral: criaGrupoColunasGrid() }, impar: impar, par: !impar };
        }
        return { descricao: descricao, resultado: resultado, codIC: codigo, totais: { geral: criaGrupoColunasGrid() }, impar: impar, par: !impar };
    }

    function preencheLinhaGrid(linha, col, qtd) {
        if (linha.totais[col] === undefined) {
            linha.totais[col] = criaGrupoColunasGrid();
        }
        incrementaGrupoColunasGrid(linha.totais[col], qtd);
        incrementaGrupoColunasGrid(linha.totais.geral, qtd);
    }

    function criaGrupoColunasGrid(qtd) {
        return { qtd: qtd || 0 };
    }

    function incrementaGrupoColunasGrid(t, qtd) {
        t.qtd += qtd;
    }

    function calculaPercentual(t, soma) {
        t.percent = soma === 0 ? 0 : (100.0 * t.qtd / soma).toFixed(2);
    }

    function flatten(a) {
        var ret = [];
        a.forEach(function (b) {
            b.forEach(function (x) {
                ret.push(x);
            });
        });
        return ret;
    }
}
CtrlResumoAutomacoes.$inject = ['$scope', '$globals'];
