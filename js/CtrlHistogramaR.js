/**
 * Histograma (de (R)epetidas) Controller
 */

function CtrlHistogramaR($scope, $globals) {
  win.title = "Histograma de Repetidas";
  $scope.versao = versao;
  $scope.rotas = rotas;
  var stmt = "";
  var resultadoquery = [];
  var data = new Date();


  $scope.localeGridText = {
      contains: 'Contém',
      notContains: 'Não contém',
      equals: 'Igual',
      notEquals: 'Diferente',
      startsWith: 'Começa com',
      endsWith: 'Termina com'
  }
  
  var resetCanvas = function(){
    $('#myChart').remove(); // this is my <canvas> element
    $('#canvas-container').append('<canvas id="myChart"><canvas>');
    canvas = document.getElementById('myChart');
    ctx = canvas.getContext('2d');
    ctx.canvas.width = $('#graph').width(); // resize to parent width
    ctx.canvas.height = $('#graph').height(); // resize to parent height
    var x = canvas.width/2;
    var y = canvas.height/2;
    ctx.font = '10pt Verdana';
    ctx.textAlign = 'center';
    ctx.fillText('This text is centered on the canvas', x, y);
  };
  
  var agora = new Date();

  var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
  var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

  $scope.periodo = {
      inicio: inicio,
      fim: fim,
      min: new Date(2013, 11, 1),
      max: agora // FIXME: atualizar ao virar o dia
  };

  $scope.linhaAtual = {}

  $scope.filtros = {
      consolFinalizados: false,
      diferencaDia: false
  }

  $scope.btnCriarInstancia = '';

  $scope.permCriarInstancia = false;
  var p = cache.aclbotoes.map(function (b) {
      return b.idview;
  }).indexOf('pag-histograma-repetidas');
  if (p >= 0) {
      var teste = cache.aclbotoes[p].permissoes.split(',');
      for (var i = 0; i < teste.length; i++) {
          if (teste[i] === 'instancia-consol') {
              $scope.permCriarInstancia = true;
          }
      }
  }

  var $view;

  $scope.dados = [];
  $scope.csv = [];
  $scope.difPorAplicacao = [];
  $scope.difAplicacoes = [];

  $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-histograma-repetidas");

      $(".botoes").css({
          'position': 'fixed',
          'left': 'auto',
          'right': '25px',
          'margin-top': '35px'
      });


      componenteDataMesSO ($scope,$view,true);
      carregaAplicacoes($view, false, false, $scope);

      carregaSegmentosPorAplicacao($scope, $view, true);

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function () {
        carregaSegmentosPorAplicacao($scope, $view)
      });

      $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-histograma-repetidas");

          $scope.listaAtrasados.apply(this);
      });

      $view.on("click", ".btn-exportar", function () {

        $scope.exportaXLSX.apply(this);
       
      });

      // Botão abortar Alex 23/05/2014
      $view.on("click", ".abortar", function () {
          $scope.abortar.apply(this);
      });

      //Alex 23/05/2014
      $scope.abortar = function () {
          abortar($scope, "#pag-histograma-repetidas");
      }
  });

  $scope.listaAtrasados = function () {
      
    var $btn_exportar = $view.find(".btn-exportar");
      $btn_exportar.prop("disabled", true);
      $btn_exportar.prop("disabled", false);

      var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
      var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

      console.log("FiltroAplicacoes:" + filtro_aplicacoes);

      var $btn_gerar = $(this);
      $btn_gerar.button("loading");

     

      data = $scope.periodo.inicio;
      var ano = parseInt(data.getFullYear())
      var mes = parseInt(function () {
        var month = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : data.getMonth() + 1;
        return month >= 0 && month < 10 ? '0' + month : month;
        }())
      //console.log("data ano: "+ ano+" mês: "+mes);

      

      stmt = "EXECUTE uspFibraPorChamador @ano = "+ano+", @aplicacao = '"+filtro_aplicacoes+"', @mes = "+mes+", @segmento = '"+filtro_segmentos+"'"
     
      mssqlQueryTedious3(stmt, function (err, result){
        if(err){
          console.log(err)
        }else{
          //console.log(result);
          console.log("tamanho array: " , result.length)
          var repetidas = [];
          var clientes = [];
          var pareto = [];
          resultadoquery = result;
          //console.log(linha.length);
         // console.log("linha "+ result[0][0].value);
          for (var i = 0; i < result.length ; i++){
            repetidas.push(result[i][0].value);
            clientes.push(result[i][1].value);
            pareto.push((parseInt(result[i][2].value.replace('%','')))/100);
          }
        };
        //console.log(pareto);
      
        retornaStatusQuery(result.length, $scope);
        $btn_gerar.button("reset");

        $btn_exportar.prop("disabled", false);

        resetCanvas();
        ctx = document.getElementById('myChart').getContext('2d');
        myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: repetidas,
            datasets: [
              {
                type: 'bar',
                label: 'Clientes',
                data: clientes,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                yAxisID: 'client',
              },
              {
                type: 'line',
                label: '% Pareto',
                data: pareto,
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 2,
                yAxisID: 'pareto',
              }
            ]
          },
          options: {
            scales: {
              yAxes: [{
                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                id: 'client',
              }, {
                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                id: 'pareto',
                // grid line settings
                gridLines: {
                  drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
                ticks: {
                  callback: function(value, index, values) {
                    return (Math.round(value * 100)) + '%';
                  }
                }
              }]
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  if (tooltipItem.datasetIndex === 1) {
                    return (Math.round(tooltipItem.yLabel * 100)) + '%';
                  } else {
                    return tooltipItem.yLabel;
                  }
                }
              }
            },
            responsive:true,
            maintainAspectRatio: false,
          },
        }); 
      }
      );
      //console.log(stmt);
      
  }

  $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');
      //console.log(resultadoquery);
      var colunas = [];
      var linhas = [];
      var mes = "";
      var linha = [];
      linha = ["Repetições","Clientes","% Pareto", "% Representação"];


      switch(data.getMonth()) { 
        case 0:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
            return i >= 0 && i < 10 ? '0' + i : i ;
            }()+"/01")
            }
            mes = "Janeiro";   
        break; 
        };
        case 1:{
          for (var i = 1; i < 29; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/02")
          }
          mes = "Fevereiro";
        break; 
        };
        case 2:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/03")
          }
          mes = "Março"
           break; 
        };
        case 3:
        { 
          for (var i = 1; i < 31; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/04")
          }
          mes = "Abril"
           break; 
        };
        case 4:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/05")
          }
          mes = "Maio"
           break; 
        };
        case 5:
        { 
          for (var i = 1; i < 31; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/06")
          }
          mes = "Junho"
           break; 
        };
        case 6:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/07")
          }
          mes = "Julho"
           break; 
        };
        case 7:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/08")
          }
          mes = "Agosto"
           break; 
        };
        case 8:
        { 
          for (var i = 1; i < 31; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/09")
          }
          mes = "Setembro"
           break; 
        };
        case 9:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/10")
          }
          mes = "Outubro"
           break; 
        };
        case 10:
        { 
          for (var i = 1; i < 31; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/11")
          }
          mes = "Novembro"
           break; 
        };
        case 11:
        { 
          for (var i = 1; i < 32; i++){
            linha.push(function () {
              return i >= 0 && i < 10 ? '0' + i : i ;
              }()+"/12")
          }
          mes = "Dezembro"
           break; 
        };
        default: { 
               
           break; 
        } 
     }
     linhas.push(linha);
     linha = [];
     for(var i = 0; i < resultadoquery.length; i++){
      for (var x = 0; x < resultadoquery[i].length; x++){
        linha.push(resultadoquery[i][x].value);
      }
      linhas.push(linha);
      linha = [];
     }
     //colunas.push(linhas);
     //console.log(linhas);
      // for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
      //     colunas.push($scope.gridDados.columnDefs[i].headerName);
      // }

      // colunas.pop();
      
      //var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [
          ['Mês de consulta: ' + mes]
      ].concat(linhas);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'histograma_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);

      setTimeout(function () {
          $btn_exportar.button('reset');
      }, 500);
  };

  function reverteDataBR(dataBR) {
      return dataBR.replace(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}:\d{2})/, '$3-$2-$1 $4');
  }

  function formataHoraZero(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 00:00:00');
  }

  function formataHoraZeroMenosUm(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 23:59:59');
  }

  function formataHoraCinco(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 05:00:00');
  }

  function formataHoraCheia(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2} \d{2}).+/g, '$1:00:00');
  }
}

CtrlHistogramaR.$inject = ['$scope', '$globals'];
