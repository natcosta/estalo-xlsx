function CtrlResumoUraxECH($scope, $globals) {

    win.title="Resumo URA x ECH"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-UraxECH", "Resumo URA x ECH");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
	$scope.vendas = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;
	$scope.total_geral;
	$scope.total_der;
	$scope.total_transf;
	$scope.total_naoatend;
    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        porDia: false
    };

  
	
    var empresas = cache.empresas.map(function(a){return a.codigo});

    $scope.aba = 2;



    $scope.valor = "datReferencia";



    function geraColunas(){
      var array = [];


     array.push(
		{ field: "datReferencia", displayName: "Data", width: 150, pinned: true },
        { field: "entrantes", displayName: "Entrantes URA", width: 130, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "derivadas", displayName: "Derivadas URA", width: 130, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "operacao1", displayName: "Qtd Operação 1", width: 130, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
		{ field: "operacao2", displayName: "Qtd Operação 2", width: 130, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "percOper1", displayName: "% Operação 1", width: 130, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "percOper2", displayName: "% Operação 2", width: 130, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' }
		);

      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


	  $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-resumo-UraxECH");
      treeView('#pag-resumo-UraxECH #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-UraxECH #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-UraxECH #ed', 65,'ED','dvED');
      treeView('#pag-resumo-UraxECH #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-UraxECH #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-UraxECH #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-UraxECH #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-UraxECH #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-UraxECH #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-UraxECH #admin', 255,'Administrativo','dvAdmin');
	  treeView('#pag-resumo-UraxECH #monitoracao', 275, 'Monitoração', 'dvReparo');
	  treeView('#pag-resumo-UraxECH #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/


       $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});


        $(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
        $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
        $('.navbar-inner').css('height','70px');
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

        //minuteStep: 5

      //19/03/2014
		componenteDataHora ($scope,$view);
        carregaAplicacoes($view,false,false,$scope);
   		carregaEmpresas($view);
		carregaSites($view);



        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


     


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });
		
		$view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });       

 

		 $view.find("select.filtro-empresa").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Empresas'
        });
     

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });
		
		
        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

      //2014-11-27 transição de abas
      var abas = [2,3,4];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


      // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});
       

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

      
        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

		 // Marca todos os empresas
        $view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});
        // GILBERTO 18/02/2014



      // EXIBIR AO PASSAR O MOUSE
       


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });
		
		
		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

     
        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
            $('div.btn-group.filtro-empresa').addClass('open');

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
            $('div.btn-group.filtro-empresa').removeClass('open');

        });
       


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-resumo-UraxECH");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-resumo-UraxECH");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });



        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };








    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }

		$scope.vendas = [];
        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };

    
		if ($scope.filtros.porDia) {
			var tabelaECH = "ResumoECHPassosDia"; 
			var tabelaURA = "ResumoDesempenhoGeral"; 
		}else{
			var tabelaECH = "ResumoECHPassos"; 
			var tabelaURA = "ResumoDesempenhoGeralHora"; 
		}
		stmt = db.use
	
	      + " with ECH as ("
		  + " select DatReferencia,"
		  + " sum(case when NumPassos >= 1 then QtdChamadas else 0 end) as QtdOperacao1,"
		  + " sum(case when NumPassos >= 2 then QtdChamadas else 0 end) as QtdOperacao2"
		  + " from " + tabelaECH +  ""
		  + " WHERE 1 = 1"
		  + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		  + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
		  stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		  stmt += restringe_consulta("CodSite", filtro_sites, true)
		  stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
		  stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
		  + " GROUP BY DatReferencia),"
		  + " URA as ("
		  + " select Dat_Referencia,"
		  + " sum(qtdchamadas - qtdtransfura) as qtdchamadas,"
		  + " sum(qtdderivadas) as qtdderivadas"
		  + " from " + tabelaURA +  ""
		  + " WHERE 1 = 1" //and cod_segmento = '' "
		  + " AND Dat_Referencia >= '" + formataDataHora(data_ini) + "'"
		  + " AND Dat_Referencia <= '" + formataDataHora(data_fim) + "'";
		  stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
		  stmt += restringe_consulta2("Cod_Segmento", filtro_segmentos, true)
		  stmt += restringe_consulta("CodSite", filtro_sites, true);
      stmt += " GROUP BY Dat_Referencia)"
		  + " select datReferencia,qtdOperacao1,qtdOperacao2,qtdchamadas,qtdderivadas from URA inner join ECH on DatReferencia = Dat_Referencia";	
	 
        
		
        executaQuery = executaQuery2; 
        log(stmt);


        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery2(columns) {
	
				
		  
        var data_hora = columns["datReferencia"].value,
        datReferencia = typeof columns[0].value==='string' ? formataDataHoraBR(data_hora) : formataDataHoraBR(data_hora);
        entrantes = columns["qtdchamadas"].value,
			  operacao1 = columns["qtdOperacao1"].value,
			  operacao2 = columns["qtdOperacao2"].value,
        qtdDerivadas = +columns["qtdderivadas"].value,
			  qtdPercTransf = (100 * operacao2 / operacao1).toFixed(2)
			  qtdPercDer = (100 * operacao1 / qtdDerivadas).toFixed(2)	
			  $scope.total_geral = $scope.total_geral + entrantes;	
			  $scope.total_transf = $scope.total_transf + operacao2;
			  $scope.total_der= $scope.total_der + operacao2;
			 
			  if ($scope.filtros.porDia) {datReferencia = datReferencia.replace("00:00:00","")} ;
			  
			  $scope.dados.push({
				data_hora: data_hora,
				datReferencia: datReferencia,
				operacao2: operacao2,
				derivadas: qtdDerivadas,
				operacao1: operacao1,
				entrantes: entrantes,
				percOper2: qtdPercTransf,
				percOper1: qtdPercDer
			  });



			  //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
			  if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			  }
        }

				$scope.total_geral = 0;
				$scope.total_transf = 0;
				$scope.total_der = 0;
				$scope.total_naoatend = 0;
				
                db.query(stmt,executaQuery, function (err, num_rows) {
                    userLog(stmt, 'Carrega dados', 2, err)

                  if($scope.porRegEDDD===true){
                    $('#pag-resumo-UraxECH table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1170px','margin-bottom':'100px'});
                  }else{
                    $('#pag-resumo-UraxECH table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1280px','margin-bottom':'100px'});
                  }
                    console.log("Executando query-> "+stmt+" "+num_rows);


                  var datFim = "";
                  

                    retornaStatusQuery(num_rows, $scope, datFim);
                    $btn_gerar.button('reset');
                  if(num_rows>0){
                    $btn_exportar.prop("disabled", false);
                  }
					
					$scope.dados.push({
						data_hora: "",
						datReferencia: "",
						transferida: $scope.total_transf,
						derivada: $scope.total_der,
						naoencontradas: $scope.total_naoatend,
						total: $scope.total_geral,
						pontoDerivacao: "",
						grupo1: "",
						grupo2: "TOTAL",
						regra: "",
						percTransf: (100 * $scope.total_transf / $scope.total_geral).toFixed(2)
					  });
					
                  //if(executaQuery !== executaQuery2){
                    $('.btn.btn-pivot').prop("disabled","false");
                    if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1<=limiteHoras/24 && num_rows>0){
                        $('.btn.btn-pivot').button('reset');
                    }
              
					$scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
					

					
                  });
         
				// GILBERTOOOOOO 17/03/2014
				$view.on("mouseup", "tr.resumo", function () {
					var that = $(this);
					$('tr.resumo.marcado').toggleClass('marcado');
					$scope.$apply(function () {
						that.toggleClass('marcado');
					});
				});
    };




	   
	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		  var $btn_exportar = $(this);

		  $btn_exportar.button('loading');

		  var linhas  = [];


		  linhas.push([{value:'Data', autoWidth:true}, {value:'Entrantes URA', autoWidth:true}, {value:'Derivadas URA', autoWidth:true},{value:'Qtd Operação 1', autoWidth:true},{value:'Qtd Operação 2', autoWidth:true},{value:'% Operação 1', autoWidth:true},{value:'% Operação 2', autoWidth:true}]);
			  $scope.dados.map(function (dado) {
				return linhas.push([{value:dado.datReferencia, autoWidth:true}, {value:dado.entrantes, autoWidth:true}, {value:dado.derivadas, autoWidth:true}, {value:dado.operacao1, autoWidth:true},{value:dado.operacao2, autoWidth:true},{value:dado.percOper1, autoWidth:true},{value:dado.percOper2, autoWidth:true}]);
			  });


			var planilha = {
				creator: "Estalo",
				lastModifiedBy: $scope.username || "Estalo",
				worksheets: [{ name: 'Resumo URA x ECH', data: linhas, table: true }],
				autoFilter: false,
				// Não incluir a linha do título no filtro automático
				dataRows: { first: 1 }
			};

			var xlsx = frames["xlsxjs"].window.xlsx;
			planilha = xlsx(planilha, 'binary');


			var milis = new Date();
			var file = 'resumoURAxECH_' + formataDataHoraMilis(milis) + '.xlsx';


			if (!fs.existsSync(file)) {
				fs.writeFileSync(file, planilha.base64, 'base64');
				childProcess.exec(file);
			}

			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);
		};

}

CtrlResumoUraxECH.$inject = ['$scope', '$globals'];
