function CtrlResumoRecargaM4U($scope, $globals) {
	
    win.title = "Resumo de Recargas M4U"; //Paulo 05/07/2019
    $scope.versao = versao;

    travaBotaoFiltro(0, $scope, "#pag-resumo-recarga-m4u", "Resumo de Recargas M4U - Atualizado em D-2");

    $scope.dados = [];
    $scope.colunas = [];
    $scope.csv = [];    
    
       
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };  


	$scope.valor = "d";
    // Filtros
    $scope.filtros = {       
        canais: []
    };	
	//$scope.operador = false;

    function geraColunas() {
        var array = [];

        array.push(
            {
                field: "dia",
                displayName: $scope.valor === "d" ? "Dia" : "Mês",
                width: 150
            },
            {
                field: "canal",
                displayName: "Canal",
                width: 150
            }, 
            {
                field: "valor",
                displayName: "Valor",
                width: 150,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'				
            },
            {
                field: "qtd",
                displayName: "Qtd",
                width: 150,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            },
            {
                field: "ticketMedio",
                displayName: "TicketMedio",
                width: 150,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
            }
        );

        return array;
    }
    
    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora
    };

    var $view;

    // Executa quando o conteúdo da tela for carregado.
    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-recarga-m4u");

        //19/03/2014
        componenteDataMaisHora($scope, $view);         
        carregaCanaisM4U($view,true);
        
        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });
        
        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {    
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view);
            carregaCanaisM4U($view,true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {           
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-recarga-m4u");
        }
		
		$view.on("mouseover",".grid",function(){$('div.notification').fadeIn(500)});

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {    

            limpaProgressBar($scope, "#pag-resumo-recarga-m4u");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.colunas = geraColunas();            
            $scope.listaDados.apply(this);                  
            
        }); 

        $view.on("click", "#alinkRecargaM4U", function(){ marcaTodosIndependente($('.filtro-canal-recarga-m4u'),'canaisM4U')});
     
    });

    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_canaisM4U = $view.find("select.filtro-canal-recarga-m4u").val() || [];
		var fator = $scope.valor === "h" ? "hour" : $scope.valor === "d" ? "day" : "month";

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        
        if (filtro_canaisM4U.length > 0) {
            $scope.filtros_usados += " Canais M4U: " + filtro_canaisM4U;
        }
    
        
        $scope.dados = [];
        $scope.csv = [];       
        var stmt = "";        
       
        $scope.filtro_canaisM4U_join = (filtro_canaisM4U.length > 0 ) ? "'"+filtro_canaisM4U.join("','")+"'" : '';
        
        stmt = db2.use +
		
		" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, Dia), 0) as Dia, " +
		" Canal, sum(Valor) as Valor, sum(Qtd) as Qtd, sum(valor)/isnull(sum(qtd),1) as TicketMedio" +
		" FROM ResumoRecargaM4U" +
		" WHERE  Dia >= '" + formataDataHora(data_ini) + "'" +
        " AND Dia <= '" + formataDataHora(data_fim) + "'" +
        ($scope.filtro_canaisM4U_join ? " AND Canal IN (" + $scope.filtro_canaisM4U_join + ")" : '')
		+ " group by DATEADD("+fator+", DATEDIFF("+fator+", 0, Dia), 0),canal";
               
        log(stmt);
        
        function executaQuery2(columns) {
            
            var dia = columns["Dia"] !== undefined ? ($scope.valor === "h" ? formataDataBR(columns["Dia"].value,"hora") : ($scope.valor === "d" ?  formataDataBR(columns["Dia"].value) : formataDataBR(columns["Dia"].value ,"mes" ))) : undefined;
            canal = columns["Canal"].value ? columns["Canal"].value : undefined,
            valor = columns["Valor"].value ? columns["Valor"].value : undefined,
            qtd = columns["Qtd"].value ? columns["Qtd"].value : 0,            
			TicketMedio = columns["TicketMedio"].value ? columns["TicketMedio"].value : 0;
			
			
			var s = {   
                dia: dia,             
                canal : canal,
                valor: valor,
                qtd: qtd,
                ticketMedio: TicketMedio.toFixed(2)                              
            }
					
            $scope.dados.push(s);

            var a = [];
            for (var item in $scope.colunas) {
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);
            
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }            
        }

        db2.query(stmt, executaQuery2, function (err, num_rows) {            
            num_rows = $scope.dados.length;
            console.log("Executando query-> " + stmt + " " + num_rows);
            var datFim = "";
            
            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
      
        });        
    };   

  
    // Exportar planilha XLSX
    $scope.exportaXLSX = function() {
	  
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
  
        var colunas = [];
        for (i = 0; i < $scope.colunas.length; i++) {
            colunas.push($scope.colunas[i].displayName);
        }
        
        var dadosExcel = [colunas].concat($scope.csv);
        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);
  
        var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'ResumoRecargaM4U' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);
              setTimeout(function() {
            $btn_exportar.button('reset');
        }, 500);
  
    };

}

CtrlResumoRecargaM4U.$inject = ['$scope', '$globals'];