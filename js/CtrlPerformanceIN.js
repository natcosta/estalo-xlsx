function CtrlPerformanceIN($scope, $globals) {


    win.title="Performance da IN"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 5000;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;


    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-performance-in", "Consulta limitada a "+$scope.limite_registros+" registros em resumo por DDD e dia.");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    //$scope.limite_registros = 500;

    $scope.dados = [];
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['codigo','ddd'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013
        sites: [],
        segmentos: [],
        perOrRes:"performance",
        porDia:false
    };

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-performance-in");
      treeView('#pag-performance-in #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-performance-in #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-performance-in #ed', 65,'ED','dvED');
      treeView('#pag-performance-in #ic', 95,'IC','dvIC');
      treeView('#pag-performance-in #tid', 115,'TID','dvTid');
      treeView('#pag-performance-in #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-performance-in #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-performance-in #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-performance-in #parametros', 225,'Parametros','dvParam');
      treeView('#pag-performance-in #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-performance-in  #monitoracao', 275, 'Monitoração', 'dvReparo');
         treeView('#pag-performance-in #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		 treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        //minuteStep: 5

      //19/03/2014
      componenteDataHora ($scope,$view);

      carregaAplicacoes($view,false,false,$scope);
      carregaSites($view);

      carregaSegmentosPorAplicacao($scope,$view,true);
      //GILBERTO - change de filtros

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});



        /*$view.find(".selectpicker").selectpicker({
        //noneSelectedText: 'Nenhum item selecionado',
        countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });


        // Exibe mais registros
        $scope.exibeMaisRegistros = function () {
            $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        };

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-performance-in");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function(){
                    atualizaInfo($scope,'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-performance-in");
        }


        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }


        $scope.dados = [];

        //var selects = [];


        //var stmt = db.use + "SELECT TOP " + $scope.limite_registros + " Dat_Referencia,"

      if($scope.filtros.perOrRes ==="performance"){

    var tabelas = tabelasParticionadas($scope, 'ResumoPerformanceIN', false);
    var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora(data_fim) + "'";


      var stmt = db.use + "SELECT TOP " + $scope.limite_registros +" a.codEvento,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (select CASE WHEN LEN(CodEvento)  = 1  THEN ('0' + CodEvento)"
      +" Else CodEvento END as CODEVENTO,"
      +" Codtransacao,"
      +" SUM(CONVERT(FLOAT, QtdChamadas)) as Chamadas from ResumoPerformanceIN"
      + " WHERE 1 = 1"
      + "   AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + "   AND DatReferencia "+datfim+""
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY CASE WHEN LEN(CodEvento)  = 1  THEN ('0' + CodEvento) Else CodEvento"
      + " END,CodTransacao"
      +") a GROUP BY a.codEvento"
      if(tabelas.nomes.length === 1){
        stmt += " ORDER BY a.codEvento";
      }

      if(tabelas.nomes.length > 1){

      stmt += " UNION ALL SELECT TOP " + $scope.limite_registros +" a.codEvento,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (select CASE WHEN LEN(CodEvento)  = 1  THEN ('0' + CodEvento)"
      +" Else CodEvento END as CODEVENTO,"
      +" Codtransacao,"
      +" SUM(CONVERT(FLOAT, QtdChamadas)) as Chamadas from ResumoPerformanceIN_p"
      + " WHERE 1 = 1"
      + "   AND DatReferencia >= '"+tabelas.data+"'"
      + "   AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY CASE WHEN LEN(CodEvento)  = 1  THEN ('0' + CodEvento) Else CodEvento"
      + " END,CodTransacao"
      +") a GROUP BY a.codEvento ORDER BY a.codEvento";

      }


      log(stmt);

      }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === false){


    var tabelas = tabelasParticionadas($scope, 'ResumoTransacoesINPorDDD', false);
    var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora(data_fim) + "'";


      var stmt = db.use + "SELECT TOP " + $scope.limite_registros +" a.ddd,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (SELECT ddd,codTransacao,sum(qtdchamadas) as Chamadas from ResumoTransacoesINPorDDD"
      + " WHERE 1 = 1"
      + "   AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + "   AND DatReferencia "+datfim+""
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY ddd,CodTransacao"
      +") a GROUP BY a.ddd"
      if(tabelas.nomes.length === 1){
        stmt += "ORDER BY a.DDD";
      }

      if(tabelas.nomes.length > 1){
        stmt += " UNION ALL SELECT TOP " + $scope.limite_registros +" a.ddd,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (SELECT ddd,codTransacao,sum(qtdchamadas) as Chamadas from ResumoTransacoesINPorDDD_p"
      + " WHERE 1 = 1"
      + "   AND DatReferencia >= '"+tabelas.data+"'"
      + "   AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY ddd,CodTransacao"
      +") a GROUP BY a.ddd";
        if(tabelas.length === 1){
          stmt += " ORDER BY a.ddd";
        }
      }

      log(stmt);


      }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === true){

    var tabelas = tabelasParticionadas($scope, 'ResumoTransacoesINPorDDD', false);
    var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataData(data_fim) + "'";

      var stmt = db.use + "SELECT TOP " + $scope.limite_registros +" a.ddd, a.DatReferencia,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (select ddd,codTransacao,sum(qtdchamadas) as Chamadas,"
      + "dateadd(DAY,0, datediff(day,0, DatReferencia)) as DatReferencia from ResumoTransacoesINPorDDD"
      + " WHERE 1 = 1"
      + "   AND dateadd(DAY,0, datediff(day,0, DatReferencia)) >= '" + formataData(data_ini) + "'"
      + "   AND dateadd(DAY,0, datediff(day,0, DatReferencia)) "+datfim+""
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY ddd,CodTransacao,dateadd(DAY,0, datediff(day,0, DatReferencia))"
      +") a GROUP BY a.ddd, a.datReferencia";

      if(tabelas.nomes.length === 1){
        stmt += " ORDER BY a.ddd, a.datReferencia";
      }

      if(tabelas.nomes.length > 1){

      stmt += " UNION ALL SELECT TOP " + $scope.limite_registros +" a.ddd, a.DatReferencia,"
      + "CONSULTA = sum(case when a.CodTransacao='CONSULTA' then a.Chamadas end),"
      + "ELEGIVEL = sum(case when a.CodTransacao='ELEGIVEL' then a.Chamadas end),"
      + "GARANTIA = sum(case when a.CodTransacao='GARANTIA' then a.Chamadas end),"
      + "MIGRACAO = sum(case when a.CodTransacao='MIGRAÇÃO' then a.Chamadas end),"
      + "ADESAO   = sum(case when a.CodTransacao='ADESÃO' then Chamadas end)"
      + " FROM (select ddd,codTransacao,sum(qtdchamadas) as Chamadas,"
      + "dateadd(DAY,0, datediff(day,0, DatReferencia)) as DatReferencia from ResumoTransacoesINPorDDD_p"
      + " WHERE 1 = 1"
      + "   AND dateadd(DAY,0, datediff(day,0, DatReferencia)) >= '"+tabelas.data+"'"
      + "   AND dateadd(DAY,0, datediff(day,0, DatReferencia)) <= '" + formataData(data_fim) + "'"
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      if (filtro_segmentos != "") {
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      } else {
          stmt += " AND CodSegmento = ''"
      }
      stmt += " GROUP BY ddd,CodTransacao,dateadd(DAY,0, datediff(day,0, DatReferencia))"
      +") a GROUP BY a.ddd, a.datReferencia ORDER BY a.ddd, a.datReferencia";

      }

      log(stmt);
      }

      var stmtCountRows = stmtContaLinhas(stmt);


        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas (columns) {
                $globals.numeroDeRegistros = columns[0].value;
        }

      var
              acumuConsulta = 0,
              acumuElegivel = 0,
              acumuGarantia = 0,
              acumuMigracao = 0,
              acumuAdesao   = 0,
              acumuTotal = 0;

       var i = 0;

        function executaQuery(columns){


            if($scope.filtros.perOrRes ==="performance"){
                var
            codigo = columns[0].value,
            respostaIN = unique2(cache.performanceINs.map(function(p){if(p.codigo === columns[0].value){return p.nome}})).toString(),
            consulta = +columns[1].value,
            elegivel = +columns[2].value,
            garantia = +columns[3].value,
            migracao = +columns[4].value,
            adesao   = +columns[5].value;

              $scope.dados.push({
                  codigo: codigo,
                  respostaIN: respostaIN,
                  consulta: consulta,
                  elegivel: elegivel,
                  garantia: garantia,
                  migracao: migracao,
                  adesao:adesao
                });

            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-performance-in");


            }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === false){

                var
            ddd = columns[0].value,
            consulta = +columns[1].value,
            elegivel = +columns[2].value,
            garantia = +columns[3].value,
            migracao = +columns[4].value,
            adesao   = +columns[5].value,
            total = +columns[1].value + +columns[2].value + +columns[3].value + +columns[4].value + +columns[5].value;

              $scope.dados.push({
                  ddd: ddd,
                  consulta: consulta,
                  elegivel: elegivel,
                  garantia: garantia,
                  migracao: migracao,
                  adesao: adesao,
                  total: total
                });
              acumuConsulta += consulta;
              acumuElegivel += elegivel;
              acumuGarantia += garantia;
              acumuMigracao += migracao;
              acumuAdesao += adesao;
              acumuTotal += total;

              //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-performance-in");


            }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === true){


                var
            ddd = columns[0].value,
            data = formataDataBR(columns[1].value),
            consulta = +columns[2].value,
            elegivel = +columns[3].value,
            garantia = +columns[4].value,
            migracao = +columns[5].value,
            adesao   = +columns[6].value,
            total = +columns[2].value + +columns[3].value + +columns[4].value + +columns[5].value + +columns[6].value;


                $scope.dados.push({
                  ddd: ddd,
                  data: data,
                  consulta: consulta,
                  elegivel: elegivel,
                  garantia: garantia,
                  migracao: migracao,
                  adesao:adesao,
                  total: total
                });
              acumuConsulta += consulta;
              acumuElegivel += elegivel;
              acumuGarantia += garantia;
              acumuMigracao += migracao;
              acumuAdesao += adesao;
              acumuTotal += total;


                if($scope.dados.length%1000===0){

                    $('#pag-performance-in table').css({'margin-top':'100px','max-width':'1210px','margin-bottom':'100px'});
                    $('.performance').hide();
                    $('.resumoDDD').hide();
                    $('.resumoDDDdia').show();

                    $scope.$apply();
                }

                //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-performance-in");
            }

        }



        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
          //console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);

                db.query(stmt,executaQuery, function (err, num_rows) {
                    userLog(stmt, 'Carrega registros', 2, err)
                    console.log("Executando query-> "+stmt+" "+num_rows);
                    retornaStatusQuery(num_rows, $scope);
                    $btn_gerar.button('reset');
                    if(num_rows>0){
                      $btn_exportar.prop("disabled", false);
                    }


                  if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === true && $scope.dados.length>0){
                    $scope.dados.push({
                      ddd: "Total",
                      data: "",
                      consulta: acumuConsulta,
                      elegivel: acumuElegivel,
                      garantia: acumuGarantia,
                      migracao: acumuMigracao,
                      adesao:acumuAdesao,
                      total: acumuTotal
                    });

$('#pag-performance-in table').css({'margin-top':'100px','max-width':'1210px','margin-bottom':'100px','margin-left':'auto','margin-right':'auto'});
                     $('.performance').hide();
                     $('.resumoDDD').hide();
                     $('.resumoDDDdia').show();



                  }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === false && $scope.dados.length>0){
                    $scope.dados.push({
                      ddd: "Total",
                      consulta: acumuConsulta,
                      elegivel: acumuElegivel,
                      garantia: acumuGarantia,
                      migracao: acumuMigracao,
                      adesao:acumuAdesao,
                      total: acumuTotal
                    });

$('#pag-performance-in table').css({'margin-top':'100px','max-width':'1000px','margin-bottom':'100px','margin-left':'auto','margin-right':'auto'});
                    $('.performance').hide();
                    $('.resumoDDD').show();
                    $('.resumoDDDdia').hide();

                  }else if($scope.filtros.perOrRes ==="performance" && $scope.dados.length>0){
                    $('#pag-performance-in table').css({'margin-top':'100px','max-width':'1310px','margin-bottom':'100px'});
                    $('.performance').show();
                    $('.resumoDDD').hide();
                    $('.resumoDDDdia').hide();
                  }

                });
          //});


          // GILBERTOOOOOO 20/03/2014
          $view.on("mouseup", "tr.resumo", function () {
              var that = $(this);
              console.log('passei');
              $('tr.resumo.marcado').toggleClass('marcado');
              $scope.$apply(function () {
                  that.toggleClass('marcado');
              });
          });

    };


   // Exportar planilha XLSX
   $scope.exportaXLSX = function () {

   var $btn_exportar = $(this);
   $btn_exportar.button('loading');

   //Alex 15-02-2014 TEMPLATE
      var tName = "";
   if($scope.filtros.perOrRes ==="performance"){
     tName = "tPerformanceINR";
   }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === false){
     tName = "tPerformanceIND";
   }else if($scope.filtros.perOrRes ==="resumo" && $scope.filtros.porDia === true){
     tName = "tPerformanceINDD";
   }


   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='"+tName+"'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = ''+tName+'.xlsx';



   var buffer = toBuffer(toArrayBuffer(arquivo));

   fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'p'+(tName.substring(2)).substring(0,tName.length)+'_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)
            //?
          });
    };
}
CtrlPerformanceIN.$inject = ['$scope', '$globals'];
