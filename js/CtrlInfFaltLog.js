function CtrlInfFaltLog($scope, $globals) {

    win.title="Informações faltantes nos logs"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;
	$scope.callLog = "";
	
	$scope.nomeExportacao = "InfoFaltantesLogs";


    $scope.limite_registros = 5000;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao(""); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-info-faltante-log", "Informações faltantes nos logs");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
	$scope.dados2 = [];

    $scope.gridDados = {
				
		data: $scope.dados,
		columnDefs: $scope.colunas,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	$scope.gridDados2 = {
		
		data: $scope.dados2,
		columnDefs: $scope.colunas2,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};

    
    $scope.ordenacao = 'site';
    $scope.decrescente = false;
	$scope.linkArqLog = "";


    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: []
    };

    $scope.aba = 2;
	
	

    function geraColunas(tipo){
      var array = [];
	  
	  

      array.push(
	    { field: "datReferencia", displayName: "Data", width: 100, pinned: true },
		{ field: "aplicacao", displayName: "Aplicação", width: 100, pinned: true },        
        { field: "qtd", displayName: "Qtd", width: 100, pinned: true },
		{ field: "chamadas", displayName: "Total", width: 100, pinned: true },
		{ field: "qtdPercChamadas", displayName: "%", width: 100, pinned: true }
      );


      return array;
    }
	
	/*
	$scope.randomSize = function () {
		var newHeight = Math.floor(Math.random() * (300 - 100 + 1) + 300);
		var newWidth = Math.floor(Math.random() * (600 - 200 + 1) + 200);
		
		angular.element(document.getElementsByClassName('grid')[0]).css('height', newHeight + 'px');
		angular.element(document.getElementsByClassName('grid')[0]).css('width', newWidth + 'px');
	};
	*/

    // Filtros: data e hora
    var agora = new Date();


    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
        };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-info-faltante-log");
      treeView('#pag-info-faltante-log #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-info-faltante-log #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-info-faltante-log #ed', 65,'ED','dvED');
      treeView('#pag-info-faltante-log #ic', 95,'IC','dvIC');
      treeView('#pag-info-faltante-log #tid', 115,'TID','dvTid');
      treeView('#pag-info-faltante-log #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-info-faltante-log #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-info-faltante-log #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-info-faltante-log #parametros', 225,'Parametros','dvParam');
      treeView('#pag-info-faltante-log #admin', 255,'Administrativo','dvAdmin');
	  treeView('#pag-info-faltante-log #monitoracao', 275, 'Monitoração', 'dvReparo');
      treeView('#pag-info-faltante-log #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      /*$('.nav.aba3').css('display','none');*/

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

      //19/03/2014
      componenteDataHora ($scope,$view);

      //ALEX - Carregando filtros
      carregaAplicacoes($view,false,false,$scope);
      carregaSites($view);

      carregaSegmentosPorAplicacao($scope,$view,true);
      //GILBERTO - change de filtros

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});



    $view.find("select.filtro-aplicacao").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} aplicações',
        showSubtext: true
    });

    $view.find("select.filtro-site").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} Sites/POPs',
        showSubtext: true
    });

    $view.find("select.filtro-segmento").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} segmentos'
    });

    $view.find("select.filtro-op").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} operações'
    });



    $view.on("change", "select.filtro-site", function () {
        var filtro_sites = $(this).val() || [];
        Estalo.filtros.filtro_sites = filtro_sites;
    });
	
	

      //2014-11-27 transição de abas
      var abas = [2,3];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


    //Marcar todos

    // Marca todos os sites
    $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

    // Marca todos os segmentos
    $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

    //Marcar todas aplicações
    $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

    


//GILBERTO - change de segmentos

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        


        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        

        var limite = 999;
        $scope.valorCol = limite;



        // Exibe mais registros
        $scope.exibeMaisRegistros = function () {
          $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        };


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
			
          limpaProgressBar($scope, "#pag-info-faltante-log");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            $scope.colunas = geraColunas(1);            
			$scope.colunas2 = geraColunas(2);    
            $scope.listaDados.apply(this);
        });



        $view.on("click", ".btn-exportarCSV-interface", function () {
          // Exportando aba Assunto
          exportaCSV($scope.colunas, $scope.dados, "Assunto_");

          // Exportando aba TransferID
          exportaCSV($scope.colunas2, $scope.dados2, "TranferID_");
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-info-faltante-log");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });




    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportarCSV-interface");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        



        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
    + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        




        $scope.dados = [];
		$scope.dados2 = [];
		var consultas = 0;
		
		
		var stmt = db.use+'';
		
		stmt += 'with a as('
		stmt += "select convert(date, DatReferencia) as DatReferencia, CodAplicacao, sum(Qtd) as qtd"
		+" from ResumoAssunto"
		+ " WHERE 1 = 1"
		+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		+ " AND DatReferencia <= '"+formataDataHora(data_fim)+"'"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)        
		+" and assunto = ''"
		+" group by convert(date, DatReferencia), CodAplicacao"
		+'),'
        +'b as('
		stmt += "select convert(date, DatReferencia) as DatReferencia, CodAplicacao, sum(Qtd) as chamadas"
		+" from ResumoAssunto"
		+ " WHERE 1 = 1"
		+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		+ " AND DatReferencia <= '"+formataDataHora(data_fim)+"'"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)        
		+" group by convert(date, DatReferencia), CodAplicacao"
		stmt +=")"
		+" select a.DatReferencia, a.CodAplicacao, a.qtd, b.chamadas"
		+" from  a left outer join b"
		+" on a.DatReferencia = b.DatReferencia"
		+" and a.CodAplicacao = b.CodAplicacao"
        stmt += " order by a.DatReferencia ASC";

		log(stmt);
		
		var stmt2 = db.use+'';
		
		stmt2 += 'with a as('
		stmt2 += "select convert(date, DatReferencia) as DatReferencia, CodAplicacao, sum(QtdChamadas) as qtd"
		+" from ResumoTransferID"
		+ " WHERE 1 = 1"
		+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		+ " AND DatReferencia <= '"+formataDataHora(data_fim)+"'"
		+ " AND (IndicDelig = 'TRN' OR IndicDelig is NULL)"
        stmt2 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt2 += restringe_consulta("CodSite", filtro_sites, true)
		stmt2 += restringe_consulta("CodSegmento", filtro_segmentos, true)        
		+" and TransferID = ''"
		+" group by convert(date, DatReferencia), CodAplicacao"
		+'),'
        +'b as('
		stmt2 += "select convert(date, DatReferencia) as DatReferencia, CodAplicacao, sum(QtdChamadas) as chamadas"
		+" from ResumoTransferID"
		+ " WHERE 1 = 1"
		+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		+ " AND DatReferencia <= '"+formataDataHora(data_fim)+"'"
		+ " AND (IndicDelig = 'TRN' OR IndicDelig is NULL)"
        stmt2 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt2 += restringe_consulta("CodSite", filtro_sites, true)
		stmt2 += restringe_consulta("CodSegmento", filtro_segmentos, true)        
		+" group by convert(date, DatReferencia), CodAplicacao"
		stmt2 +=")"
		+" select a.DatReferencia, a.CodAplicacao, a.qtd, b.chamadas"
		+" from  a left outer join b"
		+" on a.DatReferencia = b.DatReferencia"
		+" and a.CodAplicacao = b.CodAplicacao"
        stmt2 += " order by a.DatReferencia ASC";
		
		
		

		log(stmt2);



        var stmtCountRows = stmtContaLinhas(stmt);


        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas (columns) {
                $globals.numeroDeRegistros = columns[0].value;
        }

        


        function executaQuery(columns){
        
            var data_hora = columns[0].value,
			  datReferencia = typeof columns[0].value==='string' ? formataDataBRString(data_hora) : formataDataBR(data_hora),
			  aplicacao = columns[1].value,			
			  qtd = columns[2].value,
			  chamadas = columns[3].value,
			  qtdPercChamadas = (100 * qtd / chamadas).toFixed(2);
			 
            $scope.dados.push({
              datReferencia: datReferencia,
			  aplicacao: aplicacao,
              //tipoInformacao: tipoInformacao,
              qtd: qtd,
			  chamadas:chamadas,
			  qtdPercChamadas:qtdPercChamadas
			
            });
            
           
        }
		
		function executaQuery2(columns){
        
            var data_hora = columns[0].value,
			  datReferencia = typeof columns[0].value==='string' ? formataDataBRString(data_hora) : formataDataBR(data_hora),
			  aplicacao = columns[1].value,			
			  qtd = columns[2].value,
			  chamadas = columns[3].value,
			  qtdPercChamadas = (100 * qtd / chamadas).toFixed(2);
			 
            $scope.dados2.push({
              datReferencia: datReferencia,
			  aplicacao: aplicacao,
              //tipoInformacao: tipoInformacao,
              qtd: qtd,
			  chamadas:chamadas,
			  qtdPercChamadas:qtdPercChamadas
			
            });
            
           
        }
		
		db.query(stmt, executaQuery, function (err, num_rows) {
			consultas +=num_rows;
                console.log("Executando query-> " + stmt + " " + num_rows);
				db.query(stmt2, executaQuery2, function (err, num_rows) {
			consultas +=num_rows;
                console.log("Executando query-> " + stmt2 + " " + num_rows);
				
				
                userLog(stmt2, 'Numero de registros', 2, err)
                retornaStatusQuery(consultas, $scope);
                $btn_gerar.button('reset');


              if(consultas > 0){
				$scope.colunas = geraColunas(1);				
				$scope.gridDados.data = $scope.dados;
				$scope.gridDados.columnDefs = $scope.colunas;				
				
				$scope.colunas2 = geraColunas(2);				
				$scope.gridDados2.data = $scope.dados2;
				$scope.gridDados2.columnDefs = $scope.colunas2;				
				$scope.$apply();
				
				  
                $btn_exportar.prop("disabled", false);
              }else{
				  $scope.dados = [];
				  $scope.gridDados.data = $scope.dados;
				  $scope.dados2 = [];
				  $scope.gridDados2.data = $scope.dados2;
				  $scope.$apply();
				  
			  }
			  
            });
			});
		
          
            
         
          
        //});
          $view.on("mouseup", "tr.resumo", function () {
              var that = $(this);
              //console.log('passei');
              $('tr.resumo.marcado').toggleClass('marcado');
              $scope.$apply(function () {
                  that.toggleClass('marcado');
              });
          });
    };
	
	
	// Consulta o log da chamada
  $scope.consultaCallLog = function (chamada,num) {
	  
	  if(chamada['evidencia'+num]===""){
		  $scope.callLog = "Não há log";
          $scope.linkArqLog = "";
		  return;
	  }
	  
	  $scope.callLog = "Carregando...";	  
	

	var tabela = "CallLog";
	if (unique2(cache.apls.map(function (a) { if (a.nvp) return a.codigo; })).indexOf(chamada.aplicacao) >= 0) {
	  tabela = "CallLogNVP";
	}
	if (chamada.aplicacao === "MENUOI") {
	  tabela = "CallLogM4U";
	}

	var stmt = "SELECT TOP 1 Comprimido AS ARQUIVO FROM " + tabela
	  + " WHERE CodUCID = '" + chamada["evidencia"+num] + "'"
	  //+ "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'"
	  + " ORDER BY LEN(CodUCID)";
	log(stmt);	


	
	executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) { console.log(dado); });
	
	    var teste4 = false;
	
		var teste3 = setInterval(function () {
		  if(fs.existsSync(tempDir3()+chamada["evidencia"+num] + '_log.zip')){
			  clearInterval(teste3);			
		
	  // Descomprimir o log original
	  zlib.unzip(new Buffer(fs.readFileSync(tempDir3()+chamada["evidencia"+num] + '_log.zip')), function (err, buffer) {
		$scope.$apply(function () {
		  if (err) { clearInterval(teste3); log(err); return; }		  
		  //console.log(buffer.toString());
		  $scope.callLog = buffer.toString();	
		  fs.writeFileSync(tempDir3()+chamada["evidencia"+num] + '.txt',$scope.callLog);
		  $scope.callLog = $scope.callLog.replace(/(?:\r\n|\r|\n)/g, '<br />');
		  $scope.linkArqLog = tempDir3()+chamada["evidencia"+num] + '.txt';
		  
		  setTimeout(function(){
			  highlight(chamada.tipoInformacao);
		  },3000);
		  
		  
		   if(fs.existsSync(tempDir3()+chamada["evidencia"+num] + '_log.zip')){
			   fs.unlinkSync(tempDir3()+chamada["evidencia"+num] + '_log.zip');
			   
			   teste4 = false;
		   }
		   
		});
	  }); 
		  }else{			  
			  //$('#uar').text("Carregando...");			  
			  if(!teste4){
				  tempoLimiteCarregarLogOriginal();
				  teste4 = true;
			  }
		  }
		}, 500);
		
		function tempoLimiteCarregarLogOriginal(){		
		setTimeout(function(){
			if(!fs.existsSync(tempDir3()+chamada.evidencia + '_log.zip') && teste4){
				clearInterval(teste3);
				$scope.callLog = "Log demorando para carregar ou indisponível";
				$scope.linkArqLog = "";
				$scope.$apply();
				//$('#uar').text("Log demorando para carregar ou indisponível");				
			}
		},20000);
		}
	
	return true;
  };


// Exporta planilha XLSX
  $scope.exportaCSV = function () {

    var linhas  = [];
    $scope.dados.forEach(function (dado) {
      linhas.push($scope.colunas.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });
	
	var linhas2  = [];
    $scope.dados2.forEach(function (dado) {
      linhas2.push($scope.colunas2.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });

    // Exportando aba Assunto
    separator = ";"
    colunas = ""
    file_name = tempDir3() + "Assunto_" + Date.now()
    for (i = 0; i < $scope.colunas.length; i++) {
        colunas = colunas + $scope.colunas[i].displayName + separator
    }
    colunas = colunas + "\n"
    for (i = 0; i < linhas.length; i++) {
        colunas = colunas + linhas[i].join(separator) + "\n"
    }
    
    exportaTXT(colunas, file_name, 'csv', true);

    // Exportando aba TranferID
    colunas = ""
    file_name = tempDir3() + "TransferID_" + Date.now()
    for (i = 0; i < $scope.colunas2.length; i++) {
        colunas = colunas + $scope.colunas2[i].displayName + separator
    }
    colunas = colunas + "\n"
    for (i = 0; i < linhas2.length; i++) {
        colunas = colunas + linhas2[i].join(separator) + "\n"
    }
    
    exportaTXT(colunas, file_name, 'csv', true);
  };

    
		
	
	$scope.abrirLinkPreviewLog = function (){
		//console.log("Tentando abrir preview");
		//console.log($scope.linkArq);
		gui.Shell.openItem($scope.linkArqLog);
	}
}
CtrlInfFaltLog.$inject = ['$scope','$globals'];
