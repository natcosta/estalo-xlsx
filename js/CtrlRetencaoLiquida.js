/*
 * CtrlRetencaoLiquida
 */
function CtrlRetencaoLiquida($scope, $globals) {

    win.title="Resumo de repetidas do reset"; //27/02/2014
	
	$scope.nomeExportacao = "ResumoRetliq";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
    
    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-retencao-liquida", "Resumo de repetidas do reset");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
	$scope.dados2 = [];
	$scope.dados3 = [];
	$scope.dados4 = [];
	
	$scope.pivot = false;
	
	
	$scope.gridDados = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados,
		columnDefs: $scope.colunas,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	$scope.gridDados2 = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados2,
		columnDefs: $scope.colunas2,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	$scope.gridDados3 = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados3,
		columnDefs: $scope.colunas3,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
		
	$scope.gridDados4 = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados4,
		columnDefs: $scope.colunas4,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	/* ag grid */
	
	$scope.gridDados4ag = {
    columnDefs: [],
    rowData: []
	};

	$scope.grupos = [];
	$scope.arrDatas = [];
	$scope.dadosQuery = [];
	$scope.objDadosPorData = {};
	$scope.objDadosPorGrupo = {};
	
	/*ag grid */
	
	$scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;

    $scope.log = [];

    $scope.aplicacoes = cache.apls; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        sites: [],
        segmentos: []
    };

    $scope.tabela = "";
    $scope.valor = "24h";
    $scope.agrupar = false;
    $scope.perc = false;



  function geraColunas(tipo){
    var array = [];

    array.push(
	{ field: "data_hora_BR", displayName: "Data", width: 130, pinned: true },
	{ field: "aplicacao", displayName: "Aplicação", width: 130, pinned: true }
	);
	
	if(tipo ===1){
		array.push({ field: "codregiao", displayName: "DDD", width: 100, pinned: true });
	}else if(tipo===2){
		array.push({ field: "codregiao", displayName: "UF", width: 100, pinned: true });
	}else if(tipo===3){
		array.push({ field: "codregiao", displayName: "Região", width: 100, pinned: true });
	}
	
	array.push(
	{ field: "qtdChamadas", displayName: "Oferecidas", width: 100, cellClass: "grid-align", pinned: false},
	{ field: "qtdRetidas", displayName: "Retidas", width: 100, cellClass: "grid-align", pinned: false},
	{ field: "qtdRepetidas", displayName: "Repetidas", width: 100, cellClass: "grid-align", pinned: false}
	
	);
    return array;

  }

    $scope.aba = 2;

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: mesAnterior(dat_consoli),
            max: dat_consoli*/
        };

    var $view;

    $scope.$on('$viewContentLoaded', function () {


        $view = $("#pag-retencao-liquida");

      $(".aba3").css({'position':'fixed','left':'55px','top':'40px'});
      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});


        componenteDataHora ($scope,$view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view,false,false,$scope);
		carregaDoisDigitos($view,true);
		carregaMotivos($view);
		carregaEntradas($view);

        

        carregaSegmentosPorAplicacao($scope,$view,true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


        $view.on("change", "select.filtro-aplicacao", function(){
          var arr = $(this).val();
          if(arr !== null && arr[0]==='NOVAOITV'){
            $('.filtro-chamador').prop('disabled',false);
          }else{
            $('.filtro-chamador').prop('disabled','disabled');
          }
        });


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-motivo").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} motivos',
            showSubtext: true
        });
		
		$view.find("select.filtro-entrada").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} entradas',
            showSubtext: true
        });


      


        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

		//Marca um RANGE de dígitos
        $view.on("click", "#alinkUltDoisDig", function(){
            //marcaTodosIndependente($('.filtro-ud'),'uds')
            
            rangePrompt('Determine a gama de valores a serem incluídos como "00-15".', "Marcar Valores", function callback(valor1, valor2){
              function doubleDigit(n){
                  return (n).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
              }
              
              if(valor1==0&&valor2==0){
                $('.filtro-udd').selectpicker('val', []);
              };
              
              if(valor1&&valor2){//valor.indexOf("-")
                console.log("Possui -");
                //console.log("Valor 1:"+valor.split("-")[0]+" Segundo Valor: "+valor.split("-")[1]);
                
                //console.log("Valor 2:"+valor.split("-")[0]);
                var i=doubleDigit(valor1);//valor.split("-")[0];
                var j=doubleDigit(valor2);//valor.split("-")[1];
                console.log("Valor 1:"+i+" Valor2: "+j);
                console.log("I: "+i+" J: "+j);
                rangeOfValues = [];            
                while(i<=j){//7
                  if(i==0){
                    rangeOfValues.push("00");
                  console.log("Adicionou: "+i);
                  }else{
                    rangeOfValues.push(doubleDigit(i));
                    console.log("Adicionou: "+doubleDigit(i));
                  }
                  i++;
                  //i=doubleDigit(i)+1;
                 }
              //$('.filtro-udd').selectpicker().val();
              $('.filtro-udd').selectpicker('val', rangeOfValues);
              
              }else{
                console.log("Não Possui -");
              }
              
              
            });
           });
		   
		   
		   // EXIBIR AO PASSAR O MOUSE     
        
        $('div > ul > li >>div> div.btn-group.filtro-udd').mouseover(function () {
            $('div.btn-group.filtro-udd').addClass('open');
            $('div.btn-group.filtro-udd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
       
        $('div > ul > li >>div> div.btn-group.filtro-udd').mouseout(function () {
            $('div.btn-group.filtro-udd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ud').mouseover(function () {
            $('div.btn-group.filtro-ud').addClass('open');
            $('div.btn-group.filtro-ud>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ud').mouseout(function () {
            $('div.btn-group.filtro-ud').removeClass('open');
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-retencao-liquida");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            
			$scope.colunas = geraColunas(1);            
			$scope.colunas2 = geraColunas(2);            
			$scope.colunas3 = geraColunas(3);            
			$scope.colunas4 = geraColunas(4);
            $scope.listaDados.apply(this);
			
        });

        /* $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        }); */

        $view.on("click", ".btn-exportarCSV-interface", function () {
          // Exportando aba DDD
          exportaCSV($scope.colunas, $scope.dados, "DDD_");

          // Exportando aba UF
          exportaCSV($scope.colunas2, $scope.dados2, "UF_");

          // Exportando aba Região
          exportaCSV($scope.colunas3, $scope.dados3, "Região_");

          // Exportando aba URA
          exportaCSV($scope.colunas4, $scope.dados4, "URA_");
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);


            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });



        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-retencao-liquida");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };




    // Lista chamadas conforme filtros
    $scope.listaDados = function () {




        var sufixo = $scope.valor;
  
        $globals.numeroDeRegistros = 0;
  
        var $btn_exportar = $view.find(".btn-exportarCSV-interface");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val()|| [];
        var filtro_udd = $view.find("select.filtro-udd").val() || [];
		var filtro_motivo = $view.find("select.filtro-motivo").val() || [];
		var filtro_entrada = $view.find("select.filtro-entrada").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_udd.length > 0) { $scope.filtros_usados += " UDs: " + filtro_udd; }
		if (filtro_motivo.length > 0) { $scope.filtros_usados += " Motivos: " + filtro_motivo; }
		if (filtro_entrada.length > 0) { $scope.filtros_usados += " Entradas: " + filtro_entrada; }
		$scope.dados = [];
		$scope.dados2 = [];
		$scope.dados3 = [];
		$scope.dados4 = [];
		var consultas = 0;
		$scope.grupos = filtro_aplicacoes;

      var stmt = db.use;
	  
	  stmt += "with a as( "
	  
	  
	  stmt += "Select DatReferencia as datr, CodAplicacao as capli, ddd as creg, "            
      stmt += "SUM(QtdRepetidas) as rep "     
      + " FROM " + db.prefixo + "ResumoResetRepetidas"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt += restringe_consulta("FinalTel", filtro_udd, true)
		stmt += restringe_consulta("Motivo", filtro_motivo, true)
		stmt += restringe_consulta("Entrada", filtro_entrada, true)
		stmt += " and janela = '"+sufixo+"'"
		stmt += " GROUP BY DatReferencia, CodAplicacao, ddd"
	  
	  stmt += "), "
	  stmt += "b as ( "

      stmt += "Select DatReferencia as datr, CodAplicacao as capli, ddd as creg, "            
      stmt += "SUM(QtdOferecidas) as cha, SUM(QtdRetidas) as ret "     
      + " FROM " + db.prefixo + "ResumoRepetidasReset"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt += restringe_consulta("FinalTel", filtro_udd, true)
		//stmt += restringe_consulta("Motivo", filtro_motivo, true)
		//stmt += restringe_consulta("Entrada", filtro_entrada, true)
		stmt += " and janela = '"+sufixo+"'"
		stmt += " GROUP BY DatReferencia, CodAplicacao, ddd"
		
	  stmt += ") "
	  stmt += "select b.datr, b.capli, b.creg,b.cha,b.ret,isnull(a.rep,0) as rep "
	  stmt += "from a "
	  stmt += "right outer join b "
	  stmt += "on a.datr = b.datr and a.capli = b.capli and a.creg = b.creg "
	  stmt += "order by b.datr, b.capli, b.creg";

        log(stmt);
		
	var stmt2 = db.use;

      stmt2 += "with a as( "	
	  
	  stmt2 += "Select DatReferencia as datr, CodAplicacao as capli, c.UF as creg, " 
      stmt2 += "SUM(QtdRepetidas) as rep " 	  
      + " FROM " + db.prefixo + "ResumoResetRepetidas r"
	  + " left outer join UF_DDD c"
      + " on  c.ddd= r.DDD"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt2 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt2 += restringe_consulta("FinalTel", filtro_udd, true)
		stmt2 += restringe_consulta("Motivo", filtro_motivo, true)
		stmt2 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt2 += " and janela = '"+sufixo+"'"
		stmt2 += " GROUP BY DatReferencia, CodAplicacao,c.UF"        
		
	  stmt2 += "), "
	  stmt2 += "b as ( "

      stmt2 += "Select DatReferencia as datr, CodAplicacao as capli, c.UF as creg, " 
      stmt2 += "SUM(QtdOferecidas) as cha, SUM(QtdRetidas) as ret " 	  
      + " FROM " + db.prefixo + "ResumoRepetidasReset r"
	  + " left outer join UF_DDD c"
      + " on  c.ddd= r.DDD"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt2 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt2 += restringe_consulta("FinalTel", filtro_udd, true)
		//stmt2 += restringe_consulta("Motivo", filtro_motivo, true)
		//stmt2 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt2 += " and janela = '"+sufixo+"'"
		stmt2 += " GROUP BY DatReferencia, CodAplicacao,c.UF"
        
		
		
	  stmt2 += ") "
	  stmt2 += "select b.datr, b.capli, b.creg,b.cha,b.ret,isnull(a.rep,0) as rep "
	  stmt2 += "from a "
	  stmt2 += "right outer join b "
	  stmt2 += "on a.datr = b.datr and a.capli = b.capli and a.creg = b.creg "
	  stmt2 += "order by b.datr, b.capli, b.creg";      

        log(stmt2);
		
		
		var stmt3 = db.use;
		
		stmt3 += "with a as( "
      

      stmt3 += "Select DatReferencia as datr, CodAplicacao as capli, c.Regiao as creg, " 
      stmt3 += "SUM(QtdRepetidas) as rep " 	  
      + " FROM " + db.prefixo + "ResumoResetRepetidas r"
	  + " left outer join UF_DDD c"
      + " on  c.ddd= r.DDD"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt3 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt3 += restringe_consulta("FinalTel", filtro_udd, true)
		stmt3 += restringe_consulta("Motivo", filtro_motivo, true)
		stmt3 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt3 += " and janela = '"+sufixo+"'"
		stmt3 += " GROUP BY DatReferencia, CodAplicacao,c.Regiao"
        
		stmt3 += "), "
	    stmt3 += "b as ( "
		
		
	  stmt3 += "Select DatReferencia as datr, CodAplicacao as capli, c.Regiao as creg, " 
      stmt3 += "SUM(QtdOferecidas) as cha, SUM(QtdRetidas) as ret " 	  
      + " FROM " + db.prefixo + "ResumoRepetidasReset r"
	  + " left outer join UF_DDD c"
      + " on  c.ddd= r.DDD"
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt3 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt3 += restringe_consulta("FinalTel", filtro_udd, true)
		//stmt3 += restringe_consulta("Motivo", filtro_motivo, true)
		//stmt3 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt3 += " and janela = '"+sufixo+"'"
		stmt3 += " GROUP BY DatReferencia, CodAplicacao,c.Regiao"
		
		
	  stmt3 += ") "
	  stmt3 += "select b.datr, b.capli, b.creg,b.cha,b.ret,isnull(a.rep,0) as rep "
	  stmt3 += "from a "
	  stmt3 += "right outer join b "
	  stmt3 += "on a.datr = b.datr and a.capli = b.capli and a.creg = b.creg "
	  stmt3 += "order by b.datr, b.capli, b.creg";
      

        log(stmt3);
		
		
		
		var stmt4 = db.use;
		
		stmt4 += "with a as( "
      

      stmt4 += "Select DatReferencia as datr, CodAplicacao as capli, " 
      stmt4 += "SUM(QtdRepetidas) as rep " 	  
      + " FROM " + db.prefixo + "ResumoResetRepetidas r"	  
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt4 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt4 += restringe_consulta("FinalTel", filtro_udd, true)
		stmt4 += restringe_consulta("Motivo", filtro_motivo, true)
		stmt4 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt4 += " and janela = '"+sufixo+"'"
		stmt4 += " GROUP BY DatReferencia, CodAplicacao"
		
		
		stmt4 += "), "
	    stmt4 += "b as ( "
		
		
	  stmt4 += "Select DatReferencia as datr, CodAplicacao as capli, " 
      stmt4 += "SUM(QtdOferecidas) as cha, SUM(QtdRetidas) as ret " 	  
      + " FROM " + db.prefixo + "ResumoRepetidasReset r"	  
      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        stmt4 += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt4 += restringe_consulta("FinalTel", filtro_udd, true)
		//stmt4 += restringe_consulta("Motivo", filtro_motivo, true)
		//stmt4 += restringe_consulta("Entrada", filtro_entrada, true)
		stmt4 += " and janela = '"+sufixo+"'"
		stmt4 += " GROUP BY DatReferencia, CodAplicacao"
		
	  stmt4 += ") "
	  stmt4 += "select b.datr,b.capli,b.cha,b.ret,isnull(a.rep,0) as rep "
	  stmt4 += "from a "
	  stmt4 += "right outer join b "
	  stmt4 += "on a.datr = b.datr and a.capli = b.capli "
	  stmt4 += "order by b.datr, b.capli";
	  
	  if($scope.pivot){
        
      /* ag grid */
	$scope.arrDatas = [];
    $scope.dadosQuery = [];
    $scope.objDadosPorData = {};
    $scope.objDadosPorGrupo = {};
    $scope.gridDados4ag.rowData = [];
    $scope.gridDados4ag.columnDefs = [
      {
        headerName: "Aplicação",
        field: "grupo",
        pinned: 'left',
        cellStyle: function(params) {
          if (params.node.rowIndex % 3 === 0) {
            return { 
              'border-top': '#04b2d9 solid 2px',
              'border-right': '0',
              'border-left': '0',
              'background': '#f5f5f5'
            }
          } else {
            return { 
              // 'border-top': 'f5f5f5',
              'border-right': '0',
              'border-left': '0',
              'background': '#f5f5f5'
            }
          }
        }
      },
      {
        headerName: "Status",
        field: "status",
        pinned: 'left',
        width: 100,
        cellStyle: function(params) {
          if (params.node.rowIndex % 3 === 0) {
            if (params.node.rowIndex % 2 === 0) {
              return { 
                'border-top': '#04b2d9 solid 2px',
                'border-right': '0',
                'border-left': '0',
                'background': '#f2f2f2'
              }
            } else {
              return { 
                'border-top': '#04b2d9 solid 2px',
                'border-right': '0',
                'border-left': '0',
                'background': '#e2e2e2'
              }
            }
          } else if (params.node.rowIndex % 2 === 0) {
            return { 
              'border-top': '#f5f5f5 solid',
              'border-right': '0',
              'border-left': '0',
              'background': '#f2f2f2'
            } 
          } else {
            return { 
              'border-top': '#f5f5f5 solid',
              'border-right': '0',
              'border-left': '0',
              'background': '#e2e2e2'
            }
          }
        }
      }
    ];
    $scope.gridDados4ag.api.setColumnDefs([]);
    $scope.gridDados4ag.api.setRowData([]);
	
	
    /* ag grid */
	  }
        log(stmt4);


        var stmtCountRows = stmtContaLinhas(stmt);


        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }
		
		var totaisDados = {
				data_hora: "_",
                data_hora_BR: "_",
                aplicacao: "_",
                codregiao: "TOTAL",
                qtdChamadas: 0,
				qtdRetidas: 0,
                qtdRepetidas: 0

            };
		var totaisDados2 = {
				data_hora: "_",
                data_hora_BR: "_",
                aplicacao: "_",
                codregiao: "TOTAL",
                qtdChamadas: 0,
				qtdRetidas: 0,
                qtdRepetidas: 0

            };
		var totaisDados3 = {
				data_hora: "_",
                data_hora_BR: "_",
                aplicacao: "_",
                codregiao: "TOTAL",
                qtdChamadas: 0,
				qtdRetidas: 0,
                qtdRepetidas: 0

            };
		var totaisDados4 = {
				data_hora: "_",
                data_hora_BR: "_",                
                aplicacao: "TOTAL",
                qtdChamadas: 0,
				qtdRetidas: 0,
                qtdRepetidas: 0

            };

		function executaQuery(columns) {
			
			var data_hora = columns[0].value,
			data_hora_BR = typeof data_hora ==='string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
			aplicacao = obtemNomeAplicacao(columns["capli"].value),
			codregiao = columns["creg"] === undefined ? "" : columns["creg"].value,
			qtdChamadas = +columns["cha"].value,
			qtdRetidas = +columns["ret"].value,
			qtdRepetidas = +columns["rep"].value;
			
			$scope.dados.push({
				data_hora: data_hora,
                data_hora_BR: data_hora_BR.substring(0,10),
                aplicacao: aplicacao,
                codregiao: codregiao,
                qtdChamadas: qtdChamadas,
				qtdRetidas: qtdRetidas,
                qtdRepetidas: qtdRepetidas

            });
			
			totaisDados.qtdChamadas += qtdChamadas;
			totaisDados.qtdRetidas += qtdRetidas;
			totaisDados.qtdRepetidas += qtdRepetidas;
            
        }
		
		function executaQuery2(columns) {
			
			var data_hora = columns[0].value,
			data_hora_BR = typeof data_hora ==='string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
			aplicacao = obtemNomeAplicacao(columns["capli"].value),
			codregiao = columns["creg"] === undefined ? "" : columns["creg"].value,
			qtdChamadas = +columns["cha"].value,
			qtdRetidas = +columns["ret"].value,
			qtdRepetidas = +columns["rep"].value;
			
			$scope.dados2.push({
				data_hora: data_hora,
                data_hora_BR: data_hora_BR.substring(0,10),
                aplicacao: aplicacao,
                codregiao: codregiao,
                qtdChamadas: qtdChamadas,
				qtdRetidas: qtdRetidas,
                qtdRepetidas: qtdRepetidas

            });
			
			totaisDados2.qtdChamadas += qtdChamadas;
			totaisDados2.qtdRetidas += qtdRetidas;
			totaisDados2.qtdRepetidas += qtdRepetidas;
            
        }
		
		function executaQuery3(columns) {
			
			var data_hora = columns[0].value,
			data_hora_BR = typeof data_hora ==='string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
			aplicacao = obtemNomeAplicacao(columns["capli"].value),
			codregiao = columns["creg"] === undefined ? "" : columns["creg"].value,
			qtdChamadas = +columns["cha"].value,
			qtdRetidas = +columns["ret"].value,
			qtdRepetidas = +columns["rep"].value;
			
			$scope.dados3.push({
				data_hora: data_hora,
                data_hora_BR: data_hora_BR.substring(0,10),
                aplicacao: aplicacao,
                codregiao: codregiao,
                qtdChamadas: qtdChamadas,
				qtdRetidas: qtdRetidas,
                qtdRepetidas: qtdRepetidas

            });
			totaisDados3.qtdChamadas += qtdChamadas;
			totaisDados3.qtdRetidas += qtdRetidas;
			totaisDados3.qtdRepetidas += qtdRepetidas;
            
        }
		/*
		function executaQuery4(columns) {
			
			var data_hora = columns[0].value,
			data_hora_BR = typeof data_hora ==='string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
			aplicacao = obtemNomeAplicacao(columns["capli"].value),			
			qtdChamadas = +columns["cha"].value,
			qtdRetidas = +columns["ret"].value,
			qtdRepetidas = +columns["rep"].value;
			
			$scope.dados4.push({
				data_hora: data_hora,
                data_hora_BR: data_hora_BR.substring(0,10),
                aplicacao: aplicacao,               
                qtdChamadas: qtdChamadas,
				qtdRetidas: qtdRetidas,
                qtdRepetidas: qtdRepetidas

            });
            
        }*/
		
    $scope.totalEfetuadas = {};
    $scope.totalAtendidas = {};
    $scope.totalSucesso = {};
				/* ag grid */
		function executaQuery4(columns){
			
			
			if($scope.pivot){
	
      var grupoQuery = columns['capli'].value;
	  var data_hora = columns['datr'].value;
	  var dataQuery = formataDataString2(formataDataHoraBR(data_hora));
      
      var efetuadasQuery = columns['cha'].value ? columns['cha'].value : "0";
      var atendidasQuery = columns['ret'].value ? columns['ret'].value : "0";
      var sucessoQuery   = columns['rep'].value ? columns['rep'].value : "0";
      
      $scope.dadosQuery.push({
        grupo: grupoQuery,
        data: dataQuery,
        efetuadas: efetuadasQuery,
        atendidas: atendidasQuery,
        sucesso: sucessoQuery
      });

      if ($scope.totalAtendidas[dataQuery]) {
        $scope.totalEfetuadas[dataQuery] += columns['cha'].value;
        $scope.totalAtendidas[dataQuery] += columns['ret'].value;
        $scope.totalSucesso[dataQuery] += columns['rep'].value;
      } else {
        $scope.totalEfetuadas[dataQuery] = columns['cha'].value
        $scope.totalAtendidas[dataQuery] = columns['ret'].value
        $scope.totalSucesso[dataQuery] = columns['rep'].value
      }
			}else{
				var data_hora = columns[0].value,
			data_hora_BR = typeof data_hora ==='string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
			aplicacao = obtemNomeAplicacao(columns["capli"].value),			
			qtdChamadas = +columns["cha"].value,
			qtdRetidas = +columns["ret"].value,
			qtdRepetidas = +columns["rep"].value;
			
			$scope.dados4.push({
				data_hora: data_hora,
                data_hora_BR: data_hora_BR.substring(0,10),
                aplicacao: aplicacao,               
                qtdChamadas: qtdChamadas,
				qtdRetidas: qtdRetidas,
                qtdRepetidas: qtdRepetidas

            });
			
			totaisDados4.qtdChamadas += qtdChamadas;
			totaisDados4.qtdRetidas += qtdRetidas;
			totaisDados4.qtdRepetidas += qtdRepetidas;
			
			
			}
    }
	
	function formataDataString2(valor) {
      var ano = valor.substring(6,10);
      var mes = valor.substring(3,5);
      var dia = valor.substring(0,2);
      return dia + '/' + mes + '/' + ano;
    }
		

    function criaArrayDatas(dados) {
      var seen = {};
      var out = [];
      var len = dados.length;
      var j = 0;
      for(var i = 0; i < len; i++) {
           var item = dados[i].data;
           if(seen[item] !== 1) {
                 seen[item] = 1;
                //  out[j++] = {[item] : {}}
                 out[j++] = item;
           }
      }
      return out;
    }
	/* ag grid */


        
		db.query(stmt, executaQuery, function (err, num_rows) {
			consultas +=num_rows;
			console.log("Executando query-> " + stmt + " " + num_rows);
			$scope.colunas = geraColunas(1);				
			$scope.gridDados.data = $scope.dados;
			$scope.gridDados.columnDefs = $scope.colunas;
			$scope.$apply();
			db.query(stmt2, executaQuery2, function (err, num_rows) {
				consultas +=num_rows;
				console.log("Executando query-> " + stmt2 + " " + num_rows);
				$scope.colunas2 = geraColunas(2);
				$scope.gridDados2.data = $scope.dados2;
				$scope.gridDados2.columnDefs = $scope.colunas2;
				$scope.$apply();
				db.query(stmt3, executaQuery3, function (err, num_rows) {
					consultas +=num_rows;
					console.log("Executando query-> " + stmt3 + " " + num_rows);
					$scope.colunas3 = geraColunas(3);
					$scope.gridDados3.data = $scope.dados3;
					$scope.gridDados3.columnDefs = $scope.colunas3;
					$scope.$apply();
					db.query(stmt4, executaQuery4, function (err, num_rows) {
						consultas +=num_rows;
						console.log("Executando query-> " + stmt4 + " " + num_rows);
						
				if($scope.pivot){		
						
		$scope.arrDatas = criaArrayDatas($scope.dadosQuery);

        for (var i = 0 ; i < $scope.dadosQuery.length ; i++) {
          $scope.objDadosPorData[$scope.arrDatas[i]] = $scope.dadosQuery.filter( function(el) {
            return el.data == $scope.arrDatas[i];
          })
        }
        for (var i = 0 ; i < $scope.grupos.length ; i++) {
          $scope.objDadosPorGrupo[$scope.grupos[i]] = {
            Oferecidas: [],
            Retidas: [],
            Repetidas: []
          }
          for (var data in $scope.objDadosPorData) {
            var filtraGrupoPorData = $scope.objDadosPorData[data].filter(function(el) {
              return el.grupo == $scope.grupos[i];
            })
            if (filtraGrupoPorData[0]) {
              $scope.objDadosPorGrupo[$scope.grupos[i]].Oferecidas.push(filtraGrupoPorData[0]['efetuadas']);
              $scope.objDadosPorGrupo[$scope.grupos[i]].Retidas.push(filtraGrupoPorData[0]['atendidas']);
              $scope.objDadosPorGrupo[$scope.grupos[i]].Repetidas.push(filtraGrupoPorData[0]['sucesso']);
            }
          }
        }

        for( var i = 0 ; i < $scope.arrDatas.length ; i++) {
          $scope.gridDados4ag.columnDefs.push({
            headerName: $scope.arrDatas[i],
            width: 110,
            field: $scope.arrDatas[i],
            cellStyle: function(params) {
              if (params.node.rowIndex % 3 === 0) {
                if (params.node.rowIndex % 2 === 0) {
                  return {
                    'border-top': '#04b2d9 solid 2px',
                    'background': '#f2f2f2',
                    'border-right': '0',
                    'border-left': '0',
                    textAlign: 'center'
                  }
                } else {
                  return {
                    'border-top': '#04b2d9 solid 2px',
                    'background': '#e2e2e2',
                    'border-right': '0',
                    'border-left': '0',
                    textAlign: 'center'
                  }
                }
              } else  if (params.node.rowIndex % 2 === 0) {
                return {
                  borderTop: '#f5f5f5 solid',
                  background: '#f2f2f2',
                  textAlign: 'center',
                  'border-right': '0',
                  'border-left': '0',
                }
              } else {
                return { 
                  borderTop: '#f5f5f5 solid',
                  background: '#e2e2e2',
                  textAlign: 'center',
                  'border-right': '0',
                  'border-left': '0',
                }
              }
            }
          })
        }

        for ( var grupo in $scope.objDadosPorGrupo ) {
          for ( var status in $scope.objDadosPorGrupo[grupo]) {
            var grupoFormatado = status == 'Oferecidas' ? grupo : '';
            var tempObj = {
              grupo: grupoFormatado,
              status: status
            }
            for (var i = 0 ; i < $scope.arrDatas.length ; i++) {
              tempObj[$scope.arrDatas[i]] = $scope.objDadosPorGrupo[grupo][status][i] || 0
            }
            $scope.gridDados4ag.rowData.push(tempObj)
          }
        }

        var rowTotaisEfetuadas = {
          grupo: '',
          status: 'Oferecidas'
        }
        var rowTotaisAtendidas = {
          grupo: 'Total',
          status: 'Retidas'
        }
        var rowTotaisSucesso = {
          grupo: '',
          status: 'Repetidas'
        }

        for ( var dataDeConsulta in $scope.totalAtendidas ) {
          rowTotaisEfetuadas[dataDeConsulta] = $scope.totalEfetuadas[dataDeConsulta];
          rowTotaisAtendidas[dataDeConsulta] = $scope.totalAtendidas[dataDeConsulta];
          rowTotaisSucesso[dataDeConsulta] = $scope.totalSucesso[dataDeConsulta];
        }
        $scope.gridDados4ag.rowData.push(rowTotaisEfetuadas)
        $scope.gridDados4ag.rowData.push(rowTotaisAtendidas)
        $scope.gridDados4ag.rowData.push(rowTotaisSucesso)

        $scope.gridDados4ag.api.setColumnDefs($scope.gridDados4ag.columnDefs);
        $scope.gridDados4ag.api.setRowData($scope.gridDados4ag.rowData)
        $btn_exportar.prop("disabled", false);
					}else{
						
						
						$scope.colunas4 = geraColunas(4);
						$scope.gridDados4.data = $scope.dados4;
						$scope.gridDados4.columnDefs = $scope.colunas4;				
						$scope.$apply();
						
						userLog(stmt4, 'Numero de registros', 2, err);
					}
					
						$scope.dados.push(totaisDados);
						$scope.dados2.push(totaisDados2);
						$scope.dados3.push(totaisDados3);
						$scope.dados4.push(totaisDados4);
						retornaStatusQuery(consultas, $scope);
						$btn_gerar.button('reset');
						
						if(consultas>0){
							$btn_exportar.prop("disabled", false);
						}
					});
				});
			});
		});


        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.repetidas", function () {
            var that = $(this);
            $('tr.repetidas.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });


      

    };


    
    // Exporta planilha XLSX
  /* $scope.exportaXLSX = function () {
    var $btn_exportar = $(this);

    $btn_exportar.button('loading');

    var linhas  = [];
    linhas.push($scope.colunas.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return col.displayName; } 
	}));
    $scope.dados.forEach(function (dado) {
      linhas.push($scope.colunas.filterMap(function (col,index) {
		  if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });
	
	var linhas2  = [];
    linhas2.push($scope.colunas2.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return col.displayName; } 
	}));
    $scope.dados2.forEach(function (dado) {
      linhas2.push($scope.colunas.filterMap(function (col,index) {
		  if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });
	
	var linhas3  = [];
    linhas3.push($scope.colunas3.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return col.displayName; } 
	}));
    $scope.dados3.forEach(function (dado) {
      linhas3.push($scope.colunas3.filterMap(function (col,index) {
		  if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });
	
	var linhas4  = [];
    linhas4.push($scope.colunas4.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) { return col.displayName; } 
	}));
    $scope.dados4.forEach(function (dado) {
      linhas4.push($scope.colunas4.filterMap(function (col,index) {
		  if (col.visible || col.visible === undefined) { return dado[col.field]; } 
	}));
    });
	
	
	var works = [];
	
	if(linhas.length > 0) works.push({ name: "DDD", data: linhas, table: true });
    if(linhas2.length > 0) works.push({ name: "UF", data: linhas2, table: true });
	if(linhas3.length > 0) works.push({ name: "Região", data: linhas3, table: true });
	if(linhas4.length > 0) works.push({ name: "Ura", data: linhas4, table: true });

    var planilha = {
    creator: "Estalo",
    lastModifiedBy: loginUsuario || "Estalo",
    worksheets: works,
    autoFilter: false,
    // Não incluir a linha do título no filtro automático
    dataRows: { first: 1 }
  };

    var xlsx = frames["xlsxjs"].window.xlsx;
    planilha = xlsx(planilha, 'binary');
	
    var caminho = $scope.nomeExportacao + "_" + formataDataHoraMilis(new Date()) + ".xlsx";
    fs.writeFileSync(caminho, planilha.base64, 'base64');
    childProcess.exec(caminho);

    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  }; */
}
CtrlRetencaoLiquida.$inject = ['$scope', '$globals'];
