function CtrlResumoPV($scope, $globals) {

    win.title = "Resumo por Promoção Vigente";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-resumo-promocao-vigente", "Resumo por Promoção Vigente");

    $scope.status_progress_bar = 0;
    $scope.dados = [];
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        sites: [],
        segmentos: []
    };

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-promocao-vigente");
      treeView('#pag-resumo-promocao-vigente #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-promocao-vigente #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-promocao-vigente #ed', 65,'ED','dvED');
      treeView('#pag-resumo-promocao-vigente #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-promocao-vigente #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-promocao-vigente #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-promocao-vigente #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-promocao-vigente #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-promocao-vigente #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-promocao-vigente #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-resumo-promocao-vigente #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-resumo-promocao-vigente #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      //19/03/2014
      componenteDataHora ($scope,$view);


      //ALEX - Carregando filtros
      carregaAplicacoes($view,false,false,$scope);
      carregaSites($view);

      carregaSegmentosPorAplicacao($scope,$view,true);
      //GILBERTO - change de filtros

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-segmento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} segmentos'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-resumo-promocao-vigente");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function(){
                    atualizaInfo($scope,'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            $scope.listaDados.apply(this);
        });



        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-resumo-promocao-vigente");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }

        //28/08/2014 Lógica para pegar novas tabelas consolidadas
        var tabelas = tabelasParticionadas($scope, 'ConsolPromocaoVigente', true);

        $scope.dados = [];
        var stmt = "";

        var datfim = "";
        tabelas.length > 1 ? datfim = "< '2014-09-01'" : datfim = "<= '" + formataData(data_fim) + "'";

        stmt = db.use + "SELECT CASE WHEN (GROUPING(Cod_Promocao) = 1) THEN 'TOTAL' "
            + " ELSE ISNULL(Cod_Promocao, 'UNKNOWN') "
            + " END AS Cod_Promocao, sum(Qtd_Chamadas) as quantidade, "
            + " cast( (sum(Qtd_Chamadas)*100.0/ "
            + " (SELECT sum(Qtd_Chamadas) from ConsolPromocaoVigente "
            + " WHERE 1 = 1 "
            + " AND Dat_Referencia >= '" + formataDataHora(data_ini) + "'"
            + " AND Dat_Referencia " + datfim + " ";
        stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("Cod_Site", filtro_sites, true)
        if (filtro_segmentos != "") {
            stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        } else {
            stmt += " AND Cod_Segmento = '' "
        }
        stmt += " )) as decimal(5,2)) AS perc"

            + " FROM " + db.prefixo + "" + tabelas[0] + ""
            + " WHERE 1 = 1"
            + " AND Dat_Referencia >= '" + formataDataHora(data_ini) + "'"
            + " AND Dat_Referencia " + datfim + " ";
        stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("Cod_Site", filtro_sites, true)
        if (filtro_segmentos != "") {
            stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        } else {
            stmt += " AND Cod_Segmento = ''"
        }
        stmt += " GROUP BY CUBE (Cod_Promocao)"; //ORDER BY quantidade ";

        if (tabelas.nomes.length === 1) {
            stmt += "ORDER BY quantidade";
        }

        if (tabelas.length > 1) {

            stmt += " UNION ALL SELECT CASE WHEN (GROUPING(Cod_Promocao) = 1) THEN 'TOTAL' "
            + " ELSE ISNULL(Cod_Promocao, 'UNKNOWN') "
            + " END AS Cod_Promocao, sum(Qtd_Chamadas) as quantidade, "
            + " cast( (sum(Qtd_Chamadas)*100.0/ "
            + " (SELECT sum(Qtd_Chamadas) from ConsolPromocaoVigente_p "
            + " WHERE 1 = 1 "
            + " AND Dat_Referencia >= '2014-09-01 00:00:00'"
            + " AND Dat_Referencia <= '" + formataDataHora(data_fim) + "'"
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("Cod_Site", filtro_sites, true)
            if (filtro_segmentos != "") {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            } else {
                stmt += " AND Cod_Segmento = '' "
            }
            stmt += " )) as decimal(5,2)) AS perc"

                + " FROM " + db.prefixo + "" + tabelas[1] + ""
                + " WHERE 1 = 1"
                + " AND Dat_Referencia >= '2014-09-01 00:00:00'"
                + " AND Dat_Referencia <= '" + formataDataHora(data_fim) + "'"
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("Cod_Site", filtro_sites, true)
            if (filtro_segmentos != "") {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            } else {
                stmt += " AND Cod_Segmento = ''"
            }
            stmt += " GROUP BY CUBE (Cod_Promocao)"; //ORDER BY quantidade ";
        }

        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
                $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery(columns) {
            //db.query(stmt, function (columns) {

                $scope.query = "";

                var
            codPromocao = columns[0].value,
            qtdChamadas = +columns[1].value,
            //qtdPercentual = ((+columns[1].value * 100) / qtdChamadas).toFixed(2);
            qtdPercentual = +columns[2].value;

                $scope.dados.push({
                    codPromocao: codPromocao,
                    qtdChamadas: qtdChamadas,
                    qtdPercentual: qtdPercentual
                });
                //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-promocao-vigente");
              if($scope.dados.length%1000===0){
                $scope.$apply();
              }

        }

        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
            //console.log("Executando query-> " + stmtCountRows + " " + $globals.numeroDeRegistros);
        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                $btn_gerar.button('reset');
                if(num_rows>0){
                  $btn_exportar.prop("disabled", false);
                }
            });
        //});

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };



     // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='tResumoPV'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = 'tResumoPV.xlsx';



    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'resumoPromocaoVigente_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
            userLog(stmt, 'Exportar XLSX', 2, err)
            //?
          });

    };

}
CtrlResumoPV.$inject = ['$scope', '$globals'];
