function CtrlResumoIC($scope, $globals) {
	
    win.title = "Resumo por Item de Controle";
    $scope.versao = versao;
    $scope.loop = []
    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-resumo-ic", "Resumo por Item de Controle");

    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.colunas = [{
            field: "cod_ic",
            displayName: "Código",
            width: 80,
            pinned: true
        },
        {
            field: "nome_ic",
            displayName: "Descrição",
            width: 300,
            pinned: true
        }
    ];
    
    // $scope.gridDados = {
    //     data: "dados",
    //     columnDefs: "colunas",
    //     enableColumnResize: true,
    //     enablePinning: true
    // };

    $scope.columnDefs = [{
            headerName: 'Código',
            field: 'codIC'
        },
        {
            headerName: 'Descrição',
            field: 'descricao'
        }
    ];

    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    $scope.gridDados = {
        rowSelection: 'single',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        // onRowClicked: criarInstancia,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado foi encontrado.</span>',
        columnDefs: $scope.columnDefs,
        rowData: [],
        localeText: $scope.localeGridText
    };

    $scope.datas = [];
    $scope.totais = {};
    $scope.ordenacao = 'nome_ic';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];
    $scope.iit = false;
    $scope.tlv = false;
    iit = "";
    tlv = "";
    $scope.csv = []
    $scope.csvCabecalho = ["Código","Descrição"]
    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: []
    };

    /*for (var cod_aplicacao in cache.aplicacoes) {
      var apl = cache.aplicacoes[cod_aplicacao];
      if (apl.itens_controle) {
        apl.itens_controle.indice = geraIndice(apl.itens_controle);
      }
    }*/

    $scope.aba = 2;

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-ic");
				
        $scope.gridDados.api.hideOverlay();
		
        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });


        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view, false, false, $scope);
        //carregaAplicacoesComData($view);
        carregaSites($view);
        carregaRegioes($view);
        carregaDDDs($view);
        carregaSegmentosPorAplicacao($scope, $view, true);
		carregaEmpresas($view,true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view);
            //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
        });

        $view.on("change", "select.filtro-regiao", function () {
            carregaDDDsPorRegiao($scope, $view);
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        //Marcar todos
        // Marca todos os sites
        $view.on("click", "#alinkSite", function () {
            marcaTodosIndependente($('.filtro-site'), 'sites')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });

        // Marca todos os DDDs
        $view.on("click", "#alinkDDD", function () {
            marcaTodosIndependente($('.filtro-ddd'), 'ddds')
        });

        // Marca todos os regiões
        $view.on("click", "#alinkReg", function () {
            $view.on("click", "#alinkReg", function () {
                marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
            });
        });
		
		// Marca todos os empresas
		$view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});



        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        //$view.on("change", "select.filtro-op", function () {
        //    Estalo.filtros.filtro_operacoes = $(this).val();
        //});


        //25/05/2015 Alex - Informação sobre consolidação
        delay($(".filtro-aplicacao >.dropdown-menu li a"), function () {
            cache.apls.forEach(function (apl) {
                if (apl.nome === aplFocus) {
                    console.log(apl.codigo);
                    var stmt = db.use + " SELECT 'Aplicação " + apl.codigo + " com registros até',MAX(DatReferencia),'COM DDD' FROM ResumoIC WHERE CodAplicacao = '" + apl.codigo + "' AND DatReferencia > GETDATE() - 2 UNION ALL SELECT 'e ',MAX(DataHora),'SEM DDD' FROM ConsolidaItemControle WHERE Cod_Aplicacao = '" + apl.codigo + "' AND DataHora > GETDATE() - 2";
                    executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
                        console.log(dado)
                    });
                }
            });
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            $scope.gridDados.rowData = [];
            $scope.gridDados.api.showLoadingOverlay();
            $scope.columnDefs = [{
                    headerName: 'Código',
                    field: 'codIC'
                },
                {
                    headerName: 'Descrição',
                    field: 'descricao'
                }
            ];

            limpaProgressBar($scope, "#pag-resumo-ic");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }


            var todas = true;

            obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val(), true);

            var seguir = [];



            function teste() {
                setTimeout(function () {

                    var itens = typeof $('select.filtro-aplicacao').val() === 'string' ? [$('select.filtro-aplicacao').val()] : $('select.filtro-aplicacao').val();

                    for (var i = 0; i < itens.length; i++) {
                        if (cache.aplicacoes[itens[i]].hasOwnProperty('itens_controle')) {
                            if (seguir.indexOf(itens[i]) < 0) seguir.push(itens[i]);
                        }
                    }

                    if (seguir.length >= itens.length) {
                        clearTimeout(teste);
                        console.log("seguir daqui");
                        $scope.listaDados.apply(this);
                    } else {
                        teste();
                    }
                }, 500);
            }
            teste();
            //exibeDataUltAtualAplic();

        });

        function exibeDataUltAtualAplic() {

            //Data da última atualização da aplicação
            stmt = db.use + "SELECT top 1 dataAtualizacao FROM ConsolidaItemControle " +
                "WHERE 1 = 1 "
            //stmt += restringe_consulta("ic.Cod_Aplicacao", filtro_aplicacoes, true);

        }

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        $view.on("click", ".btn-exportarCSV-interface", function() {
            // var params = {
            //     allColumns: true,
            //     fileName: 'export_' + Date.now(),
            //     columnSeparator: ';'
            // }

            // $scope.gridDados.api.exportDataAsCsv(params);

            if ($scope.gridDados.rowData.length > 0) {
                separator = ";"
                colunas = "";
                file_name = tempDir3() + "export_" + Date.now();
                for (i = 0; i < $scope.colunas.length; i++) {
                    colunas = colunas + $scope.columnDefs[i].headerName + separator
                }
                colunas = colunas + "\n"

                for (var i = 0; i < $scope.gridDados.rowData.length; i++) {
                    for (var v in $scope.gridDados.rowData[i]) {
                        colunas = colunas + $scope.gridDados.rowData[i][v] + separator;
                    }
                    colunas = colunas + "\n";
                }
                // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                // }
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'csv', true);
                $scope.csv = [];
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-ic");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');


    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + "" +
            " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
        if (filtro_regioes.length > 0) {
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
        }
		
		if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }
        //if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }


        $scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
        var stmt = "";
        $scope.ICs = [];
        $scope.datas = [];
        $scope.totais = {
            geral: {
                qtd_entrantes: 0
            }
        };

        var tabelas = tabelasParticionadas($scope, 'ConsolidaItemControleD', false);
        var datfim = tabelas.nomes.length > 1 ? "'" + tabelas.data + "'" : "'" + formataDataHora(precisaoHoraFim(data_fim)) + "'";

        var iit = "";
        var tlv = "";
		
		if(filtro_aplicacoes.length === 1 && filtro_aplicacoes[0] === 'TELAUNICAOITOTAL'){
			
				 if(!(				 
				 filtro_sites.length === 0 &&
				 filtro_regioes.length === 0 &&
				 filtro_ddds.length === 0
					  )){
					 alert("Filtro empresa somente para aplicação Tela única Oi Total");
					 retornaStatusQuery(0, $scope, "");
					 $btn_gerar.button('reset');
					 return;
				 }
				 
	    stmt = db.use + "select CodIC,DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0), sum(QtdChamadas)"+
		" from ResumoTelaUnicaICdia"+
		" where 1 = 1"+
		" AND DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'" +
		" AND " + datfim + "";
		stmt += restringe_consulta("CodEmpresa", filtro_empresas, true);
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
		stmt +=" AND CodAplicacao = 'TELAUNICAOITOTAL'"+
		" group by CodIC,DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)"+
		" order by CodIC";
		
		}else{
			
			
			if(!(				 
			filtro_empresas.length === 0)){
				 alert("Filtro empresa somente para aplicação Tela única Oi Total");
				 retornaStatusQuery(0, $scope, "");
				 $btn_gerar.button('reset');
				 return;
			}

			if (filtro_regioes.length > 0 || filtro_ddds.length > 0) {

				if ($scope.iit === false) {
					iit = "- ISNULL(QtdTransfURA, 0)";
				}
				if ($scope.tlv === false) {
					tlv = "- ISNULL(QtdTransfTLV, 0)";
				}
				//console.log("TLV: "+$scope.tlv+" IIT: "+$scope.iit);
				//console.log("Var TLV: "+tlv+ "Var IIT:"+iit);

				var ddds = [];
				if (filtro_ddds.length == 0) {
					filtro_regioes.forEach(function (codigo) {
						ddds = ddds.concat(unique2(cache.ddds.map(function (ddd) {
							if (ddd.regiao === codigo) {
								return ddd.codigo
							}
						})) || []);
					});
				} else {
					ddds = filtro_ddds;
				}

				stmt = db.use + "with cte as (" +
					" SELECT Cod_Aplicacao, Cod_Item" +
					" FROM " + db.prefixo + "ItemDeControle" +
					" WHERE 1 = 1"
				stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
				stmt += " ), cte2 as (" +
					" SELECT distinct CodAplicacao, CodIC as Cod_Item" +
					" FROM " + db.prefixo + "ResumoICDia CI" +
					" LEFT OUTER JOIN UF_DDD as UD on CI.DDD = UD.DDD" +
					" WHERE 1 = 1"
				stmt += " AND DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'" +
					" AND " + datfim + ""
				stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
				stmt += restringe_consulta("CI.ddd", ddds, true);
				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
				}
				stmt += restringe_consulta("CodSite", filtro_sites, true);

				stmt += " )" +
					" select cte.Cod_Item, NULL, 0" +
					" from cte" +
					" left outer join cte2 on cte.Cod_Aplicacao = cte2.CodAplicacao and cte.Cod_Item = cte2.Cod_Item" +
					" where cte2.Cod_Item is null" +
					" UNION ALL" +
					" SELECT CodIC as Cod_Item, DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)," +
					" SUM(QtdChamadas " + iit + tlv + ") AS Qtd" +
					" FROM " + db.prefixo + "ResumoICDia CI" +
					" LEFT OUTER JOIN UF_DDD as UD on CI.DDD = UD.DDD"
				stmt += " WHERE DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'" +
					" AND " + datfim + ""
				stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
				stmt += restringe_consulta("CI.ddd", ddds, true);
				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
				}
				stmt += restringe_consulta("CodSite", filtro_sites, true);
				stmt += " GROUP BY CodIC," +
					" DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)" +
					" order by Cod_Item";
			} else {

				if ($scope.iit === false) {
					iit = "- ISNULL(QtdIIT, 0)";
				}
				if ($scope.tlv === false) {
					tlv = "- ISNULL(QtdTLV, 0)";
				}
				//console.log("TLV: "+$scope.tlv+" IIT: "+$scope.iit);
				//console.log("Var TLV: "+tlv+ "Var IIT:"+iit);

				stmt = db.use + "with cte as (" +
					" SELECT Cod_Aplicacao, Cod_Item" +
					" FROM " + db.prefixo + "ItemDeControle" +
					" WHERE 1 = 1"
				stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
				stmt += " ), cte2 as (" +
					" SELECT distinct Cod_Aplicacao, Cod_ItemDeControle as Cod_Item" +
					" FROM " + db.prefixo + "ConsolidaItemControleD" +
					" WHERE 1 = 1"
				stmt += " AND Cod_ItemDeControle <> '' AND DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'" +
					" AND " + datfim + ""
				stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
				}
				stmt += restringe_consulta("Cod_Site", filtro_sites, true);
				stmt += " )" +
					" select cte.Cod_Item, NULL, 0" +
					" from cte" +
					" left outer join cte2 on cte.Cod_Aplicacao = cte2.Cod_Aplicacao and cte.Cod_Item = cte2.Cod_Item" +
					" where cte2.Cod_Item is null" +
					" UNION ALL" +
					" SELECT Cod_ItemDeControle as Cod_Item, DATEADD(day, DATEDIFF(day, 0, DataHora), 0)," +
					" SUM(Qtd " + iit + tlv + ") AS Qtd" +
					" FROM " + db.prefixo + "ConsolidaItemControleD"
				stmt += " WHERE Cod_ItemDeControle <> '' AND DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'" +
					" AND " + datfim + ""
				stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
				}
				stmt += restringe_consulta("Cod_Site", filtro_sites, true);
				stmt += " GROUP BY Cod_ItemDeControle," +
					" DATEADD(day, DATEDIFF(day, 0, DataHora), 0)" +
					" order by Cod_Item";
			}
		}

        log(stmt);

        function formataData2(data) {
            var y = data.getFullYear(),
                m = data.getMonth() + 1,
                d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery(columns) {
            if ($.isNumeric(columns[0].value) == false) {
                var cod_ic = columns[0].value,
                    data = columns[1].value === null ? null : "_" + formataData2(columns[1].value),
                    data_BR = columns[1].value === null ? null : formataDataBR(columns[1].value),
                    qtd_entrantes = +columns[2].value

                if (data != null) {
                    var d = $scope.datas[data];
                    if (d === undefined) {
                        d = {
                            data: data,
                            data_BR: data_BR
                        };
                        $scope.datas.push(d);
                        $scope.datas[data] = d;
                        $scope.totais[data] = {
                            qtd_entrantes: 0
                        };
                    }
                }

                var dado = $scope.dados[cod_ic];
                if (dado === undefined) {
                    dado = {
                        cod_ic: cod_ic,
                        nome_ic: obtemDescricaoItemControleDasAplicacoes(filtro_aplicacoes, cod_ic),
                        totais: {
                            geral: {
                                qtd_entrantes: 0
                            }
                        }
                    };
                    $scope.dados.push(dado);
                    $scope.dados[cod_ic] = dado;
                }

                if (data != null) {
                    dado.totais[data] = {
                        qtd_entrantes: qtd_entrantes
                    };
                    dado.totais.geral.qtd_entrantes += qtd_entrantes;
                    $scope.totais[data].qtd_entrantes += qtd_entrantes;
                    $scope.totais.geral.qtd_entrantes += qtd_entrantes;
                }

                if ($scope.dados.length % 1000 === 0) {
                    $scope.$apply();
                }
            }
        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
                $scope.gridDados.api.showNoRowsOverlay();
            } else {
                $scope.gridDados.api.hideOverlay();
            }

            $scope.datas.sort(function (a, b) {
                return a.data < b.data ? -1 : 1;
            });

            $scope.datas.forEach(function (d) {
                $scope.columnDefs.push({ headerName: d.data_BR, field: d.data });
            });

            $scope.columnDefs.push({ headerName: 'Total', field: 'dataTotal' });

            $scope.gridDados.api.setColumnDefs($scope.columnDefs);

            // Preencher com zero datas inexistentes em cada IC
            $scope.dados.forEach(function (dado) {
                linhaCSV = [dado.cod_ic, dado.nome_ic.replace(/(\n|\r)/g,' ').replace(/\&.+\;/g, '')]
                rowData = { codIC: dado.cod_ic, descricao: dado.nome_ic.replace(/(\n|\r)/g,' ').replace(/\&.+\;/g, '') }

                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = {
                            qtd_entrantes: 0
                        };
                        linhaCSV.push(0);
                        rowData[d.data] = 0;
                    } else {
                        linhaCSV.push(dado.totais[d.data].qtd_entrantes);
                        rowData[d.data] = dado.totais[d.data].qtd_entrantes;
                    }
                })
                linhaCSV.push(dado.totais.geral.qtd_entrantes);
                rowData['dataTotal'] = dado.totais.geral.qtd_entrantes;
                $scope.csv.push(linhaCSV);
                $scope.gridDados.rowData.push(rowData);
            });
            linhaCSVTotais = ['', 'TOTAL']

            $scope.gridDados.api.setRowData($scope.gridDados.rowData);
            $scope.gridDados.api.refreshCells({force: true});

            $scope.datas.forEach(function (d) {
                if ($scope.csvCabecalho.indexOf(d.data_BR) == -1){
                    $scope.csvCabecalho.push(d.data_BR)
                }
                $scope.colunas.push({
                    field: "totais." + d.data + ".qtd_entrantes",
                    displayName: d.data_BR,
                    width: 100
                });
                linhaCSVTotais.push($scope.totais[d.data].qtd_entrantes)
            });
            linhaCSVTotais.push($scope.totais.geral.qtd_entrantes)
            $scope.csv.push(linhaCSVTotais)
            $scope.colunas.push({
                field: "totais.geral.qtd_entrantes",
                displayName: "TOTAL",
                width: 100
            });
            $scope.dados.push({
                cod_ic: "",
                nome_ic: "TOTAL",
                totais: $scope.totais
            });
            if ($scope.csvCabecalho.indexOf("TOTAL") == -1){
                $scope.csvCabecalho.push("TOTAL")
            }
            $scope.$apply();
        });

        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

        $btn_gerar.button('reset');
        $btn_exportar.prop("disabled", false);
        $btn_exportar_csv.prop("disabled", false);
        $btn_exportar_dropdown.prop("disabled", false);
        return
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho = $scope.datas.map(function (d) {
            return {
                value: d.data_BR,
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            };
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({
            value: "Item de controle",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        // Inserir última coluna: total por linha
        cabecalho.push({
            value: "Total",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        var linhas = $scope.dados.map(function (dado) {
            var linha = $scope.datas.map(function (d) {
                try {
                    return {
                        value: dado.totais[d.data].qtd_entrantes || 0
                    };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            linha.unshift({
                value: dado.cod_ic + " - " + dado.nome_ic
            });

            // Inserir última coluna: total por linha
            linha.push({
                value: dado.totais.geral.qtd_entrantes
            });

            return linha;
        });

        // Criar última linha, com os totais por data
        var linha_totais = $scope.datas.map(function (d) {
            try {
                return {
                    value: $scope.totais[d.data].qtd_entrantes || 0
                };
            } catch (ex) {
                return 0;
            }
        });

        // Inserir primeira coluna
        linha_totais.unshift({
            value: "Total",
            bold: 1
        });

        // Inserir última coluna: total geral
        linha_totais.push({
            value: $scope.totais.geral.qtd_entrantes
        });

        // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);

        // Inserir última linha: totais por data
        linhas.push(linha_totais);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: 'Resumo por IC',
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoItemControle_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

    $scope.abreAjuda = function (link) {
        abreAjuda(link);
    }

}
CtrlResumoIC.$inject = ['$scope', '$globals'];