function CtrlInteratividadeTelaUnica ($scope, $globals) {

    win.title="Detalhamento de Interatividade - Tela Única"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
    //AlteraJovem

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-interatividade-telaunica", "Detalhamento de Interatividade - Tela Única");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.csv = [];
    $scope.vendas = [];
    $scope.datasComDados = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;
    $scope.total_geral;
    $scope.total_der;
    $scope.total_transf;
    $scope.total_naoatend;
    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    //$scope.aplicacoes = []; // FIXME: copy
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    // Filtros
    /*$scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        //porDia: false
    };*/
    
    $scope.valor = "DataHora_Inicio";
    function geraColunas(){
      var array = [];


     array.push(
        { field: "DataHora_Inicio", displayName: "Data", width: "25%", pinned: true },
        { field: "operador", displayName: "Operador", width: "15%", pinned: true },
        { field: "tratado", displayName: "Tratado", width: "15%", pinned: true },
        //{ field: "dataAjuste", displayName: "Data ajuste", width: "15%", pinned: true },
        { field: "valor", displayName: "Valor", width: "15%", pinned: true },
        { field: "la", displayName: "Nº LA", width: "15%", pinned: true }
		);

      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2018, 1, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


	  $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-interatividade-telaunica");
        $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});
        $(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
        $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
        $('.navbar-inner').css('height','70px');
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

        //minuteStep: 5

        //19/03/2014
        componenteDataHora ($scope,$view);
        /*carregaAplicacoes($view,false,false,$scope);
        carregaSites($view);
        carregaOperacoesECH($view);

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });
		
        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });       
		
        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });
        
        
		 $view.find("select.filtro-tipoplano").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Plano'
        });

      // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});
        
        
        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});
        
        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });
		
		
		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });       */


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          //$scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-interatividade-telaunica");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-interatividade-telaunica");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            
            $scope.listaDados.apply(this);
        });
        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };
    
    // Lista chamadas conforme filtros
    $scope.listaDados = function () {
        $scope.dados = [];
        $scope.csv = [];
		$scope.datasComDados = [];
        $globals.numeroDeRegistros = 0;
        
        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
		
    

        
    
        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        
			stmt = db.use
			+ " select DataHora, Usuario, NumANI,DataAjuste,Valor,la" 
            + " from telaunicainteratividade"			
			+ " WHERE 1 = 1"
			+ " AND DataHora >= '" + formataDataHora(data_ini) + "'"
			+ " AND DataHora <= '" + formataDataHora(data_fim) + "'"			
			+ " ORDER BY DataHora asc";
		  
		log(stmt);
		var stmtCountRows = stmtContaLinhasInteratividade(stmt);
		
		//24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			console.log("Contando linhas "+columns[0].value+"...");
			$scope.datasComDados.push(columns[1].value);
			$globals.numeroDeRegistros = columns[0].value;
		}
		
		function executaQuery(columns) {
			var data_hora = columns["DataHora"].value,
			DataHora_Inicio = typeof columns[0].value==='string' ? formataDataHoraBR(data_hora) : formataDataHoraBR(data_hora);
			var operador = columns["Usuario"].value,
				valor = columns["Valor"].value,
				//dataAjuste = columns["DataAjuste"].value,
				la = columns["la"].value,
				tratado = columns["NumANI"].value;
			
			
			$scope.dados.push({
				data_hora: data_hora,
				DataHora_Inicio: DataHora_Inicio,
				operador: operador,
				tratado: tratado,
				//dataAjuste: dataAjuste,
				valor: valor,
				la: la
            });
            
            $scope.csv.push([
                DataHora_Inicio,
				operador,
				tratado,
				//dataAjuste: dataAjuste,
				valor,
				la
            ]);

	
    }
		
		
		  
		//13/06/2014 Query de 5 em 5 minutos
		var stmts = arrayHorasQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);
		
		//13/06/2014 Data final do penúltimo array com base na hora final do filtro, último array é uma flag
		stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/DataHora <= \'.+\' /g,"DataHora <= \'"  +formataDataHora(new Date(data_fim))+"\' ")];
		
		var controle = 0;
		var total = 0;
		
		function proximaHora(stmt){
			var pularQuery = false;
			/*for (var i = 0; i < datasComDados.length; i++){
				var d = new RegExp ('DataHora_Inicio >= \''+datasComDados[i]+'(.*?)\'');
				if(stmt.match(d) === null){
					pularQuery = true;
				}else{
					pularQuery = false;
					datasComDados.splice(0,i-1);
					break;
				}
			}*/
			
			function comOuSemDados(err,c){
				$('.notification .ng-binding').text("Aguarde... "+atualizaProgressExtrator(controle,stmts.length)+ " Encontrados: "+total).selectpicker('refresh');
				controle++;
				$scope.$apply();

				//Executa dbquery enquanto array de querys menor que length-1
				if(controle < stmts.length - 1){
					proximaHora(stmts[controle].toString());
				}
				
				//Evento fim
				if(controle === stmts.length - 1){
					if(c === undefined) console.log("fim " + controle);
					if(err){
						retornaStatusQuery(undefined, $scope);
					}else{
						retornaStatusQuery($scope.dados.length, $scope);
					}
					
					$btn_gerar.button('reset');
					if($scope.dados.length > 0){
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
					}else{
						$btn_exportar.prop("disabled", true);
                        $btn_exportar_csv.prop("disabled", true);
                        $btn_exportar_dropdown.prop("disabled", true);
					}
				}
			}
			
			if(pularQuery){
				comOuSemDados(false,"console");
			}else{
				db.query(stmt, executaQuery, function (err, num_rows) {
					console.log("Executando query-> " + stmt + " " + num_rows);
					num_rows !== undefined ? total += num_rows : total += 0;
					comOuSemDados(err);
					$scope.$apply();
				});
			}
		}
		
		/*db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
			console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);
			if($globals.numeroDeRegistros === 0){
				retornaStatusQuery(0, $scope);
				$btn_gerar.button('reset');
			}else{*/
				//Disparo inicial
				console.log("Primeira query");
				proximaHora(stmts[0].toString());
				$scope.colunas = geraColunas();
			/*}
		});*/
		   

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }
		
		

         
				// GILBERTOOOOOO 17/03/2014
				$view.on("mouseup", "tr.resumo", function () {
					var that = $(this);
					$('tr.resumo.marcado').toggleClass('marcado');
					$scope.$apply(function () {
						that.toggleClass('marcado');
					});
				});
    };




	   
	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		  var $btn_exportar = $(this);

		  $btn_exportar.button('loading');

		  var linhas  = [];


		  linhas.push([{value:'Data', autoWidth:true}, {value:'Operador', autoWidth:true},{value:'Tratado', autoWidth:true}, 
			{value:'Valor', autoWidth:true},{value:'Nº LA', autoWidth:true}]);
	  //{value:'Data ajuste', autoWidth:true},
		  
		  
			  $scope.dados.map(function (dado) {
          return linhas.push([
            {value:dado.DataHora_Inicio, autoWidth:true},
            {value:dado.operador , autoWidth:true},
            {value:" "+dado.tratado+" " , autoWidth:true},
            //{value:dado.dataAjuste , autoWidth:true},
            {value:dado.valor , autoWidth:true},
            {value:dado.la , autoWidth:true}
          ]);
			  });


			var planilha = {
				creator: "Estalo",
				lastModifiedBy: $scope.username || "Estalo",
				worksheets: [{ name: 'Detalhamento de Interatividade - Tela Única', data: linhas, table: true }],
				autoFilter: false,
				// Não incluir a linha do título no filtro automático
				dataRows: { first: 1 }
			};

			var xlsx = frames["xlsxjs"].window.xlsx;
			planilha = xlsx(planilha, 'binary');


			var milis = new Date();
			var file = 'InteratividadeTelaUnica' + formataDataHoraMilis(milis) + '.xlsx';


			if (!fs.existsSync(file)) {
				fs.writeFileSync(file, planilha.base64, 'base64');
				childProcess.exec(file);
			}

			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);
		};

}

CtrlInteratividadeTelaUnica .$inject = ['$scope', '$globals'];
