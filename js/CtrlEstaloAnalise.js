/*
 *
 */
function CtrlEstaloAnalise($scope, $globals) {

    win.title = "Ferramenta analítica do Estalo"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 5000;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoED"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-estalo-analise", "Ferramenta analítica do Estalo");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];

    $scope.decrescente = true;


    $scope.log = [];



    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };




    $scope.aba = 2;

    function geraColunas() {
        var array = [];


        array.push(
          { field: "data", displayName: "Data", width: 150, pinned: false},
          { field: "login", displayName: "Login", width: 120, pinned: false },
          { field: "dominio", displayName: "Domínio", width: 120, pinned: false },
          { field: "query", displayName: "Query", width: 217, pinned: false },
          { field: "relatorio", displayName: "Relatório", width: 205, pinned: false },
          { field: "nome", displayName: "Nome", width: 237, pinned: false }

        );
        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = agora;

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-estalo-analise");
      treeView('#pag-estalo-analise #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-estalo-analise #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-estalo-analise #ed', 65,'ED','dvED');
      treeView('#pag-estalo-analise #ic', 95,'IC','dvIC');
      treeView('#pag-estalo-analise #tid', 115,'TID','dvTid');
      treeView('#pag-estalo-analise #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-estalo-analise #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-estalo-analise #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-estalo-analise #parametros', 225,'Parametros','dvParam');
      treeView('#pag-estalo-analise #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-estalo-analise #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-estalo-analise #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/

        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        //minuteStep: 5

        //Alex 21/02/2014 datetime picker data separada

        //19/03/2014
        componenteDataMaisHora($scope, $view);

        carregaUsuarios($view);
        carregaRelatorios($view);
        carregaDominios($view);


        $view.find("select.filtro-usuario").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} usuários',
            showSubtext: true
        });

        $view.find("select.filtro-relatorio").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} relatórios',
            showSubtext: true
        });

        $view.find("select.filtro-dominio").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} domínios',
            showSubtext: true
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-estalo-analise");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-estalo-analise");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        $globals.numeroDeRegistros = 0;
        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;


        var filtro_usuarios = $view.find("select.filtro-usuario").val() || [];
        var filtro_relatorios = $view.find("select.filtro-relatorio").val() || [];
        var filtro_dominios = $view.find("select.filtro-dominio").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);


        $scope.dados = [];

        var tabela = "logestalo";

        stmt = db.use + '';



        stmt += " Select datahora, login, u.dominio, query, relatorio, Nome"
        + " FROM " + db.prefixo + tabela + " l"
        + " left outer join UsuariosEstatura u"
        + " on l.login = u.LoginUsuario"
        + " WHERE 1 = 1"
        + " AND CONVERT(DATE,datahora) >= '" + formataData(data_ini) + "'"
        + " AND CONVERT(DATE,datahora) <= '" + formataData(data_fim) + "'";
        stmt += restringe_consulta("login",filtro_usuarios,true);
        stmt += restringe_consulta("relatorio",filtro_relatorios,true);
        stmt += restringe_consulta("u.dominio",filtro_dominios,true);
        stmt += " ORDER BY datahora";


        //log(stmt);




        function executaQuery(columns) {


            var data = formataDataHoraBR(columns["datahora"].value),
                login = columns["login"].value,
                dominio = columns["dominio"].value,
                query = columns["query"].value,
                relatorio = columns["relatorio"].value,
                nome = columns["Nome"].value;

            $scope.dados.push({
                data: data,
                login: login,
                dominio: dominio,
                query: query,
                relatorio: relatorio,
                nome: nome
            });
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
            //if ($scope.dados.length % 1000 === 0) {
            //    $scope.$apply();
            //}
        }



            db.query(stmt, executaQuery, function (err, num_rows) {

                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                //$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                    $btn_exportar.prop("disabled", false);
                }
            });

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.estado_de", function () {
            var that = $(this);
            $('tr.estado_de.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };


}
CtrlEstaloAnalise.$inject = ['$scope', '$globals'];
