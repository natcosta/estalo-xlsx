function CtrlAjuda($scope, $globals,$location) {
  
            // pega base Url
            $scope.setaDireita = '<i class="icon-chevron-right"></i>';
            $scope.baseUrl = base == "" ? "http://www.versatec.com.br/oi3/" : base;
            var linkNumero = location.hash;
            var linkNumero = linkNumero.split("=");
            // funcao que troca o active (azul) dos links
            $scope.trocaAtivo = function (x) {
                for (i = 0; i < $scope.objLinks.length; i++) {
                    document.getElementById("manual" + i).className = "";
                }
                document.getElementById("manual" + x).className = "active";
                document.getElementById("iframeDireita").src = $scope.baseUrl + "pdfjs/web/files/" + $scope.objLinks[x].link + "/" + $scope.objLinks[x].link + ".html";
            }
            window.addEventListener("resize", maxBool);
            //Função que verifica se a janela está maximizada ou não.
            function maxBool() {
                console.log(window.outerWidth);
                if (window.outerWidth <= 1180) {
                    $scope.setaDireita = '';
                    $scope.$apply();
                } else {
                    //Maximizada
                    $scope.setaDireita = '<i class="icon-chevron-right"></i>';
                    $scope.$apply();
                }
            }
            // Obj com Links de Manuais
            $scope.objLinks = [{
                    nome: "Detalhamento de chamadas",
                    link: "Detalhamentodechamadas_v4.doc"
                }, {
                    nome: "Resumo dia a dia",
                    link: "Resumodiaadia_V3.doc"
                },
                {
                    nome: "Resumo hora a hora",
                    link: "ResumohoraahoraV3.doc"
                },
                {
                    nome: "Resumo por estados de dialogo",
                    link: "Resumo_por_Estados_de_Dialogo.doc"
                },
                {
                    nome: "Resumo por item de controle",
                    link: "Resumo_por_Item_De_Controle.doc"
                },
                {
                    nome: "Resumo por transfer ID",
                    link: "Resumo_por_TransferID.doc"
                },
                {
                    nome: "Telas de cadastro de Prompt & IC",
                    link: "Cadastro de Prompts e ICs.doc"
                },
                {
                    nome: "Cadastro de Usuários",
                    link: "Cadastro de Usuário.doc"
                }
            ];
            // troca de source no iframe
            $scope.abreIframe = function (link) {
                document.getElementById("iframeDireita").src = $scope.baseUrl + "pdfjs/web/files/" + link + "/" + link + ".html";
            }
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    setTimeout(function(){
                        try{
                            location.hash.split("?")[1].split("=")[1];
                            $scope.trocaAtivo(location.hash.split("?")[1].split("=")[1]);
                        }
                        catch(err){
                            console.log(err);
                            $scope.trocaAtivo(0);
                        }
                       
                   
                    },0)
           
        })
    })

}
CtrlAjuda.$inject = ['$scope', '$globals'];