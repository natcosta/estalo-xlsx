/*
 * CtrlDerivacoesRUD
 */
function CtrlDerivacoesRUD($scope, $globals) {



	win.title = "Resumo de repetidas por último dígito"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

	//$scope.datsUltMesResR = pad(dataUltimaConsolidacao("ResumoR").getMonth() + 1) + "/" + dataUltimaConsolidacao("ResumoR").getFullYear();
	//$scope.datsUltMesResRFull = formataDataBR(dataUltimaConsolidacao("ResumoR"));

	//Alex 08/02/2014
	travaBotaoFiltro(0, $scope, "#pag-derivacoes-repetidas-finaltel", "Resumo de repetidas por último dígito");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	$scope.dados = [];
	$scope.csv = [];

	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		headerRowTemplate: 'headerRow',
		enableColumnResize: false,
		enablePinning: true
	};


	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;

	$scope.log = [];

	$scope.aplicacoes = cache.apls; // FIXME: copy
	$scope.sites = [];
	$scope.segmentos = [];

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		sites: [],
		segmentos: [],
		chamador: ""
	};

	$scope.tabela = "";
	$scope.valor = "24h";
	$scope.agrupar = false;
	$scope.perc = false;



	function geraColunas() {
		var array = [];


		//,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'

		array.push({
			field: "data_hora_BR",
			displayName: "Data",
			width: 130,
			pinned: true
		}, {
			field: "aplicacao",
			displayName: "Aplicação",
			width: 130,
			pinned: true
		});

		if (!$scope.agrupar) array.push({
			field: "codregiao",
			displayName: "Região",
			width: 130,
			pinned: true
		});

		array.push({
			field: "finaltel",
			displayName: "Último dígito",
			width: 100,
			pinned: true
		}, {
			field: "qtdClientes",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false
		}, {
			field: "qtdChamadas",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdChamadasFinal",
			displayName: "Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});

		if ($scope.perc) array.push({
			field: "percQtdChamadasFinal",
			displayName: "% Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});

		array.push({
			field: "qtdChamadasDeriv",
			displayName: "Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});

		if ($scope.perc) array.push({
			field: "percQtdChamadasDeriv",
			displayName: "% Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdChamadasAband",
			displayName: "Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdChamadasAband",
			displayName: "% Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});

		array.push({
			field: "qtdRepetidas",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		array.push({
			field: "qtdRepetidasFinal",
			displayName: "Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,

			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepetidasFinal",
			displayName: "% Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRepetidasDeriv",
			displayName: "Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepetidasDeriv",
			displayName: "% Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRepetidasAband",
			displayName: "Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepetidasAband",
			displayName: "% Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});

		array.push({
			field: "qtdDerivadas",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		array.push({
			field: "qtdDerivadasFinal",
			displayName: "Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdDerivadasFinal",
			displayName: "% Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdDerivadasDeriv",
			displayName: "Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdDerivadasDeriv",
			displayName: "% Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdDerivadasAband",
			displayName: "Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdDerivadasAband",
			displayName: "% Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});

		array.push({
			field: "qtdRepAposDeriv",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		array.push({
			field: "qtdRepAposDerivFinal",
			displayName: "Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepAposDerivFinal",
			displayName: "% Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRepAposDerivDeriv",
			displayName: "Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepAposDerivDeriv",
			displayName: "% Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRepAposDerivAband",
			displayName: "Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRepAposDerivAband",
			displayName: "% Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRederivadas",
			displayName: "Total",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		array.push({
			field: "qtdRederivadasFinal",
			displayName: "Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRederivadasFinal",
			displayName: "% Final.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRederivadasDeriv",
			displayName: "Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRederivadasDeriv",
			displayName: "% Deriv.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});
		array.push({
			field: "qtdRederivadasAband",
			displayName: "Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});
		if ($scope.perc) array.push({
			field: "percQtdRederivadasAband",
			displayName: "% Aband.",
			width: 100,
			cellClass: "grid-align",
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		});

		return array;

	}

	$scope.aba = 2;

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: mesAnterior(dat_consoli),
		max: dat_consoli*/
	};

	var $view;

	$scope.$on('$viewContentLoaded', function () {


		$view = $("#pag-derivacoes-repetidas-finaltel");

		$(".botoes").css({
			'position': 'fixed',
			'left': 'auto',
			'right': '25px',
			'margin-top': '35px'
		});


		componenteDataHora($scope, $view);

		//ALEX - Carregando filtros
		carregaAplicacoes($view, false, false, $scope);

		carregaDigitos($view, true);

		carregaSegmentosPorAplicacao($scope, $view, true);
		//GILBERTO - change de filtros

		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view)
		});


		$view.on("change", "select.filtro-aplicacao", function () {
			var arr = $(this).val();
			if (arr !== null && arr[0] === 'NOVAOITV') {
				$('.filtro-chamador').prop('disabled', false);
			} else {
				$('.filtro-chamador').prop('disabled', 'disabled');
			}
		});


		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-ud").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} último dígito',
			showSubtext: true
		});


		//2014-11-27 transição de abas
		var abas = [2, 3, 4, 5];

		$view.on("click", "#alinkAnt", function () {

			if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
			if ($scope.aba > abas[0]) {
				$scope.aba--;
				mudancaDeAba();
			}
		});

		$view.on("click", "#alinkPro", function () {

			if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

			if ($scope.aba < abas[abas.length - 1]) {
				$scope.aba++;
				mudancaDeAba();
			}

		});

		function mudancaDeAba() {
			abas.forEach(function (a) {
				if ($scope.aba === a) {
					$('.nav.aba' + a + '').fadeIn(500);
				} else {
					$('.nav.aba' + a + '').css('display', 'none');
				}
			});
		}


		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});

		// Marca todos os uds
		$view.on("click", "#alinkUltDig", function () {
			marcaTodosIndependente($('.filtro-ud'), 'uds')
		});



		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
			$("div.data-inicio.input-append.date").data("datetimepicker").hide();
			$("div.data-fim.input-append.date").data("datetimepicker").hide();
			$('div.btn-group.filtro-aplicacao').addClass('open');
			$('div.btn-group.filtro-aplicacao>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '400px'
			});

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
			$('div.btn-group.filtro-aplicacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ud').mouseover(function () {
			$('div.btn-group.filtro-ud').addClass('open');
			$('div.btn-group.filtro-ud>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '400px'
			});
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ud').mouseout(function () {
			$('div.btn-group.filtro-ud').removeClass('open');
		});


		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {
			limpaProgressBar($scope, "#pag-derivacoes-repetidas-finaltel");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}

			/*//22/03/2014 Testa se uma aplicação foi selecionada
			var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
			if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
			    setTimeout(function(){
			        atualizaInfo($scope,'Selecione uma aplicação');
			        effectNotification();
			        $view.find(".btn-gerar").button('reset');
			    },500);
			    return;
			}*/

			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);


			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		/*$view.on("change", "#24h", function (){
		  $('#pordia').attr('disabled',false);
		});

		 $view.on("change", "#2h", function (){
		   $('#pordia').attr('disabled','disabled');
		   $('#pordia').prop('checked',false);
		 });

		 $view.on("change", "#1h", function (){
		   $('#pordia').attr('disabled','disabled');
		   $('#pordia').prop('checked',false);
		 });*/


		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-derivacoes-repetidas-finaltel");
		}

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});

	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};




	// Lista chamadas conforme filtros
	$scope.listaDados = function () {

		//var pordia = $('#pordia').prop('checked');
		//Teste
		var pordia = true;
		//var porcpf = $('#porcpf').prop('checked');
		//Teste
		var porcpf = false;



		porcpf ? $scope.tabela = "CPF" : $scope.tabela = "";


		var sufixo = $scope.valor + $scope.tabela;
		console.log("Por CPF: " + porcpf + " Por dia: " + pordia + " Sufixo tabela: " + sufixo);


		$globals.numeroDeRegistros = 0;
		//if (!connection) {
		//  db.connect(config, $scope.listaChamadas);
		//  return;
		//}

		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');


		var filtro_chamadores = $scope.filtros.chamador;
		if (filtro_chamadores === "x") filtro_chamadores = "";
		if (filtro_chamadores.match(/^\d*x\d*$/)) {
			filtro_chamadores = filtro_chamadores.replace("x", "%");
		}
		filtro_chamadores = filtro_chamadores !== "" ? [filtro_chamadores] : [];

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_ud = $view.find("select.filtro-ud").val() || [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
			" até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_aplicacoes.length > 0) {
			$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		}
		if (filtro_ud.length > 0) {
			$scope.filtros_usados += " UDs: " + filtro_ud;
		}

		if (filtro_chamadores != "") {
			$scope.filtros_usados += (
				$('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? " Chamador: " :
				$('.filtro-chamador').attr('placeholder') === 'Contrato' ? " Contrato: " :
				" CPF/CNPJ: "
			) + filtro_chamadores;
		}

		$scope.dados = [];
		$scope.csv = [];

		var stmt = db.use;
		//teste
		//sufixo = "";
		if (pordia === false) {

			stmt += "Select DatReferencia as datr, CodAplicacao as capli, "
			if (!$scope.agrupar) stmt += "CodRegiao as creg , "
			stmt += "TR.FinalTel  as ft, QtdClientes as qc, "
			stmt += "QtdChamadas as cha, QtdFinalizadas as chaf, QtdDerivadas as chad, QtdAbandonadas as chaa, "
			stmt += "QtdRepetidas as rep, QtdRepFinal as repf, QtdRepDeriv as repd, QtdRepAband as repa, "
			stmt += "QtdRepAposFinal as reaf, QtdFinalAposFinal as reaff, QtdDerivAposFinal as reafd, QtdAbandAposFinal as reafa, "
			stmt += "QtdRepAposDeriv as rea, QtdFinalAposDeriv as readf, QtdDerivAposDeriv as readd, QtdAbandAposDeriv as reada, "
			stmt += "QtdRepAposAband as reab, QtdFinalAposAband as reabf, QtdDerivAposAband as reabd, QtdAbandAposAband as reaba " +
				" FROM " + db.prefixo + "TotaisRepetidas" + sufixo + "FinalTel" +
				" WHERE 1 = 1" +
				" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
				" AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
			stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
			stmt += " order by DatReferencia";



		} else {

			/*stmt += " with tcu as ("
			  + "   SELECT DatReferencia, codAplicacao,"
			  + "      SUM(QtdClientes) as qc, SUM(QtdClientesDerivados) as qcd"
			  + "   FROM " + db.prefixo + "TotaisClientesUnicos"
			  + "   WHERE 1 = 1"
			  + "     AND DatReferencia >= '" + formataData(data_ini) + "'"
			  + "     AND DatReferencia <= '" + formataData(data_fim) + "'"
			if (filtro_aplicacoes.length === 1) {
			  stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
			}
			  stmt += "   GROUP BY DatReferencia, codAplicacao"
			  + " )"*/


			stmt += "Select CONVERT(DATE,TR.DatReferencia) as datr, TR.CodAplicacao as capli, "
			if (!$scope.agrupar) stmt += "TR.CodRegiao as creg , "
			stmt += "TR.FinalTel  as ft, TR.QtdClientes as qc, "
			stmt += "SUM(TR.QtdChamadas) as cha, SUM(TR.QtdFinalizadas) as chaf, SUM(TR.QtdDerivadas) as chad, SUM(TR.QtdAbandonadas) as chaa, "
			stmt += "SUM(TR.QtdRepetidas) as rep, SUM(TR.QtdRepFinal) as repf, SUM(TR.QtdRepDeriv) as repd, SUM(TR.QtdRepAband) as repa, "
			stmt += "SUM(TR.QtdRepAposFinal) as reaf, SUM(TR.QtdFinalAposFinal) as reaff, SUM(TR.QtdDerivAposFinal) as reafd, SUM(TR.QtdAbandAposFinal) as reafa, "
			stmt += "SUM(TR.QtdRepAposDeriv) as rea, SUM(TR.QtdFinalAposDeriv) as readf, SUM(TR.QtdDerivAposDeriv) as readd, SUM(TR.QtdAbandAposDeriv) as reada, "
			stmt += "SUM(TR.QtdRepAposAband) as reab, SUM(TR.QtdFinalAposAband) as reabf, SUM(TR.QtdDerivAposAband) as reabd, SUM(TR.QtdAbandAposAband) as reaba "


				+
				" FROM " + db.prefixo + "TotaisRepetidas" + sufixo + "FinalTel as TR"

			/*stmt += " left outer join tcu  on CONVERT(DATE,TR.DatReferencia) = tcu.DatReferencia"
			stmt += " AND TR.CodAplicacao =  tcu.CodAplicacao"*/
			stmt += " WHERE 1 = 1"
				//stmt +=  $scope.agrupar === true ? "=" : "<>"
				+
				" AND CONVERT(DATE,TR.DatReferencia) >= '" + formataData(data_ini) + "'" +
				" AND CONVERT(DATE,TR.DatReferencia) <= '" + formataData(data_fim) + "'"
			stmt += restringe_consulta("TR.CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("TR.FinalTel", filtro_ud, true)
			stmt += " GROUP BY CONVERT(DATE,TR.DatReferencia),TR.CodAplicacao, "
			if (!$scope.agrupar) stmt += "TR.CodRegiao,"
			stmt += "TR.FinalTel"
			stmt += ",TR.QtdClientes"
			stmt += " ORDER BY CONVERT(DATE,TR.DatReferencia)";


		}

		log(stmt);


		var stmtCountRows = stmtContaLinhas(stmt);


		// Alex 24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			$globals.numeroDeRegistros = columns[0].value;
		}

		function executaQuery(columns) {
			//db.query(stmt, function (columns) {



			var data_hora = columns[0].value,
				data_hora_BR = typeof data_hora === 'string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
				aplicacao = obtemNomeAplicacao(columns["capli"].value),
				codregiao = columns["creg"] === undefined ? "" : columns["creg"].value,
				finaltel = columns["ft"].value,
				qtdClientes = columns["qc"] === undefined ? "NA" : columns["qc"].value === null ? "ND" : +columns["qc"].value,


				qtdChamadas = +columns["cha"].value,
				qtdChamadasFinal = +columns["chaf"].value,
				percQtdChamadasFinal = qtdChamadasFinal === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasFinal / qtdChamadas).toFixed(2)),
				qtdChamadasDeriv = +columns["chad"].value,
				percQtdChamadasDeriv = qtdChamadasDeriv === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasDeriv / qtdChamadas).toFixed(2)),
				qtdChamadasAband = +columns["chaa"].value,
				percQtdChamadasAband = qtdChamadasAband === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasAband / qtdChamadas).toFixed(2)),


				qtdRepetidas = +columns["rep"].value,
				qtdRepetidasFinal = +columns["repf"].value,
				percQtdRepetidasFinal = qtdRepetidasFinal === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasFinal / qtdRepetidas).toFixed(2)),
				qtdRepetidasDeriv = +columns["repd"].value,
				percQtdRepetidasDeriv = qtdRepetidasFinal === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasDeriv / qtdRepetidas).toFixed(2)),
				qtdRepetidasAband = +columns["repa"].value,
				percQtdRepetidasAband = qtdRepetidasAband === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasAband / qtdRepetidas).toFixed(2)),

				qtdDerivadas = +columns["reaf"].value,
				qtdDerivadasFinal = +columns["reaff"].value,
				percQtdDerivadasFinal = qtdDerivadasFinal === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasFinal / qtdDerivadas).toFixed(2)),
				qtdDerivadasDeriv = +columns["reafd"].value,
				percQtdDerivadasDeriv = qtdDerivadasDeriv === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasDeriv / qtdDerivadas).toFixed(2)),
				qtdDerivadasAband = +columns["reafa"].value,
				percQtdDerivadasAband = qtdDerivadasAband === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasAband / qtdDerivadas).toFixed(2)),

				qtdRepAposDeriv = +columns["rea"].value,
				qtdRepAposDerivFinal = +columns["readf"].value,
				percQtdRepAposDerivFinal = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivFinal / qtdRepAposDeriv).toFixed(2)),
				qtdRepAposDerivDeriv = +columns["readd"].value,
				percQtdRepAposDerivDeriv = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivDeriv / qtdRepAposDeriv).toFixed(2)),
				qtdRepAposDerivAband = +columns["reada"].value,
				percQtdRepAposDerivAband = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivAband / qtdRepAposDeriv).toFixed(2)),

				qtdRederivadas = +columns["reab"].value,
				qtdRederivadasFinal = +columns["reabf"].value,
				percQtdRederivadasFinal = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasFinal / qtdRederivadas).toFixed(2)),
				qtdRederivadasDeriv = +columns["reabd"].value,
				percQtdRederivadasDeriv = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasDeriv / qtdRederivadas).toFixed(2)),
				qtdRederivadasAband = +columns["reaba"].value,
				percQtdRederivadasAband = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasAband / qtdRederivadas).toFixed(2));

			var s = {
				data_hora: data_hora,
				data_hora_BR: data_hora_BR.substring(0, 10),
				aplicacao: aplicacao,
				codregiao: codregiao,
				finaltel: finaltel,
				qtdClientes: qtdClientes,


				qtdChamadas: qtdChamadas,
				qtdChamadasFinal: qtdChamadasFinal,
				percQtdChamadasFinal: percQtdChamadasFinal,
				qtdChamadasDeriv: qtdChamadasDeriv,
				percQtdChamadasDeriv: percQtdChamadasDeriv,
				qtdChamadasAband: qtdChamadasAband,
				percQtdChamadasAband: percQtdChamadasAband,

				qtdRepetidas: qtdRepetidas,
				qtdRepetidasFinal: qtdRepetidasFinal,
				percQtdRepetidasFinal: percQtdRepetidasFinal,
				qtdRepetidasDeriv: qtdRepetidasDeriv,
				percQtdRepetidasDeriv: percQtdRepetidasDeriv,
				qtdRepetidasAband: qtdRepetidasAband,
				percQtdRepetidasAband: percQtdRepetidasAband,

				qtdDerivadas: qtdDerivadas,
				qtdDerivadasFinal: qtdDerivadasFinal,
				percQtdDerivadasFinal: percQtdDerivadasFinal,
				qtdDerivadasDeriv: qtdDerivadasDeriv,
				percQtdDerivadasDeriv: percQtdDerivadasDeriv,
				qtdDerivadasAband: qtdDerivadasAband,
				percQtdDerivadasAband: percQtdDerivadasAband,

				qtdRepAposDeriv: qtdRepAposDeriv,
				qtdRepAposDerivFinal: qtdRepAposDerivFinal,
				percQtdRepAposDerivFinal: percQtdRepAposDerivFinal,
				qtdRepAposDerivDeriv: qtdRepAposDerivDeriv,
				percQtdRepAposDerivDeriv: percQtdRepAposDerivDeriv,
				qtdRepAposDerivAband: qtdRepAposDerivAband,
				percQtdRepAposDerivAband: percQtdRepAposDerivAband,

				qtdRederivadas: qtdRederivadas,
				qtdRederivadasFinal: qtdRederivadasFinal,
				percQtdRederivadasFinal: percQtdRederivadasFinal,
				qtdRederivadasDeriv: qtdRederivadasDeriv,
				percQtdRederivadasDeriv: percQtdRederivadasDeriv,
				qtdRederivadasAband: qtdRederivadasAband,
				percQtdRederivadasAband: percQtdRederivadasAband,

				temTotaisClientes: typeof qtdClientes === "number"
			}

			$scope.dados.push(s);

			var a = [];
			for (var item in $scope.colunas) {
				a.push(s[$scope.colunas[item].field]);
			}

			$scope.csv.push(a);

			// $scope.csv.push([
			// 	data_hora,
			// 	data_hora_BR.substring(0, 10),
			// 	aplicacao,
			// 	codregiao,
			// 	finaltel,
			// 	qtdClientes,
			// 	qtdChamadas,
			// 	qtdChamadasFinal,
			// 	percQtdChamadasFinal,
			// 	qtdChamadasDeriv,
			// 	percQtdChamadasDeriv,
			// 	qtdChamadasAband,
			// 	percQtdChamadasAband,
			// 	qtdRepetidas,
			// 	qtdRepetidasFinal,
			// 	percQtdRepetidasFinal,
			// 	qtdRepetidasDeriv,
			// 	percQtdRepetidasDeriv,
			// 	qtdRepetidasAband,
			// 	percQtdRepetidasAband,
			// 	qtdDerivadas,
			// 	qtdDerivadasFinal,
			// 	percQtdDerivadasFinal,
			// 	qtdDerivadasDeriv,
			// 	percQtdDerivadasDeriv,
			// 	qtdDerivadasAband,
			// 	percQtdDerivadasAband,
			// 	qtdRepAposDeriv,
			// 	qtdRepAposDerivFinal,
			// 	percQtdRepAposDerivFinal,
			// 	qtdRepAposDerivDeriv,
			// 	percQtdRepAposDerivDeriv,
			// 	qtdRepAposDerivAband,
			// 	percQtdRepAposDerivAband,
			// 	qtdRederivadas,
			// 	qtdRederivadasFinal,
			// 	percQtdRederivadasFinal,
			// 	qtdRederivadasDeriv,
			// 	percQtdRederivadasDeriv,
			// 	qtdRederivadasAband,
			// 	percQtdRederivadasAband,
			// 	typeof qtdClientes === "number"
			// ]);
			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}


		/*,

		// Limpa grid
		function (err, num_rows) {

		retornaStatusQuery(num_rows, $scope);
		$btn_gerar.button('reset');
		$btn_exportar.prop("disabled", false);
		$scope.$apply(function () {
		});
		});*/

		//db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
		//console.log("Executando query-> " + stmtCountRows + " " + $globals.numeroDeRegistros);
		db.query(stmt, executaQuery, function (err, num_rows) {
			console.log("Executando query-> " + stmt + " " + num_rows);
			userLog(stmt, 'Numero de registros', 2, err)
			retornaStatusQuery(num_rows, $scope);
			$btn_gerar.button('reset');

			$('.ngUnPinnedIcon').css('display', 'none');
			$('.ngPinnedIcon').css('display', 'none');
			$('.ngHeaderText').css('background-color', 'rgb(234, 234, 234)');

			if ($scope.agrupar) {
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .a').css({
					'width': '350px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
			} else {
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .a').css({
					'width': '480px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
			}


			if ($scope.perc) {
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .c').css({
					'width': '690px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .d').css({
					'width': '700px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
			} else {
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .c').css({
					'width': '390px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
				$('#pag-derivacoes-repetidas-finaltel .ngHeaderCell .d').css({
					'width': '400px',
					'float': 'left',
					'text-align': 'center',
					'border-style': 'solid',
					'border-color': '#dedede',
					'border-width': '5px'
				});
			}

			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportar_csv.prop("disabled", false);
				$btn_exportar_dropdown.prop("disabled", false);
			}
		});
		//});

		// GILBERTOOOOOO 17/03/2014
		$view.on("mouseup", "tr.repetidas", function () {
			var that = $(this);
			$('tr.repetidas.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});


		//Alex 09/05/2014

		$(".ngViewport").scroll(function () {
			$('.ngUnPinnedIcon').css('display', 'none');
			$('.ngPinnedIcon').css('display', 'none');
			$('.ngHeaderText').css('background-color', 'rgb(234, 234, 234)');
			/*if($(this).scrollLeft() >= 50 && $(this).scrollLeft() <= 600){
			  $(this).scrollLeft(600);
			}else if($(this).scrollLeft() >= 650 && $(this).scrollLeft() <= 1300){
			  $(this).scrollLeft(1300);
			}else if($(this).scrollLeft() >= 1350 && $(this).scrollLeft() <= 2000){
			  $(this).scrollLeft(2000);
			}else if($(this).scrollLeft() >= 2050 && $(this).scrollLeft() <= 2700){
			  $(this).scrollLeft(2700);
			}*/
		});

	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);
		$btn_exportar.button('loading');


		var arquivo = "";
		$scope.agrupar ? arquivo = "tDerivacoesRUDsemReg" : arquivo = "tDerivacoesRUD";


		//Alex 15-02-2014 - 26/03/2014 TEMPLATE
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='" + arquivo + "'";
		log(stmt);
		db.query(stmt, function (columns) {
			var dataAtualizacao = columns[0].value,
				nomeRelatorio = columns[1].value,
				arquivo = columns[2].value;


			var milis = new Date();
			var baseFile = 'tDerivacoesRUD.xlsx';



			var buffer = toBuffer(toArrayBuffer(arquivo));

			fs.writeFileSync(baseFile, buffer, 'binary');

			var file = 'derivacoesRUD_' + formataDataHoraMilis(milis) + '.xlsx';

			var newData;


			fs.readFile(baseFile, function (err, data) {
				// Create a template
				var t = new XlsxTemplate(data);

				// Perform substitution
				t.substitute(1, {
					filtros: $scope.filtros_usados,
					planDados: trataPorcentagem($scope.dados, true)
				});

				// Get binary data
				newData = t.generate();


				if (!fs.existsSync(file)) {
					fs.writeFileSync(file, newData, 'binary');
					childProcess.exec(file);
					trataPorcentagem($scope.dados, false);
				}
			});

			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);



		}, function (err, num_rows) {
			//?
		});

	};
}
CtrlDerivacoesRUD.$inject = ['$scope', '$globals'];