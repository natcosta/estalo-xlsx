function CtrlChamadasCTG2($scope, $globals) {
  win.title = "Detalhamento de chamadas CTG";
  var limite = 50000;
  $scope.versao = versao;
  //Alex 08/02/2014	
  travaBotaoFiltro(0, $scope, "#pag-chamadas-ctg", "Detalhamento de chamadas CTG 2 - Limite para exportação xlsx: " + limite + " registros.");
  $scope.dados = [];
  $scope.csv = [];

  $scope.colunas = [];
  $scope.gridDados = {
    data: "dados",
    columnDefs: "colunas",
    enableColumnResize: true,
    enablePinning: true
  };

  $scope.aplicacoes = []; // FIXME: copy
  $scope.sites = [];
  $scope.segmentos = [];
  $scope.ordenacao = ['DATA_HORA_INICIO_PASSO1'];
  $scope.decrescente = false;

  var tempData = "";

  var novaMacro;
  var csvExportado = '';


  // Filtros
  $scope.filtros = {
    aplicacoes: [],
    // ALEX - 29/01/2013
    sites: [],
    segmentos: [],
    isHFiltroED: false,
    porDia: false,
    porDDD: false
  };

  var exibeNLU = false;
  var exibeFibraRep = false;
  var exibeReclamacao = false;
  var exibeContas = false;

  //Alex 02/08/2017
  $scope.intervalo = testaPermissoesAclBotoes('pag-chamadas-ctg', 'intervalo');

  // Grid apresentação 05/11/2019
  function geraColunasNova() {
    var columnDefs = [
      {
        headerName: 'Data',
        children: [
          { field: "DATA_HORA_INICIO_PASSO1", headerName: "Data hora (Operação 1)", width: 160, pinned: true },
          { field: "DataHora_Inicio", headerName: "Data hora (URA)", width: 160 }
        ]

      },
      {
        headerName: '',
        children: [
          { field: "CodAplicacao", headerName: "Aplicação", width: 120, columnGroupShow: 'close', filter: 'agNumberColumnFilter' },
          { field: "CodSegmento", headerName: "Segmento", width: 140, columnGroupShow: 'open', filter: 'agNumberColumnFilter' }
        ]

      },
      {
        headerName: '',
        children: [
          { headerName: 'Chamador', field: 'NumANI', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Tratado', field: 'ClienteTratado', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Último estado', field: 'UltimoED', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'TransferID', field: 'TransferID', width: 140, filter: 'agNumberColumnFilter' },
          { headerName: 'Regra', field: 'CodRegra', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'CtxID', field: 'CtxID', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'CALL_DISP', field: 'CALL_DISP', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Transf.Ext', field: 'FL_TRANSF_EXT', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Transf.', field: 'FL_TRANSFERENCIA', width: 90, filter: 'agNumberColumnFilter' },
          { headerName: 'companyName', field: 'CodEmpresa', width: 140, filter: 'agNumberColumnFilter' },
          { headerName: 'SELECTEDDESTNAME', field: 'CodSkill', width: 150, filter: 'agNumberColumnFilter' }
        ]
      }]
    if (exibeNLU || exibeFibraRep || exibeContas || exibeReclamacao) columnDefs.push(
      {
        headerName: 'Atendente',
        children: [
          { headerName: 'Confiança', columnGroupShow: 'closed', field: 'confianca', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Elocução', columnGroupShow: 'open', field: 'elocucao', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Resultado', columnGroupShow: 'open', field: 'resultado', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Reconhecimento', columnGroupShow: 'open', field: 'reconhecimento', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Score', columnGroupShow: 'open', field: 'score', width: 80, filter: 'agNumberColumnFilter' },
          { headerName: 'Tipo da chamada', columnGroupShow: 'open', field: 'tipo', width: 145, filter: 'agNumberColumnFilter' },

        ]
      })

    columnDefs.push(
      {
        headerName: 'Operações',
        children: [
          { headerName: 'Operação 1', columnGroupShow: 'closed', field: 'OPERACAO_FINAL_PASSO1', width: 190, filter: 'agNumberColumnFilter' },
          { headerName: 'Operação 2', columnGroupShow: 'open', field: 'operacao2', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Operação 3', columnGroupShow: 'open', field: 'operacao3', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Operação 4', columnGroupShow: 'open', field: 'operacao4', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Operação 5', columnGroupShow: 'open', field: 'operacao5', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Operação 6', columnGroupShow: 'open', field: 'operacao6', width: 120, filter: 'agNumberColumnFilter' }
        ]
      },
      {
        headerName: 'Empresas',
        children: [
          { headerName: 'Empresa 1', columnGroupShow: 'closed', field: 'EMPRESA_PASSO1', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'Empresa 2', columnGroupShow: 'open', field: 'empresa2', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Empresa 3', columnGroupShow: 'open', field: 'empresa3', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Empresa 4', columnGroupShow: 'open', field: 'empresa4', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Empresa 5', columnGroupShow: 'open', field: 'empresa5', width: 110, filter: 'agNumberColumnFilter' },
          { headerName: 'Empresa 6', columnGroupShow: 'open', field: 'empresa6', width: 110, filter: 'agNumberColumnFilter' }
        ]
      },
      {
        headerName: '',
        children: [
          { headerName: 'Sessão', columnGroupShow: 'open', field: 'SessaoRecVoz', width: 90, filter: 'agNumberColumnFilter' },
          { headerName: 'Assunto', columnGroupShow: 'open', field: 'Assunto', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Tempo Atend.', columnGroupShow: 'open', field: 'DuracaoURA', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'Data Hora FIM (Humano)', columnGroupShow: 'open', field: 'DATA_HORA_FIM', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'BDAberto', columnGroupShow: 'open', field: 'BDAberto', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'OSAberta', columnGroupShow: 'open', field: 'OSAberta', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'COD_SKILL_PASSO2', columnGroupShow: 'open', field: 'COD_SKILL_PASSO2', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'TAG_RECVOZ', columnGroupShow: 'open', field: 'TAG_RECVOZ', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'VIP_FLAG', columnGroupShow: 'open', field: 'VIP_FLAG', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'ICS', columnGroupShow: 'open', field: 'ICS', width: 120, filter: 'agNumberColumnFilter' }
        ]
      },
      {
        headerName: 'Ura',
        children: [
          { headerName: 'URAResgateAnatel', columnGroupShow: 'open', field: 'URAResgateAnatel', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URAProduto', columnGroupShow: 'close', field: 'URAProduto', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URAOpcao', columnGroupShow: 'open', field: 'URAOpcao', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BDABERTODENTRODOPRAZO', columnGroupShow: 'open', field: 'URA_BDABERTODENTRODOPRAZO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BDABERTOFORADOPRAZO', columnGroupShow: 'open', field: 'URA_BDABERTOFORADOPRAZO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BDVELOXABERTODENTRODOPRAZO', columnGroupShow: 'open', field: 'URA_BDVELOXABERTODENTRODOPRAZO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BDVELOXABERTOFORADOPRAZO', columnGroupShow: 'open', field: 'URA_BDVELOXABERTOFORADOPRAZO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BERCARIO', columnGroupShow: 'open', field: 'URA_BERCARIO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BLACKLISTCREP', columnGroupShow: 'open', field: 'URA_BLACKLISTCREP', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BLOQUEIO_FIXA', columnGroupShow: 'open', field: 'URA_BLOQUEIO_FIXA', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_CLIENTE_FIBRA', columnGroupShow: 'open', field: 'URA_CLIENTE_FIBRA', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_MAILING_R2', columnGroupShow: 'open', field: 'URA_MAILING_R2', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_OS', columnGroupShow: 'open', field: 'URA_OS', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_HORARIOCREP', columnGroupShow: 'open', field: 'URA_HORARIOCREP', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_PILOTOCREP', columnGroupShow: 'open', field: 'URA_PILOTOCREP', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_QUARENTENACREP', columnGroupShow: 'open', field: 'URA_QUARENTENACREP', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_REPETIDACREP', columnGroupShow: 'open', field: 'URA_REPETIDACREP', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_RESGATEANATELST', columnGroupShow: 'open', field: 'URA_RESGATEANATELST', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_TIPOBLOQUEIO', columnGroupShow: 'open', field: 'URA_TIPOBLOQUEIO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_BAIRRO', columnGroupShow: 'open', field: 'URA_BAIRRO', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_CIDADE', columnGroupShow: 'open', field: 'URA_CIDADE', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_MAILINGCONVERGENTE', columnGroupShow: 'open', field: 'URA_MAILINGCONVERGENTE', width: 130, filter: 'agNumberColumnFilter' },
          { headerName: 'URA_INADIMPLENTE', columnGroupShow: 'open', field: 'URA_INADIMPLENTE', width: 130, filter: 'agNumberColumnFilter' }

        ]
      },
      {
        headerName: 'Datas e Status',
        children: [
          { headerName: 'DATANASC', columnGroupShow: 'close', field: 'DATANASC', width: 160, filter: 'agNumberColumnFilter' },
          { headerName: 'DATAINST', columnGroupShow: 'open', field: 'DATAINST', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'DATAINSTVELOX', columnGroupShow: 'open', field: 'DATAINSTVELOX', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'STATUSFIXO', columnGroupShow: 'open', field: 'Statusfixo', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'STATUSVELOX', columnGroupShow: 'open', field: 'StatusVelox', width: 120, filter: 'agNumberColumnFilter' },
          { headerName: 'STATUSOITV', columnGroupShow: 'open', field: 'StatusOiTV', width: 120, filter: 'agNumberColumnFilter' }
        ]
      },
      {
        headerName: 'Grupo Dirigida',
        children: [
          { headerName: 'GRUPO_DIRIGIDA1', columnGroupShow: 'close', field: 'GrupoDirigida1', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'GRUPO_DIRIGIDA2', columnGroupShow: 'open', field: 'GrupoDirigida2', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'GRUPO_DIRIGIDA3', columnGroupShow: 'open', field: 'GrupoDirigida3', width: 150, filter: 'agNumberColumnFilter' }
        ]
      },
      {
        headerName: 'Elocucao Dirigida',
        children: [
          { headerName: 'ELOCUCAO_DIRIGIDA1', columnGroupShow: 'close', field: 'ElocucaoDirigida1', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'ELOCUCAO_DIRIGIDA2', columnGroupShow: 'open', field: 'ElocucaoDirigida2', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'ELOCUCAO_DIRIGIDA3', columnGroupShow: 'open', field: 'ElocucaoDirigida3', width: 150, filter: 'agNumberColumnFilter' }
        ]
      }/*,
	  
	  {
        headerName: 'Pergunta Dirigida',
        children: [
          { headerName: 'ELOCUCAO_RECVOZ_DIR1', columnGroupShow: 'close', field: 'ElocucaoRecVozDir1', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'RESULTADO_RECVOZ_DIR1', columnGroupShow: 'close', field: 'ResultadoRecVozDir1', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'SCORE_RECVOZ_DIR1', columnGroupShow: 'close', field: 'ScoreRecVozDir1', width: 150, filter: 'agNumberColumnFilter' },
		  { headerName: 'ELOCUCAO_RECVOZ_DIR2', columnGroupShow: 'open', field: 'ElocucaoRecVozDir2', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'RESULTADO_RECVOZ_DIR2', columnGroupShow: 'open', field: 'ResultadoRecVozDir2', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'SCORE_RECVOZ_DIR2', columnGroupShow: 'open', field: 'ScoreRecVozDir2', width: 150, filter: 'agNumberColumnFilter' },
		  { headerName: 'ELOCUCAO_RECVOZ_DIR3', columnGroupShow: 'open', field: 'ElocucaoRecVozDir3', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'RESULTADO_RECVOZ_DIR3', columnGroupShow: 'open', field: 'ResultadoRecVozDir3', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'SCORE_RECVOZ_DIR3', columnGroupShow: 'open', field: 'ScoreRecVozDir3', width: 150, filter: 'agNumberColumnFilter' },
		  { headerName: 'ELOCUCAO_RECVOZ_DIR4', columnGroupShow: 'open', field: 'ElocucaoRecVozDir4', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'RESULTADO_RECVOZ_DIR4', columnGroupShow: 'open', field: 'ResultadoRecVozDir4', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'SCORE_RECVOZ_DIR4', columnGroupShow: 'open', field: 'ScoreRecVozDir4', width: 150, filter: 'agNumberColumnFilter' },
		  { headerName: 'ELOCUCAO_RECVOZ_DIR5', columnGroupShow: 'open', field: 'ElocucaoRecVozDir5', width: 170, filter: 'agNumberColumnFilter' },
          { headerName: 'RESULTADO_RECVOZ_DIR5', columnGroupShow: 'open', field: 'ResultadoRecVozDir5', width: 150, filter: 'agNumberColumnFilter' },
          { headerName: 'SCORE_RECVOZ_DIR5', columnGroupShow: 'open', field: 'ScoreRecVozDir5', width: 150, filter: 'agNumberColumnFilter' }
        ]
      }*/





    );
    return columnDefs
  }

  $scope.gridOptions = {
    defaultColDef: {
      sortable: true,
      resizable: true,
      filter: true
    },
    debug: true,
    columnDefs: [],
    rowData: []
  };

  // Exportação CSV Colunas
  function geraColunas() {
    var array = [];
    array.push(
      {
        displayName: "Data hora (Operação 1)",
        field: "DATA_HORA_INICIO_PASSO1"
      },
      {
        displayName: "Data hora (URA)",
        field: "DataHora_Inicio"
      }
    );

    if ($scope.intervalo) {
      array.push({
        displayName: "Intervalo (Segundos)"
      });
    }
    array.push(
      {
        displayName: "Aplicação",
        field: 'CodAplicacao'
      },
      {
        displayName: "Segmento",
        field: "CodSegmento"
      },
      {
        displayName: "Chamador",
        field: "NumAni"
      },
      {
        displayName: "Tratado",
        field: 'ClienteTratado'
      },
      {
        displayName: "Último estado", field: 'UltimoED'
      },
      {
        displayName: "TransferID", field: 'TransferID'
      },
      {
        displayName: "Regra", field: 'CodRegra'
      },
      {
        displayName: "CTXID", field: 'CtxID'
      },
      {
        displayName: "CALL_DISP", field: 'CALL_DISP'
      },
      {
        displayName: "Transf. Ext", field: 'FL_TRANSF_EXT'
      },
      {
        displayName: "Transf.", field: 'FL_TRANSFERENCIA'
      }
    );
    if (exibeNLU || exibeFibraRep || exibeContas || exibeReclamacao)
      array.push(
        {
          displayName: "Confiança", field: 'confianca'
        },
        {
          displayName: "Elocução", field: 'elocucao'
        },
        {
          displayName: "Resultado", field: 'resultado'
        },
        {
          displayName: "Reconhecimento", field: 'reconhecimento',
        },
        {
          displayName: "Score", field: 'score'
        },
        {
          displayName: "Tipo de Chamada", field: 'tipo'
        }
      );

    array.push(
      {
        displayName: "Operação 1", field: 'OPERACAO_FINAL_PASSO1'
      },
      {
        displayName: "Empresa 1", field: 'EMPRESA_PASSO1'
      },
      {
        displayName: "Operação 2", field: 'operacao2'
      },
      {
        displayName: "Empresa 2", field: 'empresa2'
      },
      {
        displayName: "Operação 3", field: 'operacao3'
      },
      {
        displayName: "Empresa 3", field: 'empresa3'
      },
      {
        displayName: "Operação 4", field: 'operacao4'
      },
      {
        displayName: "Empresa 4", field: 'empresa4'
      },
      {
        displayName: "Operação 5", field: 'operacao5'
      },
      {
        displayName: "Empresa 5", field: 'empresa5'
      },
      {
        displayName: "Operação 6", field: 'operacao6'
      },
      {
        displayName: "Empresa 6", field: 'empresa6'
      },
      {
        displayName: "Sessão", field: 'SessaoRecVoz'
      },
      {
        displayName: "Assunto", field: 'Assunto'
      },
      {
        displayName: "Tempo atend.", field: 'DuracaoURA'
      },
      {
        displayName: "Data hora fim (HUMANO)", field: 'DATA_HORA_FIM'
      },
      {
        displayName: "BDAberto", field: 'BDAberto'

      },
      {
        displayName: "OSAberta", field: 'OSAberta'
      },
      {
        displayName: "URAResgateAnatel", field: 'URAResgateAnatel'
      },
      {
        displayName: "URAProduto", field: 'URAProduto'
      },
      {
        displayName: "URAOpcao", field: 'URAOpcao'
      },
      {
        displayName: "COD_SKILL_PASSO2", field: 'COD_SKILL_PASSO2'
      },

      {
        displayName: "TAG_RECVOZ", field: 'TAG_RECVOZ'
      },
      {
        displayName: "URA_BDABERTODENTRODOPRAZO", field: 'URA_BDABERTODENTRODOPRAZO'
      },
      {
        displayName: "URA_BDABERTOFORADOPRAZO", field: 'URA_BDABERTOFORADOPRAZO'
      },
      {
        displayName: "URA_BDVELOXABERTODENTRODOPRAZO", field: 'URA_BDVELOXABERTODENTRODOPRAZO'
      },
      {
        displayName: "URA_BDVELOXABERTOFORADOPRAZO", field: 'URA_BDVELOXABERTOFORADOPRAZO'
      },
      {
        displayName: "URA_BERCARIO", field: 'URA_BERCARIO'
      },
      {
        displayName: "URA_BLACKLISTCREP", field: 'URA_BLACKLISTCREP'
      },
      {
        displayName: "URA_BLOQUEIO_FIXA", field: 'URA_BLOQUEIO_FIXA'
      },
      {
        displayName: "URA_CLIENTE_FIBRA", field: 'URA_CLIENTE_FIBRA'
      },
      {
        displayName: "URA_HORARIOCREP", field: 'URA_HORARIOCREP'
      },
      {
        displayName: "URA_MAILING_R2", field: 'URA_MAILING_R2'
      },
      {
        displayName: "URA_OS", field: 'URA_OS'
      },
      {
        displayName: "URA_PILOTOCREP", field: 'URA_PILOTOCREP'
      },
      {
        displayName: "URA_QUARENTENACREP", field: 'URA_QUARENTENACREP'
      },
      {
        displayName: "URA_REPETIDACREP", field: 'URA_REPETIDACREP'
      },

      {
        displayName: "URA_RESGATEANATELST", field: 'URA_RESGATEANATELST'
      },
      {
        displayName: "URA_TIPOBLOQUEIO", field: 'URA_TIPOBLOQUEIO'
      },
      {
        displayName: "VIP_FLAG", field: 'VIP_FLAG'
      },
      {
        displayName: "URA_BAIRRO", field: 'URA_BAIRRO'
      },
      {
        displayName: "URA_CIDADE", field: 'URA_CIDADE'
      },
      {
        displayName: "URA_MAILINGCONVERGENTE", field: 'URA_MAILINGCONVERGENTE'
      },
      {
        displayName: "URA_INADIMPLENTE", field: 'URA_INADIMPLENTE'
      },
      {
        displayName: "ICS", field: 'ICS'
      },
      {
        displayName: "DATANASC", field: 'DATANASC'
      },
      {
        displayName: "DATAINST", field: 'DATAINST'
      },
      {
        displayName: "DATAINSTVELOX", field: 'DATAINSTVELOX'
      },
      {
        displayName: "STATUSFIXO", field: 'Statusfixo'
      },

      {
        displayName: "STATUSVELOX", field: 'StatusVelox'
      },
      {
        displayName: "STATUSOITV", field: 'StatusOiTV'
      },
      {
        displayName: "GRUPO_DIRIGIDA1", field: 'GrupoDirigida1'
      },
      {
        displayName: "GRUPO_DIRIGIDA2", field: 'GrupoDirigida2'
      },
      {
        displayName: "GRUPO_DIRIGIDA3", field: 'GrupoDirigida3'
      },
      {
        displayName: "ELOCUCAO_DIRIGIDA1", field: 'ElocucaoDirigida1'
      },
      {
        displayName: "ELOCUCAO_DIRIGIDA2", field: 'ElocucaoDirigida2'
      },
      {
        displayName: "ELOCUCAO_DIRIGIDA3", field: 'ElocucaoDirigida3'
      },
      /*{
          displayName: "ELOCUCAO_RECVOZ_DIR1", field: 'ElocucaoRecVozDir1'
        },
      {
          displayName: "RESULTADO_RECVOZ_DIR1", field: 'ResultadoRecVozDir1'
        },
      {
          displayName: "SCORE_RECVOZ_DIR1", field: 'ScoreRecVozDir1'
        },
      {
          displayName: "ELOCUCAO_RECVOZ_DIR2", field: 'ElocucaoRecVozDir2'
        },
      {
          displayName: "RESULTADO_RECVOZ_DIR2", field: 'ResultadoRecVozDir2'
        },
      {
          displayName: "SCORE_RECVOZ_DIR2", field: 'ScoreRecVozDir2'
        },
      {
          displayName: "ELOCUCAO_RECVOZ_DIR3", field: 'ElocucaoRecVozDir3'
        },
      {
          displayName: "RESULTADO_RECVOZ_DIR3", field: 'ResultadoRecVozDir3'
        },
      {
          displayName: "SCORE_RECVOZ_DIR3", field: 'ScoreRecVozDir3'
        },
      {
          displayName: "ELOCUCAO_RECVOZ_DIR4", field: 'ElocucaoRecVozDir4'
        },
      {
          displayName: "RESULTADO_RECVOZ_DIR4", field: 'ResultadoRecVozDir4'
        },
      {
          displayName: "SCORE_RECVOZ_DIR4", field: 'ScoreRecVozDir4'
        },
      {
          displayName: "ELOCUCAO_RECVOZ_DIR5", field: 'ElocucaoRecVozDir5'
        },
      {
          displayName: "RESULTADO_RECVOZ_DIR5", field: 'ResultadoRecVozDir5'
        },
      {
          displayName: "SCORE_RECVOZ_DIR5", field: 'ScoreRecVozDir5'
        },*/
      {
        displayName: "companyName", field: 'CodEmpresa'
      },
      {
        displayName: "SELECTEDDESTNAME", field: 'CodSkill'
      }
    );

    return array;
  }

  // Filtros: data e hora
  var agora = new Date();

  //var dat_consoli = new Date(teste_data);

  //Alex 03/03/2014
  var inicio, fim;
  Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = antesDeOntemInicio();
  Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = antesDeOntemFim();

  $scope.periodo = {
    inicio: inicio,
    fim: fim,
    min: new Date(2013, 11, 1),
    max: agora // FIXME: atualizar ao virar o dia

  };

  var $view;


  $scope.$on('$viewContentLoaded', function () {
    $view = $("#pag-chamadas-ctg");

    //19/03/2014
    componenteDataHora($scope, $view);
    carregaRegioes($view);
    carregaAplicacoes($view, false, false, $scope);
    carregaOperacoesECH($view, true);
    carregaSites($view);
    carregaEmpresas($view, true);
    carregaRegras($view, true);
    carregaSkills($view, true);

    carregaSegmentosPorAplicacao($scope, $view, true);
    // Popula lista de segmentos a partir das aplicações selecionadas
    $view.on("change", "select.filtro-aplicacao", function () {
      carregaSegmentosPorAplicacao($scope, $view)
    });

    carregaDDDsPorAplicacao($scope, $view, true);
    // Popula lista de  DDDs a partir das aplicações selecionadas
    $view.on("change", "select.filtro-aplicacao", function () {
      carregaDDDsPorAplicacao($scope, $view)
    });


    //GILBERTOO - change de segmentos
    $view.on("change", "select.filtro-segmento", function () {
      Estalo.filtros.filtro_segmentos = $(this).val();
    });

    // Marca todos os operacoes
    $view.on("click", "#alinkOper", function () {
      marcaTodosIndependente($('.filtro-operacao'), 'operacoes')
    });

    // Marca todos os edstransf
    $view.on("click", "#alinkTid", function () {
      marcaTodosIndependente($('.filtro-transferid'), 'transferid')
    });

    // Marca todos os edstransf
    $view.on("click", "#alinkEDT", function () {
      marcaTodosIndependente($('.filtro-edstransf'), 'edstransf')
    });

    // Marca todos os sites
    $view.on("click", "#alinkSite", function () {
      marcaTodosIndependente($('.filtro-site'), 'sites')
    });

    // Marca todos os segmentos
    $view.on("click", "#alinkSeg", function () {
      marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
    });

    // Marca todos os empresas
    $view.on("click", "#alinkEmp", function () {
      marcaTodosIndependente($('.filtro-empresa'), 'empresa')
    });

    // Marca todos os regras
    $view.on("click", "#alinkRegra", function () {
      marcaTodosIndependente($('.filtro-regra'), 'regra')
    });

    // Marca todos os skills
    $view.on("click", "#alinkSkill", function () {
      marcaTodosIndependente($('.filtro-skill'), 'skill')
    });

    //Marcar todas aplicações
    $view.on("click", "#alinkApl", function () {
      marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
    });


    $view.on("click", ".btn-exportar", function () {
      $scope.exportaXLSX.apply(this);
    });

    // Limpa filtros Alex 03/03/2014
    $view.on("click", ".btn-limpar-filtros", function () {
      $scope.porRegEDDD = false;
      $scope.limparFiltros.apply(this);
    });

    $scope.limparFiltros = function () {
      iniciaFiltros();
      componenteDataHora($scope, $view, true);

      var partsPath = window.location.pathname.split("/");
      var part = partsPath[partsPath.length - 1];

      var $btn_limpar = $view.find(".btn-limpar-filtros");
      $btn_limpar.prop("disabled", true);
      $btn_limpar.button('loading');

      setTimeout(function () {
        $btn_limpar.button('reset');
        //window.location.href = part + window.location.hash +'/';
      }, 500);
    }

    // Botão agora Alex 03/03/2014
    $view.on("click", ".btn-agora", function () {
      $scope.agora.apply(this);
    });

    $scope.agora = function () {
      iniciaAgora($view, $scope);
    }

    $view.on("click", ".btn-exportarCSV-interface", function () {
      var newName = formataDataHoraMilis(new Date()).replace(/[^0-9]/g, '');
      if (csvExportado === '') {
        fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData, 'ascii');
        fs.renameSync(tempDir3() + 'dados_ctg.csv', tempDir3() + 'export_' + newName + '.csv');
        childProcess.exec("start excel " + tempDir3() + 'export_' + newName + '.csv',
          function (error, stdout, stderr) {
            if (error) {
              console.error("exec error: " + error);
              alert("Parece que o seu sistema não tem o Excel instalado.");
              childProcess.exec(tempDir3() + 'export_' + newName + '.csv');
              return;
            }

          });
        csvExportado = tempDir3() + 'export_' + newName + '.csv';

      } else {
        alert("Dado já foi exportado(" + csvExportado + ").");
      }
      $(".dropdown-exportar").toggle();
    });

    // Botão abortar Alex 23/05/2014
    $view.on("click", ".abortar", function () {
      $scope.abortar.apply(this);
      var $btn_exportar = $view.find(".btn-exportar");
      var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
      var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
      $btn_exportar.prop("disabled", false);
      $btn_exportar_csv.prop("disabled", false);
      $btn_exportar_dropdown.prop("disabled", false);

    });

    //Alex 23/05/2014
    $scope.abortar = function () {
      abortar($scope, "#pag-chamadas-ctg");
    }

    // Lista chamadas conforme filtros
    $view.on("click", ".btn-gerar", function () {

      limpaProgressBar($scope, "#pag-chamadas-ctg");
      //22/03/2014 Testa se data início é maior que a data fim
      var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
      var testedata = testeDataMaisHora(data_ini, data_fim);
      if (testedata !== "") {
        setTimeout(function () {
          atualizaInfo($scope, testedata);
          effectNotification();
          $view.find(".btn-gerar").button('reset');
        }, 500);
        return;
      }
      $scope.colunas = geraColunas();
      $scope.listaDados.apply(this);
      $scope.gridOptions.columnDefs = geraColunasNova();
      $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
    });

    $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');


    $view.on("change", 'input:checkbox[name=atendente]', function () {
      if ($("input[name='atendente']:checked").val() == 'exibeNLU') {
        exibeNLU = true;
        document.getElementById("todos").checked = false;
      } else {
        exibeNLU = false;

      }
    });
    $('input:radio[name=atendente]').css('margin', '1px 3px 3px 3px');
    $view.on("change", 'input:checkbox[name=fibra]', function () {
      if ($("input[name='fibra']:checked").val() == 'exibeFibraRep') {
        exibeFibraRep = true;
        document.getElementById("todos").checked = false;
      } else {
        exibeFibraRep = false;
      }
    });
    $('input:radio[name=reclamacao]').css('margin', '1px 3px 3px 3px');
    $view.on("change", 'input:checkbox[name=reclamacao]', function () {
      if ($("input[name='reclamacao']:checked").val() == 'exibeReclamacao') {
        exibeReclamacao = true;
        document.getElementById("todos").checked = false;
      } else {
        exibeReclamacao = false;
      }
    });
    $('input:radio[name=contas]').css('margin', '1px 3px 3px 3px');
    $view.on("change", 'input:checkbox[name=contas]', function () {
      if ($("input[name='contas']:checked").val() == 'exibeContas') {
        exibeContas = true;
        document.getElementById("todos").checked = false;
      } else {
        exibeContas = false;
      }
    });
    $view.on("change", 'input:checkbox[name=todos]', function () {
      if ($("input[name='todos']:checked").val() == 'exibePadrao') {
        exibeFibraRep = false;
        exibeReclamacao = false;
        exibeContas = false;
        exibeNLU = false;
        document.getElementById("atendente").checked = false;
        document.getElementById("fibra").checked = false;
        document.getElementById("reclamacao").checked = false;
        document.getElementById("contas").checked = false;
      } else {
      }
    });

  });


  // Lista chamadas conforme filtros
  $scope.listaDados = function () {
    csvExportado = '';

    //var totalGeral = 0;

    if (fs.existsSync(tempDir3() + 'dados_ctg.csv')) fs.unlinkSync(tempDir3() + 'dados_ctg.csv');
    var header = '';
    for (var item in $scope.colunas) {
      if (typeof ($scope.colunas[item]) === "object") header += $scope.colunas[item].displayName + ';';
    }
    header += ';\r\n';
    tempData = header;
    $scope.csv = [];
    var datasComDados = {};
    $globals.numeroDeRegistros = 0;

    var $btn_exportar = $view.find(".btn-exportar");
    var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
    var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
    $btn_exportar.prop("disabled", true);
    $btn_exportar_csv.prop("disabled", true);
    $btn_exportar_dropdown.prop("disabled", true);

    var $btn_gerar = $(this);
    $btn_gerar.button('loading');

    var data_ini = $scope.periodo.inicio,
      data_fim = $scope.periodo.fim;

    var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

    if (filtro_aplicacoes.length === 0) {
      atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
      $btn_gerar.button('reset');
      return;
    }
    var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
    var filtro_sites = $view.find("select.filtro-site").val() || [];
    var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
    var filtro_ed = $view.find("select.filtro-ed").val() || [];
    var filtro_edt = garanteVetor($(".filtro-edstransf").val());
    if (filtro_edt.length > 0) {
      filtro_edt = filtro_edt.join(",").split(",");
    }
    var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
    var filtro_regras = $view.find("select.filtro-regra").val() || [];
    var filtro_skills = $view.find("select.filtro-skill").val() || [];
    var filtro_transferid = $view.find("select.filtro-transferid").val() || [];


    //filtros usados
    $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
      " até " + formataDataHoraBR($scope.periodo.fim);
    if (filtro_aplicacoes.length > 0) $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
    if (filtro_operacoes.length > 0) $scope.filtros_usados += " Operações: " + filtro_operacoes;
    if (filtro_sites.length > 0) $scope.filtros_usados += " Sites: " + filtro_sites;
    if (filtro_segmentos.length > 0) $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
    if (filtro_ed.length > 0) $scope.filtros_usados += " EDs: " + filtro_ed;
    if (filtro_edt.length > 0) $scope.filtros_usados += " EDsTransf: " + filtro_edt;
    if (filtro_empresas.length > 0) $scope.filtros_usados += " Empresas: " + filtro_empresas;
    if (filtro_regras.length > 0) $scope.filtros_usados += " Regras: " + filtro_regras;
    if (filtro_transferid.length > 0) $scope.filtros_transferid += " TransferId: " + filtro_transferid;
    if (filtro_skills.length > 0) {
      var filtro_skills_mod = [];
      for (var i = 0; i < filtro_skills.length; i++) {
        var lsRegExp = /%/g;
        filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp, '&lt;'));
      }
      $scope.filtros_usados += " Skills: " + filtro_skills_mod;
    }

    $scope.dados = [];
    var stmt = "";
    //var executaQuery = "";


    var tabelaConsultar = "CruzamentoCTG";

    camposAdicionais = ", confiancarecvoz, elocucaorecvoz, resultadorecvoz, reconhecimentorecvoz, scorerecvoz, tipo_chamada";
    var like = ""
    if (exibeNLU) {
      like += " and (OpcaoNLU like '%A%'"
      if (exibeFibraRep) {
        like += " OR OpcaoNLU like '%F%'"
      }
      if (exibeContas) {
        like += " OR OpcaoNLU like '%C%'"
      }
      if (exibeReclamacao) {
        like += " OR OpcaoNLU like '%R%' "
      }
      like += ")"
    } else if (exibeFibraRep) {
      like += " and ( OpcaoNLU like '%F%'"

      if (exibeContas) {
        like += " OR OpcaoNLU like '%C%'"
      }
      if (exibeReclamacao) {
        like += " OR OpcaoNLU like '%R%' "
      }
      like += ")"
    } else
      if (exibeContas) {
        like += " and ( OpcaoNLU like '%C%'"

        if (exibeReclamacao) {
          like += " OR OpcaoNLU like '%R%' "
        }
        like += ")"
      } else if (exibeReclamacao) {
        like += " and OpcaoNLU like '%R%' "
      }


    // CONSULTA SQL 1.1
    stmt =
      db.use +
      " SELECT DATA_HORA_INICIO_PASSO1,DataHora_Inicio,CodAplicacao,CodSegmento,NumANI,ClienteTratado,ED.Nom_Estado AS UltimoED,ucid,TEL_BINADO, CALL_DISP, FL_TRANSF_EXT, FL_TRANSFERENCIA" +
      camposAdicionais +
      " ,TransferID,CtxID,CodRegra,isnull(SessaoRecVoz,'') as SessaoRecVoz ,OPERACAO_FINAL_PASSO1,EMPRESA_PASSO1,Transferencias,Assunto,DuracaoURA,DATA_HORA_FIM, BDAberto, OSAberta, URAResgateAnatel, URAProduto, URAOpcao, VariaveisJSON, ICS,DATANASC,DATAINST,DATAINSTVELOX,Statusfixo,StatusVelox,StatusMovel,StatusOiTV, GrupoDirigida1, GrupoDirigida2, GrupoDirigida3, ElocucaoDirigida1, ElocucaoDirigida2, ElocucaoDirigida3" +
      //" ,ElocucaoRecVozDir1, ResultadoRecVozDir1, ScoreRecVozDir1, ElocucaoRecVozDir2, ResultadoRecVozDir2, ScoreRecVozDir2 ,ElocucaoRecVozDir3, ResultadoRecVozDir3, ScoreRecVozDir3 ,ElocucaoRecVozDir4, ResultadoRecVozDir4, ScoreRecVozDir4 ,ElocucaoRecVozDir5, ResultadoRecVozDir5, ScoreRecVozDir5" +
      " ,CodEmpresa,CodSkill" +
      " FROM " +
      tabelaConsultar +
      " AS RE" +
      " LEFT OUTER JOIN ESTADO_DIALOGO AS ED" +
      " ON RE.UltimoED = ED.Cod_Estado AND RE.CodAplicacao =  ED.Cod_Aplicacao" +
      " WHERE 1 = 1" +
      " AND DATA_HORA_INICIO_PASSO1 >= '" +
      formataDataHora(data_ini) +
      "'" +
      " AND DATA_HORA_INICIO_PASSO1 <= '" +
      formataDataHora(data_fim) +
      "'";
    stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
    stmt += restringe_consulta("TransferId", filtro_transferid, true);
    stmt += restringe_consulta("OPERACAO_FINAL_PASSO1", filtro_operacoes, true);
    stmt += restringe_consulta("CodSite", filtro_sites, true);
    stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
    stmt += restringe_consulta("EMPRESA_PASSO1", filtro_empresas, true);
    stmt += restringe_consulta("UltimoED", filtro_edt, true);
    stmt += like;
    stmt += " ORDER BY DATA_HORA_INICIO_PASSO1 ASC";

    try {
      //executaQuery = executaQuery2;
      log(stmt);
    } catch (err) {
      console.log(err);
    }

    var stmtCountRows = stmtContaLinhasChamadasCTG(stmt);

    // Alex 24/02/2014 Contador de linhas 
    function contaLinhas(columns) {
      //var item = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'": "'+_.size(datasComDados)+'"}';
      var item1 = '{"' + columns[1].value.replace(/[^0-9]/g, '') + '4": "' + columns[1].value + '0:00' + '|' + columns[1].value + '4:59' + '"}';
      var item2 = '{"' + columns[1].value.replace(/[^0-9]/g, '') + '9": "' + columns[1].value + '5:00' + '|' + columns[1].value + '9:59' + '"}';
      jsonConcat(datasComDados, JSON.parse(item1));
      jsonConcat(datasComDados, JSON.parse(item2));
      $globals.numeroDeRegistros = columns[0].value;
    }

    var objColunasTabela = {};

    function executaQuery1(columns) {

      executaQuery(columns);
      // Insere no grid
      $scope.dados.push(objColunasTabela);
      var a = [];
      for (var item in $scope.colunas) {
        a.push(objColunasTabela[$scope.colunas[item].field]);
      }
      $scope.csv.push(a);

      // FIM GERA DADOS COLUNAS
      //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
      if ($scope.dados.length % 1000 === 0) {
        fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData, 'ascii');
        tempData = "";
        $scope.$apply();

      }
    }
    $scope.gridOptions.api.setColumnDefs([]);
    $scope.gridOptions.api.setRowData([]);

    function executaQuery2(columns) {

      executaQuery(columns);
      if (tempData.length > 240000) {
        fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData, 'ascii');
        tempData = "";
      }
    }

    function executaQuery(columns) {
      //totalGeral++;
      objColunasTabela = {};

      // GERA DADOS COLUNAS
      // DETALHAMENTO INICIO
      objColunasTabela["DATA_HORA_INICIO_PASSO1"] = columns["DATA_HORA_INICIO_PASSO1"] !== undefined ? columns["DATA_HORA_INICIO_PASSO1"].value : undefined;
      objColunasTabela["DataHora_Inicio"] = columns["DataHora_Inicio"] !== undefined ? columns["DataHora_Inicio"].value : undefined;
      objColunasTabela["intervalo"] = (new Date(objColunasTabela["DATA_HORA_INICIO_PASSO1"]) - new Date(objColunasTabela["DataHora_Inicio"])) / 1000;
      objColunasTabela["CodAplicacao"] = columns["CodAplicacao"] !== undefined ? columns["CodAplicacao"].value : undefined;
      objColunasTabela["UltimoED"] = columns["UltimoED"] !== undefined ? columns["UltimoED"].value : undefined;
      objColunasTabela["CodAplicacao"] = obtemNomeAplicacao(objColunasTabela["CodAplicacao"]);
      objColunasTabela["CodSegmento"] = columns["CodSegmento"] !== undefined ? columns["CodSegmento"].value : undefined;
      objColunasTabela["CodSegmento"] = obtemNomeSegmento(objColunasTabela["CodSegmento"]);
      objColunasTabela["NumANI"] = columns["NumANI"] !== undefined ? columns["NumANI"].value : undefined;
      objColunasTabela["ClienteTratado"] = columns["ClienteTratado"] !== undefined ? columns["ClienteTratado"].value : undefined;
      objColunasTabela["TransferID"] = columns["TransferID"] !== undefined ? columns["TransferID"].value : undefined;
      objColunasTabela["CtxID"] = columns["CtxID"] !== undefined ? (columns["CtxID"].value).toString() : undefined;
      objColunasTabela["UCID"] = columns["UCID"] !== undefined ? columns["UCID"].value : undefined;

      objColunasTabela["CALL_DISP"] = columns["CALL_DISP"] !== undefined ? columns["CALL_DISP"].value : undefined;
      objColunasTabela["FL_TRANSF_EXT"] = columns["FL_TRANSF_EXT"] !== undefined ? columns["FL_TRANSF_EXT"].value : undefined;
      objColunasTabela["FL_TRANSFERENCIA"] = columns["FL_TRANSFERENCIA"] !== undefined ? columns["FL_TRANSFERENCIA"].value : undefined;

      // NLU
      if (exibeNLU || exibeFibraRep || exibeContas || exibeReclamacao) {
        objColunasTabela["confianca"] = columns["confiancarecvoz"] !== undefined ? columns["confiancarecvoz"].value : undefined;
        objColunasTabela["elocucao"] = columns["elocucaorecvoz"] !== undefined ? columns["elocucaorecvoz"].value : undefined;
        objColunasTabela["resultado"] = columns["resultadorecvoz"] !== undefined ? columns["resultadorecvoz"].value : undefined;
        objColunasTabela["reconhecimento"] = columns["reconhecimentorecvoz"] !== undefined ? columns["reconhecimentorecvoz"].value : undefined;
        objColunasTabela["score"] = columns["scorerecvoz"] !== undefined ? columns["scorerecvoz"].value : undefined;
        objColunasTabela["tipo"] = columns["tipo_chamada"] !== undefined ? columns["tipo_chamada"].value : undefined;

      }

      objColunasTabela["TEL_DIGITADO"] = columns["TEL_DIGITADO"] !== undefined ? columns["TEL_DIGITADO"].value : undefined;
      objColunasTabela["TEL_BINADO"] = columns["TEL_BINADO"] !== undefined ? columns["TEL_BINADO"].value : undefined;
      objColunasTabela["CLITRATADO"] = columns["CLITRATADO"] !== undefined ? columns["CLITRATADO"].value : undefined;
      objColunasTabela["CHAM_ATENDIDA"] = columns["CHAM_ATENDIDA"] !== undefined ? columns["CHAM_ATENDIDA"].value : undefined;
      objColunasTabela["CHAM_ABAND"] = columns["CHAM_ABAND"] !== undefined ? columns["CHAM_ABAND"].value : undefined;

      objColunasTabela["DATA_HORA_FIM_PASSO1"] = columns["DATA_HORA_FIM_PASSO1"] !== undefined ? columns["DATA_HORA_FIM_PASSO1"].value : undefined;
      objColunasTabela["DURACAO_PASSO1"] = columns["DURACAO_PASSO1"] !== undefined ? columns["DURACAO_PASSO1"].value : undefined;
      objColunasTabela["TEMPO_FILA_PASSO1"] = columns["TEMPO_FILA_PASSO1"] !== undefined ? columns["TEMPO_FILA_PASSO1"].value : undefined;
      objColunasTabela["MATRICULA_AGENTE_PASSO1"] = columns["MATRICULA_AGENTE_PASSO1"] !== undefined ? columns["MATRICULA_AGENTE_PASSO1"].value : undefined;
      objColunasTabela["COD_SKILL_PASSO1"] = columns["COD_SKILL_PASSO1"] !== undefined ? columns["COD_SKILL_PASSO1"].value : undefined;
      objColunasTabela["ID_ECADOP_PASSO1"] = columns["ID_ECADOP_PASSO1"] !== undefined ? columns["ID_ECADOP_PASSO1"].value : undefined;
      objColunasTabela["DAC_PASSO1"] = columns["DAC_PASSO1"] !== undefined ? columns["DAC_PASSO1"].value : undefined;
      objColunasTabela["OPERACAO_FINAL_PASSO1"] = columns["OPERACAO_FINAL_PASSO1"] !== undefined ? columns["OPERACAO_FINAL_PASSO1"].value : undefined;
      objColunasTabela["EMPRESA_PASSO1"] = columns["EMPRESA_PASSO1"] !== undefined ? columns["EMPRESA_PASSO1"].value : undefined;
      objColunasTabela["CodRegra"] = columns["CodRegra"] !== undefined ? columns["CodRegra"].value : undefined;
      objColunasTabela["SessaoRecVoz"] = columns["SessaoRecVoz"] !== undefined ? columns["SessaoRecVoz"].value : undefined;
      objColunasTabela["Assunto"] = columns["Assunto"] !== undefined ? columns["Assunto"].value : undefined;
      objColunasTabela["DuracaoURA"] = columns["DuracaoURA"] !== undefined ? columns["DuracaoURA"].value : undefined;
      objColunasTabela["DATA_HORA_FIM"] = columns["DATA_HORA_FIM"] !== undefined ? columns["DATA_HORA_FIM"].value : undefined;
      // DETALHAMENTO FIM PT1

      // PARSE JSON OBJ TRANSFERENCIAS
      objColunasTabela["Transferencias"] = columns["Transferencias"] !== undefined ? columns["Transferencias"].value : undefined;

      // Nova coluna inserida ES-4403 25/02/2019 - Paulo Raoni
      var COD_SKILL_PASSO2 = objColunasTabela["Transferencias"] ? JSON.parse(objColunasTabela["Transferencias"]) : null;
      COD_SKILL_PASSO2 = COD_SKILL_PASSO2.passos && COD_SKILL_PASSO2.passos.length > 0 ? (COD_SKILL_PASSO2.passos[0].COD_SKILL ? COD_SKILL_PASSO2.passos[0].COD_SKILL : null) : null;
      //console.log("Transferências: ",objColunasTabela["Transferencias"]);
      //console.log("Transferências parsed: ", JSON.parse(objColunasTabela["Transferencias"]));
      //console.log("COD_SKILL_PASSO2: ", COD_SKILL_PASSO2);

      objColunasTabela["COD_SKILL_PASSO2"] = COD_SKILL_PASSO2 ? COD_SKILL_PASSO2 : null;

      // Novas colunas inseridas ES-4403 21/03/2019 - Paulo Raoni
      objColunasTabela["VariaveisJSON"] =
        columns["VariaveisJSON"] !== undefined
          ? columns["VariaveisJSON"].value
          : undefined;
      var variaveisJSONParsed = !!objColunasTabela["VariaveisJSON"]
        ? JSON.parse(objColunasTabela["VariaveisJSON"].replace(/[\/\\&#@.+¨()$%'*!?<>]/g, ''))
        : undefined;

      for (item in variaveisJSONParsed) {
        variaveisJSONParsed[item] = variaveisJSONParsed[item].replace(/[{}:,"]/g, '');
      }
      var variaveisArray = cache.varjson;

      function insertVariaveisJSONParsedIntoGrid() {

        variaveisArray.forEach(function (el) {
          var key = el['codigo'];
          var value = el['nome'];
          if (!variaveisJSONParsed) {
            objColunasTabela[value] = undefined;
            return;
          }
          if (variaveisJSONParsed) {
            var valueJSON = variaveisJSONParsed[key] ? variaveisJSONParsed[key] : null;
            objColunasTabela[value] = valueJSON ? valueJSON : null;
          }

        });


      }


      insertVariaveisJSONParsedIntoGrid();


      objColunasTabela["ICS"] = columns["ICS"] !== undefined ? columns["ICS"].value : undefined;
      objColunasTabela["DATANASC"] = columns["DATANASC"] !== undefined ? columns["DATANASC"].value : undefined;
      objColunasTabela["DATAINST"] = columns["DATAINST"] !== undefined ? columns["DATAINST"].value : undefined;
      objColunasTabela["DATAINSTVELOX"] = columns["DATAINSTVELOX"] !== undefined ? columns["DATAINSTVELOX"].value : undefined;
      objColunasTabela["Statusfixo"] = columns["Statusfixo"] !== undefined ? columns["Statusfixo"].value : undefined;
      objColunasTabela["StatusVelox"] = columns["StatusVelox"] !== undefined ? columns["StatusVelox"].value : undefined;
      objColunasTabela["StatusMovel"] = columns["StatusMovel"] !== undefined ? columns["StatusMovel"].value : undefined;
      objColunasTabela["StatusOiTV"] = columns["StatusOiTV"] !== undefined ? columns["StatusOiTV"].value : undefined;
      objColunasTabela["GrupoDirigida1"] = columns["GrupoDirigida1"] !== undefined ? columns["GrupoDirigida1"].value : undefined;
      objColunasTabela["GrupoDirigida2"] = columns["GrupoDirigida2"] !== undefined ? columns["GrupoDirigida2"].value : undefined;
      objColunasTabela["GrupoDirigida3"] = columns["GrupoDirigida3"] !== undefined ? columns["GrupoDirigida3"].value : undefined;
      objColunasTabela["ElocucaoDirigida1"] = columns["ElocucaoDirigida1"] !== undefined ? columns["ElocucaoDirigida1"].value : undefined;
      objColunasTabela["ElocucaoDirigida2"] = columns["ElocucaoDirigida2"] !== undefined ? columns["ElocucaoDirigida2"].value : undefined;
      objColunasTabela["ElocucaoDirigida3"] = columns["ElocucaoDirigida3"] !== undefined ? columns["ElocucaoDirigida3"].value : undefined;

      /*objColunasTabela["ElocucaoRecVozDir1"] = columns["ElocucaoRecVozDir1"] !== undefined ? columns["ElocucaoRecVozDir1"].value : undefined;
      objColunasTabela["ResultadoRecVozDir1"] = columns["ResultadoRecVozDir1"] !== undefined ? columns["ResultadoRecVozDir1"].value : undefined;
      objColunasTabela["ScoreRecVozDir1"] = columns["ScoreRecVozDir1"] !== undefined ? columns["ScoreRecVozDir1"].value : undefined;
      objColunasTabela["ElocucaoRecVozDir2"] = columns["ElocucaoRecVozDir2"] !== undefined ? columns["ElocucaoRecVozDir2"].value : undefined;
      objColunasTabela["ResultadoRecVozDir2"] = columns["ResultadoRecVozDir2"] !== undefined ? columns["ResultadoRecVozDir2"].value : undefined;
      objColunasTabela["ScoreRecVozDir2"] = columns["ScoreRecVozDir2"] !== undefined ? columns["ScoreRecVozDir2"].value : undefined;
      objColunasTabela["ElocucaoRecVozDir3"] = columns["ElocucaoRecVozDir3"] !== undefined ? columns["ElocucaoRecVozDir3"].value : undefined;
      objColunasTabela["ResultadoRecVozDir3"] = columns["ResultadoRecVozDir3"] !== undefined ? columns["ResultadoRecVozDir3"].value : undefined;
      objColunasTabela["ScoreRecVozDir3"] = columns["ScoreRecVozDir3"] !== undefined ? columns["ScoreRecVozDir3"].value : undefined;
      objColunasTabela["ElocucaoRecVozDir4"] = columns["ElocucaoRecVozDir4"] !== undefined ? columns["ElocucaoRecVozDir4"].value : undefined;
      objColunasTabela["ResultadoRecVozDir4"] = columns["ResultadoRecVozDir4"] !== undefined ? columns["ResultadoRecVozDir4"].value : undefined;
      objColunasTabela["ScoreRecVozDir4"] = columns["ScoreRecVozDir4"] !== undefined ? columns["ScoreRecVozDir4"].value : undefined;
      objColunasTabela["ElocucaoRecVozDir5"] = columns["ElocucaoRecVozDir5"] !== undefined ? columns["ElocucaoRecVozDir5"].value : undefined;
      objColunasTabela["ResultadoRecVozDir5"] = columns["ResultadoRecVozDir5"] !== undefined ? columns["ResultadoRecVozDir5"].value : undefined;
      objColunasTabela["ScoreRecVozDir5"] = columns["ScoreRecVozDir5"] !== undefined ? columns["ScoreRecVozDir5"].value : undefined;*/


      try {
        if (typeof objColunasTabela["Transferencias"] !== "object") objColunasTabela["Transferencias"] = JSON.parse(objColunasTabela["Transferencias"]);
        var ultimo = objColunasTabela["Transferencias"].passos.length;
        for (var i = 0; i < ultimo; i++) {
          objColunasTabela["operacao" + (i + 2)] = objColunasTabela["Transferencias"].passos[i].OPERACAO_FINAL;
          objColunasTabela["empresa" + (i + 2)] = objColunasTabela["Transferencias"].passos[i].EMPRESA;
        }

      } catch (ex) {
        objColunasTabela["operacao2"] = "";
        objColunasTabela["empresa2"] = "";
        objColunasTabela["operacao3"] = "";
        objColunasTabela["empresa3"] = "";
        objColunasTabela["operacao4"] = "";
        objColunasTabela["empresa4"] = "";
        objColunasTabela["operacao5"] = "";
        objColunasTabela["empresa5"] = "";
        objColunasTabela["operacao6"] = "";
        objColunasTabela["empresa6"] = "";
        //console.log(ex);
      }
      // FIM PARSE JSON OBJ TRANSFERENCIAS
      // DETALHAMENTO FIM PT2     
      if (objColunasTabela["DATA_HORA_FIM"] !== null) objColunasTabela["DATA_HORA_FIM"] = formataDataHoraBR(objColunasTabela["DATA_HORA_FIM"]);
      objColunasTabela["DataHora_Inicio"] = formataDataHoraBR(objColunasTabela["DataHora_Inicio"]);
      objColunasTabela["DATA_HORA_INICIO_PASSO1"] = formataDataHoraBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]);

      // Novos dados inseridos ES-4181 01/02/2019 - Paulo Raoni
      objColunasTabela["BDAberto"] = columns["BDAberto"] !== null ? columns["BDAberto"].value : null;
      objColunasTabela["OSAberta"] = columns["OSAberta"] !== null ? columns["OSAberta"].value : null;
      objColunasTabela["URAResgateAnatel"] = columns["URAResgateAnatel"] !== null ? columns["URAResgateAnatel"].value : null;
      objColunasTabela["URAProduto"] = columns["URAProduto"] !== null ? columns["URAProduto"].value : null;
      objColunasTabela["URAOpcao"] = columns["URAOpcao"] !== null ? columns["URAOpcao"].value : null;

      //Nova colunas pela ES-5122 Brunno
      objColunasTabela["CodEmpresa"] = columns["CodEmpresa"] !== null ? columns["CodEmpresa"].value : null;
      objColunasTabela["CodSkill"] = columns["CodSkill"] !== null ? columns["CodSkill"].value : null;


      tempData += objColunasTabela.DATA_HORA_INICIO_PASSO1 + ';' +
        objColunasTabela.DataHora_Inicio + ';' +
        objColunasTabela.CodAplicacao + ';' +
        objColunasTabela.CodSegmento + ';' +
        objColunasTabela.NumANI + ';' +
        objColunasTabela.ClienteTratado + ';' +
        objColunasTabela.UltimoED + ';' +
        objColunasTabela.TransferID + ';' +
        objColunasTabela.CodRegra + ';' +
        (objColunasTabela.CtxID !== undefined ? "'" + objColunasTabela.CtxID : "") + ';' +
        (objColunasTabela.CALL_DISP !== undefined ? objColunasTabela.CALL_DISP : "") + ';' +
        (objColunasTabela.FL_TRANSF_EXT !== undefined ? objColunasTabela.FL_TRANSF_EXT : "") + ';' +
        (objColunasTabela.FL_TRANSFERENCIA !== undefined ? objColunasTabela.FL_TRANSFERENCIA : "") + ';';


      if (exibeNLU || exibeFibraRep || exibeContas || exibeReclamacao) {
        tempData += (objColunasTabela.confianca !== undefined ? objColunasTabela.confianca : "") + ';' +
          (objColunasTabela.elocucao !== undefined ? objColunasTabela.elocucao : "") + ';' +
          (objColunasTabela.resultado !== undefined ? objColunasTabela.resultado : "") + ';' +
          (objColunasTabela.reconhecimento !== undefined ? objColunasTabela.reconhecimento : "") + ';' +
          (objColunasTabela.score !== undefined ? objColunasTabela.score : "") + ';' +
          (objColunasTabela.tipo !== undefined ? objColunasTabela.tipo : "") + ';';
      }

      tempData += (objColunasTabela.OPERACAO_FINAL_PASSO1 !== undefined ? objColunasTabela.OPERACAO_FINAL_PASSO1 : "") + ';' +
        (objColunasTabela.EMPRESA_PASSO1 !== undefined ? objColunasTabela.EMPRESA_PASSO1 : "") + ';' +
        (objColunasTabela.operacao2 !== undefined ? objColunasTabela.operacao2 : "") + ';' +
        (objColunasTabela.empresa2 !== undefined ? objColunasTabela.empresa2 : "") + ';' +
        (objColunasTabela.operacao3 !== undefined ? objColunasTabela.operacao3 : "") + ';' +
        (objColunasTabela.empresa3 !== undefined ? objColunasTabela.empresa3 : "") + ';' +
        (objColunasTabela.operacao4 !== undefined ? objColunasTabela.operacao4 : "") + ';' +
        (objColunasTabela.empresa4 !== undefined ? objColunasTabela.empresa4 : "") + ';' +
        (objColunasTabela.operacao5 !== undefined ? objColunasTabela.operacao5 : "") + ';' +
        (objColunasTabela.empresa5 !== undefined ? objColunasTabela.empresa5 : "") + ';' +
        (objColunasTabela.operacao6 !== undefined ? objColunasTabela.operacao6 : "") + ';' +
        (objColunasTabela.empresa6 !== undefined ? objColunasTabela.empresa6 : "") + ';' +
        (objColunasTabela.SessaoRecVoz !== undefined ? objColunasTabela.SessaoRecVoz : "") + ';' +
        (objColunasTabela.Assunto !== undefined ? objColunasTabela.Assunto : "") + ';' +
        (objColunasTabela.DuracaoURA !== null ? objColunasTabela.DuracaoURA : "") + ';' +
        (objColunasTabela.DATA_HORA_FIM !== null ? objColunasTabela.DATA_HORA_FIM : "") + ';' +
        (objColunasTabela.BDAberto !== undefined ? objColunasTabela.BDAberto : "") + ';' +
        (objColunasTabela.OSAberta !== undefined ? objColunasTabela.OSAberta : "") + ';' +
        (objColunasTabela.URAResgateAnatel !== undefined ? objColunasTabela.URAResgateAnatel : "") + ';' +
        (objColunasTabela.URAProduto !== undefined ? objColunasTabela.URAProduto : "") + ';' +
        (objColunasTabela.URAOpcao !== undefined ? objColunasTabela.URAOpcao : "") + ';' +
        //Novas colunas abaixo - Paulo Raoni - última edição 26/03/2019
        (objColunasTabela.COD_SKILL_PASSO2 !== null ? objColunasTabela.COD_SKILL_PASSO2 : "") + ';' +
        (objColunasTabela.TAG_RECVOZ !== undefined ? objColunasTabela.TAG_RECVOZ : "") + ';' +
        (objColunasTabela.URA_BDABERTODENTRODOPRAZO !== undefined ? objColunasTabela.URA_BDABERTODENTRODOPRAZO : "") + ';' +
        (objColunasTabela.URA_BDABERTOFORADOPRAZO !== undefined ? objColunasTabela.URA_BDABERTOFORADOPRAZO : "") + ';' +
        (objColunasTabela.URA_BDVELOXABERTODENTRODOPRAZO !== undefined ? objColunasTabela.URA_BDVELOXABERTODENTRODOPRAZO : "") + ';' +
        (objColunasTabela.URA_BDVELOXABERTOFORADOPRAZO !== undefined ? objColunasTabela.URA_BDVELOXABERTOFORADOPRAZO : "") + ';' +
        (objColunasTabela.URA_BERCARIO !== undefined ? objColunasTabela.URA_BERCARIO : "") + ';' +
        (objColunasTabela.URA_BLACKLISTCREP !== undefined ? objColunasTabela.URA_BLACKLISTCREP : "") + ';' +
        (objColunasTabela.URA_BLOQUEIO_FIXA !== undefined ? objColunasTabela.URA_BLOQUEIO_FIXA : "") + ';' +
        (objColunasTabela.URA_CLIENTE_FIBRA !== undefined ? objColunasTabela.URA_CLIENTE_FIBRA : "") + ';' +
        (objColunasTabela.URA_HORARIOCREP !== undefined ? objColunasTabela.URA_HORARIOCREP : "") + ';' +
        (objColunasTabela.URA_MAILING_R2 !== undefined ? objColunasTabela.URA_MAILING_R2 : "") + ';' +
        (objColunasTabela.URA_OS !== undefined ? objColunasTabela.URA_OS : "") + ';' +
        (objColunasTabela.URA_PILOTOCREP !== undefined ? objColunasTabela.URA_PILOTOCREP : "") + ';' +
        (objColunasTabela.URA_QUARENTENACREP !== undefined ? objColunasTabela.URA_QUARENTENACREP : "") + ';' +
        (objColunasTabela.URA_REPETIDACREP !== undefined ? objColunasTabela.URA_REPETIDACREP : "") + ';' +
        (objColunasTabela.URA_RESGATEANATELST !== undefined ? objColunasTabela.URA_RESGATEANATELST : "") + ';' +
        (objColunasTabela.URA_TIPOBLOQUEIO !== undefined ? objColunasTabela.URA_TIPOBLOQUEIO : "") + ';' +
        (objColunasTabela.VIP_FLAG !== undefined ? objColunasTabela.VIP_FLAG : "") + ';' +
        (objColunasTabela.URA_BAIRRO !== undefined ? objColunasTabela.URA_BAIRRO : "") + ';' +
        (objColunasTabela.URA_CIDADE !== undefined ? objColunasTabela.URA_CIDADE : "") + ';' +
        (objColunasTabela.URA_MAILINGCONVERGENTE !== undefined ? objColunasTabela.URA_MAILINGCONVERGENTE : "") + ';' +
        (objColunasTabela.URA_INADIMPLENTE !== undefined ? objColunasTabela.URA_INADIMPLENTE : "") + ';' +
        (objColunasTabela.ICS !== undefined ? objColunasTabela.ICS : "") + ';' +
        (objColunasTabela.DATANASC !== undefined ? objColunasTabela.DATANASC : "") + ';' +
        (objColunasTabela.DATAINST !== undefined ? objColunasTabela.DATAINST : "") + ';' +
        (objColunasTabela.DATAINSTVELOX !== undefined ? objColunasTabela.DATAINSTVELOX : "") + ';' +
        (objColunasTabela.Statusfixo !== undefined ? objColunasTabela.Statusfixo : "") + ';' +
        //(objColunasTabela.StatusMovel !== undefined ? objColunasTabela.StatusMovel : "") + ';'+ 
        (objColunasTabela.StatusVelox !== undefined ? objColunasTabela.StatusVelox : "") + ';' +
        (objColunasTabela.StatusOiTV !== undefined ? objColunasTabela.StatusOiTV : "") + ';' +
        (objColunasTabela.GrupoDirigida1 !== undefined ? objColunasTabela.GrupoDirigida1 : "") + ';' +
        (objColunasTabela.GrupoDirigida2 !== undefined ? objColunasTabela.GrupoDirigida2 : "") + ';' +
        (objColunasTabela.GrupoDirigida3 !== undefined ? objColunasTabela.GrupoDirigida3 : "") + ';' +
        (objColunasTabela.ElocucaoDirigida1 !== undefined ? objColunasTabela.ElocucaoDirigida1 : "") + ';' +
        (objColunasTabela.ElocucaoDirigida2 !== undefined ? objColunasTabela.ElocucaoDirigida2 : "") + ';' +
        (objColunasTabela.ElocucaoDirigida3 !== undefined ? objColunasTabela.ElocucaoDirigida3 : "") + ';' +

        /*(objColunasTabela.ElocucaoRecVozDir1 !== undefined ? objColunasTabela.ElocucaoRecVozDir1 : "") + ';' +
        (objColunasTabela.ResultadoRecVozDir1 !== undefined ? objColunasTabela.ResultadoRecVozDir1 : "") + ';' +
        (objColunasTabela.ScoreRecVozDir1 !== undefined ? objColunasTabela.ScoreRecVozDir1 : "") + ';' +
        (objColunasTabela.ElocucaoRecVozDir2 !== undefined ? objColunasTabela.ElocucaoRecVozDir2 : "") + ';' +
        (objColunasTabela.ResultadoRecVozDir2 !== undefined ? objColunasTabela.ResultadoRecVozDir2 : "") + ';' +
        (objColunasTabela.ScoreRecVozDir2 !== undefined ? objColunasTabela.ScoreRecVozDir2 : "") + ';' +
        (objColunasTabela.ElocucaoRecVozDir3 !== undefined ? objColunasTabela.ElocucaoRecVozDir3 : "") + ';' +
        (objColunasTabela.ResultadoRecVozDir3 !== undefined ? objColunasTabela.ResultadoRecVozDir3 : "") + ';' +
        (objColunasTabela.ScoreRecVozDir3 !== undefined ? objColunasTabela.ScoreRecVozDir3 : "") + ';' +
        (objColunasTabela.ElocucaoRecVozDir4 !== undefined ? objColunasTabela.ElocucaoRecVozDir4 : "") + ';' +
        (objColunasTabela.ResultadoRecVozDir4 !== undefined ? objColunasTabela.ResultadoRecVozDir4 : "") + ';' +
        (objColunasTabela.ScoreRecVozDir4 !== undefined ? objColunasTabela.ScoreRecVozDir4 : "") + ';' +
        (objColunasTabela.ElocucaoRecVozDir5 !== undefined ? objColunasTabela.ElocucaoRecVozDir5 : "") + ';' +
        (objColunasTabela.ResultadoRecVozDir5 !== undefined ? objColunasTabela.ResultadoRecVozDir5 : "") + ';' +
        (objColunasTabela.ScoreRecVozDir5 !== undefined ? objColunasTabela.ScoreRecVozDir5 : "") + ';' +*/



        (objColunasTabela.CodEmpresa !== undefined ? objColunasTabela.CodEmpresa : "") + ';' +
        (objColunasTabela.CodSkill !== undefined ? objColunasTabela.CodSkill : "") + ';' + '\r\n';

      //COD_SKILL_PASSO2            
    }


    function retornaQuery(indice) {
      var obj = Object.keys(datasComDados);
      var stmtMod = stmt;
      var ini = stmtMod.match(/>= '(.*?)'/)[1];
      var fim = stmtMod.match(/<= '(.*?)'/)[1];
      if (indice > 0) stmtMod = stmtMod.replace(ini, datasComDados[obj[indice]].split('|')[0]);
      if (indice < obj.length - 1) stmtMod = stmtMod.replace(fim, datasComDados[obj[indice]].split('|')[1]);
      return stmtMod;
    }


    var controle = 0;
    var total = 0;


    function proximaHora(stmt) {

      function comOuSemDados(err, c, txt) {
        $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, Object.keys(datasComDados).length) + " Encontrados: " + total + " " + txt).selectpicker('refresh');
        controle++;
        $scope.$apply();

        //Executa dbquery enquanto array de querys menor que length-1
        if (controle < Object.keys(datasComDados).length) {
          proximaHora(retornaQuery(controle));
          if (c === undefined) console.log(controle);
        }

        //Evento fim
        if (controle === Object.keys(datasComDados).length) {
          if (c === undefined) console.log("fim " + controle + " " + new Date());
          if (err) {
            retornaStatusQuery(undefined, $scope);
          } else {
            retornaStatusQuery($scope.dados.length, $scope);
          }

          $btn_gerar.button('reset');
          if ($scope.dados.length > 0) {
            fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData, 'ascii');
            tempData = "";
            $btn_exportar.prop("disabled", false);
            $btn_exportar_csv.prop("disabled", false);
            $btn_exportar_dropdown.prop("disabled", false);
          } else {
            $btn_exportar.prop("disabled", true);
            $btn_exportar_csv.prop("disabled", true);
            $btn_exportar_dropdown.prop("disabled", true);
          }
        }
      }
      db.query(stmt, ($scope.dados.length < limite ? executaQuery1 : executaQuery2), function (err, num_rows) {
        console.log("Executando query-> " + stmt + " " + num_rows);
        num_rows !== undefined ? total += num_rows : total += 0;
        comOuSemDados(err, undefined, ($scope.dados.length < limite ? "" : "Inserindo em arquivo..."));
        if (num_rows > 0) {
          $btn_exportar.prop("disabled", false);
          $scope.gridOptions.api.setRowData($scope.dados);
        }
      });

    }



    db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
      console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);

      if ($globals.numeroDeRegistros === 0) {
        retornaStatusQuery(0, $scope);
        $btn_gerar.button('reset');
      } else {
        //Disparo inicial
        proximaHora(retornaQuery(0));
      }

    });

    //});

    // GILBERTOOOOOO 17/03/2014
    $view.on("mouseup", "tr.resumo", function () {
      var that = $(this);
      $('tr.resumo.marcado').toggleClass('marcado');
      $scope.$apply(function () {
        that.toggleClass('marcado');
      });
    });
  };

  // Exportar planilha XLSX
  $scope.exportaXLSX = function () {

    var $btn_exportar = $(this);
    $btn_exportar.button('loading');

    var colunas = [];
    for (i = 0; i < $scope.colunas.length; i++) {
      colunas.push($scope.colunas[i].displayName);
    }

    // var colunasFinal = [colunas];

    console.log(colunas);

    var dadosExcel = [colunas].concat($scope.csv);

    var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

    var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
    var wb = XLSX.utils.book_new();
    var milis = new Date();
    var arquivoNome = 'tChamadasCTGMore_' + formataDataHoraMilis(milis) + '.xlsx';
    XLSX.utils.book_append_sheet(wb, ws)
    console.log(wb);
    XLSX.writeFile(wb, tempDir3() + arquivoNome, {
      compression: true
    });
    console.log('Arquivo escrito');
    childProcess.exec(tempDir3() + arquivoNome);

    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  };
}

CtrlChamadasCTG2.$inject = ['$scope', '$globals'];
