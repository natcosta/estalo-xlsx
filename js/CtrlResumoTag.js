/*
 * CtrlED
 */
function CtrlResumoTag($scope, $globals) {

	win.title = "Resumo por tag"; //Brunno 11/03/2019
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 5000;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;


	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoED"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-resumo-tag", "");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	$scope.dados = [];
	$scope.csv = [];
	$scope.dadosMesAnterior = [];
	$scope.ordenacao = 'qtdEntrantes';
	$scope.decrescente = true;
	$scope.iit = false;
	$scope.tlv = false;
	$scope.atentende = false;
	$scope.ignorairrelevante = false;
	$scope.parcial = false;
	exibeContas= false 
	exibeFibraRep =false 
	exibeReclamacao = false
	ic = []

	$scope.log = [];

	$scope.aplicacoes = cache.apls; // FIXME: copy
	$scope.sites = [];
	$scope.segmentos = [];

	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		sites: [],
		segmentos: []
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;


	$scope.porDDD = null;

	$scope.edsMesAnterior = false;
	$scope.aba = 2;

	function geraColunas() {
		var array = [];


		array.push({
			field: "nomeTag",
			displayName: "Tag",
			width: 200,
			pinned: false
		});


		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];

		if (filtro_regioes.length > 0) {
			array.push({
				field: "ddd",
				displayName: "DDD",
				width: 50,
				pinned: false
			});
		}

		array.push(
		
			{
				field: "qtdExecucoes",
				displayName: "Execuções",
				width: 91,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			},  {
				field: "qtdChamadas",
				displayName: "Chamadas",
				width: 91,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			},  {
				field: "qtdEntrantes",
				displayName: "Entrantes",
				width: 91,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			}, {
				field: "qtdDerivadas",
				displayName: "Derivadas",
				width: 92,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			}, {
				field: "qtdFinalizadas",
				displayName: "Finalizadas",
				width: 110,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			}, {
				field: "qtdAbandonadas",
				displayName: "Abandonadas",
				width: 120,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			}, {
				field: "qtdRetidas",
				displayName: "Retidas",
				width: 84,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
			}, {
				field: "tma",
				displayName: "TMA",
				width: 67,
				pinned: false
			}, {
				field: "qtdPercDerivadas",
				displayName: "% Derivadas",
				width: 118,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:"" }}</span></div>'
			}, {
				field: "qtdPercFinalizadas",
				displayName: "% Finalizadas",
				width: 113,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:"" }}</span></div>'
			}, {
				field: "qtdPercAbandonadas",
				displayName: "% Abandonadas",
				width: 134,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:"" }}</span></div>'
			}

		);
		return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: mesAnterior(dat_consoli),
		max: dat_consoli*/
	};

	var $view;

	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-resumo-tag");

	

		$(".botoes").css({
			'position': 'fixed',
			'left': 'auto',
			'right': '25px',
			'margin-top': '35px'
		});

		$('input:checkbox[name=atendente]').css('margin', '1px 3px 3px 3px');
		  $view.on("change", 'input:checkbox[name=atendente]', function () {
			if ($("input[name='atendente']:checked").val() == 'exibeAtendente') {
			  document.getElementById("todos").checked = false;
			} 
		  });

		  $('input:checkbox[name=fibra]').css('margin', '1px 3px 3px 3px');
		  $view.on("change", 'input:checkbox[name=fibra]', function () {
			if ($("input[name='fibra']:checked").val() == 'exibeFibraRep') {
			  exibeFibraRep = true;
			  ic.push('ICDA077')
			  document.getElementById("todos").checked = false;
			} else {
			  exibeFibraRep = false;
			  ic.splice( ic.indexOf('ICDA077'), 1 );
			}
		  });
		  $('input:checkbox[name=reclamacao]').css('margin', '1px 3px 3px 3px');
		  $view.on("change", 'input:checkbox[name=reclamacao]', function () {
			if ($("input[name='reclamacao']:checked").val() == 'exibeReclamacao') {
			  exibeReclamacao = true;
			  ic.push('ICDERV007')
			  document.getElementById("todos").checked = false;
			} else {
			  exibeReclamacao = false;
			  ic.splice( ic.indexOf('ICDERV007'), 1 );
			}
		  });
		  $('input:checkbox[name=contas]').css('margin', '1px 3px 3px 3px');
		  $view.on("change", 'input:checkbox[name=contas]', function () {
			if ($("input[name='contas']:checked").val() == 'exibeContas') {
			  exibeContas = true;
			  ic.push('ICDERV008')
			  document.getElementById("todos").checked = false;
			} else {
			  exibeContas = false;
			  ic.splice( ic.indexOf('ICDERV008'), 1 );
			}
			

		  });
			  
		  $('input:checkbox[name=todos]').css('margin', '1px 3px 3px 3px');
		    $view.on("change", 'input:checkbox[name=todos]', function () {
			if ($("input[name='todos']:checked").val() == 'exibePadrao') {
				document.getElementById("reclamacao").checked = false;
				document.getElementById("fibra").checked = false;
				document.getElementById("contas").checked = false;
				document.getElementById("atendente").checked = false;
				exibeContas = false;
				ic.splice( ic.indexOf('ICDERV008'), 1 );
				exibeReclamacao = false;
				ic.splice( ic.indexOf('ICDERV007'), 1 );
				exibeFibraRep = false;
			    ic.splice( ic.indexOf('ICDA077'), 1 );
			}			

		  });

		  
		//minuteStep: 5

		//Alex 21/02/2014 datetime picker data separada

		//19/03/2014
		componenteDataMaisHora($scope, $view);

		carregaRegioes($view);
		carregaAplicacoes($view, false, false, $scope);
		carregaSites($view);
		carregaIcsRecVoz($view, true);  

		carregaSegmentosPorAplicacao($scope, $view, true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view)
		});


		carregaDDDsPorRegiao($scope, $view, true);
		// Popula lista de  ddds a partir das regioes selecionadas
		$view.on("change", "select.filtro-regiao", function () {

			$('#alinkApl').removeAttr('disabled');
			$(".filtro-aplicacao").attr('multiple', 'multiple');
			$('#HFiltroED').prop('checked', false);
			$(".filtro-movel-fixo").attr('multiple', 'multiple');
			//$('#HFiltroIC').prop('checked',false);
			$scope.filtros.isHFiltroED = false;
			//$scope.filtros.isHFiltroIC = false;
			$('#oueED').attr('disabled', 'disabled');
			$('#oueED').prop('checked', false);
			//$('#oueIC').attr('disabled', 'disabled');
			//$('#oueIC').prop('checked',false);
			//$('#iit').attr('disabled', false);

			var $sel_eds = $view.find("select.filtro-ed");
			$sel_eds.
			val([])
				.selectpicker('refresh');
			$sel_eds
				//.html("")
				.prop("disabled", "disabled")
				.selectpicker('refresh');



			carregaDDDsPorRegiao($scope, $view);

		});


		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});

		$view.find("select.filtro-segmento").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} segmentos'
		});


		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-tipo", function () {
			var filtro_tipos = $(this).val() || [];
			Estalo.filtros.filtro_tipo = filtro_tipos;
		});

		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-regiao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} regiões',
			showSubtext: true
		});


		//2014-11-27 transição de abas
		var abas = [2, 3];

		$view.on("click", "#alinkAnt", function () {

			if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
			if ($scope.aba > abas[0]) {
				$scope.aba--;
				mudancaDeAba();
			}
		});

		$view.on("click", "#alinkPro", function () {

			if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;
			if ($scope.aba < abas[abas.length - 1]) {
				$scope.aba++;
				mudancaDeAba();
			}

		});

		function mudancaDeAba() {
			abas.forEach(function (a) {
				if ($scope.aba === a) {
					$('.nav.aba' + a + '').fadeIn(500);
				} else {
					$('.nav.aba' + a + '').css('display', 'none');
				}
			});
		}


		//Marcar todos

		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function () {
			marcaTodosIndependente($('.filtro-ddd'), 'ddds')
		});

		//Bernardo 20-02-2014 Marcar todas as regiões
		$view.on("click", "#alinkReg", function () {
			marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
		});

		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function () {
			marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
		});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});



		//GILBERTO - change de segmentos

		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});


		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {
			limpaProgressBar($scope, "#pag-resumo-tag");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}

			//22/03/2014 Testa se uma aplicação foi selecionada
			var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
			if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
				setTimeout(function () {
					atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}
			

			obtemEdsDasAplicacoes($('select.filtro-aplicacao').val());

			var seguir = [];



			function teste() {
				setTimeout(function () {

					var itens = typeof $('select.filtro-aplicacao').val() === 'string' ? [$('select.filtro-aplicacao').val()] : $('select.filtro-aplicacao').val();

					for (var i = 0; i < itens.length; i++) {
						if (cache.aplicacoes[itens[i]].hasOwnProperty('estados')) {
							if (seguir.indexOf(itens[i]) < 0) seguir.push(itens[i]);
						}
					}

					if (seguir.length >= itens.length) {
						clearTimeout(teste);
						console.log("seguir daqui");
						
						$scope.colunas = geraColunas();
						$scope.listaDados.apply(this);
					} else {
						teste();
					}
				}, 500);
			}
			teste();



		});

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataMaisHora($scope, $view, true);

			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-resumo-tag");
		}

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});

	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {
		
		$scope.csv = [];

		$globals.numeroDeRegistros = 0;
		//if (!connection) {
		//  db.connect(config, $scope.listaChamadas);
		//  return;
		//}

		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_movel_fixo = $view.find("select.filtro-movel-fixo").val() || {};
		var filtro_perfil = "";
		if ($scope.filtro_atendente)  filtro_perfil = "'AT',";
		if ($scope.filtro_reclamacao)  filtro_perfil += "'RC',";
		if ($scope.filtro_contas)  filtro_perfil += "'CN',";
		if ($scope.filtro_fibra)  filtro_perfil += "'RP',";
		filtro_perfil = filtro_perfil.slice(0,-1);
		 
		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + "" +
			" até " + formataDataBR($scope.periodo.fim);
		$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		if (filtro_sites.length > 0) {
			$scope.filtros_usados += " Sites: " + filtro_sites;
		}
		if (filtro_segmentos.length > 0) {
			$scope.filtros_usados += " Segmentos: " + filtro_segmentos;
		}

		if(filtro_movel_fixo.length > 0) {$scope.filtros_usados += " Tipos: "+ filtro_movel_fixo;}

		if (filtro_regioes.length > 0 && filtro_ddds.length === 0) {
			var options = [];
			var v = [];
			filtro_regioes.forEach(function (codigo) {
				v = v.concat(unique2(cache.ddds.map(function (ddd) {
					if (ddd.regiao === codigo) {
						return ddd.codigo
					}
				})) || []);
			});
			unique(v).forEach((function (filtro_ddds) {
				return function (codigo) {
					var ddd = cache.ddds.indice[codigo];
					filtro_ddds.push(codigo);
				};
			})(filtro_ddds));
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
		} else if (filtro_regioes.length > 0 && filtro_ddds.length !== 0) {
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
			$scope.filtros_usados += " DDDs: " + filtro_ddds;
		}
		
		if(filtro_perfil !=""){
			tabela = ' ResumoNLUxIC '
		}else{
			tabela = ' ResumoNLU '
		}

		$scope.dados = [];
		$scope.dadosMesAnterior = [];

		var iit = "";
		var tlv = "";
		var entradaiit = ""
		var tempoIIT = "";
		var tempoTLV = "";
		var entradatlv = "";
		if ($scope.iit === false) {
			iit = "- ISNULL(QtdChamadasIIT, 0) ";
			tempoIIT = "- ISNULL(TempoTransfURA, 0) ";
			entradaiit = "- ISNULL(QtdUltimaTagIIT, 0) ";
		}
		if ($scope.tlv === false) {
			tlv = "- ISNULL(QtdChamadasTLV, 0)";
			tempoTLV = "- ISNULL(TempoTransfTLV, 0)";
			entradatlv = "- ISNULL(QtdUltimaTagTLV, 0)";
		}

		//19-03/2015 Parcial resumo ed



		var diaAtual = "";


		var incluiCampos;
		if (filtro_regioes.length !== 0 || filtro_ddds.length !== 0) {
			incluiCampos = function () {
				return ", DDD";
			};
			$scope.porDDD = true;
		} else {
			incluiCampos = function () {
				return "";
			};
			$scope.porDDD = false;
		}


		stmt = db.use + '';
		stmt += " Select" + testaLimite($scope.limite_registros) + "Tag"
			//+ " ,ED.Nom_Estado as descCod"
			+
			incluiCampos() +
			" ,ISNULL(sum(QtdFinalizadas),0) as f, ISNULL(sum(QtdDerivadas),0) as d, ISNULL(sum(QtdAbandonadas),0) as a," +
			" ISNULL(sum(QtdExecucoes),0) as e," +
			" ISNULL(sum(TempoEncerradas " + tempoIIT + tempoTLV + "),0) as t," +
			" ISNULL(sum(QtdChamadas " + iit + tlv + "),0) as c," +
			" ISNULL(sum(QtdUltimaTag "+ entradaiit + entradatlv+ "),0) as h" +
			" FROM " + db.prefixo + tabela +" as RE ";
			if(filtro_perfil != ""){
				stmt += " INNER JOIN ICRECVOZ IC ON re.CodAplicacao = ic.CodAplicacao and re.CodIc = ic.CodIc  "
			}
			stmt += " WHERE CONVERT(DATE,re.DatReferencia) >= '" + formataData(data_ini) + "'" +
			" AND CONVERT(DATE,re.DatReferencia) <= '" + formataData(data_fim) + "'"
			stmt += restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("re.CodSite", filtro_sites, true);
			if(filtro_perfil != ""){
				stmt += " AND PERFIL IN (" + filtro_perfil + ")";
			}

		
			stmt += restringe_consulta("re.DDD", filtro_ddds, true);
			//Testa se usuário filtrou segmentos, senão filtra o total de todos segmentos(Cod_Segmento = '') - Bernardo
			
			if (filtro_segmentos.length === 0) {
				stmt += " "
			} else {
				stmt += restringe_consulta("re.CodSegmento", filtro_segmentos, true)
			}

			stmt += " Group by Tag" + incluiCampos();
			stmt += " ORDER BY Tag" + incluiCampos();


		log(stmt);
		$scope.filtros_usados += $scope.parcial ? " <b>Registro PARCIAL</b>" : "";


		stmt2 = stmt.replace(formataData(data_ini), formataData(mesAnterior2(data_ini)));
		stmt2 = stmt2.replace(formataData(data_fim), formataData(data_ini));

		console.log("edsMesAnterior -> " + stmt2);

		if ($scope.edsMesAnterior === false) {
			stmt2 = "select getdate()";
		}



		function edsMesAnterior(columns) {

			if ($scope.edsMesAnterior === true) {

				var nomeTag = columns["Tag"].value,
					ddd = columns["DDD"] !== undefined ? +columns["DDD"].value : undefined,
					qtdFinalizadas = +columns["f"].value,
					qtdDerivadas = +columns["d"].value,
					qtdAbandonadas = +columns["a"].value,
					qtdExecucoes = +columns["e"].value,
					qtdChamadas = +columns["c"].value,
					qtdEntrantes = +columns["h"].value,
					qtdRetidas = qtdFinalizadas + qtdAbandonadas,
					tma = columns["t"] !== undefined ? (+columns["t"].value / qtdChamadas).toFixed(2) : undefined,
					qtdPercDerivadas = +((+columns["d"].value * 100) / qtdChamadas).toFixed(2),
					qtdPercFinalizadas = +((+columns["f"].value * 100) / qtdChamadas).toFixed(2),					
					qtdPercAbandonadas = +((+columns["a"].value * 100) / qtdChamadas).toFixed(2);

				$scope.dadosMesAnterior.push({
					nomeTag: nomeTag,
					ddd: ddd,	
					qtdExecucoes: qtdExecucoes,					
					qtdChamadas: qtdChamadas,					
					qtdEntrantes: qtdEntrantes,
					qtdDerivadas: qtdDerivadas,
					qtdFinalizadas: qtdFinalizadas,					
					qtdAbandonadas: qtdAbandonadas,					
					qtdRetidas: qtdRetidas,
					tma: tma > 0 ? tma : 0,
					qtdPercDerivadas: qtdPercDerivadas > 0 ? qtdPercDerivadas : 0,
					qtdPercFinalizadas: qtdPercFinalizadas > 0 ? qtdPercFinalizadas : 0,					
					qtdPercAbandonadas: qtdPercAbandonadas > 0 ? qtdPercAbandonadas : 0
				});
			}
		}

		function executaQuery(columns) {


			var nomeTag = columns["Tag"].value,
				ddd = columns["DDD"] !== undefined ? +columns["DDD"].value : undefined,
				qtdExecucoes = +columns["e"].value,
				qtdChamadas = +columns["c"].value,				
				qtdDerivadas = +columns["d"].value,
				qtdFinalizadas = +columns["f"].value,				
				qtdAbandonadas = +columns["a"].value,
				qtdEntrantes = +columns["h"].value ,
				qtdRetidas = qtdFinalizadas + qtdAbandonadas,
				tma = columns["t"] !== undefined ? (+columns["t"].value / qtdChamadas).toFixed(2) : undefined,
				qtdPercDerivadas = +((+columns["d"].value * 100) / qtdChamadas).toFixed(2),
				qtdPercFinalizadas = +((+columns["f"].value * 100) / qtdChamadas).toFixed(2),				
				qtdPercAbandonadas = +((+columns["a"].value * 100) / qtdChamadas).toFixed(2);

			$scope.dados.push({
				nomeTag: nomeTag,
				ddd: ddd,
				qtdExecucoes: qtdExecucoes,
				qtdChamadas: qtdChamadas,				
				qtdEntrantes: qtdEntrantes,
				qtdDerivadas: qtdDerivadas,
				qtdFinalizadas: qtdFinalizadas,
				qtdAbandonadas: qtdAbandonadas,				
				qtdRetidas: qtdRetidas,
				tma: tma > 0 ? tma : 0,
				qtdPercDerivadas: qtdPercDerivadas > 0 ? qtdPercDerivadas : 0,
				qtdPercFinalizadas: qtdPercFinalizadas > 0 ? qtdPercFinalizadas : 0,				
				qtdPercAbandonadas: qtdPercAbandonadas > 0 ? qtdPercAbandonadas : 0
			});
			
			if(ddd !==undefined){
				$scope.csv.push([
					nomeTag,	
					ddd,	
					qtdExecucoes,
					qtdChamadas,					
					qtdEntrantes,
					qtdDerivadas,
					qtdFinalizadas,				
					qtdAbandonadas,				
					qtdRetidas,
					tma > 0 ? tma : 0,
					qtdPercDerivadas > 0 ? qtdPercDerivadas : 0,
					qtdPercFinalizadas > 0 ? qtdPercFinalizadas : 0,				
					qtdPercAbandonadas > 0 ? qtdPercAbandonadas : 0
				])
			}else{
				$scope.csv.push([
					nomeTag,
					qtdExecucoes,					
					qtdChamadas,					
					qtdEntrantes,
					qtdDerivadas,
					qtdFinalizadas,				
					qtdAbandonadas,				
					qtdRetidas,
					tma > 0 ? tma : 0,
					qtdPercDerivadas > 0 ? qtdPercDerivadas : 0,
					qtdPercFinalizadas > 0 ? qtdPercFinalizadas : 0,				
					qtdPercAbandonadas > 0 ? qtdPercAbandonadas : 0
				])
			}

		}


		db.query(stmt2, edsMesAnterior, function (err, num_rows) {
			db.query(stmt, executaQuery, function (err, num_rows) {
				userLog(stmt, 'Exporta TXT', 2, err)
				if ($scope.edsMesAnterior === true) {

					var d = unique2($scope.dados.map(function (item) {
						return item.nomeTag
					}));
					var dma = unique2($scope.dadosMesAnterior.map(function (item) {
						return item.nomeTag
					}));

					var arquivo = 'EDsZeradosMesAnterior_' + formataDataHoraMilis(new Date()) + '.txt';

					dma.forEach(function (item) {

						if (d.indexOf(item) < 0) {
							fs.appendFileSync('' + tempDir() + arquivo, item + "\r\n");

							$scope.dados.push({
								nomeTag: item,
								ddd: 0,
								qtdExecucoes: 0,
								qtdChamadas: 0,								
								qtdEntrantes: 0,
								qtdDerivadas: 0,
								qtdFinalizadas: 0,								
								qtdAbandonadas: 0,								
								qtdRetidas: 0,
								tma: 0,
								qtdPercDerivadas: 0,
								qtdPercFinalizadas: 0,								
								qtdPercAbandonadas: 0
							});

							$scope.csv.push([
								item,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0
							])


						}
					}, abrirArquivo());

					function abrirArquivo() {
						setTimeout(function () {
							childProcess.exec('' + tempDir() + arquivo);
						}, 2000);
					}

				}
				console.log("Executando query-> " + stmt + " " + num_rows);
				retornaStatusQuery(num_rows, $scope);
				$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
				$btn_gerar.button('reset');
				if (num_rows > 0) {
					$btn_exportar.prop("disabled", false);
					$btn_exportar_csv.prop("disabled", false);
					$btn_exportar_dropdown.prop("disabled", false);
				}
			});
		});

		// GILBERTOOOOOO 17/03/2014
		$view.on("mouseup", "tr.estado_de", function () {
			var that = $(this);
			$('tr.estado_de.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});
	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var colunas = [];
		
		
		
        for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
        }

        // colunas.pop();

        console.log(colunas);

        var dadosExcel = [colunas].concat($scope.csv);

        var dadosComFiltro = [
            ['Filtros:' + $scope.filtros_usados]
        ].concat(dadosExcel);

        var ws = XLSX.utils.aoa_to_sheet(dadosExcel);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'resumoED_' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

	$scope.abreAjuda = function (link) {
		abreAjuda(link);
	}
}
CtrlResumoTag.$inject = ['$scope', '$globals'];