function CtrlProdutosUra($scope, $http, $globals) {

    win.title = "Cadastro de Produtos Vendidos"; //Bruno 22/03/2017
    $scope.versao = versao;

    $scope.criaPVData = []; // Dados do grid 

    //Colunas do grid 
    $scope.colunas = [
        { field: "codProd", enableFiltering: true, displayName: "Código do Produto", cellClass: "grid-align", width: '20%', pinned: false },//  cellClass:"span1", width: 80,
        { field: "desc", enableFiltering: true, displayName: "Descrição", cellClass: "grid-align", width: '34%', pinned: false },// cellClass:"span2",width: 80,
        { field: "gruprod", enableFiltering: true, displayName: "Grupo do Produto", cellClass: "grid-align", width: '34%', pinned: false },// cellClass:"span1", width: 150,
         { field: "edita", enableFiltering: false, displayName: "Edição", cellClass: "grid-align", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-view" ng-click="grid.appScope.EPV(row.entity)" title="Editar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];


    // Grid 
    $scope.gridDados = {
        data: $scope.criaPVData,
        columnDefs: $scope.colunas,
        enableColumnResize: false,
        enablePining: true,
        enableFiltering: true,
        exporterMenuPdf: false,
        filterOptions: $scope.filtros,
        showFilter: true,
        enableGridMenu: true,
        enableSelectAll: true,
        exporterCsvFilename: 'ProdutosURA.csv',
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    $scope.filtros = {
        filterText: "",
        useExternalFilter: true
    };
    // Filtro 
    $scope.filterUser = function () {
        var filterText = $scope.userFilter;
        if (filterText !== '') {
            $scope.filtros.filterText = filterText;
        } else {
            $scope.filtros.filterText = '';
        }
    };
    // Fim do Filtro
    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-pv-view");
        treeView('#pag-pv #chamadas', 15, 'Chamadas', 'dvchamadas');
        treeView('#pag-pv #repetidas', 35, 'Repetidas', 'dvRepetidas');
        treeView('#pag-pv #ed', 65, 'ED', 'dvED');
        treeView('#pag-pv #ic', 95, 'IC', 'dvIC');
        treeView('#pag-pv #tid', 115, 'TID', 'dvTid');
        treeView('#pag-pv #vendas', 145, 'Vendas', 'dvVendas');
        treeView('#pag-pv #falhas', 165, 'Falhas', 'dvFalhas');
        treeView('#pag-pv #extratores', 195, 'Extratores', 'dvExtratores');
        treeView('#pag-pv #parametros', 225, 'Parametros', 'dvParam');
        treeView('#pag-pv #admin', 255, 'Administrativo', 'dvAdmin');
        treeView('#pag-pv #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-pv #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');

        $("#pag-pv .botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });



        $view.find("select.filtro-rotas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Permissões',
            showSubtext: true
        });


        $("#pag-pv #modal-header").on('hidden', function () {
            $("#pag-pv .registerAlert").hide();
            $("#pag-pv .registerAlertText").text("");
            $("#pag-pv .container button").blur();
            console.log("Blurred buttons");
        });

        $("#pag-pv #modal-view").on('hidden', function () {
            $("#pag-pv .registerAlert").hide();
            $("#pag-pv .registerAlertText").text("");
            $("#pag-pv .container button").blur();
            console.log("Blurred buttons");
        });

        //preenchendo list com os relatorios

        var options2 = [];
        cache.relatorios.forEach(function (form) {

            options2.push('<option value="' + form.codProd + '">' + form.desc + form.gruprod + '</option>');

        });

        $scope.listaPV();
    });



    function populatePVGrid(columns) {

        var
            gridCodProd = columns[0].value,
            gridDesc = columns[1].value,
            gridGruProd = columns[2].value,

        dado = {

            codProd: gridCodProd,
            desc: gridDesc,
            gruprod: gridGruProd,

        };

        $scope.criaPVData.push(dado);
        $scope.gridDados.data = $scope.criaPVData;
    }


    // Função responsável por popular o grid 
    $scope.listaPV = function () {

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');
        $scope.criaPVData = [];

        console.log("Listando Produtos Vendidos");

        // Fim da permissão responsavel por popular o grid 

        listaPV(populatePVGrid, $scope);

        $btn_gerar.button('reset');

    };

    // Função responsável por REGISTRAR  produtos vendidos no banco de dados 

    $scope.registraPV = function() {
        $scope.exist = false;

        function validForm() {
            $("#pag-pv .registerAlertText").text("");
            if (($scope.codProd == "" || $scope.codProd == undefined) ||
                    ($scope.desc == "" || $scope.desc == undefined) ||
                ($scope.gruprod == "" || $scope.gruprod == undefined)
                ) {
                var resultstring = "Favor preencha ";
                console.log("Valor da Código do Produto: " + $scope.codProd);
                console.log("Valor da Descrição: " + $scope.desc);
                console.log("Valor do Grupo do Produto: " + $scope.gruprod);

                if ($scope.codProd == "" || $scope.codProd == undefined) {
                    resultstring = resultstring + "Código do Produto, ";
                    $scope.codProd = "";
                }
                if ($scope.desc == "" || $scope.desc == undefined) {
                    resultstring = resultstring + "Descrição, ";
                    $scope.desc = "";
                }
                if ($scope.gruprod == "" || $scope.gruprod == undefined) {
                    resultstring = resultstring + "Grupo do Produto, ";
                    $scope.gruprod = "";
                }
                

                resultstring = resultstring + " e tente novamente.";
                console.log("Error: " + resultstring);
                $("#pag-pv .registerAlertText").text(resultstring);
                $("#pag-pv .registerAlert").show();
                setTimeout(function () {
                    $("#pag-pv .registerAlert").hide();
                }, 3000);
                return false;
            }

            return true;
        }



        console.log("Connected? " + db.isConnected());

        if (validForm()) {

            registraPV($scope.codProd, $scope.desc, $scope.gruprod)
            $("#pag-pv #modal-pv-registro").modal('hide'); //fecha o modal 
            $("#pag-pv #modal-pv-registro input").val(""); // limpa os inputs
            

        } else {

        }

    };
    // Fim da função responsável por registrar produto vendido

    function listaPV(callBackPV) {
        array = [];
        pv = "Select CodProduto,Descricao,GrupoProduto From ProdutosURA";

        db.query(pv, callBackPV, function (err, num_rows) {

            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            retornaStatusQuery(num_rows, $scope);
            console.log("" + num_rows);
        });
    }

    /* Inserções do Bruno para função de produtos vendidos */
    function registraPV(codProd, desc, gruprod) {
        var stmt = db.use
        + "INSERT " + db.prefixo + "ProdutosURA (CodProduto,Descricao,GrupoProduto)"
        + " VALUES "
        + "('" + codProd + "','" + desc + "','" + gruprod + "')"


        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        // Cadastra o produto vendido
        db.query(stmt, function () { }, function (err, num_rows) {
            console.log("Executando query-> " + stmt);
            if (err) {
                console.log("Erro ao registrar: " + err);
                $("#pag-pv .registerAlertText").text("Erro ao registrar: " + err);
                $("#pag-pv .registerAlert").show();
                setTimeout(function () {
                    $("#pag-pv .registerAlert").hide();
                }, 3000);
            } else {
                console.log("Sem erros na db.query!");
                $("#pag-pv .registerAlertText").text("Produto Vendido registrado com sucesso");
                $("#pag-pv .registerAlert").show();
                document.getElementById("FormPV").reset();

                if ($scope.listaPV != undefined && $scope.listaPV != "") {
                    console.log("Scopo Lista PV");
                    $scope.listaPV();
                }
                setTimeout(function () {
                    $("#pag-pv .registerAlert").hide();
                }, 3000);
            }
        });
        // Fim do cadastro de produto vendido

    }
    // Inicio da função responsável por salvar a edição de produto vendido
    $scope.row = ""; // Variável responsável pela linha em edição no array de produto vendido
    $scope.EPV = function (row) {
        $scope.exist = false;

        console.log("Edita PV Valores: " + row + " Row: " + row);


        var editcodProd = row.codProd;
        var editdesc = row.desc;
        var editgruprod = row.gruprod;

        $scope.editcodProd = row.codProd;
        $scope.editdesc = row.desc;
        $scope.editgruprod = row.gruprod;

        console.log("Código do Produto: " + editcodProd);
        console.log("Descrição: " + editdesc);
        console.log("Grupo do Produto: " + editgruprod);
        $scope.row = row;
    };

    $scope.verifPV = function () {
        console.log('Verificação de Repetidos');
        $("#pag-pv .registerAlertText").text("");
        if (
            (($scope.editcodProd == "" || $scope.editcodProd == undefined) ||
             ($scope.editdesc == "" || $scope.editdesc == undefined) ||
            ($scope.editgruprod == "" || $scope.editgruprod == undefined)
            )
            ) {
            var resultstring = "Favor preencha ";

            console.log("Valor do Código do Produto: " + $scope.editcodProd);
            console.log("Valor da Descrição: " + $scope.editdesc);
            console.log("Valor do Grupo do Produto: " + $scope.editgruprod);

            if ($scope.editcodProd == "" || $scope.editcodProd == undefined) {
                resultstring = resultstring + "Código do Produto, ";
                $scope.editcodProd = "";
            }

            if ($scope.editdesc == "" || $scope.editdesc == undefined) {
                resultstring = resultstring + "Descrição, ";
                $scope.editdesc = "";
            }
            if ($scope.editgruprod == "" || $scope.editgruprod == undefined) {
                resultstring = resultstring + "Grupo do Produto, ";
                $scope.editgruprod = "";
            }

            resultstring = resultstring + " e tente novamente.";
            console.log("Error: " + resultstring);
            $("#pag-pv .registerAlertText").text(resultstring);
            $("#pag-pv .registerAlert").show();
            setTimeout(function () {
                $("#pag-pv .registerAlert").hide();
            }, 3000);
            return false;
        }
        return true;
    }

    $scope.EdPV= function () {
        row = $scope.row;
        $scope.exist = false;
        var editcodProd = row.codProd;
        var editdesc = row.desc;
        var editgruprod = row.gruprod;

        var stmt = db.use
+ "UPDATE " + db.prefixo + "ProdutosURA SET CodProduto= '" + $scope.editcodProd + "', Descricao='" + $scope.editdesc + "',  GrupoProduto='" + $scope.editgruprod + "' WHERE CodProduto='" + editcodProd + "'and  Descricao='" + editdesc + "'and  GrupoProduto='" + editgruprod + "'";

        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        if ($scope.verifPV()) {
            //edita o produto vendido
            db.query(stmt, function () { }, function (err, num_rows) {
                console.log("Executando query-> " + stmt);
                if (err) {
                    console.log("Erro ao editar: " + err);
                    $("#pag-pv .registerAlertText").text("Erro ao editar: " + err);
                    $("#pag-pv .registerAlert").show();
                    setTimeout(function () {
                        $("#pag-pv .registerAlert").hide();
                    }, 3000);
                } else {
                    console.log("Sem erros na db.query!");
                    $("#pag-pv .registerAlertText").text("Produto Vendido editado com sucesso");
                    $("#pag-pv .registerAlert").show();

                    if ($scope.listaPV != undefined && $scope.listaPV != "") {
                        console.log("Scopo Lista PV");
                        $scope.listaPV();
                    }
                    setTimeout(function () {
                        $("#pag-pv .registerAlert").hide();
                    }, 3000);
                }
            });


            // Fim do edita produto vendido
        }
    }
}
///// Fim Bruno
CtrlProdutosUra.$inject = ['$scope', '$globals'];
