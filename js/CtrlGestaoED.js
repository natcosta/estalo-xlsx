var changeAplicacao = false;

function CtrlGestaoED($scope, $globals) {
	
	function getClipboard(){		
		try{
			require('child_process').exec('clip < clip', function(err, stdout, stderr) {
				var texto = fs.readFileSync("clip");
				atualizaInfo($scope, "Valor "+texto+" copiado para área de transferência");						
			});
		}catch(ex){
			//console.log(ex);
		}
	}
	
    win.title = "Gestão estado de diálogo"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoED"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-gestao-estados", "Carregando últimos EDs cadastrados automaticamente em 7 dias...");
	
	limpaProgressBar($scope, "#pag-gestao-estados",true);
	$('.btn-gerar').button('loading');
    
	//Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
				rowTemplate: '<div ng-repeat="col in renderedColumns" ng-class="{\'sucesso\':row.getProperty(\'cod_status\') == 0,\'alerta\':row.getProperty(\'cod_status\') == 1 }" class="ngCell {{col.cellClass}} {{col.colIndex()}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.ordenacao = ['DataModificacaoReal','nomeEstado'];
    $scope.decrescente = true;
	$scope.especificoED = "";


    $scope.log = [];

    $scope.aplicacoes = cache.apls; // FIXME: copy


    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        classificacoes: [],
        estado: []
    };


    var $view;
	$scope.comVolume = true;
	$scope.configurado = false;
	$scope.importado = false;


    function geraColunas(){
      var array = [];
	  
      array.push(
        { field: "nomeEstado", displayName: "Estado", width: 200, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text ng-dblclick="copyClipboard(row.entity.nomeEstado)">{{row.entity.nomeEstado}}</span></div>' },
        { field: "nomeAplicacao", displayName: "Aplicação", width: 150, pinned: false },
        { field: "finalizacao", displayName: "Finalização", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.finalizacao" ng-click="mudar1(row.entity)" >' },
        { field: "fimUltestado", displayName: "Fin Ult Est", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.fimUltestado" ng-click="mudar3(row.entity)">' },
        { field: "abandUltestado", displayName: "Aband Ult Est", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.abandUltestado" ng-click="mudar2(row.entity)">' },
        { field: "contingencia", displayName: "Contingência", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.contingencia" ng-click="mudar(row.entity)">' },
        { field: "erro", displayName: "Erro", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.erro" ng-click="mudar(row.entity)">' },
        { field: "pesquisa", displayName: "Pesquisa", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.pesquisa" ng-click="mudar(row.entity)">' },
        { field: "irrelevante", displayName: "Irrelevante", width: 117, pinned: false, cellClass: "grid-align", cellTemplate: '<input type="checkbox" style = "margin: 7px;" ng-model="row.entity.irrelevante" ng-click="mudar(row.entity)">' },
		{ field: "dataModificacao", displayName: "Modificado em", width: 145, pinned: false }
      );

      return array;
    }

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-gestao-estados");
     
        carregaAplicacoesPorUsuario($view, function () { 
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
		});
        carregaClassificacoesED($view);
		
        $view.on("change", "select.filtro-aplicacao", function () {
            changeAplicacao = true;
            var filtro_aplicacoes = $(this).val() || [];
        });
        
        $("#pag-gestao-estados").on("click", ".filtro-aplicacao div.dropdown-menu.open ul li", function () {

            if (changeAplicacao === false) {
				$('select.filtro-aplicacao').val('').selectpicker('refresh');
			}			
			if($('select.filtro-aplicacao').val() === "Aplicacoes"){
				$scope.filtroEdPopulado2 = {};
				$scope.filtroEdPopulado = [];
				$scope.arrayFiltroEds = [];
				$scope.$apply();
			}
            changeAplicacao = false;

        });
		
		$view.find("select.filtro-classificacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Classificações',
            showSubtext: true
        });
		
		// Marca todas classificacoes
        $view.on("click", "#alinkClass", function () { marcaTodosIndependente($('.filtro-classificacao')) });

        // EXIBIR AO PASSAR O MOUSE
        $('div.btn-group.filtro-classificacao').mouseover(function () {
            $('div.btn-group.filtro-classificacao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.btn-group.filtro-classificacao').mouseout(function () {
            $('div.btn-group.filtro-classificacao').removeClass('open');
        });
		
		

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-gestao-estados");
			
			//22/03/2014 Testa se uma aplicação foi selecionada
			var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
			if (filtro_aplicacoes === 'Aplicacoes') {
				
				if($('.ui-select-multiple.ui-select-bootstrap input.ui-select-search').val() === ""){				
					setTimeout(function(){
						atualizaInfo($scope,'Selecione uma aplicação');
						effectNotification();
						$view.find(".btn-gerar").button('reset');
					},500);
					return;
				}else{
					$scope.especificoED = $('.ui-select-multiple.ui-select-bootstrap input.ui-select-search').val();
				}
			}			
             $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
		});
		
		$view.on("click", "#gestaoInput", function () {
            files = document.getElementById('gestaoInput');	
			if (files.addEventListener) files.addEventListener('change', handleFile, false);
			document.getElementById('gestaoInput').value = "";	  
		});

        //Salvar dados do table
        $view.on("click", ".btn-salvar", function () {    
			$(".modal").css("z-index","1050");

        });	
		
		$view.on("click", ".ui-select-multiple.ui-select-bootstrap input.ui-select-search", function () {
			if (!fs.existsSync('clip')) return;
			try{
			var texto = fs.readFileSync("clip");
			$('.ui-select-multiple.ui-select-bootstrap input.ui-select-search').val(texto);
			fs.unlinkSync("clip");
			}catch(ex){
				//console.log(ex);
			}
		
		});

        $view.on("click", "#btn-salvarED", function () {
			$(".modal").css("z-index","1050");
                $scope.salvaDados.apply(this);        
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);          
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
         
            $('select.filtro-classificacao').val('').selectpicker('refresh');
            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");         
            $btn_limpar.button('loading');       
            $scope.listaDados.apply(this);


            setTimeout(function () {
                $btn_limpar.button('reset');             
            }, 500);
        }
        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
			
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
			$('#btn-salvarED').button('reset');
			$scope.especificoED = "";
            abortar($scope, "#pag-gestao-estados");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');

        //Bernardo 10/09/2014
        function carregaClassificacoesED(view) {
            var optClass = [];
            //Bernardo 10/09/2014
            var classificacoesED = [
              { codigo: 0, nome: "Finalização" },
              { codigo: 1, nome: "Finalização Ult Estado" },
              { codigo: 2, nome: "Abandono Ult Estado" },
              { codigo: 3, nome: "Contingência" },
              { codigo: 4, nome: "Erro" },
              { codigo: 5, nome: "Pesquisa" },
              { codigo: 6, nome: "Irrelevante" }           
            ];
    
            optClass.push('<option value="">' + "Classificações de ED's" + '</option>');
            var optClass = classificacoesED.map(function (array) {
                return '<option value="' + array.codigo + '">' + array.nome + '</option>';
            });
            view.find("select.filtro-classificacao").html(optClass.join());
        }
    });
	
  function obtemCodAplicacao (n){
	  return unique2(cache.apls.map(function(a){if( a.nome === n) return a .codigo}))[0];
  }
	
	function retornaModelo(campo,apl,boleano,estados,index,codApl){		
		var comp = "'"+apl+"'";		
		if(!codApl) comp = "'"+obtemCodAplicacao(apl)+"'" ;		
		var modelo = "update Estado_Dialogo set "+campo+" = '"+boleano+"', Configurado = 'S', DataModificacao = getdate() where Nom_Estado in ('"+estados+"')"
		+" and Cod_Aplicacao = "+comp+";";		
		modelo += " insert into Log_Estado_Dialogo values ("
		+ " '" + index + "', '" + apl + "',"
		+ " '" + loginUsuario + "',getdate(),"
		+ " '"+campo+":" + boleano + "("+estados.replace(/','/g,",")+")');";		
		return modelo;
	}


    var sqls = [];
    //contador das queries
    var contador = 0;

    //Função para salvar dados
    $scope.salvaDados = function () {

        limpaProgressBar($scope, "#pag-gestao-estados");
        sqls = [];
        contador = 0;
        var $btn_salvar = $(this);
        //testa se existem dados na table
        if ($scope.dados.length === 0) {
            atualizaInfo($scope, '<font color = "white">Não há dados para salvar.</font>');
            $btn_salvar.button('reset');
            return;
        }

        $btn_salvar.button('loading');
		
	  var campos = ['finalizacao','fimUltestado','abandUltestado','contingencia','erro','pesquisa','irrelevante'];
	  var espelhos = ['Flag_Finalizacao','IndicFimUltEstado','IndicAbandonoUltEstado','IndicContingencia','IndicErro','IndicPesquisa','Indic_Exibe'];
	 
	  var obj = {};
	  var updates = "";
	  
	  $scope.dados.forEach(function(d){	
		  d.cod_status = 0;
		  for (var j = 0; j < campos.length; j++){
			  var irrelevante = "";
			  if(campos[j] === 'irrelevante'){
				  if(d[campos[j]] === true) {
					  irrelevante = "N";
				  }else if(d[campos[j]] === false){
					  irrelevante= "S";
				  }
			  }else{
				  if(d[campos[j]] === true) {
					  irrelevante = "S";
				  }else if(d[campos[j]] === false){
					  irrelevante= "N";
				  }
			  }
			  if(obj[espelhos[j]+'_'+d["codAplicacao"]+'_'+irrelevante] === undefined){
				  obj[espelhos[j]+'_'+d["codAplicacao"]+'_'+irrelevante] =  {eds:[d["nomeEstado"]], campo: espelhos[j], aplicacao: d["codAplicacao"], boleano: irrelevante};
			  }else{
				  obj[espelhos[j]+'_'+d["codAplicacao"]+'_'+irrelevante].eds.push(d["nomeEstado"]);
			  }
		 }
	  });
	  
	  Object.keys(obj).forEach(function(key,index) {
		  //console.log(key, obj[key]);
		  updates += retornaModelo(obj[key].campo,obj[key].aplicacao,obj[key].boleano,obj[key].eds.join("','"),index,true);
		  sqls.push(updates);
		  updates = "";
		  });

        //executa cada update
        executarQuery(sqls[0]);
    };
	
	
	var afetados = 0;

    //Função para gravar cada query
    function executarQuery(sQuery) {
        db.query(sQuery, null, function (err, num_rows) {
            console.log("Executando query-> " + sQuery + " " + num_rows);
            userLog(sQuery, 'Atualiza registros', 3, err)
            if (err) {
                console.log("ERRO: " + err);
            } else {
                console.log("O UPDATE retornou " + num_rows + " registros");
			}

            contador++;
			afetados = $scope.dados.length;			
			
            if (contador <= sqls.length - 1) {
                executarQuery(sqls[contador]);				
            } else {      
				if($scope.importado){
					atualizaInfo($scope, afetados+" registro(s) importado(s) salvo(s) com sucesso");						
				}else{
					atualizaInfo($scope, afetados+" registro(s) salvo(s) com sucesso");	
					//$scope.listaDados.apply(this);
				}
					afetados = 0;			
                $('.btn-salvar').button('reset');
                $('#btn-salvarED').button('reset');
                $('#confirmaUpdate').trigger('click');
            }
        });
    }	
	
	 $scope.copyClipboard = function(item) {
			fs.writeFileSync("clip",item);
			getClipboard();		 
			console.log(item);
	 };

    $scope.mudar1 = function (item) {

      if(item.finalizacao === true){
          item.abandUltestado = false;
          item.fimUltestado = false;
      }else{
		  item.abandUltestado = false;
          item.fimUltestado = false;
	  }

    };

    $scope.mudar2 = function (item) {

        if (item.abandUltestado === true) {
          item.finalizacao = false;
          item.fimUltestado = false;
		}else{
		  item.finalizacao = false;
          item.fimUltestado = false;
		}

    };

    $scope.mudar3 = function (item) {

        if (item.fimUltestado === true) {
            item.abandUltestado = false;
            item.finalizacao = false;
        }else{
			item.abandUltestado = false;
            item.finalizacao = false;
		}

    };

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {
		
		$scope.ordenacao = ['DataModificacaoReal','nomeEstado'];
		
		afetados = 0;
		$scope.importado = false;
		$(".modal").css("z-index","-1");

        $globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
		$btn_exportar.prop("disabled", true);
        var $btn_gerar = $(this);
        $btn_gerar.button('loading');



        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
     
        var filtro_classificacoes = $view.find("select.filtro-classificacao").val() || [];

        if (filtro_aplicacoes.length === 0) {
            atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
            $btn_gerar.button('reset');
            return;
        }

        $scope.dados = [];


        var stmt = " Select Nom_Estado,Cod_Aplicacao,Flag_Finalizacao,IndicContingencia,"
        + " IndicErro,IndicPesquisa,Indic_Exibe,IndicAbandonoUltEstado,IndicFimUltEstado, Configurado,"
		+ " case when datamodificacao > datainclusao then datamodificacao else datainclusao end as DataModificacao"
        + " FROM Estado_Dialogo"
        + " WHERE 1=1  "
		+ " and Nom_Estado not like '%[0-9][0-9][0-9][0-9][/_+-][0-9][0-9][/_+-][0-9][0-9][/_+-][0-9][0-9][/_+-][0-9][0-9][/_+-][0-9][0-9][/_+-][0-9][0-9][0-9]%'"
      
		if($scope.especificoED !== ""){
			stmt += " and Nom_Estado like '%"+$scope.especificoED+"%'"
			+" and Cod_Aplicacao in ( "
			+" Select codAplicacao from Responsavel_Aplicacao where"
			+" LoginUsuario ='" + loginUsuario + "' and Dominio = '" + dominioUsuario + "')";				
		}else{
			stmt += restringe_consulta("Cod_Estado", $scope.filtroEDSelect, true);
			if (filtro_aplicacoes !== 'Aplicacoes') stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
			stmt += restringe_classificacao(filtro_classificacoes);
		}
		if(!$scope.configurado) stmt += " and CadastroAutomatico = 'S' and (Configurado <> 'S' or Configurado is null)";
        
		if($scope.comVolume){
			//Desativar gestão de ED´s de aplicações sem volumetria
			stmt += " and Cod_Estado in ( "
			+" select distinct Cod_Estado_Dialogo from Resumo_EncerramentoURAVOZD "
			+" where Dat_Referencia >= convert (date,GETDATE()-7) "
			+" and Dat_Referencia < convert (date,GETDATE()) "
			+" and Cod_Aplicacao in ( "
			+" Select codAplicacao from Responsavel_Aplicacao where"
			+" LoginUsuario ='" + loginUsuario + "' and Dominio = '" + dominioUsuario + "')"
			+")"
			+" and DataInclusao >= convert (date,GETDATE()-7)"
		}
		
        stmt += " order by DataInclusao";
        log(stmt);


        var stmtCountRows = stmtContaLinhas(stmt);


        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery(columns) {
            var nomeEstado = columns["Nom_Estado"].value,
                codAplicacao = columns["Cod_Aplicacao"].value,
                nomeAplicacao = columns["Cod_Aplicacao"].value,
                finalizacao = columns["Flag_Finalizacao"].value === "S" ? true : false,
                abandono = columns["IndicAbandonoUltEstado"].value === "S" ? true : false,
                fimUltestado = columns["IndicFimUltEstado"].value === "S" ? true : false,
                contingencia = columns["IndicContingencia"].value === "S" ? true : false,
                erro = columns["IndicErro"].value === "S" ? true : false,
                pesquisa = columns["IndicPesquisa"].value === "S" ? true : false,
                irrelevante = columns["Indic_Exibe"].value !== "S" ? true : false,
				configurado = columns["Configurado"].value === "S" ? true : false,
                excluido = false,
				dataModificacaoReal = columns["DataModificacao"].value,
				dataModificacao = columns["DataModificacao"].value ? formataDataHoraBR(columns["DataModificacao"].value): columns["DataModificacao"].value;

            $scope.dados.push({				
                nomeEstado: nomeEstado,
                codAplicacao: codAplicacao,
                nomeAplicacao: obtemNomeAplicacao(codAplicacao),
                finalizacao: finalizacao,
                fimUltestado: fimUltestado,
                abandUltestado: abandono,
                contingencia: contingencia,
                erro: erro,
                pesquisa: pesquisa,
                irrelevante: irrelevante,
                excluido: excluido,
				cod_status: configurado ? 0 : 1,
				dataModificacao: dataModificacao,
				dataModificacaoReal: dataModificacaoReal
            });
            if ($scope.dados.length % 100 === 0) {
                $scope.$apply();
            }
        }

        //Bernardo 12/09/2014
        function restringe_classificacao(filtro) {
            var strClass = "";
            if (filtro.length > 0) {
                //strClass = "(";
                for (var i = 0; i < filtro.length; i++) {
                    var operador = i === 0 ? " and (" : " or ";
                    switch (filtro[i]) {
                        case "0":
                            strClass += operador + "Flag_Finalizacao = 'S'";
                            break;
                        case "1":
                            strClass += operador + "indicFimUltEstado = 'S'";
                            break;
                        case "2":
                            strClass += operador + "indicAbandonoUltEstado = 'S'";
                            break;
                        case "3":
                            strClass += operador + "IndicContingencia = 'S'";
                            break;
                        case "4":
                            strClass += operador + "IndicErro = 'S'";
                            break;
                        case "5":
                            strClass += operador + "IndicPesquisa = 'S'";
                            break;
                        case "6":
                            strClass += operador + "Indic_Exibe <> 'S'";
                            break;
                        case "7":
                    }
                }
                strClass += ")"
            }
            return strClass;
        }
        
		
        db.query(stmt, executaQuery, function (err, num_rows) {
			$(".modal").css("z-index","-1");
            retornaStatusQuery(num_rows, $scope);
            if ($scope.comVolume) {

                var notificacao = $('.notification .ng-binding').text();
                atualizaInfo($scope, '<font color = "white">' + notificacao + ' Sempre ao abrir a tela, serão listados os últimos EDs cadastrados automaticamente e com volumetria, nos últimos 7 dias.</font>');
            }
            
			$('.btn-gerar').button('reset');  
			
            if (num_rows > 0) {				
				$($('.colt2')[0]).attr('title','Classifica a chamada como finalização');
				$($('.colt3')[0]).attr('title','Classifica a chamada como finalização, caso seja o último ED relevante da chamada');
				$($('.colt4')[0]).attr('title','Classifica a chamada como abandono, caso seja o último ED relevante da chamada');
				$($('.colt5')[0]).attr('title','ED de contingência (Depreciado)');
				$($('.colt6')[0]).attr('title','ED de erro de aplicação (Depreciado)');
				$($('.colt7')[0]).attr('title','ED de pesquisa de satisfação (Depreciado)');
				$($('.colt8')[0]).attr('title','A classificação da chamada é considerada no ED antecessor');
				$($('.colt9')[0]).attr('title','Exibe a data de criação para ED não configurado');
                $btn_exportar.prop("disabled", false);
            }
			$scope.especificoED = "";
        });
  
    };

//Bernardo - 10/09/2014 - Função que carrega aplicações, de acordo com responsabilidade do usuário
function carregaAplicacoesPorUsuario(view,callback) {

    var sql = "Select R.CodAplicacao,A.Descricao from Responsavel_Aplicacao R inner join MapeamentoAplicacoes A"
    + " on R.CodAplicacao = A.CodAplicacao"
    + " where LoginUsuario ='" + loginUsuario + "' and Dominio = '" + dominioUsuario + "'";

    var optApl = [];

    optApl.push('<option data-hidden="true" value="Aplicacoes">Aplicações</option>')
    function executaQuery(columns) {
        var codAplicacao = columns["CodAplicacao"].value,
            nomeAplicacao = columns["Descricao"].value;
        optApl.push('<option value="' + codAplicacao + '">' + nomeAplicacao + '</option>');

    }
    db.query(sql, executaQuery, function (err, num_rows) {
        console.log("Executando query-> " + sql + " " + num_rows);
        userLog(sql, 'Função que carrega aplicações, de acordo com responsabilidade do usuário', 2, err)
        view.find("select.filtro-aplicacao").html(optApl.filter(function (i) { return i }).join()).selectpicker('refresh');  
        if (callback) callback();

    });
}


// Exporta planilha XLSX
  $scope.exportaXLSX = function () {
    var $btn_exportar = $(this);

    $btn_exportar.button('loading');

    var linhas  = [];
    linhas.push($scope.colunas.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) {
			if(col.displayName !== "Editar" && col.displayName !== "Excluir") return col.displayName; 
		} 
	}));
    $scope.dados.forEach(function (dado) {
		linhas.push($scope.colunas.filterMap(function (col,index) {
			if (col.visible || col.visible === undefined) { 
					if (col.field === "dataModificacao"){
						return dado[col.field] ? dado[col.field] : "";
					}else{
						return dado[col.field] ? (dado[col.field] === true ? "S" : dado[col.field]) : "N"; 
					}
			} 
		}));
    });
	
	var works = [];
	
	if(linhas.length > 0) works.push({ name: "Gestão ED", data: linhas, table: true });


    var planilha = {
    creator: "Estalo",
    lastModifiedBy: loginUsuario || "Estalo",
    worksheets: works,
    autoFilter: false,    
    dataRows: { first: 1 }
  };

    var xlsx = frames["xlsxjs"].window.xlsx;
    planilha = xlsx(planilha, 'binary');
	
    var caminho = tempDir3()+"gestaoED" +"_"+ formataDataHoraMilis(new Date()) + ".xlsx";
    fs.writeFileSync(caminho, planilha.base64, 'base64');
    childProcess.exec(caminho);

    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  };  
  
 function handleFile(e) {
	 afetados = 0;
	 $scope.dados = [];
	 $scope.importado = true;
	 
	  limpaProgressBar($scope, "#pag-gestao-estados");
	 
	  sqls = [];
      contador = 0;
	 
	  var campos = ['Flag_Finalizacao','IndicFimUltEstado','IndicAbandonoUltEstado','IndicContingencia','IndicErro','IndicPesquisa','Indic_Exibe'];
	 
	  var obj = {};
	  var updates = "";
	  var files = e.target.files;
	  var workbook ="";
	  var csv = "";
	  
	  try{
		workbook = XLSX.readFile(files[0].path);
		csv = XLSX.utils.sheet_to_csv(workbook.Sheets["Gestão ED"]);

	  }catch(ex){
		return;
	  }
	  
	  var linhas = csv.split('\n');
	  
	for (var i = 1; i < linhas.length; i++){
	  if(linhas[i]!=""){//tratar linha em branco
	  
			  var s = {};
			  var cps = linhas[i].split(',');
					for (var j = 0; j < campos.length; j++){
						  var nomeEstado = cps[0],
						nomeAplicacao = cps[1],
						codAplicacao = obtemCodAplicacao(nomeAplicacao),						
						finalizacao = cps[2].toUpperCase() === "S" ? true : false,
						fimUltestado = cps[3].toUpperCase() === "S" ? true : false,
						abandono = cps[4].toUpperCase() === "S" ? true : false,						
						contingencia = cps[5].toUpperCase() === "S" ? true : false,
						erro = cps[6].toUpperCase() === "S" ? true : false,
						pesquisa = cps[7].toUpperCase() === "S" ? true : false,
						irrelevante = cps[8].toUpperCase() === "S" ? true : false,
						configurado = false,
						excluido = false;


					s = {				
						nomeEstado: nomeEstado,
						codAplicacao: codAplicacao,
						nomeAplicacao: nomeAplicacao,
						finalizacao: finalizacao,
						fimUltestado: fimUltestado,
						abandUltestado: abandono,
						contingencia: contingencia,
						erro: erro,
						pesquisa: pesquisa,
						irrelevante: irrelevante,
						excluido: excluido,
						cod_status: configurado ? 0 : 1
					};				
					
			  }
			  
					if(s.nomeEstado !== "" && codAplicacao !== undefined){					
						$scope.dados.push(s);					
					}
					if ($scope.dados.length % 100 === 0) {
						$scope.$apply();
					}
		  }
	  }
	  if($scope.dados.length > 0){
		  atualizaInfo($scope,"Dados importados com sucesso");
		  $scope.$apply();
	  }else{
		  atualizaInfo($scope,"Nenhum dado importado");
	  }
  }
}

CtrlGestaoED.$inject = ['$scope', '$globals'];