function CtrlDerivacaoVDN($scope, $globals) {

    win.title="Derivações por VDN"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;


    $scope.limite_registros = 5000;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao(""); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-derivacao-vdn", "Derivação por VDN");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];

    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    //Alex 14/03/2014 - x usado para exibir "" na view
    $scope.derivadas = "x";
    $scope.finalizadas = "x";
    $scope.abandonadas = "x";
    $scope.entrantes = "x";
    $scope.percDerivadas = "x";
    $scope.percFinalizadas = "x";
    $scope.percAbandonadas = "x";
    $scope.somaPercs = "x";
    $scope.ordenacao = 'site';
    $scope.decrescente = false;


    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: []
    };

    $scope.aba = 2;

    function geraColunas(){
      var array = [];

      array.push(
        { field: "site", displayName: "Site", width: 50, pinned: true },
        { field: "vdn", displayName: "VDN", width: 100, pinned: true },
        { field: "grupo", displayName: "Grupo", width: 100, pinned: true },
        { field: "operacao", displayName: "Operação", width: 100, pinned: true },
        { field: "quantidade", displayName: "Qtd", width: 100, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "percChamadas", displayName: "% chamadas", width: 110, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
        { field: "percChamadasAcumuladas", displayName: "% chamadas acum.", width: 160, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
        { field: "percDerivadas", displayName: "% derivadas", width: 110, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
        { field: "percDerivadasAcumuladas", displayName: "% derivadas acum.", width: 160, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' }
      );


      return array;
    }

    // Filtros: data e hora
    var agora = new Date();


    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
        };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-derivacao-vdn");
      treeView('#pag-derivacao-vdn #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-derivacao-vdn #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-derivacao-vdn #ed', 65,'ED','dvED');
      treeView('#pag-derivacao-vdn #ic', 95,'IC','dvIC');
      treeView('#pag-derivacao-vdn #tid', 115,'TID','dvTid');
      treeView('#pag-derivacao-vdn #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-derivacao-vdn #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-derivacao-vdn #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-derivacao-vdn #parametros', 225,'Parametros','dvParam');
      treeView('#pag-derivacao-vdn #admin', 255,'Administrativo','dvAdmin');
	  treeView('#pag-derivacao-vdn #monitoracao', 275, 'Monitoração', 'dvReparo');
      treeView('#pag-derivacao-vdn #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      /*$('.nav.aba3').css('display','none');*/

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

      //19/03/2014
      componenteDataHora ($scope,$view);

      //ALEX - Carregando filtros
      carregaAplicacoes($view,false,false,$scope);
      carregaSites($view);

      carregaSegmentosPorAplicacao($scope,$view,true);
      //GILBERTO - change de filtros

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});



    $view.find("select.filtro-aplicacao").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} aplicações',
        showSubtext: true
    });

    $view.find("select.filtro-site").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} Sites/POPs',
        showSubtext: true
    });

    $view.find("select.filtro-segmento").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} segmentos'
    });

    $view.find("select.filtro-op").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} operações'
    });



    $view.on("change", "select.filtro-site", function () {
        var filtro_sites = $(this).val() || [];
        Estalo.filtros.filtro_sites = filtro_sites;
    });


      //2014-11-27 transição de abas
      var abas = [2,3];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


    //Marcar todos

    // Marca todos os sites
    $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

    // Marca todos os segmentos
    $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

    //Marcar todas aplicações
    $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

    // Marca todos os operações
    $view.on("click", "#alinkOp", function(){ marcaTodosIndependente($('.filtro-op'),'ops')});



//GILBERTO - change de segmentos

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        $view.on("change", "select.filtro-op", function () {
            Estalo.filtros.filtro_operacoes = $(this).val();
        });


        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-op').mouseover(function () {
          if(!$('div.btn-group.filtro-op .btn').hasClass('disabled')){
            $('div.btn-group.filtro-op').addClass('open');
            $('div.btn-group.filtro-op>div>ul').css({'max-height':'600px','overflow-y':'auto','min-height':'1px'});
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-op').mouseout(function () {
            $('div.btn-group.filtro-op').removeClass('open');
        });


        var limite = 999;
        $scope.valorCol = limite;



        // Exibe mais registros
        $scope.exibeMaisRegistros = function () {
          $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        };


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-derivacao-vdn");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });



        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-derivacao-vdn");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });




    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_operacoes = $view.find("select.filtro-op").val() || [];



        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
    + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }




        $scope.dados = [];


    var tabelas = tabelasParticionadas($scope, 'VDNsOUTPorEstadoDialogoD', false);
    var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataData(data_fim) + "'";

      tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

      stmt += "SELECT TOP " + $scope.limite_registros
      + " VED.Cod_Site as CodSite, VED.Cod_VDN as CodVdn, isnull(GV.DescVdn,'') as DescVdn,"
      + " isnull(GV.Operacao,'') as CodOperacao, SUM(Qtd_Chamadas) as Chamadas"
      + " From " + db.prefixo + tabelas.nomes[0]+ " as VED"  //"+descobrirSufixo(data_ini,data_fim)+"
      + " Left Outer Join " + db.prefixo + "VdnsUra as GV"
      + " on VED.Cod_Site = GV.CodSite and VED.Cod_VDN = GV.CodVDN"
      + " WHERE 1 = 1"
      + " AND Dat_Referencia >= '" + formataData(data_ini) + "'"
      + " AND Dat_Referencia "+datfim+""
        stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("Cod_Site", filtro_sites, true)

        for(var i = 0; i < filtro_operacoes.length; i++){
          if(filtro_operacoes[i]==="SINERGIA" || filtro_operacoes[i]==="FIXADTMF" || filtro_operacoes[i]==="0800VELOX" || filtro_operacoes[i]==="0800VXDTMF"){
            stmt += restringe_consulta("Operacao", filtro_operacoes, true)
          }else{
            stmt += restringe_consulta("Central", filtro_operacoes, true)
          }
        }

        stmt += restringe_consulta("Operacao", filtro_operacoes, true)
        if(filtro_segmentos!=""){
          stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        }else{
            stmt += " AND Cod_Segmento = ''"
        }
      stmt += " group by Cod_site, Cod_VDN,DescVdn,Operacao";

      if(tabelas.nomes.length === 1){
        stmt +=" order by chamadas DESC";
      }


      if(tabelas.nomes.length > 1){
      stmt += " union all SELECT TOP " + $scope.limite_registros
      + " VED.Cod_Site as CodSite, VED.Cod_VDN as CodVdn, isnull(GV.DescVdn,'') as DescVdn,"
      + " isnull(GV.Operacao,'') as CodOperacao, SUM(Qtd_Chamadas) as Chamadas"
      + " From " + db.prefixo + tabelas.nomes[1]+ " as VED"  //"+descobrirSufixo(data_ini,data_fim)+"
      + " Left Outer Join " + db.prefixo + "VdnsUra as GV"
      + " on VED.Cod_Site = GV.CodSite and VED.Cod_VDN = GV.CodVDN"
      + " WHERE 1 = 1"
      + " AND Dat_Referencia >= '"+tabelas.data+"'"
      + " AND Dat_Referencia <= '" + formataData(data_fim) + "'"
        stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("Cod_Site", filtro_sites, true)

        for(var i = 0; i < filtro_operacoes.length; i++){
          if(filtro_operacoes[i]==="SINERGIA" || filtro_operacoes[i]==="FIXADTMF" || filtro_operacoes[i]==="0800VELOX" || filtro_operacoes[i]==="0800VXDTMF"){
            stmt += restringe_consulta("Operacao", filtro_operacoes, true)
          }else{
            stmt += restringe_consulta("Central", filtro_operacoes, true)
          }
        }

        stmt += restringe_consulta("Operacao", filtro_operacoes, true)
        if(filtro_segmentos!=""){
          stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        }else{
            stmt += " AND Cod_Segmento = ''"
        }
      stmt += " group by Cod_site, Cod_VDN,DescVdn,Operacao";

      stmt +=")Select CodSite, CodVdn, DescVdn, CodOperacao, sum(Chamadas) as c"
           +" FROM q group by CodSite, CodVdn, DescVdn, CodOperacao ORDER BY c desc";

      }


      log(stmt);


      var tabelas = tabelasParticionadas($scope, 'Resumo_EncerramentoURAVOZD', false);
      var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataData(data_fim) + "'";

      tabelas.nomes.length > 1 ? stmt2 = db.use+'with q as (' : stmt2 = db.use+'';

      stmt2 += "Select count(*) as Linhas, isnull(sum(QtdChamadas),0) as Chamadas, isnull(sum(QtdExecucoes),0) as Execucoes,"
      + " isnull(sum(QtdDerivadas),0) as Derivadas, isnull(sum(QtdFinalizadas),0) as Finalizadas,"
      + " isnull(sum(QtdAbandonadas),0) as Abandonadas, isnull(sum(TempoEncerradas),0) as Tempo "
      + " from "+tabelas.nomes[0]+" as RU"
      + " WHERE 1 = 1"
      + " AND Dat_Referencia >= '" + formataData(data_ini) + "'"
      + " AND Dat_Referencia "+datfim+""
        stmt2 += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt2 += restringe_consulta("Cod_Site", filtro_sites, true)
        if(filtro_segmentos!=""){
          stmt2 += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        }else{
            stmt2 += " AND Cod_Segmento = ''";
        }

      if(tabelas.nomes.length > 1){

      stmt2 += " union all Select count(*) as Linhas, isnull(sum(QtdChamadas),0) as Chamadas,"
      + " isnull(sum(QtdExecucoes),0) as Execucoes,"
      + " isnull(sum(QtdDerivadas),0) as Derivadas, isnull(sum(QtdFinalizadas),0) as Finalizadas,"
      + " isnull(sum(QtdAbandonadas),0) as Abandonadas, isnull(sum(TempoEncerradas),0) as Tempo "
      + " from "+tabelas.nomes[1]+" as RU"
      + " WHERE 1 = 1"
      + " AND Dat_Referencia >= '" + tabelas.data + "'"
      + " AND Dat_Referencia <= '" + formataData(data_fim) + "'"
        stmt2 += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
        stmt2 += restringe_consulta("Cod_Site", filtro_sites, true)
        if(filtro_segmentos!=""){
          stmt2 += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
        }else{
            stmt2 += " AND Cod_Segmento = ''";
        }

        stmt2 +=")Select sum(Linhas), sum(Chamadas), sum(Execucoes),sum(Derivadas),sum(Finalizadas),sum(Abandonadas), sum(Tempo) FROM q";

      }

       log(stmt2);



        var stmtCountRows = stmtContaLinhas(stmt);


        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas (columns) {
                $globals.numeroDeRegistros = columns[0].value;
        }

        var qtdAcumulada = 0,
            percChamadas = 0,
            percChamadasAcumuladas = 0,
            percDerivadas = 0,
            percDerivadasAcumuladas = 0;


        function executaQuery(columns){
        //db.query(stmt, function (columns) {


            var
            site = columns[0].value,
            vdn = columns[1].value,
            grupo = columns[2].value,
            operacao = columns[3].value,
            quantidade = +columns[4].value;

            qtdAcumulada += +columns[4].value;

            percChamadas = +(columns[4].value * 100 / $scope.entrantes).toFixed(1);
            //percChamadas = +(percChamadas.substring(0,percChamadas.length-1));
            percChamadas = percChamadas === Infinity ? 0 : percChamadas;

            percChamadasAcumuladas = +(qtdAcumulada * 100 / $scope.entrantes).toFixed(1);
            //percChamadasAcumuladas = +(percChamadasAcumuladas.substring(0,percChamadasAcumuladas.length-1));
            percChamadasAcumuladas = percChamadasAcumuladas === Infinity ? 0 : percChamadasAcumuladas;

            percDerivadas = +(columns[4].value * 100 / $scope.derivadas).toFixed(1);
            //percDerivadas = +(percDerivadas.substring(0,percDerivadas.length-1));
            percDerivadas = percDerivadas === Infinity ? 0 : percDerivadas;

            percDerivadasAcumuladas = +(qtdAcumulada * 100 / $scope.derivadas).toFixed(1);
            //percDerivadasAcumuladas = +(percDerivadasAcumuladas.substring(0,percDerivadasAcumuladas.length-1));
            percDerivadasAcumuladas = percDerivadasAcumuladas === Infinity ? 0 : percDerivadasAcumuladas;

            $scope.dados.push({
              site: site,
              vdn: vdn,
              grupo: grupo,
              operacao: operacao,
              quantidade: quantidade,
              percChamadas: percChamadas,
              percChamadasAcumuladas: percChamadasAcumuladas,
              percDerivadas: percDerivadas,
              percDerivadasAcumuladas: percDerivadasAcumuladas
            });
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-derivacao-vdn");

           if($scope.dados.length%1000===0){
                $scope.$apply();
              }
        }

        function executaQuery2(columns){
            $scope.$apply(function () {

            var
            derivadas = columns[3].value,
            finalizadas = columns[4].value,
            abandonadas = columns[5].value;



            $scope.derivadas = derivadas;
            $scope.finalizadas = finalizadas;
            $scope.abandonadas =  abandonadas;
            $scope.entrantes = +derivadas + +finalizadas + +abandonadas;
            $scope.percDerivadas = +((+derivadas * 100)/$scope.entrantes).toFixed(2);
            $scope.percFinalizadas =  +((+finalizadas * 100)/$scope.entrantes).toFixed(2);
            $scope.percAbandonadas = +((+abandonadas * 100)/$scope.entrantes).toFixed(2);
            try{
            $scope.somaPercs = ($scope.percDerivadas + $scope.percFinalizadas + $scope.percAbandonadas).toFixed(2);
            }catch(ex){
              console.log(ex);
            }
           });
        }

        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
          //console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);
          db.query(stmt2,executaQuery2, function (err, num_rows) {
            console.log("Executando query-> "+stmt2+" "+num_rows);
            db.query(stmt, executaQuery, function (err, num_rows) {
              console.log("Executando query-> "+stmt+" "+num_rows);
              var valorEntrantes = 0;
              userLog(stmt, 'tabelasParticionadas', 2, err)
              if($scope.entrantes > 0){
                valorEntrantes = 4;
              }

              retornaStatusQuery(num_rows + valorEntrantes, $scope);
              if(valorEntrantes > 0 && num_rows===0){
                retornaStatusQuery(4, $scope);
              }
              $btn_gerar.button('reset');
              if(num_rows>0){
                $btn_exportar.prop("disabled", false);
              }
            });
          });
        //});
          $view.on("mouseup", "tr.resumo", function () {
              var that = $(this);
              //console.log('passei');
              $('tr.resumo.marcado').toggleClass('marcado');
              $scope.$apply(function () {
                  that.toggleClass('marcado');
              });
          });
    };




    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='tDerivacaoVDN'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = 'tDerivacaoVDN.xlsx';



    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'derivacaoVDN_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;

   var percDerivadas = 0, percFinalizadas = 0, percAbandonadas = 0, somaPercs = 0;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

     if(!isNaN($scope.percDerivadas)){percDerivadas = $scope.percDerivadas;};
     if(!isNaN($scope.percFinalizadas)){percFinalizadas = $scope.percFinalizadas;};
     if(!isNaN($scope.percAbandonadas)){percAbandonadas = $scope.percAbandonadas;};
     if(!isNaN($scope.somaPercs)){somaPercs = $scope.somaPercs;};

                // Perform substitution
                t.substitute(1, {

                  finalizadas: $scope.finalizadas,
                  abandonadas: $scope.abandonadas,
                  entrantes: $scope.entrantes,
                  percDerivadas: percDerivadas,
                  percFinalizadas: percFinalizadas,
                  percAbandonadas: percAbandonadas,
                  somaPercs: somaPercs,
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados,
                  derivadas: $scope.derivadas

                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)
            //?
          });

    };
}
CtrlDerivacaoVDN.$inject = ['$scope','$globals'];
