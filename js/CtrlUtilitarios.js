function CtrlUtilitarios($scope, $globals) {

    win.title="Utilitários"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;


    //Alex 24/02/2014
    travaBotaoFiltro(0, $scope, "#pag-permissoes", "Utilitários (uso interno).");


    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    $scope.usuario = {};
    $scope.dados = [];


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-permissoes");
      treeView('#pag-permissoes #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-permissoes #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-permissoes #ed', 65,'ED','dvED');
      treeView('#pag-permissoes #ic', 95,'IC','dvIC');
      treeView('#pag-permissoes #tid', 115,'TID','dvTid');
      treeView('#pag-permissoes #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-permissoes #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-permissoes #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-permissoes #parametros', 225,'Parametros','dvParam');
      treeView('#pag-permissoes #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-permissoes #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-permissoes #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');




        //preenchendo list com os relatorios
        var options2 = [];
        cache.relatorios.forEach(function (form) {

                  options2.push('<option value="' + form.codigo + '">' + form.nome + '</option>');

        });
        $view.find("select.filtro-relatorio").html(options2.join());




        $view.find("select.filtro-relatorio").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} relatórios',
            showSubtext: true
        });



        //Bernardo 20-02-2014 Marcar todos os relatórios
        $view.on("click", "#alinkRel", function () {
            var valor = $('#alinkRel').attr("data-var");
            if (valor === "1") {
                $('.filtro-relatorio').selectpicker('deselectAll');
                $('#alinkRel').attr("data-var", "0");
            } else {
                $('.filtro-relatorio').selectpicker('selectAll');
                $('#alinkRel').attr("data-var", "1");
            }
        });







        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-relatorio').mouseover(function () {
            $('div.btn-group.filtro-relatorio').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-relatorio').mouseout(function () {
            $('div.btn-group.filtro-relatorio').removeClass('open');
        });




        // Salva dados conforme filtros
        $view.on("click", ".btn-salvar", function () {


          //22/03/2014 Testa se um relatório foi selecionada
            var filtro_relatorios = $view.find("select.filtro-relatorio").val() || [];
            if (filtro_relatorios.length === 0 || filtro_relatorios[0] === null) {
                setTimeout(function(){
                    atualizaInfo($scope,'Selecione ao menos um relatório');
                    effectNotification();
                    $view.find(".btn-salvar").button('reset');
                },500);
                return;
            }


          if($scope.formP.$valid){
            limpaProgressBar($scope, "#pag-permissoes");
            $scope.salvaDados.apply(this);
          }else{
            setTimeout(function(){
                    atualizaInfo($scope,"Dados incorretos");
                    effectNotification();
                    $view.find(".btn-salvar").button('reset');
                },500);
                return;
          }
        });

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-permissoes");
        }

    });


    $scope.add = function(){
        var f = document.getElementById('file').files[0],
            r = new FileReader(),arr;
        r.onloadend = function(e){
            var data = e.target.result;
            //send you binary data via $http or $resource or do anything else with it
            if(data){

                try{
                var json = JSON.parse(data);
                }catch(ex){
                    setTimeout(function(){
                        atualizaInfo($scope,"Arquivo inválido!");
                    },500);
                    return;
                }

                var objeto = json;
                arr = $.map(objeto, function(value, index) {
                    return [value];
                });
                $scope.dados = arr;

                //Alex 15-02-2014 - 26/03/2014 TEMPLATE
            var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='tLog'";
            log(stmt);
            /*db.query(stmt, function (columns) {
                var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;*/


                /*var milis = new Date();
                var baseFile = 'tLog.xlsx';
                var buffer = toBuffer(toArrayBuffer(arquivo));
                fs.writeFileSync(baseFile, buffer, 'binary');*/

                //teste
                var milis = new Date();
                var baseFile = 'templates/tLog.xlsx';
                //teste

                var file = 'log_' + formataDataHoraMilis(milis) + '.xlsx';

                var newData;

                fs.readFile(baseFile, function (err, data) {

                    // Create a template
                    var t = new XlsxTemplate(data);

                    // Perform substitution
                    t.substitute(1, {
                        planDados: $scope.dados
                    });

                    // Get binary data
                    newData = t.generate();


                    if (!fs.existsSync(file)) {
                        fs.writeFileSync(file, newData, 'binary');
                        childProcess.exec(file);
                    }
                });


            /*}, function (err, num_rows) {
                //?
            });*/



            }
        }
        //r.readAsBinaryString(f);
        r.readAsText(f);


    }



        // Salva dados conforme filtros
    $scope.salvaDados = function () {

        $globals.numeroDeRegistros = 0;



        var $btn_salvar = $(this);
        $btn_salvar.button('loading');

        var filtro_relatorios = $view.find("select.filtro-relatorio").val() || [];


        function montaInserts(valores){
            var valor = "";
            for(var i = 0; i<filtro_relatorios.length; i++){
                valor += "('"+quoteReplace($scope.usuario.login)+"','"+quoteReplace($scope.usuario.dominio)+"','"+valores[i]+"'),";
            }
            valor = valor.substring(0,valor.length-1);
            return valor;

        }

        var stmt = db.use
        + "INSERT " + db.prefixo + "UsuariosEstatura (LoginUsuario,Dominio,Empresa,Nome,CpfUsuario,TipoUsuario,DataCadastro,EmailUsuario)"
        +" VALUES "
        +"('"+quoteReplace($scope.usuario.login)+"','"+quoteReplace($scope.usuario.dominio)+"','"+quoteReplace($scope.usuario.empresa)+"',"
        +"'"+quoteReplace($scope.usuario.nome)+"','"+$scope.usuario.cpf+"','N',GETDATE (),'"+$scope.usuario.email+"')";

        log(stmt);

        var stmt2 = db.use
        + "INSERT " + db.prefixo + "PermissoesUsuariosEstatura (LoginUsuario,Dominio,Formulario)"
        +" VALUES "
        +""+montaInserts(filtro_relatorios)+"";

        log(stmt2);




        function executaQuery (columns) {
            //
        }

        function executaQuery2(columns){
            atualizaProgressBar(0,0,$scope,"#pag-permissoes");
        }

        var total = 0;

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Adiciona usuario', 1, err)
                console.log("Executando query-> "+stmt+" "+num_rows);
                total +=num_rows;
                db.query(stmt2, executaQuery2, function (err, num_rows) {
                    userLog(stmt, 'Adiciona permissao', 1, err)
                    console.log("Executando query-> "+stmt2+" "+num_rows);
                    total +=num_rows;
                    $scope.usuario = {};
                    //$scope.formP = {};
                    filtro_relatorios = [];

                    $scope.$apply();
                    atualizaInfo($scope, ""+total+" inseridos.");
                    $btn_salvar.button('reset');
                });
            });
    };
}
CtrlUtilitarios.$inject = ['$scope','$globals'];
