/*
 * CtrlChamadas
 */
var changeFalhas = false;

function CtrlChamadas($scope, $globals) {
  $('#grid').css('display','none');
  $scope.usoDeMemoria = "";
  $scope.baseUrl = (base == "") ? "http://www.versatec.com.br/oi3/" : base;
  setInterval(function () {
	$scope.usoDeMemoria = (os.freemem()/1024/1024).toFixed(2)+" MB"+" -> "+(os.totalmem()/1024/1024).toFixed(2)+" MB";
	$scope.$apply();
	  //console.log($scope.usoDeMemoria);
  }, 1000);

  /*intervaloExtratorStatus = undefined;
  intervaloExtratorStatusAux = undefined;

  intervaloExtratorStatusAux = setInterval(function () {
	if (extratorStatus !== "") {
	  $scope.status_extrator = extratorStatus;
	  $scope.$apply();
	}
  }, 0);*/

  $scope.valor3 = tipoVersao;
  $scope.login = loginUsuario;
  $scope.dominio = dominioUsuario;
  //$scope.callLogValue = $(".callLogValue").val();

  //versao = cache.releaseEstaloData;
  link = cache.releaseEstaloLink;
  $scope.htmlRelease = arrumaHtml(cache.releaseEstaloHtml);
  versaoMod = versao.substring(9, 17).replace(new RegExp('/', 'g'), '_');

  if ($globals.logado === false) {

	win.title = "Estalo " + versao;
	if (fs.existsSync(tempDir3()+"log_extrator.txt")) {
	  fs.unlinkSync(tempDir3()+"log_extrator.txt");
	}
  } else {
	win.title = "Detalhamento de chamadas";
	cache.datsUltMesDetCham = new Date(cache.datsUltMesDetCham);
	$scope.datsUltMesDetCham = pad(cache.datsUltMesDetCham.getMonth() + 1) + "/" + cache.datsUltMesDetCham.getFullYear();
	$scope.datsUltMesDetChamFull = formataDataHoraBR(cache.datsUltMesDetCham);

	//TROCA ESTALO.EXE
	var estaloNovo = new Date(cache.releaseEstaloEXE);
	  var estaloAtual = new Date(fs.statSync(tempDir3()+'Estalo.exe').mtime);

	  console.log("estalonovo -> "+estaloNovo);
	  console.log("estaloatual -> "+estaloAtual);

	if (estaloAtual < estaloNovo) {

	setTimeout(function () {
	  if (fs.existsSync(tempDir3()+"Estalo.exe") && fs.existsSync(tempDir3()+"Estalo_temp")) {
		try {
		  fs.unlinkSync(tempDir3()+'Estalo.exe');
		  if (fs.existsSync(tempDir3()+"Estalo_temp")) {
			try {
			  fs.renameSync(tempDir3()+'Estalo_temp', tempDir3()+'Estalo.exe');
			} catch (ex) {
			  console.log(ex);
			}
			console.log("Nova versão trocada");
		  }
		} catch (ex) {
		  console.log(ex);
		}
	  }

	}, 5000);

	}//TROCA ESTALO.EXE


	//TROCA TOOL.EXE

	if (!fs.existsSync(tempDir3()+"tool.exe.full")) {
	  var toolNovo = new Date(unique2(cache.toolInfos.map(function (i) {
		if (i.nome === 'TOOL'+tipoVersao+'.EXE') return i.datahora;
	  })));
	  var toolAtual = new Date(fs.statSync(tempDir3()+'tool.exe').mtime);

	  var pjsonTeste = require('./package.json');
	  console.log("toolnovo -> "+toolNovo);
	  console.log("toolatual -> "+toolAtual);

	  if (toolAtual < toolNovo) {
		if (pjsonTeste.window.title !== "estalo_developer") {
		  console.log("Troca de TOOL"+tipoVersao+".EXE");
		  var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
		  + " WHERE NomeRelatorio='TOOL"+tipoVersao+".EXE'";
		  executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) { console.log(dado); });
		}
	  }
	}//TROCA TOOL.EXE


  }

  $scope.versao = versao;
  $scope.rotas = rotas;

  $scope.limite_registros = 10000;
  $scope.incrementoRegistrosExibidos = 100;
  var tempoLimite = 500;
  $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
  $scope.estadoEnx = false;
  $scope.itemEnx = false;
  $scope.itemEnxAll = false;

  if (versao === "OLD VERSION") {
	travaBotaoFiltro(0, $scope, "#pag-chamadas", "Esta versão do Estalo está desatualizada");
  } else {
	travaBotaoFiltro(0, $scope, "#pag-chamadas", "Consulta realizada de 5 em 5 minutos.");
  }

  splash($scope, "#pag-chamadas", base + "img/logo-versatil.png", $globals.logado);

  $scope.chamadas = [];
  $scope.ordenacao = ['data_hora', 'chamador'];
  $scope.decrescente = false;
  $scope.ocultar = {
	tratado: false,
	log_formatado: false,
	log_original: true
  };

  $scope.colunas = [];
  $scope.gridDados = {
	data: "chamadas",
	rowTemplate: '<div ng-dblclick="formataXML(row.entity)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
	columnDefs: "colunas",
	enableColumnResize: true,
	enablePinning: true
  };

  $scope.chamada_atual = undefined;
  $scope.callLog = "";
  $scope.logxml = [];

  $scope.log = [];

  $scope.aplicacoes = [];
  $scope.segmentos = [];
  $scope.falhas = [];
  $scope.valor = "Telefone tratado";

  // Filtros
  $scope.filtros = {
	aplicacoes: [],
	segmentos: [],
	falhas: [],
	chamador: "",
	//isHFiltroED: false,
	//isHFiltroIC: false,
	isUltimoED: false,
	UCID: "",
	arq_cache: bkpCache,
	nps: false
  };

  $scope.extratorArquivoPorData = false;
  $scope.msgAlerta = false;
  $scope.callFlow = false;
  $scope.extrator = false;
  $scope.descExtracao = "";
  $scope.descExtracao2 = "";
  $scope.aba = 2;

  $scope.memoria = os.totalmem() - os.freemem() > 512;

  function apenasUSSD($filtro) {
	var filtro_aplicacoes = $filtro.val() || [];
	if (typeof filtro_aplicacoes === 'string') { filtro_aplicacoes = [ filtro_aplicacoes ]; }
	return filtro_aplicacoes.every(function (apl) { return apl.match(/^(USSD|PUSH)/); });
  }

  function geraColunas() {
	var array = [];

	array.push({ field: "data_hora_BR", displayName: "Data", width: 150, pinned: true },
	  { field: "chamador", displayName: "Chamador", width: 100, pinned: false });

	$scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));
	if (!$scope.ocultar.tratado) {
	  array.push({ field: "tratado", displayName: "Tratado", width: 100, pinned: false });
	}

	array.push({ field: "aplicacao", displayName: "Aplicação", width: 130, pinned: false },
	  { field: "segmento", displayName: "Segmento", width: 100, pinned: false },
	  { field: "encerramento", displayName: "Encerramento", width: 150, pinned: false },
	  { field: "estado_final", displayName: "Último estado", width: 400, pinned: false });

	if ($scope.filtros.nps === true) {
	  array.push({ field: "npss", displayName: "Satisf.", width: 80, pinned: false },
		{ field: "npsr", displayName: "Recom.", width: 80, pinned: false });
	}

	array.push({ field: "ucid", displayName: "Log", width: 65, cellClass: "grid-align", pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>' });

	return array;
  }

  // Filtros: data e hora
  var agora = new Date();

  var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
  var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

  $scope.periodo = {
	inicio: inicio,
	fim: fim,
	min: new Date(2013, 11, 1),
	max: agora // FIXME: atualizar ao virar o dia
  };

  var $view;
  var numanis = [];


  var invalidsNumanis = [];

  $scope.$on('$viewContentLoaded', function () {

	/*if (!fs.existsSync(tempDir3()+"config_"+versaoMod)) {
	  fs.writeFileSync(tempDir3()+"config_"+versaoMod, 0);
	} else if (parseInt(fs.readFileSync(tempDir3()+"config_"+versaoMod).toString()) === 1) {
	  fs.writeFileSync(tempDir3()+"config_"+versaoMod, 0);
	}
	*/

  // $('#clearSearchToolButton').click(function(){
		// clearSearchTool();
		// console.log("Chamou a função para limpar");
	// });

	$('.log-original').css('overflow', 'hidden');
	$('#modalAlerta').on('hidden', function () {
	  $("#modalAlerta").css({'margin-top': '0px', 'width': '0px', 'z-index': '0'});

	  if ($scope.msgAlerta) {
		var fs = require("fs");
		var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
		fs.writeFileSync(tempDir3()+'versao.txt', pegaDataVersao);
		//console.log(localStorage.getItem("ignorarReleaseNews"));
	  }

	  $('div.notification').fadeIn(500);
	});

	$view = $("#pag-chamadas");
	treeView('#pag-chamadas #chamadas', 15, 'Chamadas', 'dvchamadas');
	treeView('#pag-chamadas #repetidas', 35, 'Repetidas', 'dvRepetidas');
	treeView('#pag-chamadas #ed', 65, 'ED', 'dvED');
	treeView('#pag-chamadas #ic', 95, 'IC', 'dvIC');
	treeView('#pag-chamadas #tid', 115, 'TID', 'dvTid');
	treeView('#pag-chamadas #vendas', 145, 'Vendas', 'dvVendas');
	treeView('#pag-chamadas #falhas', 165, 'Falhas', 'dvFalhas');
	treeView('#pag-chamadas #extratores', 195, 'Extratores', 'dvExtratores');
	treeView('#pag-chamadas #parametros', 225, 'Parametros', 'dvParam');
	treeView('#pag-chamadas #admin', 255, 'Administrativo', 'dvAdmin');
	treeView('#pag-chamadas #monitoracao', 275, 'Monitoração', 'dvReparo');
	treeView('#pag-chamadas #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	treeView('#pag-chamadas #uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');	

	$(".dropdown-menu").mousemove(function (event) {
	  if ($("#modalAlerta").attr("aria-hidden") == "false") {
		$('#listaM').remove();
	  } else {
		$("#modalAlerta").remove();
	  }
	  //$('#listaM').remove();
	});

	//$('.nav.aba3').css('display', 'none');
	//$('.nav.aba4').css('display', 'none');
	//$('.nav.aba5').css('display', 'none');
	//$('.nav.aba6').css('display', 'none');
	  
	  
	$(".aba4").css({'position': 'fixed', 'left': '630px', 'top': '40px'});
	$(".aba5").css({'position': 'fixed', 'left': '400px', 'top': '40px'});
	$('#contextVal').css('width', '50px');
	$(".aba6").css({'position': 'fixed', 'left': '54px', 'top': '40px'});
	$(".ultimomes").css({'position': 'fixed', 'left': '770px', 'top': '50px'});
	$(".botoes").css({'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px'});

	//minuteStep: 5

	componenteDataHora($scope, $view);

	// Carregar filtros
	carregaAplicacoes($view, false, false, $scope);
	carregaSites($view);
	carregaFalhas($view);
	carregaJsons($view);
	carregaSegmentosPorAplicacao($scope, $view, true);

	// change de filtros

	// Popula lista de segmentos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function () {
	  carregaSegmentosPorAplicacao($scope, $view);
	  //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
	});

	$view.on("change", "select.filtro-segmento", function () {
	  Estalo.filtros.filtro_segmentos = $(this).val() || [];
	});

	$view.on("change", "select.filtro-ic", function () {
	  Estalo.filtros.filtro_ic = $(this).val() || [];
	});

	$view.on("change", "select.filtro-ed", function () {
	  Estalo.filtros.filtro_ed = $(this).val() || [];
	});

	$view.on("change", "select.filtro-jsons", function () {
	  Estalo.filtros.varjson = $(this).val() || [];
	});

	$view.on("change", "select.filtro-site", function () {
	  var filtro_sites = $(this).val() || [];
	  Estalo.filtros.filtro_sites = filtro_sites;
	});

	/*$("#import2").click(function() {
	  $('#modalConfirma').css("z-index","1040");
	});*/

	$("#confirma").click(function () {
	  $("#browser").trigger("click");
	});

	$("#confirma").click(function() {
	  $('#modalConfirma').css("z-index","-999");
	});

	$('#browser').change(function () {
	  numanis = fs.readFileSync($("#browser").val()).toString().split('\r\n');
	  var n1 = [];
	  var n2 = [];

	  if (numanis.length > 0) {
		for (var i = 0; i < numanis.length; i++) {
		  if (n1.indexOf(numanis[i].trim()) < 0 && numanis[i].trim() !== "" && numanis[i].trim().length >= 10 && numanis[i].trim().length <= 11) {
			n1.push(numanis[i].trim());
		  } else if (numanis[i].trim() !== "") {
			n2.push(numanis[i].trim());
		  }
		}
		if (numanis.length > 0) {
		  numanis = n1;
		  invalidsNumanis = n2;
		  $('#import2').css('background', '#A9D0F5').selectpicker('refresh');
		} else {
		  numanis = [];
		  invalidsNumanis = [];
		  $("#browser").val("");
		  alert("Arquivo fora de especificação. O padrão é DDD + 8 ou 9 dígitos para cada linha.");
		}
	  }
	});

	$view.on("change", "select.filtro-falhas", function () {
	  changeFalhas = true;
	  var filtro_falhas = $(this).val() || [];
	  Estalo.filtros.filtro_falhas = filtro_falhas;
	});

	$("#pag-chamadas").on("click", ".filtro-falhas div.dropdown-menu.open ul li", function () {
	  if (changeFalhas === false) {
		Estalo.filtros.filtro_falhas = [];
		$('select.filtro-falhas').val('').selectpicker('refresh');
	  }
	  changeFalhas = false;
	});

	//$view.on("click", "filtro-falhas", function () {
	//    alert('click');
	//    $('select.filtro-falhas').val('').selectpicker('refresh');
	//});

	$("#pag-chamadas").on("click", ".dropdown-menu.opcao", function () {
	  if ($('.filtro-chamador').attr('placeholder') === "CPF/CNPJ" || $('.filtro-chamador').attr('placeholder') === "Contrato") {
		$('#callFlow').attr('disabled', true);
		$('#callFlow').prop('checked', false);
		$scope.callFlow = false;
	  } else {
		$('#callFlow').attr('disabled', false);
	  }
	});

	// transição de abas
	var abas = [2, 3, 4, 5, 6];

	$view.on("click", "#alinkAnt", function () {
	  if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
	  if ($scope.aba > abas[0]) {
		$scope.aba--;
		mudancaDeAba();
	  }
	});

	$view.on("click", "#alinkPro", function () {
	  if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

	  if ($scope.aba < abas[abas.length - 1]) {
		$scope.aba++;
		mudancaDeAba();
	  }
	});

	function mudancaDeAba() {
	  abas.forEach(function (a) {
		if ($scope.aba === a) {
		  $('.nav.aba' + a + '').fadeIn(500);
		} else {
		  $('.nav.aba' + a + '').css('display', 'none');
		}
	  });
	}

	// Marcar todos

	// Marca todos os sites
	$view.on("click", "#alinkSite", function () { marcaTodosIndependente($('.filtro-site'), 'sites'); });

	// Marca todos os segmentos
	$view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos'); });

	// Marcar todas aplicações
	$view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope); });

	// Default view filtros

	$view.find("select.filtro-aplicacao").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} aplicações',
	  showSubtext: true
	});

	$view.find("select.filtro-site").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} Sites/POPs',
	  showSubtext: true
	});
	$view.find("select.filtro-segmento").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} segmentos'
	});

	$view.find("select.filtro-ed").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} EDs'
	});

	$view.find("select.filtro-ic").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} ICs'
	});

	$view.find("select.filtro-falhas").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} Falhas'
	});

	$view.find("select.filtro-jsons").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} Variáveis'
	});

	// Matar processo
	$view.on("click", ".statusExtrator", function () {
	  if (thread !== undefined) {
		$scope.status_extrator = "";
		$scope.$apply();
		clearInterval(intervaloExtratorStatus);
		clearInterval(intervaloExtratorStatusAux);
		$('.notification .ng-binding').text("Operação cancelada.").selectpicker('refresh');
		thread.kill();
		thread = undefined;
	  }
	});

	// EXIBIR e OCULTAR AO TIRAR O MOUSE

	// EXIBIR AO PASSAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
	  $("div.data-inicio.input-append.date").data("datetimepicker").hide();
	  $("div.data-fim.input-append.date").data("datetimepicker").hide();
	  $('div.btn-group.filtro-aplicacao').addClass('open');
	  $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
	  $('div.btn-group.filtro-aplicacao').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
	  $('div.btn-group.filtro-site').addClass('open');
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
	  $('div.btn-group.filtro-site').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-jsons').mouseover(function () {
	  $('div.btn-group.filtro-jsons').addClass('open');
	  $('div.btn-group.filtro-jsons>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-jsons').mouseout(function () {
	  $('div.btn-group.filtro-jsons').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
	  // Não mostrar filtros desabilitados
	  if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
		$('div.btn-group.filtro-segmento').addClass('open');
		$('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div>div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
	  $('div.btn-group.filtro-segmento').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE estado de dialogo
	$('div.btn-group.filtro-ed').mouseover(function () {
	  // Não mostrar filtros desabilitados
	  if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
		$('div.btn-group.filtro-ed').addClass('open');
		$('div.btn-group.filtro-ed>div>ul').css({ 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE estado de dialogo
	$('div.btn-group.filtro-ed').mouseout(function () {
	  $('div.btn-group.filtro-ed').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE item de controle
	$('div.btn-group.filtro-ic').mouseover(function () {
	  // Não mostrar filtros desabilitados
	  if (!$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
		$('div.btn-group.filtro-ic').addClass('open');
		$('div.btn-group.filtro-ic>div>ul').css({ 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE item de controle
	$('div.btn-group.filtro-ic').mouseout(function () {
	  $('div.btn-group.filtro-ic').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE falhas
	$('div.btn-group.filtro-falhas').mouseover(function () {
	  // Não mostrar filtros desabilitados
	  if (!$('div.btn-group.filtro-falhas .btn').hasClass('disabled')) {
		$('div.btn-group.filtro-falhas').addClass('open');
		$('div.btn-group.filtro-falhas>div>ul').css({ 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE Falhas
	$('div.btn-group.filtro-falhas').mouseout(function () {
	  $('div.btn-group.filtro-falhas').removeClass('open');
	});

	// Informação sobre consolidação
	delay($(".filtro-aplicacao >.dropdown-menu li a"), function () {
	  cache.apls.forEach(function (apl) {
		if (apl.nome === aplFocus) {
		  console.log(apl.codigo);
		  var stmt = db.use+" SELECT 'Aplicação "+apl.codigo+" atualizada até',MAX(DataHora) FROM HistoricoConsolidacao WHERE CodAplicacao = '"+apl.codigo+"'";
		  executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) { console.log(dado); });
		}
	  });
	});

	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
	  $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	  // Forçar reposionamento do scroll
	  /* var h = document.getElementById('pag-chamadas').scrollHeight; $('body').scrollTop(h);*/
	};

	$view.on("click", ".btn-logar", function () {
	  if ($scope.valor3 !== tipoVersao) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
	  + " WHERE NomeRelatorio = 'CONFIGINIT"+$scope.valor3+"'";
		executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) { console.log(dado); });
	  }

	  $scope.montaRotas.apply(this);

	  // Mostra Modal
	  var fs = require("fs");

	  if (fs.existsSync(tempDir3()+'versao.txt')) {
		var dataVersao = fs.readFileSync(tempDir3()+'versao.txt');
	  } else {
		console.log("Versão não encontrada... Criando arquivo com versão atual");
		fs.writeFileSync(tempDir3()+'versao.txt', pegaDataVersao);
	  }

	  //console.log(dataVersao.toString()+versao);
	  var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
	  //console.log(pegaDataVersao[0]);
	  if (dataVersao.toString() != pegaDataVersao[0] || dataVersao.toString() == "undefined") {
		setTimeout(function () {
		  $("#modalAlerta").modal('show');
		  $("#modalAlerta").css({'margin-top': '90px', 'width': '600px', 'z-index': '2000'});
		  $globals.viewAlerta = 1;
				  //fs.writeFileSync(tempDir3()+"config_"+versaoMod,$globals.viewAlerta);
		}, 1500);
	  }
	  //Fim Mostra Modal
	});

	$view.on("click", ".btn-recarregar", function () {
	  location.reload(true);
	});

	// Lista chamadas conforme filtros
	$view.on("click", ".btn-gerar", function () {
	  limpaProgressBar($scope, "#pag-chamadas");
	  // Testar se data início é maior que a data fim
	  var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
	  var testedata = testeDataMaisHora(data_ini, data_fim);
	  if (testedata !== "") {
		setTimeout(function () {
		  atualizaInfo($scope, testedata);
		  effectNotification();
		  $view.find(".btn-gerar").button('reset');
		}, 500);
		return;
	  }

	  if ($scope.extrator) {
		$scope.extracao.apply(this);
		return;
	  }

	  //var filtro_ics = $view.find("select.filtro-ic").val() || [];
	  var filtro_ics = $scope.filtroICSelect || [];
	  var filtro_eds = $scope.filtroEDSelect || [];
	  var filtro_jsons = $view.find("select.filtro-jsons").val() || [];

	  /*if (_02d(data_ini.getMonth() + 1) !== _02d(data_fim.getMonth() + 1)) {
		setTimeout(function () {
		  atualizaInfo($scope, "Operação válida somente para o mesmo mês.");
		  effectNotification();
		  $view.find(".btn-gerar").button('reset');
		}, 500);
		return;
	  }*/

	  if (filtro_jsons.length > 1 && $('#contextVal').val() === "") {
		pv = [];
		for (var i = 1; i < filtro_jsons.length; i++) {
		  pv.push(';');
		}
		$('#contextVal').val(pv.join(""));
	  }

	  var contexto = $('#contextVal').val().split(';');
	  if (contexto[0] === "" && contexto.length === 1 && filtro_jsons.length === 0) contexto = [];

	  if (filtro_jsons.length !== contexto.length) {
		setTimeout(function () {
		  atualizaInfo($scope, '<font color = "White">O número de variáveis de contexto deve ser o mesmo que os valores pretendidos separados por ponto e vírgula</font>');
		  effectNotification();
		  $view.find(".btn-gerar").button('reset');
		  $view.find(".btn-exportar").prop("disabled", "disabled");
		}, 500);
		return;
	  }

	  //if ($scope.filtros.isHFiltroIC === true && filtro_ics.length === 0 || $scope.filtros.isHFiltroED === true && filtro_eds.length === 0) {
	  //  setTimeout(function () {
	  //    atualizaInfo($scope, '<font color = "White">Filtros de estados de diálogo e itens de controle exigem  ao menos 1 item selecionado quando habilitados.</font>');
	  //    effectNotification();
	  //    $view.find(".btn-gerar").button('reset');
	  //    $view.find(".btn-exportar").prop("disabled", "disabled");
	  //  }, 500);
	  //  return;
	  //}

	  //if ($scope.filtros.isHFiltroIC === true && filtro_ics.length > 3 || $scope.filtros.isHFiltroED === true && filtro_eds.length > 3) {
	  //  setTimeout(function () {
	  //    atualizaInfo($scope, '<font color = "White">Filtros de estados de diálogo e itens de controle têm limite de 3 itens por consulta.</font>');
	  //    effectNotification();
	  //    $view.find(".btn-gerar").button('reset');
	  //    $view.find(".btn-exportar").prop("disabled", "disabled");
	  //  }, 500);
	  //  return;
	  //}

	  var itens = $('select.filtro-aplicacao').val();
	  if (typeof itens === 'string') { itens = [ itens ]; }

	  var todas = false;

	  if (itens === null || itens.length > 3) {
		itens = cache.apls.map(function (a) { return a.codigo; });
		obtemEdsTodasAplicacoes(itens);
		todas = true;
	  } else {
		obtemEdsDasAplicacoes(itens);
	  }
	  var seguir = [];
	  function teste() {
		setTimeout(function () {
		  for (var i=0; i < itens.length; i++) {
			if (cache.aplicacoes[itens[i]].hasOwnProperty('estados')) {
			  if (seguir.indexOf(itens[i]) < 0) seguir.push(itens[i]);
			}
		  }

		  if (seguir.length >= itens.length) {
			clearTimeout(teste);
			console.log("seguir daqui");
			$scope.colunas = geraColunas();
			$scope.listaChamadas.apply(this);
		  } else {
			if (todas) {
			  $('.notification .ng-binding').text("Carregando EDs de todas as aplicações...").selectpicker('refresh');
			} else {
			  $('.notification .ng-binding').text("Carregando EDs...").selectpicker('refresh');
			}
			teste();
		  }
		}, 500);
	  }
	  teste();
	});

	$view.on("click", ".btn-baixarB", function () {
	  limpaProgressBar($scope, "#pag-chamadas");
	  $scope.consultaConsolidadoB.apply(this);
	});

	$view.on("dblclick", "div.ng-scope.ngRow", function () {
	  //$scope.formataXML($scope.chamada_atual);
	  //$view.find(".btn-log-original").parent().removeClass("active");
	  //$view.find(".btn-log-formatado").parent().addClass("active");
	  $scope.$apply(function () {
		$scope.ocultar.log_original = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_formatado = false;
	  });
	  $view.find("#modal-log").modal('show');
	});

	$view.on("click", "tr.chamada", function () {
	  var that = $(this);
	  $('tr.chamada.marcado').toggleClass('marcado');
	  $scope.$apply(function () {
		that.toggleClass('marcado');
	  });
	});

	  /*$view.on("change", "#HFiltroED", function () {

		  ////{ Limpa Filtro ED
		  $("#filtro-ed").attr("placeholder", "Estados de Diálogo");
		  $("#filtro-ed").val("");
		  $scope.filtroEDSelect = [];
		  arr2 = [];
		  ////} Fim Limpa Filtro ED

	  })*/

	  /*$view.on("change", "#HFiltroIC", function () {

		  ////{ Limpa Filtro IC
		  $("#filtro-ic").attr("placeholder", "Itens de Controle");
		  $("#filtro-ic").val("");
		  $scope.filtroICSelect =[];
		  vetorFiltroIc=[];
		   $scope.filtros.isHFiltroIC = false;
		  ////} Fim Limpa Filtro IC

	  })*/


	// Habilitar filtro estado de diálogo
	$view.on("click", "#filtro-ed", function () {

		if($('select.filtro-aplicacao').val() === null) return;

	  //if ($scope.filtros.isHFiltroED === true) {
		////{ Limpa Filtro IC
		//$("#filtro-ic").attr("placeholder", "Itens de Controle");
		//$("#filtro-ic").val("");
		//$scope.filtroICSelect =[];
		//vetorFiltroIc=[];
		// $scope.filtros.isHFiltroIC = false;
		  ////} Fim Limpa Filtro IC



		//$('#alinkApl').attr('disabled', 'disabled');
        //$(".filtro-aplicacao").removeAttr('multiple').selectpicker('refresh');
		$('#ultimoED').attr('disabled', false);

		carregaAplicacoes($view, true, false, $scope);

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val();

		// Popular lista de eds a partir das aplicações selecionadas
		var $sel_eds = $view.find("select.filtro-ed");

		if (filtro_aplicacoes.map === undefined) {
		  filtro_aplicacoes = [filtro_aplicacoes];
		}

		if (filtro_aplicacoes.length === 0) {
		  $sel_eds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		  $('#alinked').attr('disabled', 'disabled');
		  return;
		}

		var filtro_eds = {};
		var eds = obtemEdsDasAplicacoes(filtro_aplicacoes) || [];
		var options4 = [];
		var v = "";

		var teste = setInterval(function () {
			filtro_aplicacoes.forEach(function(fa){				
		  if (cache.aplicacoes[fa].hasOwnProperty('estados')) {			 
			clearInterval(teste);
			//alert("ok");
			//$scope.filtros.isHFiltroED = true;
			$('.notification .ng-binding').text("EDs carregados").selectpicker('refresh');
			eds = [cache.aplicacoes[fa].estados];
			eds.forEach(function (filtro_eds) {
			  v = filtro_eds || [];
			  unique(v).forEach((function (filtro_eds) {
				return function (codigo) {
				  for (var i = 0; i < v.length; i++) {
					var contem = (Estalo.filtros.filtro_ed.indexOf("" + v[i]['codigo']) >= 0);
					var nome = v[i]['nome'].substring(0, 47) + (v[i]['nome'].length > 47 ? "..." : "");
					//  options4.push('<option value="' + v[i]['codigo'] + '" ' + (contem ? " selected" : "") + '>' + v[i]['codigo'] + " - " + nome + '</option>');
					options4.push(v[i]['codigo'] + " " + nome);
					//console.log("ED Carregado");
				  }
				};
			  })(filtro_eds));

			  // $sel_eds.html(options4.join());

			  $scope.options4opt = options4;
			  var autocompleteED = $('#filtro-ed').typeahead();
			  autocompleteED.data('typeahead').source = $scope.options4opt;

			  $sel_eds
				.prop("disabled", false)
				.selectpicker('refresh');
			});
		  } else {
			$('.notification .ng-binding').text("Carregando EDs...").selectpicker('refresh');
			$sel_eds
			  .html("")
			  .prop("disabled", true)
			  .selectpicker('refresh');
			$('#alinked').attr('disabled', 'disabled');

		  }
			});  
		}, 500);
	  
	});
	
	
	$view.on("dblclick","#filtro-ed", function () {
		////{ Limpa Filtro ED
		$("#filtro-ed").attr("placeholder", "Estados de Diálogo");
		$("#filtro-ed").val("");
		$scope.filtroEDSelect = [];
		var $sel_eds = $view.find("select.filtro-ed");
		$sel_eds		  
		  .prop("disabled", "disabled")
		  .selectpicker('refresh');
	});


	// Habilitar filtro item de controle
	$view.on("click", "#filtro-ic", function () {

		if($('select.filtro-aplicacao').val() === null) return;
	 



		//$('#alinkApl').attr('disabled', 'disabled');
		//$(".filtro-aplicacao").removeAttr('multiple').selectpicker('refresh');

		carregaAplicacoes($view, true, false, $scope);

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val();
		  

		// Popular lista de ics a partir das aplicações selecionadas
		var $sel_ics = $view.find("select.filtro-ic");
		  
		if (filtro_aplicacoes.map === undefined) {
		  filtro_aplicacoes = [filtro_aplicacoes];
		}

		if (filtro_aplicacoes.length === 0) {
		  $sel_ics
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		  $('#alinkic').attr('disabled', 'disabled');
		  return;
		}

		var todas = true;
		/*if (cache.aplicacoes[filtro_aplicacoes].hasOwnProperty('itens_controle')) {
		  if (cache.aplicacoes[filtro_aplicacoes].itens_controle.length <= 100) {
			delete cache.aplicacoes[filtro_aplicacoes].itens_controle;
			todas = true;
		  }
		}*/

		var filtro_ics = {};
		var ics = obtemItensDeControleDasAplicacoes(filtro_aplicacoes, todas) || [];
		var options5 = [];
		var option5temp = [];
		var v = "";
		
		var teste = setInterval(function () {
			
			filtro_aplicacoes.forEach(function(fa){//,index){				
		  if (cache.aplicacoes[fa].hasOwnProperty('itens_controle')) {
			  //if(index === filtro_aplicacoes.length-1){
				  //console.log("ok -> "+(index+1)+" - "+filtro_aplicacoes.length);
				  //alert("ok -> "+(index+1)+" - "+filtro_aplicacoes.length);
			  clearInterval(teste);
			  //}
			//alert("ok");
			//$scope.filtros.isHFiltroIC = true;
		   // $('#filtro-ic').prop("disabled", false);
			$('.notification .ng-binding').text("ICs carregados").selectpicker('refresh');
			$('#filtro-ic').attr("placeholder", "Itens de Controle");
			ics = [cache.aplicacoes[fa].itens_controle];

			ics.forEach(function (filtro_ics) {
			  v = filtro_ics || [];
			  unique(v).forEach((function (filtro_ics) {
				return function (codigo) {
				  for (var i = 0; i < v.length; i++) {
					var contem = (Estalo.filtros.filtro_ic.indexOf("" + v[i]['codigo']) >= 0);
					var descricao = v[i]['descricao'].substring(0, 47) + (v[i]['descricao'].length > 47 ? "..." : "");					
					
										
					if (options5.indexOf(v[i]['codigo']+' '+descricao) === -1) {
						
						if(option5temp.indexOf(v[i]['codigo']) >= 0){
							options5.push(v[i]['codigo']+' Mais de uma descrição para o mesmo IC');																		
						}else{
							options5.push(v[i]['codigo']+' '+descricao);																		
							option5temp.push(v[i]['codigo']);
						}
						
						
					}else{
						options5[options5.indexOf(v[i]['codigo']+' '+descricao)] = v[i]['codigo']+' '+descricao;
					}
   
				  }
				};
			  })(filtro_ics));
			  //4 char  max 10

			  
			  
		/*	  options5.shift();
        for ( var x in v){
          for ( var y in v){
            if(v[x].codigo==v[y].codigo && v[x].descricao!=v[y].descricao){
              options5.map(function (a,index){
                var z = a.split(" ");
                  if(z[0]==v[y].codigo){
                    // if(z[1]!=""){
                  options5[index] = z[0]+ " Possue mais de uma descrição";
                  console.log("Mudou "+options5[index]);
                // }
                }
              });
            }
          }
        }*/
			  


			  $scope.options5opt =options5;
			  
			  // $sel_ics.html(options5.join());
			  //console.log($scope.options5opt);

			  var autocompleteIC = $('#filtro-ic').typeahead();
			  autocompleteIC.data('typeahead').source = $scope.options5opt;
			  //$('#filtro-ic').prop( "disabled", true );
			  $sel_ics
				.prop("disabled", false)
				.selectpicker('refresh');
			 });
			 
		  } else {
			  $('.notification .ng-binding').text("Carregando ICs...").selectpicker('refresh');
			  $('#filtro-ic').attr("placeholder", "Carregando ICs...");
			$sel_ics
			  .html("")
			  .prop("disabled", true)
			  .selectpicker('refresh');
			$('#alinkic').attr('disabled', 'disabled');

		  }
			});
		}, 500);		
	});
	
	
	$view.on("dblclick", "#filtro-ic",function(){
		////{ Limpa Filtro IC
		  $("#filtro-ic").attr("placeholder", "Itens de Controle");
		  $("#filtro-ic").val("");
		  $scope.filtroICSelect = [];
		  vetorFiltroIc = [];
		  $scope.filtros.isHFiltroIC = false;		
	});
	  
	 
	$view.on("click", ".btn-log-prerouting", function (ev) {
	  ev.preventDefault();

	  $scope.consultaCallLog($scope.chamada_atual);

	  $scope.$apply(function () {
		$scope.ocultar.log_formatado = true;
		$scope.ocultar.log_prerouting = false;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_original = true;
	  });
	});

	$view.on("click", ".btn-log-preroutingret", function (ev) {
	  ev.preventDefault();

	  $scope.consultaCallLog($scope.chamada_atual);

	  $scope.$apply(function () {
		$scope.ocultar.log_formatado = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = false;
		$scope.ocultar.log_original = true;
	  });
	});

	$view.on("click", ".btn-log-original", function (ev) {
	  ev.preventDefault();

	  $scope.consultaCallLog($scope.chamada_atual);

	  $scope.$apply(function () {
		$scope.ocultar.log_formatado = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_original = false;
	  });
	});

	$view.on("click", ".btn-log-formatado", function (ev) {
	  ev.preventDefault();

	  $scope.formataXML($scope.chamada_atual);

	  $scope.$apply(function () {
		$scope.ocultar.log_original = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_formatado = false;
	  });
	});

	$view.on("click", ".btn-log-formatadoEdEnx", function (ev) {
	  $scope.formataXML($scope.chamada_atual, true);
	  $scope.$apply();
	});

	$view.on("click", ".btn-log-formatadoIcEnx", function (ev) {
	  $('.btn-log-formatadoAllIcEnx').attr('checked', false);
	  $scope.itemEnxAll = false;
	  $scope.formataXML($scope.chamada_atual, true);
	  $scope.$apply();
	});

	$view.on("click", ".btn-log-formatadoAllIcEnx", function (ev) {
	  $('.btn-log-formatadoIcEnx').attr('checked', false);
	  $scope.itemEnx = false;
	  $scope.formataXML($scope.chamada_atual, true);
	  $scope.$apply();
	});

	$view.on("click", ".btn-exportar", function () {
	  $scope.exportaXLSX.apply(this);
	});

	// Limpa filtros
	$view.on("click", ".btn-limpar-filtros", function () {
	  $scope.limparFiltros.apply(this);
	});

	$scope.limparFiltros = function () {
	  iniciaFiltros();
	  componenteDataHora($scope, $view, "chamada");

	  var partsPath = window.location.pathname.split("/");
	  var part = partsPath[partsPath.length - 1];

	  var $btn_limpar = $view.find(".btn-limpar-filtros");
	  $btn_limpar.prop("disabled", true);
	  $btn_limpar.button('loading');

	  
	  ////{ Limpa Filtro ED
	  $("#filtro-ed").attr("placeholder", "Estados de Diálogo");
	  $("#filtro-ed").val("");
	  $scope.filtroEDSelect = [];
	  arr2 = [];
	  ////} Fim Limpa Filtro ED

	  ////{ Limpa Filtro IC
	  $("#filtro-ic").attr("placeholder", "Itens de Controle");
	  $("#filtro-ic").val("");
	  $scope.filtroICSelect = [];
	  vetorFiltroIc = [];
	  ////} Fim Limpa Filtro IC

	  $('#alinkApl').removeAttr('disabled');
	  $(".filtro-aplicacao").attr('multiple', 'multiple');
	  $('#ultimoED').attr('disabled', 'disabled');

	  setTimeout(function () {
		$btn_limpar.button('reset');
		$scope.$apply();
	  }, 500);
	};



	// Botão agora
	$view.on("click", ".btn-agora", function () {
	  $scope.agora.apply(this);
	});

	$scope.agora = function () {
	  iniciaAgora($view, $scope);
	};

	// Botão abortar
	$view.on("click", ".abortar", function () {
	  $scope.abortar.apply(this);
	});

	$scope.abortar = function () {
	  abortar($scope, "#pag-chamadas");
	};

	$view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  });

  // Consulta consolidado resumo venda TEMPLATE
  $scope.consultaConsolidadoB = function () {
	var $btn_baixarB = $(this);
	$btn_baixarB.button('loading');

	var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

	var arquivo = "planilhaDeCallFlow";

	var baseFile = '' + tempDir() + arquivo + '.xlsm';
	var txtFile = '' + tempDir() + arquivo + '.txt';

	if (!fs.existsSync(baseFile)) {
	  var stmt_teste = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo"
		+ " FROM " + db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='planilhaDeCallFlow'";
	  console.log(stmt_teste);
	  $('.notification .ng-binding').text($('.notification .ng-binding').text() + " Baixando template...");
	  db.query(stmt_teste, function (columns) {
		var dataAtualizacao = columns[0].value,
			nomeRelatorio = columns[1].value,
			arquivoB = columns[2].value;

		var buffer = toBuffer(toArrayBuffer(arquivoB));
		fs.writeFileSync(baseFile, buffer, 'binary');
	  }, function (err, num_rows) {
		if (num_rows < 0) {
		  return;
		}
		baixaTXT();
	  });
	} else {
	  if (formataDataHora(fs.statSync(baseFile).atime) !== formataDataHora(fs.statSync(baseFile).ctime)) {
		try {
		  fs.unlinkSync(baseFile);
		  $('.notification .ng-binding').text("Template inválido removido.");
		} catch (ex) {
		  $('.notification .ng-binding').text("Ação falhou. Feche arquivo " + arquivo + '.xlsm');
		}
		$('.btn-baixarB').button('reset');
		$view.find('.ampulheta').hide();
		return;
	  }
	  baixaTXT();
	}

	function baixaTXT() {
	  var stmt_teste = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo"
		+ " FROM " + db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='" + arquivo + "_txt'";
	  console.log(stmt_teste);
	  $('.notification .ng-binding').text($('.notification .ng-binding').text() + " Baixando dados...");
	  db.query(stmt_teste, function (columns) {
		var dataAtualizacao = columns[0].value,
			nomeRelatorio = columns[1].value,
			arquivoB = columns[2].value;

		zlib.unzip(new Buffer(arquivoB), function (err, buffer) {
		  if (err) { log(err); return; }
		  fs.writeFile(txtFile, buffer.toString(), function () { childProcess.exec(baseFile); });
		});

		//var buffer = toBuffer(toArrayBuffer(arquivoB));
		//fs.writeFileSync(txtFile, buffer, 'binary');
	  }, function (err, num_rows) {
		if (num_rows < 0) {
		  return;
		}
		$('.notification .ng-binding').text("Abrindo arquivo...");
		$('.btn-baixarB').button('reset');
		$view.find('.ampulheta').hide();
	  });
	}
  };

  // Lista chamadas conforme filtros
  $scope.listaChamadas = function () {
	var placeholder = $scope.filtros.chamador;
	$globals.numeroDeRegistros = 0;
	$scope.ordenacao = ['data_hora', 'chamador'];
	var $btn_exportar = $view.find(".btn-exportar");
	/*$btn_exportar.prop("disabled", true);*/

	var $btn_gerar = $(this);
	$btn_gerar.button('loading');

	var filtro_chamadores = $scope.filtros.chamador;
	if (filtro_chamadores === "x") filtro_chamadores = "";
	if (filtro_chamadores.match(/^\d*x\d*$/)) {
	  filtro_chamadores = filtro_chamadores.replace("x", "%");
	}

	filtro_chamadores = filtro_chamadores !== "" ? [filtro_chamadores] : [];

	var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;

	var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

	if (placeholder === "" && !$scope.callFlow && filtro_aplicacoes.length === 0 && numanis.length === 0 && invalidsNumanis.length === 0) {
	  atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
	  $btn_gerar.button('reset');
	  return;
	}

	var filtro_sites = $view.find("select.filtro-site").val() || [];
	var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
	var filtro_jsons = $view.find("select.filtro-jsons").val() || [];
	var contex = $('#contextVal').val().split(';');

	if (filtro_jsons.length === 0 && contex[0] ==="") contex = [];

	var filtro_valJsons = contex || [];
	var filtro_falhas = $view.find("select.filtro-falhas").val() || [];
	//var texto = "", ed = $(".filtro-ed").val() || [], ic = $(".filtro-ic").val() || [];
	var texto = "", ed = $scope.filtroEDSelect || [], ic = $scope.filtroICSelect || [];
	$scope.filtros.isUltimoED === true ? texto = "Último estado" : texto = "Estados de diálogo";

	var edNome = $scope.filtroEDSelect ? $scope.filtroEDSelect.map(function (e) { return '<se na="' + obtemNomeEstado($('.filtro-aplicacao').val(), e) + '"'; }) : [];

	$scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));
	if ($scope.ocultar.tratado) {
	  $('#pag-chamadas table').css({ 'margin-top': '40px', 'margin-bottom': '100px', 'max-width': '1190px', 'margin-left': 'auto', 'margin-right': 'auto' });
	  $scope.$apply();
	} else {
	  $('#pag-chamadas table').css({ 'margin-top': '40px', 'margin-bottom': '100px', 'max-width': '1310px' });
	  $scope.$apply();
	}

	// filtros usados

	$scope.filtros_usados = "";

	if (placeholder === "") {
	  $scope.filtros_usados += " Período: " + formataDataHoraBR($scope.periodo.inicio)
		+ " até " + formataDataHoraBR($scope.periodo.fim);
	}

	if (numanis.length > 0 && $('#data').prop("checked") == false) { $scope.filtros_usados = ""; }

	if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
	if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
	if (filtro_falhas!= "Falhas") { $scope.filtros_usados += " Falha: " + filtro_falhas; }
	if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
	if (filtro_jsons.length > 0) { $scope.filtros_usados += " Variáveis: " + filtro_jsons; }
	if (filtro_valJsons.length > 0 && filtro_valJsons[0] !== "") { $scope.filtros_usados += " Valores: " + filtro_valJsons; }

	if (filtro_chamadores != "") {
	  $scope.filtros_usados += ($('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? " Chamador: " :
			$('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' ? " Telefone tratado: " : $('.filtro-chamador').attr('placeholder') === 'Contrato' ? " Contrato: " :
			$('.filtro-chamador').attr('placeholder') === 'Protocolo' ? " Protocolo: " : " CPF/CNPJ: ") + filtro_chamadores;
	}
	if (ed.length > 0 && !$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
	  $scope.filtros_usados += " " + texto + ": " + ed;
	}
	if (ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
	  $scope.filtros_usados += " Items de controle: " + ic;
	}

	$scope.chamadas = [];

	var xml = "";
	var nps = "";
	var tabela = "IVRCDR";

	//if ($scope.filtros.isHFiltroIC === true && ic.length > 0) {
	if (ic.length > 0) {
	  var likes = ic.map(function (codIC) {
		return "XMLIVR LIKE '%<ty>pc</ty><va>" + codIC + "</va>%'";
	  });
	  xml = ", CASE WHEN " + likes.join(" OR ") + " THEN 0 ELSE 1 END AS XMLIVR";
	}

	if ($scope.filtros.nps === true) {
	  nps = ', respostaPesq';
	}

	var valor = placeholder !== "" ? 10000 : $scope.limite_registros;
	var stmt = db.use
	  + " SELECT" + testaLimite(valor)
	  + "   IVR.CodUCIDIVR, IVR.DataHora_Inicio, NumANI, ClienteTratado, IVR.CodAplicacao,"
	  + "   QtdSegsDuracao, SegmentoChamada, IndicSucessoGeral, Sequencia_EstadoDialogo " + xml + ", IndicDelig, transferid, ctxid, CodSite" + nps
	  + (filtro_falhas !== "Falhas" ? ', ErroAp, ErroCtg, SemPmt, SemDig, ErroVendas' : '')
	  + (filtro_jsons.length > 0 ? ', PreRoteamentoRet + contexto as PreRoteamentoRet' : '')
	  + " FROM " + db.prefixo + tabela + " as IVR";
	if (placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ') {
	  stmt += " JOIN CPF_CHAMADA AS CC"
		+ "   ON CC.CodUCIDIVR = IVR.CodUCIDIVR AND CC.CodAplicacao = IVR.CodAplicacao";
	}
	if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
	  stmt += " JOIN CONTRATO_CHAMADA AS CC"
		+ "   ON CC.CodUCIDIVR = IVR.CodUCIDIVR AND CC.CodAplicacao = IVR.CodAplicacao";
	}
	stmt += " WHERE 1 = 1";

	if (placeholder === "") {
	  stmt += "   AND IVR.DataHora_Inicio >= '" + formataDataHora($scope.periodo.inicio) + "'"
		+ "   AND IVR.DataHora_Inicio <= '" + formataDataHora($scope.periodo.fim) + "'";
	}

	if (filtro_aplicacoes.map === undefined) {
	  filtro_aplicacoes = [filtro_aplicacoes];
	}
	stmt += restringe_consulta("IVR.CodAplicacao", filtro_aplicacoes, true);
///////////////////////////////////////////////////////////////////////////////////////////////
	//if ($scope.filtros.isHFiltroED) {
	//    stmt += restringe_consulta_like("Sequencia_estadoDialogo", $scope.filtroEDSelect, $scope.oueED);
	//}
///////////////////////////////////////////////////////////////////////////////////////////////
	//stmt += restringe_consulta("CodSite", filtro_sites, true);

	//stmt += restringe_consulta_like("PreRoteamentoRet",filtro_jsons,true,"rot",filtro_valJsons);

	/*if (filtro_falhas != "Falhas") {
	  stmt += " AND " + filtro_falhas + " = 1 ";
	}*/

	//if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
	//  if ($scope.filtros.isUltimoED === true) {
	//    stmt += restringe_consulta_like("Sequencia_EstadoDialogo", ed);
	//  } else {
	//    stmt += restringe_consulta_like("XMLIVR", edNome);
	//  }
	//}

	//if (!$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
	//  stmt += restringe_consulta_like("XMLIVR", ic, false, true);
	//}

	//stmt += restringe_consulta("SegmentoChamada", filtro_segmentos, true);

	// Testar se filtra por chamador ou cliente tratado
	if (filtro_chamadores.length > 0) {
	  var campoTelefone = "";
	  var nonoDigito = false;

	  if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
		campoTelefone = "NumAni";
		nonoDigito = true;
	  } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
		campoTelefone = "ClienteTratado";
		nonoDigito = true;
	  } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ') {
		campoTelefone = "CC.CPF";
	  } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
		campoTelefone = "CC.CONTRATO";
	  } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Protocolo') {
		campoTelefone = "Protocolo";
	  }

	  if (filtro_chamadores[0].indexOf("%") !== -1) {
		stmt += restringe_consulta_like(campoTelefone, filtro_chamadores);
	  } else {
		// Tratar nono dígito
		if (nonoDigito && (filtro_chamadores[0].length === 11 || filtro_chamadores[0].length === 10)) {
		  if (filtro_chamadores[0].length === 10) {
			var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2), filtro_chamadores[0].substring(0, 2) + 9);
		  }
		  if (filtro_chamadores[0].length === 11) {
			var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2) + 9, filtro_chamadores[0].substring(0, 2));
		  }
		  filtro_chamadores.push(newNum);
		}
		stmt += restringe_consulta(campoTelefone, filtro_chamadores, true);
	  }
	}

	stmt += " order by dataHora_Inicio, numANI";

	stmtOriginal = stmt;
	dataHoraPesquisa = formataDataHora(new Date());

	if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' || placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
	  var tipoTel = $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? "NumAni" : "ClienteTratado";
	  var telCallFLow = false;
	  for (var i = 0; i < filtro_chamadores.length; i++) {
		if (obtemTelCallFlow(filtro_chamadores[i]) == true) { telCallFLow = true; }
	  }

	  var stmtPre = db.use;
	  if (telCallFLow) {
		stmtOriginal = stmtOriginal.replace('IVRCDR', 'chamadascallflow') + " ";
		//stmt = stmtPre + stmt.replace('IVRCDR','chamadascallflow') + " ELSE IF (SELECT count(*) FROM  IVRCDR WHERE NUMANI in ('"+filtro_chamadores.join('\'\,\'')+"')) > 0" +  stmt+ " ELSE BEGIN SET nocount on INSERT INTO contingenciaEstalo VALUES ('"+dataHoraPesquisa+"','"+loginUsuario+"','"+dominioUsuario+"','"+quoteReplace("select * from ivrcdr where NUMANI in ('"+filtro_chamadores.join('\'\,\'')+"')")+"','"+win.title+"',null) SET nocount off END ";
	  } else {
		stmtOriginal = stmtOriginal + ";INSERT INTO contingenciaEstalo VALUES ('" + dataHoraPesquisa + "','" + loginUsuario + "','" + dominioUsuario + "','" + quoteReplace("select * from ivrcdr where " + tipoTel + " in ('" + filtro_chamadores.join('\'\,\'') + "')") + "','" + win.title + "',null)";
	  }
	}

	var stmts = [];

	if (numanis.length > 0 && placeholder === "") {
	  var tipoTel = $('#2h').prop("checked") ? "Telefone Chamador" : "Telefone Tratado";
	  stmts = arrayNumAnisQuery(stmt, numanis, tipoTel, $scope.extratorArquivoPorData, data_ini, data_fim);
	  $('#import2').css('background', '#f5f5f5').selectpicker('refresh');

	  for (var i = 0; i < invalidsNumanis.length; i++) {
		$scope.chamadas.push({
		  UCID: "",
		  data_hora: "",
		  data_hora_BR: "",
		  data: "",
		  chamador: $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado'? invalidsNumanis[i]:"",
		  tratado: $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador'? invalidsNumanis[i]:"",
		  cod_aplicacao: "",
		  aplicacao: "",
		  duracao: "",
		  segmento: "",
		  encerramento: "",
		  estados: "",
		  estado_final: "",
		  transferid: "",
		  ctxid: ""
		});
	  }

	  numanis = [];
	  invalidsNumanis = [];
	  $("#browser").val("");
	} else {
	  if (placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' || placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
		stmt = "select getdate()/*"+stmtOriginal+"*/";
		stmts =[stmtOriginal, stmt, "select getdate()/*"+stmtOriginal+"*/"];
	  } else {
		if (ed.indexOf("0") < 0) {
		  if (placeholder === "") {
			//Por 5 minutos
			stmts = arrayCincoMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
		  } else {
			stmts.push(stmt);
		  }
		} else {
		  if (placeholder === "") {
			//Por minuto
			stmts = arrayMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
		  } else {
			stmts.push(stmt);
		  }
		}
	  }
	}

	var executaQuery = "";
	//var aplsPrompts = [];

	if (placeholder !== "") {
	  executaQuery = executaQuery3;
	  log(stmt);
	  disparo = new Date();
	} else {
	  if ($scope.filtros.isUltimoED === true) {
		executaQuery = executaQuery2;
		log("Último ED->" + stmt);
		disparo = new Date();
	  } else {
		executaQuery = executaQuery1;
		log(stmt);
		disparo = new Date();
	  }
	}

	var controle = 0;
	var total = 0;

	var contador = 0;
	var tempoIni = 0;
	var tempoFim = 0;
	var tempoDecorrido = 0;
	var ucids = [];
	var aplsDistincts = [];
	var disparo;
	var tempoPassadoPreCont;
	var chamadasTemp = [];

	// Padrão
	function executaQuery1(columns) {
	  contador++;

	  var UCID = columns[0].value,
		data_hora = formataDataHora(columns[1].value),
		data_hora_BR = formataDataHoraBR(columns[1].value),
		data = columns[1].value;
		//chamador = columns[2].value === 'Anonymous' ? columns[2].value : +columns[2].value;

	  var chamador = columns[2].value,
		  tratado = columns[3].value,
		  cod_aplicacao = columns[4].value,
		  aplicacao = obtemNomeAplicacao(columns[4].value),
		  duracao = columns[5].value,
		  cod_segmento = columns[6].value,
		  segmento = cod_segmento,
		  encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
		  estados = columns[8].value.split(","),
		  estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
		  transferid = columns["transferid"].value,
		  ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
		  codsite = columns["CodSite"]!== undefined ? columns["CodSite"].value : "",
		  preroteamentoret = columns["PreRoteamentoRet"]!== undefined ? columns["PreRoteamentoRet"].value : "",
		  xmlivr = columns["XMLIVR"]!== undefined ? columns["XMLIVR"].value : "",
		  //xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join("") : "")  : "",
		  npss = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
		  npsr = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

	  if (filtro_falhas != "Falhas") {
		var erroap = columns["ErroAp"]!== undefined ? columns["ErroAp"].value : "",
			erroctg = columns["ErroCtg"]!== undefined ? columns["ErroCtg"].value : "",
			erropmt = columns["SemPmt"]!== undefined ? columns["SemPmt"].value : "",
			errodig = columns["SemDig"]!== undefined ? columns["SemDig"].value : "",
			errovendas = columns["ErroVendas"]!== undefined ? columns["ErroVendas"].value : "";
	  }

	   if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
		 segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
	   }
	  // if (cod_aplicacao === "USSD" || cod_aplicacao === "USSD880" || cod_aplicacao === "USSD3000" || cod_aplicacao === "USSD144") segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);

	  chamadasTemp.push({
		UCID: UCID,
		data_hora: data_hora,
		data_hora_BR: data_hora_BR,
		data: data,
		chamador: chamador,
		tratado: tratado,
		cod_aplicacao: cod_aplicacao,
		aplicacao: aplicacao,
		duracao: duracao,
		segmento: segmento,
		encerramento: encerramento,
		estados: estados,
		estado_final: estado_final,
		transferid: transferid,
		ctxid: ctxid,
		codsite: codsite,
		preroteamentoret: preroteamentoret,
		xmlivr: xmlivr,
		npss: npss,
		npsr: npsr,
		erroap: erroap,
		erroctg: erroctg,
		erropmt: erropmt,
		errodig: errodig,
		errovendas: errovendas
	  });
	}

	// Último estado
	function executaQuery2(columns) {
	  var UCID = columns[0].value,
		  data_hora = formataDataHora(columns[1].value),
		  data_hora_BR = formataDataHoraBR(columns[1].value),
		  data = columns[1].value;
		  //chamador = columns[2].value === 'Anonymous' ? columns[2].value : +columns[2].value;
	  var chamador = columns[2].value,
		  tratado = columns[3].value,
		  cod_aplicacao = columns[4].value,
		  aplicacao = obtemNomeAplicacao(columns[4].value),
		  duracao = columns[5].value,
		  cod_segmento = columns[6].value,
		  segmento = cod_segmento,
		  encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
		  estados = columns[8].value.split(","),
		  estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
		  cod_estado_final = "",
		  transferid = columns["transferid"].value,
		  ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
		  codsite = columns["CodSite"]!== undefined ? columns["CodSite"].value : "",
		  preroteamentoret = columns["PreRoteamentoRet"]!== undefined ? columns["PreRoteamentoRet"].value : "",
		  xmlivr = columns["XMLIVR"]!== undefined ? columns["XMLIVR"].value : "",
		  //xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join(""):"")  : "",
		  npss = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
		  npsr = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

	  if (filtro_falhas != "Falhas") {
		var erroap = columns["ErroAp"]!== undefined ? columns["ErroAp"].value : "",
			erroctg = columns["ErroCtg"]!== undefined ? columns["ErroCtg"].value : "",
			erropmt = columns["SemPmt"]!== undefined ? columns["SemPmt"].value : "",
			errodig = columns["SemDig"]!== undefined ? columns["SemDig"].value : "",
			errovendas = columns["ErroVendas"]!== undefined ? columns["ErroVendas"].value : "";
	  }

	   if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
		 segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
	   }
	  // if (cod_aplicacao === "USSD" || cod_aplicacao === "USSD880" || cod_aplicacao === "USSD3000" || cod_aplicacao === "USSD144") segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);

	  for (var i = estados.length - 1; i >= 0; i--) {
		var codigo = estados[i],
		  estado = obtemEstado(cod_aplicacao, codigo);
		if (estado && estado.exibir) {
		  estado_final = estado.nome;
		  cod_estado_final = estado.codigo;
		  break;
		}
	  }

	  var filtro_eds = $scope.filtroEDSelect;

	  var items = [];
	  items = jQuery.grep([cod_estado_final], function (item) {
		return jQuery.inArray(item, filtro_eds) < 0;
	  });

	  if (items.length === 0) {
		chamadasTemp.push({
		  UCID: UCID,
		  data_hora: data_hora,
		  data_hora_BR: data_hora_BR,
		  data: data,
		  chamador: chamador,
		  tratado: tratado,
		  cod_aplicacao: cod_aplicacao,
		  aplicacao: aplicacao,
		  duracao: duracao,
		  segmento: segmento,
		  encerramento: encerramento,
		  estados: estados,
		  estado_final: estado_final,
		  transferid: transferid,
		  ctxid: ctxid,
		  codsite: codsite,
		  preroteamentoret: preroteamentoret,
		  xmlivr: xmlivr,
		  npss: npss,
		  npsr: npsr,
		  erroap: erroap,
		  erroctg: erroctg,
		  erropmt: erropmt,
		  errodig: errodig,
		  errovendas: errovendas
		});
	  }
	}

	// Padrão
	function executaQuery3(columns) {
	  if (columns["CodUCIDIVR"] !== undefined) {
		var UCID = columns[0].value,
		  data_hora = formataDataHora(columns[1].value),
		  data_hora_BR = formataDataHoraBR(columns[1].value),
		  data = columns[1].value;
		  //if(columns[2].value=== 'Anonymous'){ chamador = columns[2].value; }else{ chamador = +columns[2].value; };

		var chamador = columns[2].value,
			tratado = columns[3].value,
			cod_aplicacao = columns[4].value,
			aplicacao = obtemNomeAplicacao(columns[4].value),
			duracao = columns[5].value,
			cod_segmento = columns[6].value,
			segmento = cod_segmento,
			encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
			estados = columns[8].value.split(","),
			estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
			transferid = columns["transferid"].value,
			ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
			codsite = columns["CodSite"]!== undefined ? columns["CodSite"].value : "",
			preroteamentoret = columns["PreRoteamentoRet"]!== undefined ? columns["PreRoteamentoRet"].value : "",
			xmlivr = columns["XMLIVR"]!== undefined ? columns["XMLIVR"].value : "",
			//xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join(""):"")  : "",
			npss = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
			npsr = columns["respostaPesq"]!== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

		if (filtro_falhas != "Falhas") {
		  var erroap = columns["ErroAp"]!== undefined ? columns["ErroAp"].value : "",
			  erroctg = columns["ErroCtg"]!== undefined ? columns["ErroCtg"].value : "",
			  erropmt = columns["SemPmt"]!== undefined ? columns["SemPmt"].value : "",
			  errodig = columns["SemDig"]!== undefined ? columns["SemDig"].value : "",
			  errovendas = columns["ErroVendas"]!== undefined ? columns["ErroVendas"].value : "";
		}

		 if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
		  segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
		}
		// if (cod_aplicacao === "USSD" || cod_aplicacao === "USSD880" || cod_aplicacao === "USSD3000" || cod_aplicacao === "USSD144") segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);



		if (ucids.indexOf(UCID) < 0) {
		  if (placeholder !== "") {
			for (var i = estados.length - 1; i >= 0; i--) {
			  var codigo = estados[i],
				estado = obtemEstado(cod_aplicacao, codigo);
			  if (estado && estado.exibir) {
				estado_final = estado.nome;
				break;
			  }
			}

			$scope.chamadas.push({
			  UCID: UCID,
			  data_hora: data_hora,
			  data_hora_BR: data_hora_BR,
			  data: data,
			  chamador: chamador,
			  tratado: tratado,
			  cod_aplicacao: cod_aplicacao,
			  aplicacao: aplicacao,
			  duracao: duracao,
			  segmento: obtemNomeSegmento(segmento),
			  encerramento: encerramento,
			  estados: estados,
			  estado_final: estado_final,
			  transferid: transferid,
			  ctxid: ctxid,
			  codsite: codsite,
			  preroteamentoret: preroteamentoret,
			  xmlivr: xmlivr,
			  npss: npss,
			  npsr: npsr,
			  erroap: erroap,
			  erroctg: erroctg,
			  erropmt: erropmt,
			  errodig: errodig,
			  errovendas: errovendas
			});

			$scope.$apply(function () {
			  $scope.ordenacao = ['data_hora', 'chamador'];

			  if (placeholder !== "") {
				var arrSort = $scope.chamadas;
				arrSort.sort(function (a, b) {
				  return (a.data< b.data) ? 1 : ((b.data< a.data) ? -1 : 0);
				});
				$scope.chamadas = arrSort;
			  }
			});
		  } else {
			chamadasTemp.push({
			  UCID: UCID,
			  data_hora: data_hora,
			  data_hora_BR: data_hora_BR,
			  data: data,
			  chamador: chamador,
			  tratado: tratado,
			  cod_aplicacao: cod_aplicacao,
			  aplicacao: aplicacao,
			  duracao: duracao,
			  segmento: segmento,
			  encerramento: encerramento,
			  estados: estados,
			  estado_final: estado_final,
			  transferid: transferid,
			  ctxid: ctxid,
			  codsite: codsite,
			  preroteamentoret: preroteamentoret,
			  xmlivr: xmlivr,
			  npss: npss,
			  npsr: npsr,
			  erroap: erroap,
			  erroctg: erroctg,
			  erropmt: erropmt,
			  errodig: errodig,
			  errovendas: errovendas
			});
		  }

		  ucids.push(UCID);
		}
	  } else {
		if (columns['status']!== undefined) {
		  contador++;
		  console.log("TIMER-> "+contador+" "+aplStatusMail);

		  if (contador > tempoLimite) {
			console.log("Enviar email");
			controle++;
		  }

		  if (columns['status'].value !== null && columns['status'].value !== 2) {
			columns['status'].value === 1 ? console.log('uar 1') : console.log('uar 0');
			controle++;
		  }
		}
	  }
	}

	/*if (columns['status'] !== undefined || columns['status'] !== 2) {
	  if (columns['status'].value !== null) {
		columns['status'].value === 1 ? console.log('uar 1') : console.log('uar 0');
		controle++;
	  }
	}

	// próxima
	if (placeholder !== "") {
	  stmts = stmts[0];
	}

	if (stmts[controle].toString() === "select getdate()") {
	  controle = 0;
	  total = 0;
	  stmts = [stmtOriginal.substring(0,stmtOriginal.indexOf('order'))];
	  proxima(stmtOriginal);
	  return;
	}*/

	if (placeholder === "") {
	  //Data final do penúltimo array com base na hora final do filtro, último array é uma flag
	  //Por hora
	  if (ed.indexOf("0") < 0) {
		if (stmts[stmts.length -2].toString().match(/IVR.DataHora_Inicio <= \'.+\' /g) !== null) {
		  stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/IVR.DataHora_Inicio <= \'.+\' /g, "IVR.DataHora_Inicio <= \'" +formataDataHora(new Date(data_fim))+"\' ")];
		} else if (stmts[stmts.length -2].toString().match(/IVR.DataHora_Inicio <= \'.+\'' /g) !== null) {
		  stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/IVR.DataHora_Inicio <= \'.+\'' /g, "IVR.DataHora_Inicio <= \'" +formataDataHora(new Date(data_fim))+"\' ")];
		}
		//stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(formataDataHora(precisaoMinutoFim(data_fim)), formataDataHora(data_fim))];
	  } else {
		//Data final do penúltimo array com base na hora final do filtro, último array é uma flag
		//Por minuto
		stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(formataDataHora(precisaoSegundoFim(data_fim)), formataDataHora(data_fim))];
	  }
	}

	function executaTestesDeInsercao() {
	  for (var c = 0; c < chamadasTemp.length; c++) {
		var inserir = true;
		
		if (ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled') && ed.length == 0) {
			inserir = (chamadasTemp[c].xmlivr === 0);
		} else if (ed.length > 0 && !$('div.btn-group.filtro-ed .btn').hasClass('disabled') && ic.length == 0) {
			for (var i = 0; i < ed.length; i++) {
				inserir = (chamadasTemp[c].estados.indexOf(ed[i]) >= 0);
				if(inserir) break;				
			}
		} else if (ed.length > 0 && !$('div.btn-group.filtro-ed .btn').hasClass('disabled') && ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
			for (var i = 0; i < ed.length; i++) {
				inserir = (chamadasTemp[c].estados.indexOf(ed[i]) >= 0);
				if(inserir) break;
			}
			if (inserir) {
				inserir = (chamadasTemp[c].xmlivr === 0);
			}
		}

		// Site
		if (filtro_sites.length > 0) {
		  inserir = (filtro_sites.indexOf(chamadasTemp[c].codsite) >= 0);
		}

		// Segmento
		if (filtro_segmentos.length > 0) {
		  inserir = (filtro_segmentos.indexOf(chamadasTemp[c].segmento) >= 0);
		}

		if (filtro_falhas !== "Falhas") {
		  inserir = (chamadasTemp[c].erroap === 1 && filtro_falhas === "ErroAp" || chamadasTemp[c].erroctg === 1 && filtro_falhas === "ErroCtg" || chamadasTemp[c].erropmt === 1 && filtro_falhas === "SemPmt" || chamadasTemp[c].errodig === 1 && filtro_falhas === "SemDig" || chamadasTemp[c].errovendas === 1 && filtro_falhas === "ErroVendas");
		}

		if (filtro_jsons.length > 0) {
		  var strs = $('#contextVal').val().split(';');
		  for (var i=0; i < filtro_jsons.length; i++) {
			var s = (strs[i] !== "" && strs[i] !== undefined) ? '"'+strs[i]+'"' : "";
			inserir = ((chamadasTemp[c].preroteamentoret.toUpperCase()).match((filtro_jsons[i]+':'+s).toUpperCase()) !== null);
		  }
		}

		if ($scope.filtros.nps === true) {
		  inserir = (chamadasTemp[c].npss !== "" || chamadasTemp[c].npsr !== "");
		}

		if (inserir) {
		  if ($scope.filtros.isUltimoED === false) {
			for (var i = chamadasTemp[c].estados.length - 1; i >= 0; i--) {
			  var codigo = chamadasTemp[c].estados[i],
				estado = obtemEstado(chamadasTemp[c].cod_aplicacao, codigo);
			  if (estado && estado.exibir) {
				chamadasTemp[c].estado_final = estado.nome;
				break;
			  }
			}
		  }

		  $scope.chamadas.push({
			UCID: chamadasTemp[c].UCID,
			data_hora: chamadasTemp[c].data_hora,
			data_hora_BR: chamadasTemp[c].data_hora_BR,
			data: chamadasTemp[c].data,
			chamador: chamadasTemp[c].chamador,
			tratado: chamadasTemp[c].tratado,
			cod_aplicacao: chamadasTemp[c].cod_aplicacao,
			aplicacao: chamadasTemp[c].aplicacao,
			duracao: chamadasTemp[c].duracao,
			segmento: obtemNomeSegmento(chamadasTemp[c].segmento),
			encerramento: chamadasTemp[c].encerramento,
			estados: chamadasTemp[c].estados,
			estado_final: chamadasTemp[c].estado_final,
			transferid: chamadasTemp[c].transferid,
			ctxid: chamadasTemp[c].ctxid,
			npss: chamadasTemp[c].npss,
			npsr: chamadasTemp[c].npsr
		  });
		}

		/*if ($scope.chamadas.length % 1000 === 0) {
		  $scope.$apply(function () {
			$scope.ordenacao = ['data_hora', 'chamador'];

			if (placeholder !== "") {
			  var arrSort = $scope.chamadas;
			  arrSort.sort(function (a,b) {
				return a.data < b.data ? 1 : (b.data < a.data ? -1 : 0);
			  });
			  $scope.chamadas = arrSort;
			}
		  });
		}*/
	  }

	  console.log("terminei");
	}

	function proxima(s) {
	  tempoIni = tempoFim;
	  tempoFim = contador;
	  tempoDecorrido = tempoFim-tempoIni;

	  chamadasTemp = [];

	  db.query(s, executaQuery, function (err, num_rows) {
		console.log("Executando query-> " + s + " " + num_rows);

		num_rows !== undefined ? total += num_rows : total += 0;

		var telCallFLow = false;
		for (var i = 0; i < filtro_chamadores.length; i++) {
		  if (obtemTelCallFlow(filtro_chamadores[i]) === true) { telCallFLow = true; }
		}

		if (telCallFLow == false && placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' || telCallFLow == false && placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
		  proxima2("SELECT top 1 login, dominio, query, status from contingenciaestalo where login = '"+loginUsuario+"' and dominio = '"+dominioUsuario+"' and datahora = '"+dataHoraPesquisa+"'");
		  console.log("passei aqui");
		  var tempoPassado = new Date() - disparo;
		  log("Secs(PreCont): "+militosec(tempoPassado)+" "+stmt);
		  tempoPassadoPreCont = militosec(tempoPassado);
		} else if (placeholder !== "") {
		  retornaStatusQuery($scope.chamadas.length, $scope);
		  var tempoPassado = new Date() - disparo;
		  log("Secs: "+militosec(tempoPassado)+" "+stmt);
		  $btn_gerar.button('reset');
		  if ($scope.chamadas.length > 0) {
			$btn_exportar.prop("disabled", false);
		  } else {
			$btn_exportar.prop("disabled", "disabled");
		  }
		} else {
		  $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, stmts.length) + " Pesquisados: " + total + (tempoDecorrido > 15000 ? " A consulta está demorando mais do que o esperado. " : "")).selectpicker('refresh');

		  controle++;

		  $scope.$apply();

		  // Executa dbquery enquanto array de querys menor que length-1
		  if (controle < stmts.length - 1) {
			if (placeholder === "") { executaTestesDeInsercao(); }

			proxima(stmts[controle].toString());
			console.log(controle);
		  }

		  //Evento fim
		  if (controle === stmts.length - 1) {
			if (placeholder === "") { executaTestesDeInsercao(); }

			if (err) {
			  retornaStatusQuery(undefined, $scope);
			  var tempoPassado = new Date() - disparo;
			  log("Secs: "+militosec(tempoPassado)+" "+stmt);
			  console.log("heim");
			} else {
			  retornaStatusQuery($scope.chamadas.length, $scope);
			  var tempoPassado = new Date() - disparo;
			  log("Secs: "+militosec(tempoPassado)+" "+stmt);
			}
			console.log(err);
			$btn_gerar.button('reset');
			if ($scope.chamadas.length > 0) {
			  $btn_exportar.prop("disabled", false);
			   $('#grid').css('display','block');
			} else {
			  $btn_exportar.prop("disabled", "disabled");
			  $('#grid').css('display','none');
			}
		  }
		}
	  });
	}

	function proxima2(s) {
	  db.query(s, executaQuery, function (err, num_rows) {
		console.log("Executando query-> " + s + " " + num_rows);

		num_rows !== undefined ? total += num_rows : total += 0;

		/*if (num_rows === 0 && s.match('INSERT') !== null) {
		  stmts = [];
		  stmts.push("SELECT top 1 login, dominio, query, status from contingenciaestalo where login = '"+loginUsuario+"' and dominio = '"+dominioUsuario+"' and datahora = '"+dataHoraPesquisa+"'");
		  controle++;
		  //return;
		}*/

		$('.notification .ng-binding').text("Aguardando resposta... (Dados de contigência de outro servidor para meses anteriores)").selectpicker('refresh');

		//controle++;

		$scope.$apply();

		// Executa dbquery enquanto array de querys menor que length-1
		if (controle < stmts.length - 1) {
		  if (placeholder === "") { executaTestesDeInsercao(); }
		  proxima2("/*uar 1*/SELECT top 1 login, dominio, query, status from contingenciaestalo where login = '"+loginUsuario+"' and dominio = '"+dominioUsuario+"' and datahora = '"+dataHoraPesquisa+"'");
		}

		// Evento fim
		if (controle === stmts.length - 1) {
		  if (placeholder === "") { executaTestesDeInsercao(); }

		  if (err) {
			retornaStatusQuery(undefined, $scope);
			var tempoPassado = new Date() - disparo;
			log("Secs: "+militosec(tempoPassado)+" "+stmt);
			console.log("heim");
		  } else {
			if (s.substring(0, 9) !== "/*uar 1*/") {
			  retornaStatusQuery($scope.chamadas.length, $scope);
			  var tempoPassado = new Date() - disparo;
			  log("Secs(Cont): "+(militosec(tempoPassado) - tempoPassadoPreCont)+" "+(stmt!== undefined ? stmt : stmtOriginal));
			}

			console.log("thurai-> "+contador);

			if (contador > tempoLimite && aplStatusMail) {
			  /*if (fs.existsSync(tempDir()+'anexo.txt')) {
				fs.unlinkSync(tempDir()+'anexo.txt');
			  }
			  fs.appendFileSync(tempDir()+'anexo.txt', '<!DOCTYPE html><html data-ng-app="Estalo" lang="pt-br"><head><meta charset="utf-8" name="viewport" content="initial-scale=1.0, user-scalable=no"  /><title>Estalo</title><link rel="stylesheet" href="http://www.versatec.com.br/oi2/css/estilo1.css" /><link rel="stylesheet" href="http://www.versatec.com.br/oi2/css/estilo2.css" /></head><body><body>'+$('body').html()+'</body></html>');*/
			  var stmt = db.use+" insert into emails values(GETDATE(),'"+loginUsuario+"','"+dominioUsuario+"','Consulta ao "+$('.filtro-chamador').attr('placeholder')+" "+$('.filtro-chamador').val()+" está demorando mais do que o esperado.','')";
			  setTimeout(function () {
				executaThreadBasico('extratorDiaADia2', stringParaExtrator(stmt, []), function (dado) { console.log(dado); });
				$('.notification .ng-binding').text($('.notification .ng-binding').text()+" Não foi possível trazer dados do banco de contingência.").selectpicker('refresh');
			  }, 5000);
			  aplStatusMail = false;
			}

			if (stmts[controle].toString().substring(0, 16) === "select getdate()") {
			  controle = 0;
			  total = 0;
			  stmts = [stmtOriginal.substring(0, stmtOriginal.indexOf('INSERT'))];
			  proxima2(stmtOriginal.substring(0, stmtOriginal.indexOf('INSERT')));
			  return;
			}
		  }

		  console.log(err);
		  $btn_gerar.button('reset');
		  if ($scope.chamadas.length > 0) {
			$btn_exportar.prop("disabled", false);
			$('#grid').css('display','block');
		  } else {
			$btn_exportar.prop("disabled", "disabled");
			$('#grid').css('display','none');
		  }
		}
	  });
	}

	// Disparo inicial

	proxima(stmts[0].toString());
  };

  // Consulta o log da chamada
  $scope.consultaCallLog = function (chamada) {
	// Se a chamada acabou de ser consultada (incluindo o log original), não há nada a fazer
	if (chamada === $scope.chamada_atual && $scope.callLog !== "") return true;

	var tabela = "CallLog_";
	if (unique2(cache.apls.map(function (a) { if (a.nvp) return a.codigo; })).indexOf(chamada.cod_aplicacao) >= 0) {
	  tabela = "CallLogNVP_";
	}
	if (chamada.cod_aplicacao === "MENUOI") {
	  tabela = "CallLogM4U_";
	}

	var stmt = "SELECT TOP 1 Comprimido FROM " + tabela + chamada.data_hora.substr(5, 2)
	  + " WHERE CodUCID = '" + chamada.UCID + "'"
	  + "   OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'"
	  + " ORDER BY LEN(CodUCID)";
	log(stmt);

	$scope.chamadaCallLogData = chamada.data;
	$scope.tabelaCallLog = tabela;

	db.query(stmt, function (columns) {
	  var comprimido = columns[0].value;

	  // Descomprimir o log original
	  zlib.unzip(new Buffer(comprimido), function (err, buffer) {
		$scope.$apply(function () {
		  if (err) { log(err); return; }
		  $scope.callLog = buffer.toString();
		   search(".log-original");
		});
	  });
	}, function (err, num_rows) {
	  if (err) {
		$('#myModalLabel').text("Erro de conexão");
		retornaMsgErro(err);
	  } else if (num_rows === 0) {
		$('#myModalLabel').text("Log original não encontrado");
		if ($scope.tabelaCallLog === "CallLog_" && $scope.chamadaCallLogData < cache.datsUltMesCallLog) $('#myModalLabel').text("O log já não se encontra disponível na nossa base de dados");
		if ($scope.tabelaCallLog === "CallLogNVP_" && $scope.chamadaCallLogData < cache.datsUltMesCallLogNVP) $('#myModalLabel').text("O log já não se encontra disponível na nossa base de dados");
		if ($scope.tabelaCallLog ==="CallLogM4U_" && $scope.chamadaCallLogData < cache.datsUltMesCallLogM4U) $('#myModalLabel').text("O log já não se encontra disponível na nossa base de dados");
	  } else {
		$('#myModalLabel').text("Log da chamada");
	  }
	  
	  // Log ORIGINAL
	 
	search(".log-original");
	});
	return true;
  };

  $scope.infos = [];

  // Consulta e formata o XML da chamada
  $scope.formataXML = function (chamada, check) {
	 
	//alert("ED "+$scope.itemEnx+" IC "+$scope.estadoEnx);

	$('#myModalLabel').text("Log da chamada");

	/*if (!cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts')) {
	  obtemPromptsDasAplicacoes(chamada.cod_aplicacao);
	}*/

	delete cache.aplicacoes[chamada.cod_aplicacao].prompts;
	delete cache.aplicacoes[chamada.cod_aplicacao].itens_controleOnDemand;

	/*if (!cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controle')) {
	  if (chamada.cod_aplicacao !== 'CORPO') {

		//obtemItensDeControleDasAplicacoes(chamada.cod_aplicacao, true);
	  }
	} else {
	  //if (cache.aplicacoes[chamada.cod_aplicacao].itens_controle.length <= 100) {
	  //  delete cache.aplicacoes[chamada.cod_aplicacao].itens_controle;
	  //  obtemItensDeControleDasAplicacoes(chamada.cod_aplicacao,true);
	  //}
	}*/

	// Se a chamada acabou de ser consultada, não há nada a fazer
	if (check === undefined) {
	  if (chamada === $scope.chamada_atual) return true;
	}

	// Limpar o log original da chamada consultada anteriormente
	$scope.chamada_atual = chamada;
	$scope.callLog = "";
	$scope.ocultar.log_original = true;
	$scope.ocultar.log_prerouting = true;
	$scope.ocultar.log_preroutingret = true;
	$scope.ocultar.log_formatado = false;
	$scope.logxml = "";

	// Buscar o log em XML
	var stmt = "SELECT XMLIVR,PREROTEAMENTO,PREROTEAMENTORET FROM IVRCDR WHERE CodUCIDIVR = '" + chamada.UCID + "'";
	log(stmt);

	var xml = "",
		routing = "",
		routingRet = "";
	db.query(stmt, function (columns) {
	  xml = columns[0].value;
	  routing = String(columns[1].value),
	  routingRet = String(columns[2].value);
	  parseXMLPrompts(columns[0].value, chamada);
	  parseXMLICs(columns[0].value, chamada);
	}, function (err, num_rows) {
	  if (num_rows < 0) {
		return;
	  }
	  var teste = setInterval(function () {
		if (cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') && cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controleOnDemand') && cache.aplicacoes[chamada.cod_aplicacao].itens_controleOnDemand['indice'] !== undefined){ /*|| cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') && chamada.cod_aplicacao === 'CORPO') {*/
		  clearInterval(teste);
		  $('div.modal-header h3').text("Log da chamada");
		  $scope.$apply(function () {
			// Formatar o log em XML
			//console.log(xml);
			if ($scope.estadoEnx === true) {
			  $scope.logxml = parseXML(RemoverEDsExcedentes(xml), chamada, $scope.itemEnx, $scope.itemEnxAll);
			} else {
			  $scope.logxml = parseXML(xml, chamada, $scope.itemEnx, $scope.itemEnxAll);
			}

			$scope.prerouting = obterRouting(routing);
			$scope.preroutingret = obterRouting(routingRet);
		  });
		} else {
		  $('div.modal-header h3').text("Carregando...");
		}
	  }, 500);
	});
	console.log("nova consulta...");

	return true;
  };

  function obterRouting(valor) {
	if (valor === "") {
	  return "Não há dados";
	}

	valor= valor.replace("}", "").replace("{", "");
	var obj = valor.split(",");

	for (var i = 0; i < obj.length; i++) {
	  var cod= obj[i].substring(0, 2);
	  var nome = obtemNomeVarJSon(cod);
	  obj[i] = obj[i].replace(cod, nome).toUpperCase();
	}

	var resultado = obj.sort().toString();
	while (resultado.indexOf(",") > 0) {
	  resultado = resultado.replace(",", "\n");
	}

	return resultado;
  }

  // Exporta planilha XLSX
  $scope.exportaXLSX = function () {
	var $btn_exportar = $(this);
	$btn_exportar.button('loading');

	if ($scope.chamadas.length > 50000) {
	  if (fs.existsSync(tempDir3()+'tChamadasMore.xlsm')) {
		fs.unlinkSync(tempDir3()+'tChamadasMore.xlsm');
	  }

	  // TEMPLATE
	  var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo"
		+ " FROM "+ db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='tChamadasMore'";
	  log(stmt);
	  db.query(stmt, function (columns) {
		var dataAtualizacao = columns[0].value,
			nomeRelatorio = columns[1].value,
			arquivo = columns[2].value;

		var milis = new Date();
		var baseFile = 'tChamadasMore.xlsm';
		var buffer = toBuffer(toArrayBuffer(arquivo));
		fs.writeFileSync(tempDir3()+baseFile, buffer, 'binary');
	  }, function (err, num_rows) {
		if (fs.existsSync(tempDir3()+'dados_chamadas.txt')) {
		  fs.unlinkSync(tempDir3()+'dados_chamadas.txt');
		}
		var i = 0;
		$scope.chamadas.forEach(function (v) {
		  fs.appendFileSync(tempDir3()+'dados_chamadas.txt', v.data_hora_BR+'\t'+v.chamador+'\t'+v.tratado+'\t'+v.aplicacao+'\t'+v.duracao+'\t'+v.segmento+'\t'+v.encerramento+'\t'+v.estado_final+'\t'+(v.transferid !== "" ? v.transferid : "NA")+'\t'+(v.ctxid !== "" ? v.ctxid : "NA")+'\t'+(v.npss !== "" ? v.npss : "NA")+'\t'+(v.npsr !== "" ? v.npsr : "NA")+'\t'+'\t\n');

		  i++;
		  if (i === $scope.chamadas.length) {
			childProcess.exec(tempDir3()+'tChamadasMore.xlsm');
		  }
		});

		setTimeout(function () {
		  $btn_exportar.button('reset');
		}, 500);
	  });
	} else {
	  // TEMPLATE
	  var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo"
		+ " FROM " + db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='tChamadas'";
	  log(stmt);
	  db.query(stmt, function (columns) {
		var dataAtualizacao = columns[0].value,
			nomeRelatorio = columns[1].value,
			arquivo = columns[2].value;

		var milis = new Date();
		var baseFile = 'tChamadas.xlsx';
		var buffer = toBuffer(toArrayBuffer(arquivo));
		fs.writeFileSync(baseFile, buffer, 'binary');

		// teste
		//var milis = new Date();
		//var baseFile = 'templates/tChamadas.xlsx';

		var file = 'detalhamentoChamadas_' + formataDataHoraMilis(milis) + '.xlsx';

		var newData;

		fs.readFile(baseFile, function (err, data) {
		  // Create a template
		  var t = new XlsxTemplate(data);

		  // Perform substitution
		  t.substitute(1, {
			filtros: $scope.filtros_usados,
			planDados: $scope.chamadas
		  });

		  // Get binary data
		  newData = t.generate();

		  if (!fs.existsSync(file)) {
			fs.writeFileSync(file, newData, 'binary');
			childProcess.exec(file);
		  }
		});

		setTimeout(function () {
		  $btn_exportar.button('reset');
		}, 500);
	  }, function (err, num_rows) {
		//?
	  });
	}
  };

  // Autenticação
  $scope.montaRotas = function () {
	var forms = [];

	var stmt = db.use + "SELECT DISTINCT Formulario FROM PermissoesUsuariosEstatura where LoginUsuario = '" + $scope.login + "' AND UPPER(Dominio) = '" + $scope.dominio + "'";
	log(stmt);
	db.query(stmt, function (columns) {
	  var form = columns[0].value;
	  forms.push(form);
	}, function (err, num_rows) {
	  if (num_rows > 0) {

		var text = fs.readFileSync(tempDir() + "infoCache", "utf-8");
		text = JSON.parse(text);
		text.cache = $scope.filtros.arq_cache;
		fs.writeFileSync(tempDir() + "infoCache", JSON.stringify(text));

		var bkpRotas = rotas;
		rotas = [];
		/*//chamadas
		rotas.push(bkpRotas[0]);*/
		$globals.logado = true;
		for (var i = 0; i < forms.length; i++) {
		  bkpRotas.forEach(function (rota) {

			if (rota.form === forms[i]) {
			  var indice = bkpRotas.indexOf(rota);
			  //rota.templateUrl = rota.templateUrl.replace(".html","2.html");
			  rotas.push(bkpRotas[indice]);
			}

			/* var htmlTemp = unique2(rotasInit.map(function(r){if(r.url.indexOf(rota.url)>=0){ return r.templateUrl }else{ return undefined} })).toString();
			if (htmlTemp !== undefined) {
			  rota.templateUrl = base + htmlTemp;
			}*/
		  });
		}

		// reordenação por ordem
		rotas.sort(function (primeiro, segundo) {
		  // Ordenar somente por ordem
		  return primeiro.ordem - segundo.ordem;
		});

		// cookie
		//fs.writeFileSync("cookie", $scope.login+";"+$scope.dominio);

		var partsPath = window.location.pathname.split("/");
		var part = partsPath[partsPath.length - 1];
		window.location.href = part + window.location.hash + '/';
	  }

	  $('.login form label').text("");
	  /*setTimeout(function () {
		if (err) {
		  retornaMsgErro(err);
		}
	  });*/

	  if (num_rows === 0) {
		retornaMsgErro("Sem permissão", true);
	  }
	});
	//return true;
  };



			 //Plugin jQuery
	jQuery.extend({
	highlight: function (node, re, nodeName, className) {
	  if (node.nodeType === 3) {
		var match = node.data.match(re);
		if (match) {
		  var highlight = document.createElement(nodeName || 'span');
		  highlight.className = className || 'highlight';
		  var wordNode = node.splitText(match.index);
		  wordNode.splitText(match[0].length);
		  var wordClone = wordNode.cloneNode(true);
		  highlight.appendChild(wordClone);
		  wordNode.parentNode.replaceChild(highlight, wordNode);
		  return 1; //skip added node in parent
		}
	  } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
		  !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
		  !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
		for (var i = 0; i < node.childNodes.length; i++) {
		  i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
		}
	  }
	  return 0;
	}
  });

	jQuery.fn.unhighlight = function (options) {
	  var settings = { className: 'highlight', element: 'span' };
	  jQuery.extend(settings, options);

	  return this.find(settings.element + "." + settings.className).each(function () {
		var parent = this.parentNode;
		parent.replaceChild(this.firstChild, this);
		parent.normalize();
	  }).end();
	};

	jQuery.fn.highlight = function (words, options) {
	  var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
	  jQuery.extend(settings, options);

	  if (words.constructor === String) {
		words = [words];
	  }
	  words = jQuery.grep(words, function(word, i){
		return word != '';
	  });
	  words = jQuery.map(words, function(word, i) {
		return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	  });
	  if (words.length == 0) { return this; };

	  var flag = settings.caseSensitive ? "" : "i";
	  var pattern = "(" + words.join("|") + ")";
	  if (settings.wordsOnly) {
		pattern = "\\b" + pattern + "\\b";
	  }
	  var re = new RegExp(pattern, flag);

	  return this.each(function () {
		jQuery.highlight(this, re, settings.element, settings.className);
	  });
	};

  //Fim do Plugin

	/* Função de busca */
	function searchAndHighlight(searchTerm, selector, resultElement) {
			if (searchTerm&&!(searchTerm===" ")) {
		  var selector = selector;
		  //Remove resultados antigos ou elementos marcados
					$('.highlighted').removeClass('highlighted');

					//Remove os resultados anteriores (matches)
					/*
		  $span = $('span.match');
					$span.replaceWith($span.html());
		  */

		if(selector==""||!selector){
		  if( $("div.log-formatado").css('display') == 'block') {
			selector = "div.log-formatado";
			console.log("Div log formatado ativa");
		  }
		  if( $("div.log-original").css('display') == 'block') {
			selector = "div.log-original";
			console.log("Div log original ativa");
		  }
		  if( $("div.log-prerouting").css('display') == 'block') {
			selector = "div.log-prerouting";
			console.log("Div log prerouting ativa");
		  }
		  if( $("div.log-preroutingret").css('display') == 'block') {
			selector = "div.log-preroutingret";
			console.log("Div log preroutingret ativa");
		  }
		}

		console.log("BEFORE BUGGING BEFORE BUGGING Selector: "+selector);
		//searchTerm = searchTerm.trim();
		
		$(selector).highlight(searchTerm, {className: 'match'});
				var matches = $('.match');
		//console.log("Matches: "+matches.length+" searchTerm "+searchTerm);
		// Funções dependentes de resultados a seguir
				if (matches != null && matches.length > 0) {

		  try{
			resultElement.text(matches.length+' encontrados'); // Conta a quantidade de elementos encontrados
		  }catch(er){
			console.log("Erro na função de pesquisa "+er);
		  }

		  // Marca o primeiro elemento MATCH como highlighted ao efetuar uma pesquisa
					$('.match:first').addClass('highlighted');

					var i = 0;

		  // Função responsável por "scrollar" e marcar o próximo elemento match como highlighted
		  $('.searchtool_next').on('click', function () {

						i++;

						if (i >= $('.match').length) i = 0;

			  //Remove o atual e marca o próximo elemento match
						$('.match').removeClass('highlighted');
						$('.match').eq(i).addClass('highlighted');

			// Cálculo final até a posição do elemento marcado highlighted
			var container = $('.modal-body'), scrollTo = $('.match').eq(i);
			var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

			container.scrollTop(
			  wheretogo-125
			);
			$('.highlighted').focus();

			// Fim do cálculo e scroll
					});

			// Função para "Scrollar" e "Marcar" o anterior
		  $('.searchtool_prev').on('click', function () {

						i--;
						if (i < 0) i = $('.match').length - 1;
			  //Remove o atual e marca o elemento anterior
						$('.match').removeClass('highlighted');
						$('.match').eq(i).addClass('highlighted');
			  //Cálculo final da posição para o scroll
			var container = $('.modal-body'), scrollTo = $('.match').eq(i);
			var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

			container.scrollTop(
			  wheretogo-125
			);

			$('.highlighted').focus();

		  });


					if ($('.highlighted:first').length) { //if match found, scroll to where the first one appears
						$(window).scrollTop($('.highlighted:first').position().top);
					}
					return true;
				}
			}
			return false;
		}

	// Função responsável por checar atualizações no INPUT text
	$('.searchtool_text').each(function() {

	  // ResultElement é o elemento onde a quantidade de resultados é exibida
	  var resultElement = $('.search_results');
	  var searchElement = $(this);

	  search();
	  // Save current value of element
	  $(this).data('oldVal', $(this));

	  // Look for changes in the value
	  $(this).bind("propertychange keyup input paste", function(event){

		  // If value has changed...
		   if ($(this).data('oldVal') != $(this).val()) {
		  // Updated stored value
		  $(this).data('oldVal', $(this).val());

			  $("div .log-original").unhighlight({className: 'match'});
			  $("div .log-formatado").unhighlight({className: 'match'});
			  $("div .log-prerouting").unhighlight({className: 'match'});
			  $("div .log-preroutingret").unhighlight({className: 'match'});
		  if (!searchAndHighlight($(this).val(), "", resultElement)) {
			  resultElement.html('Sem resultados.');
		  }
		  }
	  });
		}).delay(300);

	function clearSearchTool(searchElement, resultElement){
		
	  // Função responsável por resetar a função de pesquisa
	  //console.log("Limpou a ferramenta de pesquisa de logs");
	  
	  if(resultElement==""||resultElement==null){
		  resultElement = $('.search_results');
	  }
	  $(resultElement).html('');
	  $(searchElement).val('');
	  $(searchElement).attr("placeholder", "Buscar...");
	  $("div .log-original").unhighlight({className: 'match'});
	  $("div .log-formatado").unhighlight({className: 'match'});
	  $("div .log-prerouting").unhighlight({className: 'match'});
	  $("div .log-preroutingret").unhighlight({className: 'match'});
	};

	$(".changeSearchLog").click(function(){
		var resultElement = $('.search_results');
	  clearSearchTool("", resultElement);
	  console.log("Chamou a função para limpar");
	});

	$("#modal-log").on('hidden', function () {
	  var resultElement = $('.search_results');
	  var searchElement = $('.searchtool_text');
	  clearSearchTool(searchElement, resultElement);
	  console.log("Chamou a função para limpar");
	});

	/*Fim da função de busca*/

  //{ EDs Input Controle
  var modificaAlertaEDs = function (x) {
	if (x=="Elemento repetido ou número máximo de EDs acumulados") {
	  $('.notification .ng-binding').text(x).selectpicker('refresh');
	  setTimeout(function () {
		$('.notification .ng-binding').text("").selectpicker('refresh');
	  }, 3000);
	} else {
	  $("#filtro-ed").attr("placeholder", x);
	}
  };
  var verificaRepetidoEDs = function (x, item) {
	console.log(x);
	if (x.length==3) return true;
	for (var z in x) {
	  try {
		if (x[z].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	  try {
		if (x[z+1].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	}
	return false;
  };
  //var $myTextarea = $('#demo');
  //$myTextarea.css("background-color","grey").css("color","white");
  var arr2 = [];
  $('#filtro-ed').typeahead({
	source: $scope.options5opt,
	minLength: 3,
	items: 20,
	updater: function (item) {
	  var temp;
	  if (arr2.length < 1) {
		temp = item.split(" ");
		arr2.push(temp[0]);
		$scope.filtroEDSelect = arr2;
		console.log($scope.filtroEDSelect);
		modificaAlertaEDs(arr2[0]);
		return '';
	  } else {
		temp = item.split(" ");
		if (verificaRepetidoEDs(arr2, temp[0])) {
		  modificaAlertaEDs("Elemento repetido ou número máximo de EDs acumulados");
		  setTimeout(function () {
			if (arr2.length==3) { modificaAlertaEDs(arr2[0] +", "+ arr2[1] + ", " + arr2[2]); }
			if (arr2.length==2) { modificaAlertaEDs(arr2[0] +", "+ arr2[1]); }
			if (arr2.length==1) { modificaAlertaEDs(arr2[0]) ; }
		  }, 3000);
		} else {
		  temp = item.split(" ");
		  arr2.push(temp[0]);
		  //$myTextarea.css("color","white");
		  if (arr2.length==3) { modificaAlertaEDs(arr2[0] +", "+ arr2[1] + ", " + arr2[2]); }
		  if (arr2.length==2) { modificaAlertaEDs(arr2[0] +", "+ arr2[1]); }
		  if (arr2.length==1) { modificaAlertaEDs(arr2[0]) ; }
		  $scope.filtroEDSelect = arr2;
		  console.log($scope.filtroEDSelect);
		}
		//verificaRepetido($myTextarea,item)? $('.notification .ng-binding').text("Elemento repetido ou máximo de EDs alcançados...").selectpicker('refresh'); : $myTextarea.append(',',item);
		return '';
	  }
	  $scope.filtroEDSelect = arr2;
	  console.log($scope.filtroEDSelect);
	}
  });
  //} Fim EDs Input Controle

  //{ ICs Input Controle
  var modificaAlertaICs = function (x) {
	if (x=="Elemento repetido ou número máximo de ICs acumulados") {
	  $('.notification .ng-binding').text(x).selectpicker('refresh');
	  setTimeout(function () {
		$('.notification .ng-binding').text("").selectpicker('refresh');
	  }, 3000);
	} else {
	  $("#filtro-ic").attr("placeholder", x);
	}
  };
  var verificaRepetidoICs = function (x, item) {
	console.log(x);
	if (x.length==3) return true;
	for (var z in x) {
	  try {
		if (x[z].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	  try {
		if (x[z+1].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	}
	return false;
  };
  //var $myTextarea = $('#demo');
  //$myTextarea.css("background-color","grey").css("color","white");
  var vetorFiltroIc = [];
  $('#filtro-ic').typeahead({
	source: $scope.options5opt,
	minLength: 3,
	items: 20,
	updater: function (item) {
	  var temp;
	  if (vetorFiltroIc.length < 1) {
		temp = item.split(" ");
		vetorFiltroIc.push(temp[0]);
		$scope.filtroICSelect = vetorFiltroIc;
		console.log($scope.filtroICSelect);
		modificaAlertaICs(vetorFiltroIc[0]);
		return '';
	  } else {
		temp = item.split(" ");
		if (verificaRepetidoICs(vetorFiltroIc, temp[0])) {
		  modificaAlertaICs("Elemento repetido ou número máximo de ICs acumulados");
		  setTimeout(function () {
			if (vetorFiltroIc.length==3) { modificaAlertaICs(vetorFiltroIc[0] +", "+ vetorFiltroIc[1] + ", " + vetorFiltroIc[2]); }
			if (vetorFiltroIc.length==2) { modificaAlertaICs(vetorFiltroIc[0] +", "+ vetorFiltroIc[1]); }
			if (vetorFiltroIc.length==1) { modificaAlertaICs(vetorFiltroIc[0]) ; }
		  }, 3000);
		} else {
		  temp = item.split(" ");
		  vetorFiltroIc.push(temp[0]);
		  // $myTextarea.css("color","white");
		  if (vetorFiltroIc.length==3) { modificaAlertaICs(vetorFiltroIc[0] +", "+ vetorFiltroIc[1] + ", " + vetorFiltroIc[2]); }
		  if (vetorFiltroIc.length==2) { modificaAlertaICs(vetorFiltroIc[0] +", "+ vetorFiltroIc[1]); }
		  if (vetorFiltroIc.length==1) { modificaAlertaICs(vetorFiltroIc[0]) ; }
		  $scope.filtroICSelect = vetorFiltroIc;
		  console.log($scope.filtroICSelect);
		}
		//verificaRepetido($myTextarea,item)? $('.notification .ng-binding').text("Elemento repetido ou máximo de ICs alcançados...").selectpicker('refresh'); : $myTextarea.append(',',item);
		return '';
	  }
	  $scope.filtroICSelect = vetorFiltroIc;
	  console.log($scope.filtroICSelect);
	}
  });
  
	function search(selector){
		var resultElement = $('.search_results');
		var searchElement = $(".searchtool_text");
		clearSearchTool("", resultElement);
		
		console.log("Search!!!!!!");
		
		if(selector==""||!selector){
		  if( $("div.log-formatado").css('display') == 'block') {
			selector = "div.log-formatado";
			console.log("Div log formatado ativa");
		  }
		  if( $("div.log-original").css('display') == 'block') {
			selector = "div.log-original";
			console.log("Div log original ativa");
		  }
		  if( $("div.log-prerouting").css('display') == 'block') {
			selector = "div.log-prerouting";
			console.log("Div log prerouting ativa");
		  }
		  if( $("div.log-preroutingret").css('display') == 'block') {
			selector = "div.log-preroutingret";
			console.log("Div log preroutingret ativa");
		  }
		}
		
		if(searchElement.val()!=""&&searchElement.val()!=undefined&&searchElement.val()!=null){
			//console.log("Função SEARCH realizando pesquisa por: "+searchElement.val()+" em "+selector);
			try{
				/*if (!searchAndHighlight(searchElement, selector, resultElement)) {
					console.log("Não Encontrados");
				}*/
				
				
				$(selector).highlight(searchElement.val(), {className: 'match'});
				$('.match:first').addClass('highlighted');
				var matches = $(".match");
				resultElement.text(matches.length+' encontrados');
				
			} catch(err){
				console.log("Error: "+err);
			}
		}
	}
  
	/*setInterval(function(){
		search();
	}, 1000);*/
  //} Fim ICs Input Controle
  $scope.$watch('callLog', function(newValue, oldValue) {
	  if(newValue){
		  console.log("NewValue: "+newValue.length);
	  }
	  setTimeout(function(){
					console.log("Preparando Log Original...");
					search(".log-original");
		}, 1000);
		console.log("Realizou a pesquisa devido ao CallLog :"+$(".searchtool_text").val());
	});	
	
	
	$scope.$watch('ocultar.log_formatado', function(newValue, oldValue) {
		console.log("Ocultar LOG FORMATADO?: "+$scope.ocultar.log_formatado);
		console.log("Old Value: "+oldValue+" new value: "+newValue);	
		if(newValue==false){
			search(".log-formatado");
			console.log("Realizou a pesquisa em Log Formatado!");
		}
	});	
		
	$scope.$watch('ocultar.log_original', function(newValue, oldValue) {
			console.log("Ocultar LOG ORIGINAL?: "+$scope.ocultar.log_original);
			console.log("Old Value: "+oldValue+" new value: "+newValue);
			if(newValue==false){
				search(".log-original");
				console.log("Realizou a pesquisa em LOG ORIGINAL por :"+$(".searchtool_text").val());
			}
	});
		
	$scope.$watch('ocultar.log_prerouting', function(newValue, oldValue) {
		console.log("Ocultar LOG prerouting?: "+$scope.ocultar.log_prerouting);
		console.log("Old Value: "+oldValue+" new value: "+newValue);
		setTimeout(function(){
					console.log("Preparando Log prerouting...");
					search(".log-prerouting");
		}, 1000);		
	});
	
	
	$scope.$watch('ocultar.log_preroutingret', function(newValue, oldValue) {
		console.log("Ocultar LOG preroutingret?: "+$scope.ocultar.log_preroutingret);
		console.log("Old Value: "+oldValue+" new value: "+newValue);
		setTimeout(function(){
					console.log("Preparando Log preroutingret...");
					search(".log-preroutingret");
		}, 1000);
	});
	
	
	
  
  $scope.abreAjuda = function (link){
	abreAjuda(link);
  }
}
CtrlChamadas.$inject = ['$scope', '$globals'];
