function CtrlResumoEmpresaDestino($scope, $globals) {

	win.title="Resumo pré-roteamento"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
  $scope.pormes = {};

	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-resumo-por-empresa-destino", "Resumo pré-roteamento");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;

	$scope.dados = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;

	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	//$scope.ordenacao = 'data_hora';
	//$scope.decrescente = true;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		// ALEX - 29/01/2013
		sites: [],
		segmentos: [],
		isHFiltroED: false,
		periodo: "hora",
		porDDD: false
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;

	$scope.porDDD = null;

	//02/06/2014 Flag
	$scope.porRegEDDD = $globals.agrupar;

	$scope.aba = 2;
	var distinctEmpresas = [];


	//var empresas = cache.empresas.map(function(a){return a.codigo});

	$scope.valor = "empresa";



	function geraColunas(){

	  var array = [];
	  var selectEmpresas = distinctEmpresas;

	  if($scope.valor !== "regra"){
	  array.push(
		 { field: "datReferencia", displayName: "Data", width: 150, pinned: true }
	   );
	  }


	  if($scope.valor === "empresa"){
		  
		  
		  for(var i= 0; i<selectEmpresas.length;i++){
			  array.push(
				  { field: "qtd_"+selectEmpresas[i]+"", displayName: selectEmpresas[i], width: 105, category: ""+obtemNomeEmpresa(selectEmpresas[i]), cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{(row.getProperty(col.field) || 0) | number}}</span></div>' }
			  );
		  }
		  

	  

	  }else if($scope.valor === "destino"){
		array.push({ field: "codskill", displayName: "Skill", width: 300, pinned: true });

		if($scope.filtros.porDDD){
		  array.push({ field: "ddd", displayName: "DDD", width: 100, pinned: true });
		}

		array.push({ field: "qtd", displayName: "qtd", width: 100, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' });

	  }else if($scope.valor === "regra"){

		array.push(
		//{ field: "cod_ic", displayName: "Código", width: 80, pinned: true },
		{ field: "nome_ic", displayName: "Regra", width: 145, pinned: true }
		);

	  }

	  return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

		$scope.periodo = {
			inicio:inicio,
			fim: fim,
			min: new Date(2013, 11, 1),
			max: agora // FIXME: atualizar ao virar o dia
			/*min: ontem(dat_consoli),
			max: dat_consoli*/
		};

	var $view;


	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-resumo-por-empresa-destino");


	  //19/03/2014
		componenteDataHora ($scope,$view);

		carregaRegioes($view);
		carregaAplicacoes($view,false,false,$scope);
		carregaSites($view);
		carregaEmpresas($view,true);
		carregaRegras($view,true);
		carregaSkills($view,true);
	    carregaDDDs($view,true);
		

		carregaSegmentosPorAplicacao($scope,$view,true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


	  
	  // Popula lista de  DDDs a partir das aplicações selecionadas
	  //$view.on("change", "select.filtro-aplicacao", function(){ carregaDDDsPorAplicacao($scope,$view)});


	  $view.on("change","input:radio[name=radioB]",function() {

		if($(this).val() === "empresa"){
		  $('#porDDD').attr('disabled', 'disabled');
		  $('#porDDD').prop('checked',false);
		}else{
		  $('#porDDD').attr('disabled', false);
		}

	  });

		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-ddd", function () {
			var filtro_ddds = $(this).val() || [];
			Estalo.filtros.filtro_ddds = filtro_ddds;
		});

		$view.on("change", "select.filtro-regiao", function () {
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});

		//GILBERTOO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});

	  
		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
		  $scope.porRegEDDD = false;
		  $scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
			$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
			componenteDataHora ($scope,$view,true);

			var partsPath = window.location.pathname.split("/");
			var part  = partsPath[partsPath.length-1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function(){
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			},500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$scope.agora = function () {
			iniciaAgora($view,$scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
		abortar($scope, "#pag-resumo-por-empresa-destino");
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

		  $scope.pivot = false;
		  limpaProgressBar($scope, "#pag-resumo-por-empresa-destino");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini,data_fim);
			if(testedata!==""){
				setTimeout(function(){
					atualizaInfo($scope,testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				},500);
				return;
			}



			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});




		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
	  $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {
		distinctEmpresas = [];
		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportarCSV");
		$btn_exportar.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ed = $view.find("select.filtro-ed").val() || [];
		var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
		var filtro_regras = $view.find("select.filtro-regra").val() || [];
		var filtro_skills = $view.find("select.filtro-skill").val() || [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
		+ " até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
		if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
		if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
		if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }
		if (filtro_regras.length > 0) { $scope.filtros_usados += " Regras: " + filtro_regras; }

		if (filtro_skills.length > 0) {
		  var filtro_skills_mod = [];
		  for(var i = 0; i < filtro_skills.length; i++){
				var lsRegExp = /%/g;
				filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp,'&lt;'));
		  }
		  $scope.filtros_usados += " Skills: " + filtro_skills_mod;
		}

		if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
		  var options = [];
		  var v = [];
		  filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
		  unique(v).forEach((function (filtro_ddds) {
				return function (codigo) {
					var ddd = cache.ddds.indice[codigo];
					filtro_ddds.push(codigo);
				};
		  })(filtro_ddds));
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		}else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		  $scope.filtros_usados += " DDDs: " + filtro_ddds;
		}

		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";
		$scope.datas = [];
		$scope.totais = { geral: { qtd_entrantes: 0 } };

		//console.log($scope.valor)
	    //console.log('yasuto - '+filtro_sites)

	  if($scope.valor === "empresa"){
			stmt = db.use
			+ " SELECT "+($scope.filtros.periodo == "dia" ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""
			+ ", CodEmpresa"
			+ ", SUM(qtdchamadas) as qtd"
			+ " FROM ResumoDDDxAtendHumano"
			+ " WHERE 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
			+ " AND CodEmpresa <> ''"
			stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
			stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
			stmt += restringe_consulta("CodRegra", filtro_regras, true)
			stmt += restringe_consulta("ddd", filtro_ddds, true)
			stmt += restringe_consulta_like2("CodSkill", filtro_skills)
			stmt += " GROUP BY "+($scope.filtros.periodo == "dia" ? "convert(date,DatReferencia)" : "DatReferencia")+", CodEmpresa"
			stmt += " ORDER BY "+($scope.filtros.periodo == "dia" ? "convert(date,DatReferencia)" : "DatReferencia")+", CodEmpresa";

	  }else if($scope.valor === "destino"){
			stmt = db.use
			+ " SELECT "+($scope.filtros.periodo == "dia" ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""
			+ ", CodSkill, SUM(qtdchamadas) as qtd"+($scope.filtros.porDDD ? " ,ddd" : "")+""
			+ " FROM ResumoDDDxAtendHumano"
			+ " WHERE 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("CodSite", filtro_sites, true)
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
			stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
			stmt += restringe_consulta("CodRegra", filtro_regras, true)
			stmt += restringe_consulta("ddd", filtro_ddds, true)
			stmt += restringe_consulta_like2("CodSkill", filtro_skills)
			stmt += " GROUP BY "+($scope.filtros.periodo == "dia" ? "convert(date,DatReferencia)" : "DatReferencia")+", CodSkill"+($scope.filtros.porDDD ? " ,DDD" : "")+"";
	  }else if($scope.valor === "regra"){
			stmt = db.use
			+ " SELECT "+($scope.filtros.periodo == "dia" || $scope.filtros.periodo == "mes" ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""
			+ ", CodRegra, SUM(qtdchamadas) as qtd"
			//"+($scope.filtros.porDDD ? " ,ddd" : "")+"
			+ " FROM ResumoDDDxAtendHumano"
			+ " WHERE 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("CodSite", filtro_sites, true)
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
			stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
			stmt += restringe_consulta("CodRegra", filtro_regras, true)
			stmt += restringe_consulta("ddd", filtro_ddds, true)
			stmt += restringe_consulta_like2("CodSkill", filtro_skills)
			stmt += " GROUP BY "+($scope.filtros.periodo == "dia" || $scope.filtros.periodo == "mes" ? "convert(date,DatReferencia)" : "DatReferencia")+", CodRegra";
			stmt += " ORDER BY CodRegra";
			//"+($scope.filtros.porDDD ? " ,DDD" : "")+"
	  }
    
		executaQuery = executaQuery2;
		log(stmt);

		function formataData2(data) {
			var y = data.getFullYear(),
			  m = data.getMonth() + 1,
			  d = data.getDate();
			return y + _02d(m) + _02d(d);
		}

		function formataDataAnoMes(data) {
			var y = data.getFullYear(),
				m = data.getMonth()+1;
				return y + _02d(m)
		}

		function executaQuery2(columns){

      if($scope.valor !== "regra"){

            var data_hora = new Date(columns["DatReferencia"].value.length === 10 ? columns["DatReferencia"].value + " 00:00:00" : columns["DatReferencia"].value),
              datReferencia = $scope.filtros.periodo == "dia" ? formataDataBR(data_hora) : formataDataHoraBR(data_hora);
      }

		  if($scope.valor === "empresa"){
			  var data_hora = new Date(columns["DatReferencia"].value.length === 10 ? columns["DatReferencia"].value + " 00:00:00" : columns["DatReferencia"].value),datReferencia = $scope.filtros.periodo == "dia" ? formataDataBR(data_hora) : formataDataHoraBR(data_hora);
			  
			  var empresa = (columns["CodEmpresa"].value).toUpperCase(), qtd = columns["qtd"].value;
			  
			  if(distinctEmpresas.indexOf(empresa)< 0 && (filtro_empresas.indexOf(empresa)>=0 || filtro_empresas.length === 0)) distinctEmpresas.push(empresa);
			  
			  var obj = { data_hora: data_hora, datReferencia: datReferencia };
			  obj["qtd_"+empresa] = qtd;
			  $scope.dados.push(obj);

		  }else if($scope.valor === "destino"){
			var codskill = columns["CodSkill"].value,
				ddd = columns["ddd"] !== undefined ? columns["ddd"].value : undefined,
				qtd = columns["qtd"].value;

			$scope.dados.push({
			  data_hora: data_hora,
			  datReferencia: datReferencia,
			  codskill: codskill,
			  ddd: ddd,
			  qtd: qtd
			});
      console.log($scope.dados.data_hora);

		  }else if($scope.valor === "regra"){
			if ($.isNumeric(columns[1].value) == false) {

if($scope.filtros.periodo == "dia"){

				var cod_ic = columns[1].value,
			  data = columns[0].value === null ? null : "_" + formataData2(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value)),
			  data_BR = columns[0].value === null ? null : formataDataBR(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value)),
			  qtd_entrantes = +columns[2].value;
			  //console.log(columns)

}
else if($scope.filtros.periodo == "mes"){
    var cod_ic = columns[1].value,
    data = columns[0].value === null ? null : "_" + formataDataAnoMes(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value)),
    data_BR = columns[0].value === null ? null : formataDataBR(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value), "mes"),
    qtd_entrantes =+ columns[2].value;
}
else{
		
    var cod_ic = columns[1].value,
          data = columns[0].value === null ? null : "_" + formataDataHora2(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value)),
          data_BR = columns[0].value === null ? null : formataDataHoraBR3(new Date(columns[0].value.length === 10 ? columns[0].value + " 00:00:00" : columns[0].value)),
          qtd_entrantes = +columns[2].value;
    }

				if (data != null) {
					var d = $scope.datas[data];
					if (d === undefined) {
						d = { data: data, data_BR: data_BR };
						$scope.datas.push(d);
						$scope.datas[data] = d;
						$scope.totais[data] = { qtd_entrantes: 0 };
					}
				}

				var dado = $scope.dados[cod_ic];
				if (dado === undefined) {
					dado = {
						cod_ic: cod_ic,
						nome_ic: cod_ic,
						totais: { geral: { qtd_entrantes: 0 } }
					};
					$scope.dados.push(dado);
					$scope.dados[cod_ic] = dado;
				}

				if (data != null) {
          
          //console.log("Formato DATA: "+data);
          
          if($scope.filtros.periodo == "mes"){
              if(cod_ic in $scope.dados){
                 $scope.dados[cod_ic].totais[data] = $scope.dados[cod_ic].totais[data] == undefined ? { qtd_entrantes: qtd_entrantes } : { qtd_entrantes: $scope.dados[cod_ic].totais[data].qtd_entrantes + qtd_entrantes};
                 $scope.dados[cod_ic].totais["geral"] = $scope.dados[cod_ic].totais["geral"] == undefined ? { qtd_entrantes: qtd_entrantes } : { qtd_entrantes: $scope.dados[cod_ic].totais["geral"].qtd_entrantes + qtd_entrantes};
             }else{
                 $scope.dados = dado;
                 $scope.dados[cod_ic].totais[data] = { qtd_entrantes: qtd_entrantes };
                 $scope.dados[cod_ic].totais["geral"].qtd_entrantes += qtd_entrantes;
             }
             //TESTANDO
                  /*$scope.dados[data].qtd_entrantes += qtd_entrantes;
                  $scope.dados.geral.qtd_entrantes += qtd_entrantes;*/
          }else{      
            dado.totais[data] = { qtd_entrantes: qtd_entrantes }; 
            dado.totais.geral.qtd_entrantes += qtd_entrantes;          
          }
          

					$scope.totais[data].qtd_entrantes += qtd_entrantes;
					$scope.totais.geral.qtd_entrantes += qtd_entrantes;
				}
			}


		  }


			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
		   if($scope.dados.length%1000===0){
				$scope.$apply();
			  }
		}


	db.query(stmt,executaQuery, function (err, num_rows) {
	    userLog(stmt, 'Carrega dados', 2, err)
					
					var complemento = "";
				
				if($scope.valor === "empresa") complemento +=" EMPRESA";
				if($scope.valor === "regra") complemento +=" REGRA";
				
					

         $scope.$apply();


					if($scope.valor === "empresa"){
						var objetoAgrupado = _.groupBy($scope.dados,'datReferencia');
						dadosTemp = [];
						for (m in objetoAgrupado){
							var item = {};
							for (var i = 0; i < objetoAgrupado[m].length; i++) {
								jQuery.extend(item, objetoAgrupado[m][i]);
							}
							dadosTemp.push(item);
							item = {};
						}
						$scope.dados = dadosTemp;
						$scope.colunas = geraColunas();


					}


				  if($scope.valor === "regra"){
					// Preencher com zero datas inexistentes em cada erro
			$scope.dados.forEach(function (dado) {

				$scope.datas.forEach(function (d) {
					if (dado.totais[d.data] === undefined) {
						dado.totais[d.data] = { qtd_entrantes: 0 };
					}
					// total = total + dado.totais[d.data]
				})				
			});

			


			$scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
			$scope.datas.forEach(function (d) {
				$scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: $scope.filtros.periodo == "dia" ? 100 : 88 });
				//console.log("PROCURANDO PELO D.DATA "+d.data +" d.data_BR "+d.data_BR);
			});

			$scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: $scope.filtros.periodo == "dia" ? 100 : 88 });
			$scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });

			
		}
		
		$scope.dados.forEach(function (dado) {
			$scope.csv.push($scope.colunas.filterMap(function (col) {
				return dado[col.field] ? dado[col.field] : "0";
			}));
		});


					retornaStatusQuery(num_rows, $scope);
					$($('.ngHeaderContainer')[0]).css('height','30px');
					$btn_gerar.button('reset');

				  if(num_rows>0){
					$btn_exportar.prop("disabled", false);
				  }



          userLog(stmt, "Consulta "+complemento, 2, err);

				});
			//});

			// GILBERTOOOOOO 17/03/2014
			$view.on("mouseup", "tr.resumo", function () {
				var that = $(this);
				$('tr.resumo.marcado').toggleClass('marcado');
				$scope.$apply(function () {
					that.toggleClass('marcado');
				});
			});
	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

	  var linhas  = [];


	  if($scope.valor === "regra"){



		// Criar cabeçalho
		var cabecalho = $scope.datas.map(function (d) {
			return { value: d.data_BR, bold: 1, hAlign: 'center', autoWidth: true };
		});

		// Inserir primeira coluna: item de controle
		cabecalho.unshift({ value: "Regra", bold: 1, hAlign: 'center', autoWidth: true });

		// Inserir última coluna: total por linha
		cabecalho.push({ value: "Total", bold: 1, hAlign: 'center', autoWidth: true });

		var linhas = $scope.dados.map(function (dado) {	
			var linha = $scope.datas.map(function (d) {
				try {
										
					return { value: dado.totais[d.data].qtd_entrantes || 0 };
				} catch (ex) {
					return 0;
				}
			});

			// Inserir primeira coluna: item de controle
			//linha.unshift({ value: dado.cod_ic + " - " + dado.nome_ic });
			linha.unshift({ value: dado.cod_ic});

			// Inserir última coluna: total por linha
			linha.push({ value: dado.totais.geral.qtd_entrantes });

			return linha;
		});

		// Criar última linha, com os totais por data
		var linha_totais = $scope.datas.map(function (d) {
			try {
				return { value: $scope.totais[d.data].qtd_entrantes || 0 };
			} catch (ex) {
				return 0;
			}
		});

		// Inserir primeira coluna
		linha_totais.unshift({ value: "Total", bold: 1 });

		// Inserir última coluna: total geral
		linha_totais.push({ value: $scope.totais.geral.qtd_entrantes });

		// Inserir primeira linha: cabeçalho
		linhas.unshift(cabecalho);

		// Inserir última linha: totais por data
		linhas.push(linha_totais);



	  }else{

		var header = distinctEmpresas;


		var h =[];
		  
		  
		  h.push({value: "Data" , autoWidth:true});

		for(var i= 0; i<header.length;i++){

		  h.push({value: header[i] , autoWidth:true});
		}
		linhas.push(h);


		$scope.dados.map(function (dado) {
		  var l = [];
			 l.push({value:dado.datReferencia, autoWidth:true});
		  for(var i= 0; i<header.length;i++){
			l.push({value:dado["qtd_"+header[i]], autoWidth:true});
		  }

		  return linhas.push(l);
		});


	  }





		var planilha = {
			creator: "Estalo",
			lastModifiedBy: $scope.username || "Estalo",
			worksheets: [{ name: 'Resumo Chamadas Pré-roteamento', data: linhas, table: true }],
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: { first: 1 }
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');


		var milis = new Date();
		var file = 'resumoEmpresaDestino_' + formataDataHoraMilis(milis) + '.xlsx';


		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, planilha.base64, 'base64');
			childProcess.exec(file);
		}

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);

	  empresas = cache.empresas.map(function(a){return a.codigo});
	  
	  var complemento = "";	
              if($scope.valor === "empresa") complemento +=" EMPRESA";
			  if($scope.valor === "regra") complemento +=" REGRA";	  
			  userLog("", "Exportação "+complemento, 5, "");

	};

}

CtrlResumoEmpresaDestino.$inject = ['$scope', '$globals'];
