/*
* CtrlReceitaNEmN
*/
function CtrlReceitaNEmN($scope, $globals) {

    win.title="Receita N em N min"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoVS"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-receita-N-em-N", "Receita N em N min");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
    $scope.filtros = {
      isReceitaTR :false
    };

    $scope.iit = false;

    //$scope.limite_registros = 3000;

    $scope.tempData = [];


    $scope.vendas = [];
    $scope.vendasTR = [];


    $scope.colunas = [];
    $scope.gridDados = {
      data: "vendas",
      rowTemplate: '<div ng-repeat="col in renderedColumns" ng-class="{\'erro\':row.getProperty(\'parcial\') == \'false\' }" class="ngCell {{col.cellClass}} {{col.colIndex()}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
      columnDefs: "colunas",
      enableColumnResize: true,
      enablePinning: true
    };

    $scope.headerRowHeight = 60;

    $scope.colunasTR = [];
    $scope.gridDadosTR = {
      headerRowHeight: $scope.headerRowHeight,
      headerRowTemplate: '<div class="ngRow ngHeaderText" ng-style="{height: headerRowHeight / 2}" style="text-align:center">Aplicação de 5 em 5 min</div><div ng-style="{ height: col.headerRowHeight / 2, top: col.headerRowHeight / 2 }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngHeaderCell"><div class="ngVerticalBar" ng-style="{height: col.headerRowHeight / 2}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-header-cell></div></div>',
      data: "vendasTR",
      columnDefs: "colunasTR",
      enableColumnResize: true,
      enablePinning: true
    };

    $scope.log = [];


    //filtros_usados
    $scope.filtros_usados = "";


    $scope.aba = 2;




    function geraColunas(){
      var array = [];


        array.push({ field: "datReferencia", displayName: "Intervalo", width: 150, pinned: false },
                   { field: "aplicacao", displayName: "Aplicação", width: 140, pinned: false },
                   { field: "qtdEntrantes", displayName: "Entrantes", width: 110, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "qtdDerivadas", displayName: "Derivadas", width: 110, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "qtdFinalizadas", displayName: "Finalizadas", width: 110, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "qtdAbandonadas", displayName: "Abandonadas", width: 120, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "receita", displayName: "Receita", width: 110, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' }

                );
      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = agora;

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: mesAnterior(dat_consoli),
            max: dat_consoli*/
        };

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-receita-N-em-N");
      treeView('#pag-receita-N-em-N #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-receita-N-em-N #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-receita-N-em-N #ed', 65,'ED','dvED');
      treeView('#pag-receita-N-em-N #ic', 95,'IC','dvIC');
      treeView('#pag-receita-N-em-N #tid', 115,'TID','dvTid');
      treeView('#pag-receita-N-em-N #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-receita-N-em-N #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-receita-N-em-N #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-receita-N-em-N #parametros', 225,'Parametros','dvParam');
      treeView('#pag-receita-N-em-N #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-receita-N-em-N #monitoracao', 275, 'Monitoração', 'dvReparo');
         treeView('#pag-receita-N-em-N #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		 treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');


            //Habilitar filtro tempo real
            $view.on("change", "#HReceitaTR", function () {
              if($scope.filtros.isReceitaTR === false){
                console.log("ativar");
                iniciaReceitaTR (0);
              }else{
                $scope.vendasTR = [];
                $('#chart_container').html("");
              }
            });

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
      $('.nav.aba2').css('margin-top','-6px');
      $('.filtro-intervalo').val(5);




       //19/03/2014
      componenteDataHora ($scope,$view);

      carregaAplicacoes($view,false,true,$scope);




      //2014-11-27 transição de abas
      var abas = [2];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }

      //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope,true)});



        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-ddd').removeClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });




        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function(){
                    atualizaInfo($scope,'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }

            limpaProgressBar($scope, "#pag-receita-N-em-N");
            //22/03/2014 Testa se data início é maior que a data fim

            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaVendas.apply(this);
        });

        $view.on("click", ".btn-baixarT", function () {
          limpaProgressBar($scope, "#pag-receita-N-em-N");
          $scope.consultaConsolidadoT.apply(this);
        });

        $view.on("click", ".btn-baixarB", function () {
          limpaProgressBar($scope, "#pag-receita-N-em-N");
          $scope.consultaConsolidadoB.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-receita-N-em-N");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });



/*
if(formataDataHora(new Date()).substring(15,16) > "5"){
  console.log(formataDataHora(new Date()).replace(formataDataHora(new Date()).substring(15,19),"5:00"));
}else if(formataDataHora(new Date()).substring(15,16) < "5"){
  console.log(formataDataHora(new Date()).replace(formataDataHora(new Date()).substring(15,19),"0:00"));
}else{
  console.log(formataDataHora(new Date()).replace(formataDataHora(new Date()).substring(15,19),"5:00"));
}
*/



function iniciaReceitaTR(t){

  var vendasTR = setTimeout(function(){

    if(win.title !== "Receita N em N min"){ clearTimeout(vendasTR); return; }

    if($scope.filtros.isReceitaTR === false){ clearTimeout(vendasTR); return; }

    console.log("Recebendo dados...");

    var aplHeader = null;
    if($('select.filtro-aplicacao').val() === null){
      aplHeader = cache.aplsVenda[Math.floor(Math.random()*cache.aplsVenda.length)];
    }else{

      console.log($('select.filtro-aplicacao').val().length);
      aplHeader = $('select.filtro-aplicacao').val()[Math.floor(Math.random() * $('select.filtro-aplicacao').val().length)];
    }

    var stmtRT = db.use+" with ra as (SELECT dateadd(minute, datediff(minute,0,DataHora) / 5 * 5, 0) as hora,codaplicacao as apl,sum(QtdChamadas) as c,sum(QtdFinalizadas)as f,sum(QtdAbandonadas)as a FROM ResumoAtendimentoURAMin as ra where DataHora BETWEEN '"+formataDataHora(minutosAnteriores(minutoAnteriorPrecisaoFim(new Date()),19))+"' AND '"+formataDataHora(new Date())+"' AND CodAplicacao IN ('"+aplHeader+"') and RA.TipoServidor ='A' group by dateadd(minute, datediff(minute,0,DataHora) / 5 * 5, 0),CodAplicacao ), vup as ( select dateadd(minute, datediff(minute,0,DataHora) / 5 * 5, 0) as h, sum(Valor) as receita from VendasURAPromo where CodAplicacao IN ('"+aplHeader+"') and DataHora BETWEEN '"+formataDataHora(minutosAnteriores(minutoAnteriorPrecisaoFim(new Date()),19))+"' AND '"+formataDataHora(new Date())+"' group by dateadd(minute, datediff(minute,0,DataHora) / 5 * 5, 0) ) select ra.hora, ra.apl,ra.c,ra.f,ra.a,vup.receita from ra left outer join vup on ra.hora = vup.h order by ra.hora ASC";

    executaThreadBasico('extratorDiaADia',stringParaExtrator(stmtRT,[]), function(dado){console.log(dado)},$scope);

    $scope.vendasTR = [];

    for (var i = 0; i< $scope.tempData.length-6; i+=6){
      //console.log($scope.tempData.slice(i,i+6));
      $scope.vendasTR.push({
        data_hora: new Date($scope.tempData[i]),
        datReferencia: formataDataHoraBR(new Date($scope.tempData[i])),
        aplicacao: $scope.tempData[i+1],
        qtdChamadas: +$scope.tempData[i+2],
        qtdFinalizadas: +$scope.tempData[i+3],
        qtdAbandonadas: +$scope.tempData[i+4],
        receita: (+$scope.tempData[i+5]).toFixed(2)
      });
    }

    aplHeader = cache.aplsVenda[Math.floor(Math.random()*cache.aplsVenda.length)];
    $scope.colunasTR = geraColunas();
    $('#grid2').css('width','740px');
    $scope.$apply();
    iniciaReceitaTR (10000);
    if((loginUsuario === "Versatil"  && dominioUsuario === "RJ2013785436-VS") || (loginUsuario === "Fernanda"  && dominioUsuario === "VERSÁTIL") || (loginUsuario === "FlavioSa"  && dominioUsuario === "VERSATILP")){
    $scope.geraGrafico();
    }

  }, t);
}

iniciaReceitaTR (0);



  $scope.geraGrafico = function(){
    $('#chart_container #y_axis').html("");
    $('#chart_container #chart').html("");
    $('#chart_container #legend').html("");
    if($scope.vendasTR.length > 0){

      var dados = [ [] ];




      $scope.vendasTR.forEach(function(d){

        dados[0].push({x: Math.round(d.data_hora/1000), y: parseInt(d.receita)});

      });


      var palette = new Rickshaw.Color.Palette();

      var array  = [
          {
            name: "Receita", data: dados[0], color: palette.color()
          }
        ];


      var graph = new Rickshaw.Graph( {
        element: document.querySelector("#chart"),
        width: Math.round(window.innerWidth / 10 * 3),
        height: Math.round(window.innerHeight / 10 * 3),
        renderer: 'line',
        series: array
      });

      var x_axis = new Rickshaw.Graph.Axis.Time( { graph: graph } );

      var y_axis = new Rickshaw.Graph.Axis.Y( {
        graph: graph,
        orientation: 'left',
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('y_axis'),
      } );

      var legend = new Rickshaw.Graph.Legend( {
        element: document.querySelector('#legend'),
        graph: graph
      } );
      graph.render();

      var hoverDetail = new Rickshaw.Graph.HoverDetail( {
        graph: graph,
        formatter: function(series, x, y) {
          var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
          var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
          var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
          return content;
        }
      });
    }
  };






    // Lista vendas conforme filtros
    $scope.listaVendas = function () {

        $globals.numeroDeRegistros = 0;

        $scope.filtros_usados = "";
        //if (!connection) {
        //  db.connect(config, $scope.listaVendas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_intervalo = $view.find("select.filtro-intervalo").val() || [];

      //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_intervalo.length > 0) { $scope.filtros_usados += " Intervalo: " + filtro_intervalo; }

        $scope.vendas = [];

        var stmt = db.use + "with ra as ("
        + " SELECT dateadd(minute, datediff(minute,0,DataHora) / "+filtro_intervalo+" * "+filtro_intervalo+", 0) as hora,codaplicacao as apl,"
        + " sum(QtdChamadas) as c, sum(QtdDerivadas)as d, sum(QtdFinalizadas)as f, sum(QtdAbandonadas)as a"
        + " FROM " + db.prefixo + "ResumoAtendimentoURAMin as ra"
        + " where DataHora BETWEEN '" + formataDataHora(data_ini) + "' AND '" + formataDataHora(data_fim) + "'";
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += " and RA.TipoServidor ='A'"
        + " group by dateadd(minute, datediff(minute,0,DataHora) / "+filtro_intervalo+" * "+filtro_intervalo+", 0),CodAplicacao"
        + " ), vup as ("
        + " select dateadd(minute, datediff(minute,0,DataHora) / "+filtro_intervalo+" * "+filtro_intervalo+", 0) as h, sum(Valor) as receita, CodAplicacao"
        + " from " + db.prefixo + "VendasURAPromo";
        stmt += " where DataHora BETWEEN '" + formataDataHora(data_ini) + "' AND '" + formataDataHora(data_fim) + "'"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        + " AND status IN('0','20')"
        + " group by dateadd(minute, datediff(minute,0,DataHora) / "+filtro_intervalo+" * "+filtro_intervalo+", 0), CodAplicacao"
        + " )"
        + " select ra.hora, ra.apl,ra.c,ra.d,ra.f,ra.a,vup.receita"
        + " from ra left outer join vup"
        + " on ra.hora = vup.h"
        + " AND RA.apl = vup.CodAplicacao"
        + " order by ra.hora";


        log(stmt);



        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas (columns) {
                $globals.numeroDeRegistros = columns[0].value;
        }



        function executaQuery(columns){

          var data_hora = columns[0].value,
              datReferencia = typeof columns[0].value==='string' ? formataDataHoraBRString(data_hora) :formataDataHoraBR(data_hora),
              aplicacao = columns["apl"].value,
              qtdDerivadas = +columns["d"].value,
              qtdFinalizadas = +columns["f"].value,
              qtdAbandonadas = +columns["a"].value,
              qtdEntrantes = $scope.iit === false ? qtdFinalizadas + qtdDerivadas + qtdAbandonadas : +columns["c"].value,
              valor = columns["receita"].value === null ? "NA" : +columns["receita"].value.toFixed(2);



                    $scope.vendas.push({
                      data_hora: data_hora,
                      datReferencia: datReferencia,
                      aplicacao: aplicacao,
                      qtdEntrantes: qtdEntrantes,
                      qtdDerivadas: qtdDerivadas,
                      qtdFinalizadas: qtdFinalizadas,
                      qtdAbandonadas: qtdAbandonadas,
                      receita: valor,
                      parcial: "true"

                    });
                //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length,$scope,"#pag-resumo-vendas");
                //console.log($scope.vendas.length+" "+$globals.numeroDeRegistros);
              if($scope.vendas.length % 1000===0){
                $scope.$apply();
              }

        }

      $scope.total_valores = 0;


        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
            //console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);
            db.query(stmt,executaQuery, function (err, num_rows) {
            console.log("Executando query-> "+stmt+" "+num_rows);
            userLog(stmt, 'Carrega dados', 2, err)
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
              /*$scope.vendas.push({
                data_hora: "",
                datReferencia: "",
                aplicacao: "",
                qtdChamadas: "",
                qtdFinalizadas: "",
                qtdAbandonadas: "TOTAL",
                receita: $scope.total_valores
              });*/

              if($scope.vendas.length > 0){
                if(new Date(new Date($scope.vendas[$scope.vendas.length-1].data_hora).getTime() + filtro_intervalo * 60000) > new Date()){
                  $scope.vendas[$scope.vendas.length-1].parcial = 'false';
                }
              }


            if(num_rows>0){
              $btn_exportar.prop("disabled", false);
            }
            });
        //});

         // GILBERTOOOOOO 17/03/2014



            $view.on("mouseup", "tr.resumo", function () {
                var that = $(this);
                $('tr.resumo.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });

    };



    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='tReceitaNEmN'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = 'tReceitaNEmN.xlsx';



    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'receitaNEmN_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                 filtros: $scope.filtros_usados,
                  planDados: $scope.vendas
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)
            //?
          });

    };
    /*$scope.celula = function (cod_status) {
        return (['success', 'danger', 'warning'])[cod_status];
    };*/
}
CtrlReceitaNEmN.$inject = ['$scope', '$globals'];
