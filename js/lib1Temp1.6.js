
//{ jQuery Library 1.9.1
/*!
 * jQuery JavaScript Library v1.9.1
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2012 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2013-2-4
 */
(function( window, undefined ) {

// Can't do this because several apps including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
// Support: Firefox 18+
//"use strict";
var
  // The deferred used on DOM ready
  readyList,

  // A central reference to the root jQuery(document)
  rootjQuery,

  // Support: IE<9
  // For `typeof node.method` instead of `node.method !== undefined`
  core_strundefined = typeof undefined,

  // Use the correct document accordingly with window argument (sandbox)
  document = window.document,
  location = window.location,

  // Map over jQuery in case of overwrite
  _jQuery = window.jQuery,

  // Map over the $ in case of overwrite
  _$ = window.$,

  // [[Class]] -> type pairs
  class2type = {},

  // List of deleted data cache ids, so we can reuse them
  core_deletedIds = [],

  core_version = "1.9.1",

  // Save a reference to some core methods
  core_concat = core_deletedIds.concat,
  core_push = core_deletedIds.push,
  core_slice = core_deletedIds.slice,
  core_indexOf = core_deletedIds.indexOf,
  core_toString = class2type.toString,
  core_hasOwn = class2type.hasOwnProperty,
  core_trim = core_version.trim,

  // Define a local copy of jQuery
  jQuery = function( selector, context ) {
	// The jQuery object is actually just the init constructor 'enhanced'
	return new jQuery.fn.init( selector, context, rootjQuery );
  },

  // Used for matching numbers
  core_pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,

  // Used for splitting on whitespace
  core_rnotwhite = /\S+/g,

  // Make sure we trim BOM and NBSP (here's looking at you, Safari 5.0 and IE)
  rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

  // A simple way to check for HTML strings
  // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
  // Strict HTML recognition (#11290: must start with <)
  rquickExpr = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,

  // Match a standalone tag
  rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,

  // JSON RegExp
  rvalidchars = /^[\],:{}\s]*$/,
  rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g,
  rvalidescape = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
  rvalidtokens = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,

  // Matches dashed string for camelizing
  rmsPrefix = /^-ms-/,
  rdashAlpha = /-([\da-z])/gi,

  // Used by jQuery.camelCase as callback to replace()
  fcamelCase = function( all, letter ) {
	return letter.toUpperCase();
  },

  // The ready event handler
  completed = function( event ) {

	// readyState === "complete" is good enough for us to call the dom ready in oldIE
	if ( document.addEventListener || event.type === "load" || document.readyState === "complete" ) {
	  detach();
	  jQuery.ready();
	}
  },
  // Clean-up method for dom ready events
  detach = function() {
	if ( document.addEventListener ) {
	  document.removeEventListener( "DOMContentLoaded", completed, false );
	  window.removeEventListener( "load", completed, false );

	} else {
	  document.detachEvent( "onreadystatechange", completed );
	  window.detachEvent( "onload", completed );
	}
  };

jQuery.fn = jQuery.prototype = {
  // The current version of jQuery being used
  jquery: core_version,

  constructor: jQuery,
  init: function( selector, context, rootjQuery ) {
	var match, elem;

	// HANDLE: $(""), $(null), $(undefined), $(false)
	if ( !selector ) {
	  return this;
	}

	// Handle HTML strings
	if ( typeof selector === "string" ) {
	  if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
		// Assume that strings that start and end with <> are HTML and skip the regex check
		match = [ null, selector, null ];

	  } else {
		match = rquickExpr.exec( selector );
	  }

	  // Match html or make sure no context is specified for #id
	  if ( match && (match[1] || !context) ) {

		// HANDLE: $(html) -> $(array)
		if ( match[1] ) {
		  context = context instanceof jQuery ? context[0] : context;

		  // scripts is true for back-compat
		  jQuery.merge( this, jQuery.parseHTML(
			match[1],
			context && context.nodeType ? context.ownerDocument || context : document,
			true
		  ) );

		  // HANDLE: $(html, props)
		  if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
			for ( match in context ) {
			  // Properties of context are called as methods if possible
			  if ( jQuery.isFunction( this[ match ] ) ) {
				this[ match ]( context[ match ] );

			  // ...and otherwise set as attributes
			  } else {
				this.attr( match, context[ match ] );
			  }
			}
		  }

		  return this;

		// HANDLE: $(#id)
		} else {
		  elem = document.getElementById( match[2] );

		  // Check parentNode to catch when Blackberry 4.6 returns
		  // nodes that are no longer in the document #6963
		  if ( elem && elem.parentNode ) {
			// Handle the case where IE and Opera return items
			// by name instead of ID
			if ( elem.id !== match[2] ) {
			  return rootjQuery.find( selector );
			}

			// Otherwise, we inject the element directly into the jQuery object
			this.length = 1;
			this[0] = elem;
		  }

		  this.context = document;
		  this.selector = selector;
		  return this;
		}

	  // HANDLE: $(expr, $(...))
	  } else if ( !context || context.jquery ) {
		return ( context || rootjQuery ).find( selector );

	  // HANDLE: $(expr, context)
	  // (which is just equivalent to: $(context).find(expr)
	  } else {
		return this.constructor( context ).find( selector );
	  }

	// HANDLE: $(DOMElement)
	} else if ( selector.nodeType ) {
	  this.context = this[0] = selector;
	  this.length = 1;
	  return this;

	// HANDLE: $(function)
	// Shortcut for document ready
	} else if ( jQuery.isFunction( selector ) ) {
	  return rootjQuery.ready( selector );
	}

	if ( selector.selector !== undefined ) {
	  this.selector = selector.selector;
	  this.context = selector.context;
	}

	return jQuery.makeArray( selector, this );
  },

  // Start with an empty selector
  selector: "",

  // The default length of a jQuery object is 0
  length: 0,

  // The number of elements contained in the matched element set
  size: function() {
	return this.length;
  },

  toArray: function() {
	return core_slice.call( this );
  },

  // Get the Nth element in the matched element set OR
  // Get the whole matched element set as a clean array
  get: function( num ) {
	return num == null ?

	  // Return a 'clean' array
	  this.toArray() :

	  // Return just the object
	  ( num < 0 ? this[ this.length + num ] : this[ num ] );
  },

  // Take an array of elements and push it onto the stack
  // (returning the new matched element set)
  pushStack: function( elems ) {

	// Build a new jQuery matched element set
	var ret = jQuery.merge( this.constructor(), elems );

	// Add the old object onto the stack (as a reference)
	ret.prevObject = this;
	ret.context = this.context;

	// Return the newly-formed element set
	return ret;
  },

  // Execute a callback for every element in the matched set.
  // (You can seed the arguments with an array of args, but this is
  // only used internally.)
  each: function( callback, args ) {
	return jQuery.each( this, callback, args );
  },

  ready: function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
  },

  slice: function() {
	return this.pushStack( core_slice.apply( this, arguments ) );
  },

  first: function() {
	return this.eq( 0 );
  },

  last: function() {
	return this.eq( -1 );
  },

  eq: function( i ) {
	var len = this.length,
	  j = +i + ( i < 0 ? len : 0 );
	return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
  },

  map: function( callback ) {
	return this.pushStack( jQuery.map(this, function( elem, i ) {
	  return callback.call( elem, i, elem );
	}));
  },

  end: function() {
	return this.prevObject || this.constructor(null);
  },

  // For internal use only.
  // Behaves like an Array's method, not like a jQuery method.
  push: core_push,
  sort: [].sort,
  splice: [].splice
};

// Give the init function the jQuery prototype for later instantiation
jQuery.fn.init.prototype = jQuery.fn;

jQuery.extend = jQuery.fn.extend = function() {
  var src, copyIsArray, copy, name, options, clone,
	target = arguments[0] || {},
	i = 1,
	length = arguments.length,
	deep = false;

  // Handle a deep copy situation
  if ( typeof target === "boolean" ) {
	deep = target;
	target = arguments[1] || {};
	// skip the boolean and the target
	i = 2;
  }

  // Handle case when target is a string or something (possible in deep copy)
  if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
	target = {};
  }

  // extend jQuery itself if only one argument is passed
  if ( length === i ) {
	target = this;
	--i;
  }

  for ( ; i < length; i++ ) {
	// Only deal with non-null/undefined values
	if ( (options = arguments[ i ]) != null ) {
	  // Extend the base object
	  for ( name in options ) {
		src = target[ name ];
		copy = options[ name ];

		// Prevent never-ending loop
		if ( target === copy ) {
		  continue;
		}

		// Recurse if we're merging plain objects or arrays
		if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
		  if ( copyIsArray ) {
			copyIsArray = false;
			clone = src && jQuery.isArray(src) ? src : [];

		  } else {
			clone = src && jQuery.isPlainObject(src) ? src : {};
		  }

		  // Never move original objects, clone them
		  target[ name ] = jQuery.extend( deep, clone, copy );

		// Don't bring in undefined values
		} else if ( copy !== undefined ) {
		  target[ name ] = copy;
		}
	  }
	}
  }

  // Return the modified object
  return target;
};

jQuery.extend({
  noConflict: function( deep ) {
	if ( window.$ === jQuery ) {
	  window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
	  window.jQuery = _jQuery;
	}

	return jQuery;
  },

  // Is the DOM ready to be used? Set to true once it occurs.
  isReady: false,

  // A counter to track how many items to wait for before
  // the ready event fires. See #6781
  readyWait: 1,

  // Hold (or release) the ready event
  holdReady: function( hold ) {
	if ( hold ) {
	  jQuery.readyWait++;
	} else {
	  jQuery.ready( true );
	}
  },

  // Handle when the DOM is ready
  ready: function( wait ) {

	// Abort if there are pending holds or we're already ready
	if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
	  return;
	}

	// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
	if ( !document.body ) {
	  return setTimeout( jQuery.ready );
	}

	// Remember that the DOM is ready
	jQuery.isReady = true;

	// If a normal DOM Ready event fired, decrement, and wait if need be
	if ( wait !== true && --jQuery.readyWait > 0 ) {
	  return;
	}

	// If there are functions bound, to execute
	readyList.resolveWith( document, [ jQuery ] );

	// Trigger any bound ready events
	if ( jQuery.fn.trigger ) {
	  jQuery( document ).trigger("ready").off("ready");
	}
  },

  // See test/unit/core.js for details concerning isFunction.
  // Since version 1.3, DOM methods and functions like alert
  // aren't supported. They return false on IE (#2968).
  isFunction: function( obj ) {
	return jQuery.type(obj) === "function";
  },

  isArray: Array.isArray || function( obj ) {
	return jQuery.type(obj) === "array";
  },

  isWindow: function( obj ) {
	return obj != null && obj == obj.window;
  },

  isNumeric: function( obj ) {
	return !isNaN( parseFloat(obj) ) && isFinite( obj );
  },

  type: function( obj ) {
	if ( obj == null ) {
	  return String( obj );
	}
	return typeof obj === "object" || typeof obj === "function" ?
	  class2type[ core_toString.call(obj) ] || "object" :
	  typeof obj;
  },

  isPlainObject: function( obj ) {
	// Must be an Object.
	// Because of IE, we also have to check the presence of the constructor property.
	// Make sure that DOM nodes and window objects don't pass through, as well
	if ( !obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
	  return false;
	}

	try {
	  // Not own constructor property must be Object
	  if ( obj.constructor &&
		!core_hasOwn.call(obj, "constructor") &&
		!core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
		return false;
	  }
	} catch ( e ) {
	  // IE8,9 Will throw exceptions on certain host objects #9897
	  return false;
	}

	// Own properties are enumerated firstly, so to speed up,
	// if last one is own, then all properties are own.

	var key;
	for ( key in obj ) {}

	return key === undefined || core_hasOwn.call( obj, key );
  },

  isEmptyObject: function( obj ) {
	var name;
	for ( name in obj ) {
	  return false;
	}
	return true;
  },

  error: function( msg ) {
	throw new Error( msg );
  },

  // data: string of html
  // context (optional): If specified, the fragment will be created in this context, defaults to document
  // keepScripts (optional): If true, will include scripts passed in the html string
  parseHTML: function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
	  return null;
	}
	if ( typeof context === "boolean" ) {
	  keepScripts = context;
	  context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
	  scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
	  return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );
	if ( scripts ) {
	  jQuery( scripts ).remove();
	}
	return jQuery.merge( [], parsed.childNodes );
  },

  parseJSON: function( data ) {
	// Attempt to parse using the native JSON parser first
	if ( window.JSON && window.JSON.parse ) {
	  return window.JSON.parse( data );
	}

	if ( data === null ) {
	  return data;
	}

	if ( typeof data === "string" ) {

	  // Make sure leading/trailing whitespace is removed (IE can't handle it)
	  data = jQuery.trim( data );

	  if ( data ) {
		// Make sure the incoming data is actual JSON
		// Logic borrowed from http://json.org/json2.js
		if ( rvalidchars.test( data.replace( rvalidescape, "@" )
		  .replace( rvalidtokens, "]" )
		  .replace( rvalidbraces, "")) ) {

		  return ( new Function( "return " + data ) )();
		}
	  }
	}

	jQuery.error( "Invalid JSON: " + data );
  },

  // Cross-browser xml parsing
  parseXML: function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
	  return null;
	}
	try {
	  if ( window.DOMParser ) { // Standard
		tmp = new DOMParser();
		xml = tmp.parseFromString( data , "text/xml" );
	  } else { // IE
		xml = new ActiveXObject( "Microsoft.XMLDOM" );
		xml.async = "false";
		xml.loadXML( data );
	  }
	} catch( e ) {
	  xml = undefined;
	}
	if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
	  jQuery.error( "Invalid XML: " + data );
	}
	return xml;
  },

  noop: function() {},

  // Evaluates a script in a global context
  // Workarounds based on findings by Jim Driscoll
  // http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
  globalEval: function( data ) {
	if ( data && jQuery.trim( data ) ) {
	  // We use execScript on Internet Explorer
	  // We use an anonymous function so that context is window
	  // rather than jQuery in Firefox
	  ( window.execScript || function( data ) {
		window[ "eval" ].call( window, data );
	  } )( data );
	}
  },

  // Convert dashed to camelCase; used by the css and data modules
  // Microsoft forgot to hump their vendor prefix (#9572)
  camelCase: function( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
  },

  nodeName: function( elem, name ) {
	return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
  },

  // args is for internal usage only
  each: function( obj, callback, args ) {
	var value,
	  i = 0,
	  length = obj.length,
	  isArray = isArraylike( obj );

	if ( args ) {
	  if ( isArray ) {
		for ( ; i < length; i++ ) {
		  value = callback.apply( obj[ i ], args );

		  if ( value === false ) {
			break;
		  }
		}
	  } else {
		for ( i in obj ) {
		  value = callback.apply( obj[ i ], args );

		  if ( value === false ) {
			break;
		  }
		}
	  }

	// A special, fast, case for the most common use of each
	} else {
	  if ( isArray ) {
		for ( ; i < length; i++ ) {
		  value = callback.call( obj[ i ], i, obj[ i ] );

		  if ( value === false ) {
			break;
		  }
		}
	  } else {
		for ( i in obj ) {
		  value = callback.call( obj[ i ], i, obj[ i ] );

		  if ( value === false ) {
			break;
		  }
		}
	  }
	}

	return obj;
  },

  // Use native String.trim function wherever possible
  trim: core_trim && !core_trim.call("\uFEFF\xA0") ?
	function( text ) {
	  return text == null ?
		"" :
		core_trim.call( text );
	} :

	// Otherwise use our own trimming functionality
	function( text ) {
	  return text == null ?
		"" :
		( text + "" ).replace( rtrim, "" );
	},

  // results is for internal usage only
  makeArray: function( arr, results ) {
	var ret = results || [];

	if ( arr != null ) {
	  if ( isArraylike( Object(arr) ) ) {
		jQuery.merge( ret,
		  typeof arr === "string" ?
		  [ arr ] : arr
		);
	  } else {
		core_push.call( ret, arr );
	  }
	}

	return ret;
  },

  inArray: function( elem, arr, i ) {
	var len;

	if ( arr ) {
	  if ( core_indexOf ) {
		return core_indexOf.call( arr, elem, i );
	  }

	  len = arr.length;
	  i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;

	  for ( ; i < len; i++ ) {
		// Skip accessing in sparse arrays
		if ( i in arr && arr[ i ] === elem ) {
		  return i;
		}
	  }
	}

	return -1;
  },

  merge: function( first, second ) {
	var l = second.length,
	  i = first.length,
	  j = 0;

	if ( typeof l === "number" ) {
	  for ( ; j < l; j++ ) {
		first[ i++ ] = second[ j ];
	  }
	} else {
	  while ( second[j] !== undefined ) {
		first[ i++ ] = second[ j++ ];
	  }
	}

	first.length = i;

	return first;
  },

  grep: function( elems, callback, inv ) {
	var retVal,
	  ret = [],
	  i = 0,
	  length = elems.length;
	inv = !!inv;

	// Go through the array, only saving the items
	// that pass the validator function
	for ( ; i < length; i++ ) {
	  retVal = !!callback( elems[ i ], i );
	  if ( inv !== retVal ) {
		ret.push( elems[ i ] );
	  }
	}

	return ret;
  },

  // arg is for internal usage only
  map: function( elems, callback, arg ) {
	var value,
	  i = 0,
	  length = elems.length,
	  isArray = isArraylike( elems ),
	  ret = [];

	// Go through the array, translating each of the items to their
	if ( isArray ) {
	  for ( ; i < length; i++ ) {
		value = callback( elems[ i ], i, arg );

		if ( value != null ) {
		  ret[ ret.length ] = value;
		}
	  }

	// Go through every key on the object,
	} else {
	  for ( i in elems ) {
		value = callback( elems[ i ], i, arg );

		if ( value != null ) {
		  ret[ ret.length ] = value;
		}
	  }
	}

	// Flatten any nested arrays
	return core_concat.apply( [], ret );
  },

  // A global GUID counter for objects
  guid: 1,

  // Bind a function to a context, optionally partially applying any
  // arguments.
  proxy: function( fn, context ) {
	var args, proxy, tmp;

	if ( typeof context === "string" ) {
	  tmp = fn[ context ];
	  context = fn;
	  fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !jQuery.isFunction( fn ) ) {
	  return undefined;
	}

	// Simulated bind
	args = core_slice.call( arguments, 2 );
	proxy = function() {
	  return fn.apply( context || this, args.concat( core_slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
  },

  // Multifunctional method to get and set values of a collection
  // The value/s can optionally be executed if it's a function
  access: function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
	  length = elems.length,
	  bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
	  chainable = true;
	  for ( i in key ) {
		jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
	  }

	// Sets one value
	} else if ( value !== undefined ) {
	  chainable = true;

	  if ( !jQuery.isFunction( value ) ) {
		raw = true;
	  }

	  if ( bulk ) {
		// Bulk operations run against the entire set
		if ( raw ) {
		  fn.call( elems, value );
		  fn = null;

		// ...except when executing function values
		} else {
		  bulk = fn;
		  fn = function( elem, key, value ) {
			return bulk.call( jQuery( elem ), value );
		  };
		}
	  }

	  if ( fn ) {
		for ( ; i < length; i++ ) {
		  fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
		}
	  }
	}

	return chainable ?
	  elems :

	  // Gets
	  bulk ?
		fn.call( elems ) :
		length ? fn( elems[0], key ) : emptyGet;
  },

  now: function() {
	return ( new Date() ).getTime();
  }
});

jQuery.ready.promise = function( obj ) {
  if ( !readyList ) {

	readyList = jQuery.Deferred();

	// Catch cases where $(document).ready() is called after the browser event has already occurred.
	// we once tried to use readyState "interactive" here, but it caused issues like the one
	// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
	if ( document.readyState === "complete" ) {
	  // Handle it asynchronously to allow scripts the opportunity to delay ready
	  setTimeout( jQuery.ready );

	// Standards-based browsers support DOMContentLoaded
	} else if ( document.addEventListener ) {
	  // Use the handy event callback
	  document.addEventListener( "DOMContentLoaded", completed, false );

	  // A fallback to window.onload, that will always work
	  window.addEventListener( "load", completed, false );

	// If IE event model is used
	} else {
	  // Ensure firing before onload, maybe late but safe also for iframes
	  document.attachEvent( "onreadystatechange", completed );

	  // A fallback to window.onload, that will always work
	  window.attachEvent( "onload", completed );

	  // If IE and not a frame
	  // continually check to see if the document is ready
	  var top = false;

	  try {
		top = window.frameElement == null && document.documentElement;
	  } catch(e) {}

	  if ( top && top.doScroll ) {
		(function doScrollCheck() {
		  if ( !jQuery.isReady ) {

			try {
			  // Use the trick by Diego Perini
			  // http://javascript.nwbox.com/IEContentLoaded/
			  top.doScroll("left");
			} catch(e) {
			  return setTimeout( doScrollCheck, 50 );
			}

			// detach all dom ready events
			detach();

			// and execute any waiting functions
			jQuery.ready();
		  }
		})();
	  }
	}
  }
  return readyList.promise( obj );
};

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
  class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {
  var length = obj.length,
	type = jQuery.type( obj );

  if ( jQuery.isWindow( obj ) ) {
	return false;
  }

  if ( obj.nodeType === 1 && length ) {
	return true;
  }

  return type === "array" || type !== "function" &&
	( length === 0 ||
	typeof length === "number" && length > 0 && ( length - 1 ) in obj );
}

// All jQuery objects should point back to these
rootjQuery = jQuery(document);
// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
  var object = optionsCache[ options ] = {};
  jQuery.each( options.match( core_rnotwhite ) || [], function( _, flag ) {
	object[ flag ] = true;
  });
  return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

  // Convert options from String-formatted to Object-formatted if needed
  // (we check in cache first)
  options = typeof options === "string" ?
	( optionsCache[ options ] || createOptions( options ) ) :
	jQuery.extend( {}, options );

  var // Flag to know if list is currently firing
	firing,
	// Last fire value (for non-forgettable lists)
	memory,
	// Flag to know if list was already fired
	fired,
	// End of the loop when firing
	firingLength,
	// Index of currently firing callback (modified by remove if needed)
	firingIndex,
	// First callback to fire (used internally by add and fireWith)
	firingStart,
	// Actual callback list
	list = [],
	// Stack of fire calls for repeatable lists
	stack = !options.once && [],
	// Fire callbacks
	fire = function( data ) {
	  memory = options.memory && data;
	  fired = true;
	  firingIndex = firingStart || 0;
	  firingStart = 0;
	  firingLength = list.length;
	  firing = true;
	  for ( ; list && firingIndex < firingLength; firingIndex++ ) {
		if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
		  memory = false; // To prevent further calls using add
		  break;
		}
	  }
	  firing = false;
	  if ( list ) {
		if ( stack ) {
		  if ( stack.length ) {
			fire( stack.shift() );
		  }
		} else if ( memory ) {
		  list = [];
		} else {
		  self.disable();
		}
	  }
	},
	// Actual Callbacks object
	self = {
	  // Add a callback or a collection of callbacks to the list
	  add: function() {
		if ( list ) {
		  // First, we save the current length
		  var start = list.length;
		  (function add( args ) {
			jQuery.each( args, function( _, arg ) {
			  var type = jQuery.type( arg );
			  if ( type === "function" ) {
				if ( !options.unique || !self.has( arg ) ) {
				  list.push( arg );
				}
			  } else if ( arg && arg.length && type !== "string" ) {
				// Inspect recursively
				add( arg );
			  }
			});
		  })( arguments );
		  // Do we need to add the callbacks to the
		  // current firing batch?
		  if ( firing ) {
			firingLength = list.length;
		  // With memory, if we're not firing then
		  // we should call right away
		  } else if ( memory ) {
			firingStart = start;
			fire( memory );
		  }
		}
		return this;
	  },
	  // Remove a callback from the list
	  remove: function() {
		if ( list ) {
		  jQuery.each( arguments, function( _, arg ) {
			var index;
			while( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
			  list.splice( index, 1 );
			  // Handle firing indexes
			  if ( firing ) {
				if ( index <= firingLength ) {
				  firingLength--;
				}
				if ( index <= firingIndex ) {
				  firingIndex--;
				}
			  }
			}
		  });
		}
		return this;
	  },
	  // Check if a given callback is in the list.
	  // If no argument is given, return whether or not list has callbacks attached.
	  has: function( fn ) {
		return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
	  },
	  // Remove all callbacks from the list
	  empty: function() {
		list = [];
		return this;
	  },
	  // Have the list do nothing anymore
	  disable: function() {
		list = stack = memory = undefined;
		return this;
	  },
	  // Is it disabled?
	  disabled: function() {
		return !list;
	  },
	  // Lock the list in its current state
	  lock: function() {
		stack = undefined;
		if ( !memory ) {
		  self.disable();
		}
		return this;
	  },
	  // Is it locked?
	  locked: function() {
		return !stack;
	  },
	  // Call all callbacks with the given context and arguments
	  fireWith: function( context, args ) {
		args = args || [];
		args = [ context, args.slice ? args.slice() : args ];
		if ( list && ( !fired || stack ) ) {
		  if ( firing ) {
			stack.push( args );
		  } else {
			fire( args );
		  }
		}
		return this;
	  },
	  // Call all the callbacks with the given arguments
	  fire: function() {
		self.fireWith( this, arguments );
		return this;
	  },
	  // To know if the callbacks have already been called at least once
	  fired: function() {
		return !!fired;
	  }
	};

  return self;
};
jQuery.extend({

  Deferred: function( func ) {
	var tuples = [
		// action, add listener, listener list, final state
		[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
		[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
		[ "notify", "progress", jQuery.Callbacks("memory") ]
	  ],
	  state = "pending",
	  promise = {
		state: function() {
		  return state;
		},
		always: function() {
		  deferred.done( arguments ).fail( arguments );
		  return this;
		},
		then: function( /* fnDone, fnFail, fnProgress */ ) {
		  var fns = arguments;
		  return jQuery.Deferred(function( newDefer ) {
			jQuery.each( tuples, function( i, tuple ) {
			  var action = tuple[ 0 ],
				fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
			  // deferred[ done | fail | progress ] for forwarding actions to newDefer
			  deferred[ tuple[1] ](function() {
				var returned = fn && fn.apply( this, arguments );
				if ( returned && jQuery.isFunction( returned.promise ) ) {
				  returned.promise()
					.done( newDefer.resolve )
					.fail( newDefer.reject )
					.progress( newDefer.notify );
				} else {
				  newDefer[ action + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
				}
			  });
			});
			fns = null;
		  }).promise();
		},
		// Get a promise for this deferred
		// If obj is provided, the promise aspect is added to the object
		promise: function( obj ) {
		  return obj != null ? jQuery.extend( obj, promise ) : promise;
		}
	  },
	  deferred = {};

	// Keep pipe for back-compat
	promise.pipe = promise.then;

	// Add list-specific methods
	jQuery.each( tuples, function( i, tuple ) {
	  var list = tuple[ 2 ],
		stateString = tuple[ 3 ];

	  // promise[ done | fail | progress ] = list.add
	  promise[ tuple[1] ] = list.add;

	  // Handle state
	  if ( stateString ) {
		list.add(function() {
		  // state = [ resolved | rejected ]
		  state = stateString;

		// [ reject_list | resolve_list ].disable; progress_list.lock
		}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
	  }

	  // deferred[ resolve | reject | notify ]
	  deferred[ tuple[0] ] = function() {
		deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
		return this;
	  };
	  deferred[ tuple[0] + "With" ] = list.fireWith;
	});

	// Make the deferred a promise
	promise.promise( deferred );

	// Call given func if any
	if ( func ) {
	  func.call( deferred, deferred );
	}

	// All done!
	return deferred;
  },

  // Deferred helper
  when: function( subordinate /* , ..., subordinateN */ ) {
	var i = 0,
	  resolveValues = core_slice.call( arguments ),
	  length = resolveValues.length,

	  // the count of uncompleted subordinates
	  remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

	  // the master Deferred. If resolveValues consist of only a single Deferred, just use that.
	  deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

	  // Update function for both resolve and progress values
	  updateFunc = function( i, contexts, values ) {
		return function( value ) {
		  contexts[ i ] = this;
		  values[ i ] = arguments.length > 1 ? core_slice.call( arguments ) : value;
		  if( values === progressValues ) {
			deferred.notifyWith( contexts, values );
		  } else if ( !( --remaining ) ) {
			deferred.resolveWith( contexts, values );
		  }
		};
	  },

	  progressValues, progressContexts, resolveContexts;

	// add listeners to Deferred subordinates; treat others as resolved
	if ( length > 1 ) {
	  progressValues = new Array( length );
	  progressContexts = new Array( length );
	  resolveContexts = new Array( length );
	  for ( ; i < length; i++ ) {
		if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
		  resolveValues[ i ].promise()
			.done( updateFunc( i, resolveContexts, resolveValues ) )
			.fail( deferred.reject )
			.progress( updateFunc( i, progressContexts, progressValues ) );
		} else {
		  --remaining;
		}
	  }
	}

	// if we're not waiting on anything, resolve the master
	if ( !remaining ) {
	  deferred.resolveWith( resolveContexts, resolveValues );
	}

	return deferred.promise();
  }
});
jQuery.support = (function() {

  var support, all, a,
	input, select, fragment,
	opt, eventName, isSupported, i,
	div = document.createElement("div");

  // Setup
  div.setAttribute( "className", "t" );
  div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";

  // Support tests won't run in some limited or non-browser environments
  all = div.getElementsByTagName("*");
  a = div.getElementsByTagName("a")[ 0 ];
  if ( !all || !a || !all.length ) {
	return {};
  }

  // First batch of tests
  select = document.createElement("select");
  opt = select.appendChild( document.createElement("option") );
  input = div.getElementsByTagName("input")[ 0 ];

  a.style.cssText = "top:1px;float:left;opacity:.5";
  support = {
	// Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
	getSetAttribute: div.className !== "t",

	// IE strips leading whitespace when .innerHTML is used
	leadingWhitespace: div.firstChild.nodeType === 3,

	// Make sure that tbody elements aren't automatically inserted
	// IE will insert them into empty tables
	tbody: !div.getElementsByTagName("tbody").length,

	// Make sure that link elements get serialized correctly by innerHTML
	// This requires a wrapper element in IE
	htmlSerialize: !!div.getElementsByTagName("link").length,

	// Get the style information from getAttribute
	// (IE uses .cssText instead)
	style: /top/.test( a.getAttribute("style") ),

	// Make sure that URLs aren't manipulated
	// (IE normalizes it by default)
	hrefNormalized: a.getAttribute("href") === "/a",

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	opacity: /^0.5/.test( a.style.opacity ),

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	cssFloat: !!a.style.cssFloat,

	// Check the default checkbox/radio value ("" on WebKit; "on" elsewhere)
	checkOn: !!input.value,

	// Make sure that a selected-by-default option has a working selected property.
	// (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
	optSelected: opt.selected,

	// Tests for enctype support on a form (#6743)
	enctype: !!document.createElement("form").enctype,

	// Makes sure cloning an html5 element does not cause problems
	// Where outerHTML is undefined, this still works
	html5Clone: document.createElement("nav").cloneNode( true ).outerHTML !== "<:nav></:nav>",

	// jQuery.support.boxModel DEPRECATED in 1.8 since we don't support Quirks Mode
	boxModel: document.compatMode === "CSS1Compat",

	// Will be defined later
	deleteExpando: true,
	noCloneEvent: true,
	inlineBlockNeedsLayout: false,
	shrinkWrapBlocks: false,
	reliableMarginRight: true,
	boxSizingReliable: true,
	pixelPosition: false
  };

  // Make sure checked status is properly cloned
  input.checked = true;
  support.noCloneChecked = input.cloneNode( true ).checked;

  // Make sure that the options inside disabled selects aren't marked as disabled
  // (WebKit marks them as disabled)
  select.disabled = true;
  support.optDisabled = !opt.disabled;

  // Support: IE<9
  try {
	delete div.test;
  } catch( e ) {
	support.deleteExpando = false;
  }

  // Check if we can trust getAttribute("value")
  input = document.createElement("input");
  input.setAttribute( "value", "" );
  support.input = input.getAttribute( "value" ) === "";

  // Check if an input maintains its value after becoming a radio
  input.value = "t";
  input.setAttribute( "type", "radio" );
  support.radioValue = input.value === "t";

  // #11217 - WebKit loses check when the name is after the checked attribute
  input.setAttribute( "checked", "t" );
  input.setAttribute( "name", "t" );

  fragment = document.createDocumentFragment();
  fragment.appendChild( input );

  // Check if a disconnected checkbox will retain its checked
  // value of true after appended to the DOM (IE6/7)
  support.appendChecked = input.checked;

  // WebKit doesn't clone checked state correctly in fragments
  support.checkClone = fragment.cloneNode( true ).cloneNode( true ).lastChild.checked;

  // Support: IE<9
  // Opera does not clone events (and typeof div.attachEvent === undefined).
  // IE9-10 clones events bound via attachEvent, but they don't trigger with .click()
  if ( div.attachEvent ) {
	div.attachEvent( "onclick", function() {
	  support.noCloneEvent = false;
	});

	div.cloneNode( true ).click();
  }

  // Support: IE<9 (lack submit/change bubble), Firefox 17+ (lack focusin event)
  // Beware of CSP restrictions (https://developer.mozilla.org/en/Security/CSP), test/csp.php
  for ( i in { submit: true, change: true, focusin: true }) {
	div.setAttribute( eventName = "on" + i, "t" );

	support[ i + "Bubbles" ] = eventName in window || div.attributes[ eventName ].expando === false;
  }

  div.style.backgroundClip = "content-box";
  div.cloneNode( true ).style.backgroundClip = "";
  support.clearCloneStyle = div.style.backgroundClip === "content-box";

  // Run tests that need a body at doc ready
  jQuery(function() {
	var container, marginDiv, tds,
	  divReset = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
	  body = document.getElementsByTagName("body")[0];

	if ( !body ) {
	  // Return for frameset docs that don't have a body
	  return;
	}

	container = document.createElement("div");
	container.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";

	body.appendChild( container ).appendChild( div );

	// Support: IE8
	// Check if table cells still have offsetWidth/Height when they are set
	// to display:none and there are still other visible table cells in a
	// table row; if so, offsetWidth/Height are not reliable for use when
	// determining if an element has been hidden directly using
	// display:none (it is still safe to use offsets if a parent element is
	// hidden; don safety goggles and see bug #4512 for more information).
	div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
	tds = div.getElementsByTagName("td");
	tds[ 0 ].style.cssText = "padding:0;margin:0;border:0;display:none";
	isSupported = ( tds[ 0 ].offsetHeight === 0 );

	tds[ 0 ].style.display = "";
	tds[ 1 ].style.display = "none";

	// Support: IE8
	// Check if empty table cells still have offsetWidth/Height
	support.reliableHiddenOffsets = isSupported && ( tds[ 0 ].offsetHeight === 0 );

	// Check box-sizing and margin behavior
	div.innerHTML = "";
	div.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";
	support.boxSizing = ( div.offsetWidth === 4 );
	support.doesNotIncludeMarginInBodyOffset = ( body.offsetTop !== 1 );

	// Use window.getComputedStyle because jsdom on node.js will break without it.
	if ( window.getComputedStyle ) {
	  support.pixelPosition = ( window.getComputedStyle( div, null ) || {} ).top !== "1%";
	  support.boxSizingReliable = ( window.getComputedStyle( div, null ) || { width: "4px" } ).width === "4px";

	  // Check if div with explicit width and no margin-right incorrectly
	  // gets computed margin-right based on width of container. (#3333)
	  // Fails in WebKit before Feb 2011 nightlies
	  // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
	  marginDiv = div.appendChild( document.createElement("div") );
	  marginDiv.style.cssText = div.style.cssText = divReset;
	  marginDiv.style.marginRight = marginDiv.style.width = "0";
	  div.style.width = "1px";

	  support.reliableMarginRight =
		!parseFloat( ( window.getComputedStyle( marginDiv, null ) || {} ).marginRight );
	}

	if ( typeof div.style.zoom !== core_strundefined ) {
	  // Support: IE<8
	  // Check if natively block-level elements act like inline-block
	  // elements when setting their display to 'inline' and giving
	  // them layout
	  div.innerHTML = "";
	  div.style.cssText = divReset + "width:1px;padding:1px;display:inline;zoom:1";
	  support.inlineBlockNeedsLayout = ( div.offsetWidth === 3 );

	  // Support: IE6
	  // Check if elements with layout shrink-wrap their children
	  div.style.display = "block";
	  div.innerHTML = "<div></div>";
	  div.firstChild.style.width = "5px";
	  support.shrinkWrapBlocks = ( div.offsetWidth !== 3 );

	  if ( support.inlineBlockNeedsLayout ) {
		// Prevent IE 6 from affecting layout for positioned elements #11048
		// Prevent IE from shrinking the body in IE 7 mode #12869
		// Support: IE<8
		body.style.zoom = 1;
	  }
	}

	body.removeChild( container );

	// Null elements to avoid leaks in IE
	container = div = tds = marginDiv = null;
  });

  // Null elements to avoid leaks in IE
  all = select = fragment = opt = a = input = null;

  return support;
})();

var rbrace = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
  rmultiDash = /([A-Z])/g;

function internalData( elem, name, data, pvt /* Internal Use Only */ ){
  if ( !jQuery.acceptData( elem ) ) {
	return;
  }

  var thisCache, ret,
	internalKey = jQuery.expando,
	getByName = typeof name === "string",

	// We have to handle DOM nodes and JS objects differently because IE6-7
	// can't GC object references properly across the DOM-JS boundary
	isNode = elem.nodeType,

	// Only DOM nodes need the global jQuery cache; JS object data is
	// attached directly to the object so GC can occur automatically
	cache = isNode ? jQuery.cache : elem,

	// Only defining an ID for JS objects if its cache already exists allows
	// the code to shortcut on the same path as a DOM node with no cache
	id = isNode ? elem[ internalKey ] : elem[ internalKey ] && internalKey;

  // Avoid doing any more work than we need to when trying to get data on an
  // object that has no data at all
  if ( (!id || !cache[id] || (!pvt && !cache[id].data)) && getByName && data === undefined ) {
	return;
  }

  if ( !id ) {
	// Only DOM nodes need a new unique ID for each element since their data
	// ends up in the global cache
	if ( isNode ) {
	  elem[ internalKey ] = id = core_deletedIds.pop() || jQuery.guid++;
	} else {
	  id = internalKey;
	}
  }

  if ( !cache[ id ] ) {
	cache[ id ] = {};

	// Avoids exposing jQuery metadata on plain JS objects when the object
	// is serialized using JSON.stringify
	if ( !isNode ) {
	  cache[ id ].toJSON = jQuery.noop;
	}
  }

  // An object can be passed to jQuery.data instead of a key/value pair; this gets
  // shallow copied over onto the existing cache
  if ( typeof name === "object" || typeof name === "function" ) {
	if ( pvt ) {
	  cache[ id ] = jQuery.extend( cache[ id ], name );
	} else {
	  cache[ id ].data = jQuery.extend( cache[ id ].data, name );
	}
  }

  thisCache = cache[ id ];

  // jQuery data() is stored in a separate object inside the object's internal data
  // cache in order to avoid key collisions between internal data and user-defined
  // data.
  if ( !pvt ) {
	if ( !thisCache.data ) {
	  thisCache.data = {};
	}

	thisCache = thisCache.data;
  }

  if ( data !== undefined ) {
	thisCache[ jQuery.camelCase( name ) ] = data;
  }

  // Check for both converted-to-camel and non-converted data property names
  // If a data property was specified
  if ( getByName ) {

	// First Try to find as-is property data
	ret = thisCache[ name ];

	// Test for null|undefined property data
	if ( ret == null ) {

	  // Try to find the camelCased property
	  ret = thisCache[ jQuery.camelCase( name ) ];
	}
  } else {
	ret = thisCache;
  }

  return ret;
}

function internalRemoveData( elem, name, pvt ) {
  if ( !jQuery.acceptData( elem ) ) {
	return;
  }

  var i, l, thisCache,
	isNode = elem.nodeType,

	// See jQuery.data for more information
	cache = isNode ? jQuery.cache : elem,
	id = isNode ? elem[ jQuery.expando ] : jQuery.expando;

  // If there is already no cache entry for this object, there is no
  // purpose in continuing
  if ( !cache[ id ] ) {
	return;
  }

  if ( name ) {

	thisCache = pvt ? cache[ id ] : cache[ id ].data;

	if ( thisCache ) {

	  // Support array or space separated string names for data keys
	  if ( !jQuery.isArray( name ) ) {

		// try the string as a key before any manipulation
		if ( name in thisCache ) {
		  name = [ name ];
		} else {

		  // split the camel cased version by spaces unless a key with the spaces exists
		  name = jQuery.camelCase( name );
		  if ( name in thisCache ) {
			name = [ name ];
		  } else {
			name = name.split(" ");
		  }
		}
	  } else {
		// If "name" is an array of keys...
		// When data is initially created, via ("key", "val") signature,
		// keys will be converted to camelCase.
		// Since there is no way to tell _how_ a key was added, remove
		// both plain key and camelCase key. #12786
		// This will only penalize the array argument path.
		name = name.concat( jQuery.map( name, jQuery.camelCase ) );
	  }

	  for ( i = 0, l = name.length; i < l; i++ ) {
		delete thisCache[ name[i] ];
	  }

	  // If there is no data left in the cache, we want to continue
	  // and let the cache object itself get destroyed
	  if ( !( pvt ? isEmptyDataObject : jQuery.isEmptyObject )( thisCache ) ) {
		return;
	  }
	}
  }

  // See jQuery.data for more information
  if ( !pvt ) {
	delete cache[ id ].data;

	// Don't destroy the parent cache unless the internal data object
	// had been the only thing left in it
	if ( !isEmptyDataObject( cache[ id ] ) ) {
	  return;
	}
  }

  // Destroy the cache
  if ( isNode ) {
	jQuery.cleanData( [ elem ], true );

  // Use delete when supported for expandos or `cache` is not a window per isWindow (#10080)
  } else if ( jQuery.support.deleteExpando || cache != cache.window ) {
	delete cache[ id ];

  // When all else fails, null
  } else {
	cache[ id ] = null;
  }
}

jQuery.extend({
  cache: {},

  // Unique for each copy of jQuery on the page
  // Non-digits removed to match rinlinejQuery
  expando: "jQuery" + ( core_version + Math.random() ).replace( /\D/g, "" ),

  // The following elements throw uncatchable exceptions if you
  // attempt to add expando properties to them.
  noData: {
	"embed": true,
	// Ban all objects except for Flash (which handle expandos)
	"object": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
	"applet": true
  },

  hasData: function( elem ) {
	elem = elem.nodeType ? jQuery.cache[ elem[jQuery.expando] ] : elem[ jQuery.expando ];
	return !!elem && !isEmptyDataObject( elem );
  },

  data: function( elem, name, data ) {
	return internalData( elem, name, data );
  },

  removeData: function( elem, name ) {
	return internalRemoveData( elem, name );
  },

  // For internal use only.
  _data: function( elem, name, data ) {
	return internalData( elem, name, data, true );
  },

  _removeData: function( elem, name ) {
	return internalRemoveData( elem, name, true );
  },

  // A method for determining if a DOM node can handle the data expando
  acceptData: function( elem ) {
	// Do not set data on non-element because it will not be cleared (#8335).
	if ( elem.nodeType && elem.nodeType !== 1 && elem.nodeType !== 9 ) {
	  return false;
	}

	var noData = elem.nodeName && jQuery.noData[ elem.nodeName.toLowerCase() ];

	// nodes accept data unless otherwise specified; rejection can be conditional
	return !noData || noData !== true && elem.getAttribute("classid") === noData;
  }
});

jQuery.fn.extend({
  data: function( key, value ) {
	var attrs, name,
	  elem = this[0],
	  i = 0,
	  data = null;

	// Gets all values
	if ( key === undefined ) {
	  if ( this.length ) {
		data = jQuery.data( elem );

		if ( elem.nodeType === 1 && !jQuery._data( elem, "parsedAttrs" ) ) {
		  attrs = elem.attributes;
		  for ( ; i < attrs.length; i++ ) {
			name = attrs[i].name;

			if ( !name.indexOf( "data-" ) ) {
			  name = jQuery.camelCase( name.slice(5) );

			  dataAttr( elem, name, data[ name ] );
			}
		  }
		  jQuery._data( elem, "parsedAttrs", true );
		}
	  }

	  return data;
	}

	// Sets multiple values
	if ( typeof key === "object" ) {
	  return this.each(function() {
		jQuery.data( this, key );
	  });
	}

	return jQuery.access( this, function( value ) {

	  if ( value === undefined ) {
		// Try to fetch any internally stored data first
		return elem ? dataAttr( elem, key, jQuery.data( elem, key ) ) : null;
	  }

	  this.each(function() {
		jQuery.data( this, key, value );
	  });
	}, null, value, arguments.length > 1, null, true );
  },

  removeData: function( key ) {
	return this.each(function() {
	  jQuery.removeData( this, key );
	});
  }
});

function dataAttr( elem, key, data ) {
  // If nothing was found internally, try to fetch any
  // data from the HTML5 data-* attribute
  if ( data === undefined && elem.nodeType === 1 ) {

	var name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();

	data = elem.getAttribute( name );

	if ( typeof data === "string" ) {
	  try {
		data = data === "true" ? true :
		  data === "false" ? false :
		  data === "null" ? null :
		  // Only convert to a number if it doesn't change the string
		  +data + "" === data ? +data :
		  rbrace.test( data ) ? jQuery.parseJSON( data ) :
			data;
	  } catch( e ) {}

	  // Make sure we set the data so it isn't changed later
	  jQuery.data( elem, key, data );

	} else {
	  data = undefined;
	}
  }

  return data;
}

// checks a cache object for emptiness
function isEmptyDataObject( obj ) {
  var name;
  for ( name in obj ) {

	// if the public data object is empty, the private is still empty
	if ( name === "data" && jQuery.isEmptyObject( obj[name] ) ) {
	  continue;
	}
	if ( name !== "toJSON" ) {
	  return false;
	}
  }

  return true;
}
jQuery.extend({
  queue: function( elem, type, data ) {
	var queue;

	if ( elem ) {
	  type = ( type || "fx" ) + "queue";
	  queue = jQuery._data( elem, type );

	  // Speed up dequeue by getting out quickly if this is just a lookup
	  if ( data ) {
		if ( !queue || jQuery.isArray(data) ) {
		  queue = jQuery._data( elem, type, jQuery.makeArray(data) );
		} else {
		  queue.push( data );
		}
	  }
	  return queue || [];
	}
  },

  dequeue: function( elem, type ) {
	type = type || "fx";

	var queue = jQuery.queue( elem, type ),
	  startLength = queue.length,
	  fn = queue.shift(),
	  hooks = jQuery._queueHooks( elem, type ),
	  next = function() {
		jQuery.dequeue( elem, type );
	  };

	// If the fx queue is dequeued, always remove the progress sentinel
	if ( fn === "inprogress" ) {
	  fn = queue.shift();
	  startLength--;
	}

	hooks.cur = fn;
	if ( fn ) {

	  // Add a progress sentinel to prevent the fx queue from being
	  // automatically dequeued
	  if ( type === "fx" ) {
		queue.unshift( "inprogress" );
	  }

	  // clear up the last queue stop function
	  delete hooks.stop;
	  fn.call( elem, next, hooks );
	}

	if ( !startLength && hooks ) {
	  hooks.empty.fire();
	}
  },

  // not intended for public consumption - generates a queueHooks object, or returns the current one
  _queueHooks: function( elem, type ) {
	var key = type + "queueHooks";
	return jQuery._data( elem, key ) || jQuery._data( elem, key, {
	  empty: jQuery.Callbacks("once memory").add(function() {
		jQuery._removeData( elem, type + "queue" );
		jQuery._removeData( elem, key );
	  })
	});
  }
});

jQuery.fn.extend({
  queue: function( type, data ) {
	var setter = 2;

	if ( typeof type !== "string" ) {
	  data = type;
	  type = "fx";
	  setter--;
	}

	if ( arguments.length < setter ) {
	  return jQuery.queue( this[0], type );
	}

	return data === undefined ?
	  this :
	  this.each(function() {
		var queue = jQuery.queue( this, type, data );

		// ensure a hooks for this queue
		jQuery._queueHooks( this, type );

		if ( type === "fx" && queue[0] !== "inprogress" ) {
		  jQuery.dequeue( this, type );
		}
	  });
  },
  dequeue: function( type ) {
	return this.each(function() {
	  jQuery.dequeue( this, type );
	});
  },
  // Based off of the plugin by Clint Helfers, with permission.
  // http://blindsignals.com/index.php/2009/07/jquery-delay/
  delay: function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
	  var timeout = setTimeout( next, time );
	  hooks.stop = function() {
		clearTimeout( timeout );
	  };
	});
  },
  clearQueue: function( type ) {
	return this.queue( type || "fx", [] );
  },
  // Get a promise resolved when queues of a certain type
  // are emptied (fx is the type by default)
  promise: function( type, obj ) {
	var tmp,
	  count = 1,
	  defer = jQuery.Deferred(),
	  elements = this,
	  i = this.length,
	  resolve = function() {
		if ( !( --count ) ) {
		  defer.resolveWith( elements, [ elements ] );
		}
	  };

	if ( typeof type !== "string" ) {
	  obj = type;
	  type = undefined;
	}
	type = type || "fx";

	while( i-- ) {
	  tmp = jQuery._data( elements[ i ], type + "queueHooks" );
	  if ( tmp && tmp.empty ) {
		count++;
		tmp.empty.add( resolve );
	  }
	}
	resolve();
	return defer.promise( obj );
  }
});
var nodeHook, boolHook,
  rclass = /[\t\r\n]/g,
  rreturn = /\r/g,
  rfocusable = /^(?:input|select|textarea|button|object)$/i,
  rclickable = /^(?:a|area)$/i,
  rboolean = /^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,
  ruseDefault = /^(?:checked|selected)$/i,
  getSetAttribute = jQuery.support.getSetAttribute,
  getSetInput = jQuery.support.input;

jQuery.fn.extend({
  attr: function( name, value ) {
	return jQuery.access( this, jQuery.attr, name, value, arguments.length > 1 );
  },

  removeAttr: function( name ) {
	return this.each(function() {
	  jQuery.removeAttr( this, name );
	});
  },

  prop: function( name, value ) {
	return jQuery.access( this, jQuery.prop, name, value, arguments.length > 1 );
  },

  removeProp: function( name ) {
	name = jQuery.propFix[ name ] || name;
	return this.each(function() {
	  // try/catch handles cases where IE balks (such as removing a property on window)
	  try {
		this[ name ] = undefined;
		delete this[ name ];
	  } catch( e ) {}
	});
  },

  addClass: function( value ) {
	var classes, elem, cur, clazz, j,
	  i = 0,
	  len = this.length,
	  proceed = typeof value === "string" && value;

	if ( jQuery.isFunction( value ) ) {
	  return this.each(function( j ) {
		jQuery( this ).addClass( value.call( this, j, this.className ) );
	  });
	}

	if ( proceed ) {
	  // The disjunction here is for better compressibility (see removeClass)
	  classes = ( value || "" ).match( core_rnotwhite ) || [];

	  for ( ; i < len; i++ ) {
		elem = this[ i ];
		cur = elem.nodeType === 1 && ( elem.className ?
		  ( " " + elem.className + " " ).replace( rclass, " " ) :
		  " "
		);

		if ( cur ) {
		  j = 0;
		  while ( (clazz = classes[j++]) ) {
			if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
			  cur += clazz + " ";
			}
		  }
		  elem.className = jQuery.trim( cur );

		}
	  }
	}

	return this;
  },

  removeClass: function( value ) {
	var classes, elem, cur, clazz, j,
	  i = 0,
	  len = this.length,
	  proceed = arguments.length === 0 || typeof value === "string" && value;

	if ( jQuery.isFunction( value ) ) {
	  return this.each(function( j ) {
		jQuery( this ).removeClass( value.call( this, j, this.className ) );
	  });
	}
	if ( proceed ) {
	  classes = ( value || "" ).match( core_rnotwhite ) || [];

	  for ( ; i < len; i++ ) {
		elem = this[ i ];
		// This expression is here for better compressibility (see addClass)
		cur = elem.nodeType === 1 && ( elem.className ?
		  ( " " + elem.className + " " ).replace( rclass, " " ) :
		  ""
		);

		if ( cur ) {
		  j = 0;
		  while ( (clazz = classes[j++]) ) {
			// Remove *all* instances
			while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
			  cur = cur.replace( " " + clazz + " ", " " );
			}
		  }
		  elem.className = value ? jQuery.trim( cur ) : "";
		}
	  }
	}

	return this;
  },

  toggleClass: function( value, stateVal ) {
	var type = typeof value,
	  isBool = typeof stateVal === "boolean";

	if ( jQuery.isFunction( value ) ) {
	  return this.each(function( i ) {
		jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
	  });
	}

	return this.each(function() {
	  if ( type === "string" ) {
		// toggle individual class names
		var className,
		  i = 0,
		  self = jQuery( this ),
		  state = stateVal,
		  classNames = value.match( core_rnotwhite ) || [];

		while ( (className = classNames[ i++ ]) ) {
		  // check each className given, space separated list
		  state = isBool ? state : !self.hasClass( className );
		  self[ state ? "addClass" : "removeClass" ]( className );
		}

	  // Toggle whole class name
	  } else if ( type === core_strundefined || type === "boolean" ) {
		if ( this.className ) {
		  // store className if set
		  jQuery._data( this, "__className__", this.className );
		}

		// If the element has a class name or if we're passed "false",
		// then remove the whole classname (if there was one, the above saved it).
		// Otherwise bring back whatever was previously saved (if anything),
		// falling back to the empty string if nothing was stored.
		this.className = this.className || value === false ? "" : jQuery._data( this, "__className__" ) || "";
	  }
	});
  },

  hasClass: function( selector ) {
	var className = " " + selector + " ",
	  i = 0,
	  l = this.length;
	for ( ; i < l; i++ ) {
	  if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
		return true;
	  }
	}

	return false;
  },

  val: function( value ) {
	var ret, hooks, isFunction,
	  elem = this[0];

	if ( !arguments.length ) {
	  if ( elem ) {
		hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

		if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
		  return ret;
		}

		ret = elem.value;

		return typeof ret === "string" ?
		  // handle most common string cases
		  ret.replace(rreturn, "") :
		  // handle cases where value is null/undef or number
		  ret == null ? "" : ret;
	  }

	  return;
	}

	isFunction = jQuery.isFunction( value );

	return this.each(function( i ) {
	  var val,
		self = jQuery(this);

	  if ( this.nodeType !== 1 ) {
		return;
	  }

	  if ( isFunction ) {
		val = value.call( this, i, self.val() );
	  } else {
		val = value;
	  }

	  // Treat null/undefined as ""; convert numbers to string
	  if ( val == null ) {
		val = "";
	  } else if ( typeof val === "number" ) {
		val += "";
	  } else if ( jQuery.isArray( val ) ) {
		val = jQuery.map(val, function ( value ) {
		  return value == null ? "" : value + "";
		});
	  }

	  hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

	  // If set returns undefined, fall back to normal setting
	  if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
		this.value = val;
	  }
	});
  }
});

jQuery.extend({
  valHooks: {
	option: {
	  get: function( elem ) {
		// attributes.value is undefined in Blackberry 4.7 but
		// uses .value. See #6932
		var val = elem.attributes.value;
		return !val || val.specified ? elem.value : elem.text;
	  }
	},
	select: {
	  get: function( elem ) {
		var value, option,
		  options = elem.options,
		  index = elem.selectedIndex,
		  one = elem.type === "select-one" || index < 0,
		  values = one ? null : [],
		  max = one ? index + 1 : options.length,
		  i = index < 0 ?
			max :
			one ? index : 0;

		// Loop through all the selected options
		for ( ; i < max; i++ ) {
		  option = options[ i ];

		  // oldIE doesn't update selected after form reset (#2551)
		  if ( ( option.selected || i === index ) &&
			  // Don't return options that are disabled or in a disabled optgroup
			  ( jQuery.support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null ) &&
			  ( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

			// Get the specific value for the option
			value = jQuery( option ).val();

			// We don't need an array for one selects
			if ( one ) {
			  return value;
			}

			// Multi-Selects return an array
			values.push( value );
		  }
		}

		return values;
	  },

	  set: function( elem, value ) {
		var values = jQuery.makeArray( value );

		jQuery(elem).find("option").each(function() {
		  this.selected = jQuery.inArray( jQuery(this).val(), values ) >= 0;
		});

		if ( !values.length ) {
		  elem.selectedIndex = -1;
		}
		return values;
	  }
	}
  },

  attr: function( elem, name, value ) {
	var hooks, notxml, ret,
	  nType = elem.nodeType;

	// don't get/set attributes on text, comment and attribute nodes
	if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
	  return;
	}

	// Fallback to prop when attributes are not supported
	if ( typeof elem.getAttribute === core_strundefined ) {
	  return jQuery.prop( elem, name, value );
	}

	notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

	// All attributes are lowercase
	// Grab necessary hook if one is defined
	if ( notxml ) {
	  name = name.toLowerCase();
	  hooks = jQuery.attrHooks[ name ] || ( rboolean.test( name ) ? boolHook : nodeHook );
	}

	if ( value !== undefined ) {

	  if ( value === null ) {
		jQuery.removeAttr( elem, name );

	  } else if ( hooks && notxml && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
		return ret;

	  } else {
		elem.setAttribute( name, value + "" );
		return value;
	  }

	} else if ( hooks && notxml && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
	  return ret;

	} else {

	  // In IE9+, Flash objects don't have .getAttribute (#12945)
	  // Support: IE9+
	  if ( typeof elem.getAttribute !== core_strundefined ) {
		ret =  elem.getAttribute( name );
	  }

	  // Non-existent attributes return null, we normalize to undefined
	  return ret == null ?
		undefined :
		ret;
	}
  },

  removeAttr: function( elem, value ) {
	var name, propName,
	  i = 0,
	  attrNames = value && value.match( core_rnotwhite );

	if ( attrNames && elem.nodeType === 1 ) {
	  while ( (name = attrNames[i++]) ) {
		propName = jQuery.propFix[ name ] || name;

		// Boolean attributes get special treatment (#10870)
		if ( rboolean.test( name ) ) {
		  // Set corresponding property to false for boolean attributes
		  // Also clear defaultChecked/defaultSelected (if appropriate) for IE<8
		  if ( !getSetAttribute && ruseDefault.test( name ) ) {
			elem[ jQuery.camelCase( "default-" + name ) ] =
			  elem[ propName ] = false;
		  } else {
			elem[ propName ] = false;
		  }

		// See #9699 for explanation of this approach (setting first, then removal)
		} else {
		  jQuery.attr( elem, name, "" );
		}

		elem.removeAttribute( getSetAttribute ? name : propName );
	  }
	}
  },

  attrHooks: {
	type: {
	  set: function( elem, value ) {
		if ( !jQuery.support.radioValue && value === "radio" && jQuery.nodeName(elem, "input") ) {
		  // Setting the type on a radio button after the value resets the value in IE6-9
		  // Reset value to default in case type is set after value during creation
		  var val = elem.value;
		  elem.setAttribute( "type", value );
		  if ( val ) {
			elem.value = val;
		  }
		  return value;
		}
	  }
	}
  },

  propFix: {
	tabindex: "tabIndex",
	readonly: "readOnly",
	"for": "htmlFor",
	"class": "className",
	maxlength: "maxLength",
	cellspacing: "cellSpacing",
	cellpadding: "cellPadding",
	rowspan: "rowSpan",
	colspan: "colSpan",
	usemap: "useMap",
	frameborder: "frameBorder",
	contenteditable: "contentEditable"
  },

  prop: function( elem, name, value ) {
	var ret, hooks, notxml,
	  nType = elem.nodeType;

	// don't get/set properties on text, comment and attribute nodes
	if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
	  return;
	}

	notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

	if ( notxml ) {
	  // Fix name and attach hooks
	  name = jQuery.propFix[ name ] || name;
	  hooks = jQuery.propHooks[ name ];
	}

	if ( value !== undefined ) {
	  if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
		return ret;

	  } else {
		return ( elem[ name ] = value );
	  }

	} else {
	  if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
		return ret;

	  } else {
		return elem[ name ];
	  }
	}
  },

  propHooks: {
	tabIndex: {
	  get: function( elem ) {
		// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
		// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
		var attributeNode = elem.getAttributeNode("tabindex");

		return attributeNode && attributeNode.specified ?
		  parseInt( attributeNode.value, 10 ) :
		  rfocusable.test( elem.nodeName ) || rclickable.test( elem.nodeName ) && elem.href ?
			0 :
			undefined;
	  }
	}
  }
});

// Hook for boolean attributes
boolHook = {
  get: function( elem, name ) {
	var
	  // Use .prop to determine if this attribute is understood as boolean
	  prop = jQuery.prop( elem, name ),

	  // Fetch it accordingly
	  attr = typeof prop === "boolean" && elem.getAttribute( name ),
	  detail = typeof prop === "boolean" ?

		getSetInput && getSetAttribute ?
		  attr != null :
		  // oldIE fabricates an empty string for missing boolean attributes
		  // and conflates checked/selected into attroperties
		  ruseDefault.test( name ) ?
			elem[ jQuery.camelCase( "default-" + name ) ] :
			!!attr :

		// fetch an attribute node for properties not recognized as boolean
		elem.getAttributeNode( name );

	return detail && detail.value !== false ?
	  name.toLowerCase() :
	  undefined;
  },
  set: function( elem, value, name ) {
	if ( value === false ) {
	  // Remove boolean attributes when set to false
	  jQuery.removeAttr( elem, name );
	} else if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
	  // IE<8 needs the *property* name
	  elem.setAttribute( !getSetAttribute && jQuery.propFix[ name ] || name, name );

	// Use defaultChecked and defaultSelected for oldIE
	} else {
	  elem[ jQuery.camelCase( "default-" + name ) ] = elem[ name ] = true;
	}

	return name;
  }
};

// fix oldIE value attroperty
if ( !getSetInput || !getSetAttribute ) {
  jQuery.attrHooks.value = {
	get: function( elem, name ) {
	  var ret = elem.getAttributeNode( name );
	  return jQuery.nodeName( elem, "input" ) ?

		// Ignore the value *property* by using defaultValue
		elem.defaultValue :

		ret && ret.specified ? ret.value : undefined;
	},
	set: function( elem, value, name ) {
	  if ( jQuery.nodeName( elem, "input" ) ) {
		// Does not return so that setAttribute is also used
		elem.defaultValue = value;
	  } else {
		// Use nodeHook if defined (#1954); otherwise setAttribute is fine
		return nodeHook && nodeHook.set( elem, value, name );
	  }
	}
  };
}

// IE6/7 do not support getting/setting some attributes with get/setAttribute
if ( !getSetAttribute ) {

  // Use this for any attribute in IE6/7
  // This fixes almost every IE6/7 issue
  nodeHook = jQuery.valHooks.button = {
	get: function( elem, name ) {
	  var ret = elem.getAttributeNode( name );
	  return ret && ( name === "id" || name === "name" || name === "coords" ? ret.value !== "" : ret.specified ) ?
		ret.value :
		undefined;
	},
	set: function( elem, value, name ) {
	  // Set the existing or create a new attribute node
	  var ret = elem.getAttributeNode( name );
	  if ( !ret ) {
		elem.setAttributeNode(
		  (ret = elem.ownerDocument.createAttribute( name ))
		);
	  }

	  ret.value = value += "";

	  // Break association with cloned elements by also using setAttribute (#9646)
	  return name === "value" || value === elem.getAttribute( name ) ?
		value :
		undefined;
	}
  };

  // Set contenteditable to false on removals(#10429)
  // Setting to empty string throws an error as an invalid value
  jQuery.attrHooks.contenteditable = {
	get: nodeHook.get,
	set: function( elem, value, name ) {
	  nodeHook.set( elem, value === "" ? false : value, name );
	}
  };

  // Set width and height to auto instead of 0 on empty string( Bug #8150 )
  // This is for removals
  jQuery.each([ "width", "height" ], function( i, name ) {
	jQuery.attrHooks[ name ] = jQuery.extend( jQuery.attrHooks[ name ], {
	  set: function( elem, value ) {
		if ( value === "" ) {
		  elem.setAttribute( name, "auto" );
		  return value;
		}
	  }
	});
  });
}


// Some attributes require a special call on IE
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !jQuery.support.hrefNormalized ) {
  jQuery.each([ "href", "src", "width", "height" ], function( i, name ) {
	jQuery.attrHooks[ name ] = jQuery.extend( jQuery.attrHooks[ name ], {
	  get: function( elem ) {
		var ret = elem.getAttribute( name, 2 );
		return ret == null ? undefined : ret;
	  }
	});
  });

  // href/src property should get the full normalized URL (#10299/#12915)
  jQuery.each([ "href", "src" ], function( i, name ) {
	jQuery.propHooks[ name ] = {
	  get: function( elem ) {
		return elem.getAttribute( name, 4 );
	  }
	};
  });
}

if ( !jQuery.support.style ) {
  jQuery.attrHooks.style = {
	get: function( elem ) {
	  // Return undefined in the case of empty string
	  // Note: IE uppercases css property names, but if we were to .toLowerCase()
	  // .cssText, that would destroy case senstitivity in URL's, like in "background"
	  return elem.style.cssText || undefined;
	},
	set: function( elem, value ) {
	  return ( elem.style.cssText = value + "" );
	}
  };
}

// Safari mis-reports the default selected property of an option
// Accessing the parent's selectedIndex property fixes it
if ( !jQuery.support.optSelected ) {
  jQuery.propHooks.selected = jQuery.extend( jQuery.propHooks.selected, {
	get: function( elem ) {
	  var parent = elem.parentNode;

	  if ( parent ) {
		parent.selectedIndex;

		// Make sure that it also works with optgroups, see #5701
		if ( parent.parentNode ) {
		  parent.parentNode.selectedIndex;
		}
	  }
	  return null;
	}
  });
}

// IE6/7 call enctype encoding
if ( !jQuery.support.enctype ) {
  jQuery.propFix.enctype = "encoding";
}

// Radios and checkboxes getter/setter
if ( !jQuery.support.checkOn ) {
  jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
	  get: function( elem ) {
		// Handle the case where in Webkit "" is returned instead of "on" if a value isn't specified
		return elem.getAttribute("value") === null ? "on" : elem.value;
	  }
	};
  });
}
jQuery.each([ "radio", "checkbox" ], function() {
  jQuery.valHooks[ this ] = jQuery.extend( jQuery.valHooks[ this ], {
	set: function( elem, value ) {
	  if ( jQuery.isArray( value ) ) {
		return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
	  }
	}
  });
});
var rformElems = /^(?:input|select|textarea)$/i,
  rkeyEvent = /^key/,
  rmouseEvent = /^(?:mouse|contextmenu)|click/,
  rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
  rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
  return true;
}

function returnFalse() {
  return false;
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

  global: {},

  add: function( elem, types, handler, data, selector ) {
	var tmp, events, t, handleObjIn,
	  special, eventHandle, handleObj,
	  handlers, type, namespaces, origType,
	  elemData = jQuery._data( elem );

	// Don't attach events to noData or text/comment nodes (but allow plain objects)
	if ( !elemData ) {
	  return;
	}

	// Caller can pass in an object of custom data in lieu of the handler
	if ( handler.handler ) {
	  handleObjIn = handler;
	  handler = handleObjIn.handler;
	  selector = handleObjIn.selector;
	}

	// Make sure that the handler has a unique ID, used to find/remove it later
	if ( !handler.guid ) {
	  handler.guid = jQuery.guid++;
	}

	// Init the element's event structure and main handler, if this is the first
	if ( !(events = elemData.events) ) {
	  events = elemData.events = {};
	}
	if ( !(eventHandle = elemData.handle) ) {
	  eventHandle = elemData.handle = function( e ) {
		// Discard the second event of a jQuery.event.trigger() and
		// when an event is called after a page has unloaded
		return typeof jQuery !== core_strundefined && (!e || jQuery.event.triggered !== e.type) ?
		  jQuery.event.dispatch.apply( eventHandle.elem, arguments ) :
		  undefined;
	  };
	  // Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
	  eventHandle.elem = elem;
	}

	// Handle multiple events separated by a space
	// jQuery(...).bind("mouseover mouseout", fn);
	types = ( types || "" ).match( core_rnotwhite ) || [""];
	t = types.length;
	while ( t-- ) {
	  tmp = rtypenamespace.exec( types[t] ) || [];
	  type = origType = tmp[1];
	  namespaces = ( tmp[2] || "" ).split( "." ).sort();

	  // If event changes its type, use the special event handlers for the changed type
	  special = jQuery.event.special[ type ] || {};

	  // If selector defined, determine special event api type, otherwise given type
	  type = ( selector ? special.delegateType : special.bindType ) || type;

	  // Update special based on newly reset type
	  special = jQuery.event.special[ type ] || {};

	  // handleObj is passed to all event handlers
	  handleObj = jQuery.extend({
		type: type,
		origType: origType,
		data: data,
		handler: handler,
		guid: handler.guid,
		selector: selector,
		needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
		namespace: namespaces.join(".")
	  }, handleObjIn );

	  // Init the event handler queue if we're the first
	  if ( !(handlers = events[ type ]) ) {
		handlers = events[ type ] = [];
		handlers.delegateCount = 0;

		// Only use addEventListener/attachEvent if the special events handler returns false
		if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
		  // Bind the global event handler to the element
		  if ( elem.addEventListener ) {
			elem.addEventListener( type, eventHandle, false );

		  } else if ( elem.attachEvent ) {
			elem.attachEvent( "on" + type, eventHandle );
		  }
		}
	  }

	  if ( special.add ) {
		special.add.call( elem, handleObj );

		if ( !handleObj.handler.guid ) {
		  handleObj.handler.guid = handler.guid;
		}
	  }

	  // Add to the element's handler list, delegates in front
	  if ( selector ) {
		handlers.splice( handlers.delegateCount++, 0, handleObj );
	  } else {
		handlers.push( handleObj );
	  }

	  // Keep track of which events have ever been used, for event optimization
	  jQuery.event.global[ type ] = true;
	}

	// Nullify elem to prevent memory leaks in IE
	elem = null;
  },

  // Detach an event or set of events from an element
  remove: function( elem, types, handler, selector, mappedTypes ) {
	var j, handleObj, tmp,
	  origCount, t, events,
	  special, handlers, type,
	  namespaces, origType,
	  elemData = jQuery.hasData( elem ) && jQuery._data( elem );

	if ( !elemData || !(events = elemData.events) ) {
	  return;
	}

	// Once for each type.namespace in types; type may be omitted
	types = ( types || "" ).match( core_rnotwhite ) || [""];
	t = types.length;
	while ( t-- ) {
	  tmp = rtypenamespace.exec( types[t] ) || [];
	  type = origType = tmp[1];
	  namespaces = ( tmp[2] || "" ).split( "." ).sort();

	  // Unbind all events (on this namespace, if provided) for the element
	  if ( !type ) {
		for ( type in events ) {
		  jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
		}
		continue;
	  }

	  special = jQuery.event.special[ type ] || {};
	  type = ( selector ? special.delegateType : special.bindType ) || type;
	  handlers = events[ type ] || [];
	  tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

	  // Remove matching events
	  origCount = j = handlers.length;
	  while ( j-- ) {
		handleObj = handlers[ j ];

		if ( ( mappedTypes || origType === handleObj.origType ) &&
		  ( !handler || handler.guid === handleObj.guid ) &&
		  ( !tmp || tmp.test( handleObj.namespace ) ) &&
		  ( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
		  handlers.splice( j, 1 );

		  if ( handleObj.selector ) {
			handlers.delegateCount--;
		  }
		  if ( special.remove ) {
			special.remove.call( elem, handleObj );
		  }
		}
	  }

	  // Remove generic event handler if we removed something and no more handlers exist
	  // (avoids potential for endless recursion during removal of special event handlers)
	  if ( origCount && !handlers.length ) {
		if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
		  jQuery.removeEvent( elem, type, elemData.handle );
		}

		delete events[ type ];
	  }
	}

	// Remove the expando if it's no longer used
	if ( jQuery.isEmptyObject( events ) ) {
	  delete elemData.handle;

	  // removeData also checks for emptiness and clears the expando if empty
	  // so use it instead of delete
	  jQuery._removeData( elem, "events" );
	}
  },

  trigger: function( event, data, elem, onlyHandlers ) {
	var handle, ontype, cur,
	  bubbleType, special, tmp, i,
	  eventPath = [ elem || document ],
	  type = core_hasOwn.call( event, "type" ) ? event.type : event,
	  namespaces = core_hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

	cur = tmp = elem = elem || document;

	// Don't do events on text and comment nodes
	if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
	  return;
	}

	// focus/blur morphs to focusin/out; ensure we're not firing them right now
	if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
	  return;
	}

	if ( type.indexOf(".") >= 0 ) {
	  // Namespaced trigger; create a regexp to match event type in handle()
	  namespaces = type.split(".");
	  type = namespaces.shift();
	  namespaces.sort();
	}
	ontype = type.indexOf(":") < 0 && "on" + type;

	// Caller can pass in a jQuery.Event object, Object, or just an event type string
	event = event[ jQuery.expando ] ?
	  event :
	  new jQuery.Event( type, typeof event === "object" && event );

	event.isTrigger = true;
	event.namespace = namespaces.join(".");
	event.namespace_re = event.namespace ?
	  new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
	  null;

	// Clean up the event in case it is being reused
	event.result = undefined;
	if ( !event.target ) {
	  event.target = elem;
	}

	// Clone any incoming data and prepend the event, creating the handler arg list
	data = data == null ?
	  [ event ] :
	  jQuery.makeArray( data, [ event ] );

	// Allow special events to draw outside the lines
	special = jQuery.event.special[ type ] || {};
	if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
	  return;
	}

	// Determine event propagation path in advance, per W3C events spec (#9951)
	// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
	if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

	  bubbleType = special.delegateType || type;
	  if ( !rfocusMorph.test( bubbleType + type ) ) {
		cur = cur.parentNode;
	  }
	  for ( ; cur; cur = cur.parentNode ) {
		eventPath.push( cur );
		tmp = cur;
	  }

	  // Only add window if we got to document (e.g., not plain obj or detached DOM)
	  if ( tmp === (elem.ownerDocument || document) ) {
		eventPath.push( tmp.defaultView || tmp.parentWindow || window );
	  }
	}

	// Fire handlers on the event path
	i = 0;
	while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

	  event.type = i > 1 ?
		bubbleType :
		special.bindType || type;

	  // jQuery handler
	  handle = ( jQuery._data( cur, "events" ) || {} )[ event.type ] && jQuery._data( cur, "handle" );
	  if ( handle ) {
		handle.apply( cur, data );
	  }

	  // Native handler
	  handle = ontype && cur[ ontype ];
	  if ( handle && jQuery.acceptData( cur ) && handle.apply && handle.apply( cur, data ) === false ) {
		event.preventDefault();
	  }
	}
	event.type = type;

	// If nobody prevented the default action, do it now
	if ( !onlyHandlers && !event.isDefaultPrevented() ) {

	  if ( (!special._default || special._default.apply( elem.ownerDocument, data ) === false) &&
		!(type === "click" && jQuery.nodeName( elem, "a" )) && jQuery.acceptData( elem ) ) {

		// Call a native DOM method on the target with the same name name as the event.
		// Can't use an .isFunction() check here because IE6/7 fails that test.
		// Don't do default actions on window, that's where global variables be (#6170)
		if ( ontype && elem[ type ] && !jQuery.isWindow( elem ) ) {

		  // Don't re-trigger an onFOO event when we call its FOO() method
		  tmp = elem[ ontype ];

		  if ( tmp ) {
			elem[ ontype ] = null;
		  }

		  // Prevent re-triggering of the same event, since we already bubbled it above
		  jQuery.event.triggered = type;
		  try {
			elem[ type ]();
		  } catch ( e ) {
			// IE<9 dies on focus/blur to hidden element (#1486,#12518)
			// only reproducible on winXP IE8 native, not IE9 in IE8 mode
		  }
		  jQuery.event.triggered = undefined;

		  if ( tmp ) {
			elem[ ontype ] = tmp;
		  }
		}
	  }
	}

	return event.result;
  },

  dispatch: function( event ) {

	// Make a writable jQuery.Event from the native event object
	event = jQuery.event.fix( event );

	var i, ret, handleObj, matched, j,
	  handlerQueue = [],
	  args = core_slice.call( arguments ),
	  handlers = ( jQuery._data( this, "events" ) || {} )[ event.type ] || [],
	  special = jQuery.event.special[ event.type ] || {};

	// Use the fix-ed jQuery.Event rather than the (read-only) native event
	args[0] = event;
	event.delegateTarget = this;

	// Call the preDispatch hook for the mapped type, and let it bail if desired
	if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
	  return;
	}

	// Determine handlers
	handlerQueue = jQuery.event.handlers.call( this, event, handlers );

	// Run delegates first; they may want to stop propagation beneath us
	i = 0;
	while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
	  event.currentTarget = matched.elem;

	  j = 0;
	  while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

		// Triggered event must either 1) have no namespace, or
		// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
		if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

		  event.handleObj = handleObj;
		  event.data = handleObj.data;

		  ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
			  .apply( matched.elem, args );

		  if ( ret !== undefined ) {
			if ( (event.result = ret) === false ) {
			  event.preventDefault();
			  event.stopPropagation();
			}
		  }
		}
	  }
	}

	// Call the postDispatch hook for the mapped type
	if ( special.postDispatch ) {
	  special.postDispatch.call( this, event );
	}

	return event.result;
  },

  handlers: function( event, handlers ) {
	var sel, handleObj, matches, i,
	  handlerQueue = [],
	  delegateCount = handlers.delegateCount,
	  cur = event.target;

	// Find delegate handlers
	// Black-hole SVG <use> instance trees (#13180)
	// Avoid non-left-click bubbling in Firefox (#3861)
	if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

	  for ( ; cur != this; cur = cur.parentNode || this ) {

		// Don't check non-elements (#13208)
		// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
		if ( cur.nodeType === 1 && (cur.disabled !== true || event.type !== "click") ) {
		  matches = [];
		  for ( i = 0; i < delegateCount; i++ ) {
			handleObj = handlers[ i ];

			// Don't conflict with Object.prototype properties (#13203)
			sel = handleObj.selector + " ";

			if ( matches[ sel ] === undefined ) {
			  matches[ sel ] = handleObj.needsContext ?
				jQuery( sel, this ).index( cur ) >= 0 :
				jQuery.find( sel, this, null, [ cur ] ).length;
			}
			if ( matches[ sel ] ) {
			  matches.push( handleObj );
			}
		  }
		  if ( matches.length ) {
			handlerQueue.push({ elem: cur, handlers: matches });
		  }
		}
	  }
	}

	// Add the remaining (directly-bound) handlers
	if ( delegateCount < handlers.length ) {
	  handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
	}

	return handlerQueue;
  },

  fix: function( event ) {
	if ( event[ jQuery.expando ] ) {
	  return event;
	}

	// Create a writable copy of the event object and normalize some properties
	var i, prop, copy,
	  type = event.type,
	  originalEvent = event,
	  fixHook = this.fixHooks[ type ];

	if ( !fixHook ) {
	  this.fixHooks[ type ] = fixHook =
		rmouseEvent.test( type ) ? this.mouseHooks :
		rkeyEvent.test( type ) ? this.keyHooks :
		{};
	}
	copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

	event = new jQuery.Event( originalEvent );

	i = copy.length;
	while ( i-- ) {
	  prop = copy[ i ];
	  event[ prop ] = originalEvent[ prop ];
	}

	// Support: IE<9
	// Fix target property (#1925)
	if ( !event.target ) {
	  event.target = originalEvent.srcElement || document;
	}

	// Support: Chrome 23+, Safari?
	// Target should not be a text node (#504, #13143)
	if ( event.target.nodeType === 3 ) {
	  event.target = event.target.parentNode;
	}

	// Support: IE<9
	// For mouse/key events, metaKey==false if it's undefined (#3368, #11328)
	event.metaKey = !!event.metaKey;

	return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
  },

  // Includes some event props shared by KeyEvent and MouseEvent
  props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

  fixHooks: {},

  keyHooks: {
	props: "char charCode key keyCode".split(" "),
	filter: function( event, original ) {

	  // Add which for key events
	  if ( event.which == null ) {
		event.which = original.charCode != null ? original.charCode : original.keyCode;
	  }

	  return event;
	}
  },

  mouseHooks: {
	props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
	filter: function( event, original ) {
	  var body, eventDoc, doc,
		button = original.button,
		fromElement = original.fromElement;

	  // Calculate pageX/Y if missing and clientX/Y available
	  if ( event.pageX == null && original.clientX != null ) {
		eventDoc = event.target.ownerDocument || document;
		doc = eventDoc.documentElement;
		body = eventDoc.body;

		event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
		event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
	  }

	  // Add relatedTarget, if necessary
	  if ( !event.relatedTarget && fromElement ) {
		event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
	  }

	  // Add which for click: 1 === left; 2 === middle; 3 === right
	  // Note: button is not normalized, so don't use it
	  if ( !event.which && button !== undefined ) {
		event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
	  }

	  return event;
	}
  },

  special: {
	load: {
	  // Prevent triggered image.load events from bubbling to window.load
	  noBubble: true
	},
	click: {
	  // For checkbox, fire native event so checked state will be right
	  trigger: function() {
		if ( jQuery.nodeName( this, "input" ) && this.type === "checkbox" && this.click ) {
		  this.click();
		  return false;
		}
	  }
	},
	focus: {
	  // Fire native event if possible so blur/focus sequence is correct
	  trigger: function() {
		if ( this !== document.activeElement && this.focus ) {
		  try {
			this.focus();
			return false;
		  } catch ( e ) {
			// Support: IE<9
			// If we error on focus to hidden element (#1486, #12518),
			// let .trigger() run the handlers
		  }
		}
	  },
	  delegateType: "focusin"
	},
	blur: {
	  trigger: function() {
		if ( this === document.activeElement && this.blur ) {
		  this.blur();
		  return false;
		}
	  },
	  delegateType: "focusout"
	},

	beforeunload: {
	  postDispatch: function( event ) {

		// Even when returnValue equals to undefined Firefox will still show alert
		if ( event.result !== undefined ) {
		  event.originalEvent.returnValue = event.result;
		}
	  }
	}
  },

  simulate: function( type, elem, event, bubble ) {
	// Piggyback on a donor event to simulate a different one.
	// Fake originalEvent to avoid donor's stopPropagation, but if the
	// simulated event prevents default then we do the same on the donor.
	var e = jQuery.extend(
	  new jQuery.Event(),
	  event,
	  { type: type,
		isSimulated: true,
		originalEvent: {}
	  }
	);
	if ( bubble ) {
	  jQuery.event.trigger( e, null, elem );
	} else {
	  jQuery.event.dispatch.call( elem, e );
	}
	if ( e.isDefaultPrevented() ) {
	  event.preventDefault();
	}
  }
};

jQuery.removeEvent = document.removeEventListener ?
  function( elem, type, handle ) {
	if ( elem.removeEventListener ) {
	  elem.removeEventListener( type, handle, false );
	}
  } :
  function( elem, type, handle ) {
	var name = "on" + type;

	if ( elem.detachEvent ) {

	  // #8545, #7054, preventing memory leaks for custom events in IE6-8
	  // detachEvent needed property on element, by name of that event, to properly expose it to GC
	  if ( typeof elem[ name ] === core_strundefined ) {
		elem[ name ] = null;
	  }

	  elem.detachEvent( name, handle );
	}
  };

jQuery.Event = function( src, props ) {
  // Allow instantiation without the 'new' keyword
  if ( !(this instanceof jQuery.Event) ) {
	return new jQuery.Event( src, props );
  }

  // Event object
  if ( src && src.type ) {
	this.originalEvent = src;
	this.type = src.type;

	// Events bubbling up the document may have been marked as prevented
	// by a handler lower down the tree; reflect the correct value.
	this.isDefaultPrevented = ( src.defaultPrevented || src.returnValue === false ||
	  src.getPreventDefault && src.getPreventDefault() ) ? returnTrue : returnFalse;

  // Event type
  } else {
	this.type = src;
  }

  // Put explicitly provided properties onto the event object
  if ( props ) {
	jQuery.extend( this, props );
  }

  // Create a timestamp if incoming event doesn't have one
  this.timeStamp = src && src.timeStamp || jQuery.now();

  // Mark it as fixed
  this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
  isDefaultPrevented: returnFalse,
  isPropagationStopped: returnFalse,
  isImmediatePropagationStopped: returnFalse,

  preventDefault: function() {
	var e = this.originalEvent;

	this.isDefaultPrevented = returnTrue;
	if ( !e ) {
	  return;
	}

	// If preventDefault exists, run it on the original event
	if ( e.preventDefault ) {
	  e.preventDefault();

	// Support: IE
	// Otherwise set the returnValue property of the original event to false
	} else {
	  e.returnValue = false;
	}
  },
  stopPropagation: function() {
	var e = this.originalEvent;

	this.isPropagationStopped = returnTrue;
	if ( !e ) {
	  return;
	}
	// If stopPropagation exists, run it on the original event
	if ( e.stopPropagation ) {
	  e.stopPropagation();
	}

	// Support: IE
	// Set the cancelBubble property of the original event to true
	e.cancelBubble = true;
  },
  stopImmediatePropagation: function() {
	this.isImmediatePropagationStopped = returnTrue;
	this.stopPropagation();
  }
};

// Create mouseenter/leave events using mouseover/out and event-time checks
jQuery.each({
  mouseenter: "mouseover",
  mouseleave: "mouseout"
}, function( orig, fix ) {
  jQuery.event.special[ orig ] = {
	delegateType: fix,
	bindType: fix,

	handle: function( event ) {
	  var ret,
		target = this,
		related = event.relatedTarget,
		handleObj = event.handleObj;

	  // For mousenter/leave call the handler if related is outside the target.
	  // NB: No relatedTarget if the mouse left/entered the browser window
	  if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
		event.type = handleObj.origType;
		ret = handleObj.handler.apply( this, arguments );
		event.type = fix;
	  }
	  return ret;
	}
  };
});

// IE submit delegation
if ( !jQuery.support.submitBubbles ) {

  jQuery.event.special.submit = {
	setup: function() {
	  // Only need this for delegated form submit events
	  if ( jQuery.nodeName( this, "form" ) ) {
		return false;
	  }

	  // Lazy-add a submit handler when a descendant form may potentially be submitted
	  jQuery.event.add( this, "click._submit keypress._submit", function( e ) {
		// Node name check avoids a VML-related crash in IE (#9807)
		var elem = e.target,
		  form = jQuery.nodeName( elem, "input" ) || jQuery.nodeName( elem, "button" ) ? elem.form : undefined;
		if ( form && !jQuery._data( form, "submitBubbles" ) ) {
		  jQuery.event.add( form, "submit._submit", function( event ) {
			event._submit_bubble = true;
		  });
		  jQuery._data( form, "submitBubbles", true );
		}
	  });
	  // return undefined since we don't need an event listener
	},

	postDispatch: function( event ) {
	  // If form was submitted by the user, bubble the event up the tree
	  if ( event._submit_bubble ) {
		delete event._submit_bubble;
		if ( this.parentNode && !event.isTrigger ) {
		  jQuery.event.simulate( "submit", this.parentNode, event, true );
		}
	  }
	},

	teardown: function() {
	  // Only need this for delegated form submit events
	  if ( jQuery.nodeName( this, "form" ) ) {
		return false;
	  }

	  // Remove delegated handlers; cleanData eventually reaps submit handlers attached above
	  jQuery.event.remove( this, "._submit" );
	}
  };
}

// IE change delegation and checkbox/radio fix
if ( !jQuery.support.changeBubbles ) {

  jQuery.event.special.change = {

	setup: function() {

	  if ( rformElems.test( this.nodeName ) ) {
		// IE doesn't fire change on a check/radio until blur; trigger it on click
		// after a propertychange. Eat the blur-change in special.change.handle.
		// This still fires onchange a second time for check/radio after blur.
		if ( this.type === "checkbox" || this.type === "radio" ) {
		  jQuery.event.add( this, "propertychange._change", function( event ) {
			if ( event.originalEvent.propertyName === "checked" ) {
			  this._just_changed = true;
			}
		  });
		  jQuery.event.add( this, "click._change", function( event ) {
			if ( this._just_changed && !event.isTrigger ) {
			  this._just_changed = false;
			}
			// Allow triggered, simulated change events (#11500)
			jQuery.event.simulate( "change", this, event, true );
		  });
		}
		return false;
	  }
	  // Delegated event; lazy-add a change handler on descendant inputs
	  jQuery.event.add( this, "beforeactivate._change", function( e ) {
		var elem = e.target;

		if ( rformElems.test( elem.nodeName ) && !jQuery._data( elem, "changeBubbles" ) ) {
		  jQuery.event.add( elem, "change._change", function( event ) {
			if ( this.parentNode && !event.isSimulated && !event.isTrigger ) {
			  jQuery.event.simulate( "change", this.parentNode, event, true );
			}
		  });
		  jQuery._data( elem, "changeBubbles", true );
		}
	  });
	},

	handle: function( event ) {
	  var elem = event.target;

	  // Swallow native change events from checkbox/radio, we already triggered them above
	  if ( this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox") ) {
		return event.handleObj.handler.apply( this, arguments );
	  }
	},

	teardown: function() {
	  jQuery.event.remove( this, "._change" );

	  return !rformElems.test( this.nodeName );
	}
  };
}

// Create "bubbling" focus and blur events
if ( !jQuery.support.focusinBubbles ) {
  jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

	// Attach a single capturing handler while someone wants focusin/focusout
	var attaches = 0,
	  handler = function( event ) {
		jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
	  };

	jQuery.event.special[ fix ] = {
	  setup: function() {
		if ( attaches++ === 0 ) {
		  document.addEventListener( orig, handler, true );
		}
	  },
	  teardown: function() {
		if ( --attaches === 0 ) {
		  document.removeEventListener( orig, handler, true );
		}
	  }
	};
  });
}

jQuery.fn.extend({

  on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
	var type, origFn;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {
	  // ( types-Object, selector, data )
	  if ( typeof selector !== "string" ) {
		// ( types-Object, data )
		data = data || selector;
		selector = undefined;
	  }
	  for ( type in types ) {
		this.on( type, selector, data, types[ type ], one );
	  }
	  return this;
	}

	if ( data == null && fn == null ) {
	  // ( types, fn )
	  fn = selector;
	  data = selector = undefined;
	} else if ( fn == null ) {
	  if ( typeof selector === "string" ) {
		// ( types, selector, fn )
		fn = data;
		data = undefined;
	  } else {
		// ( types, data, fn )
		fn = data;
		data = selector;
		selector = undefined;
	  }
	}
	if ( fn === false ) {
	  fn = returnFalse;
	} else if ( !fn ) {
	  return this;
	}

	if ( one === 1 ) {
	  origFn = fn;
	  fn = function( event ) {
		// Can use an empty set, since event contains the info
		jQuery().off( event );
		return origFn.apply( this, arguments );
	  };
	  // Use same guid so caller can remove using origFn
	  fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return this.each( function() {
	  jQuery.event.add( this, types, fn, data, selector );
	});
  },
  one: function( types, selector, data, fn ) {
	return this.on( types, selector, data, fn, 1 );
  },
  off: function( types, selector, fn ) {
	var handleObj, type;
	if ( types && types.preventDefault && types.handleObj ) {
	  // ( event )  dispatched jQuery.Event
	  handleObj = types.handleObj;
	  jQuery( types.delegateTarget ).off(
		handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
		handleObj.selector,
		handleObj.handler
	  );
	  return this;
	}
	if ( typeof types === "object" ) {
	  // ( types-object [, selector] )
	  for ( type in types ) {
		this.off( type, selector, types[ type ] );
	  }
	  return this;
	}
	if ( selector === false || typeof selector === "function" ) {
	  // ( types [, fn] )
	  fn = selector;
	  selector = undefined;
	}
	if ( fn === false ) {
	  fn = returnFalse;
	}
	return this.each(function() {
	  jQuery.event.remove( this, types, fn, selector );
	});
  },

  bind: function( types, data, fn ) {
	return this.on( types, null, data, fn );
  },
  unbind: function( types, fn ) {
	return this.off( types, null, fn );
  },

  delegate: function( selector, types, data, fn ) {
	return this.on( types, selector, data, fn );
  },
  undelegate: function( selector, types, fn ) {
	// ( namespace ) or ( selector, types [, fn] )
	return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
  },

  trigger: function( type, data ) {
	return this.each(function() {
	  jQuery.event.trigger( type, data, this );
	});
  },
  triggerHandler: function( type, data ) {
	var elem = this[0];
	if ( elem ) {
	  return jQuery.event.trigger( type, data, elem, true );
	}
  }
});
/*!
 * Sizzle CSS Selector Engine
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license
 * http://sizzlejs.com/
 */
(function( window, undefined ) {

var i,
  cachedruns,
  Expr,
  getText,
  isXML,
  compile,
  hasDuplicate,
  outermostContext,

  // Local document vars
  setDocument,
  document,
  docElem,
  documentIsXML,
  rbuggyQSA,
  rbuggyMatches,
  matches,
  contains,
  sortOrder,

  // Instance-specific data
  expando = "sizzle" + -(new Date()),
  preferredDoc = window.document,
  support = {},
  dirruns = 0,
  done = 0,
  classCache = createCache(),
  tokenCache = createCache(),
  compilerCache = createCache(),

  // General-purpose constants
  strundefined = typeof undefined,
  MAX_NEGATIVE = 1 << 31,

  // Array methods
  arr = [],
  pop = arr.pop,
  push = arr.push,
  slice = arr.slice,
  // Use a stripped-down indexOf if we can't use a native one
  indexOf = arr.indexOf || function( elem ) {
	var i = 0,
	  len = this.length;
	for ( ; i < len; i++ ) {
	  if ( this[i] === elem ) {
		return i;
	  }
	}
	return -1;
  },


  // Regular expressions

  // Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
  whitespace = "[\\x20\\t\\r\\n\\f]",
  // http://www.w3.org/TR/css3-syntax/#characters
  characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

  // Loosely modeled on CSS identifier characters
  // An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
  // Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
  identifier = characterEncoding.replace( "w", "w#" ),

  // Acceptable operators http://www.w3.org/TR/selectors/#attribute-selectors
  operators = "([*^$|!~]?=)",
  attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace +
	"*(?:" + operators + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",

  // Prefer arguments quoted,
  //   then not containing pseudos/brackets,
  //   then attribute selectors/non-parenthetical expressions,
  //   then anything else
  // These preferences are here to reduce the number of selectors
  //   needing tokenize in the PSEUDO preFilter
  pseudos = ":(" + characterEncoding + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + attributes.replace( 3, 8 ) + ")*)|.*)\\)|)",

  // Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
  rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

  rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
  rcombinators = new RegExp( "^" + whitespace + "*([\\x20\\t\\r\\n\\f>+~])" + whitespace + "*" ),
  rpseudo = new RegExp( pseudos ),
  ridentifier = new RegExp( "^" + identifier + "$" ),

  matchExpr = {
	"ID": new RegExp( "^#(" + characterEncoding + ")" ),
	"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
	"NAME": new RegExp( "^\\[name=['\"]?(" + characterEncoding + ")['\"]?\\]" ),
	"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
	"ATTR": new RegExp( "^" + attributes ),
	"PSEUDO": new RegExp( "^" + pseudos ),
	"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
	  "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
	  "*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
	// For use in libraries implementing .is()
	// We use this for POS matching in `select`
	"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
	  whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
  },

  rsibling = /[\x20\t\r\n\f]*[+~]/,

  rnative = /^[^{]+\{\s*\[native code/,

  // Easily-parseable/retrievable ID or TAG or CLASS selectors
  rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

  rinputs = /^(?:input|select|textarea|button)$/i,
  rheader = /^h\d$/i,

  rescape = /'|\\/g,
  rattributeQuotes = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,

  // CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
  runescape = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
  funescape = function( _, escaped ) {
	var high = "0x" + escaped - 0x10000;
	// NaN means non-codepoint
	return high !== high ?
	  escaped :
	  // BMP codepoint
	  high < 0 ?
		String.fromCharCode( high + 0x10000 ) :
		// Supplemental Plane codepoint (surrogate pair)
		String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
  };

// Use a stripped-down slice if we can't use a native one
try {
  slice.call( preferredDoc.documentElement.childNodes, 0 )[0].nodeType;
} catch ( e ) {
  slice = function( i ) {
	var elem,
	  results = [];
	while ( (elem = this[i++]) ) {
	  results.push( elem );
	}
	return results;
  };
}

/**
 * For feature detection
 * @param {Function} fn The function to test for native support
 */
function isNative( fn ) {
  return rnative.test( fn + "" );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
  var cache,
	keys = [];

  return (cache = function( key, value ) {
	// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
	if ( keys.push( key += " " ) > Expr.cacheLength ) {
	  // Only keep the most recent entries
	  delete cache[ keys.shift() ];
	}
	return (cache[ key ] = value);
  });
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
  fn[ expando ] = true;
  return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
  var div = document.createElement("div");

  try {
	return fn( div );
  } catch (e) {
	return false;
  } finally {
	// release memory in IE
	div = null;
  }
}

function Sizzle( selector, context, results, seed ) {
  var match, elem, m, nodeType,
	// QSA vars
	i, groups, old, nid, newContext, newSelector;

  if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
	setDocument( context );
  }

  context = context || document;
  results = results || [];

  if ( !selector || typeof selector !== "string" ) {
	return results;
  }

  if ( (nodeType = context.nodeType) !== 1 && nodeType !== 9 ) {
	return [];
  }

  if ( !documentIsXML && !seed ) {

	// Shortcuts
	if ( (match = rquickExpr.exec( selector )) ) {
	  // Speed-up: Sizzle("#ID")
	  if ( (m = match[1]) ) {
		if ( nodeType === 9 ) {
		  elem = context.getElementById( m );
		  // Check parentNode to catch when Blackberry 4.6 returns
		  // nodes that are no longer in the document #6963
		  if ( elem && elem.parentNode ) {
			// Handle the case where IE, Opera, and Webkit return items
			// by name instead of ID
			if ( elem.id === m ) {
			  results.push( elem );
			  return results;
			}
		  } else {
			return results;
		  }
		} else {
		  // Context is not a document
		  if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
			contains( context, elem ) && elem.id === m ) {
			results.push( elem );
			return results;
		  }
		}

	  // Speed-up: Sizzle("TAG")
	  } else if ( match[2] ) {
		push.apply( results, slice.call(context.getElementsByTagName( selector ), 0) );
		return results;

	  // Speed-up: Sizzle(".CLASS")
	  } else if ( (m = match[3]) && support.getByClassName && context.getElementsByClassName ) {
		push.apply( results, slice.call(context.getElementsByClassName( m ), 0) );
		return results;
	  }
	}

	// QSA path
	if ( support.qsa && !rbuggyQSA.test(selector) ) {
	  old = true;
	  nid = expando;
	  newContext = context;
	  newSelector = nodeType === 9 && selector;

	  // qSA works strangely on Element-rooted queries
	  // We can work around this by specifying an extra ID on the root
	  // and working up from there (Thanks to Andrew Dupont for the technique)
	  // IE 8 doesn't work on object elements
	  if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
		groups = tokenize( selector );

		if ( (old = context.getAttribute("id")) ) {
		  nid = old.replace( rescape, "\\$&" );
		} else {
		  context.setAttribute( "id", nid );
		}
		nid = "[id='" + nid + "'] ";

		i = groups.length;
		while ( i-- ) {
		  groups[i] = nid + toSelector( groups[i] );
		}
		newContext = rsibling.test( selector ) && context.parentNode || context;
		newSelector = groups.join(",");
	  }

	  if ( newSelector ) {
		try {
		  push.apply( results, slice.call( newContext.querySelectorAll(
			newSelector
		  ), 0 ) );
		  return results;
		} catch(qsaError) {
		} finally {
		  if ( !old ) {
			context.removeAttribute("id");
		  }
		}
	  }
	}
  }

  // All others
  return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Detect xml
 * @param {Element|Object} elem An element or a document
 */
isXML = Sizzle.isXML = function( elem ) {
  // documentElement is verified for cases where it doesn't yet exist
  // (such as loading iframes in IE - #4833)
  var documentElement = elem && (elem.ownerDocument || elem).documentElement;
  return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
  var doc = node ? node.ownerDocument || node : preferredDoc;

  // If no document and documentElement is available, return
  if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
	return document;
  }

  // Set our document
  document = doc;
  docElem = doc.documentElement;

  // Support tests
  documentIsXML = isXML( doc );

  // Check if getElementsByTagName("*") returns only elements
  support.tagNameNoComments = assert(function( div ) {
	div.appendChild( doc.createComment("") );
	return !div.getElementsByTagName("*").length;
  });

  // Check if attributes should be retrieved by attribute nodes
  support.attributes = assert(function( div ) {
	div.innerHTML = "<select></select>";
	var type = typeof div.lastChild.getAttribute("multiple");
	// IE8 returns a string for some attributes even when not present
	return type !== "boolean" && type !== "string";
  });

  // Check if getElementsByClassName can be trusted
  support.getByClassName = assert(function( div ) {
	// Opera can't find a second classname (in 9.6)
	div.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>";
	if ( !div.getElementsByClassName || !div.getElementsByClassName("e").length ) {
	  return false;
	}

	// Safari 3.2 caches class attributes and doesn't catch changes
	div.lastChild.className = "e";
	return div.getElementsByClassName("e").length === 2;
  });

  // Check if getElementById returns elements by name
  // Check if getElementsByName privileges form controls or returns elements by ID
  support.getByName = assert(function( div ) {
	// Inject content
	div.id = expando + 0;
	div.innerHTML = "<a name='" + expando + "'></a><div name='" + expando + "'></div>";
	docElem.insertBefore( div, docElem.firstChild );

	// Test
	var pass = doc.getElementsByName &&
	  // buggy browsers will return fewer than the correct 2
	  doc.getElementsByName( expando ).length === 2 +
	  // buggy browsers will return more than the correct 0
	  doc.getElementsByName( expando + 0 ).length;
	support.getIdNotName = !doc.getElementById( expando );

	// Cleanup
	docElem.removeChild( div );

	return pass;
  });

  // IE6/7 return modified attributes
  Expr.attrHandle = assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild && typeof div.firstChild.getAttribute !== strundefined &&
	  div.firstChild.getAttribute("href") === "#";
  }) ?
	{} :
	{
	  "href": function( elem ) {
		return elem.getAttribute( "href", 2 );
	  },
	  "type": function( elem ) {
		return elem.getAttribute("type");
	  }
	};

  // ID find and filter
  if ( support.getIdNotName ) {
	Expr.find["ID"] = function( id, context ) {
	  if ( typeof context.getElementById !== strundefined && !documentIsXML ) {
		var m = context.getElementById( id );
		// Check parentNode to catch when Blackberry 4.6 returns
		// nodes that are no longer in the document #6963
		return m && m.parentNode ? [m] : [];
	  }
	};
	Expr.filter["ID"] = function( id ) {
	  var attrId = id.replace( runescape, funescape );
	  return function( elem ) {
		return elem.getAttribute("id") === attrId;
	  };
	};
  } else {
	Expr.find["ID"] = function( id, context ) {
	  if ( typeof context.getElementById !== strundefined && !documentIsXML ) {
		var m = context.getElementById( id );

		return m ?
		  m.id === id || typeof m.getAttributeNode !== strundefined && m.getAttributeNode("id").value === id ?
			[m] :
			undefined :
		  [];
	  }
	};
	Expr.filter["ID"] =  function( id ) {
	  var attrId = id.replace( runescape, funescape );
	  return function( elem ) {
		var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
		return node && node.value === attrId;
	  };
	};
  }

  // Tag
  Expr.find["TAG"] = support.tagNameNoComments ?
	function( tag, context ) {
	  if ( typeof context.getElementsByTagName !== strundefined ) {
		return context.getElementsByTagName( tag );
	  }
	} :
	function( tag, context ) {
	  var elem,
		tmp = [],
		i = 0,
		results = context.getElementsByTagName( tag );

	  // Filter out possible comments
	  if ( tag === "*" ) {
		while ( (elem = results[i++]) ) {
		  if ( elem.nodeType === 1 ) {
			tmp.push( elem );
		  }
		}

		return tmp;
	  }
	  return results;
	};

  // Name
  Expr.find["NAME"] = support.getByName && function( tag, context ) {
	if ( typeof context.getElementsByName !== strundefined ) {
	  return context.getElementsByName( name );
	}
  };

  // Class
  Expr.find["CLASS"] = support.getByClassName && function( className, context ) {
	if ( typeof context.getElementsByClassName !== strundefined && !documentIsXML ) {
	  return context.getElementsByClassName( className );
	}
  };

  // QSA and matchesSelector support

  // matchesSelector(:active) reports false when true (IE9/Opera 11.5)
  rbuggyMatches = [];

  // qSa(:focus) reports false when true (Chrome 21),
  // no need to also add to buggyMatches since matches checks buggyQSA
  // A support test would require too much code (would include document ready)
  rbuggyQSA = [ ":focus" ];

  if ( (support.qsa = isNative(doc.querySelectorAll)) ) {
	// Build QSA regex
	// Regex strategy adopted from Diego Perini
	assert(function( div ) {
	  // Select is set to empty string on purpose
	  // This is to test IE's treatment of not explictly
	  // setting a boolean content attribute,
	  // since its presence should be enough
	  // http://bugs.jquery.com/ticket/12359
	  div.innerHTML = "<select><option selected=''></option></select>";

	  // IE8 - Some boolean attributes are not treated correctly
	  if ( !div.querySelectorAll("[selected]").length ) {
		rbuggyQSA.push( "\\[" + whitespace + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)" );
	  }

	  // Webkit/Opera - :checked should return selected option elements
	  // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
	  // IE8 throws error here and will not see later tests
	  if ( !div.querySelectorAll(":checked").length ) {
		rbuggyQSA.push(":checked");
	  }
	});

	assert(function( div ) {

	  // Opera 10-12/IE8 - ^= $= *= and empty values
	  // Should not select anything
	  div.innerHTML = "<input type='hidden' i=''/>";
	  if ( div.querySelectorAll("[i^='']").length ) {
		rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:\"\"|'')" );
	  }

	  // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
	  // IE8 throws error here and will not see later tests
	  if ( !div.querySelectorAll(":enabled").length ) {
		rbuggyQSA.push( ":enabled", ":disabled" );
	  }

	  // Opera 10-11 does not throw on post-comma invalid pseudos
	  div.querySelectorAll("*,:x");
	  rbuggyQSA.push(",.*:");
	});
  }

  if ( (support.matchesSelector = isNative( (matches = docElem.matchesSelector ||
	docElem.mozMatchesSelector ||
	docElem.webkitMatchesSelector ||
	docElem.oMatchesSelector ||
	docElem.msMatchesSelector) )) ) {

	assert(function( div ) {
	  // Check to see if it's possible to do matchesSelector
	  // on a disconnected node (IE 9)
	  support.disconnectedMatch = matches.call( div, "div" );

	  // This should fail with an exception
	  // Gecko does not error, returns false instead
	  matches.call( div, "[s!='']:x" );
	  rbuggyMatches.push( "!=", pseudos );
	});
  }

  rbuggyQSA = new RegExp( rbuggyQSA.join("|") );
  rbuggyMatches = new RegExp( rbuggyMatches.join("|") );

  // Element contains another
  // Purposefully does not implement inclusive descendent
  // As in, an element does not contain itself
  contains = isNative(docElem.contains) || docElem.compareDocumentPosition ?
	function( a, b ) {
	  var adown = a.nodeType === 9 ? a.documentElement : a,
		bup = b && b.parentNode;
	  return a === bup || !!( bup && bup.nodeType === 1 && (
		adown.contains ?
		  adown.contains( bup ) :
		  a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
	  ));
	} :
	function( a, b ) {
	  if ( b ) {
		while ( (b = b.parentNode) ) {
		  if ( b === a ) {
			return true;
		  }
		}
	  }
	  return false;
	};

  // Document order sorting
  sortOrder = docElem.compareDocumentPosition ?
  function( a, b ) {
	var compare;

	if ( a === b ) {
	  hasDuplicate = true;
	  return 0;
	}

	if ( (compare = b.compareDocumentPosition && a.compareDocumentPosition && a.compareDocumentPosition( b )) ) {
	  if ( compare & 1 || a.parentNode && a.parentNode.nodeType === 11 ) {
		if ( a === doc || contains( preferredDoc, a ) ) {
		  return -1;
		}
		if ( b === doc || contains( preferredDoc, b ) ) {
		  return 1;
		}
		return 0;
	  }
	  return compare & 4 ? -1 : 1;
	}

	return a.compareDocumentPosition ? -1 : 1;
  } :
  function( a, b ) {
	var cur,
	  i = 0,
	  aup = a.parentNode,
	  bup = b.parentNode,
	  ap = [ a ],
	  bp = [ b ];

	// Exit early if the nodes are identical
	if ( a === b ) {
	  hasDuplicate = true;
	  return 0;

	// Parentless nodes are either documents or disconnected
	} else if ( !aup || !bup ) {
	  return a === doc ? -1 :
		b === doc ? 1 :
		aup ? -1 :
		bup ? 1 :
		0;

	// If the nodes are siblings, we can do a quick check
	} else if ( aup === bup ) {
	  return siblingCheck( a, b );
	}

	// Otherwise we need full lists of their ancestors for comparison
	cur = a;
	while ( (cur = cur.parentNode) ) {
	  ap.unshift( cur );
	}
	cur = b;
	while ( (cur = cur.parentNode) ) {
	  bp.unshift( cur );
	}

	// Walk down the tree looking for a discrepancy
	while ( ap[i] === bp[i] ) {
	  i++;
	}

	return i ?
	  // Do a sibling check if the nodes have a common ancestor
	  siblingCheck( ap[i], bp[i] ) :

	  // Otherwise nodes in our document sort first
	  ap[i] === preferredDoc ? -1 :
	  bp[i] === preferredDoc ? 1 :
	  0;
  };

  // Always assume the presence of duplicates if sort doesn't
  // pass them to our comparison function (as in Google Chrome).
  hasDuplicate = false;
  [0, 0].sort( sortOrder );
  support.detectDuplicates = hasDuplicate;

  return document;
};

Sizzle.matches = function( expr, elements ) {
  return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
  // Set document vars if needed
  if ( ( elem.ownerDocument || elem ) !== document ) {
	setDocument( elem );
  }

  // Make sure that attribute selectors are quoted
  expr = expr.replace( rattributeQuotes, "='$1']" );

  // rbuggyQSA always contains :focus, so no need for an existence check
  if ( support.matchesSelector && !documentIsXML && (!rbuggyMatches || !rbuggyMatches.test(expr)) && !rbuggyQSA.test(expr) ) {
	try {
	  var ret = matches.call( elem, expr );

	  // IE 9's matchesSelector returns false on disconnected nodes
	  if ( ret || support.disconnectedMatch ||
		  // As well, disconnected nodes are said to be in a document
		  // fragment in IE 9
		  elem.document && elem.document.nodeType !== 11 ) {
		return ret;
	  }
	} catch(e) {}
  }

  return Sizzle( expr, document, null, [elem] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
  // Set document vars if needed
  if ( ( context.ownerDocument || context ) !== document ) {
	setDocument( context );
  }
  return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
  var val;

  // Set document vars if needed
  if ( ( elem.ownerDocument || elem ) !== document ) {
	setDocument( elem );
  }

  if ( !documentIsXML ) {
	name = name.toLowerCase();
  }
  if ( (val = Expr.attrHandle[ name ]) ) {
	return val( elem );
  }
  if ( documentIsXML || support.attributes ) {
	return elem.getAttribute( name );
  }
  return ( (val = elem.getAttributeNode( name )) || elem.getAttribute( name ) ) && elem[ name ] === true ?
	name :
	val && val.specified ? val.value : null;
};

Sizzle.error = function( msg ) {
  throw new Error( "Syntax error, unrecognized expression: " + msg );
};

// Document sorting and removing duplicates
Sizzle.uniqueSort = function( results ) {
  var elem,
	duplicates = [],
	i = 1,
	j = 0;

  // Unless we *know* we can detect duplicates, assume their presence
  hasDuplicate = !support.detectDuplicates;
  results.sort( sortOrder );

  if ( hasDuplicate ) {
	for ( ; (elem = results[i]); i++ ) {
	  if ( elem === results[ i - 1 ] ) {
		j = duplicates.push( i );
	  }
	}
	while ( j-- ) {
	  results.splice( duplicates[ j ], 1 );
	}
  }

  return results;
};

function siblingCheck( a, b ) {
  var cur = b && a,
	diff = cur && ( ~b.sourceIndex || MAX_NEGATIVE ) - ( ~a.sourceIndex || MAX_NEGATIVE );

  // Use IE sourceIndex if available on both nodes
  if ( diff ) {
	return diff;
  }

  // Check if b follows a
  if ( cur ) {
	while ( (cur = cur.nextSibling) ) {
	  if ( cur === b ) {
		return -1;
	  }
	}
  }

  return a ? 1 : -1;
}

// Returns a function to use in pseudos for input types
function createInputPseudo( type ) {
  return function( elem ) {
	var name = elem.nodeName.toLowerCase();
	return name === "input" && elem.type === type;
  };
}

// Returns a function to use in pseudos for buttons
function createButtonPseudo( type ) {
  return function( elem ) {
	var name = elem.nodeName.toLowerCase();
	return (name === "input" || name === "button") && elem.type === type;
  };
}

// Returns a function to use in pseudos for positionals
function createPositionalPseudo( fn ) {
  return markFunction(function( argument ) {
	argument = +argument;
	return markFunction(function( seed, matches ) {
	  var j,
		matchIndexes = fn( [], seed.length, argument ),
		i = matchIndexes.length;

	  // Match elements found at the specified indexes
	  while ( i-- ) {
		if ( seed[ (j = matchIndexes[i]) ] ) {
		  seed[j] = !(matches[j] = seed[j]);
		}
	  }
	});
  });
}

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
  var node,
	ret = "",
	i = 0,
	nodeType = elem.nodeType;

  if ( !nodeType ) {
	// If no nodeType, this is expected to be an array
	for ( ; (node = elem[i]); i++ ) {
	  // Do not traverse comment nodes
	  ret += getText( node );
	}
  } else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
	// Use textContent for elements
	// innerText usage removed for consistency of new lines (see #11153)
	if ( typeof elem.textContent === "string" ) {
	  return elem.textContent;
	} else {
	  // Traverse its children
	  for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
		ret += getText( elem );
	  }
	}
  } else if ( nodeType === 3 || nodeType === 4 ) {
	return elem.nodeValue;
  }
  // Do not include comment or processing instruction nodes

  return ret;
};

Expr = Sizzle.selectors = {

  // Can be adjusted by the user
  cacheLength: 50,

  createPseudo: markFunction,

  match: matchExpr,

  find: {},

  relative: {
	">": { dir: "parentNode", first: true },
	" ": { dir: "parentNode" },
	"+": { dir: "previousSibling", first: true },
	"~": { dir: "previousSibling" }
  },

  preFilter: {
	"ATTR": function( match ) {
	  match[1] = match[1].replace( runescape, funescape );

	  // Move the given value to match[3] whether quoted or unquoted
	  match[3] = ( match[4] || match[5] || "" ).replace( runescape, funescape );

	  if ( match[2] === "~=" ) {
		match[3] = " " + match[3] + " ";
	  }

	  return match.slice( 0, 4 );
	},

	"CHILD": function( match ) {
	  /* matches from matchExpr["CHILD"]
		1 type (only|nth|...)
		2 what (child|of-type)
		3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
		4 xn-component of xn+y argument ([+-]?\d*n|)
		5 sign of xn-component
		6 x of xn-component
		7 sign of y-component
		8 y of y-component
	  */
	  match[1] = match[1].toLowerCase();

	  if ( match[1].slice( 0, 3 ) === "nth" ) {
		// nth-* requires argument
		if ( !match[3] ) {
		  Sizzle.error( match[0] );
		}

		// numeric x and y parameters for Expr.filter.CHILD
		// remember that false/true cast respectively to 0/1
		match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
		match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

	  // other types prohibit arguments
	  } else if ( match[3] ) {
		Sizzle.error( match[0] );
	  }

	  return match;
	},

	"PSEUDO": function( match ) {
	  var excess,
		unquoted = !match[5] && match[2];

	  if ( matchExpr["CHILD"].test( match[0] ) ) {
		return null;
	  }

	  // Accept quoted arguments as-is
	  if ( match[4] ) {
		match[2] = match[4];

	  // Strip excess characters from unquoted arguments
	  } else if ( unquoted && rpseudo.test( unquoted ) &&
		// Get excess from tokenize (recursively)
		(excess = tokenize( unquoted, true )) &&
		// advance to the next closing parenthesis
		(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

		// excess is a negative index
		match[0] = match[0].slice( 0, excess );
		match[2] = unquoted.slice( 0, excess );
	  }

	  // Return only captures needed by the pseudo filter method (type and argument)
	  return match.slice( 0, 3 );
	}
  },

  filter: {

	"TAG": function( nodeName ) {
	  if ( nodeName === "*" ) {
		return function() { return true; };
	  }

	  nodeName = nodeName.replace( runescape, funescape ).toLowerCase();
	  return function( elem ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
	  };
	},

	"CLASS": function( className ) {
	  var pattern = classCache[ className + " " ];

	  return pattern ||
		(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
		classCache( className, function( elem ) {
		  return pattern.test( elem.className || (typeof elem.getAttribute !== strundefined && elem.getAttribute("class")) || "" );
		});
	},

	"ATTR": function( name, operator, check ) {
	  return function( elem ) {
		var result = Sizzle.attr( elem, name );

		if ( result == null ) {
		  return operator === "!=";
		}
		if ( !operator ) {
		  return true;
		}

		result += "";

		return operator === "=" ? result === check :
		  operator === "!=" ? result !== check :
		  operator === "^=" ? check && result.indexOf( check ) === 0 :
		  operator === "*=" ? check && result.indexOf( check ) > -1 :
		  operator === "$=" ? check && result.slice( -check.length ) === check :
		  operator === "~=" ? ( " " + result + " " ).indexOf( check ) > -1 :
		  operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
		  false;
	  };
	},

	"CHILD": function( type, what, argument, first, last ) {
	  var simple = type.slice( 0, 3 ) !== "nth",
		forward = type.slice( -4 ) !== "last",
		ofType = what === "of-type";

	  return first === 1 && last === 0 ?

		// Shortcut for :nth-*(n)
		function( elem ) {
		  return !!elem.parentNode;
		} :

		function( elem, context, xml ) {
		  var cache, outerCache, node, diff, nodeIndex, start,
			dir = simple !== forward ? "nextSibling" : "previousSibling",
			parent = elem.parentNode,
			name = ofType && elem.nodeName.toLowerCase(),
			useCache = !xml && !ofType;

		  if ( parent ) {

			// :(first|last|only)-(child|of-type)
			if ( simple ) {
			  while ( dir ) {
				node = elem;
				while ( (node = node[ dir ]) ) {
				  if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
					return false;
				  }
				}
				// Reverse direction for :only-* (if we haven't yet done so)
				start = dir = type === "only" && !start && "nextSibling";
			  }
			  return true;
			}

			start = [ forward ? parent.firstChild : parent.lastChild ];

			// non-xml :nth-child(...) stores cache data on `parent`
			if ( forward && useCache ) {
			  // Seek `elem` from a previously-cached index
			  outerCache = parent[ expando ] || (parent[ expando ] = {});
			  cache = outerCache[ type ] || [];
			  nodeIndex = cache[0] === dirruns && cache[1];
			  diff = cache[0] === dirruns && cache[2];
			  node = nodeIndex && parent.childNodes[ nodeIndex ];

			  while ( (node = ++nodeIndex && node && node[ dir ] ||

				// Fallback to seeking `elem` from the start
				(diff = nodeIndex = 0) || start.pop()) ) {

				// When found, cache indexes on `parent` and break
				if ( node.nodeType === 1 && ++diff && node === elem ) {
				  outerCache[ type ] = [ dirruns, nodeIndex, diff ];
				  break;
				}
			  }

			// Use previously-cached element index if available
			} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
			  diff = cache[1];

			// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
			} else {
			  // Use the same loop as above to seek `elem` from the start
			  while ( (node = ++nodeIndex && node && node[ dir ] ||
				(diff = nodeIndex = 0) || start.pop()) ) {

				if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
				  // Cache the index of each encountered element
				  if ( useCache ) {
					(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
				  }

				  if ( node === elem ) {
					break;
				  }
				}
			  }
			}

			// Incorporate the offset, then check against cycle size
			diff -= last;
			return diff === first || ( diff % first === 0 && diff / first >= 0 );
		  }
		};
	},

	"PSEUDO": function( pseudo, argument ) {
	  // pseudo-class names are case-insensitive
	  // http://www.w3.org/TR/selectors/#pseudo-classes
	  // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
	  // Remember that setFilters inherits from pseudos
	  var args,
		fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
		  Sizzle.error( "unsupported pseudo: " + pseudo );

	  // The user may use createPseudo to indicate that
	  // arguments are needed to create the filter function
	  // just as Sizzle does
	  if ( fn[ expando ] ) {
		return fn( argument );
	  }

	  // But maintain support for old signatures
	  if ( fn.length > 1 ) {
		args = [ pseudo, pseudo, "", argument ];
		return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
		  markFunction(function( seed, matches ) {
			var idx,
			  matched = fn( seed, argument ),
			  i = matched.length;
			while ( i-- ) {
			  idx = indexOf.call( seed, matched[i] );
			  seed[ idx ] = !( matches[ idx ] = matched[i] );
			}
		  }) :
		  function( elem ) {
			return fn( elem, 0, args );
		  };
	  }

	  return fn;
	}
  },

  pseudos: {
	// Potentially complex pseudos
	"not": markFunction(function( selector ) {
	  // Trim the selector passed to compile
	  // to avoid treating leading and trailing
	  // spaces as combinators
	  var input = [],
		results = [],
		matcher = compile( selector.replace( rtrim, "$1" ) );

	  return matcher[ expando ] ?
		markFunction(function( seed, matches, context, xml ) {
		  var elem,
			unmatched = matcher( seed, null, xml, [] ),
			i = seed.length;

		  // Match elements unmatched by `matcher`
		  while ( i-- ) {
			if ( (elem = unmatched[i]) ) {
			  seed[i] = !(matches[i] = elem);
			}
		  }
		}) :
		function( elem, context, xml ) {
		  input[0] = elem;
		  matcher( input, null, xml, results );
		  return !results.pop();
		};
	}),

	"has": markFunction(function( selector ) {
	  return function( elem ) {
		return Sizzle( selector, elem ).length > 0;
	  };
	}),

	"contains": markFunction(function( text ) {
	  return function( elem ) {
		return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
	  };
	}),

	// "Whether an element is represented by a :lang() selector
	// is based solely on the element's language value
	// being equal to the identifier C,
	// or beginning with the identifier C immediately followed by "-".
	// The matching of C against the element's language value is performed case-insensitively.
	// The identifier C does not have to be a valid language name."
	// http://www.w3.org/TR/selectors/#lang-pseudo
	"lang": markFunction( function( lang ) {
	  // lang value must be a valid identifider
	  if ( !ridentifier.test(lang || "") ) {
		Sizzle.error( "unsupported lang: " + lang );
	  }
	  lang = lang.replace( runescape, funescape ).toLowerCase();
	  return function( elem ) {
		var elemLang;
		do {
		  if ( (elemLang = documentIsXML ?
			elem.getAttribute("xml:lang") || elem.getAttribute("lang") :
			elem.lang) ) {

			elemLang = elemLang.toLowerCase();
			return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
		  }
		} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
		return false;
	  };
	}),

	// Miscellaneous
	"target": function( elem ) {
	  var hash = window.location && window.location.hash;
	  return hash && hash.slice( 1 ) === elem.id;
	},

	"root": function( elem ) {
	  return elem === docElem;
	},

	"focus": function( elem ) {
	  return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
	},

	// Boolean properties
	"enabled": function( elem ) {
	  return elem.disabled === false;
	},

	"disabled": function( elem ) {
	  return elem.disabled === true;
	},

	"checked": function( elem ) {
	  // In CSS3, :checked should return both checked and selected elements
	  // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
	  var nodeName = elem.nodeName.toLowerCase();
	  return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
	},

	"selected": function( elem ) {
	  // Accessing this property makes selected-by-default
	  // options in Safari work properly
	  if ( elem.parentNode ) {
		elem.parentNode.selectedIndex;
	  }

	  return elem.selected === true;
	},

	// Contents
	"empty": function( elem ) {
	  // http://www.w3.org/TR/selectors/#empty-pseudo
	  // :empty is only affected by element nodes and content nodes(including text(3), cdata(4)),
	  //   not comment, processing instructions, or others
	  // Thanks to Diego Perini for the nodeName shortcut
	  //   Greater than "@" means alpha characters (specifically not starting with "#" or "?")
	  for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
		if ( elem.nodeName > "@" || elem.nodeType === 3 || elem.nodeType === 4 ) {
		  return false;
		}
	  }
	  return true;
	},

	"parent": function( elem ) {
	  return !Expr.pseudos["empty"]( elem );
	},

	// Element/input types
	"header": function( elem ) {
	  return rheader.test( elem.nodeName );
	},

	"input": function( elem ) {
	  return rinputs.test( elem.nodeName );
	},

	"button": function( elem ) {
	  var name = elem.nodeName.toLowerCase();
	  return name === "input" && elem.type === "button" || name === "button";
	},

	"text": function( elem ) {
	  var attr;
	  // IE6 and 7 will map elem.type to 'text' for new HTML5 types (search, etc)
	  // use getAttribute instead to test this case
	  return elem.nodeName.toLowerCase() === "input" &&
		elem.type === "text" &&
		( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === elem.type );
	},

	// Position-in-collection
	"first": createPositionalPseudo(function() {
	  return [ 0 ];
	}),

	"last": createPositionalPseudo(function( matchIndexes, length ) {
	  return [ length - 1 ];
	}),

	"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
	  return [ argument < 0 ? argument + length : argument ];
	}),

	"even": createPositionalPseudo(function( matchIndexes, length ) {
	  var i = 0;
	  for ( ; i < length; i += 2 ) {
		matchIndexes.push( i );
	  }
	  return matchIndexes;
	}),

	"odd": createPositionalPseudo(function( matchIndexes, length ) {
	  var i = 1;
	  for ( ; i < length; i += 2 ) {
		matchIndexes.push( i );
	  }
	  return matchIndexes;
	}),

	"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
	  var i = argument < 0 ? argument + length : argument;
	  for ( ; --i >= 0; ) {
		matchIndexes.push( i );
	  }
	  return matchIndexes;
	}),

	"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
	  var i = argument < 0 ? argument + length : argument;
	  for ( ; ++i < length; ) {
		matchIndexes.push( i );
	  }
	  return matchIndexes;
	})
  }
};

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
  Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
  Expr.pseudos[ i ] = createButtonPseudo( i );
}

function tokenize( selector, parseOnly ) {
  var matched, match, tokens, type,
	soFar, groups, preFilters,
	cached = tokenCache[ selector + " " ];

  if ( cached ) {
	return parseOnly ? 0 : cached.slice( 0 );
  }

  soFar = selector;
  groups = [];
  preFilters = Expr.preFilter;

  while ( soFar ) {

	// Comma and first run
	if ( !matched || (match = rcomma.exec( soFar )) ) {
	  if ( match ) {
		// Don't consume trailing commas as valid
		soFar = soFar.slice( match[0].length ) || soFar;
	  }
	  groups.push( tokens = [] );
	}

	matched = false;

	// Combinators
	if ( (match = rcombinators.exec( soFar )) ) {
	  matched = match.shift();
	  tokens.push( {
		value: matched,
		// Cast descendant combinators to space
		type: match[0].replace( rtrim, " " )
	  } );
	  soFar = soFar.slice( matched.length );
	}

	// Filters
	for ( type in Expr.filter ) {
	  if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
		(match = preFilters[ type ]( match ))) ) {
		matched = match.shift();
		tokens.push( {
		  value: matched,
		  type: type,
		  matches: match
		} );
		soFar = soFar.slice( matched.length );
	  }
	}

	if ( !matched ) {
	  break;
	}
  }

  // Return the length of the invalid excess
  // if we're just parsing
  // Otherwise, throw an error or return tokens
  return parseOnly ?
	soFar.length :
	soFar ?
	  Sizzle.error( selector ) :
	  // Cache the tokens
	  tokenCache( selector, groups ).slice( 0 );
}

function toSelector( tokens ) {
  var i = 0,
	len = tokens.length,
	selector = "";
  for ( ; i < len; i++ ) {
	selector += tokens[i].value;
  }
  return selector;
}

function addCombinator( matcher, combinator, base ) {
  var dir = combinator.dir,
	checkNonElements = base && dir === "parentNode",
	doneName = done++;

  return combinator.first ?
	// Check against closest ancestor/preceding element
	function( elem, context, xml ) {
	  while ( (elem = elem[ dir ]) ) {
		if ( elem.nodeType === 1 || checkNonElements ) {
		  return matcher( elem, context, xml );
		}
	  }
	} :

	// Check against all ancestor/preceding elements
	function( elem, context, xml ) {
	  var data, cache, outerCache,
		dirkey = dirruns + " " + doneName;

	  // We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
	  if ( xml ) {
		while ( (elem = elem[ dir ]) ) {
		  if ( elem.nodeType === 1 || checkNonElements ) {
			if ( matcher( elem, context, xml ) ) {
			  return true;
			}
		  }
		}
	  } else {
		while ( (elem = elem[ dir ]) ) {
		  if ( elem.nodeType === 1 || checkNonElements ) {
			outerCache = elem[ expando ] || (elem[ expando ] = {});
			if ( (cache = outerCache[ dir ]) && cache[0] === dirkey ) {
			  if ( (data = cache[1]) === true || data === cachedruns ) {
				return data === true;
			  }
			} else {
			  cache = outerCache[ dir ] = [ dirkey ];
			  cache[1] = matcher( elem, context, xml ) || cachedruns;
			  if ( cache[1] === true ) {
				return true;
			  }
			}
		  }
		}
	  }
	};
}

function elementMatcher( matchers ) {
  return matchers.length > 1 ?
	function( elem, context, xml ) {
	  var i = matchers.length;
	  while ( i-- ) {
		if ( !matchers[i]( elem, context, xml ) ) {
		  return false;
		}
	  }
	  return true;
	} :
	matchers[0];
}

function condense( unmatched, map, filter, context, xml ) {
  var elem,
	newUnmatched = [],
	i = 0,
	len = unmatched.length,
	mapped = map != null;

  for ( ; i < len; i++ ) {
	if ( (elem = unmatched[i]) ) {
	  if ( !filter || filter( elem, context, xml ) ) {
		newUnmatched.push( elem );
		if ( mapped ) {
		  map.push( i );
		}
	  }
	}
  }

  return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
  if ( postFilter && !postFilter[ expando ] ) {
	postFilter = setMatcher( postFilter );
  }
  if ( postFinder && !postFinder[ expando ] ) {
	postFinder = setMatcher( postFinder, postSelector );
  }
  return markFunction(function( seed, results, context, xml ) {
	var temp, i, elem,
	  preMap = [],
	  postMap = [],
	  preexisting = results.length,

	  // Get initial elements from seed or context
	  elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

	  // Prefilter to get matcher input, preserving a map for seed-results synchronization
	  matcherIn = preFilter && ( seed || !selector ) ?
		condense( elems, preMap, preFilter, context, xml ) :
		elems,

	  matcherOut = matcher ?
		// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
		postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

		  // ...intermediate processing is necessary
		  [] :

		  // ...otherwise use results directly
		  results :
		matcherIn;

	// Find primary matches
	if ( matcher ) {
	  matcher( matcherIn, matcherOut, context, xml );
	}

	// Apply postFilter
	if ( postFilter ) {
	  temp = condense( matcherOut, postMap );
	  postFilter( temp, [], context, xml );

	  // Un-match failing elements by moving them back to matcherIn
	  i = temp.length;
	  while ( i-- ) {
		if ( (elem = temp[i]) ) {
		  matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
		}
	  }
	}

	if ( seed ) {
	  if ( postFinder || preFilter ) {
		if ( postFinder ) {
		  // Get the final matcherOut by condensing this intermediate into postFinder contexts
		  temp = [];
		  i = matcherOut.length;
		  while ( i-- ) {
			if ( (elem = matcherOut[i]) ) {
			  // Restore matcherIn since elem is not yet a final match
			  temp.push( (matcherIn[i] = elem) );
			}
		  }
		  postFinder( null, (matcherOut = []), temp, xml );
		}

		// Move matched elements from seed to results to keep them synchronized
		i = matcherOut.length;
		while ( i-- ) {
		  if ( (elem = matcherOut[i]) &&
			(temp = postFinder ? indexOf.call( seed, elem ) : preMap[i]) > -1 ) {

			seed[temp] = !(results[temp] = elem);
		  }
		}
	  }

	// Add elements to results, through postFinder if defined
	} else {
	  matcherOut = condense(
		matcherOut === results ?
		  matcherOut.splice( preexisting, matcherOut.length ) :
		  matcherOut
	  );
	  if ( postFinder ) {
		postFinder( null, results, matcherOut, xml );
	  } else {
		push.apply( results, matcherOut );
	  }
	}
  });
}

function matcherFromTokens( tokens ) {
  var checkContext, matcher, j,
	len = tokens.length,
	leadingRelative = Expr.relative[ tokens[0].type ],
	implicitRelative = leadingRelative || Expr.relative[" "],
	i = leadingRelative ? 1 : 0,

	// The foundational matcher ensures that elements are reachable from top-level context(s)
	matchContext = addCombinator( function( elem ) {
	  return elem === checkContext;
	}, implicitRelative, true ),
	matchAnyContext = addCombinator( function( elem ) {
	  return indexOf.call( checkContext, elem ) > -1;
	}, implicitRelative, true ),
	matchers = [ function( elem, context, xml ) {
	  return ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
		(checkContext = context).nodeType ?
		  matchContext( elem, context, xml ) :
		  matchAnyContext( elem, context, xml ) );
	} ];

  for ( ; i < len; i++ ) {
	if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
	  matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
	} else {
	  matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

	  // Return special upon seeing a positional matcher
	  if ( matcher[ expando ] ) {
		// Find the next relative operator (if any) for proper handling
		j = ++i;
		for ( ; j < len; j++ ) {
		  if ( Expr.relative[ tokens[j].type ] ) {
			break;
		  }
		}
		return setMatcher(
		  i > 1 && elementMatcher( matchers ),
		  i > 1 && toSelector( tokens.slice( 0, i - 1 ) ).replace( rtrim, "$1" ),
		  matcher,
		  i < j && matcherFromTokens( tokens.slice( i, j ) ),
		  j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
		  j < len && toSelector( tokens )
		);
	  }
	  matchers.push( matcher );
	}
  }

  return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
  // A counter to specify which element is currently being matched
  var matcherCachedRuns = 0,
	bySet = setMatchers.length > 0,
	byElement = elementMatchers.length > 0,
	superMatcher = function( seed, context, xml, results, expandContext ) {
	  var elem, j, matcher,
		setMatched = [],
		matchedCount = 0,
		i = "0",
		unmatched = seed && [],
		outermost = expandContext != null,
		contextBackup = outermostContext,
		// We must always have either seed elements or context
		elems = seed || byElement && Expr.find["TAG"]( "*", expandContext && context.parentNode || context ),
		// Use integer dirruns iff this is the outermost matcher
		dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1);

	  if ( outermost ) {
		outermostContext = context !== document && context;
		cachedruns = matcherCachedRuns;
	  }

	  // Add elements passing elementMatchers directly to results
	  // Keep `i` a string if there are no elements so `matchedCount` will be "00" below
	  for ( ; (elem = elems[i]) != null; i++ ) {
		if ( byElement && elem ) {
		  j = 0;
		  while ( (matcher = elementMatchers[j++]) ) {
			if ( matcher( elem, context, xml ) ) {
			  results.push( elem );
			  break;
			}
		  }
		  if ( outermost ) {
			dirruns = dirrunsUnique;
			cachedruns = ++matcherCachedRuns;
		  }
		}

		// Track unmatched elements for set filters
		if ( bySet ) {
		  // They will have gone through all possible matchers
		  if ( (elem = !matcher && elem) ) {
			matchedCount--;
		  }

		  // Lengthen the array for every element, matched or not
		  if ( seed ) {
			unmatched.push( elem );
		  }
		}
	  }

	  // Apply set filters to unmatched elements
	  matchedCount += i;
	  if ( bySet && i !== matchedCount ) {
		j = 0;
		while ( (matcher = setMatchers[j++]) ) {
		  matcher( unmatched, setMatched, context, xml );
		}

		if ( seed ) {
		  // Reintegrate element matches to eliminate the need for sorting
		  if ( matchedCount > 0 ) {
			while ( i-- ) {
			  if ( !(unmatched[i] || setMatched[i]) ) {
				setMatched[i] = pop.call( results );
			  }
			}
		  }

		  // Discard index placeholder values to get only actual matches
		  setMatched = condense( setMatched );
		}

		// Add matches to results
		push.apply( results, setMatched );

		// Seedless set matches succeeding multiple successful matchers stipulate sorting
		if ( outermost && !seed && setMatched.length > 0 &&
		  ( matchedCount + setMatchers.length ) > 1 ) {

		  Sizzle.uniqueSort( results );
		}
	  }

	  // Override manipulation of globals by nested matchers
	  if ( outermost ) {
		dirruns = dirrunsUnique;
		outermostContext = contextBackup;
	  }

	  return unmatched;
	};

  return bySet ?
	markFunction( superMatcher ) :
	superMatcher;
}

compile = Sizzle.compile = function( selector, group /* Internal Use Only */ ) {
  var i,
	setMatchers = [],
	elementMatchers = [],
	cached = compilerCache[ selector + " " ];

  if ( !cached ) {
	// Generate a function of recursive functions that can be used to check each element
	if ( !group ) {
	  group = tokenize( selector );
	}
	i = group.length;
	while ( i-- ) {
	  cached = matcherFromTokens( group[i] );
	  if ( cached[ expando ] ) {
		setMatchers.push( cached );
	  } else {
		elementMatchers.push( cached );
	  }
	}

	// Cache the compiled function
	cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
  }
  return cached;
};

function multipleContexts( selector, contexts, results ) {
  var i = 0,
	len = contexts.length;
  for ( ; i < len; i++ ) {
	Sizzle( selector, contexts[i], results );
  }
  return results;
}

function select( selector, context, results, seed ) {
  var i, tokens, token, type, find,
	match = tokenize( selector );

  if ( !seed ) {
	// Try to minimize operations if there is only one group
	if ( match.length === 1 ) {

	  // Take a shortcut and set the context if the root selector is an ID
	  tokens = match[0] = match[0].slice( 0 );
	  if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
		  context.nodeType === 9 && !documentIsXML &&
		  Expr.relative[ tokens[1].type ] ) {

		context = Expr.find["ID"]( token.matches[0].replace( runescape, funescape ), context )[0];
		if ( !context ) {
		  return results;
		}

		selector = selector.slice( tokens.shift().value.length );
	  }

	  // Fetch a seed set for right-to-left matching
	  i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
	  while ( i-- ) {
		token = tokens[i];

		// Abort if we hit a combinator
		if ( Expr.relative[ (type = token.type) ] ) {
		  break;
		}
		if ( (find = Expr.find[ type ]) ) {
		  // Search, expanding context for leading sibling combinators
		  if ( (seed = find(
			token.matches[0].replace( runescape, funescape ),
			rsibling.test( tokens[0].type ) && context.parentNode || context
		  )) ) {

			// If seed is empty or no tokens remain, we can return early
			tokens.splice( i, 1 );
			selector = seed.length && toSelector( tokens );
			if ( !selector ) {
			  push.apply( results, slice.call( seed, 0 ) );
			  return results;
			}

			break;
		  }
		}
	  }
	}
  }

  // Compile and execute a filtering function
  // Provide `match` to avoid retokenization if we modified the selector above
  compile( selector, match )(
	seed,
	context,
	documentIsXML,
	results,
	rsibling.test( selector )
  );
  return results;
}

// Deprecated
Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Easy API for creating new setFilters
function setFilters() {}
Expr.filters = setFilters.prototype = Expr.pseudos;
Expr.setFilters = new setFilters();

// Initialize with the default document
setDocument();

// Override sizzle attribute retrieval
Sizzle.attr = jQuery.attr;
jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;


})( window );
var runtil = /Until$/,
  rparentsprev = /^(?:parents|prev(?:Until|All))/,
  isSimple = /^.[^:#\[\.,]*$/,
  rneedsContext = jQuery.expr.match.needsContext,
  // methods guaranteed to produce a unique set when starting from a unique set
  guaranteedUnique = {
	children: true,
	contents: true,
	next: true,
	prev: true
  };

jQuery.fn.extend({
  find: function( selector ) {
	var i, ret, self,
	  len = this.length;

	if ( typeof selector !== "string" ) {
	  self = this;
	  return this.pushStack( jQuery( selector ).filter(function() {
		for ( i = 0; i < len; i++ ) {
		  if ( jQuery.contains( self[ i ], this ) ) {
			return true;
		  }
		}
	  }) );
	}

	ret = [];
	for ( i = 0; i < len; i++ ) {
	  jQuery.find( selector, this[ i ], ret );
	}

	// Needed because $( selector, context ) becomes $( context ).find( selector )
	ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
	ret.selector = ( this.selector ? this.selector + " " : "" ) + selector;
	return ret;
  },

  has: function( target ) {
	var i,
	  targets = jQuery( target, this ),
	  len = targets.length;

	return this.filter(function() {
	  for ( i = 0; i < len; i++ ) {
		if ( jQuery.contains( this, targets[i] ) ) {
		  return true;
		}
	  }
	});
  },

  not: function( selector ) {
	return this.pushStack( winnow(this, selector, false) );
  },

  filter: function( selector ) {
	return this.pushStack( winnow(this, selector, true) );
  },

  is: function( selector ) {
	return !!selector && (
	  typeof selector === "string" ?
		// If this is a positional/relative selector, check membership in the returned set
		// so $("p:first").is("p:last") won't return true for a doc with two "p".
		rneedsContext.test( selector ) ?
		  jQuery( selector, this.context ).index( this[0] ) >= 0 :
		  jQuery.filter( selector, this ).length > 0 :
		this.filter( selector ).length > 0 );
  },

  closest: function( selectors, context ) {
	var cur,
	  i = 0,
	  l = this.length,
	  ret = [],
	  pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
		jQuery( selectors, context || this.context ) :
		0;

	for ( ; i < l; i++ ) {
	  cur = this[i];

	  while ( cur && cur.ownerDocument && cur !== context && cur.nodeType !== 11 ) {
		if ( pos ? pos.index(cur) > -1 : jQuery.find.matchesSelector(cur, selectors) ) {
		  ret.push( cur );
		  break;
		}
		cur = cur.parentNode;
	  }
	}

	return this.pushStack( ret.length > 1 ? jQuery.unique( ret ) : ret );
  },

  // Determine the position of an element within
  // the matched set of elements
  index: function( elem ) {

	// No argument, return index in parent
	if ( !elem ) {
	  return ( this[0] && this[0].parentNode ) ? this.first().prevAll().length : -1;
	}

	// index in selector
	if ( typeof elem === "string" ) {
	  return jQuery.inArray( this[0], jQuery( elem ) );
	}

	// Locate the position of the desired element
	return jQuery.inArray(
	  // If it receives a jQuery object, the first element is used
	  elem.jquery ? elem[0] : elem, this );
  },

  add: function( selector, context ) {
	var set = typeof selector === "string" ?
		jQuery( selector, context ) :
		jQuery.makeArray( selector && selector.nodeType ? [ selector ] : selector ),
	  all = jQuery.merge( this.get(), set );

	return this.pushStack( jQuery.unique(all) );
  },

  addBack: function( selector ) {
	return this.add( selector == null ?
	  this.prevObject : this.prevObject.filter(selector)
	);
  }
});

jQuery.fn.andSelf = jQuery.fn.addBack;

function sibling( cur, dir ) {
  do {
	cur = cur[ dir ];
  } while ( cur && cur.nodeType !== 1 );

  return cur;
}

jQuery.each({
  parent: function( elem ) {
	var parent = elem.parentNode;
	return parent && parent.nodeType !== 11 ? parent : null;
  },
  parents: function( elem ) {
	return jQuery.dir( elem, "parentNode" );
  },
  parentsUntil: function( elem, i, until ) {
	return jQuery.dir( elem, "parentNode", until );
  },
  next: function( elem ) {
	return sibling( elem, "nextSibling" );
  },
  prev: function( elem ) {
	return sibling( elem, "previousSibling" );
  },
  nextAll: function( elem ) {
	return jQuery.dir( elem, "nextSibling" );
  },
  prevAll: function( elem ) {
	return jQuery.dir( elem, "previousSibling" );
  },
  nextUntil: function( elem, i, until ) {
	return jQuery.dir( elem, "nextSibling", until );
  },
  prevUntil: function( elem, i, until ) {
	return jQuery.dir( elem, "previousSibling", until );
  },
  siblings: function( elem ) {
	return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
  },
  children: function( elem ) {
	return jQuery.sibling( elem.firstChild );
  },
  contents: function( elem ) {
	return jQuery.nodeName( elem, "iframe" ) ?
	  elem.contentDocument || elem.contentWindow.document :
	  jQuery.merge( [], elem.childNodes );
  }
}, function( name, fn ) {
  jQuery.fn[ name ] = function( until, selector ) {
	var ret = jQuery.map( this, fn, until );

	if ( !runtil.test( name ) ) {
	  selector = until;
	}

	if ( selector && typeof selector === "string" ) {
	  ret = jQuery.filter( selector, ret );
	}

	ret = this.length > 1 && !guaranteedUnique[ name ] ? jQuery.unique( ret ) : ret;

	if ( this.length > 1 && rparentsprev.test( name ) ) {
	  ret = ret.reverse();
	}

	return this.pushStack( ret );
  };
});

jQuery.extend({
  filter: function( expr, elems, not ) {
	if ( not ) {
	  expr = ":not(" + expr + ")";
	}

	return elems.length === 1 ?
	  jQuery.find.matchesSelector(elems[0], expr) ? [ elems[0] ] : [] :
	  jQuery.find.matches(expr, elems);
  },

  dir: function( elem, dir, until ) {
	var matched = [],
	  cur = elem[ dir ];

	while ( cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery( cur ).is( until )) ) {
	  if ( cur.nodeType === 1 ) {
		matched.push( cur );
	  }
	  cur = cur[dir];
	}
	return matched;
  },

  sibling: function( n, elem ) {
	var r = [];

	for ( ; n; n = n.nextSibling ) {
	  if ( n.nodeType === 1 && n !== elem ) {
		r.push( n );
	  }
	}

	return r;
  }
});

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, keep ) {

  // Can't pass null or undefined to indexOf in Firefox 4
  // Set to 0 to skip string check
  qualifier = qualifier || 0;

  if ( jQuery.isFunction( qualifier ) ) {
	return jQuery.grep(elements, function( elem, i ) {
	  var retVal = !!qualifier.call( elem, i, elem );
	  return retVal === keep;
	});

  } else if ( qualifier.nodeType ) {
	return jQuery.grep(elements, function( elem ) {
	  return ( elem === qualifier ) === keep;
	});

  } else if ( typeof qualifier === "string" ) {
	var filtered = jQuery.grep(elements, function( elem ) {
	  return elem.nodeType === 1;
	});

	if ( isSimple.test( qualifier ) ) {
	  return jQuery.filter(qualifier, filtered, !keep);
	} else {
	  qualifier = jQuery.filter( qualifier, filtered );
	}
  }

  return jQuery.grep(elements, function( elem ) {
	return ( jQuery.inArray( elem, qualifier ) >= 0 ) === keep;
  });
}
function createSafeFragment( document ) {
  var list = nodeNames.split( "|" ),
	safeFrag = document.createDocumentFragment();

  if ( safeFrag.createElement ) {
	while ( list.length ) {
	  safeFrag.createElement(
		list.pop()
	  );
	}
  }
  return safeFrag;
}

var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
	"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
  rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
  rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
  rleadingWhitespace = /^\s+/,
  rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
  rtagName = /<([\w:]+)/,
  rtbody = /<tbody/i,
  rhtml = /<|&#?\w+;/,
  rnoInnerhtml = /<(?:script|style|link)/i,
  manipulation_rcheckableType = /^(?:checkbox|radio)$/i,
  // checked="checked" or checked
  rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
  rscriptType = /^$|\/(?:java|ecma)script/i,
  rscriptTypeMasked = /^true\/(.*)/,
  rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

  // We have to close these tags to support XHTML (#13200)
  wrapMap = {
	option: [ 1, "<select multiple='multiple'>", "</select>" ],
	legend: [ 1, "<fieldset>", "</fieldset>" ],
	area: [ 1, "<map>", "</map>" ],
	param: [ 1, "<object>", "</object>" ],
	thead: [ 1, "<table>", "</table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	// IE6-8 can't serialize link, script, style, or any html5 (NoScope) tags,
	// unless wrapped in a div with non-breaking characters in front of it.
	_default: jQuery.support.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>"  ]
  },
  safeFragment = createSafeFragment( document ),
  fragmentDiv = safeFragment.appendChild( document.createElement("div") );

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

jQuery.fn.extend({
  text: function( value ) {
	return jQuery.access( this, function( value ) {
	  return value === undefined ?
		jQuery.text( this ) :
		this.empty().append( ( this[0] && this[0].ownerDocument || document ).createTextNode( value ) );
	}, null, value, arguments.length );
  },

  wrapAll: function( html ) {
	if ( jQuery.isFunction( html ) ) {
	  return this.each(function(i) {
		jQuery(this).wrapAll( html.call(this, i) );
	  });
	}

	if ( this[0] ) {
	  // The elements to wrap the target around
	  var wrap = jQuery( html, this[0].ownerDocument ).eq(0).clone(true);

	  if ( this[0].parentNode ) {
		wrap.insertBefore( this[0] );
	  }

	  wrap.map(function() {
		var elem = this;

		while ( elem.firstChild && elem.firstChild.nodeType === 1 ) {
		  elem = elem.firstChild;
		}

		return elem;
	  }).append( this );
	}

	return this;
  },

  wrapInner: function( html ) {
	if ( jQuery.isFunction( html ) ) {
	  return this.each(function(i) {
		jQuery(this).wrapInner( html.call(this, i) );
	  });
	}

	return this.each(function() {
	  var self = jQuery( this ),
		contents = self.contents();

	  if ( contents.length ) {
		contents.wrapAll( html );

	  } else {
		self.append( html );
	  }
	});
  },

  wrap: function( html ) {
	var isFunction = jQuery.isFunction( html );

	return this.each(function(i) {
	  jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
	});
  },

  unwrap: function() {
	return this.parent().each(function() {
	  if ( !jQuery.nodeName( this, "body" ) ) {
		jQuery( this ).replaceWith( this.childNodes );
	  }
	}).end();
  },

  append: function() {
	return this.domManip(arguments, true, function( elem ) {
	  if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
		this.appendChild( elem );
	  }
	});
  },

  prepend: function() {
	return this.domManip(arguments, true, function( elem ) {
	  if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
		this.insertBefore( elem, this.firstChild );
	  }
	});
  },

  before: function() {
	return this.domManip( arguments, false, function( elem ) {
	  if ( this.parentNode ) {
		this.parentNode.insertBefore( elem, this );
	  }
	});
  },

  after: function() {
	return this.domManip( arguments, false, function( elem ) {
	  if ( this.parentNode ) {
		this.parentNode.insertBefore( elem, this.nextSibling );
	  }
	});
  },

  // keepData is for internal use only--do not document
  remove: function( selector, keepData ) {
	var elem,
	  i = 0;

	for ( ; (elem = this[i]) != null; i++ ) {
	  if ( !selector || jQuery.filter( selector, [ elem ] ).length > 0 ) {
		if ( !keepData && elem.nodeType === 1 ) {
		  jQuery.cleanData( getAll( elem ) );
		}

		if ( elem.parentNode ) {
		  if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
			setGlobalEval( getAll( elem, "script" ) );
		  }
		  elem.parentNode.removeChild( elem );
		}
	  }
	}

	return this;
  },

  empty: function() {
	var elem,
	  i = 0;

	for ( ; (elem = this[i]) != null; i++ ) {
	  // Remove element nodes and prevent memory leaks
	  if ( elem.nodeType === 1 ) {
		jQuery.cleanData( getAll( elem, false ) );
	  }

	  // Remove any remaining nodes
	  while ( elem.firstChild ) {
		elem.removeChild( elem.firstChild );
	  }

	  // If this is a select, ensure that it displays empty (#12336)
	  // Support: IE<9
	  if ( elem.options && jQuery.nodeName( elem, "select" ) ) {
		elem.options.length = 0;
	  }
	}

	return this;
  },

  clone: function( dataAndEvents, deepDataAndEvents ) {
	dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
	deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

	return this.map( function () {
	  return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
	});
  },

  html: function( value ) {
	return jQuery.access( this, function( value ) {
	  var elem = this[0] || {},
		i = 0,
		l = this.length;

	  if ( value === undefined ) {
		return elem.nodeType === 1 ?
		  elem.innerHTML.replace( rinlinejQuery, "" ) :
		  undefined;
	  }

	  // See if we can take a shortcut and just use innerHTML
	  if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
		( jQuery.support.htmlSerialize || !rnoshimcache.test( value )  ) &&
		( jQuery.support.leadingWhitespace || !rleadingWhitespace.test( value ) ) &&
		!wrapMap[ ( rtagName.exec( value ) || ["", ""] )[1].toLowerCase() ] ) {

		value = value.replace( rxhtmlTag, "<$1></$2>" );

		try {
		  for (; i < l; i++ ) {
			// Remove element nodes and prevent memory leaks
			elem = this[i] || {};
			if ( elem.nodeType === 1 ) {
			  jQuery.cleanData( getAll( elem, false ) );
			  elem.innerHTML = value;
			}
		  }

		  elem = 0;

		// If using innerHTML throws an exception, use the fallback method
		} catch(e) {}
	  }

	  if ( elem ) {
		this.empty().append( value );
	  }
	}, null, value, arguments.length );
  },

  replaceWith: function( value ) {
	var isFunc = jQuery.isFunction( value );

	// Make sure that the elements are removed from the DOM before they are inserted
	// this can help fix replacing a parent with child elements
	if ( !isFunc && typeof value !== "string" ) {
	  value = jQuery( value ).not( this ).detach();
	}

	return this.domManip( [ value ], true, function( elem ) {
	  var next = this.nextSibling,
		parent = this.parentNode;

	  if ( parent ) {
		jQuery( this ).remove();
		parent.insertBefore( elem, next );
	  }
	});
  },

  detach: function( selector ) {
	return this.remove( selector, true );
  },

  domManip: function( args, table, callback ) {

	// Flatten any nested arrays
	args = core_concat.apply( [], args );

	var first, node, hasScripts,
	  scripts, doc, fragment,
	  i = 0,
	  l = this.length,
	  set = this,
	  iNoClone = l - 1,
	  value = args[0],
	  isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction || !( l <= 1 || typeof value !== "string" || jQuery.support.checkClone || !rchecked.test( value ) ) ) {
	  return this.each(function( index ) {
		var self = set.eq( index );
		if ( isFunction ) {
		  args[0] = value.call( this, index, table ? self.html() : undefined );
		}
		self.domManip( args, table, callback );
	  });
	}

	if ( l ) {
	  fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
	  first = fragment.firstChild;

	  if ( fragment.childNodes.length === 1 ) {
		fragment = first;
	  }

	  if ( first ) {
		table = table && jQuery.nodeName( first, "tr" );
		scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
		hasScripts = scripts.length;

		// Use the original fragment for the last item instead of the first because it can end up
		// being emptied incorrectly in certain situations (#8070).
		for ( ; i < l; i++ ) {
		  node = fragment;

		  if ( i !== iNoClone ) {
			node = jQuery.clone( node, true, true );

			// Keep references to cloned scripts for later restoration
			if ( hasScripts ) {
			  jQuery.merge( scripts, getAll( node, "script" ) );
			}
		  }

		  callback.call(
			table && jQuery.nodeName( this[i], "table" ) ?
			  findOrAppend( this[i], "tbody" ) :
			  this[i],
			node,
			i
		  );
		}

		if ( hasScripts ) {
		  doc = scripts[ scripts.length - 1 ].ownerDocument;

		  // Reenable scripts
		  jQuery.map( scripts, restoreScript );

		  // Evaluate executable scripts on first document insertion
		  for ( i = 0; i < hasScripts; i++ ) {
			node = scripts[ i ];
			if ( rscriptType.test( node.type || "" ) &&
			  !jQuery._data( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

			  if ( node.src ) {
				// Hope ajax is available...
				jQuery.ajax({
				  url: node.src,
				  type: "GET",
				  dataType: "script",
				  async: false,
				  global: false,
				  "throws": true
				});
			  } else {
				jQuery.globalEval( ( node.text || node.textContent || node.innerHTML || "" ).replace( rcleanScript, "" ) );
			  }
			}
		  }
		}

		// Fix #11809: Avoid leaking memory
		fragment = first = null;
	  }
	}

	return this;
  }
});

function findOrAppend( elem, tag ) {
  return elem.getElementsByTagName( tag )[0] || elem.appendChild( elem.ownerDocument.createElement( tag ) );
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
  var attr = elem.getAttributeNode("type");
  elem.type = ( attr && attr.specified ) + "/" + elem.type;
  return elem;
}
function restoreScript( elem ) {
  var match = rscriptTypeMasked.exec( elem.type );
  if ( match ) {
	elem.type = match[1];
  } else {
	elem.removeAttribute("type");
  }
  return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
  var elem,
	i = 0;
  for ( ; (elem = elems[i]) != null; i++ ) {
	jQuery._data( elem, "globalEval", !refElements || jQuery._data( refElements[i], "globalEval" ) );
  }
}

function cloneCopyEvent( src, dest ) {

  if ( dest.nodeType !== 1 || !jQuery.hasData( src ) ) {
	return;
  }

  var type, i, l,
	oldData = jQuery._data( src ),
	curData = jQuery._data( dest, oldData ),
	events = oldData.events;

  if ( events ) {
	delete curData.handle;
	curData.events = {};

	for ( type in events ) {
	  for ( i = 0, l = events[ type ].length; i < l; i++ ) {
		jQuery.event.add( dest, type, events[ type ][ i ] );
	  }
	}
  }

  // make the cloned public data object a copy from the original
  if ( curData.data ) {
	curData.data = jQuery.extend( {}, curData.data );
  }
}

function fixCloneNodeIssues( src, dest ) {
  var nodeName, e, data;

  // We do not need to do anything for non-Elements
  if ( dest.nodeType !== 1 ) {
	return;
  }

  nodeName = dest.nodeName.toLowerCase();

  // IE6-8 copies events bound via attachEvent when using cloneNode.
  if ( !jQuery.support.noCloneEvent && dest[ jQuery.expando ] ) {
	data = jQuery._data( dest );

	for ( e in data.events ) {
	  jQuery.removeEvent( dest, e, data.handle );
	}

	// Event data gets referenced instead of copied if the expando gets copied too
	dest.removeAttribute( jQuery.expando );
  }

  // IE blanks contents when cloning scripts, and tries to evaluate newly-set text
  if ( nodeName === "script" && dest.text !== src.text ) {
	disableScript( dest ).text = src.text;
	restoreScript( dest );

  // IE6-10 improperly clones children of object elements using classid.
  // IE10 throws NoModificationAllowedError if parent is null, #12132.
  } else if ( nodeName === "object" ) {
	if ( dest.parentNode ) {
	  dest.outerHTML = src.outerHTML;
	}

	// This path appears unavoidable for IE9. When cloning an object
	// element in IE9, the outerHTML strategy above is not sufficient.
	// If the src has innerHTML and the destination does not,
	// copy the src.innerHTML into the dest.innerHTML. #10324
	if ( jQuery.support.html5Clone && ( src.innerHTML && !jQuery.trim(dest.innerHTML) ) ) {
	  dest.innerHTML = src.innerHTML;
	}

  } else if ( nodeName === "input" && manipulation_rcheckableType.test( src.type ) ) {
	// IE6-8 fails to persist the checked state of a cloned checkbox
	// or radio button. Worse, IE6-7 fail to give the cloned element
	// a checked appearance if the defaultChecked value isn't also set

	dest.defaultChecked = dest.checked = src.checked;

	// IE6-7 get confused and end up setting the value of a cloned
	// checkbox/radio button to an empty string instead of "on"
	if ( dest.value !== src.value ) {
	  dest.value = src.value;
	}

  // IE6-8 fails to return the selected option to the default selected
  // state when cloning options
  } else if ( nodeName === "option" ) {
	dest.defaultSelected = dest.selected = src.defaultSelected;

  // IE6-8 fails to set the defaultValue to the correct value when
  // cloning other types of input fields
  } else if ( nodeName === "input" || nodeName === "textarea" ) {
	dest.defaultValue = src.defaultValue;
  }
}

jQuery.each({
  appendTo: "append",
  prependTo: "prepend",
  insertBefore: "before",
  insertAfter: "after",
  replaceAll: "replaceWith"
}, function( name, original ) {
  jQuery.fn[ name ] = function( selector ) {
	var elems,
	  i = 0,
	  ret = [],
	  insert = jQuery( selector ),
	  last = insert.length - 1;

	for ( ; i <= last; i++ ) {
	  elems = i === last ? this : this.clone(true);
	  jQuery( insert[i] )[ original ]( elems );

	  // Modern browsers can apply jQuery collections as arrays, but oldIE needs a .get()
	  core_push.apply( ret, elems.get() );
	}

	return this.pushStack( ret );
  };
});

function getAll( context, tag ) {
  var elems, elem,
	i = 0,
	found = typeof context.getElementsByTagName !== core_strundefined ? context.getElementsByTagName( tag || "*" ) :
	  typeof context.querySelectorAll !== core_strundefined ? context.querySelectorAll( tag || "*" ) :
	  undefined;

  if ( !found ) {
	for ( found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++ ) {
	  if ( !tag || jQuery.nodeName( elem, tag ) ) {
		found.push( elem );
	  } else {
		jQuery.merge( found, getAll( elem, tag ) );
	  }
	}
  }

  return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
	jQuery.merge( [ context ], found ) :
	found;
}

// Used in buildFragment, fixes the defaultChecked property
function fixDefaultChecked( elem ) {
  if ( manipulation_rcheckableType.test( elem.type ) ) {
	elem.defaultChecked = elem.checked;
  }
}

jQuery.extend({
  clone: function( elem, dataAndEvents, deepDataAndEvents ) {
	var destElements, node, clone, i, srcElements,
	  inPage = jQuery.contains( elem.ownerDocument, elem );

	if ( jQuery.support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test( "<" + elem.nodeName + ">" ) ) {
	  clone = elem.cloneNode( true );

	// IE<=8 does not properly clone detached, unknown element nodes
	} else {
	  fragmentDiv.innerHTML = elem.outerHTML;
	  fragmentDiv.removeChild( clone = fragmentDiv.firstChild );
	}

	if ( (!jQuery.support.noCloneEvent || !jQuery.support.noCloneChecked) &&
		(elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem) ) {

	  // We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
	  destElements = getAll( clone );
	  srcElements = getAll( elem );

	  // Fix all IE cloning issues
	  for ( i = 0; (node = srcElements[i]) != null; ++i ) {
		// Ensure that the destination node is not null; Fixes #9587
		if ( destElements[i] ) {
		  fixCloneNodeIssues( node, destElements[i] );
		}
	  }
	}

	// Copy the events from the original to the clone
	if ( dataAndEvents ) {
	  if ( deepDataAndEvents ) {
		srcElements = srcElements || getAll( elem );
		destElements = destElements || getAll( clone );

		for ( i = 0; (node = srcElements[i]) != null; i++ ) {
		  cloneCopyEvent( node, destElements[i] );
		}
	  } else {
		cloneCopyEvent( elem, clone );
	  }
	}

	// Preserve script evaluation history
	destElements = getAll( clone, "script" );
	if ( destElements.length > 0 ) {
	  setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
	}

	destElements = srcElements = node = null;

	// Return the cloned set
	return clone;
  },

  buildFragment: function( elems, context, scripts, selection ) {
	var j, elem, contains,
	  tmp, tag, tbody, wrap,
	  l = elems.length,

	  // Ensure a safe fragment
	  safe = createSafeFragment( context ),

	  nodes = [],
	  i = 0;

	for ( ; i < l; i++ ) {
	  elem = elems[ i ];

	  if ( elem || elem === 0 ) {

		// Add nodes directly
		if ( jQuery.type( elem ) === "object" ) {
		  jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

		// Convert non-html into a text node
		} else if ( !rhtml.test( elem ) ) {
		  nodes.push( context.createTextNode( elem ) );

		// Convert html into DOM nodes
		} else {
		  tmp = tmp || safe.appendChild( context.createElement("div") );

		  // Deserialize a standard representation
		  tag = ( rtagName.exec( elem ) || ["", ""] )[1].toLowerCase();
		  wrap = wrapMap[ tag ] || wrapMap._default;

		  tmp.innerHTML = wrap[1] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[2];

		  // Descend through wrappers to the right content
		  j = wrap[0];
		  while ( j-- ) {
			tmp = tmp.lastChild;
		  }

		  // Manually add leading whitespace removed by IE
		  if ( !jQuery.support.leadingWhitespace && rleadingWhitespace.test( elem ) ) {
			nodes.push( context.createTextNode( rleadingWhitespace.exec( elem )[0] ) );
		  }

		  // Remove IE's autoinserted <tbody> from table fragments
		  if ( !jQuery.support.tbody ) {

			// String was a <table>, *may* have spurious <tbody>
			elem = tag === "table" && !rtbody.test( elem ) ?
			  tmp.firstChild :

			  // String was a bare <thead> or <tfoot>
			  wrap[1] === "<table>" && !rtbody.test( elem ) ?
				tmp :
				0;

			j = elem && elem.childNodes.length;
			while ( j-- ) {
			  if ( jQuery.nodeName( (tbody = elem.childNodes[j]), "tbody" ) && !tbody.childNodes.length ) {
				elem.removeChild( tbody );
			  }
			}
		  }

		  jQuery.merge( nodes, tmp.childNodes );

		  // Fix #12392 for WebKit and IE > 9
		  tmp.textContent = "";

		  // Fix #12392 for oldIE
		  while ( tmp.firstChild ) {
			tmp.removeChild( tmp.firstChild );
		  }

		  // Remember the top-level container for proper cleanup
		  tmp = safe.lastChild;
		}
	  }
	}

	// Fix #11356: Clear elements from fragment
	if ( tmp ) {
	  safe.removeChild( tmp );
	}

	// Reset defaultChecked for any radios and checkboxes
	// about to be appended to the DOM in IE 6/7 (#8060)
	if ( !jQuery.support.appendChecked ) {
	  jQuery.grep( getAll( nodes, "input" ), fixDefaultChecked );
	}

	i = 0;
	while ( (elem = nodes[ i++ ]) ) {

	  // #4087 - If origin and destination elements are the same, and this is
	  // that element, do not do anything
	  if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
		continue;
	  }

	  contains = jQuery.contains( elem.ownerDocument, elem );

	  // Append to fragment
	  tmp = getAll( safe.appendChild( elem ), "script" );

	  // Preserve script evaluation history
	  if ( contains ) {
		setGlobalEval( tmp );
	  }

	  // Capture executables
	  if ( scripts ) {
		j = 0;
		while ( (elem = tmp[ j++ ]) ) {
		  if ( rscriptType.test( elem.type || "" ) ) {
			scripts.push( elem );
		  }
		}
	  }
	}

	tmp = null;

	return safe;
  },

  cleanData: function( elems, /* internal */ acceptData ) {
	var elem, type, id, data,
	  i = 0,
	  internalKey = jQuery.expando,
	  cache = jQuery.cache,
	  deleteExpando = jQuery.support.deleteExpando,
	  special = jQuery.event.special;

	for ( ; (elem = elems[i]) != null; i++ ) {

	  if ( acceptData || jQuery.acceptData( elem ) ) {

		id = elem[ internalKey ];
		data = id && cache[ id ];

		if ( data ) {
		  if ( data.events ) {
			for ( type in data.events ) {
			  if ( special[ type ] ) {
				jQuery.event.remove( elem, type );

			  // This is a shortcut to avoid jQuery.event.remove's overhead
			  } else {
				jQuery.removeEvent( elem, type, data.handle );
			  }
			}
		  }

		  // Remove cache only if it was not already removed by jQuery.event.remove
		  if ( cache[ id ] ) {

			delete cache[ id ];

			// IE does not allow us to delete expando properties from nodes,
			// nor does it have a removeAttribute function on Document nodes;
			// we must handle all of these cases
			if ( deleteExpando ) {
			  delete elem[ internalKey ];

			} else if ( typeof elem.removeAttribute !== core_strundefined ) {
			  elem.removeAttribute( internalKey );

			} else {
			  elem[ internalKey ] = null;
			}

			core_deletedIds.push( id );
		  }
		}
	  }
	}
  }
});
var iframe, getStyles, curCSS,
  ralpha = /alpha\([^)]*\)/i,
  ropacity = /opacity\s*=\s*([^)]*)/,
  rposition = /^(top|right|bottom|left)$/,
  // swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
  // see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
  rdisplayswap = /^(none|table(?!-c[ea]).+)/,
  rmargin = /^margin/,
  rnumsplit = new RegExp( "^(" + core_pnum + ")(.*)$", "i" ),
  rnumnonpx = new RegExp( "^(" + core_pnum + ")(?!px)[a-z%]+$", "i" ),
  rrelNum = new RegExp( "^([+-])=(" + core_pnum + ")", "i" ),
  elemdisplay = { BODY: "block" },

  cssShow = { position: "absolute", visibility: "hidden", display: "block" },
  cssNormalTransform = {
	letterSpacing: 0,
	fontWeight: 400
  },

  cssExpand = [ "Top", "Right", "Bottom", "Left" ],
  cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];

// return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

  // shortcut for names that are not vendor prefixed
  if ( name in style ) {
	return name;
  }

  // check for vendor prefixed names
  var capName = name.charAt(0).toUpperCase() + name.slice(1),
	origName = name,
	i = cssPrefixes.length;

  while ( i-- ) {
	name = cssPrefixes[ i ] + capName;
	if ( name in style ) {
	  return name;
	}
  }

  return origName;
}

function isHidden( elem, el ) {
  // isHidden might be called from jQuery#filter function;
  // in that case, element will be second argument
  elem = el || elem;
  return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
}

function showHide( elements, show ) {
  var display, elem, hidden,
	values = [],
	index = 0,
	length = elements.length;

  for ( ; index < length; index++ ) {
	elem = elements[ index ];
	if ( !elem.style ) {
	  continue;
	}

	values[ index ] = jQuery._data( elem, "olddisplay" );
	display = elem.style.display;
	if ( show ) {
	  // Reset the inline display of this element to learn if it is
	  // being hidden by cascaded rules or not
	  if ( !values[ index ] && display === "none" ) {
		elem.style.display = "";
	  }

	  // Set elements which have been overridden with display: none
	  // in a stylesheet to whatever the default browser style is
	  // for such an element
	  if ( elem.style.display === "" && isHidden( elem ) ) {
		values[ index ] = jQuery._data( elem, "olddisplay", css_defaultDisplay(elem.nodeName) );
	  }
	} else {

	  if ( !values[ index ] ) {
		hidden = isHidden( elem );

		if ( display && display !== "none" || !hidden ) {
		  jQuery._data( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
		}
	  }
	}
  }

  // Set the display of most of the elements in a second loop
  // to avoid the constant reflow
  for ( index = 0; index < length; index++ ) {
	elem = elements[ index ];
	if ( !elem.style ) {
	  continue;
	}
	if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
	  elem.style.display = show ? values[ index ] || "" : "none";
	}
  }

  return elements;
}

jQuery.fn.extend({
  css: function( name, value ) {
	return jQuery.access( this, function( elem, name, value ) {
	  var len, styles,
		map = {},
		i = 0;

	  if ( jQuery.isArray( name ) ) {
		styles = getStyles( elem );
		len = name.length;

		for ( ; i < len; i++ ) {
		  map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
		}

		return map;
	  }

	  return value !== undefined ?
		jQuery.style( elem, name, value ) :
		jQuery.css( elem, name );
	}, name, value, arguments.length > 1 );
  },
  show: function() {
	return showHide( this, true );
  },
  hide: function() {
	return showHide( this );
  },
  toggle: function( state ) {
	var bool = typeof state === "boolean";

	return this.each(function() {
	  if ( bool ? state : isHidden( this ) ) {
		jQuery( this ).show();
	  } else {
		jQuery( this ).hide();
	  }
	});
  }
});

jQuery.extend({
  // Add in style property hooks for overriding the default
  // behavior of getting and setting a style property
  cssHooks: {
	opacity: {
	  get: function( elem, computed ) {
		if ( computed ) {
		  // We should always get a number back from opacity
		  var ret = curCSS( elem, "opacity" );
		  return ret === "" ? "1" : ret;
		}
	  }
	}
  },

  // Exclude the following css properties to add px
  cssNumber: {
	"columnCount": true,
	"fillOpacity": true,
	"fontWeight": true,
	"lineHeight": true,
	"opacity": true,
	"orphans": true,
	"widows": true,
	"zIndex": true,
	"zoom": true
  },

  // Add in properties whose names you wish to fix before
  // setting or getting the value
  cssProps: {
	// normalize float css property
	"float": jQuery.support.cssFloat ? "cssFloat" : "styleFloat"
  },

  // Get and set the style property on a DOM Node
  style: function( elem, name, value, extra ) {
	// Don't set styles on text and comment nodes
	if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
	  return;
	}

	// Make sure that we're working with the right name
	var ret, type, hooks,
	  origName = jQuery.camelCase( name ),
	  style = elem.style;

	name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

	// gets hook for the prefixed version
	// followed by the unprefixed version
	hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

	// Check if we're setting a value
	if ( value !== undefined ) {
	  type = typeof value;

	  // convert relative number strings (+= or -=) to relative numbers. #7345
	  if ( type === "string" && (ret = rrelNum.exec( value )) ) {
		value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
		// Fixes bug #9237
		type = "number";
	  }

	  // Make sure that NaN and null values aren't set. See: #7116
	  if ( value == null || type === "number" && isNaN( value ) ) {
		return;
	  }

	  // If a number was passed in, add 'px' to the (except for certain CSS properties)
	  if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
		value += "px";
	  }

	  // Fixes #8908, it can be done more correctly by specifing setters in cssHooks,
	  // but it would mean to define eight (for every problematic property) identical functions
	  if ( !jQuery.support.clearCloneStyle && value === "" && name.indexOf("background") === 0 ) {
		style[ name ] = "inherit";
	  }

	  // If a hook was provided, use that value, otherwise just set the specified value
	  if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {

		// Wrapped to prevent IE from throwing errors when 'invalid' values are provided
		// Fixes bug #5509
		try {
		  style[ name ] = value;
		} catch(e) {}
	  }

	} else {
	  // If a hook was provided get the non-computed value from there
	  if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
		return ret;
	  }

	  // Otherwise just get the value from the style object
	  return style[ name ];
	}
  },

  css: function( elem, name, extra, styles ) {
	var num, val, hooks,
	  origName = jQuery.camelCase( name );

	// Make sure that we're working with the right name
	name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

	// gets hook for the prefixed version
	// followed by the unprefixed version
	hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

	// If a hook was provided get the computed value from there
	if ( hooks && "get" in hooks ) {
	  val = hooks.get( elem, true, extra );
	}

	// Otherwise, if a way to get the computed value exists, use that
	if ( val === undefined ) {
	  val = curCSS( elem, name, styles );
	}

	//convert "normal" to computed value
	if ( val === "normal" && name in cssNormalTransform ) {
	  val = cssNormalTransform[ name ];
	}

	// Return, converting to number if forced or a qualifier was provided and val looks numeric
	if ( extra === "" || extra ) {
	  num = parseFloat( val );
	  return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
	}
	return val;
  },

  // A method for quickly swapping in/out CSS properties to get correct calculations
  swap: function( elem, options, callback, args ) {
	var ret, name,
	  old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
	  old[ name ] = elem.style[ name ];
	  elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
	  elem.style[ name ] = old[ name ];
	}

	return ret;
  }
});

// NOTE: we've included the "window" in window.getComputedStyle
// because jsdom on node.js will break without it.
if ( window.getComputedStyle ) {
  getStyles = function( elem ) {
	return window.getComputedStyle( elem, null );
  };

  curCSS = function( elem, name, _computed ) {
	var width, minWidth, maxWidth,
	  computed = _computed || getStyles( elem ),

	  // getPropertyValue is only needed for .css('filter') in IE9, see #12537
	  ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined,
	  style = elem.style;

	if ( computed ) {

	  if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
		ret = jQuery.style( elem, name );
	  }

	  // A tribute to the "awesome hack by Dean Edwards"
	  // Chrome < 17 and Safari 5.0 uses "computed value" instead of "used value" for margin-right
	  // Safari 5.1.7 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
	  // this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
	  if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

		// Remember the original values
		width = style.width;
		minWidth = style.minWidth;
		maxWidth = style.maxWidth;

		// Put in the new values to get a computed value out
		style.minWidth = style.maxWidth = style.width = ret;
		ret = computed.width;

		// Revert the changed values
		style.width = width;
		style.minWidth = minWidth;
		style.maxWidth = maxWidth;
	  }
	}

	return ret;
  };
} else if ( document.documentElement.currentStyle ) {
  getStyles = function( elem ) {
	return elem.currentStyle;
  };

  curCSS = function( elem, name, _computed ) {
	var left, rs, rsLeft,
	  computed = _computed || getStyles( elem ),
	  ret = computed ? computed[ name ] : undefined,
	  style = elem.style;

	// Avoid setting ret to empty string here
	// so we don't default to auto
	if ( ret == null && style && style[ name ] ) {
	  ret = style[ name ];
	}

	// From the awesome hack by Dean Edwards
	// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

	// If we're not dealing with a regular pixel number
	// but a number that has a weird ending, we need to convert it to pixels
	// but not position css attributes, as those are proportional to the parent element instead
	// and we can't measure the parent instead because it might trigger a "stacking dolls" problem
	if ( rnumnonpx.test( ret ) && !rposition.test( name ) ) {

	  // Remember the original values
	  left = style.left;
	  rs = elem.runtimeStyle;
	  rsLeft = rs && rs.left;

	  // Put in the new values to get a computed value out
	  if ( rsLeft ) {
		rs.left = elem.currentStyle.left;
	  }
	  style.left = name === "fontSize" ? "1em" : ret;
	  ret = style.pixelLeft + "px";

	  // Revert the changed values
	  style.left = left;
	  if ( rsLeft ) {
		rs.left = rsLeft;
	  }
	}

	return ret === "" ? "auto" : ret;
  };
}

function setPositiveNumber( elem, value, subtract ) {
  var matches = rnumsplit.exec( value );
  return matches ?
	// Guard against undefined "subtract", e.g., when used as in cssHooks
	Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
	value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
  var i = extra === ( isBorderBox ? "border" : "content" ) ?
	// If we already have the right measurement, avoid augmentation
	4 :
	// Otherwise initialize for horizontal or vertical properties
	name === "width" ? 1 : 0,

	val = 0;

  for ( ; i < 4; i += 2 ) {
	// both box models exclude margin, so add it if we want it
	if ( extra === "margin" ) {
	  val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
	}

	if ( isBorderBox ) {
	  // border-box includes padding, so remove it if we want content
	  if ( extra === "content" ) {
		val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
	  }

	  // at this point, extra isn't border nor margin, so remove border
	  if ( extra !== "margin" ) {
		val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
	  }
	} else {
	  // at this point, extra isn't content, so add padding
	  val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

	  // at this point, extra isn't content nor padding, so add border
	  if ( extra !== "padding" ) {
		val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
	  }
	}
  }

  return val;
}

function getWidthOrHeight( elem, name, extra ) {

  // Start with offset property, which is equivalent to the border-box value
  var valueIsBorderBox = true,
	val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
	styles = getStyles( elem ),
	isBorderBox = jQuery.support.boxSizing && jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

  // some non-html elements return undefined for offsetWidth, so check for null/undefined
  // svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
  // MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
  if ( val <= 0 || val == null ) {
	// Fall back to computed then uncomputed css if necessary
	val = curCSS( elem, name, styles );
	if ( val < 0 || val == null ) {
	  val = elem.style[ name ];
	}

	// Computed unit is not pixels. Stop here and return.
	if ( rnumnonpx.test(val) ) {
	  return val;
	}

	// we need the check for style in case a browser which returns unreliable values
	// for getComputedStyle silently falls back to the reliable elem.style
	valueIsBorderBox = isBorderBox && ( jQuery.support.boxSizingReliable || val === elem.style[ name ] );

	// Normalize "", auto, and prepare for extra
	val = parseFloat( val ) || 0;
  }

  // use the active box-sizing model to add/subtract irrelevant styles
  return ( val +
	augmentWidthOrHeight(
	  elem,
	  name,
	  extra || ( isBorderBox ? "border" : "content" ),
	  valueIsBorderBox,
	  styles
	)
  ) + "px";
}

// Try to determine the default display value of an element
function css_defaultDisplay( nodeName ) {
  var doc = document,
	display = elemdisplay[ nodeName ];

  if ( !display ) {
	display = actualDisplay( nodeName, doc );

	// If the simple way fails, read from inside an iframe
	if ( display === "none" || !display ) {
	  // Use the already-created iframe if possible
	  iframe = ( iframe ||
		jQuery("<iframe frameborder='0' width='0' height='0'/>")
		.css( "cssText", "display:block !important" )
	  ).appendTo( doc.documentElement );

	  // Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
	  doc = ( iframe[0].contentWindow || iframe[0].contentDocument ).document;
	  doc.write("<!doctype html><html><body>");
	  doc.close();

	  display = actualDisplay( nodeName, doc );
	  iframe.detach();
	}

	// Store the correct default display
	elemdisplay[ nodeName ] = display;
  }

  return display;
}

// Called ONLY from within css_defaultDisplay
function actualDisplay( name, doc ) {
  var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),
	display = jQuery.css( elem[0], "display" );
  elem.remove();
  return display;
}

jQuery.each([ "height", "width" ], function( i, name ) {
  jQuery.cssHooks[ name ] = {
	get: function( elem, computed, extra ) {
	  if ( computed ) {
		// certain elements can have dimension info if we invisibly show them
		// however, it must have a current display style that would benefit from this
		return elem.offsetWidth === 0 && rdisplayswap.test( jQuery.css( elem, "display" ) ) ?
		  jQuery.swap( elem, cssShow, function() {
			return getWidthOrHeight( elem, name, extra );
		  }) :
		  getWidthOrHeight( elem, name, extra );
	  }
	},

	set: function( elem, value, extra ) {
	  var styles = extra && getStyles( elem );
	  return setPositiveNumber( elem, value, extra ?
		augmentWidthOrHeight(
		  elem,
		  name,
		  extra,
		  jQuery.support.boxSizing && jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		  styles
		) : 0
	  );
	}
  };
});

if ( !jQuery.support.opacity ) {
  jQuery.cssHooks.opacity = {
	get: function( elem, computed ) {
	  // IE uses filters for opacity
	  return ropacity.test( (computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "" ) ?
		( 0.01 * parseFloat( RegExp.$1 ) ) + "" :
		computed ? "1" : "";
	},

	set: function( elem, value ) {
	  var style = elem.style,
		currentStyle = elem.currentStyle,
		opacity = jQuery.isNumeric( value ) ? "alpha(opacity=" + value * 100 + ")" : "",
		filter = currentStyle && currentStyle.filter || style.filter || "";

	  // IE has trouble with opacity if it does not have layout
	  // Force it by setting the zoom level
	  style.zoom = 1;

	  // if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
	  // if value === "", then remove inline opacity #12685
	  if ( ( value >= 1 || value === "" ) &&
		  jQuery.trim( filter.replace( ralpha, "" ) ) === "" &&
		  style.removeAttribute ) {

		// Setting style.filter to null, "" & " " still leave "filter:" in the cssText
		// if "filter:" is present at all, clearType is disabled, we want to avoid this
		// style.removeAttribute is IE Only, but so apparently is this code path...
		style.removeAttribute( "filter" );

		// if there is no filter style applied in a css rule or unset inline opacity, we are done
		if ( value === "" || currentStyle && !currentStyle.filter ) {
		  return;
		}
	  }

	  // otherwise, set new filter values
	  style.filter = ralpha.test( filter ) ?
		filter.replace( ralpha, opacity ) :
		filter + " " + opacity;
	}
  };
}

// These hooks cannot be added until DOM ready because the support test
// for it is not run until after DOM ready
jQuery(function() {
  if ( !jQuery.support.reliableMarginRight ) {
	jQuery.cssHooks.marginRight = {
	  get: function( elem, computed ) {
		if ( computed ) {
		  // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
		  // Work around by temporarily setting element display to inline-block
		  return jQuery.swap( elem, { "display": "inline-block" },
			curCSS, [ elem, "marginRight" ] );
		}
	  }
	};
  }

  // Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
  // getComputedStyle returns percent when specified for top/left/bottom/right
  // rather than make the css module depend on the offset module, we just check for it here
  if ( !jQuery.support.pixelPosition && jQuery.fn.position ) {
	jQuery.each( [ "top", "left" ], function( i, prop ) {
	  jQuery.cssHooks[ prop ] = {
		get: function( elem, computed ) {
		  if ( computed ) {
			computed = curCSS( elem, prop );
			// if curCSS returns percentage, fallback to offset
			return rnumnonpx.test( computed ) ?
			  jQuery( elem ).position()[ prop ] + "px" :
			  computed;
		  }
		}
	  };
	});
  }

});

if ( jQuery.expr && jQuery.expr.filters ) {
  jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0 ||
	  (!jQuery.support.reliableHiddenOffsets && ((elem.style && elem.style.display) || jQuery.css( elem, "display" )) === "none");
  };

  jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
  };
}

// These hooks are used by animate to expand properties
jQuery.each({
  margin: "",
  padding: "",
  border: "Width"
}, function( prefix, suffix ) {
  jQuery.cssHooks[ prefix + suffix ] = {
	expand: function( value ) {
	  var i = 0,
		expanded = {},

		// assumes a single number if not a string
		parts = typeof value === "string" ? value.split(" ") : [ value ];

	  for ( ; i < 4; i++ ) {
		expanded[ prefix + cssExpand[ i ] + suffix ] =
		  parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
	  }

	  return expanded;
	}
  };

  if ( !rmargin.test( prefix ) ) {
	jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
  }
});
var r20 = /%20/g,
  rbracket = /\[\]$/,
  rCRLF = /\r?\n/g,
  rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
  rsubmittable = /^(?:input|select|textarea|keygen)/i;

jQuery.fn.extend({
  serialize: function() {
	return jQuery.param( this.serializeArray() );
  },
  serializeArray: function() {
	return this.map(function(){
	  // Can add propHook for "elements" to filter or add form elements
	  var elements = jQuery.prop( this, "elements" );
	  return elements ? jQuery.makeArray( elements ) : this;
	})
	.filter(function(){
	  var type = this.type;
	  // Use .is(":disabled") so that fieldset[disabled] works
	  return this.name && !jQuery( this ).is( ":disabled" ) &&
		rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
		( this.checked || !manipulation_rcheckableType.test( type ) );
	})
	.map(function( i, elem ){
	  var val = jQuery( this ).val();

	  return val == null ?
		null :
		jQuery.isArray( val ) ?
		  jQuery.map( val, function( val ){
			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		  }) :
		  { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
	}).get();
  }
});

//Serialize an array of form elements or a set of
//key/values into a query string
jQuery.param = function( a, traditional ) {
  var prefix,
	s = [],
	add = function( key, value ) {
	  // If value is a function, invoke it and return its value
	  value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
	  s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
	};

  // Set traditional to true for jQuery <= 1.3.2 behavior.
  if ( traditional === undefined ) {
	traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
  }

  // If an array was passed in, assume that it is an array of form elements.
  if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
	// Serialize the form elements
	jQuery.each( a, function() {
	  add( this.name, this.value );
	});

  } else {
	// If traditional, encode the "old" way (the way 1.3.2 or older
	// did it), otherwise encode params recursively.
	for ( prefix in a ) {
	  buildParams( prefix, a[ prefix ], traditional, add );
	}
  }

  // Return the resulting serialization
  return s.join( "&" ).replace( r20, "+" );
};

function buildParams( prefix, obj, traditional, add ) {
  var name;

  if ( jQuery.isArray( obj ) ) {
	// Serialize array item.
	jQuery.each( obj, function( i, v ) {
	  if ( traditional || rbracket.test( prefix ) ) {
		// Treat each array item as a scalar.
		add( prefix, v );

	  } else {
		// Item is non-scalar (array or object), encode its numeric index.
		buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
	  }
	});

  } else if ( !traditional && jQuery.type( obj ) === "object" ) {
	// Serialize object item.
	for ( name in obj ) {
	  buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
	}

  } else {
	// Serialize scalar item.
	add( prefix, obj );
  }
}
jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
  "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
  "change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

  // Handle event binding
  jQuery.fn[ name ] = function( data, fn ) {
	return arguments.length > 0 ?
	  this.on( name, null, data, fn ) :
	  this.trigger( name );
  };
});

jQuery.fn.hover = function( fnOver, fnOut ) {
  return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
};
var
  // Document location
  ajaxLocParts,
  ajaxLocation,
  ajax_nonce = jQuery.now(),

  ajax_rquery = /\?/,
  rhash = /#.*$/,
  rts = /([?&])_=[^&]*/,
  rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, // IE leaves an \r character at EOL
  // #7653, #8125, #8152: local protocol detection
  rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
  rnoContent = /^(?:GET|HEAD)$/,
  rprotocol = /^\/\//,
  rurl = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,

  // Keep a copy of the old load method
  _load = jQuery.fn.load,

  /* Prefilters
   * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
   * 2) These are called:
   *    - BEFORE asking for a transport
   *    - AFTER param serialization (s.data is a string if s.processData is true)
   * 3) key is the dataType
   * 4) the catchall symbol "*" can be used
   * 5) execution will start with transport dataType and THEN continue down to "*" if needed
   */
  prefilters = {},

  /* Transports bindings
   * 1) key is the dataType
   * 2) the catchall symbol "*" can be used
   * 3) selection will start with transport dataType and THEN go to "*" if needed
   */
  transports = {},

  // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
  allTypes = "*/".concat("*");

// #8138, IE may throw an exception when accessing
// a field from window.location if document.domain has been set
try {
  ajaxLocation = location.href;
} catch( e ) {
  // Use the href attribute of an A element
  // since IE will modify it given document.location
  ajaxLocation = document.createElement( "a" );
  ajaxLocation.href = "";
  ajaxLocation = ajaxLocation.href;
}

// Segment location into parts
ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

  // dataTypeExpression is optional and defaults to "*"
  return function( dataTypeExpression, func ) {

	if ( typeof dataTypeExpression !== "string" ) {
	  func = dataTypeExpression;
	  dataTypeExpression = "*";
	}

	var dataType,
	  i = 0,
	  dataTypes = dataTypeExpression.toLowerCase().match( core_rnotwhite ) || [];

	if ( jQuery.isFunction( func ) ) {
	  // For each dataType in the dataTypeExpression
	  while ( (dataType = dataTypes[i++]) ) {
		// Prepend if requested
		if ( dataType[0] === "+" ) {
		  dataType = dataType.slice( 1 ) || "*";
		  (structure[ dataType ] = structure[ dataType ] || []).unshift( func );

		// Otherwise append
		} else {
		  (structure[ dataType ] = structure[ dataType ] || []).push( func );
		}
	  }
	}
  };
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

  var inspected = {},
	seekingTransport = ( structure === transports );

  function inspect( dataType ) {
	var selected;
	inspected[ dataType ] = true;
	jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
	  var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
	  if( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
		options.dataTypes.unshift( dataTypeOrTransport );
		inspect( dataTypeOrTransport );
		return false;
	  } else if ( seekingTransport ) {
		return !( selected = dataTypeOrTransport );
	  }
	});
	return selected;
  }

  return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
  var deep, key,
	flatOptions = jQuery.ajaxSettings.flatOptions || {};

  for ( key in src ) {
	if ( src[ key ] !== undefined ) {
	  ( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
	}
  }
  if ( deep ) {
	jQuery.extend( true, target, deep );
  }

  return target;
}

jQuery.fn.load = function( url, params, callback ) {
  if ( typeof url !== "string" && _load ) {
	return _load.apply( this, arguments );
  }

  var selector, response, type,
	self = this,
	off = url.indexOf(" ");

  if ( off >= 0 ) {
	selector = url.slice( off, url.length );
	url = url.slice( 0, off );
  }

  // If it's a function
  if ( jQuery.isFunction( params ) ) {

	// We assume that it's the callback
	callback = params;
	params = undefined;

  // Otherwise, build a param string
  } else if ( params && typeof params === "object" ) {
	type = "POST";
  }

  // If we have elements to modify, make the request
  if ( self.length > 0 ) {
	jQuery.ajax({
	  url: url,

	  // if "type" variable is undefined, then "GET" method will be used
	  type: type,
	  dataType: "html",
	  data: params
	}).done(function( responseText ) {

	  // Save response for use in complete callback
	  response = arguments;

	  self.html( selector ?

		// If a selector was specified, locate the right elements in a dummy div
		// Exclude scripts to avoid IE 'Permission Denied' errors
		jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

		// Otherwise use the full result
		responseText );

	}).complete( callback && function( jqXHR, status ) {
	  self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
	});
  }

  return this;
};

// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ){
  jQuery.fn[ type ] = function( fn ){
	return this.on( type, fn );
  };
});

jQuery.each( [ "get", "post" ], function( i, method ) {
  jQuery[ method ] = function( url, data, callback, type ) {
	// shift arguments if data argument was omitted
	if ( jQuery.isFunction( data ) ) {
	  type = type || callback;
	  callback = data;
	  data = undefined;
	}

	return jQuery.ajax({
	  url: url,
	  type: method,
	  dataType: type,
	  data: data,
	  success: callback
	});
  };
});

jQuery.extend({

  // Counter for holding the number of active queries
  active: 0,

  // Last-Modified header cache for next request
  lastModified: {},
  etag: {},

  ajaxSettings: {
	url: ajaxLocation,
	type: "GET",
	isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
	global: true,
	processData: true,
	async: true,
	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	/*
	timeout: 0,
	data: null,
	dataType: null,
	username: null,
	password: null,
	cache: null,
	throws: false,
	traditional: false,
	headers: {},
	*/

	accepts: {
	  "*": allTypes,
	  text: "text/plain",
	  html: "text/html",
	  xml: "application/xml, text/xml",
	  json: "application/json, text/javascript"
	},

	contents: {
	  xml: /xml/,
	  html: /html/,
	  json: /json/
	},

	responseFields: {
	  xml: "responseXML",
	  text: "responseText"
	},

	// Data converters
	// Keys separate source (or catchall "*") and destination types with a single space
	converters: {

	  // Convert anything to text
	  "* text": window.String,

	  // Text to html (true = no transformation)
	  "text html": true,

	  // Evaluate text as a json expression
	  "text json": jQuery.parseJSON,

	  // Parse text as xml
	  "text xml": jQuery.parseXML
	},

	// For options that shouldn't be deep extended:
	// you can add your own custom options here if
	// and when you create one that shouldn't be
	// deep extended (see ajaxExtend)
	flatOptions: {
	  url: true,
	  context: true
	}
  },

  // Creates a full fledged settings object into target
  // with both ajaxSettings and settings fields.
  // If target is omitted, writes into ajaxSettings.
  ajaxSetup: function( target, settings ) {
	return settings ?

	  // Building a settings object
	  ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

	  // Extending ajaxSettings
	  ajaxExtend( jQuery.ajaxSettings, target );
  },

  ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
  ajaxTransport: addToPrefiltersOrTransports( transports ),

  // Main method
  ajax: function( url, options ) {

	// If url is an object, simulate pre-1.5 signature
	if ( typeof url === "object" ) {
	  options = url;
	  url = undefined;
	}

	// Force options to be an object
	options = options || {};

	var // Cross-domain detection vars
	  parts,
	  // Loop variable
	  i,
	  // URL without anti-cache param
	  cacheURL,
	  // Response headers as string
	  responseHeadersString,
	  // timeout handle
	  timeoutTimer,

	  // To know if global events are to be dispatched
	  fireGlobals,

	  transport,
	  // Response headers
	  responseHeaders,
	  // Create the final options object
	  s = jQuery.ajaxSetup( {}, options ),
	  // Callbacks context
	  callbackContext = s.context || s,
	  // Context for global events is callbackContext if it is a DOM node or jQuery collection
	  globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
		jQuery( callbackContext ) :
		jQuery.event,
	  // Deferreds
	  deferred = jQuery.Deferred(),
	  completeDeferred = jQuery.Callbacks("once memory"),
	  // Status-dependent callbacks
	  statusCode = s.statusCode || {},
	  // Headers (they are sent all at once)
	  requestHeaders = {},
	  requestHeadersNames = {},
	  // The jqXHR state
	  state = 0,
	  // Default abort message
	  strAbort = "canceled",
	  // Fake xhr
	  jqXHR = {
		readyState: 0,

		// Builds headers hashtable if needed
		getResponseHeader: function( key ) {
		  var match;
		  if ( state === 2 ) {
			if ( !responseHeaders ) {
			  responseHeaders = {};
			  while ( (match = rheaders.exec( responseHeadersString )) ) {
				responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
			  }
			}
			match = responseHeaders[ key.toLowerCase() ];
		  }
		  return match == null ? null : match;
		},

		// Raw string
		getAllResponseHeaders: function() {
		  return state === 2 ? responseHeadersString : null;
		},

		// Caches the header
		setRequestHeader: function( name, value ) {
		  var lname = name.toLowerCase();
		  if ( !state ) {
			name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
			requestHeaders[ name ] = value;
		  }
		  return this;
		},

		// Overrides response content-type header
		overrideMimeType: function( type ) {
		  if ( !state ) {
			s.mimeType = type;
		  }
		  return this;
		},

		// Status-dependent callbacks
		statusCode: function( map ) {
		  var code;
		  if ( map ) {
			if ( state < 2 ) {
			  for ( code in map ) {
				// Lazy-add the new callback in a way that preserves old ones
				statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
			  }
			} else {
			  // Execute the appropriate callbacks
			  jqXHR.always( map[ jqXHR.status ] );
			}
		  }
		  return this;
		},

		// Cancel the request
		abort: function( statusText ) {
		  var finalText = statusText || strAbort;
		  if ( transport ) {
			transport.abort( finalText );
		  }
		  done( 0, finalText );
		  return this;
		}
	  };

	// Attach deferreds
	deferred.promise( jqXHR ).complete = completeDeferred.add;
	jqXHR.success = jqXHR.done;
	jqXHR.error = jqXHR.fail;

	// Remove hash character (#7531: and string promotion)
	// Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
	// Handle falsy url in the settings object (#10093: consistency with old signature)
	// We also use the url parameter if available
	s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" ).replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

	// Alias method option to type as per ticket #12004
	s.type = options.method || options.type || s.method || s.type;

	// Extract dataTypes list
	s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( core_rnotwhite ) || [""];

	// A cross-domain request is in order when we have a protocol:host:port mismatch
	if ( s.crossDomain == null ) {
	  parts = rurl.exec( s.url.toLowerCase() );
	  s.crossDomain = !!( parts &&
		( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
		  ( parts[ 3 ] || ( parts[ 1 ] === "http:" ? 80 : 443 ) ) !=
			( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? 80 : 443 ) ) )
	  );
	}

	// Convert data if not already a string
	if ( s.data && s.processData && typeof s.data !== "string" ) {
	  s.data = jQuery.param( s.data, s.traditional );
	}

	// Apply prefilters
	inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

	// If request was aborted inside a prefilter, stop there
	if ( state === 2 ) {
	  return jqXHR;
	}

	// We can fire global events as of now if asked to
	fireGlobals = s.global;

	// Watch for a new set of requests
	if ( fireGlobals && jQuery.active++ === 0 ) {
	  jQuery.event.trigger("ajaxStart");
	}

	// Uppercase the type
	s.type = s.type.toUpperCase();

	// Determine if request has content
	s.hasContent = !rnoContent.test( s.type );

	// Save the URL in case we're toying with the If-Modified-Since
	// and/or If-None-Match header later on
	cacheURL = s.url;

	// More options handling for requests with no content
	if ( !s.hasContent ) {

	  // If data is available, append data to url
	  if ( s.data ) {
		cacheURL = ( s.url += ( ajax_rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
		// #9682: remove data so that it's not used in an eventual retry
		delete s.data;
	  }

	  // Add anti-cache in url if needed
	  if ( s.cache === false ) {
		s.url = rts.test( cacheURL ) ?

		  // If there is already a '_' parameter, set its value
		  cacheURL.replace( rts, "$1_=" + ajax_nonce++ ) :

		  // Otherwise add one to the end
		  cacheURL + ( ajax_rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ajax_nonce++;
	  }
	}

	// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
	if ( s.ifModified ) {
	  if ( jQuery.lastModified[ cacheURL ] ) {
		jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
	  }
	  if ( jQuery.etag[ cacheURL ] ) {
		jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
	  }
	}

	// Set the correct header, if data is being sent
	if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
	  jqXHR.setRequestHeader( "Content-Type", s.contentType );
	}

	// Set the Accepts header for the server, depending on the dataType
	jqXHR.setRequestHeader(
	  "Accept",
	  s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
		s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
		s.accepts[ "*" ]
	);

	// Check for headers option
	for ( i in s.headers ) {
	  jqXHR.setRequestHeader( i, s.headers[ i ] );
	}

	// Allow custom headers/mimetypes and early abort
	if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
	  // Abort if not done already and return
	  return jqXHR.abort();
	}

	// aborting is no longer a cancellation
	strAbort = "abort";

	// Install callbacks on deferreds
	for ( i in { success: 1, error: 1, complete: 1 } ) {
	  jqXHR[ i ]( s[ i ] );
	}

	// Get transport
	transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

	// If no transport, we auto-abort
	if ( !transport ) {
	  done( -1, "No Transport" );
	} else {
	  jqXHR.readyState = 1;

	  // Send global event
	  if ( fireGlobals ) {
		globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
	  }
	  // Timeout
	  if ( s.async && s.timeout > 0 ) {
		timeoutTimer = setTimeout(function() {
		  jqXHR.abort("timeout");
		}, s.timeout );
	  }

	  try {
		state = 1;
		transport.send( requestHeaders, done );
	  } catch ( e ) {
		// Propagate exception as error if not done
		if ( state < 2 ) {
		  done( -1, e );
		// Simply rethrow otherwise
		} else {
		  throw e;
		}
	  }
	}

	// Callback for when everything is done
	function done( status, nativeStatusText, responses, headers ) {
	  var isSuccess, success, error, response, modified,
		statusText = nativeStatusText;

	  // Called once
	  if ( state === 2 ) {
		return;
	  }

	  // State is "done" now
	  state = 2;

	  // Clear timeout if it exists
	  if ( timeoutTimer ) {
		clearTimeout( timeoutTimer );
	  }

	  // Dereference transport for early garbage collection
	  // (no matter how long the jqXHR object will be used)
	  transport = undefined;

	  // Cache response headers
	  responseHeadersString = headers || "";

	  // Set readyState
	  jqXHR.readyState = status > 0 ? 4 : 0;

	  // Get response data
	  if ( responses ) {
		response = ajaxHandleResponses( s, jqXHR, responses );
	  }

	  // If successful, handle type chaining
	  if ( status >= 200 && status < 300 || status === 304 ) {

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
		  modified = jqXHR.getResponseHeader("Last-Modified");
		  if ( modified ) {
			jQuery.lastModified[ cacheURL ] = modified;
		  }
		  modified = jqXHR.getResponseHeader("etag");
		  if ( modified ) {
			jQuery.etag[ cacheURL ] = modified;
		  }
		}

		// if no content
		if ( status === 204 ) {
		  isSuccess = true;
		  statusText = "nocontent";

		// if not modified
		} else if ( status === 304 ) {
		  isSuccess = true;
		  statusText = "notmodified";

		// If we have data, let's convert it
		} else {
		  isSuccess = ajaxConvert( s, response );
		  statusText = isSuccess.state;
		  success = isSuccess.data;
		  error = isSuccess.error;
		  isSuccess = !error;
		}
	  } else {
		// We extract error from statusText
		// then normalize statusText and status for non-aborts
		error = statusText;
		if ( status || !statusText ) {
		  statusText = "error";
		  if ( status < 0 ) {
			status = 0;
		  }
		}
	  }

	  // Set data for the fake xhr object
	  jqXHR.status = status;
	  jqXHR.statusText = ( nativeStatusText || statusText ) + "";

	  // Success/Error
	  if ( isSuccess ) {
		deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
	  } else {
		deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
	  }

	  // Status-dependent callbacks
	  jqXHR.statusCode( statusCode );
	  statusCode = undefined;

	  if ( fireGlobals ) {
		globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
		  [ jqXHR, s, isSuccess ? success : error ] );
	  }

	  // Complete
	  completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

	  if ( fireGlobals ) {
		globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
		// Handle the global AJAX counter
		if ( !( --jQuery.active ) ) {
		  jQuery.event.trigger("ajaxStop");
		}
	  }
	}

	return jqXHR;
  },

  getScript: function( url, callback ) {
	return jQuery.get( url, undefined, callback, "script" );
  },

  getJSON: function( url, data, callback ) {
	return jQuery.get( url, data, callback, "json" );
  }
});

/* Handles responses to an ajax request:
 * - sets all responseXXX fields accordingly
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {
  var firstDataType, ct, finalDataType, type,
	contents = s.contents,
	dataTypes = s.dataTypes,
	responseFields = s.responseFields;

  // Fill responseXXX fields
  for ( type in responseFields ) {
	if ( type in responses ) {
	  jqXHR[ responseFields[type] ] = responses[ type ];
	}
  }

  // Remove auto dataType and get content-type in the process
  while( dataTypes[ 0 ] === "*" ) {
	dataTypes.shift();
	if ( ct === undefined ) {
	  ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
	}
  }

  // Check if we're dealing with a known content-type
  if ( ct ) {
	for ( type in contents ) {
	  if ( contents[ type ] && contents[ type ].test( ct ) ) {
		dataTypes.unshift( type );
		break;
	  }
	}
  }

  // Check to see if we have a response for the expected dataType
  if ( dataTypes[ 0 ] in responses ) {
	finalDataType = dataTypes[ 0 ];
  } else {
	// Try convertible dataTypes
	for ( type in responses ) {
	  if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
		finalDataType = type;
		break;
	  }
	  if ( !firstDataType ) {
		firstDataType = type;
	  }
	}
	// Or just use first one
	finalDataType = finalDataType || firstDataType;
  }

  // If we found a dataType
  // We add the dataType to the list if needed
  // and return the corresponding response
  if ( finalDataType ) {
	if ( finalDataType !== dataTypes[ 0 ] ) {
	  dataTypes.unshift( finalDataType );
	}
	return responses[ finalDataType ];
  }
}

// Chain conversions given the request and the original response
function ajaxConvert( s, response ) {
  var conv2, current, conv, tmp,
	converters = {},
	i = 0,
	// Work with a copy of dataTypes in case we need to modify it for conversion
	dataTypes = s.dataTypes.slice(),
	prev = dataTypes[ 0 ];

  // Apply the dataFilter if provided
  if ( s.dataFilter ) {
	response = s.dataFilter( response, s.dataType );
  }

  // Create converters map with lowercased keys
  if ( dataTypes[ 1 ] ) {
	for ( conv in s.converters ) {
	  converters[ conv.toLowerCase() ] = s.converters[ conv ];
	}
  }

  // Convert to each sequential dataType, tolerating list modification
  for ( ; (current = dataTypes[++i]); ) {

	// There's only work to do if current dataType is non-auto
	if ( current !== "*" ) {

	  // Convert response if prev dataType is non-auto and differs from current
	  if ( prev !== "*" && prev !== current ) {

		// Seek a direct converter
		conv = converters[ prev + " " + current ] || converters[ "* " + current ];

		// If none found, seek a pair
		if ( !conv ) {
		  for ( conv2 in converters ) {

			// If conv2 outputs current
			tmp = conv2.split(" ");
			if ( tmp[ 1 ] === current ) {

			  // If prev can be converted to accepted input
			  conv = converters[ prev + " " + tmp[ 0 ] ] ||
				converters[ "* " + tmp[ 0 ] ];
			  if ( conv ) {
				// Condense equivalence converters
				if ( conv === true ) {
				  conv = converters[ conv2 ];

				// Otherwise, insert the intermediate dataType
				} else if ( converters[ conv2 ] !== true ) {
				  current = tmp[ 0 ];
				  dataTypes.splice( i--, 0, current );
				}

				break;
			  }
			}
		  }
		}

		// Apply converter (if not an equivalence)
		if ( conv !== true ) {

		  // Unless errors are allowed to bubble, catch and return them
		  if ( conv && s["throws"] ) {
			response = conv( response );
		  } else {
			try {
			  response = conv( response );
			} catch ( e ) {
			  return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
			}
		  }
		}
	  }

	  // Update prev for next iteration
	  prev = current;
	}
  }

  return { state: "success", data: response };
}
// Install script dataType
jQuery.ajaxSetup({
  accepts: {
	script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
  },
  contents: {
	script: /(?:java|ecma)script/
  },
  converters: {
	"text script": function( text ) {
	  jQuery.globalEval( text );
	  return text;
	}
  }
});

// Handle cache's special case and global
jQuery.ajaxPrefilter( "script", function( s ) {
  if ( s.cache === undefined ) {
	s.cache = false;
  }
  if ( s.crossDomain ) {
	s.type = "GET";
	s.global = false;
  }
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function(s) {

  // This transport only deals with cross domain requests
  if ( s.crossDomain ) {

	var script,
	  head = document.head || jQuery("head")[0] || document.documentElement;

	return {

	  send: function( _, callback ) {

		script = document.createElement("script");

		script.async = true;

		if ( s.scriptCharset ) {
		  script.charset = s.scriptCharset;
		}

		script.src = s.url;

		// Attach handlers for all browsers
		script.onload = script.onreadystatechange = function( _, isAbort ) {

		  if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

			// Handle memory leak in IE
			script.onload = script.onreadystatechange = null;

			// Remove the script
			if ( script.parentNode ) {
			  script.parentNode.removeChild( script );
			}

			// Dereference the script
			script = null;

			// Callback if not abort
			if ( !isAbort ) {
			  callback( 200, "success" );
			}
		  }
		};

		// Circumvent IE6 bugs with base elements (#2709 and #4378) by prepending
		// Use native DOM manipulation to avoid our domManip AJAX trickery
		head.insertBefore( script, head.firstChild );
	  },

	  abort: function() {
		if ( script ) {
		  script.onload( undefined, true );
		}
	  }
	};
  }
});
var oldCallbacks = [],
  rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
  jsonp: "callback",
  jsonpCallback: function() {
	var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( ajax_nonce++ ) );
	this[ callback ] = true;
	return callback;
  }
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

  var callbackName, overwritten, responseContainer,
	jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
	  "url" :
	  typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
	);

  // Handle iff the expected data type is "jsonp" or we have a parameter to set
  if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

	// Get callback name, remembering preexisting value associated with it
	callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
	  s.jsonpCallback() :
	  s.jsonpCallback;

	// Insert callback into url or form data
	if ( jsonProp ) {
	  s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
	} else if ( s.jsonp !== false ) {
	  s.url += ( ajax_rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
	}

	// Use data converter to retrieve json after script execution
	s.converters["script json"] = function() {
	  if ( !responseContainer ) {
		jQuery.error( callbackName + " was not called" );
	  }
	  return responseContainer[ 0 ];
	};

	// force json dataType
	s.dataTypes[ 0 ] = "json";

	// Install callback
	overwritten = window[ callbackName ];
	window[ callbackName ] = function() {
	  responseContainer = arguments;
	};

	// Clean-up function (fires after converters)
	jqXHR.always(function() {
	  // Restore preexisting value
	  window[ callbackName ] = overwritten;

	  // Save back as free
	  if ( s[ callbackName ] ) {
		// make sure that re-using the options doesn't screw things around
		s.jsonpCallback = originalSettings.jsonpCallback;

		// save the callback name for future use
		oldCallbacks.push( callbackName );
	  }

	  // Call if it was a function and we have a response
	  if ( responseContainer && jQuery.isFunction( overwritten ) ) {
		overwritten( responseContainer[ 0 ] );
	  }

	  responseContainer = overwritten = undefined;
	});

	// Delegate to script
	return "script";
  }
});
var xhrCallbacks, xhrSupported,
  xhrId = 0,
  // #5280: Internet Explorer will keep connections alive if we don't abort on unload
  xhrOnUnloadAbort = window.ActiveXObject && function() {
	// Abort all pending requests
	var key;
	for ( key in xhrCallbacks ) {
	  xhrCallbacks[ key ]( undefined, true );
	}
  };

// Functions to create xhrs
function createStandardXHR() {
  try {
	return new window.XMLHttpRequest();
  } catch( e ) {}
}

function createActiveXHR() {
  try {
	return new window.ActiveXObject("Microsoft.XMLHTTP");
  } catch( e ) {}
}

// Create the request object
// (This is still attached to ajaxSettings for backward compatibility)
jQuery.ajaxSettings.xhr = window.ActiveXObject ?
  /* Microsoft failed to properly
   * implement the XMLHttpRequest in IE7 (can't request local files),
   * so we use the ActiveXObject when it is available
   * Additionally XMLHttpRequest can be disabled in IE7/IE8 so
   * we need a fallback.
   */
  function() {
	return !this.isLocal && createStandardXHR() || createActiveXHR();
  } :
  // For all other browsers, use the standard XMLHttpRequest object
  createStandardXHR;

// Determine support properties
xhrSupported = jQuery.ajaxSettings.xhr();
jQuery.support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
xhrSupported = jQuery.support.ajax = !!xhrSupported;

// Create transport if the browser can provide an xhr
if ( xhrSupported ) {

  jQuery.ajaxTransport(function( s ) {
	// Cross domain only allowed if supported through XMLHttpRequest
	if ( !s.crossDomain || jQuery.support.cors ) {

	  var callback;

	  return {
		send: function( headers, complete ) {

		  // Get a new xhr
		  var handle, i,
			xhr = s.xhr();

		  // Open the socket
		  // Passing null username, generates a login popup on Opera (#2865)
		  if ( s.username ) {
			xhr.open( s.type, s.url, s.async, s.username, s.password );
		  } else {
			xhr.open( s.type, s.url, s.async );
		  }

		  // Apply custom fields if provided
		  if ( s.xhrFields ) {
			for ( i in s.xhrFields ) {
			  xhr[ i ] = s.xhrFields[ i ];
			}
		  }

		  // Override mime type if needed
		  if ( s.mimeType && xhr.overrideMimeType ) {
			xhr.overrideMimeType( s.mimeType );
		  }

		  // X-Requested-With header
		  // For cross-domain requests, seeing as conditions for a preflight are
		  // akin to a jigsaw puzzle, we simply never set it to be sure.
		  // (it can always be set on a per-request basis or even using ajaxSetup)
		  // For same-domain requests, won't change header if already provided.
		  if ( !s.crossDomain && !headers["X-Requested-With"] ) {
			headers["X-Requested-With"] = "XMLHttpRequest";
		  }

		  // Need an extra try/catch for cross domain requests in Firefox 3
		  try {
			for ( i in headers ) {
			  xhr.setRequestHeader( i, headers[ i ] );
			}
		  } catch( err ) {}

		  // Do send the request
		  // This may raise an exception which is actually
		  // handled in jQuery.ajax (so no try/catch here)
		  xhr.send( ( s.hasContent && s.data ) || null );

		  // Listener
		  callback = function( _, isAbort ) {
			var status, responseHeaders, statusText, responses;

			// Firefox throws exceptions when accessing properties
			// of an xhr when a network error occurred
			// http://helpful.knobs-dials.com/index.php/Component_returned_failure_code:_0x80040111_(NS_ERROR_NOT_AVAILABLE)
			try {

			  // Was never called and is aborted or complete
			  if ( callback && ( isAbort || xhr.readyState === 4 ) ) {

				// Only called once
				callback = undefined;

				// Do not keep as active anymore
				if ( handle ) {
				  xhr.onreadystatechange = jQuery.noop;
				  if ( xhrOnUnloadAbort ) {
					delete xhrCallbacks[ handle ];
				  }
				}

				// If it's an abort
				if ( isAbort ) {
				  // Abort it manually if needed
				  if ( xhr.readyState !== 4 ) {
					xhr.abort();
				  }
				} else {
				  responses = {};
				  status = xhr.status;
				  responseHeaders = xhr.getAllResponseHeaders();

				  // When requesting binary data, IE6-9 will throw an exception
				  // on any attempt to access responseText (#11426)
				  if ( typeof xhr.responseText === "string" ) {
					responses.text = xhr.responseText;
				  }

				  // Firefox throws an exception when accessing
				  // statusText for faulty cross-domain requests
				  try {
					statusText = xhr.statusText;
				  } catch( e ) {
					// We normalize with Webkit giving an empty statusText
					statusText = "";
				  }

				  // Filter status for non standard behaviors

				  // If the request is local and we have data: assume a success
				  // (success with no data won't get notified, that's the best we
				  // can do given current implementations)
				  if ( !status && s.isLocal && !s.crossDomain ) {
					status = responses.text ? 200 : 404;
				  // IE - #1450: sometimes returns 1223 when it should be 204
				  } else if ( status === 1223 ) {
					status = 204;
				  }
				}
			  }
			} catch( firefoxAccessException ) {
			  if ( !isAbort ) {
				complete( -1, firefoxAccessException );
			  }
			}

			// Call complete if needed
			if ( responses ) {
			  complete( status, statusText, responses, responseHeaders );
			}
		  };

		  if ( !s.async ) {
			// if we're in sync mode we fire the callback
			callback();
		  } else if ( xhr.readyState === 4 ) {
			// (IE6 & IE7) if it's in cache and has been
			// retrieved directly we need to fire the callback
			setTimeout( callback );
		  } else {
			handle = ++xhrId;
			if ( xhrOnUnloadAbort ) {
			  // Create the active xhrs callbacks list if needed
			  // and attach the unload handler
			  if ( !xhrCallbacks ) {
				xhrCallbacks = {};
				jQuery( window ).unload( xhrOnUnloadAbort );
			  }
			  // Add to list of active xhrs callbacks
			  xhrCallbacks[ handle ] = callback;
			}
			xhr.onreadystatechange = callback;
		  }
		},

		abort: function() {
		  if ( callback ) {
			callback( undefined, true );
		  }
		}
	  };
	}
  });
}
var fxNow, timerId,
  rfxtypes = /^(?:toggle|show|hide)$/,
  rfxnum = new RegExp( "^(?:([+-])=|)(" + core_pnum + ")([a-z%]*)$", "i" ),
  rrun = /queueHooks$/,
  animationPrefilters = [ defaultPrefilter ],
  tweeners = {
	"*": [function( prop, value ) {
	  var end, unit,
		tween = this.createTween( prop, value ),
		parts = rfxnum.exec( value ),
		target = tween.cur(),
		start = +target || 0,
		scale = 1,
		maxIterations = 20;

	  if ( parts ) {
		end = +parts[2];
		unit = parts[3] || ( jQuery.cssNumber[ prop ] ? "" : "px" );

		// We need to compute starting value
		if ( unit !== "px" && start ) {
		  // Iteratively approximate from a nonzero starting point
		  // Prefer the current property, because this process will be trivial if it uses the same units
		  // Fallback to end or a simple constant
		  start = jQuery.css( tween.elem, prop, true ) || end || 1;

		  do {
			// If previous iteration zeroed out, double until we get *something*
			// Use a string for doubling factor so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			start = start / scale;
			jQuery.style( tween.elem, prop, start + unit );

		  // Update scale, tolerating zero or NaN from tween.cur()
		  // And breaking the loop if scale is unchanged or perfect, or if we've just had enough
		  } while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
		}

		tween.unit = unit;
		tween.start = start;
		// If a +=/-= token was provided, we're doing a relative animation
		tween.end = parts[1] ? start + ( parts[1] + 1 ) * end : end;
	  }
	  return tween;
	}]
  };

// Animations created synchronously will run synchronously
function createFxNow() {
  setTimeout(function() {
	fxNow = undefined;
  });
  return ( fxNow = jQuery.now() );
}

function createTweens( animation, props ) {
  jQuery.each( props, function( prop, value ) {
	var collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
	  index = 0,
	  length = collection.length;
	for ( ; index < length; index++ ) {
	  if ( collection[ index ].call( animation, prop, value ) ) {

		// we're done with this property
		return;
	  }
	}
  });
}

function Animation( elem, properties, options ) {
  var result,
	stopped,
	index = 0,
	length = animationPrefilters.length,
	deferred = jQuery.Deferred().always( function() {
	  // don't match elem in the :animated selector
	  delete tick.elem;
	}),
	tick = function() {
	  if ( stopped ) {
		return false;
	  }
	  var currentTime = fxNow || createFxNow(),
		remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
		// archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)
		temp = remaining / animation.duration || 0,
		percent = 1 - temp,
		index = 0,
		length = animation.tweens.length;

	  for ( ; index < length ; index++ ) {
		animation.tweens[ index ].run( percent );
	  }

	  deferred.notifyWith( elem, [ animation, percent, remaining ]);

	  if ( percent < 1 && length ) {
		return remaining;
	  } else {
		deferred.resolveWith( elem, [ animation ] );
		return false;
	  }
	},
	animation = deferred.promise({
	  elem: elem,
	  props: jQuery.extend( {}, properties ),
	  opts: jQuery.extend( true, { specialEasing: {} }, options ),
	  originalProperties: properties,
	  originalOptions: options,
	  startTime: fxNow || createFxNow(),
	  duration: options.duration,
	  tweens: [],
	  createTween: function( prop, end ) {
		var tween = jQuery.Tween( elem, animation.opts, prop, end,
			animation.opts.specialEasing[ prop ] || animation.opts.easing );
		animation.tweens.push( tween );
		return tween;
	  },
	  stop: function( gotoEnd ) {
		var index = 0,
		  // if we are going to the end, we want to run all the tweens
		  // otherwise we skip this part
		  length = gotoEnd ? animation.tweens.length : 0;
		if ( stopped ) {
		  return this;
		}
		stopped = true;
		for ( ; index < length ; index++ ) {
		  animation.tweens[ index ].run( 1 );
		}

		// resolve when we played the last frame
		// otherwise, reject
		if ( gotoEnd ) {
		  deferred.resolveWith( elem, [ animation, gotoEnd ] );
		} else {
		  deferred.rejectWith( elem, [ animation, gotoEnd ] );
		}
		return this;
	  }
	}),
	props = animation.props;

  propFilter( props, animation.opts.specialEasing );

  for ( ; index < length ; index++ ) {
	result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
	if ( result ) {
	  return result;
	}
  }

  createTweens( animation, props );

  if ( jQuery.isFunction( animation.opts.start ) ) {
	animation.opts.start.call( elem, animation );
  }

  jQuery.fx.timer(
	jQuery.extend( tick, {
	  elem: elem,
	  anim: animation,
	  queue: animation.opts.queue
	})
  );

  // attach callbacks from options
  return animation.progress( animation.opts.progress )
	.done( animation.opts.done, animation.opts.complete )
	.fail( animation.opts.fail )
	.always( animation.opts.always );
}

function propFilter( props, specialEasing ) {
  var value, name, index, easing, hooks;

  // camelCase, specialEasing and expand cssHook pass
  for ( index in props ) {
	name = jQuery.camelCase( index );
	easing = specialEasing[ name ];
	value = props[ index ];
	if ( jQuery.isArray( value ) ) {
	  easing = value[ 1 ];
	  value = props[ index ] = value[ 0 ];
	}

	if ( index !== name ) {
	  props[ name ] = value;
	  delete props[ index ];
	}

	hooks = jQuery.cssHooks[ name ];
	if ( hooks && "expand" in hooks ) {
	  value = hooks.expand( value );
	  delete props[ name ];

	  // not quite $.extend, this wont overwrite keys already present.
	  // also - reusing 'index' from above because we have the correct "name"
	  for ( index in value ) {
		if ( !( index in props ) ) {
		  props[ index ] = value[ index ];
		  specialEasing[ index ] = easing;
		}
	  }
	} else {
	  specialEasing[ name ] = easing;
	}
  }
}

jQuery.Animation = jQuery.extend( Animation, {

  tweener: function( props, callback ) {
	if ( jQuery.isFunction( props ) ) {
	  callback = props;
	  props = [ "*" ];
	} else {
	  props = props.split(" ");
	}

	var prop,
	  index = 0,
	  length = props.length;

	for ( ; index < length ; index++ ) {
	  prop = props[ index ];
	  tweeners[ prop ] = tweeners[ prop ] || [];
	  tweeners[ prop ].unshift( callback );
	}
  },

  prefilter: function( callback, prepend ) {
	if ( prepend ) {
	  animationPrefilters.unshift( callback );
	} else {
	  animationPrefilters.push( callback );
	}
  }
});

function defaultPrefilter( elem, props, opts ) {
  /*jshint validthis:true */
  var prop, index, length,
	value, dataShow, toggle,
	tween, hooks, oldfire,
	anim = this,
	style = elem.style,
	orig = {},
	handled = [],
	hidden = elem.nodeType && isHidden( elem );

  // handle queue: false promises
  if ( !opts.queue ) {
	hooks = jQuery._queueHooks( elem, "fx" );
	if ( hooks.unqueued == null ) {
	  hooks.unqueued = 0;
	  oldfire = hooks.empty.fire;
	  hooks.empty.fire = function() {
		if ( !hooks.unqueued ) {
		  oldfire();
		}
	  };
	}
	hooks.unqueued++;

	anim.always(function() {
	  // doing this makes sure that the complete handler will be called
	  // before this completes
	  anim.always(function() {
		hooks.unqueued--;
		if ( !jQuery.queue( elem, "fx" ).length ) {
		  hooks.empty.fire();
		}
	  });
	});
  }

  // height/width overflow pass
  if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
	// Make sure that nothing sneaks out
	// Record all 3 overflow attributes because IE does not
	// change the overflow attribute when overflowX and
	// overflowY are set to the same value
	opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

	// Set display property to inline-block for height/width
	// animations on inline elements that are having width/height animated
	if ( jQuery.css( elem, "display" ) === "inline" &&
		jQuery.css( elem, "float" ) === "none" ) {

	  // inline-level elements accept inline-block;
	  // block-level elements need to be inline with layout
	  if ( !jQuery.support.inlineBlockNeedsLayout || css_defaultDisplay( elem.nodeName ) === "inline" ) {
		style.display = "inline-block";

	  } else {
		style.zoom = 1;
	  }
	}
  }

  if ( opts.overflow ) {
	style.overflow = "hidden";
	if ( !jQuery.support.shrinkWrapBlocks ) {
	  anim.always(function() {
		style.overflow = opts.overflow[ 0 ];
		style.overflowX = opts.overflow[ 1 ];
		style.overflowY = opts.overflow[ 2 ];
	  });
	}
  }


  // show/hide pass
  for ( index in props ) {
	value = props[ index ];
	if ( rfxtypes.exec( value ) ) {
	  delete props[ index ];
	  toggle = toggle || value === "toggle";
	  if ( value === ( hidden ? "hide" : "show" ) ) {
		continue;
	  }
	  handled.push( index );
	}
  }

  length = handled.length;
  if ( length ) {
	dataShow = jQuery._data( elem, "fxshow" ) || jQuery._data( elem, "fxshow", {} );
	if ( "hidden" in dataShow ) {
	  hidden = dataShow.hidden;
	}

	// store state if its toggle - enables .stop().toggle() to "reverse"
	if ( toggle ) {
	  dataShow.hidden = !hidden;
	}
	if ( hidden ) {
	  jQuery( elem ).show();
	} else {
	  anim.done(function() {
		jQuery( elem ).hide();
	  });
	}
	anim.done(function() {
	  var prop;
	  jQuery._removeData( elem, "fxshow" );
	  for ( prop in orig ) {
		jQuery.style( elem, prop, orig[ prop ] );
	  }
	});
	for ( index = 0 ; index < length ; index++ ) {
	  prop = handled[ index ];
	  tween = anim.createTween( prop, hidden ? dataShow[ prop ] : 0 );
	  orig[ prop ] = dataShow[ prop ] || jQuery.style( elem, prop );

	  if ( !( prop in dataShow ) ) {
		dataShow[ prop ] = tween.start;
		if ( hidden ) {
		  tween.end = tween.start;
		  tween.start = prop === "width" || prop === "height" ? 1 : 0;
		}
	  }
	}
  }
}

function Tween( elem, options, prop, end, easing ) {
  return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
  constructor: Tween,
  init: function( elem, options, prop, end, easing, unit ) {
	this.elem = elem;
	this.prop = prop;
	this.easing = easing || "swing";
	this.options = options;
	this.start = this.now = this.cur();
	this.end = end;
	this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
  },
  cur: function() {
	var hooks = Tween.propHooks[ this.prop ];

	return hooks && hooks.get ?
	  hooks.get( this ) :
	  Tween.propHooks._default.get( this );
  },
  run: function( percent ) {
	var eased,
	  hooks = Tween.propHooks[ this.prop ];

	if ( this.options.duration ) {
	  this.pos = eased = jQuery.easing[ this.easing ](
		percent, this.options.duration * percent, 0, 1, this.options.duration
	  );
	} else {
	  this.pos = eased = percent;
	}
	this.now = ( this.end - this.start ) * eased + this.start;

	if ( this.options.step ) {
	  this.options.step.call( this.elem, this.now, this );
	}

	if ( hooks && hooks.set ) {
	  hooks.set( this );
	} else {
	  Tween.propHooks._default.set( this );
	}
	return this;
  }
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
  _default: {
	get: function( tween ) {
	  var result;

	  if ( tween.elem[ tween.prop ] != null &&
		(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
		return tween.elem[ tween.prop ];
	  }

	  // passing an empty string as a 3rd parameter to .css will automatically
	  // attempt a parseFloat and fallback to a string if the parse fails
	  // so, simple values such as "10px" are parsed to Float.
	  // complex values such as "rotate(1rad)" are returned as is.
	  result = jQuery.css( tween.elem, tween.prop, "" );
	  // Empty strings, null, undefined and "auto" are converted to 0.
	  return !result || result === "auto" ? 0 : result;
	},
	set: function( tween ) {
	  // use step hook for back compat - use cssHook if its there - use .style if its
	  // available and use plain properties where available
	  if ( jQuery.fx.step[ tween.prop ] ) {
		jQuery.fx.step[ tween.prop ]( tween );
	  } else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
		jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
	  } else {
		tween.elem[ tween.prop ] = tween.now;
	  }
	}
  }
};

// Remove in 2.0 - this supports IE8's panic based approach
// to setting things on disconnected nodes

Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
  set: function( tween ) {
	if ( tween.elem.nodeType && tween.elem.parentNode ) {
	  tween.elem[ tween.prop ] = tween.now;
	}
  }
};

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
  var cssFn = jQuery.fn[ name ];
  jQuery.fn[ name ] = function( speed, easing, callback ) {
	return speed == null || typeof speed === "boolean" ?
	  cssFn.apply( this, arguments ) :
	  this.animate( genFx( name, true ), speed, easing, callback );
  };
});

jQuery.fn.extend({
  fadeTo: function( speed, to, easing, callback ) {

	// show any hidden elements after setting opacity to 0
	return this.filter( isHidden ).css( "opacity", 0 ).show()

	  // animate to the value specified
	  .end().animate({ opacity: to }, speed, easing, callback );
  },
  animate: function( prop, speed, easing, callback ) {
	var empty = jQuery.isEmptyObject( prop ),
	  optall = jQuery.speed( speed, easing, callback ),
	  doAnimation = function() {
		// Operate on a copy of prop so per-property easing won't be lost
		var anim = Animation( this, jQuery.extend( {}, prop ), optall );
		doAnimation.finish = function() {
		  anim.stop( true );
		};
		// Empty animations, or finishing resolves immediately
		if ( empty || jQuery._data( this, "finish" ) ) {
		  anim.stop( true );
		}
	  };
	  doAnimation.finish = doAnimation;

	return empty || optall.queue === false ?
	  this.each( doAnimation ) :
	  this.queue( optall.queue, doAnimation );
  },
  stop: function( type, clearQueue, gotoEnd ) {
	var stopQueue = function( hooks ) {
	  var stop = hooks.stop;
	  delete hooks.stop;
	  stop( gotoEnd );
	};

	if ( typeof type !== "string" ) {
	  gotoEnd = clearQueue;
	  clearQueue = type;
	  type = undefined;
	}
	if ( clearQueue && type !== false ) {
	  this.queue( type || "fx", [] );
	}

	return this.each(function() {
	  var dequeue = true,
		index = type != null && type + "queueHooks",
		timers = jQuery.timers,
		data = jQuery._data( this );

	  if ( index ) {
		if ( data[ index ] && data[ index ].stop ) {
		  stopQueue( data[ index ] );
		}
	  } else {
		for ( index in data ) {
		  if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
			stopQueue( data[ index ] );
		  }
		}
	  }

	  for ( index = timers.length; index--; ) {
		if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
		  timers[ index ].anim.stop( gotoEnd );
		  dequeue = false;
		  timers.splice( index, 1 );
		}
	  }

	  // start the next in the queue if the last step wasn't forced
	  // timers currently will call their complete callbacks, which will dequeue
	  // but only if they were gotoEnd
	  if ( dequeue || !gotoEnd ) {
		jQuery.dequeue( this, type );
	  }
	});
  },
  finish: function( type ) {
	if ( type !== false ) {
	  type = type || "fx";
	}
	return this.each(function() {
	  var index,
		data = jQuery._data( this ),
		queue = data[ type + "queue" ],
		hooks = data[ type + "queueHooks" ],
		timers = jQuery.timers,
		length = queue ? queue.length : 0;

	  // enable finishing flag on private data
	  data.finish = true;

	  // empty the queue first
	  jQuery.queue( this, type, [] );

	  if ( hooks && hooks.cur && hooks.cur.finish ) {
		hooks.cur.finish.call( this );
	  }

	  // look for any active animations, and finish them
	  for ( index = timers.length; index--; ) {
		if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
		  timers[ index ].anim.stop( true );
		  timers.splice( index, 1 );
		}
	  }

	  // look for any animations in the old queue and finish them
	  for ( index = 0; index < length; index++ ) {
		if ( queue[ index ] && queue[ index ].finish ) {
		  queue[ index ].finish.call( this );
		}
	  }

	  // turn off finishing flag
	  delete data.finish;
	});
  }
});

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
  var which,
	attrs = { height: type },
	i = 0;

  // if we include width, step value is 1 to do all cssExpand values,
  // if we don't include width, step value is 2 to skip over Left and Right
  includeWidth = includeWidth? 1 : 0;
  for( ; i < 4 ; i += 2 - includeWidth ) {
	which = cssExpand[ i ];
	attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
  }

  if ( includeWidth ) {
	attrs.opacity = attrs.width = type;
  }

  return attrs;
}

// Generate shortcuts for custom animations
jQuery.each({
  slideDown: genFx("show"),
  slideUp: genFx("hide"),
  slideToggle: genFx("toggle"),
  fadeIn: { opacity: "show" },
  fadeOut: { opacity: "hide" },
  fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
  jQuery.fn[ name ] = function( speed, easing, callback ) {
	return this.animate( props, speed, easing, callback );
  };
});

jQuery.speed = function( speed, easing, fn ) {
  var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
	complete: fn || !fn && easing ||
	  jQuery.isFunction( speed ) && speed,
	duration: speed,
	easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
  };

  opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
	opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

  // normalize opt.queue - true/undefined/null -> "fx"
  if ( opt.queue == null || opt.queue === true ) {
	opt.queue = "fx";
  }

  // Queueing
  opt.old = opt.complete;

  opt.complete = function() {
	if ( jQuery.isFunction( opt.old ) ) {
	  opt.old.call( this );
	}

	if ( opt.queue ) {
	  jQuery.dequeue( this, opt.queue );
	}
  };

  return opt;
};

jQuery.easing = {
  linear: function( p ) {
	return p;
  },
  swing: function( p ) {
	return 0.5 - Math.cos( p*Math.PI ) / 2;
  }
};

jQuery.timers = [];
jQuery.fx = Tween.prototype.init;
jQuery.fx.tick = function() {
  var timer,
	timers = jQuery.timers,
	i = 0;

  fxNow = jQuery.now();

  for ( ; i < timers.length; i++ ) {
	timer = timers[ i ];
	// Checks the timer has not already been removed
	if ( !timer() && timers[ i ] === timer ) {
	  timers.splice( i--, 1 );
	}
  }

  if ( !timers.length ) {
	jQuery.fx.stop();
  }
  fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
  if ( timer() && jQuery.timers.push( timer ) ) {
	jQuery.fx.start();
  }
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
  if ( !timerId ) {
	timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
  }
};

jQuery.fx.stop = function() {
  clearInterval( timerId );
  timerId = null;
};

jQuery.fx.speeds = {
  slow: 600,
  fast: 200,
  // Default speed
  _default: 400
};

// Back Compat <1.8 extension point
jQuery.fx.step = {};

if ( jQuery.expr && jQuery.expr.filters ) {
  jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
	  return elem === fn.elem;
	}).length;
  };
}
jQuery.fn.offset = function( options ) {
  if ( arguments.length ) {
	return options === undefined ?
	  this :
	  this.each(function( i ) {
		jQuery.offset.setOffset( this, options, i );
	  });
  }

  var docElem, win,
	box = { top: 0, left: 0 },
	elem = this[ 0 ],
	doc = elem && elem.ownerDocument;

  if ( !doc ) {
	return;
  }

  docElem = doc.documentElement;

  // Make sure it's not a disconnected DOM node
  if ( !jQuery.contains( docElem, elem ) ) {
	return box;
  }

  // If we don't have gBCR, just use 0,0 rather than error
  // BlackBerry 5, iOS 3 (original iPhone)
  if ( typeof elem.getBoundingClientRect !== core_strundefined ) {
	box = elem.getBoundingClientRect();
  }
  win = getWindow( doc );
  return {
	top: box.top  + ( win.pageYOffset || docElem.scrollTop )  - ( docElem.clientTop  || 0 ),
	left: box.left + ( win.pageXOffset || docElem.scrollLeft ) - ( docElem.clientLeft || 0 )
  };
};

jQuery.offset = {

  setOffset: function( elem, options, i ) {
	var position = jQuery.css( elem, "position" );

	// set position first, in-case top/left are set even on static elem
	if ( position === "static" ) {
	  elem.style.position = "relative";
	}

	var curElem = jQuery( elem ),
	  curOffset = curElem.offset(),
	  curCSSTop = jQuery.css( elem, "top" ),
	  curCSSLeft = jQuery.css( elem, "left" ),
	  calculatePosition = ( position === "absolute" || position === "fixed" ) && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1,
	  props = {}, curPosition = {}, curTop, curLeft;

	// need to be able to calculate position if either top or left is auto and position is either absolute or fixed
	if ( calculatePosition ) {
	  curPosition = curElem.position();
	  curTop = curPosition.top;
	  curLeft = curPosition.left;
	} else {
	  curTop = parseFloat( curCSSTop ) || 0;
	  curLeft = parseFloat( curCSSLeft ) || 0;
	}

	if ( jQuery.isFunction( options ) ) {
	  options = options.call( elem, i, curOffset );
	}

	if ( options.top != null ) {
	  props.top = ( options.top - curOffset.top ) + curTop;
	}
	if ( options.left != null ) {
	  props.left = ( options.left - curOffset.left ) + curLeft;
	}

	if ( "using" in options ) {
	  options.using.call( elem, props );
	} else {
	  curElem.css( props );
	}
  }
};


jQuery.fn.extend({

  position: function() {
	if ( !this[ 0 ] ) {
	  return;
	}

	var offsetParent, offset,
	  parentOffset = { top: 0, left: 0 },
	  elem = this[ 0 ];

	// fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is it's only offset parent
	if ( jQuery.css( elem, "position" ) === "fixed" ) {
	  // we assume that getBoundingClientRect is available when computed position is fixed
	  offset = elem.getBoundingClientRect();
	} else {
	  // Get *real* offsetParent
	  offsetParent = this.offsetParent();

	  // Get correct offsets
	  offset = this.offset();
	  if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
		parentOffset = offsetParent.offset();
	  }

	  // Add offsetParent borders
	  parentOffset.top  += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
	  parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
	}

	// Subtract parent offsets and element margins
	// note: when an element has margin: auto the offsetLeft and marginLeft
	// are the same in Safari causing offset.left to incorrectly be 0
	return {
	  top:  offset.top  - parentOffset.top - jQuery.css( elem, "marginTop", true ),
	  left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true)
	};
  },

  offsetParent: function() {
	return this.map(function() {
	  var offsetParent = this.offsetParent || document.documentElement;
	  while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position") === "static" ) ) {
		offsetParent = offsetParent.offsetParent;
	  }
	  return offsetParent || document.documentElement;
	});
  }
});


// Create scrollLeft and scrollTop methods
jQuery.each( {scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function( method, prop ) {
  var top = /Y/.test( prop );

  jQuery.fn[ method ] = function( val ) {
	return jQuery.access( this, function( elem, method, val ) {
	  var win = getWindow( elem );

	  if ( val === undefined ) {
		return win ? (prop in win) ? win[ prop ] :
		  win.document.documentElement[ method ] :
		  elem[ method ];
	  }

	  if ( win ) {
		win.scrollTo(
		  !top ? val : jQuery( win ).scrollLeft(),
		  top ? val : jQuery( win ).scrollTop()
		);

	  } else {
		elem[ method ] = val;
	  }
	}, method, val, arguments.length, null );
  };
});

function getWindow( elem ) {
  return jQuery.isWindow( elem ) ?
	elem :
	elem.nodeType === 9 ?
	  elem.defaultView || elem.parentWindow :
	  false;
}
// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
  jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
	// margin is only for outerHeight, outerWidth
	jQuery.fn[ funcName ] = function( margin, value ) {
	  var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
		extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

	  return jQuery.access( this, function( elem, type, value ) {
		var doc;

		if ( jQuery.isWindow( elem ) ) {
		  // As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
		  // isn't a whole lot we can do. See pull request at this URL for discussion:
		  // https://github.com/jquery/jquery/pull/764
		  return elem.document.documentElement[ "client" + name ];
		}

		// Get document width or height
		if ( elem.nodeType === 9 ) {
		  doc = elem.documentElement;

		  // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
		  // unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
		  return Math.max(
			elem.body[ "scroll" + name ], doc[ "scroll" + name ],
			elem.body[ "offset" + name ], doc[ "offset" + name ],
			doc[ "client" + name ]
		  );
		}

		return value === undefined ?
		  // Get width or height on the element, requesting but not forcing parseFloat
		  jQuery.css( elem, type, extra ) :

		  // Set width or height on the element
		  jQuery.style( elem, type, value, extra );
	  }, type, chainable ? margin : undefined, chainable, null );
	};
  });
});
// Limit scope pollution from any deprecated API
// (function() {

// })();
// Expose jQuery to the global object
window.jQuery = window.$ = jQuery;

// Expose jQuery as an AMD module, but only for AMD loaders that
// understand the issues with loading multiple versions of jQuery
// in a page that all might call define(). The loader will indicate
// they have special allowances for multiple jQuery versions by
// specifying define.amd.jQuery = true. Register as a named module,
// since jQuery can be concatenated with other files that may use define,
// but not use a proper concatenation script that understands anonymous
// AMD modules. A named AMD is safest and most robust way to register.
// Lowercase jquery is used because AMD module names are derived from
// file names, and jQuery is normally delivered in a lowercase file name.
// Do this after creating the global so that if an AMD module wants to call
// noConflict to hide this version of jQuery, it will work.
if ( typeof define === "function" && define.amd && define.amd.jQuery ) {
  define( "jquery", [], function () { return jQuery; } );
}

})( window );

//} FIM DE jquery-1.9.1 9605 linhas

//{ jQuery UI 1.10.4
/*! jQuery UI - v1.10.4 - 2014-01-29
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.position.js, jquery.ui.draggable.js, jquery.ui.droppable.js, jquery.ui.resizable.js, jquery.ui.selectable.js, jquery.ui.sortable.js, jquery.ui.accordion.js, jquery.ui.autocomplete.js, jquery.ui.button.js, jquery.ui.datepicker.js, jquery.ui.dialog.js, jquery.ui.menu.js, jquery.ui.progressbar.js, jquery.ui.slider.js, jquery.ui.spinner.js, jquery.ui.tabs.js, jquery.ui.tooltip.js, jquery.ui.effect.js, jquery.ui.effect-blind.js, jquery.ui.effect-bounce.js, jquery.ui.effect-clip.js, jquery.ui.effect-drop.js, jquery.ui.effect-explode.js, jquery.ui.effect-fade.js, jquery.ui.effect-fold.js, jquery.ui.effect-highlight.js, jquery.ui.effect-pulsate.js, jquery.ui.effect-scale.js, jquery.ui.effect-shake.js, jquery.ui.effect-slide.js, jquery.ui.effect-transfer.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

(function( $, undefined ) {

var uuid = 0,
  runiqueId = /^ui-id-\d+$/;

// $.ui might exist from components with no dependencies, e.g., $.ui.position
$.ui = $.ui || {};

$.extend( $.ui, {
  version: "1.10.4",

  keyCode: {
	BACKSPACE: 8,
	COMMA: 188,
	DELETE: 46,
	DOWN: 40,
	END: 35,
	ENTER: 13,
	ESCAPE: 27,
	HOME: 36,
	LEFT: 37,
	NUMPAD_ADD: 107,
	NUMPAD_DECIMAL: 110,
	NUMPAD_DIVIDE: 111,
	NUMPAD_ENTER: 108,
	NUMPAD_MULTIPLY: 106,
	NUMPAD_SUBTRACT: 109,
	PAGE_DOWN: 34,
	PAGE_UP: 33,
	PERIOD: 190,
	RIGHT: 39,
	SPACE: 32,
	TAB: 9,
	UP: 38
  }
});

// plugins
$.fn.extend({
  focus: (function( orig ) {
	return function( delay, fn ) {
	  return typeof delay === "number" ?
		this.each(function() {
		  var elem = this;
		  setTimeout(function() {
			$( elem ).focus();
			if ( fn ) {
			  fn.call( elem );
			}
		  }, delay );
		}) :
		orig.apply( this, arguments );
	};
  })( $.fn.focus ),

  scrollParent: function() {
	var scrollParent;
	if (($.ui.ie && (/(static|relative)/).test(this.css("position"))) || (/absolute/).test(this.css("position"))) {
	  scrollParent = this.parents().filter(function() {
		return (/(relative|absolute|fixed)/).test($.css(this,"position")) && (/(auto|scroll)/).test($.css(this,"overflow")+$.css(this,"overflow-y")+$.css(this,"overflow-x"));
	  }).eq(0);
	} else {
	  scrollParent = this.parents().filter(function() {
		return (/(auto|scroll)/).test($.css(this,"overflow")+$.css(this,"overflow-y")+$.css(this,"overflow-x"));
	  }).eq(0);
	}

	return (/fixed/).test(this.css("position")) || !scrollParent.length ? $(document) : scrollParent;
  },

  zIndex: function( zIndex ) {
	if ( zIndex !== undefined ) {
	  return this.css( "zIndex", zIndex );
	}

	if ( this.length ) {
	  var elem = $( this[ 0 ] ), position, value;
	  while ( elem.length && elem[ 0 ] !== document ) {
		// Ignore z-index if position is set to a value where z-index is ignored by the browser
		// This makes behavior of this function consistent across browsers
		// WebKit always returns auto if the element is positioned
		position = elem.css( "position" );
		if ( position === "absolute" || position === "relative" || position === "fixed" ) {
		  // IE returns 0 when zIndex is not specified
		  // other browsers return a string
		  // we ignore the case of nested elements with an explicit value of 0
		  // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
		  value = parseInt( elem.css( "zIndex" ), 10 );
		  if ( !isNaN( value ) && value !== 0 ) {
			return value;
		  }
		}
		elem = elem.parent();
	  }
	}

	return 0;
  },

  uniqueId: function() {
	return this.each(function() {
	  if ( !this.id ) {
		this.id = "ui-id-" + (++uuid);
	  }
	});
  },

  removeUniqueId: function() {
	return this.each(function() {
	  if ( runiqueId.test( this.id ) ) {
		$( this ).removeAttr( "id" );
	  }
	});
  }
});

// selectors
function focusable( element, isTabIndexNotNaN ) {
  var map, mapName, img,
	nodeName = element.nodeName.toLowerCase();
  if ( "area" === nodeName ) {
	map = element.parentNode;
	mapName = map.name;
	if ( !element.href || !mapName || map.nodeName.toLowerCase() !== "map" ) {
	  return false;
	}
	img = $( "img[usemap=#" + mapName + "]" )[0];
	return !!img && visible( img );
  }
  return ( /input|select|textarea|button|object/.test( nodeName ) ?
	!element.disabled :
	"a" === nodeName ?
	  element.href || isTabIndexNotNaN :
	  isTabIndexNotNaN) &&
	// the element and all of its ancestors must be visible
	visible( element );
}

function visible( element ) {
  return $.expr.filters.visible( element ) &&
	!$( element ).parents().addBack().filter(function() {
	  return $.css( this, "visibility" ) === "hidden";
	}).length;
}

$.extend( $.expr[ ":" ], {
  data: $.expr.createPseudo ?
	$.expr.createPseudo(function( dataName ) {
	  return function( elem ) {
		return !!$.data( elem, dataName );
	  };
	}) :
	// support: jQuery <1.8
	function( elem, i, match ) {
	  return !!$.data( elem, match[ 3 ] );
	},

  focusable: function( element ) {
	return focusable( element, !isNaN( $.attr( element, "tabindex" ) ) );
  },

  tabbable: function( element ) {
	var tabIndex = $.attr( element, "tabindex" ),
	  isTabIndexNaN = isNaN( tabIndex );
	return ( isTabIndexNaN || tabIndex >= 0 ) && focusable( element, !isTabIndexNaN );
  }
});

// support: jQuery <1.8
if ( !$( "<a>" ).outerWidth( 1 ).jquery ) {
  $.each( [ "Width", "Height" ], function( i, name ) {
	var side = name === "Width" ? [ "Left", "Right" ] : [ "Top", "Bottom" ],
	  type = name.toLowerCase(),
	  orig = {
		innerWidth: $.fn.innerWidth,
		innerHeight: $.fn.innerHeight,
		outerWidth: $.fn.outerWidth,
		outerHeight: $.fn.outerHeight
	  };

	function reduce( elem, size, border, margin ) {
	  $.each( side, function() {
		size -= parseFloat( $.css( elem, "padding" + this ) ) || 0;
		if ( border ) {
		  size -= parseFloat( $.css( elem, "border" + this + "Width" ) ) || 0;
		}
		if ( margin ) {
		  size -= parseFloat( $.css( elem, "margin" + this ) ) || 0;
		}
	  });
	  return size;
	}

	$.fn[ "inner" + name ] = function( size ) {
	  if ( size === undefined ) {
		return orig[ "inner" + name ].call( this );
	  }

	  return this.each(function() {
		$( this ).css( type, reduce( this, size ) + "px" );
	  });
	};

	$.fn[ "outer" + name] = function( size, margin ) {
	  if ( typeof size !== "number" ) {
		return orig[ "outer" + name ].call( this, size );
	  }

	  return this.each(function() {
		$( this).css( type, reduce( this, size, true, margin ) + "px" );
	  });
	};
  });
}

// support: jQuery <1.8
if ( !$.fn.addBack ) {
  $.fn.addBack = function( selector ) {
	return this.add( selector == null ?
	  this.prevObject : this.prevObject.filter( selector )
	);
  };
}

// support: jQuery 1.6.1, 1.6.2 (http://bugs.jquery.com/ticket/9413)
if ( $( "<a>" ).data( "a-b", "a" ).removeData( "a-b" ).data( "a-b" ) ) {
  $.fn.removeData = (function( removeData ) {
	return function( key ) {
	  if ( arguments.length ) {
		return removeData.call( this, $.camelCase( key ) );
	  } else {
		return removeData.call( this );
	  }
	};
  })( $.fn.removeData );
}





// deprecated
$.ui.ie = !!/msie [\w.]+/.exec( navigator.userAgent.toLowerCase() );

$.support.selectstart = "onselectstart" in document.createElement( "div" );
$.fn.extend({
  disableSelection: function() {
	return this.bind( ( $.support.selectstart ? "selectstart" : "mousedown" ) +
	  ".ui-disableSelection", function( event ) {
		event.preventDefault();
	  });
  },

  enableSelection: function() {
	return this.unbind( ".ui-disableSelection" );
  }
});

$.extend( $.ui, {
  // $.ui.plugin is deprecated. Use $.widget() extensions instead.
  plugin: {
	add: function( module, option, set ) {
	  var i,
		proto = $.ui[ module ].prototype;
	  for ( i in set ) {
		proto.plugins[ i ] = proto.plugins[ i ] || [];
		proto.plugins[ i ].push( [ option, set[ i ] ] );
	  }
	},
	call: function( instance, name, args ) {
	  var i,
		set = instance.plugins[ name ];
	  if ( !set || !instance.element[ 0 ].parentNode || instance.element[ 0 ].parentNode.nodeType === 11 ) {
		return;
	  }

	  for ( i = 0; i < set.length; i++ ) {
		if ( instance.options[ set[ i ][ 0 ] ] ) {
		  set[ i ][ 1 ].apply( instance.element, args );
		}
	  }
	}
  },

  // only used by resizable
  hasScroll: function( el, a ) {

	//If overflow is hidden, the element might have extra content, but the user wants to hide it
	if ( $( el ).css( "overflow" ) === "hidden") {
	  return false;
	}

	var scroll = ( a && a === "left" ) ? "scrollLeft" : "scrollTop",
	  has = false;

	if ( el[ scroll ] > 0 ) {
	  return true;
	}

	// TODO: determine which cases actually cause this to happen
	// if the element doesn't have the scroll set, see if it's possible to
	// set the scroll
	el[ scroll ] = 1;
	has = ( el[ scroll ] > 0 );
	el[ scroll ] = 0;
	return has;
  }
});

})( jQuery );
(function( $, undefined ) {

var uuid = 0,
  slice = Array.prototype.slice,
  _cleanData = $.cleanData;
$.cleanData = function( elems ) {
  for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
	try {
	  $( elem ).triggerHandler( "remove" );
	// http://bugs.jquery.com/ticket/8235
	} catch( e ) {}
  }
  _cleanData( elems );
};

$.widget = function( name, base, prototype ) {
  var fullName, existingConstructor, constructor, basePrototype,
	// proxiedPrototype allows the provided prototype to remain unmodified
	// so that it can be used as a mixin for multiple widgets (#8876)
	proxiedPrototype = {},
	namespace = name.split( "." )[ 0 ];

  name = name.split( "." )[ 1 ];
  fullName = namespace + "-" + name;

  if ( !prototype ) {
	prototype = base;
	base = $.Widget;
  }

  // create selector for plugin
  $.expr[ ":" ][ fullName.toLowerCase() ] = function( elem ) {
	return !!$.data( elem, fullName );
  };

  $[ namespace ] = $[ namespace ] || {};
  existingConstructor = $[ namespace ][ name ];
  constructor = $[ namespace ][ name ] = function( options, element ) {
	// allow instantiation without "new" keyword
	if ( !this._createWidget ) {
	  return new constructor( options, element );
	}

	// allow instantiation without initializing for simple inheritance
	// must use "new" keyword (the code above always passes args)
	if ( arguments.length ) {
	  this._createWidget( options, element );
	}
  };
  // extend with the existing constructor to carry over any static properties
  $.extend( constructor, existingConstructor, {
	version: prototype.version,
	// copy the object used to create the prototype in case we need to
	// redefine the widget later
	_proto: $.extend( {}, prototype ),
	// track widgets that inherit from this widget in case this widget is
	// redefined after a widget inherits from it
	_childConstructors: []
  });

  basePrototype = new base();
  // we need to make the options hash a property directly on the new instance
  // otherwise we'll modify the options hash on the prototype that we're
  // inheriting from
  basePrototype.options = $.widget.extend( {}, basePrototype.options );
  $.each( prototype, function( prop, value ) {
	if ( !$.isFunction( value ) ) {
	  proxiedPrototype[ prop ] = value;
	  return;
	}
	proxiedPrototype[ prop ] = (function() {
	  var _super = function() {
		  return base.prototype[ prop ].apply( this, arguments );
		},
		_superApply = function( args ) {
		  return base.prototype[ prop ].apply( this, args );
		};
	  return function() {
		var __super = this._super,
		  __superApply = this._superApply,
		  returnValue;

		this._super = _super;
		this._superApply = _superApply;

		returnValue = value.apply( this, arguments );

		this._super = __super;
		this._superApply = __superApply;

		return returnValue;
	  };
	})();
  });
  constructor.prototype = $.widget.extend( basePrototype, {
	// TODO: remove support for widgetEventPrefix
	// always use the name + a colon as the prefix, e.g., draggable:start
	// don't prefix for widgets that aren't DOM-based
	widgetEventPrefix: existingConstructor ? (basePrototype.widgetEventPrefix || name) : name
  }, proxiedPrototype, {
	constructor: constructor,
	namespace: namespace,
	widgetName: name,
	widgetFullName: fullName
  });

  // If this widget is being redefined then we need to find all widgets that
  // are inheriting from it and redefine all of them so that they inherit from
  // the new version of this widget. We're essentially trying to replace one
  // level in the prototype chain.
  if ( existingConstructor ) {
	$.each( existingConstructor._childConstructors, function( i, child ) {
	  var childPrototype = child.prototype;

	  // redefine the child widget using the same prototype that was
	  // originally used, but inherit from the new version of the base
	  $.widget( childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto );
	});
	// remove the list of existing child constructors from the old constructor
	// so the old child constructors can be garbage collected
	delete existingConstructor._childConstructors;
  } else {
	base._childConstructors.push( constructor );
  }

  $.widget.bridge( name, constructor );
};

$.widget.extend = function( target ) {
  var input = slice.call( arguments, 1 ),
	inputIndex = 0,
	inputLength = input.length,
	key,
	value;
  for ( ; inputIndex < inputLength; inputIndex++ ) {
	for ( key in input[ inputIndex ] ) {
	  value = input[ inputIndex ][ key ];
	  if ( input[ inputIndex ].hasOwnProperty( key ) && value !== undefined ) {
		// Clone objects
		if ( $.isPlainObject( value ) ) {
		  target[ key ] = $.isPlainObject( target[ key ] ) ?
			$.widget.extend( {}, target[ key ], value ) :
			// Don't extend strings, arrays, etc. with objects
			$.widget.extend( {}, value );
		// Copy everything else by reference
		} else {
		  target[ key ] = value;
		}
	  }
	}
  }
  return target;
};

$.widget.bridge = function( name, object ) {
  var fullName = object.prototype.widgetFullName || name;
  $.fn[ name ] = function( options ) {
	var isMethodCall = typeof options === "string",
	  args = slice.call( arguments, 1 ),
	  returnValue = this;

	// allow multiple hashes to be passed on init
	options = !isMethodCall && args.length ?
	  $.widget.extend.apply( null, [ options ].concat(args) ) :
	  options;

	if ( isMethodCall ) {
	  this.each(function() {
		var methodValue,
		  instance = $.data( this, fullName );
		if ( !instance ) {
		  return $.error( "cannot call methods on " + name + " prior to initialization; " +
			"attempted to call method '" + options + "'" );
		}
		if ( !$.isFunction( instance[options] ) || options.charAt( 0 ) === "_" ) {
		  return $.error( "no such method '" + options + "' for " + name + " widget instance" );
		}
		methodValue = instance[ options ].apply( instance, args );
		if ( methodValue !== instance && methodValue !== undefined ) {
		  returnValue = methodValue && methodValue.jquery ?
			returnValue.pushStack( methodValue.get() ) :
			methodValue;
		  return false;
		}
	  });
	} else {
	  this.each(function() {
		var instance = $.data( this, fullName );
		if ( instance ) {
		  instance.option( options || {} )._init();
		} else {
		  $.data( this, fullName, new object( options, this ) );
		}
	  });
	}

	return returnValue;
  };
};

$.Widget = function( /* options, element */ ) {};
$.Widget._childConstructors = [];

$.Widget.prototype = {
  widgetName: "widget",
  widgetEventPrefix: "",
  defaultElement: "<div>",
  options: {
	disabled: false,

	// callbacks
	create: null
  },
  _createWidget: function( options, element ) {
	element = $( element || this.defaultElement || this )[ 0 ];
	this.element = $( element );
	this.uuid = uuid++;
	this.eventNamespace = "." + this.widgetName + this.uuid;
	this.options = $.widget.extend( {},
	  this.options,
	  this._getCreateOptions(),
	  options );

	this.bindings = $();
	this.hoverable = $();
	this.focusable = $();

	if ( element !== this ) {
	  $.data( element, this.widgetFullName, this );
	  this._on( true, this.element, {
		remove: function( event ) {
		  if ( event.target === element ) {
			this.destroy();
		  }
		}
	  });
	  this.document = $( element.style ?
		// element within the document
		element.ownerDocument :
		// element is window or document
		element.document || element );
	  this.window = $( this.document[0].defaultView || this.document[0].parentWindow );
	}

	this._create();
	this._trigger( "create", null, this._getCreateEventData() );
	this._init();
  },
  _getCreateOptions: $.noop,
  _getCreateEventData: $.noop,
  _create: $.noop,
  _init: $.noop,

  destroy: function() {
	this._destroy();
	// we can probably remove the unbind calls in 2.0
	// all event bindings should go through this._on()
	this.element
	  .unbind( this.eventNamespace )
	  // 1.9 BC for #7810
	  // TODO remove dual storage
	  .removeData( this.widgetName )
	  .removeData( this.widgetFullName )
	  // support: jquery <1.6.3
	  // http://bugs.jquery.com/ticket/9413
	  .removeData( $.camelCase( this.widgetFullName ) );
	this.widget()
	  .unbind( this.eventNamespace )
	  .removeAttr( "aria-disabled" )
	  .removeClass(
		this.widgetFullName + "-disabled " +
		"ui-state-disabled" );

	// clean up events and states
	this.bindings.unbind( this.eventNamespace );
	this.hoverable.removeClass( "ui-state-hover" );
	this.focusable.removeClass( "ui-state-focus" );
  },
  _destroy: $.noop,

  widget: function() {
	return this.element;
  },

  option: function( key, value ) {
	var options = key,
	  parts,
	  curOption,
	  i;

	if ( arguments.length === 0 ) {
	  // don't return a reference to the internal hash
	  return $.widget.extend( {}, this.options );
	}

	if ( typeof key === "string" ) {
	  // handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
	  options = {};
	  parts = key.split( "." );
	  key = parts.shift();
	  if ( parts.length ) {
		curOption = options[ key ] = $.widget.extend( {}, this.options[ key ] );
		for ( i = 0; i < parts.length - 1; i++ ) {
		  curOption[ parts[ i ] ] = curOption[ parts[ i ] ] || {};
		  curOption = curOption[ parts[ i ] ];
		}
		key = parts.pop();
		if ( arguments.length === 1 ) {
		  return curOption[ key ] === undefined ? null : curOption[ key ];
		}
		curOption[ key ] = value;
	  } else {
		if ( arguments.length === 1 ) {
		  return this.options[ key ] === undefined ? null : this.options[ key ];
		}
		options[ key ] = value;
	  }
	}

	this._setOptions( options );

	return this;
  },
  _setOptions: function( options ) {
	var key;

	for ( key in options ) {
	  this._setOption( key, options[ key ] );
	}

	return this;
  },
  _setOption: function( key, value ) {
	this.options[ key ] = value;

	if ( key === "disabled" ) {
	  this.widget()
		.toggleClass( this.widgetFullName + "-disabled ui-state-disabled", !!value )
		.attr( "aria-disabled", value );
	  this.hoverable.removeClass( "ui-state-hover" );
	  this.focusable.removeClass( "ui-state-focus" );
	}

	return this;
  },

  enable: function() {
	return this._setOption( "disabled", false );
  },
  disable: function() {
	return this._setOption( "disabled", true );
  },

  _on: function( suppressDisabledCheck, element, handlers ) {
	var delegateElement,
	  instance = this;

	// no suppressDisabledCheck flag, shuffle arguments
	if ( typeof suppressDisabledCheck !== "boolean" ) {
	  handlers = element;
	  element = suppressDisabledCheck;
	  suppressDisabledCheck = false;
	}

	// no element argument, shuffle and use this.element
	if ( !handlers ) {
	  handlers = element;
	  element = this.element;
	  delegateElement = this.widget();
	} else {
	  // accept selectors, DOM elements
	  element = delegateElement = $( element );
	  this.bindings = this.bindings.add( element );
	}

	$.each( handlers, function( event, handler ) {
	  function handlerProxy() {
		// allow widgets to customize the disabled handling
		// - disabled as an array instead of boolean
		// - disabled class as method for disabling individual parts
		if ( !suppressDisabledCheck &&
			( instance.options.disabled === true ||
			  $( this ).hasClass( "ui-state-disabled" ) ) ) {
		  return;
		}
		return ( typeof handler === "string" ? instance[ handler ] : handler )
		  .apply( instance, arguments );
	  }

	  // copy the guid so direct unbinding works
	  if ( typeof handler !== "string" ) {
		handlerProxy.guid = handler.guid =
		  handler.guid || handlerProxy.guid || $.guid++;
	  }

	  var match = event.match( /^(\w+)\s*(.*)$/ ),
		eventName = match[1] + instance.eventNamespace,
		selector = match[2];
	  if ( selector ) {
		delegateElement.delegate( selector, eventName, handlerProxy );
	  } else {
		element.bind( eventName, handlerProxy );
	  }
	});
  },

  _off: function( element, eventName ) {
	eventName = (eventName || "").split( " " ).join( this.eventNamespace + " " ) + this.eventNamespace;
	element.unbind( eventName ).undelegate( eventName );
  },

  _delay: function( handler, delay ) {
	function handlerProxy() {
	  return ( typeof handler === "string" ? instance[ handler ] : handler )
		.apply( instance, arguments );
	}
	var instance = this;
	return setTimeout( handlerProxy, delay || 0 );
  },

  _hoverable: function( element ) {
	this.hoverable = this.hoverable.add( element );
	this._on( element, {
	  mouseenter: function( event ) {
		$( event.currentTarget ).addClass( "ui-state-hover" );
	  },
	  mouseleave: function( event ) {
		$( event.currentTarget ).removeClass( "ui-state-hover" );
	  }
	});
  },

  _focusable: function( element ) {
	this.focusable = this.focusable.add( element );
	this._on( element, {
	  focusin: function( event ) {
		$( event.currentTarget ).addClass( "ui-state-focus" );
	  },
	  focusout: function( event ) {
		$( event.currentTarget ).removeClass( "ui-state-focus" );
	  }
	});
  },

  _trigger: function( type, event, data ) {
	var prop, orig,
	  callback = this.options[ type ];

	data = data || {};
	event = $.Event( event );
	event.type = ( type === this.widgetEventPrefix ?
	  type :
	  this.widgetEventPrefix + type ).toLowerCase();
	// the original event may come from any element
	// so we need to reset the target on the new event
	event.target = this.element[ 0 ];

	// copy original event properties over to the new event
	orig = event.originalEvent;
	if ( orig ) {
	  for ( prop in orig ) {
		if ( !( prop in event ) ) {
		  event[ prop ] = orig[ prop ];
		}
	  }
	}

	this.element.trigger( event, data );
	return !( $.isFunction( callback ) &&
	  callback.apply( this.element[0], [ event ].concat( data ) ) === false ||
	  event.isDefaultPrevented() );
  }
};

$.each( { show: "fadeIn", hide: "fadeOut" }, function( method, defaultEffect ) {
  $.Widget.prototype[ "_" + method ] = function( element, options, callback ) {
	if ( typeof options === "string" ) {
	  options = { effect: options };
	}
	var hasOptions,
	  effectName = !options ?
		method :
		options === true || typeof options === "number" ?
		  defaultEffect :
		  options.effect || defaultEffect;
	options = options || {};
	if ( typeof options === "number" ) {
	  options = { duration: options };
	}
	hasOptions = !$.isEmptyObject( options );
	options.complete = callback;
	if ( options.delay ) {
	  element.delay( options.delay );
	}
	if ( hasOptions && $.effects && $.effects.effect[ effectName ] ) {
	  element[ method ]( options );
	} else if ( effectName !== method && element[ effectName ] ) {
	  element[ effectName ]( options.duration, options.easing, callback );
	} else {
	  element.queue(function( next ) {
		$( this )[ method ]();
		if ( callback ) {
		  callback.call( element[ 0 ] );
		}
		next();
	  });
	}
  };
});

})( jQuery );
(function( $, undefined ) {

var mouseHandled = false;
$( document ).mouseup( function() {
  mouseHandled = false;
});

$.widget("ui.mouse", {
  version: "1.10.4",
  options: {
	cancel: "input,textarea,button,select,option",
	distance: 1,
	delay: 0
  },
  _mouseInit: function() {
	var that = this;

	this.element
	  .bind("mousedown."+this.widgetName, function(event) {
		return that._mouseDown(event);
	  })
	  .bind("click."+this.widgetName, function(event) {
		if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {
		  $.removeData(event.target, that.widgetName + ".preventClickEvent");
		  event.stopImmediatePropagation();
		  return false;
		}
	  });

	this.started = false;
  },

  // TODO: make sure destroying one instance of mouse doesn't mess with
  // other instances of mouse
  _mouseDestroy: function() {
	this.element.unbind("."+this.widgetName);
	if ( this._mouseMoveDelegate ) {
	  $(document)
		.unbind("mousemove."+this.widgetName, this._mouseMoveDelegate)
		.unbind("mouseup."+this.widgetName, this._mouseUpDelegate);
	}
  },

  _mouseDown: function(event) {
	// don't let more than one widget handle mouseStart
	if( mouseHandled ) { return; }

	// we may have missed mouseup (out of window)
	(this._mouseStarted && this._mouseUp(event));

	this._mouseDownEvent = event;

	var that = this,
	  btnIsLeft = (event.which === 1),
	  // event.target.nodeName works around a bug in IE 8 with
	  // disabled inputs (#7620)
	  elIsCancel = (typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
	if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
	  return true;
	}

	this.mouseDelayMet = !this.options.delay;
	if (!this.mouseDelayMet) {
	  this._mouseDelayTimer = setTimeout(function() {
		that.mouseDelayMet = true;
	  }, this.options.delay);
	}

	if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
	  this._mouseStarted = (this._mouseStart(event) !== false);
	  if (!this._mouseStarted) {
		event.preventDefault();
		return true;
	  }
	}

	// Click event may never have fired (Gecko & Opera)
	if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {
	  $.removeData(event.target, this.widgetName + ".preventClickEvent");
	}

	// these delegates are required to keep context
	this._mouseMoveDelegate = function(event) {
	  return that._mouseMove(event);
	};
	this._mouseUpDelegate = function(event) {
	  return that._mouseUp(event);
	};
	$(document)
	  .bind("mousemove."+this.widgetName, this._mouseMoveDelegate)
	  .bind("mouseup."+this.widgetName, this._mouseUpDelegate);

	event.preventDefault();

	mouseHandled = true;
	return true;
  },

  _mouseMove: function(event) {
	// IE mouseup check - mouseup happened when mouse was out of window
	if ($.ui.ie && ( !document.documentMode || document.documentMode < 9 ) && !event.button) {
	  return this._mouseUp(event);
	}

	if (this._mouseStarted) {
	  this._mouseDrag(event);
	  return event.preventDefault();
	}

	if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
	  this._mouseStarted =
		(this._mouseStart(this._mouseDownEvent, event) !== false);
	  (this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event));
	}

	return !this._mouseStarted;
  },

  _mouseUp: function(event) {
	$(document)
	  .unbind("mousemove."+this.widgetName, this._mouseMoveDelegate)
	  .unbind("mouseup."+this.widgetName, this._mouseUpDelegate);

	if (this._mouseStarted) {
	  this._mouseStarted = false;

	  if (event.target === this._mouseDownEvent.target) {
		$.data(event.target, this.widgetName + ".preventClickEvent", true);
	  }

	  this._mouseStop(event);
	}

	return false;
  },

  _mouseDistanceMet: function(event) {
	return (Math.max(
		Math.abs(this._mouseDownEvent.pageX - event.pageX),
		Math.abs(this._mouseDownEvent.pageY - event.pageY)
	  ) >= this.options.distance
	);
  },

  _mouseDelayMet: function(/* event */) {
	return this.mouseDelayMet;
  },

  // These are placeholder methods, to be overriden by extending plugin
  _mouseStart: function(/* event */) {},
  _mouseDrag: function(/* event */) {},
  _mouseStop: function(/* event */) {},
  _mouseCapture: function(/* event */) { return true; }
});

})(jQuery);
(function( $, undefined ) {

$.ui = $.ui || {};

var cachedScrollbarWidth,
  max = Math.max,
  abs = Math.abs,
  round = Math.round,
  rhorizontal = /left|center|right/,
  rvertical = /top|center|bottom/,
  roffset = /[\+\-]\d+(\.[\d]+)?%?/,
  rposition = /^\w+/,
  rpercent = /%$/,
  _position = $.fn.position;

function getOffsets( offsets, width, height ) {
  return [
	parseFloat( offsets[ 0 ] ) * ( rpercent.test( offsets[ 0 ] ) ? width / 100 : 1 ),
	parseFloat( offsets[ 1 ] ) * ( rpercent.test( offsets[ 1 ] ) ? height / 100 : 1 )
  ];
}

function parseCss( element, property ) {
  return parseInt( $.css( element, property ), 10 ) || 0;
}

function getDimensions( elem ) {
  var raw = elem[0];
  if ( raw.nodeType === 9 ) {
	return {
	  width: elem.width(),
	  height: elem.height(),
	  offset: { top: 0, left: 0 }
	};
  }
  if ( $.isWindow( raw ) ) {
	return {
	  width: elem.width(),
	  height: elem.height(),
	  offset: { top: elem.scrollTop(), left: elem.scrollLeft() }
	};
  }
  if ( raw.preventDefault ) {
	return {
	  width: 0,
	  height: 0,
	  offset: { top: raw.pageY, left: raw.pageX }
	};
  }
  return {
	width: elem.outerWidth(),
	height: elem.outerHeight(),
	offset: elem.offset()
  };
}

$.position = {
  scrollbarWidth: function() {
	if ( cachedScrollbarWidth !== undefined ) {
	  return cachedScrollbarWidth;
	}
	var w1, w2,
	  div = $( "<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>" ),
	  innerDiv = div.children()[0];

	$( "body" ).append( div );
	w1 = innerDiv.offsetWidth;
	div.css( "overflow", "scroll" );

	w2 = innerDiv.offsetWidth;

	if ( w1 === w2 ) {
	  w2 = div[0].clientWidth;
	}

	div.remove();

	return (cachedScrollbarWidth = w1 - w2);
  },
  getScrollInfo: function( within ) {
	var overflowX = within.isWindow || within.isDocument ? "" :
		within.element.css( "overflow-x" ),
	  overflowY = within.isWindow || within.isDocument ? "" :
		within.element.css( "overflow-y" ),
	  hasOverflowX = overflowX === "scroll" ||
		( overflowX === "auto" && within.width < within.element[0].scrollWidth ),
	  hasOverflowY = overflowY === "scroll" ||
		( overflowY === "auto" && within.height < within.element[0].scrollHeight );
	return {
	  width: hasOverflowY ? $.position.scrollbarWidth() : 0,
	  height: hasOverflowX ? $.position.scrollbarWidth() : 0
	};
  },
  getWithinInfo: function( element ) {
	var withinElement = $( element || window ),
	  isWindow = $.isWindow( withinElement[0] ),
	  isDocument = !!withinElement[ 0 ] && withinElement[ 0 ].nodeType === 9;
	return {
	  element: withinElement,
	  isWindow: isWindow,
	  isDocument: isDocument,
	  offset: withinElement.offset() || { left: 0, top: 0 },
	  scrollLeft: withinElement.scrollLeft(),
	  scrollTop: withinElement.scrollTop(),
	  width: isWindow ? withinElement.width() : withinElement.outerWidth(),
	  height: isWindow ? withinElement.height() : withinElement.outerHeight()
	};
  }
};

$.fn.position = function( options ) {
  if ( !options || !options.of ) {
	return _position.apply( this, arguments );
  }

  // make a copy, we don't want to modify arguments
  options = $.extend( {}, options );

  var atOffset, targetWidth, targetHeight, targetOffset, basePosition, dimensions,
	target = $( options.of ),
	within = $.position.getWithinInfo( options.within ),
	scrollInfo = $.position.getScrollInfo( within ),
	collision = ( options.collision || "flip" ).split( " " ),
	offsets = {};

  dimensions = getDimensions( target );
  if ( target[0].preventDefault ) {
	// force left top to allow flipping
	options.at = "left top";
  }
  targetWidth = dimensions.width;
  targetHeight = dimensions.height;
  targetOffset = dimensions.offset;
  // clone to reuse original targetOffset later
  basePosition = $.extend( {}, targetOffset );

  // force my and at to have valid horizontal and vertical positions
  // if a value is missing or invalid, it will be converted to center
  $.each( [ "my", "at" ], function() {
	var pos = ( options[ this ] || "" ).split( " " ),
	  horizontalOffset,
	  verticalOffset;

	if ( pos.length === 1) {
	  pos = rhorizontal.test( pos[ 0 ] ) ?
		pos.concat( [ "center" ] ) :
		rvertical.test( pos[ 0 ] ) ?
		  [ "center" ].concat( pos ) :
		  [ "center", "center" ];
	}
	pos[ 0 ] = rhorizontal.test( pos[ 0 ] ) ? pos[ 0 ] : "center";
	pos[ 1 ] = rvertical.test( pos[ 1 ] ) ? pos[ 1 ] : "center";

	// calculate offsets
	horizontalOffset = roffset.exec( pos[ 0 ] );
	verticalOffset = roffset.exec( pos[ 1 ] );
	offsets[ this ] = [
	  horizontalOffset ? horizontalOffset[ 0 ] : 0,
	  verticalOffset ? verticalOffset[ 0 ] : 0
	];

	// reduce to just the positions without the offsets
	options[ this ] = [
	  rposition.exec( pos[ 0 ] )[ 0 ],
	  rposition.exec( pos[ 1 ] )[ 0 ]
	];
  });

  // normalize collision option
  if ( collision.length === 1 ) {
	collision[ 1 ] = collision[ 0 ];
  }

  if ( options.at[ 0 ] === "right" ) {
	basePosition.left += targetWidth;
  } else if ( options.at[ 0 ] === "center" ) {
	basePosition.left += targetWidth / 2;
  }

  if ( options.at[ 1 ] === "bottom" ) {
	basePosition.top += targetHeight;
  } else if ( options.at[ 1 ] === "center" ) {
	basePosition.top += targetHeight / 2;
  }

  atOffset = getOffsets( offsets.at, targetWidth, targetHeight );
  basePosition.left += atOffset[ 0 ];
  basePosition.top += atOffset[ 1 ];

  return this.each(function() {
	var collisionPosition, using,
	  elem = $( this ),
	  elemWidth = elem.outerWidth(),
	  elemHeight = elem.outerHeight(),
	  marginLeft = parseCss( this, "marginLeft" ),
	  marginTop = parseCss( this, "marginTop" ),
	  collisionWidth = elemWidth + marginLeft + parseCss( this, "marginRight" ) + scrollInfo.width,
	  collisionHeight = elemHeight + marginTop + parseCss( this, "marginBottom" ) + scrollInfo.height,
	  position = $.extend( {}, basePosition ),
	  myOffset = getOffsets( offsets.my, elem.outerWidth(), elem.outerHeight() );

	if ( options.my[ 0 ] === "right" ) {
	  position.left -= elemWidth;
	} else if ( options.my[ 0 ] === "center" ) {
	  position.left -= elemWidth / 2;
	}

	if ( options.my[ 1 ] === "bottom" ) {
	  position.top -= elemHeight;
	} else if ( options.my[ 1 ] === "center" ) {
	  position.top -= elemHeight / 2;
	}

	position.left += myOffset[ 0 ];
	position.top += myOffset[ 1 ];

	// if the browser doesn't support fractions, then round for consistent results
	if ( !$.support.offsetFractions ) {
	  position.left = round( position.left );
	  position.top = round( position.top );
	}

	collisionPosition = {
	  marginLeft: marginLeft,
	  marginTop: marginTop
	};

	$.each( [ "left", "top" ], function( i, dir ) {
	  if ( $.ui.position[ collision[ i ] ] ) {
		$.ui.position[ collision[ i ] ][ dir ]( position, {
		  targetWidth: targetWidth,
		  targetHeight: targetHeight,
		  elemWidth: elemWidth,
		  elemHeight: elemHeight,
		  collisionPosition: collisionPosition,
		  collisionWidth: collisionWidth,
		  collisionHeight: collisionHeight,
		  offset: [ atOffset[ 0 ] + myOffset[ 0 ], atOffset [ 1 ] + myOffset[ 1 ] ],
		  my: options.my,
		  at: options.at,
		  within: within,
		  elem : elem
		});
	  }
	});

	if ( options.using ) {
	  // adds feedback as second argument to using callback, if present
	  using = function( props ) {
		var left = targetOffset.left - position.left,
		  right = left + targetWidth - elemWidth,
		  top = targetOffset.top - position.top,
		  bottom = top + targetHeight - elemHeight,
		  feedback = {
			target: {
			  element: target,
			  left: targetOffset.left,
			  top: targetOffset.top,
			  width: targetWidth,
			  height: targetHeight
			},
			element: {
			  element: elem,
			  left: position.left,
			  top: position.top,
			  width: elemWidth,
			  height: elemHeight
			},
			horizontal: right < 0 ? "left" : left > 0 ? "right" : "center",
			vertical: bottom < 0 ? "top" : top > 0 ? "bottom" : "middle"
		  };
		if ( targetWidth < elemWidth && abs( left + right ) < targetWidth ) {
		  feedback.horizontal = "center";
		}
		if ( targetHeight < elemHeight && abs( top + bottom ) < targetHeight ) {
		  feedback.vertical = "middle";
		}
		if ( max( abs( left ), abs( right ) ) > max( abs( top ), abs( bottom ) ) ) {
		  feedback.important = "horizontal";
		} else {
		  feedback.important = "vertical";
		}
		options.using.call( this, props, feedback );
	  };
	}

	elem.offset( $.extend( position, { using: using } ) );
  });
};

$.ui.position = {
  fit: {
	left: function( position, data ) {
	  var within = data.within,
		withinOffset = within.isWindow ? within.scrollLeft : within.offset.left,
		outerWidth = within.width,
		collisionPosLeft = position.left - data.collisionPosition.marginLeft,
		overLeft = withinOffset - collisionPosLeft,
		overRight = collisionPosLeft + data.collisionWidth - outerWidth - withinOffset,
		newOverRight;

	  // element is wider than within
	  if ( data.collisionWidth > outerWidth ) {
		// element is initially over the left side of within
		if ( overLeft > 0 && overRight <= 0 ) {
		  newOverRight = position.left + overLeft + data.collisionWidth - outerWidth - withinOffset;
		  position.left += overLeft - newOverRight;
		// element is initially over right side of within
		} else if ( overRight > 0 && overLeft <= 0 ) {
		  position.left = withinOffset;
		// element is initially over both left and right sides of within
		} else {
		  if ( overLeft > overRight ) {
			position.left = withinOffset + outerWidth - data.collisionWidth;
		  } else {
			position.left = withinOffset;
		  }
		}
	  // too far left -> align with left edge
	  } else if ( overLeft > 0 ) {
		position.left += overLeft;
	  // too far right -> align with right edge
	  } else if ( overRight > 0 ) {
		position.left -= overRight;
	  // adjust based on position and margin
	  } else {
		position.left = max( position.left - collisionPosLeft, position.left );
	  }
	},
	top: function( position, data ) {
	  var within = data.within,
		withinOffset = within.isWindow ? within.scrollTop : within.offset.top,
		outerHeight = data.within.height,
		collisionPosTop = position.top - data.collisionPosition.marginTop,
		overTop = withinOffset - collisionPosTop,
		overBottom = collisionPosTop + data.collisionHeight - outerHeight - withinOffset,
		newOverBottom;

	  // element is taller than within
	  if ( data.collisionHeight > outerHeight ) {
		// element is initially over the top of within
		if ( overTop > 0 && overBottom <= 0 ) {
		  newOverBottom = position.top + overTop + data.collisionHeight - outerHeight - withinOffset;
		  position.top += overTop - newOverBottom;
		// element is initially over bottom of within
		} else if ( overBottom > 0 && overTop <= 0 ) {
		  position.top = withinOffset;
		// element is initially over both top and bottom of within
		} else {
		  if ( overTop > overBottom ) {
			position.top = withinOffset + outerHeight - data.collisionHeight;
		  } else {
			position.top = withinOffset;
		  }
		}
	  // too far up -> align with top
	  } else if ( overTop > 0 ) {
		position.top += overTop;
	  // too far down -> align with bottom edge
	  } else if ( overBottom > 0 ) {
		position.top -= overBottom;
	  // adjust based on position and margin
	  } else {
		position.top = max( position.top - collisionPosTop, position.top );
	  }
	}
  },
  flip: {
	left: function( position, data ) {
	  var within = data.within,
		withinOffset = within.offset.left + within.scrollLeft,
		outerWidth = within.width,
		offsetLeft = within.isWindow ? within.scrollLeft : within.offset.left,
		collisionPosLeft = position.left - data.collisionPosition.marginLeft,
		overLeft = collisionPosLeft - offsetLeft,
		overRight = collisionPosLeft + data.collisionWidth - outerWidth - offsetLeft,
		myOffset = data.my[ 0 ] === "left" ?
		  -data.elemWidth :
		  data.my[ 0 ] === "right" ?
			data.elemWidth :
			0,
		atOffset = data.at[ 0 ] === "left" ?
		  data.targetWidth :
		  data.at[ 0 ] === "right" ?
			-data.targetWidth :
			0,
		offset = -2 * data.offset[ 0 ],
		newOverRight,
		newOverLeft;

	  if ( overLeft < 0 ) {
		newOverRight = position.left + myOffset + atOffset + offset + data.collisionWidth - outerWidth - withinOffset;
		if ( newOverRight < 0 || newOverRight < abs( overLeft ) ) {
		  position.left += myOffset + atOffset + offset;
		}
	  }
	  else if ( overRight > 0 ) {
		newOverLeft = position.left - data.collisionPosition.marginLeft + myOffset + atOffset + offset - offsetLeft;
		if ( newOverLeft > 0 || abs( newOverLeft ) < overRight ) {
		  position.left += myOffset + atOffset + offset;
		}
	  }
	},
	top: function( position, data ) {
	  var within = data.within,
		withinOffset = within.offset.top + within.scrollTop,
		outerHeight = within.height,
		offsetTop = within.isWindow ? within.scrollTop : within.offset.top,
		collisionPosTop = position.top - data.collisionPosition.marginTop,
		overTop = collisionPosTop - offsetTop,
		overBottom = collisionPosTop + data.collisionHeight - outerHeight - offsetTop,
		top = data.my[ 1 ] === "top",
		myOffset = top ?
		  -data.elemHeight :
		  data.my[ 1 ] === "bottom" ?
			data.elemHeight :
			0,
		atOffset = data.at[ 1 ] === "top" ?
		  data.targetHeight :
		  data.at[ 1 ] === "bottom" ?
			-data.targetHeight :
			0,
		offset = -2 * data.offset[ 1 ],
		newOverTop,
		newOverBottom;
	  if ( overTop < 0 ) {
		newOverBottom = position.top + myOffset + atOffset + offset + data.collisionHeight - outerHeight - withinOffset;
		if ( ( position.top + myOffset + atOffset + offset) > overTop && ( newOverBottom < 0 || newOverBottom < abs( overTop ) ) ) {
		  position.top += myOffset + atOffset + offset;
		}
	  }
	  else if ( overBottom > 0 ) {
		newOverTop = position.top - data.collisionPosition.marginTop + myOffset + atOffset + offset - offsetTop;
		if ( ( position.top + myOffset + atOffset + offset) > overBottom && ( newOverTop > 0 || abs( newOverTop ) < overBottom ) ) {
		  position.top += myOffset + atOffset + offset;
		}
	  }
	}
  },
  flipfit: {
	left: function() {
	  $.ui.position.flip.left.apply( this, arguments );
	  $.ui.position.fit.left.apply( this, arguments );
	},
	top: function() {
	  $.ui.position.flip.top.apply( this, arguments );
	  $.ui.position.fit.top.apply( this, arguments );
	}
  }
};

// fraction support test
(function () {
  var testElement, testElementParent, testElementStyle, offsetLeft, i,
	body = document.getElementsByTagName( "body" )[ 0 ],
	div = document.createElement( "div" );

  //Create a "fake body" for testing based on method used in jQuery.support
  testElement = document.createElement( body ? "div" : "body" );
  testElementStyle = {
	visibility: "hidden",
	width: 0,
	height: 0,
	border: 0,
	margin: 0,
	background: "none"
  };
  if ( body ) {
	$.extend( testElementStyle, {
	  position: "absolute",
	  left: "-1000px",
	  top: "-1000px"
	});
  }
  for ( i in testElementStyle ) {
	testElement.style[ i ] = testElementStyle[ i ];
  }
  testElement.appendChild( div );
  testElementParent = body || document.documentElement;
  testElementParent.insertBefore( testElement, testElementParent.firstChild );

  div.style.cssText = "position: absolute; left: 10.7432222px;";

  offsetLeft = $( div ).offset().left;
  $.support.offsetFractions = offsetLeft > 10 && offsetLeft < 11;

  testElement.innerHTML = "";
  testElementParent.removeChild( testElement );
})();

}( jQuery ) );
(function( $, undefined ) {

$.widget("ui.draggable", $.ui.mouse, {
  version: "1.10.4",
  widgetEventPrefix: "drag",
  options: {
	addClasses: true,
	appendTo: "parent",
	axis: false,
	connectToSortable: false,
	containment: false,
	cursor: "auto",
	cursorAt: false,
	grid: false,
	handle: false,
	helper: "original",
	iframeFix: false,
	opacity: false,
	refreshPositions: false,
	revert: false,
	revertDuration: 500,
	scope: "default",
	scroll: true,
	scrollSensitivity: 20,
	scrollSpeed: 20,
	snap: false,
	snapMode: "both",
	snapTolerance: 20,
	stack: false,
	zIndex: false,

	// callbacks
	drag: null,
	start: null,
	stop: null
  },
  _create: function() {

	if (this.options.helper === "original" && !(/^(?:r|a|f)/).test(this.element.css("position"))) {
	  this.element[0].style.position = "relative";
	}
	if (this.options.addClasses){
	  this.element.addClass("ui-draggable");
	}
	if (this.options.disabled){
	  this.element.addClass("ui-draggable-disabled");
	}

	this._mouseInit();

  },

  _destroy: function() {
	this.element.removeClass( "ui-draggable ui-draggable-dragging ui-draggable-disabled" );
	this._mouseDestroy();
  },

  _mouseCapture: function(event) {

	var o = this.options;

	// among others, prevent a drag on a resizable-handle
	if (this.helper || o.disabled || $(event.target).closest(".ui-resizable-handle").length > 0) {
	  return false;
	}

	//Quit if we're not on a valid handle
	this.handle = this._getHandle(event);
	if (!this.handle) {
	  return false;
	}

	$(o.iframeFix === true ? "iframe" : o.iframeFix).each(function() {
	  $("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>")
	  .css({
		width: this.offsetWidth+"px", height: this.offsetHeight+"px",
		position: "absolute", opacity: "0.001", zIndex: 1000
	  })
	  .css($(this).offset())
	  .appendTo("body");
	});

	return true;

  },

  _mouseStart: function(event) {

	var o = this.options;

	//Create and append the visible helper
	this.helper = this._createHelper(event);

	this.helper.addClass("ui-draggable-dragging");

	//Cache the helper size
	this._cacheHelperProportions();

	//If ddmanager is used for droppables, set the global draggable
	if($.ui.ddmanager) {
	  $.ui.ddmanager.current = this;
	}

	/*
	 * - Position generation -
	 * This block generates everything position related - it's the core of draggables.
	 */

	//Cache the margins of the original element
	this._cacheMargins();

	//Store the helper's css position
	this.cssPosition = this.helper.css( "position" );
	this.scrollParent = this.helper.scrollParent();
	this.offsetParent = this.helper.offsetParent();
	this.offsetParentCssPosition = this.offsetParent.css( "position" );

	//The element's absolute position on the page minus margins
	this.offset = this.positionAbs = this.element.offset();
	this.offset = {
	  top: this.offset.top - this.margins.top,
	  left: this.offset.left - this.margins.left
	};

	//Reset scroll cache
	this.offset.scroll = false;

	$.extend(this.offset, {
	  click: { //Where the click happened, relative to the element
		left: event.pageX - this.offset.left,
		top: event.pageY - this.offset.top
	  },
	  parent: this._getParentOffset(),
	  relative: this._getRelativeOffset() //This is a relative to absolute position minus the actual position calculation - only used for relative positioned helper
	});

	//Generate the original position
	this.originalPosition = this.position = this._generatePosition(event);
	this.originalPageX = event.pageX;
	this.originalPageY = event.pageY;

	//Adjust the mouse offset relative to the helper if "cursorAt" is supplied
	(o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt));

	//Set a containment if given in the options
	this._setContainment();

	//Trigger event + callbacks
	if(this._trigger("start", event) === false) {
	  this._clear();
	  return false;
	}

	//Recache the helper size
	this._cacheHelperProportions();

	//Prepare the droppable offsets
	if ($.ui.ddmanager && !o.dropBehaviour) {
	  $.ui.ddmanager.prepareOffsets(this, event);
	}


	this._mouseDrag(event, true); //Execute the drag once - this causes the helper not to be visible before getting its correct position

	//If the ddmanager is used for droppables, inform the manager that dragging has started (see #5003)
	if ( $.ui.ddmanager ) {
	  $.ui.ddmanager.dragStart(this, event);
	}

	return true;
  },

  _mouseDrag: function(event, noPropagation) {
	// reset any necessary cached properties (see #5009)
	if ( this.offsetParentCssPosition === "fixed" ) {
	  this.offset.parent = this._getParentOffset();
	}

	//Compute the helpers position
	this.position = this._generatePosition(event);
	this.positionAbs = this._convertPositionTo("absolute");

	//Call plugins and callbacks and use the resulting position if something is returned
	if (!noPropagation) {
	  var ui = this._uiHash();
	  if(this._trigger("drag", event, ui) === false) {
		this._mouseUp({});
		return false;
	  }
	  this.position = ui.position;
	}

	if(!this.options.axis || this.options.axis !== "y") {
	  this.helper[0].style.left = this.position.left+"px";
	}
	if(!this.options.axis || this.options.axis !== "x") {
	  this.helper[0].style.top = this.position.top+"px";
	}
	if($.ui.ddmanager) {
	  $.ui.ddmanager.drag(this, event);
	}

	return false;
  },

  _mouseStop: function(event) {

	//If we are using droppables, inform the manager about the drop
	var that = this,
	  dropped = false;
	if ($.ui.ddmanager && !this.options.dropBehaviour) {
	  dropped = $.ui.ddmanager.drop(this, event);
	}

	//if a drop comes from outside (a sortable)
	if(this.dropped) {
	  dropped = this.dropped;
	  this.dropped = false;
	}

	//if the original element is no longer in the DOM don't bother to continue (see #8269)
	if ( this.options.helper === "original" && !$.contains( this.element[ 0 ].ownerDocument, this.element[ 0 ] ) ) {
	  return false;
	}

	if((this.options.revert === "invalid" && !dropped) || (this.options.revert === "valid" && dropped) || this.options.revert === true || ($.isFunction(this.options.revert) && this.options.revert.call(this.element, dropped))) {
	  $(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
		if(that._trigger("stop", event) !== false) {
		  that._clear();
		}
	  });
	} else {
	  if(this._trigger("stop", event) !== false) {
		this._clear();
	  }
	}

	return false;
  },

  _mouseUp: function(event) {
	//Remove frame helpers
	$("div.ui-draggable-iframeFix").each(function() {
	  this.parentNode.removeChild(this);
	});

	//If the ddmanager is used for droppables, inform the manager that dragging has stopped (see #5003)
	if( $.ui.ddmanager ) {
	  $.ui.ddmanager.dragStop(this, event);
	}

	return $.ui.mouse.prototype._mouseUp.call(this, event);
  },

  cancel: function() {

	if(this.helper.is(".ui-draggable-dragging")) {
	  this._mouseUp({});
	} else {
	  this._clear();
	}

	return this;

  },

  _getHandle: function(event) {
	return this.options.handle ?
	  !!$( event.target ).closest( this.element.find( this.options.handle ) ).length :
	  true;
  },

  _createHelper: function(event) {

	var o = this.options,
	  helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event])) : (o.helper === "clone" ? this.element.clone().removeAttr("id") : this.element);

	if(!helper.parents("body").length) {
	  helper.appendTo((o.appendTo === "parent" ? this.element[0].parentNode : o.appendTo));
	}

	if(helper[0] !== this.element[0] && !(/(fixed|absolute)/).test(helper.css("position"))) {
	  helper.css("position", "absolute");
	}

	return helper;

  },

  _adjustOffsetFromHelper: function(obj) {
	if (typeof obj === "string") {
	  obj = obj.split(" ");
	}
	if ($.isArray(obj)) {
	  obj = {left: +obj[0], top: +obj[1] || 0};
	}
	if ("left" in obj) {
	  this.offset.click.left = obj.left + this.margins.left;
	}
	if ("right" in obj) {
	  this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left;
	}
	if ("top" in obj) {
	  this.offset.click.top = obj.top + this.margins.top;
	}
	if ("bottom" in obj) {
	  this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top;
	}
  },

  _getParentOffset: function() {

	//Get the offsetParent and cache its position
	var po = this.offsetParent.offset();

	// This is a special case where we need to modify a offset calculated on start, since the following happened:
	// 1. The position of the helper is absolute, so it's position is calculated based on the next positioned parent
	// 2. The actual offset parent is a child of the scroll parent, and the scroll parent isn't the document, which means that
	//    the scroll is included in the initial calculation of the offset of the parent, and never recalculated upon drag
	if(this.cssPosition === "absolute" && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) {
	  po.left += this.scrollParent.scrollLeft();
	  po.top += this.scrollParent.scrollTop();
	}

	//This needs to be actually done for all browsers, since pageX/pageY includes this information
	//Ugly IE fix
	if((this.offsetParent[0] === document.body) ||
	  (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() === "html" && $.ui.ie)) {
	  po = { top: 0, left: 0 };
	}

	return {
	  top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"),10) || 0),
	  left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"),10) || 0)
	};

  },

  _getRelativeOffset: function() {

	if(this.cssPosition === "relative") {
	  var p = this.element.position();
	  return {
		top: p.top - (parseInt(this.helper.css("top"),10) || 0) + this.scrollParent.scrollTop(),
		left: p.left - (parseInt(this.helper.css("left"),10) || 0) + this.scrollParent.scrollLeft()
	  };
	} else {
	  return { top: 0, left: 0 };
	}

  },

  _cacheMargins: function() {
	this.margins = {
	  left: (parseInt(this.element.css("marginLeft"),10) || 0),
	  top: (parseInt(this.element.css("marginTop"),10) || 0),
	  right: (parseInt(this.element.css("marginRight"),10) || 0),
	  bottom: (parseInt(this.element.css("marginBottom"),10) || 0)
	};
  },

  _cacheHelperProportions: function() {
	this.helperProportions = {
	  width: this.helper.outerWidth(),
	  height: this.helper.outerHeight()
	};
  },

  _setContainment: function() {

	var over, c, ce,
	  o = this.options;

	if ( !o.containment ) {
	  this.containment = null;
	  return;
	}

	if ( o.containment === "window" ) {
	  this.containment = [
		$( window ).scrollLeft() - this.offset.relative.left - this.offset.parent.left,
		$( window ).scrollTop() - this.offset.relative.top - this.offset.parent.top,
		$( window ).scrollLeft() + $( window ).width() - this.helperProportions.width - this.margins.left,
		$( window ).scrollTop() + ( $( window ).height() || document.body.parentNode.scrollHeight ) - this.helperProportions.height - this.margins.top
	  ];
	  return;
	}

	if ( o.containment === "document") {
	  this.containment = [
		0,
		0,
		$( document ).width() - this.helperProportions.width - this.margins.left,
		( $( document ).height() || document.body.parentNode.scrollHeight ) - this.helperProportions.height - this.margins.top
	  ];
	  return;
	}

	if ( o.containment.constructor === Array ) {
	  this.containment = o.containment;
	  return;
	}

	if ( o.containment === "parent" ) {
	  o.containment = this.helper[ 0 ].parentNode;
	}

	c = $( o.containment );
	ce = c[ 0 ];

	if( !ce ) {
	  return;
	}

	over = c.css( "overflow" ) !== "hidden";

	this.containment = [
	  ( parseInt( c.css( "borderLeftWidth" ), 10 ) || 0 ) + ( parseInt( c.css( "paddingLeft" ), 10 ) || 0 ),
	  ( parseInt( c.css( "borderTopWidth" ), 10 ) || 0 ) + ( parseInt( c.css( "paddingTop" ), 10 ) || 0 ) ,
	  ( over ? Math.max( ce.scrollWidth, ce.offsetWidth ) : ce.offsetWidth ) - ( parseInt( c.css( "borderRightWidth" ), 10 ) || 0 ) - ( parseInt( c.css( "paddingRight" ), 10 ) || 0 ) - this.helperProportions.width - this.margins.left - this.margins.right,
	  ( over ? Math.max( ce.scrollHeight, ce.offsetHeight ) : ce.offsetHeight ) - ( parseInt( c.css( "borderBottomWidth" ), 10 ) || 0 ) - ( parseInt( c.css( "paddingBottom" ), 10 ) || 0 ) - this.helperProportions.height - this.margins.top  - this.margins.bottom
	];
	this.relative_container = c;
  },

  _convertPositionTo: function(d, pos) {

	if(!pos) {
	  pos = this.position;
	}

	var mod = d === "absolute" ? 1 : -1,
	  scroll = this.cssPosition === "absolute" && !( this.scrollParent[ 0 ] !== document && $.contains( this.scrollParent[ 0 ], this.offsetParent[ 0 ] ) ) ? this.offsetParent : this.scrollParent;

	//Cache the scroll
	if (!this.offset.scroll) {
	  this.offset.scroll = {top : scroll.scrollTop(), left : scroll.scrollLeft()};
	}

	return {
	  top: (
		pos.top	+																// The absolute mouse position
		this.offset.relative.top * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.top * mod -										// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : this.offset.scroll.top ) * mod )
	  ),
	  left: (
		pos.left +																// The absolute mouse position
		this.offset.relative.left * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.left * mod	-										// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : this.offset.scroll.left ) * mod )
	  )
	};

  },

  _generatePosition: function(event) {

	var containment, co, top, left,
	  o = this.options,
	  scroll = this.cssPosition === "absolute" && !( this.scrollParent[ 0 ] !== document && $.contains( this.scrollParent[ 0 ], this.offsetParent[ 0 ] ) ) ? this.offsetParent : this.scrollParent,
	  pageX = event.pageX,
	  pageY = event.pageY;

	//Cache the scroll
	if (!this.offset.scroll) {
	  this.offset.scroll = {top : scroll.scrollTop(), left : scroll.scrollLeft()};
	}

	/*
	 * - Position constraining -
	 * Constrain the position to a mix of grid, containment.
	 */

	// If we are not dragging yet, we won't check for options
	if ( this.originalPosition ) {
	  if ( this.containment ) {
		if ( this.relative_container ){
		  co = this.relative_container.offset();
		  containment = [
			this.containment[ 0 ] + co.left,
			this.containment[ 1 ] + co.top,
			this.containment[ 2 ] + co.left,
			this.containment[ 3 ] + co.top
		  ];
		}
		else {
		  containment = this.containment;
		}

		if(event.pageX - this.offset.click.left < containment[0]) {
		  pageX = containment[0] + this.offset.click.left;
		}
		if(event.pageY - this.offset.click.top < containment[1]) {
		  pageY = containment[1] + this.offset.click.top;
		}
		if(event.pageX - this.offset.click.left > containment[2]) {
		  pageX = containment[2] + this.offset.click.left;
		}
		if(event.pageY - this.offset.click.top > containment[3]) {
		  pageY = containment[3] + this.offset.click.top;
		}
	  }

	  if(o.grid) {
		//Check for grid elements set to 0 to prevent divide by 0 error causing invalid argument errors in IE (see ticket #6950)
		top = o.grid[1] ? this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY;
		pageY = containment ? ((top - this.offset.click.top >= containment[1] || top - this.offset.click.top > containment[3]) ? top : ((top - this.offset.click.top >= containment[1]) ? top - o.grid[1] : top + o.grid[1])) : top;

		left = o.grid[0] ? this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX;
		pageX = containment ? ((left - this.offset.click.left >= containment[0] || left - this.offset.click.left > containment[2]) ? left : ((left - this.offset.click.left >= containment[0]) ? left - o.grid[0] : left + o.grid[0])) : left;
	  }

	}

	return {
	  top: (
		pageY -																	// The absolute mouse position
		this.offset.click.top	-												// Click offset (relative to the element)
		this.offset.relative.top -												// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.top +												// The offsetParent's offset without borders (offset + border)
		( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : this.offset.scroll.top )
	  ),
	  left: (
		pageX -																	// The absolute mouse position
		this.offset.click.left -												// Click offset (relative to the element)
		this.offset.relative.left -												// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.left +												// The offsetParent's offset without borders (offset + border)
		( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : this.offset.scroll.left )
	  )
	};

  },

  _clear: function() {
	this.helper.removeClass("ui-draggable-dragging");
	if(this.helper[0] !== this.element[0] && !this.cancelHelperRemoval) {
	  this.helper.remove();
	}
	this.helper = null;
	this.cancelHelperRemoval = false;
  },

  // From now on bulk stuff - mainly helpers

  _trigger: function(type, event, ui) {
	ui = ui || this._uiHash();
	$.ui.plugin.call(this, type, [event, ui]);
	//The absolute position has to be recalculated after plugins
	if(type === "drag") {
	  this.positionAbs = this._convertPositionTo("absolute");
	}
	return $.Widget.prototype._trigger.call(this, type, event, ui);
  },

  plugins: {},

  _uiHash: function() {
	return {
	  helper: this.helper,
	  position: this.position,
	  originalPosition: this.originalPosition,
	  offset: this.positionAbs
	};
  }

});

$.ui.plugin.add("draggable", "connectToSortable", {
  start: function(event, ui) {

	var inst = $(this).data("ui-draggable"), o = inst.options,
	  uiSortable = $.extend({}, ui, { item: inst.element });
	inst.sortables = [];
	$(o.connectToSortable).each(function() {
	  var sortable = $.data(this, "ui-sortable");
	  if (sortable && !sortable.options.disabled) {
		inst.sortables.push({
		  instance: sortable,
		  shouldRevert: sortable.options.revert
		});
		sortable.refreshPositions();	// Call the sortable's refreshPositions at drag start to refresh the containerCache since the sortable container cache is used in drag and needs to be up to date (this will ensure it's initialised as well as being kept in step with any changes that might have happened on the page).
		sortable._trigger("activate", event, uiSortable);
	  }
	});

  },
  stop: function(event, ui) {

	//If we are still over the sortable, we fake the stop event of the sortable, but also remove helper
	var inst = $(this).data("ui-draggable"),
	  uiSortable = $.extend({}, ui, { item: inst.element });

	$.each(inst.sortables, function() {
	  if(this.instance.isOver) {

		this.instance.isOver = 0;

		inst.cancelHelperRemoval = true; //Don't remove the helper in the draggable instance
		this.instance.cancelHelperRemoval = false; //Remove it in the sortable instance (so sortable plugins like revert still work)

		//The sortable revert is supported, and we have to set a temporary dropped variable on the draggable to support revert: "valid/invalid"
		if(this.shouldRevert) {
		  this.instance.options.revert = this.shouldRevert;
		}

		//Trigger the stop of the sortable
		this.instance._mouseStop(event);

		this.instance.options.helper = this.instance.options._helper;

		//If the helper has been the original item, restore properties in the sortable
		if(inst.options.helper === "original") {
		  this.instance.currentItem.css({ top: "auto", left: "auto" });
		}

	  } else {
		this.instance.cancelHelperRemoval = false; //Remove the helper in the sortable instance
		this.instance._trigger("deactivate", event, uiSortable);
	  }

	});

  },
  drag: function(event, ui) {

	var inst = $(this).data("ui-draggable"), that = this;

	$.each(inst.sortables, function() {

	  var innermostIntersecting = false,
		thisSortable = this;

	  //Copy over some variables to allow calling the sortable's native _intersectsWith
	  this.instance.positionAbs = inst.positionAbs;
	  this.instance.helperProportions = inst.helperProportions;
	  this.instance.offset.click = inst.offset.click;

	  if(this.instance._intersectsWith(this.instance.containerCache)) {
		innermostIntersecting = true;
		$.each(inst.sortables, function () {
		  this.instance.positionAbs = inst.positionAbs;
		  this.instance.helperProportions = inst.helperProportions;
		  this.instance.offset.click = inst.offset.click;
		  if (this !== thisSortable &&
			this.instance._intersectsWith(this.instance.containerCache) &&
			$.contains(thisSortable.instance.element[0], this.instance.element[0])
		  ) {
			innermostIntersecting = false;
		  }
		  return innermostIntersecting;
		});
	  }


	  if(innermostIntersecting) {
		//If it intersects, we use a little isOver variable and set it once, so our move-in stuff gets fired only once
		if(!this.instance.isOver) {

		  this.instance.isOver = 1;
		  //Now we fake the start of dragging for the sortable instance,
		  //by cloning the list group item, appending it to the sortable and using it as inst.currentItem
		  //We can then fire the start event of the sortable with our passed browser event, and our own helper (so it doesn't create a new one)
		  this.instance.currentItem = $(that).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", true);
		  this.instance.options._helper = this.instance.options.helper; //Store helper option to later restore it
		  this.instance.options.helper = function() { return ui.helper[0]; };

		  event.target = this.instance.currentItem[0];
		  this.instance._mouseCapture(event, true);
		  this.instance._mouseStart(event, true, true);

		  //Because the browser event is way off the new appended portlet, we modify a couple of variables to reflect the changes
		  this.instance.offset.click.top = inst.offset.click.top;
		  this.instance.offset.click.left = inst.offset.click.left;
		  this.instance.offset.parent.left -= inst.offset.parent.left - this.instance.offset.parent.left;
		  this.instance.offset.parent.top -= inst.offset.parent.top - this.instance.offset.parent.top;

		  inst._trigger("toSortable", event);
		  inst.dropped = this.instance.element; //draggable revert needs that
		  //hack so receive/update callbacks work (mostly)
		  inst.currentItem = inst.element;
		  this.instance.fromOutside = inst;

		}

		//Provided we did all the previous steps, we can fire the drag event of the sortable on every draggable drag, when it intersects with the sortable
		if(this.instance.currentItem) {
		  this.instance._mouseDrag(event);
		}

	  } else {

		//If it doesn't intersect with the sortable, and it intersected before,
		//we fake the drag stop of the sortable, but make sure it doesn't remove the helper by using cancelHelperRemoval
		if(this.instance.isOver) {

		  this.instance.isOver = 0;
		  this.instance.cancelHelperRemoval = true;

		  //Prevent reverting on this forced stop
		  this.instance.options.revert = false;

		  // The out event needs to be triggered independently
		  this.instance._trigger("out", event, this.instance._uiHash(this.instance));

		  this.instance._mouseStop(event, true);
		  this.instance.options.helper = this.instance.options._helper;

		  //Now we remove our currentItem, the list group clone again, and the placeholder, and animate the helper back to it's original size
		  this.instance.currentItem.remove();
		  if(this.instance.placeholder) {
			this.instance.placeholder.remove();
		  }

		  inst._trigger("fromSortable", event);
		  inst.dropped = false; //draggable revert needs that
		}

	  }

	});

  }
});

$.ui.plugin.add("draggable", "cursor", {
  start: function() {
	var t = $("body"), o = $(this).data("ui-draggable").options;
	if (t.css("cursor")) {
	  o._cursor = t.css("cursor");
	}
	t.css("cursor", o.cursor);
  },
  stop: function() {
	var o = $(this).data("ui-draggable").options;
	if (o._cursor) {
	  $("body").css("cursor", o._cursor);
	}
  }
});

$.ui.plugin.add("draggable", "opacity", {
  start: function(event, ui) {
	var t = $(ui.helper), o = $(this).data("ui-draggable").options;
	if(t.css("opacity")) {
	  o._opacity = t.css("opacity");
	}
	t.css("opacity", o.opacity);
  },
  stop: function(event, ui) {
	var o = $(this).data("ui-draggable").options;
	if(o._opacity) {
	  $(ui.helper).css("opacity", o._opacity);
	}
  }
});

$.ui.plugin.add("draggable", "scroll", {
  start: function() {
	var i = $(this).data("ui-draggable");
	if(i.scrollParent[0] !== document && i.scrollParent[0].tagName !== "HTML") {
	  i.overflowOffset = i.scrollParent.offset();
	}
  },
  drag: function( event ) {

	var i = $(this).data("ui-draggable"), o = i.options, scrolled = false;

	if(i.scrollParent[0] !== document && i.scrollParent[0].tagName !== "HTML") {

	  if(!o.axis || o.axis !== "x") {
		if((i.overflowOffset.top + i.scrollParent[0].offsetHeight) - event.pageY < o.scrollSensitivity) {
		  i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop + o.scrollSpeed;
		} else if(event.pageY - i.overflowOffset.top < o.scrollSensitivity) {
		  i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop - o.scrollSpeed;
		}
	  }

	  if(!o.axis || o.axis !== "y") {
		if((i.overflowOffset.left + i.scrollParent[0].offsetWidth) - event.pageX < o.scrollSensitivity) {
		  i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft + o.scrollSpeed;
		} else if(event.pageX - i.overflowOffset.left < o.scrollSensitivity) {
		  i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft - o.scrollSpeed;
		}
	  }

	} else {

	  if(!o.axis || o.axis !== "x") {
		if(event.pageY - $(document).scrollTop() < o.scrollSensitivity) {
		  scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed);
		} else if($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity) {
		  scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed);
		}
	  }

	  if(!o.axis || o.axis !== "y") {
		if(event.pageX - $(document).scrollLeft() < o.scrollSensitivity) {
		  scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed);
		} else if($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity) {
		  scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);
		}
	  }

	}

	if(scrolled !== false && $.ui.ddmanager && !o.dropBehaviour) {
	  $.ui.ddmanager.prepareOffsets(i, event);
	}

  }
});

$.ui.plugin.add("draggable", "snap", {
  start: function() {

	var i = $(this).data("ui-draggable"),
	  o = i.options;

	i.snapElements = [];

	$(o.snap.constructor !== String ? ( o.snap.items || ":data(ui-draggable)" ) : o.snap).each(function() {
	  var $t = $(this),
		$o = $t.offset();
	  if(this !== i.element[0]) {
		i.snapElements.push({
		  item: this,
		  width: $t.outerWidth(), height: $t.outerHeight(),
		  top: $o.top, left: $o.left
		});
	  }
	});

  },
  drag: function(event, ui) {

	var ts, bs, ls, rs, l, r, t, b, i, first,
	  inst = $(this).data("ui-draggable"),
	  o = inst.options,
	  d = o.snapTolerance,
	  x1 = ui.offset.left, x2 = x1 + inst.helperProportions.width,
	  y1 = ui.offset.top, y2 = y1 + inst.helperProportions.height;

	for (i = inst.snapElements.length - 1; i >= 0; i--){

	  l = inst.snapElements[i].left;
	  r = l + inst.snapElements[i].width;
	  t = inst.snapElements[i].top;
	  b = t + inst.snapElements[i].height;

	  if ( x2 < l - d || x1 > r + d || y2 < t - d || y1 > b + d || !$.contains( inst.snapElements[ i ].item.ownerDocument, inst.snapElements[ i ].item ) ) {
		if(inst.snapElements[i].snapping) {
		  (inst.options.snap.release && inst.options.snap.release.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item })));
		}
		inst.snapElements[i].snapping = false;
		continue;
	  }

	  if(o.snapMode !== "inner") {
		ts = Math.abs(t - y2) <= d;
		bs = Math.abs(b - y1) <= d;
		ls = Math.abs(l - x2) <= d;
		rs = Math.abs(r - x1) <= d;
		if(ts) {
		  ui.position.top = inst._convertPositionTo("relative", { top: t - inst.helperProportions.height, left: 0 }).top - inst.margins.top;
		}
		if(bs) {
		  ui.position.top = inst._convertPositionTo("relative", { top: b, left: 0 }).top - inst.margins.top;
		}
		if(ls) {
		  ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l - inst.helperProportions.width }).left - inst.margins.left;
		}
		if(rs) {
		  ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r }).left - inst.margins.left;
		}
	  }

	  first = (ts || bs || ls || rs);

	  if(o.snapMode !== "outer") {
		ts = Math.abs(t - y1) <= d;
		bs = Math.abs(b - y2) <= d;
		ls = Math.abs(l - x1) <= d;
		rs = Math.abs(r - x2) <= d;
		if(ts) {
		  ui.position.top = inst._convertPositionTo("relative", { top: t, left: 0 }).top - inst.margins.top;
		}
		if(bs) {
		  ui.position.top = inst._convertPositionTo("relative", { top: b - inst.helperProportions.height, left: 0 }).top - inst.margins.top;
		}
		if(ls) {
		  ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l }).left - inst.margins.left;
		}
		if(rs) {
		  ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r - inst.helperProportions.width }).left - inst.margins.left;
		}
	  }

	  if(!inst.snapElements[i].snapping && (ts || bs || ls || rs || first)) {
		(inst.options.snap.snap && inst.options.snap.snap.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item })));
	  }
	  inst.snapElements[i].snapping = (ts || bs || ls || rs || first);

	}

  }
});

$.ui.plugin.add("draggable", "stack", {
  start: function() {
	var min,
	  o = this.data("ui-draggable").options,
	  group = $.makeArray($(o.stack)).sort(function(a,b) {
		return (parseInt($(a).css("zIndex"),10) || 0) - (parseInt($(b).css("zIndex"),10) || 0);
	  });

	if (!group.length) { return; }

	min = parseInt($(group[0]).css("zIndex"), 10) || 0;
	$(group).each(function(i) {
	  $(this).css("zIndex", min + i);
	});
	this.css("zIndex", (min + group.length));
  }
});

$.ui.plugin.add("draggable", "zIndex", {
  start: function(event, ui) {
	var t = $(ui.helper), o = $(this).data("ui-draggable").options;
	if(t.css("zIndex")) {
	  o._zIndex = t.css("zIndex");
	}
	t.css("zIndex", o.zIndex);
  },
  stop: function(event, ui) {
	var o = $(this).data("ui-draggable").options;
	if(o._zIndex) {
	  $(ui.helper).css("zIndex", o._zIndex);
	}
  }
});

})(jQuery);
(function( $, undefined ) {

function isOverAxis( x, reference, size ) {
  return ( x > reference ) && ( x < ( reference + size ) );
}

$.widget("ui.droppable", {
  version: "1.10.4",
  widgetEventPrefix: "drop",
  options: {
	accept: "*",
	activeClass: false,
	addClasses: true,
	greedy: false,
	hoverClass: false,
	scope: "default",
	tolerance: "intersect",

	// callbacks
	activate: null,
	deactivate: null,
	drop: null,
	out: null,
	over: null
  },
  _create: function() {

	var proportions,
	  o = this.options,
	  accept = o.accept;

	this.isover = false;
	this.isout = true;

	this.accept = $.isFunction(accept) ? accept : function(d) {
	  return d.is(accept);
	};

	this.proportions = function( /* valueToWrite */ ) {
	  if ( arguments.length ) {
		// Store the droppable's proportions
		proportions = arguments[ 0 ];
	  } else {
		// Retrieve or derive the droppable's proportions
		return proportions ?
		  proportions :
		  proportions = {
			width: this.element[ 0 ].offsetWidth,
			height: this.element[ 0 ].offsetHeight
		  };
	  }
	};

	// Add the reference and positions to the manager
	$.ui.ddmanager.droppables[o.scope] = $.ui.ddmanager.droppables[o.scope] || [];
	$.ui.ddmanager.droppables[o.scope].push(this);

	(o.addClasses && this.element.addClass("ui-droppable"));

  },

  _destroy: function() {
	var i = 0,
	  drop = $.ui.ddmanager.droppables[this.options.scope];

	for ( ; i < drop.length; i++ ) {
	  if ( drop[i] === this ) {
		drop.splice(i, 1);
	  }
	}

	this.element.removeClass("ui-droppable ui-droppable-disabled");
  },

  _setOption: function(key, value) {

	if(key === "accept") {
	  this.accept = $.isFunction(value) ? value : function(d) {
		return d.is(value);
	  };
	}
	$.Widget.prototype._setOption.apply(this, arguments);
  },

  _activate: function(event) {
	var draggable = $.ui.ddmanager.current;
	if(this.options.activeClass) {
	  this.element.addClass(this.options.activeClass);
	}
	if(draggable){
	  this._trigger("activate", event, this.ui(draggable));
	}
  },

  _deactivate: function(event) {
	var draggable = $.ui.ddmanager.current;
	if(this.options.activeClass) {
	  this.element.removeClass(this.options.activeClass);
	}
	if(draggable){
	  this._trigger("deactivate", event, this.ui(draggable));
	}
  },

  _over: function(event) {

	var draggable = $.ui.ddmanager.current;

	// Bail if draggable and droppable are same element
	if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
	  return;
	}

	if (this.accept.call(this.element[0],(draggable.currentItem || draggable.element))) {
	  if(this.options.hoverClass) {
		this.element.addClass(this.options.hoverClass);
	  }
	  this._trigger("over", event, this.ui(draggable));
	}

  },

  _out: function(event) {

	var draggable = $.ui.ddmanager.current;

	// Bail if draggable and droppable are same element
	if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
	  return;
	}

	if (this.accept.call(this.element[0],(draggable.currentItem || draggable.element))) {
	  if(this.options.hoverClass) {
		this.element.removeClass(this.options.hoverClass);
	  }
	  this._trigger("out", event, this.ui(draggable));
	}

  },

  _drop: function(event,custom) {

	var draggable = custom || $.ui.ddmanager.current,
	  childrenIntersection = false;

	// Bail if draggable and droppable are same element
	if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
	  return false;
	}

	this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
	  var inst = $.data(this, "ui-droppable");
	  if(
		inst.options.greedy &&
		!inst.options.disabled &&
		inst.options.scope === draggable.options.scope &&
		inst.accept.call(inst.element[0], (draggable.currentItem || draggable.element)) &&
		$.ui.intersect(draggable, $.extend(inst, { offset: inst.element.offset() }), inst.options.tolerance)
	  ) { childrenIntersection = true; return false; }
	});
	if(childrenIntersection) {
	  return false;
	}

	if(this.accept.call(this.element[0],(draggable.currentItem || draggable.element))) {
	  if(this.options.activeClass) {
		this.element.removeClass(this.options.activeClass);
	  }
	  if(this.options.hoverClass) {
		this.element.removeClass(this.options.hoverClass);
	  }
	  this._trigger("drop", event, this.ui(draggable));
	  return this.element;
	}

	return false;

  },

  ui: function(c) {
	return {
	  draggable: (c.currentItem || c.element),
	  helper: c.helper,
	  position: c.position,
	  offset: c.positionAbs
	};
  }

});

$.ui.intersect = function(draggable, droppable, toleranceMode) {

  if (!droppable.offset) {
	return false;
  }

  var draggableLeft, draggableTop,
	x1 = (draggable.positionAbs || draggable.position.absolute).left,
	y1 = (draggable.positionAbs || draggable.position.absolute).top,
	x2 = x1 + draggable.helperProportions.width,
	y2 = y1 + draggable.helperProportions.height,
	l = droppable.offset.left,
	t = droppable.offset.top,
	r = l + droppable.proportions().width,
	b = t + droppable.proportions().height;

  switch (toleranceMode) {
	case "fit":
	  return (l <= x1 && x2 <= r && t <= y1 && y2 <= b);
	case "intersect":
	  return (l < x1 + (draggable.helperProportions.width / 2) && // Right Half
		x2 - (draggable.helperProportions.width / 2) < r && // Left Half
		t < y1 + (draggable.helperProportions.height / 2) && // Bottom Half
		y2 - (draggable.helperProportions.height / 2) < b ); // Top Half
	case "pointer":
	  draggableLeft = ((draggable.positionAbs || draggable.position.absolute).left + (draggable.clickOffset || draggable.offset.click).left);
	  draggableTop = ((draggable.positionAbs || draggable.position.absolute).top + (draggable.clickOffset || draggable.offset.click).top);
	  return isOverAxis( draggableTop, t, droppable.proportions().height ) && isOverAxis( draggableLeft, l, droppable.proportions().width );
	case "touch":
	  return (
		(y1 >= t && y1 <= b) ||	// Top edge touching
		(y2 >= t && y2 <= b) ||	// Bottom edge touching
		(y1 < t && y2 > b)		// Surrounded vertically
	  ) && (
		(x1 >= l && x1 <= r) ||	// Left edge touching
		(x2 >= l && x2 <= r) ||	// Right edge touching
		(x1 < l && x2 > r)		// Surrounded horizontally
	  );
	default:
	  return false;
	}

};

/*
  This manager tracks offsets of draggables and droppables
*/
$.ui.ddmanager = {
  current: null,
  droppables: { "default": [] },
  prepareOffsets: function(t, event) {

	var i, j,
	  m = $.ui.ddmanager.droppables[t.options.scope] || [],
	  type = event ? event.type : null, // workaround for #2317
	  list = (t.currentItem || t.element).find(":data(ui-droppable)").addBack();

	droppablesLoop: for (i = 0; i < m.length; i++) {

	  //No disabled and non-accepted
	  if(m[i].options.disabled || (t && !m[i].accept.call(m[i].element[0],(t.currentItem || t.element)))) {
		continue;
	  }

	  // Filter out elements in the current dragged item
	  for (j=0; j < list.length; j++) {
		if(list[j] === m[i].element[0]) {
		  m[i].proportions().height = 0;
		  continue droppablesLoop;
		}
	  }

	  m[i].visible = m[i].element.css("display") !== "none";
	  if(!m[i].visible) {
		continue;
	  }

	  //Activate the droppable if used directly from draggables
	  if(type === "mousedown") {
		m[i]._activate.call(m[i], event);
	  }

	  m[ i ].offset = m[ i ].element.offset();
	  m[ i ].proportions({ width: m[ i ].element[ 0 ].offsetWidth, height: m[ i ].element[ 0 ].offsetHeight });

	}

  },
  drop: function(draggable, event) {

	var dropped = false;
	// Create a copy of the droppables in case the list changes during the drop (#9116)
	$.each(($.ui.ddmanager.droppables[draggable.options.scope] || []).slice(), function() {

	  if(!this.options) {
		return;
	  }
	  if (!this.options.disabled && this.visible && $.ui.intersect(draggable, this, this.options.tolerance)) {
		dropped = this._drop.call(this, event) || dropped;
	  }

	  if (!this.options.disabled && this.visible && this.accept.call(this.element[0],(draggable.currentItem || draggable.element))) {
		this.isout = true;
		this.isover = false;
		this._deactivate.call(this, event);
	  }

	});
	return dropped;

  },
  dragStart: function( draggable, event ) {
	//Listen for scrolling so that if the dragging causes scrolling the position of the droppables can be recalculated (see #5003)
	draggable.element.parentsUntil( "body" ).bind( "scroll.droppable", function() {
	  if( !draggable.options.refreshPositions ) {
		$.ui.ddmanager.prepareOffsets( draggable, event );
	  }
	});
  },
  drag: function(draggable, event) {

	//If you have a highly dynamic page, you might try this option. It renders positions every time you move the mouse.
	if(draggable.options.refreshPositions) {
	  $.ui.ddmanager.prepareOffsets(draggable, event);
	}

	//Run through all droppables and check their positions based on specific tolerance options
	$.each($.ui.ddmanager.droppables[draggable.options.scope] || [], function() {

	  if(this.options.disabled || this.greedyChild || !this.visible) {
		return;
	  }

	  var parentInstance, scope, parent,
		intersects = $.ui.intersect(draggable, this, this.options.tolerance),
		c = !intersects && this.isover ? "isout" : (intersects && !this.isover ? "isover" : null);
	  if(!c) {
		return;
	  }

	  if (this.options.greedy) {
		// find droppable parents with same scope
		scope = this.options.scope;
		parent = this.element.parents(":data(ui-droppable)").filter(function () {
		  return $.data(this, "ui-droppable").options.scope === scope;
		});

		if (parent.length) {
		  parentInstance = $.data(parent[0], "ui-droppable");
		  parentInstance.greedyChild = (c === "isover");
		}
	  }

	  // we just moved into a greedy child
	  if (parentInstance && c === "isover") {
		parentInstance.isover = false;
		parentInstance.isout = true;
		parentInstance._out.call(parentInstance, event);
	  }

	  this[c] = true;
	  this[c === "isout" ? "isover" : "isout"] = false;
	  this[c === "isover" ? "_over" : "_out"].call(this, event);

	  // we just moved out of a greedy child
	  if (parentInstance && c === "isout") {
		parentInstance.isout = false;
		parentInstance.isover = true;
		parentInstance._over.call(parentInstance, event);
	  }
	});

  },
  dragStop: function( draggable, event ) {
	draggable.element.parentsUntil( "body" ).unbind( "scroll.droppable" );
	//Call prepareOffsets one final time since IE does not fire return scroll events when overflow was caused by drag (see #5003)
	if( !draggable.options.refreshPositions ) {
	  $.ui.ddmanager.prepareOffsets( draggable, event );
	}
  }
};

})(jQuery);
(function( $, undefined ) {

function num(v) {
  return parseInt(v, 10) || 0;
}

function isNumber(value) {
  return !isNaN(parseInt(value, 10));
}

$.widget("ui.resizable", $.ui.mouse, {
  version: "1.10.4",
  widgetEventPrefix: "resize",
  options: {
	alsoResize: false,
	animate: false,
	animateDuration: "slow",
	animateEasing: "swing",
	aspectRatio: false,
	autoHide: false,
	containment: false,
	ghost: false,
	grid: false,
	handles: "e,s,se",
	helper: false,
	maxHeight: null,
	maxWidth: null,
	minHeight: 10,
	minWidth: 10,
	// See #7960
	zIndex: 90,

	// callbacks
	resize: null,
	start: null,
	stop: null
  },
  _create: function() {

	var n, i, handle, axis, hname,
	  that = this,
	  o = this.options;
	this.element.addClass("ui-resizable");

	$.extend(this, {
	  _aspectRatio: !!(o.aspectRatio),
	  aspectRatio: o.aspectRatio,
	  originalElement: this.element,
	  _proportionallyResizeElements: [],
	  _helper: o.helper || o.ghost || o.animate ? o.helper || "ui-resizable-helper" : null
	});

	//Wrap the element if it cannot hold child nodes
	if(this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)) {

	  //Create a wrapper element and set the wrapper to the new current internal element
	  this.element.wrap(
		$("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
		  position: this.element.css("position"),
		  width: this.element.outerWidth(),
		  height: this.element.outerHeight(),
		  top: this.element.css("top"),
		  left: this.element.css("left")
		})
	  );

	  //Overwrite the original this.element
	  this.element = this.element.parent().data(
		"ui-resizable", this.element.data("ui-resizable")
	  );

	  this.elementIsWrapper = true;

	  //Move margins to the wrapper
	  this.element.css({ marginLeft: this.originalElement.css("marginLeft"), marginTop: this.originalElement.css("marginTop"), marginRight: this.originalElement.css("marginRight"), marginBottom: this.originalElement.css("marginBottom") });
	  this.originalElement.css({ marginLeft: 0, marginTop: 0, marginRight: 0, marginBottom: 0});

	  //Prevent Safari textarea resize
	  this.originalResizeStyle = this.originalElement.css("resize");
	  this.originalElement.css("resize", "none");

	  //Push the actual element to our proportionallyResize internal array
	  this._proportionallyResizeElements.push(this.originalElement.css({ position: "static", zoom: 1, display: "block" }));

	  // avoid IE jump (hard set the margin)
	  this.originalElement.css({ margin: this.originalElement.css("margin") });

	  // fix handlers offset
	  this._proportionallyResize();

	}

	this.handles = o.handles || (!$(".ui-resizable-handle", this.element).length ? "e,s,se" : { n: ".ui-resizable-n", e: ".ui-resizable-e", s: ".ui-resizable-s", w: ".ui-resizable-w", se: ".ui-resizable-se", sw: ".ui-resizable-sw", ne: ".ui-resizable-ne", nw: ".ui-resizable-nw" });
	if(this.handles.constructor === String) {

	  if ( this.handles === "all") {
		this.handles = "n,e,s,w,se,sw,ne,nw";
	  }

	  n = this.handles.split(",");
	  this.handles = {};

	  for(i = 0; i < n.length; i++) {

		handle = $.trim(n[i]);
		hname = "ui-resizable-"+handle;
		axis = $("<div class='ui-resizable-handle " + hname + "'></div>");

		// Apply zIndex to all handles - see #7960
		axis.css({ zIndex: o.zIndex });

		//TODO : What's going on here?
		if ("se" === handle) {
		  axis.addClass("ui-icon ui-icon-gripsmall-diagonal-se");
		}

		//Insert into internal handles object and append to element
		this.handles[handle] = ".ui-resizable-"+handle;
		this.element.append(axis);
	  }

	}

	this._renderAxis = function(target) {

	  var i, axis, padPos, padWrapper;

	  target = target || this.element;

	  for(i in this.handles) {

		if(this.handles[i].constructor === String) {
		  this.handles[i] = $(this.handles[i], this.element).show();
		}

		//Apply pad to wrapper element, needed to fix axis position (textarea, inputs, scrolls)
		if (this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i)) {

		  axis = $(this.handles[i], this.element);

		  //Checking the correct pad and border
		  padWrapper = /sw|ne|nw|se|n|s/.test(i) ? axis.outerHeight() : axis.outerWidth();

		  //The padding type i have to apply...
		  padPos = [ "padding",
			/ne|nw|n/.test(i) ? "Top" :
			/se|sw|s/.test(i) ? "Bottom" :
			/^e$/.test(i) ? "Right" : "Left" ].join("");

		  target.css(padPos, padWrapper);

		  this._proportionallyResize();

		}

		//TODO: What's that good for? There's not anything to be executed left
		if(!$(this.handles[i]).length) {
		  continue;
		}
	  }
	};

	//TODO: make renderAxis a prototype function
	this._renderAxis(this.element);

	this._handles = $(".ui-resizable-handle", this.element)
	  .disableSelection();

	//Matching axis name
	this._handles.mouseover(function() {
	  if (!that.resizing) {
		if (this.className) {
		  axis = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);
		}
		//Axis, default = se
		that.axis = axis && axis[1] ? axis[1] : "se";
	  }
	});

	//If we want to auto hide the elements
	if (o.autoHide) {
	  this._handles.hide();
	  $(this.element)
		.addClass("ui-resizable-autohide")
		.mouseenter(function() {
		  if (o.disabled) {
			return;
		  }
		  $(this).removeClass("ui-resizable-autohide");
		  that._handles.show();
		})
		.mouseleave(function(){
		  if (o.disabled) {
			return;
		  }
		  if (!that.resizing) {
			$(this).addClass("ui-resizable-autohide");
			that._handles.hide();
		  }
		});
	}

	//Initialize the mouse interaction
	this._mouseInit();

  },

  _destroy: function() {

	this._mouseDestroy();

	var wrapper,
	  _destroy = function(exp) {
		$(exp).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing")
		  .removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove();
	  };

	//TODO: Unwrap at same DOM position
	if (this.elementIsWrapper) {
	  _destroy(this.element);
	  wrapper = this.element;
	  this.originalElement.css({
		position: wrapper.css("position"),
		width: wrapper.outerWidth(),
		height: wrapper.outerHeight(),
		top: wrapper.css("top"),
		left: wrapper.css("left")
	  }).insertAfter( wrapper );
	  wrapper.remove();
	}

	this.originalElement.css("resize", this.originalResizeStyle);
	_destroy(this.originalElement);

	return this;
  },

  _mouseCapture: function(event) {
	var i, handle,
	  capture = false;

	for (i in this.handles) {
	  handle = $(this.handles[i])[0];
	  if (handle === event.target || $.contains(handle, event.target)) {
		capture = true;
	  }
	}

	return !this.options.disabled && capture;
  },

  _mouseStart: function(event) {

	var curleft, curtop, cursor,
	  o = this.options,
	  iniPos = this.element.position(),
	  el = this.element;

	this.resizing = true;

	// bugfix for http://dev.jquery.com/ticket/1749
	if ( (/absolute/).test( el.css("position") ) ) {
	  el.css({ position: "absolute", top: el.css("top"), left: el.css("left") });
	} else if (el.is(".ui-draggable")) {
	  el.css({ position: "absolute", top: iniPos.top, left: iniPos.left });
	}

	this._renderProxy();

	curleft = num(this.helper.css("left"));
	curtop = num(this.helper.css("top"));

	if (o.containment) {
	  curleft += $(o.containment).scrollLeft() || 0;
	  curtop += $(o.containment).scrollTop() || 0;
	}

	//Store needed variables
	this.offset = this.helper.offset();
	this.position = { left: curleft, top: curtop };
	this.size = this._helper ? { width: this.helper.width(), height: this.helper.height() } : { width: el.width(), height: el.height() };
	this.originalSize = this._helper ? { width: el.outerWidth(), height: el.outerHeight() } : { width: el.width(), height: el.height() };
	this.originalPosition = { left: curleft, top: curtop };
	this.sizeDiff = { width: el.outerWidth() - el.width(), height: el.outerHeight() - el.height() };
	this.originalMousePosition = { left: event.pageX, top: event.pageY };

	//Aspect Ratio
	this.aspectRatio = (typeof o.aspectRatio === "number") ? o.aspectRatio : ((this.originalSize.width / this.originalSize.height) || 1);

	cursor = $(".ui-resizable-" + this.axis).css("cursor");
	$("body").css("cursor", cursor === "auto" ? this.axis + "-resize" : cursor);

	el.addClass("ui-resizable-resizing");
	this._propagate("start", event);
	return true;
  },

  _mouseDrag: function(event) {

	//Increase performance, avoid regex
	var data,
	  el = this.helper, props = {},
	  smp = this.originalMousePosition,
	  a = this.axis,
	  prevTop = this.position.top,
	  prevLeft = this.position.left,
	  prevWidth = this.size.width,
	  prevHeight = this.size.height,
	  dx = (event.pageX-smp.left)||0,
	  dy = (event.pageY-smp.top)||0,
	  trigger = this._change[a];

	if (!trigger) {
	  return false;
	}

	// Calculate the attrs that will be change
	data = trigger.apply(this, [event, dx, dy]);

	// Put this in the mouseDrag handler since the user can start pressing shift while resizing
	this._updateVirtualBoundaries(event.shiftKey);
	if (this._aspectRatio || event.shiftKey) {
	  data = this._updateRatio(data, event);
	}

	data = this._respectSize(data, event);

	this._updateCache(data);

	// plugins callbacks need to be called first
	this._propagate("resize", event);

	if (this.position.top !== prevTop) {
	  props.top = this.position.top + "px";
	}
	if (this.position.left !== prevLeft) {
	  props.left = this.position.left + "px";
	}
	if (this.size.width !== prevWidth) {
	  props.width = this.size.width + "px";
	}
	if (this.size.height !== prevHeight) {
	  props.height = this.size.height + "px";
	}
	el.css(props);

	if (!this._helper && this._proportionallyResizeElements.length) {
	  this._proportionallyResize();
	}

	// Call the user callback if the element was resized
	if ( ! $.isEmptyObject(props) ) {
	  this._trigger("resize", event, this.ui());
	}

	return false;
  },

  _mouseStop: function(event) {

	this.resizing = false;
	var pr, ista, soffseth, soffsetw, s, left, top,
	  o = this.options, that = this;

	if(this._helper) {

	  pr = this._proportionallyResizeElements;
	  ista = pr.length && (/textarea/i).test(pr[0].nodeName);
	  soffseth = ista && $.ui.hasScroll(pr[0], "left") /* TODO - jump height */ ? 0 : that.sizeDiff.height;
	  soffsetw = ista ? 0 : that.sizeDiff.width;

	  s = { width: (that.helper.width()  - soffsetw), height: (that.helper.height() - soffseth) };
	  left = (parseInt(that.element.css("left"), 10) + (that.position.left - that.originalPosition.left)) || null;
	  top = (parseInt(that.element.css("top"), 10) + (that.position.top - that.originalPosition.top)) || null;

	  if (!o.animate) {
		this.element.css($.extend(s, { top: top, left: left }));
	  }

	  that.helper.height(that.size.height);
	  that.helper.width(that.size.width);

	  if (this._helper && !o.animate) {
		this._proportionallyResize();
	  }
	}

	$("body").css("cursor", "auto");

	this.element.removeClass("ui-resizable-resizing");

	this._propagate("stop", event);

	if (this._helper) {
	  this.helper.remove();
	}

	return false;

  },

  _updateVirtualBoundaries: function(forceAspectRatio) {
	var pMinWidth, pMaxWidth, pMinHeight, pMaxHeight, b,
	  o = this.options;

	b = {
	  minWidth: isNumber(o.minWidth) ? o.minWidth : 0,
	  maxWidth: isNumber(o.maxWidth) ? o.maxWidth : Infinity,
	  minHeight: isNumber(o.minHeight) ? o.minHeight : 0,
	  maxHeight: isNumber(o.maxHeight) ? o.maxHeight : Infinity
	};

	if(this._aspectRatio || forceAspectRatio) {
	  // We want to create an enclosing box whose aspect ration is the requested one
	  // First, compute the "projected" size for each dimension based on the aspect ratio and other dimension
	  pMinWidth = b.minHeight * this.aspectRatio;
	  pMinHeight = b.minWidth / this.aspectRatio;
	  pMaxWidth = b.maxHeight * this.aspectRatio;
	  pMaxHeight = b.maxWidth / this.aspectRatio;

	  if(pMinWidth > b.minWidth) {
		b.minWidth = pMinWidth;
	  }
	  if(pMinHeight > b.minHeight) {
		b.minHeight = pMinHeight;
	  }
	  if(pMaxWidth < b.maxWidth) {
		b.maxWidth = pMaxWidth;
	  }
	  if(pMaxHeight < b.maxHeight) {
		b.maxHeight = pMaxHeight;
	  }
	}
	this._vBoundaries = b;
  },

  _updateCache: function(data) {
	this.offset = this.helper.offset();
	if (isNumber(data.left)) {
	  this.position.left = data.left;
	}
	if (isNumber(data.top)) {
	  this.position.top = data.top;
	}
	if (isNumber(data.height)) {
	  this.size.height = data.height;
	}
	if (isNumber(data.width)) {
	  this.size.width = data.width;
	}
  },

  _updateRatio: function( data ) {

	var cpos = this.position,
	  csize = this.size,
	  a = this.axis;

	if (isNumber(data.height)) {
	  data.width = (data.height * this.aspectRatio);
	} else if (isNumber(data.width)) {
	  data.height = (data.width / this.aspectRatio);
	}

	if (a === "sw") {
	  data.left = cpos.left + (csize.width - data.width);
	  data.top = null;
	}
	if (a === "nw") {
	  data.top = cpos.top + (csize.height - data.height);
	  data.left = cpos.left + (csize.width - data.width);
	}

	return data;
  },

  _respectSize: function( data ) {

	var o = this._vBoundaries,
	  a = this.axis,
	  ismaxw = isNumber(data.width) && o.maxWidth && (o.maxWidth < data.width), ismaxh = isNumber(data.height) && o.maxHeight && (o.maxHeight < data.height),
	  isminw = isNumber(data.width) && o.minWidth && (o.minWidth > data.width), isminh = isNumber(data.height) && o.minHeight && (o.minHeight > data.height),
	  dw = this.originalPosition.left + this.originalSize.width,
	  dh = this.position.top + this.size.height,
	  cw = /sw|nw|w/.test(a), ch = /nw|ne|n/.test(a);
	if (isminw) {
	  data.width = o.minWidth;
	}
	if (isminh) {
	  data.height = o.minHeight;
	}
	if (ismaxw) {
	  data.width = o.maxWidth;
	}
	if (ismaxh) {
	  data.height = o.maxHeight;
	}

	if (isminw && cw) {
	  data.left = dw - o.minWidth;
	}
	if (ismaxw && cw) {
	  data.left = dw - o.maxWidth;
	}
	if (isminh && ch) {
	  data.top = dh - o.minHeight;
	}
	if (ismaxh && ch) {
	  data.top = dh - o.maxHeight;
	}

	// fixing jump error on top/left - bug #2330
	if (!data.width && !data.height && !data.left && data.top) {
	  data.top = null;
	} else if (!data.width && !data.height && !data.top && data.left) {
	  data.left = null;
	}

	return data;
  },

  _proportionallyResize: function() {

	if (!this._proportionallyResizeElements.length) {
	  return;
	}

	var i, j, borders, paddings, prel,
	  element = this.helper || this.element;

	for ( i=0; i < this._proportionallyResizeElements.length; i++) {

	  prel = this._proportionallyResizeElements[i];

	  if (!this.borderDif) {
		this.borderDif = [];
		borders = [prel.css("borderTopWidth"), prel.css("borderRightWidth"), prel.css("borderBottomWidth"), prel.css("borderLeftWidth")];
		paddings = [prel.css("paddingTop"), prel.css("paddingRight"), prel.css("paddingBottom"), prel.css("paddingLeft")];

		for ( j = 0; j < borders.length; j++ ) {
		  this.borderDif[ j ] = ( parseInt( borders[ j ], 10 ) || 0 ) + ( parseInt( paddings[ j ], 10 ) || 0 );
		}
	  }

	  prel.css({
		height: (element.height() - this.borderDif[0] - this.borderDif[2]) || 0,
		width: (element.width() - this.borderDif[1] - this.borderDif[3]) || 0
	  });

	}

  },

  _renderProxy: function() {

	var el = this.element, o = this.options;
	this.elementOffset = el.offset();

	if(this._helper) {

	  this.helper = this.helper || $("<div style='overflow:hidden;'></div>");

	  this.helper.addClass(this._helper).css({
		width: this.element.outerWidth() - 1,
		height: this.element.outerHeight() - 1,
		position: "absolute",
		left: this.elementOffset.left +"px",
		top: this.elementOffset.top +"px",
		zIndex: ++o.zIndex //TODO: Don't modify option
	  });

	  this.helper
		.appendTo("body")
		.disableSelection();

	} else {
	  this.helper = this.element;
	}

  },

  _change: {
	e: function(event, dx) {
	  return { width: this.originalSize.width + dx };
	},
	w: function(event, dx) {
	  var cs = this.originalSize, sp = this.originalPosition;
	  return { left: sp.left + dx, width: cs.width - dx };
	},
	n: function(event, dx, dy) {
	  var cs = this.originalSize, sp = this.originalPosition;
	  return { top: sp.top + dy, height: cs.height - dy };
	},
	s: function(event, dx, dy) {
	  return { height: this.originalSize.height + dy };
	},
	se: function(event, dx, dy) {
	  return $.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [event, dx, dy]));
	},
	sw: function(event, dx, dy) {
	  return $.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [event, dx, dy]));
	},
	ne: function(event, dx, dy) {
	  return $.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [event, dx, dy]));
	},
	nw: function(event, dx, dy) {
	  return $.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [event, dx, dy]));
	}
  },

  _propagate: function(n, event) {
	$.ui.plugin.call(this, n, [event, this.ui()]);
	(n !== "resize" && this._trigger(n, event, this.ui()));
  },

  plugins: {},

  ui: function() {
	return {
	  originalElement: this.originalElement,
	  element: this.element,
	  helper: this.helper,
	  position: this.position,
	  size: this.size,
	  originalSize: this.originalSize,
	  originalPosition: this.originalPosition
	};
  }

});

/*
 * Resizable Extensions
 */

$.ui.plugin.add("resizable", "animate", {

  stop: function( event ) {
	var that = $(this).data("ui-resizable"),
	  o = that.options,
	  pr = that._proportionallyResizeElements,
	  ista = pr.length && (/textarea/i).test(pr[0].nodeName),
	  soffseth = ista && $.ui.hasScroll(pr[0], "left") /* TODO - jump height */ ? 0 : that.sizeDiff.height,
	  soffsetw = ista ? 0 : that.sizeDiff.width,
	  style = { width: (that.size.width - soffsetw), height: (that.size.height - soffseth) },
	  left = (parseInt(that.element.css("left"), 10) + (that.position.left - that.originalPosition.left)) || null,
	  top = (parseInt(that.element.css("top"), 10) + (that.position.top - that.originalPosition.top)) || null;

	that.element.animate(
	  $.extend(style, top && left ? { top: top, left: left } : {}), {
		duration: o.animateDuration,
		easing: o.animateEasing,
		step: function() {

		  var data = {
			width: parseInt(that.element.css("width"), 10),
			height: parseInt(that.element.css("height"), 10),
			top: parseInt(that.element.css("top"), 10),
			left: parseInt(that.element.css("left"), 10)
		  };

		  if (pr && pr.length) {
			$(pr[0]).css({ width: data.width, height: data.height });
		  }

		  // propagating resize, and updating values for each animation step
		  that._updateCache(data);
		  that._propagate("resize", event);

		}
	  }
	);
  }

});

$.ui.plugin.add("resizable", "containment", {

  start: function() {
	var element, p, co, ch, cw, width, height,
	  that = $(this).data("ui-resizable"),
	  o = that.options,
	  el = that.element,
	  oc = o.containment,
	  ce = (oc instanceof $) ? oc.get(0) : (/parent/.test(oc)) ? el.parent().get(0) : oc;

	if (!ce) {
	  return;
	}

	that.containerElement = $(ce);

	if (/document/.test(oc) || oc === document) {
	  that.containerOffset = { left: 0, top: 0 };
	  that.containerPosition = { left: 0, top: 0 };

	  that.parentData = {
		element: $(document), left: 0, top: 0,
		width: $(document).width(), height: $(document).height() || document.body.parentNode.scrollHeight
	  };
	}

	// i'm a node, so compute top, left, right, bottom
	else {
	  element = $(ce);
	  p = [];
	  $([ "Top", "Right", "Left", "Bottom" ]).each(function(i, name) { p[i] = num(element.css("padding" + name)); });

	  that.containerOffset = element.offset();
	  that.containerPosition = element.position();
	  that.containerSize = { height: (element.innerHeight() - p[3]), width: (element.innerWidth() - p[1]) };

	  co = that.containerOffset;
	  ch = that.containerSize.height;
	  cw = that.containerSize.width;
	  width = ($.ui.hasScroll(ce, "left") ? ce.scrollWidth : cw );
	  height = ($.ui.hasScroll(ce) ? ce.scrollHeight : ch);

	  that.parentData = {
		element: ce, left: co.left, top: co.top, width: width, height: height
	  };
	}
  },

  resize: function( event ) {
	var woset, hoset, isParent, isOffsetRelative,
	  that = $(this).data("ui-resizable"),
	  o = that.options,
	  co = that.containerOffset, cp = that.position,
	  pRatio = that._aspectRatio || event.shiftKey,
	  cop = { top:0, left:0 }, ce = that.containerElement;

	if (ce[0] !== document && (/static/).test(ce.css("position"))) {
	  cop = co;
	}

	if (cp.left < (that._helper ? co.left : 0)) {
	  that.size.width = that.size.width + (that._helper ? (that.position.left - co.left) : (that.position.left - cop.left));
	  if (pRatio) {
		that.size.height = that.size.width / that.aspectRatio;
	  }
	  that.position.left = o.helper ? co.left : 0;
	}

	if (cp.top < (that._helper ? co.top : 0)) {
	  that.size.height = that.size.height + (that._helper ? (that.position.top - co.top) : that.position.top);
	  if (pRatio) {
		that.size.width = that.size.height * that.aspectRatio;
	  }
	  that.position.top = that._helper ? co.top : 0;
	}

	that.offset.left = that.parentData.left+that.position.left;
	that.offset.top = that.parentData.top+that.position.top;

	woset = Math.abs( (that._helper ? that.offset.left - cop.left : (that.offset.left - cop.left)) + that.sizeDiff.width );
	hoset = Math.abs( (that._helper ? that.offset.top - cop.top : (that.offset.top - co.top)) + that.sizeDiff.height );

	isParent = that.containerElement.get(0) === that.element.parent().get(0);
	isOffsetRelative = /relative|absolute/.test(that.containerElement.css("position"));

	if ( isParent && isOffsetRelative ) {
	  woset -= Math.abs( that.parentData.left );
	}

	if (woset + that.size.width >= that.parentData.width) {
	  that.size.width = that.parentData.width - woset;
	  if (pRatio) {
		that.size.height = that.size.width / that.aspectRatio;
	  }
	}

	if (hoset + that.size.height >= that.parentData.height) {
	  that.size.height = that.parentData.height - hoset;
	  if (pRatio) {
		that.size.width = that.size.height * that.aspectRatio;
	  }
	}
  },

  stop: function(){
	var that = $(this).data("ui-resizable"),
	  o = that.options,
	  co = that.containerOffset,
	  cop = that.containerPosition,
	  ce = that.containerElement,
	  helper = $(that.helper),
	  ho = helper.offset(),
	  w = helper.outerWidth() - that.sizeDiff.width,
	  h = helper.outerHeight() - that.sizeDiff.height;

	if (that._helper && !o.animate && (/relative/).test(ce.css("position"))) {
	  $(this).css({ left: ho.left - cop.left - co.left, width: w, height: h });
	}

	if (that._helper && !o.animate && (/static/).test(ce.css("position"))) {
	  $(this).css({ left: ho.left - cop.left - co.left, width: w, height: h });
	}

  }
});

$.ui.plugin.add("resizable", "alsoResize", {

  start: function () {
	var that = $(this).data("ui-resizable"),
	  o = that.options,
	  _store = function (exp) {
		$(exp).each(function() {
		  var el = $(this);
		  el.data("ui-resizable-alsoresize", {
			width: parseInt(el.width(), 10), height: parseInt(el.height(), 10),
			left: parseInt(el.css("left"), 10), top: parseInt(el.css("top"), 10)
		  });
		});
	  };

	if (typeof(o.alsoResize) === "object" && !o.alsoResize.parentNode) {
	  if (o.alsoResize.length) { o.alsoResize = o.alsoResize[0]; _store(o.alsoResize); }
	  else { $.each(o.alsoResize, function (exp) { _store(exp); }); }
	}else{
	  _store(o.alsoResize);
	}
  },

  resize: function (event, ui) {
	var that = $(this).data("ui-resizable"),
	  o = that.options,
	  os = that.originalSize,
	  op = that.originalPosition,
	  delta = {
		height: (that.size.height - os.height) || 0, width: (that.size.width - os.width) || 0,
		top: (that.position.top - op.top) || 0, left: (that.position.left - op.left) || 0
	  },

	  _alsoResize = function (exp, c) {
		$(exp).each(function() {
		  var el = $(this), start = $(this).data("ui-resizable-alsoresize"), style = {},
			css = c && c.length ? c : el.parents(ui.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];

		  $.each(css, function (i, prop) {
			var sum = (start[prop]||0) + (delta[prop]||0);
			if (sum && sum >= 0) {
			  style[prop] = sum || null;
			}
		  });

		  el.css(style);
		});
	  };

	if (typeof(o.alsoResize) === "object" && !o.alsoResize.nodeType) {
	  $.each(o.alsoResize, function (exp, c) { _alsoResize(exp, c); });
	}else{
	  _alsoResize(o.alsoResize);
	}
  },

  stop: function () {
	$(this).removeData("resizable-alsoresize");
  }
});

$.ui.plugin.add("resizable", "ghost", {

  start: function() {

	var that = $(this).data("ui-resizable"), o = that.options, cs = that.size;

	that.ghost = that.originalElement.clone();
	that.ghost
	  .css({ opacity: 0.25, display: "block", position: "relative", height: cs.height, width: cs.width, margin: 0, left: 0, top: 0 })
	  .addClass("ui-resizable-ghost")
	  .addClass(typeof o.ghost === "string" ? o.ghost : "");

	that.ghost.appendTo(that.helper);

  },

  resize: function(){
	var that = $(this).data("ui-resizable");
	if (that.ghost) {
	  that.ghost.css({ position: "relative", height: that.size.height, width: that.size.width });
	}
  },

  stop: function() {
	var that = $(this).data("ui-resizable");
	if (that.ghost && that.helper) {
	  that.helper.get(0).removeChild(that.ghost.get(0));
	}
  }

});

$.ui.plugin.add("resizable", "grid", {

  resize: function() {
	var that = $(this).data("ui-resizable"),
	  o = that.options,
	  cs = that.size,
	  os = that.originalSize,
	  op = that.originalPosition,
	  a = that.axis,
	  grid = typeof o.grid === "number" ? [o.grid, o.grid] : o.grid,
	  gridX = (grid[0]||1),
	  gridY = (grid[1]||1),
	  ox = Math.round((cs.width - os.width) / gridX) * gridX,
	  oy = Math.round((cs.height - os.height) / gridY) * gridY,
	  newWidth = os.width + ox,
	  newHeight = os.height + oy,
	  isMaxWidth = o.maxWidth && (o.maxWidth < newWidth),
	  isMaxHeight = o.maxHeight && (o.maxHeight < newHeight),
	  isMinWidth = o.minWidth && (o.minWidth > newWidth),
	  isMinHeight = o.minHeight && (o.minHeight > newHeight);

	o.grid = grid;

	if (isMinWidth) {
	  newWidth = newWidth + gridX;
	}
	if (isMinHeight) {
	  newHeight = newHeight + gridY;
	}
	if (isMaxWidth) {
	  newWidth = newWidth - gridX;
	}
	if (isMaxHeight) {
	  newHeight = newHeight - gridY;
	}

	if (/^(se|s|e)$/.test(a)) {
	  that.size.width = newWidth;
	  that.size.height = newHeight;
	} else if (/^(ne)$/.test(a)) {
	  that.size.width = newWidth;
	  that.size.height = newHeight;
	  that.position.top = op.top - oy;
	} else if (/^(sw)$/.test(a)) {
	  that.size.width = newWidth;
	  that.size.height = newHeight;
	  that.position.left = op.left - ox;
	} else {
	  if ( newHeight - gridY > 0 ) {
		that.size.height = newHeight;
		that.position.top = op.top - oy;
	  } else {
		that.size.height = gridY;
		that.position.top = op.top + os.height - gridY;
	  }
	  if ( newWidth - gridX > 0 ) {
		that.size.width = newWidth;
		that.position.left = op.left - ox;
	  } else {
		that.size.width = gridX;
		that.position.left = op.left + os.width - gridX;
	  }
	}
  }

});

})(jQuery);
(function( $, undefined ) {

$.widget("ui.selectable", $.ui.mouse, {
  version: "1.10.4",
  options: {
	appendTo: "body",
	autoRefresh: true,
	distance: 0,
	filter: "*",
	tolerance: "touch",

	// callbacks
	selected: null,
	selecting: null,
	start: null,
	stop: null,
	unselected: null,
	unselecting: null
  },
  _create: function() {
	var selectees,
	  that = this;

	this.element.addClass("ui-selectable");

	this.dragged = false;

	// cache selectee children based on filter
	this.refresh = function() {
	  selectees = $(that.options.filter, that.element[0]);
	  selectees.addClass("ui-selectee");
	  selectees.each(function() {
		var $this = $(this),
		  pos = $this.offset();
		$.data(this, "selectable-item", {
		  element: this,
		  $element: $this,
		  left: pos.left,
		  top: pos.top,
		  right: pos.left + $this.outerWidth(),
		  bottom: pos.top + $this.outerHeight(),
		  startselected: false,
		  selected: $this.hasClass("ui-selected"),
		  selecting: $this.hasClass("ui-selecting"),
		  unselecting: $this.hasClass("ui-unselecting")
		});
	  });
	};
	this.refresh();

	this.selectees = selectees.addClass("ui-selectee");

	this._mouseInit();

	this.helper = $("<div class='ui-selectable-helper'></div>");
  },

  _destroy: function() {
	this.selectees
	  .removeClass("ui-selectee")
	  .removeData("selectable-item");
	this.element
	  .removeClass("ui-selectable ui-selectable-disabled");
	this._mouseDestroy();
  },

  _mouseStart: function(event) {
	var that = this,
	  options = this.options;

	this.opos = [event.pageX, event.pageY];

	if (this.options.disabled) {
	  return;
	}

	this.selectees = $(options.filter, this.element[0]);

	this._trigger("start", event);

	$(options.appendTo).append(this.helper);
	// position helper (lasso)
	this.helper.css({
	  "left": event.pageX,
	  "top": event.pageY,
	  "width": 0,
	  "height": 0
	});

	if (options.autoRefresh) {
	  this.refresh();
	}

	this.selectees.filter(".ui-selected").each(function() {
	  var selectee = $.data(this, "selectable-item");
	  selectee.startselected = true;
	  if (!event.metaKey && !event.ctrlKey) {
		selectee.$element.removeClass("ui-selected");
		selectee.selected = false;
		selectee.$element.addClass("ui-unselecting");
		selectee.unselecting = true;
		// selectable UNSELECTING callback
		that._trigger("unselecting", event, {
		  unselecting: selectee.element
		});
	  }
	});

	$(event.target).parents().addBack().each(function() {
	  var doSelect,
		selectee = $.data(this, "selectable-item");
	  if (selectee) {
		doSelect = (!event.metaKey && !event.ctrlKey) || !selectee.$element.hasClass("ui-selected");
		selectee.$element
		  .removeClass(doSelect ? "ui-unselecting" : "ui-selected")
		  .addClass(doSelect ? "ui-selecting" : "ui-unselecting");
		selectee.unselecting = !doSelect;
		selectee.selecting = doSelect;
		selectee.selected = doSelect;
		// selectable (UN)SELECTING callback
		if (doSelect) {
		  that._trigger("selecting", event, {
			selecting: selectee.element
		  });
		} else {
		  that._trigger("unselecting", event, {
			unselecting: selectee.element
		  });
		}
		return false;
	  }
	});

  },

  _mouseDrag: function(event) {

	this.dragged = true;

	if (this.options.disabled) {
	  return;
	}

	var tmp,
	  that = this,
	  options = this.options,
	  x1 = this.opos[0],
	  y1 = this.opos[1],
	  x2 = event.pageX,
	  y2 = event.pageY;

	if (x1 > x2) { tmp = x2; x2 = x1; x1 = tmp; }
	if (y1 > y2) { tmp = y2; y2 = y1; y1 = tmp; }
	this.helper.css({left: x1, top: y1, width: x2-x1, height: y2-y1});

	this.selectees.each(function() {
	  var selectee = $.data(this, "selectable-item"),
		hit = false;

	  //prevent helper from being selected if appendTo: selectable
	  if (!selectee || selectee.element === that.element[0]) {
		return;
	  }

	  if (options.tolerance === "touch") {
		hit = ( !(selectee.left > x2 || selectee.right < x1 || selectee.top > y2 || selectee.bottom < y1) );
	  } else if (options.tolerance === "fit") {
		hit = (selectee.left > x1 && selectee.right < x2 && selectee.top > y1 && selectee.bottom < y2);
	  }

	  if (hit) {
		// SELECT
		if (selectee.selected) {
		  selectee.$element.removeClass("ui-selected");
		  selectee.selected = false;
		}
		if (selectee.unselecting) {
		  selectee.$element.removeClass("ui-unselecting");
		  selectee.unselecting = false;
		}
		if (!selectee.selecting) {
		  selectee.$element.addClass("ui-selecting");
		  selectee.selecting = true;
		  // selectable SELECTING callback
		  that._trigger("selecting", event, {
			selecting: selectee.element
		  });
		}
	  } else {
		// UNSELECT
		if (selectee.selecting) {
		  if ((event.metaKey || event.ctrlKey) && selectee.startselected) {
			selectee.$element.removeClass("ui-selecting");
			selectee.selecting = false;
			selectee.$element.addClass("ui-selected");
			selectee.selected = true;
		  } else {
			selectee.$element.removeClass("ui-selecting");
			selectee.selecting = false;
			if (selectee.startselected) {
			  selectee.$element.addClass("ui-unselecting");
			  selectee.unselecting = true;
			}
			// selectable UNSELECTING callback
			that._trigger("unselecting", event, {
			  unselecting: selectee.element
			});
		  }
		}
		if (selectee.selected) {
		  if (!event.metaKey && !event.ctrlKey && !selectee.startselected) {
			selectee.$element.removeClass("ui-selected");
			selectee.selected = false;

			selectee.$element.addClass("ui-unselecting");
			selectee.unselecting = true;
			// selectable UNSELECTING callback
			that._trigger("unselecting", event, {
			  unselecting: selectee.element
			});
		  }
		}
	  }
	});

	return false;
  },

  _mouseStop: function(event) {
	var that = this;

	this.dragged = false;

	$(".ui-unselecting", this.element[0]).each(function() {
	  var selectee = $.data(this, "selectable-item");
	  selectee.$element.removeClass("ui-unselecting");
	  selectee.unselecting = false;
	  selectee.startselected = false;
	  that._trigger("unselected", event, {
		unselected: selectee.element
	  });
	});
	$(".ui-selecting", this.element[0]).each(function() {
	  var selectee = $.data(this, "selectable-item");
	  selectee.$element.removeClass("ui-selecting").addClass("ui-selected");
	  selectee.selecting = false;
	  selectee.selected = true;
	  selectee.startselected = true;
	  that._trigger("selected", event, {
		selected: selectee.element
	  });
	});
	this._trigger("stop", event);

	this.helper.remove();

	return false;
  }

});

})(jQuery);
(function( $, undefined ) {

function isOverAxis( x, reference, size ) {
  return ( x > reference ) && ( x < ( reference + size ) );
}

function isFloating(item) {
  return (/left|right/).test(item.css("float")) || (/inline|table-cell/).test(item.css("display"));
}

$.widget("ui.sortable", $.ui.mouse, {
  version: "1.10.4",
  widgetEventPrefix: "sort",
  ready: false,
  options: {
	appendTo: "parent",
	axis: false,
	connectWith: false,
	containment: false,
	cursor: "auto",
	cursorAt: false,
	dropOnEmpty: true,
	forcePlaceholderSize: false,
	forceHelperSize: false,
	grid: false,
	handle: false,
	helper: "original",
	items: "> *",
	opacity: false,
	placeholder: false,
	revert: false,
	scroll: true,
	scrollSensitivity: 20,
	scrollSpeed: 20,
	scope: "default",
	tolerance: "intersect",
	zIndex: 1000,

	// callbacks
	activate: null,
	beforeStop: null,
	change: null,
	deactivate: null,
	out: null,
	over: null,
	receive: null,
	remove: null,
	sort: null,
	start: null,
	stop: null,
	update: null
  },
  _create: function() {

	var o = this.options;
	this.containerCache = {};
	this.element.addClass("ui-sortable");

	//Get the items
	this.refresh();

	//Let's determine if the items are being displayed horizontally
	this.floating = this.items.length ? o.axis === "x" || isFloating(this.items[0].item) : false;

	//Let's determine the parent's offset
	this.offset = this.element.offset();

	//Initialize mouse events for interaction
	this._mouseInit();

	//We're ready to go
	this.ready = true;

  },

  _destroy: function() {
	this.element
	  .removeClass("ui-sortable ui-sortable-disabled");
	this._mouseDestroy();

	for ( var i = this.items.length - 1; i >= 0; i-- ) {
	  this.items[i].item.removeData(this.widgetName + "-item");
	}

	return this;
  },

  _setOption: function(key, value){
	if ( key === "disabled" ) {
	  this.options[ key ] = value;

	  this.widget().toggleClass( "ui-sortable-disabled", !!value );
	} else {
	  // Don't call widget base _setOption for disable as it adds ui-state-disabled class
	  $.Widget.prototype._setOption.apply(this, arguments);
	}
  },

  _mouseCapture: function(event, overrideHandle) {
	var currentItem = null,
	  validHandle = false,
	  that = this;

	if (this.reverting) {
	  return false;
	}

	if(this.options.disabled || this.options.type === "static") {
	  return false;
	}

	//We have to refresh the items data once first
	this._refreshItems(event);

	//Find out if the clicked node (or one of its parents) is a actual item in this.items
	$(event.target).parents().each(function() {
	  if($.data(this, that.widgetName + "-item") === that) {
		currentItem = $(this);
		return false;
	  }
	});
	if($.data(event.target, that.widgetName + "-item") === that) {
	  currentItem = $(event.target);
	}

	if(!currentItem) {
	  return false;
	}
	if(this.options.handle && !overrideHandle) {
	  $(this.options.handle, currentItem).find("*").addBack().each(function() {
		if(this === event.target) {
		  validHandle = true;
		}
	  });
	  if(!validHandle) {
		return false;
	  }
	}

	this.currentItem = currentItem;
	this._removeCurrentsFromItems();
	return true;

  },

  _mouseStart: function(event, overrideHandle, noActivation) {

	var i, body,
	  o = this.options;

	this.currentContainer = this;

	//We only need to call refreshPositions, because the refreshItems call has been moved to mouseCapture
	this.refreshPositions();

	//Create and append the visible helper
	this.helper = this._createHelper(event);

	//Cache the helper size
	this._cacheHelperProportions();

	/*
	 * - Position generation -
	 * This block generates everything position related - it's the core of draggables.
	 */

	//Cache the margins of the original element
	this._cacheMargins();

	//Get the next scrolling parent
	this.scrollParent = this.helper.scrollParent();

	//The element's absolute position on the page minus margins
	this.offset = this.currentItem.offset();
	this.offset = {
	  top: this.offset.top - this.margins.top,
	  left: this.offset.left - this.margins.left
	};

	$.extend(this.offset, {
	  click: { //Where the click happened, relative to the element
		left: event.pageX - this.offset.left,
		top: event.pageY - this.offset.top
	  },
	  parent: this._getParentOffset(),
	  relative: this._getRelativeOffset() //This is a relative to absolute position minus the actual position calculation - only used for relative positioned helper
	});

	// Only after we got the offset, we can change the helper's position to absolute
	// TODO: Still need to figure out a way to make relative sorting possible
	this.helper.css("position", "absolute");
	this.cssPosition = this.helper.css("position");

	//Generate the original position
	this.originalPosition = this._generatePosition(event);
	this.originalPageX = event.pageX;
	this.originalPageY = event.pageY;

	//Adjust the mouse offset relative to the helper if "cursorAt" is supplied
	(o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt));

	//Cache the former DOM position
	this.domPosition = { prev: this.currentItem.prev()[0], parent: this.currentItem.parent()[0] };

	//If the helper is not the original, hide the original so it's not playing any role during the drag, won't cause anything bad this way
	if(this.helper[0] !== this.currentItem[0]) {
	  this.currentItem.hide();
	}

	//Create the placeholder
	this._createPlaceholder();

	//Set a containment if given in the options
	if(o.containment) {
	  this._setContainment();
	}

	if( o.cursor && o.cursor !== "auto" ) { // cursor option
	  body = this.document.find( "body" );

	  // support: IE
	  this.storedCursor = body.css( "cursor" );
	  body.css( "cursor", o.cursor );

	  this.storedStylesheet = $( "<style>*{ cursor: "+o.cursor+" !important; }</style>" ).appendTo( body );
	}

	if(o.opacity) { // opacity option
	  if (this.helper.css("opacity")) {
		this._storedOpacity = this.helper.css("opacity");
	  }
	  this.helper.css("opacity", o.opacity);
	}

	if(o.zIndex) { // zIndex option
	  if (this.helper.css("zIndex")) {
		this._storedZIndex = this.helper.css("zIndex");
	  }
	  this.helper.css("zIndex", o.zIndex);
	}

	//Prepare scrolling
	if(this.scrollParent[0] !== document && this.scrollParent[0].tagName !== "HTML") {
	  this.overflowOffset = this.scrollParent.offset();
	}

	//Call callbacks
	this._trigger("start", event, this._uiHash());

	//Recache the helper size
	if(!this._preserveHelperProportions) {
	  this._cacheHelperProportions();
	}


	//Post "activate" events to possible containers
	if( !noActivation ) {
	  for ( i = this.containers.length - 1; i >= 0; i-- ) {
		this.containers[ i ]._trigger( "activate", event, this._uiHash( this ) );
	  }
	}

	//Prepare possible droppables
	if($.ui.ddmanager) {
	  $.ui.ddmanager.current = this;
	}

	if ($.ui.ddmanager && !o.dropBehaviour) {
	  $.ui.ddmanager.prepareOffsets(this, event);
	}

	this.dragging = true;

	this.helper.addClass("ui-sortable-helper");
	this._mouseDrag(event); //Execute the drag once - this causes the helper not to be visible before getting its correct position
	return true;

  },

  _mouseDrag: function(event) {
	var i, item, itemElement, intersection,
	  o = this.options,
	  scrolled = false;

	//Compute the helpers position
	this.position = this._generatePosition(event);
	this.positionAbs = this._convertPositionTo("absolute");

	if (!this.lastPositionAbs) {
	  this.lastPositionAbs = this.positionAbs;
	}

	//Do scrolling
	if(this.options.scroll) {
	  if(this.scrollParent[0] !== document && this.scrollParent[0].tagName !== "HTML") {

		if((this.overflowOffset.top + this.scrollParent[0].offsetHeight) - event.pageY < o.scrollSensitivity) {
		  this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop + o.scrollSpeed;
		} else if(event.pageY - this.overflowOffset.top < o.scrollSensitivity) {
		  this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop - o.scrollSpeed;
		}

		if((this.overflowOffset.left + this.scrollParent[0].offsetWidth) - event.pageX < o.scrollSensitivity) {
		  this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft + o.scrollSpeed;
		} else if(event.pageX - this.overflowOffset.left < o.scrollSensitivity) {
		  this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft - o.scrollSpeed;
		}

	  } else {

		if(event.pageY - $(document).scrollTop() < o.scrollSensitivity) {
		  scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed);
		} else if($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity) {
		  scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed);
		}

		if(event.pageX - $(document).scrollLeft() < o.scrollSensitivity) {
		  scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed);
		} else if($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity) {
		  scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);
		}

	  }

	  if(scrolled !== false && $.ui.ddmanager && !o.dropBehaviour) {
		$.ui.ddmanager.prepareOffsets(this, event);
	  }
	}

	//Regenerate the absolute position used for position checks
	this.positionAbs = this._convertPositionTo("absolute");

	//Set the helper position
	if(!this.options.axis || this.options.axis !== "y") {
	  this.helper[0].style.left = this.position.left+"px";
	}
	if(!this.options.axis || this.options.axis !== "x") {
	  this.helper[0].style.top = this.position.top+"px";
	}

	//Rearrange
	for (i = this.items.length - 1; i >= 0; i--) {

	  //Cache variables and intersection, continue if no intersection
	  item = this.items[i];
	  itemElement = item.item[0];
	  intersection = this._intersectsWithPointer(item);
	  if (!intersection) {
		continue;
	  }

	  // Only put the placeholder inside the current Container, skip all
	  // items from other containers. This works because when moving
	  // an item from one container to another the
	  // currentContainer is switched before the placeholder is moved.
	  //
	  // Without this, moving items in "sub-sortables" can cause
	  // the placeholder to jitter beetween the outer and inner container.
	  if (item.instance !== this.currentContainer) {
		continue;
	  }

	  // cannot intersect with itself
	  // no useless actions that have been done before
	  // no action if the item moved is the parent of the item checked
	  if (itemElement !== this.currentItem[0] &&
		this.placeholder[intersection === 1 ? "next" : "prev"]()[0] !== itemElement &&
		!$.contains(this.placeholder[0], itemElement) &&
		(this.options.type === "semi-dynamic" ? !$.contains(this.element[0], itemElement) : true)
	  ) {

		this.direction = intersection === 1 ? "down" : "up";

		if (this.options.tolerance === "pointer" || this._intersectsWithSides(item)) {
		  this._rearrange(event, item);
		} else {
		  break;
		}

		this._trigger("change", event, this._uiHash());
		break;
	  }
	}

	//Post events to containers
	this._contactContainers(event);

	//Interconnect with droppables
	if($.ui.ddmanager) {
	  $.ui.ddmanager.drag(this, event);
	}

	//Call callbacks
	this._trigger("sort", event, this._uiHash());

	this.lastPositionAbs = this.positionAbs;
	return false;

  },

  _mouseStop: function(event, noPropagation) {

	if(!event) {
	  return;
	}

	//If we are using droppables, inform the manager about the drop
	if ($.ui.ddmanager && !this.options.dropBehaviour) {
	  $.ui.ddmanager.drop(this, event);
	}

	if(this.options.revert) {
	  var that = this,
		cur = this.placeholder.offset(),
		axis = this.options.axis,
		animation = {};

	  if ( !axis || axis === "x" ) {
		animation.left = cur.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft);
	  }
	  if ( !axis || axis === "y" ) {
		animation.top = cur.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop);
	  }
	  this.reverting = true;
	  $(this.helper).animate( animation, parseInt(this.options.revert, 10) || 500, function() {
		that._clear(event);
	  });
	} else {
	  this._clear(event, noPropagation);
	}

	return false;

  },

  cancel: function() {

	if(this.dragging) {

	  this._mouseUp({ target: null });

	  if(this.options.helper === "original") {
		this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
	  } else {
		this.currentItem.show();
	  }

	  //Post deactivating events to containers
	  for (var i = this.containers.length - 1; i >= 0; i--){
		this.containers[i]._trigger("deactivate", null, this._uiHash(this));
		if(this.containers[i].containerCache.over) {
		  this.containers[i]._trigger("out", null, this._uiHash(this));
		  this.containers[i].containerCache.over = 0;
		}
	  }

	}

	if (this.placeholder) {
	  //$(this.placeholder[0]).remove(); would have been the jQuery way - unfortunately, it unbinds ALL events from the original node!
	  if(this.placeholder[0].parentNode) {
		this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
	  }
	  if(this.options.helper !== "original" && this.helper && this.helper[0].parentNode) {
		this.helper.remove();
	  }

	  $.extend(this, {
		helper: null,
		dragging: false,
		reverting: false,
		_noFinalSort: null
	  });

	  if(this.domPosition.prev) {
		$(this.domPosition.prev).after(this.currentItem);
	  } else {
		$(this.domPosition.parent).prepend(this.currentItem);
	  }
	}

	return this;

  },

  serialize: function(o) {

	var items = this._getItemsAsjQuery(o && o.connected),
	  str = [];
	o = o || {};

	$(items).each(function() {
	  var res = ($(o.item || this).attr(o.attribute || "id") || "").match(o.expression || (/(.+)[\-=_](.+)/));
	  if (res) {
		str.push((o.key || res[1]+"[]")+"="+(o.key && o.expression ? res[1] : res[2]));
	  }
	});

	if(!str.length && o.key) {
	  str.push(o.key + "=");
	}

	return str.join("&");

  },

  toArray: function(o) {

	var items = this._getItemsAsjQuery(o && o.connected),
	  ret = [];

	o = o || {};

	items.each(function() { ret.push($(o.item || this).attr(o.attribute || "id") || ""); });
	return ret;

  },

  /* Be careful with the following core functions */
  _intersectsWith: function(item) {

	var x1 = this.positionAbs.left,
	  x2 = x1 + this.helperProportions.width,
	  y1 = this.positionAbs.top,
	  y2 = y1 + this.helperProportions.height,
	  l = item.left,
	  r = l + item.width,
	  t = item.top,
	  b = t + item.height,
	  dyClick = this.offset.click.top,
	  dxClick = this.offset.click.left,
	  isOverElementHeight = ( this.options.axis === "x" ) || ( ( y1 + dyClick ) > t && ( y1 + dyClick ) < b ),
	  isOverElementWidth = ( this.options.axis === "y" ) || ( ( x1 + dxClick ) > l && ( x1 + dxClick ) < r ),
	  isOverElement = isOverElementHeight && isOverElementWidth;

	if ( this.options.tolerance === "pointer" ||
	  this.options.forcePointerForContainers ||
	  (this.options.tolerance !== "pointer" && this.helperProportions[this.floating ? "width" : "height"] > item[this.floating ? "width" : "height"])
	) {
	  return isOverElement;
	} else {

	  return (l < x1 + (this.helperProportions.width / 2) && // Right Half
		x2 - (this.helperProportions.width / 2) < r && // Left Half
		t < y1 + (this.helperProportions.height / 2) && // Bottom Half
		y2 - (this.helperProportions.height / 2) < b ); // Top Half

	}
  },

  _intersectsWithPointer: function(item) {

	var isOverElementHeight = (this.options.axis === "x") || isOverAxis(this.positionAbs.top + this.offset.click.top, item.top, item.height),
	  isOverElementWidth = (this.options.axis === "y") || isOverAxis(this.positionAbs.left + this.offset.click.left, item.left, item.width),
	  isOverElement = isOverElementHeight && isOverElementWidth,
	  verticalDirection = this._getDragVerticalDirection(),
	  horizontalDirection = this._getDragHorizontalDirection();

	if (!isOverElement) {
	  return false;
	}

	return this.floating ?
	  ( ((horizontalDirection && horizontalDirection === "right") || verticalDirection === "down") ? 2 : 1 )
	  : ( verticalDirection && (verticalDirection === "down" ? 2 : 1) );

  },

  _intersectsWithSides: function(item) {

	var isOverBottomHalf = isOverAxis(this.positionAbs.top + this.offset.click.top, item.top + (item.height/2), item.height),
	  isOverRightHalf = isOverAxis(this.positionAbs.left + this.offset.click.left, item.left + (item.width/2), item.width),
	  verticalDirection = this._getDragVerticalDirection(),
	  horizontalDirection = this._getDragHorizontalDirection();

	if (this.floating && horizontalDirection) {
	  return ((horizontalDirection === "right" && isOverRightHalf) || (horizontalDirection === "left" && !isOverRightHalf));
	} else {
	  return verticalDirection && ((verticalDirection === "down" && isOverBottomHalf) || (verticalDirection === "up" && !isOverBottomHalf));
	}

  },

  _getDragVerticalDirection: function() {
	var delta = this.positionAbs.top - this.lastPositionAbs.top;
	return delta !== 0 && (delta > 0 ? "down" : "up");
  },

  _getDragHorizontalDirection: function() {
	var delta = this.positionAbs.left - this.lastPositionAbs.left;
	return delta !== 0 && (delta > 0 ? "right" : "left");
  },

  refresh: function(event) {
	this._refreshItems(event);
	this.refreshPositions();
	return this;
  },

  _connectWith: function() {
	var options = this.options;
	return options.connectWith.constructor === String ? [options.connectWith] : options.connectWith;
  },

  _getItemsAsjQuery: function(connected) {

	var i, j, cur, inst,
	  items = [],
	  queries = [],
	  connectWith = this._connectWith();

	if(connectWith && connected) {
	  for (i = connectWith.length - 1; i >= 0; i--){
		cur = $(connectWith[i]);
		for ( j = cur.length - 1; j >= 0; j--){
		  inst = $.data(cur[j], this.widgetFullName);
		  if(inst && inst !== this && !inst.options.disabled) {
			queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element) : $(inst.options.items, inst.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), inst]);
		  }
		}
	  }
	}

	queries.push([$.isFunction(this.options.items) ? this.options.items.call(this.element, null, { options: this.options, item: this.currentItem }) : $(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]);

	function addItems() {
	  items.push( this );
	}
	for (i = queries.length - 1; i >= 0; i--){
	  queries[i][0].each( addItems );
	}

	return $(items);

  },

  _removeCurrentsFromItems: function() {

	var list = this.currentItem.find(":data(" + this.widgetName + "-item)");

	this.items = $.grep(this.items, function (item) {
	  for (var j=0; j < list.length; j++) {
		if(list[j] === item.item[0]) {
		  return false;
		}
	  }
	  return true;
	});

  },

  _refreshItems: function(event) {

	this.items = [];
	this.containers = [this];

	var i, j, cur, inst, targetData, _queries, item, queriesLength,
	  items = this.items,
	  queries = [[$.isFunction(this.options.items) ? this.options.items.call(this.element[0], event, { item: this.currentItem }) : $(this.options.items, this.element), this]],
	  connectWith = this._connectWith();

	if(connectWith && this.ready) { //Shouldn't be run the first time through due to massive slow-down
	  for (i = connectWith.length - 1; i >= 0; i--){
		cur = $(connectWith[i]);
		for (j = cur.length - 1; j >= 0; j--){
		  inst = $.data(cur[j], this.widgetFullName);
		  if(inst && inst !== this && !inst.options.disabled) {
			queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element[0], event, { item: this.currentItem }) : $(inst.options.items, inst.element), inst]);
			this.containers.push(inst);
		  }
		}
	  }
	}

	for (i = queries.length - 1; i >= 0; i--) {
	  targetData = queries[i][1];
	  _queries = queries[i][0];

	  for (j=0, queriesLength = _queries.length; j < queriesLength; j++) {
		item = $(_queries[j]);

		item.data(this.widgetName + "-item", targetData); // Data for target checking (mouse manager)

		items.push({
		  item: item,
		  instance: targetData,
		  width: 0, height: 0,
		  left: 0, top: 0
		});
	  }
	}

  },

  refreshPositions: function(fast) {

	//This has to be redone because due to the item being moved out/into the offsetParent, the offsetParent's position will change
	if(this.offsetParent && this.helper) {
	  this.offset.parent = this._getParentOffset();
	}

	var i, item, t, p;

	for (i = this.items.length - 1; i >= 0; i--){
	  item = this.items[i];

	  //We ignore calculating positions of all connected containers when we're not over them
	  if(item.instance !== this.currentContainer && this.currentContainer && item.item[0] !== this.currentItem[0]) {
		continue;
	  }

	  t = this.options.toleranceElement ? $(this.options.toleranceElement, item.item) : item.item;

	  if (!fast) {
		item.width = t.outerWidth();
		item.height = t.outerHeight();
	  }

	  p = t.offset();
	  item.left = p.left;
	  item.top = p.top;
	}

	if(this.options.custom && this.options.custom.refreshContainers) {
	  this.options.custom.refreshContainers.call(this);
	} else {
	  for (i = this.containers.length - 1; i >= 0; i--){
		p = this.containers[i].element.offset();
		this.containers[i].containerCache.left = p.left;
		this.containers[i].containerCache.top = p.top;
		this.containers[i].containerCache.width	= this.containers[i].element.outerWidth();
		this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
	  }
	}

	return this;
  },

  _createPlaceholder: function(that) {
	that = that || this;
	var className,
	  o = that.options;

	if(!o.placeholder || o.placeholder.constructor === String) {
	  className = o.placeholder;
	  o.placeholder = {
		element: function() {

		  var nodeName = that.currentItem[0].nodeName.toLowerCase(),
			element = $( "<" + nodeName + ">", that.document[0] )
			  .addClass(className || that.currentItem[0].className+" ui-sortable-placeholder")
			  .removeClass("ui-sortable-helper");

		  if ( nodeName === "tr" ) {
			that.currentItem.children().each(function() {
			  $( "<td>&#160;</td>", that.document[0] )
				.attr( "colspan", $( this ).attr( "colspan" ) || 1 )
				.appendTo( element );
			});
		  } else if ( nodeName === "img" ) {
			element.attr( "src", that.currentItem.attr( "src" ) );
		  }

		  if ( !className ) {
			element.css( "visibility", "hidden" );
		  }

		  return element;
		},
		update: function(container, p) {

		  // 1. If a className is set as 'placeholder option, we don't force sizes - the class is responsible for that
		  // 2. The option 'forcePlaceholderSize can be enabled to force it even if a class name is specified
		  if(className && !o.forcePlaceholderSize) {
			return;
		  }

		  //If the element doesn't have a actual height by itself (without styles coming from a stylesheet), it receives the inline height from the dragged item
		  if(!p.height()) { p.height(that.currentItem.innerHeight() - parseInt(that.currentItem.css("paddingTop")||0, 10) - parseInt(that.currentItem.css("paddingBottom")||0, 10)); }
		  if(!p.width()) { p.width(that.currentItem.innerWidth() - parseInt(that.currentItem.css("paddingLeft")||0, 10) - parseInt(that.currentItem.css("paddingRight")||0, 10)); }
		}
	  };
	}

	//Create the placeholder
	that.placeholder = $(o.placeholder.element.call(that.element, that.currentItem));

	//Append it after the actual current item
	that.currentItem.after(that.placeholder);

	//Update the size of the placeholder (TODO: Logic to fuzzy, see line 316/317)
	o.placeholder.update(that, that.placeholder);

  },

  _contactContainers: function(event) {
	var i, j, dist, itemWithLeastDistance, posProperty, sizeProperty, base, cur, nearBottom, floating,
	  innermostContainer = null,
	  innermostIndex = null;

	// get innermost container that intersects with item
	for (i = this.containers.length - 1; i >= 0; i--) {

	  // never consider a container that's located within the item itself
	  if($.contains(this.currentItem[0], this.containers[i].element[0])) {
		continue;
	  }

	  if(this._intersectsWith(this.containers[i].containerCache)) {

		// if we've already found a container and it's more "inner" than this, then continue
		if(innermostContainer && $.contains(this.containers[i].element[0], innermostContainer.element[0])) {
		  continue;
		}

		innermostContainer = this.containers[i];
		innermostIndex = i;

	  } else {
		// container doesn't intersect. trigger "out" event if necessary
		if(this.containers[i].containerCache.over) {
		  this.containers[i]._trigger("out", event, this._uiHash(this));
		  this.containers[i].containerCache.over = 0;
		}
	  }

	}

	// if no intersecting containers found, return
	if(!innermostContainer) {
	  return;
	}

	// move the item into the container if it's not there already
	if(this.containers.length === 1) {
	  if (!this.containers[innermostIndex].containerCache.over) {
		this.containers[innermostIndex]._trigger("over", event, this._uiHash(this));
		this.containers[innermostIndex].containerCache.over = 1;
	  }
	} else {

	  //When entering a new container, we will find the item with the least distance and append our item near it
	  dist = 10000;
	  itemWithLeastDistance = null;
	  floating = innermostContainer.floating || isFloating(this.currentItem);
	  posProperty = floating ? "left" : "top";
	  sizeProperty = floating ? "width" : "height";
	  base = this.positionAbs[posProperty] + this.offset.click[posProperty];
	  for (j = this.items.length - 1; j >= 0; j--) {
		if(!$.contains(this.containers[innermostIndex].element[0], this.items[j].item[0])) {
		  continue;
		}
		if(this.items[j].item[0] === this.currentItem[0]) {
		  continue;
		}
		if (floating && !isOverAxis(this.positionAbs.top + this.offset.click.top, this.items[j].top, this.items[j].height)) {
		  continue;
		}
		cur = this.items[j].item.offset()[posProperty];
		nearBottom = false;
		if(Math.abs(cur - base) > Math.abs(cur + this.items[j][sizeProperty] - base)){
		  nearBottom = true;
		  cur += this.items[j][sizeProperty];
		}

		if(Math.abs(cur - base) < dist) {
		  dist = Math.abs(cur - base); itemWithLeastDistance = this.items[j];
		  this.direction = nearBottom ? "up": "down";
		}
	  }

	  //Check if dropOnEmpty is enabled
	  if(!itemWithLeastDistance && !this.options.dropOnEmpty) {
		return;
	  }

	  if(this.currentContainer === this.containers[innermostIndex]) {
		return;
	  }

	  itemWithLeastDistance ? this._rearrange(event, itemWithLeastDistance, null, true) : this._rearrange(event, null, this.containers[innermostIndex].element, true);
	  this._trigger("change", event, this._uiHash());
	  this.containers[innermostIndex]._trigger("change", event, this._uiHash(this));
	  this.currentContainer = this.containers[innermostIndex];

	  //Update the placeholder
	  this.options.placeholder.update(this.currentContainer, this.placeholder);

	  this.containers[innermostIndex]._trigger("over", event, this._uiHash(this));
	  this.containers[innermostIndex].containerCache.over = 1;
	}


  },

  _createHelper: function(event) {

	var o = this.options,
	  helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event, this.currentItem])) : (o.helper === "clone" ? this.currentItem.clone() : this.currentItem);

	//Add the helper to the DOM if that didn't happen already
	if(!helper.parents("body").length) {
	  $(o.appendTo !== "parent" ? o.appendTo : this.currentItem[0].parentNode)[0].appendChild(helper[0]);
	}

	if(helper[0] === this.currentItem[0]) {
	  this._storedCSS = { width: this.currentItem[0].style.width, height: this.currentItem[0].style.height, position: this.currentItem.css("position"), top: this.currentItem.css("top"), left: this.currentItem.css("left") };
	}

	if(!helper[0].style.width || o.forceHelperSize) {
	  helper.width(this.currentItem.width());
	}
	if(!helper[0].style.height || o.forceHelperSize) {
	  helper.height(this.currentItem.height());
	}

	return helper;

  },

  _adjustOffsetFromHelper: function(obj) {
	if (typeof obj === "string") {
	  obj = obj.split(" ");
	}
	if ($.isArray(obj)) {
	  obj = {left: +obj[0], top: +obj[1] || 0};
	}
	if ("left" in obj) {
	  this.offset.click.left = obj.left + this.margins.left;
	}
	if ("right" in obj) {
	  this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left;
	}
	if ("top" in obj) {
	  this.offset.click.top = obj.top + this.margins.top;
	}
	if ("bottom" in obj) {
	  this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top;
	}
  },

  _getParentOffset: function() {


	//Get the offsetParent and cache its position
	this.offsetParent = this.helper.offsetParent();
	var po = this.offsetParent.offset();

	// This is a special case where we need to modify a offset calculated on start, since the following happened:
	// 1. The position of the helper is absolute, so it's position is calculated based on the next positioned parent
	// 2. The actual offset parent is a child of the scroll parent, and the scroll parent isn't the document, which means that
	//    the scroll is included in the initial calculation of the offset of the parent, and never recalculated upon drag
	if(this.cssPosition === "absolute" && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) {
	  po.left += this.scrollParent.scrollLeft();
	  po.top += this.scrollParent.scrollTop();
	}

	// This needs to be actually done for all browsers, since pageX/pageY includes this information
	// with an ugly IE fix
	if( this.offsetParent[0] === document.body || (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() === "html" && $.ui.ie)) {
	  po = { top: 0, left: 0 };
	}

	return {
	  top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"),10) || 0),
	  left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"),10) || 0)
	};

  },

  _getRelativeOffset: function() {

	if(this.cssPosition === "relative") {
	  var p = this.currentItem.position();
	  return {
		top: p.top - (parseInt(this.helper.css("top"),10) || 0) + this.scrollParent.scrollTop(),
		left: p.left - (parseInt(this.helper.css("left"),10) || 0) + this.scrollParent.scrollLeft()
	  };
	} else {
	  return { top: 0, left: 0 };
	}

  },

  _cacheMargins: function() {
	this.margins = {
	  left: (parseInt(this.currentItem.css("marginLeft"),10) || 0),
	  top: (parseInt(this.currentItem.css("marginTop"),10) || 0)
	};
  },

  _cacheHelperProportions: function() {
	this.helperProportions = {
	  width: this.helper.outerWidth(),
	  height: this.helper.outerHeight()
	};
  },

  _setContainment: function() {

	var ce, co, over,
	  o = this.options;
	if(o.containment === "parent") {
	  o.containment = this.helper[0].parentNode;
	}
	if(o.containment === "document" || o.containment === "window") {
	  this.containment = [
		0 - this.offset.relative.left - this.offset.parent.left,
		0 - this.offset.relative.top - this.offset.parent.top,
		$(o.containment === "document" ? document : window).width() - this.helperProportions.width - this.margins.left,
		($(o.containment === "document" ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top
	  ];
	}

	if(!(/^(document|window|parent)$/).test(o.containment)) {
	  ce = $(o.containment)[0];
	  co = $(o.containment).offset();
	  over = ($(ce).css("overflow") !== "hidden");

	  this.containment = [
		co.left + (parseInt($(ce).css("borderLeftWidth"),10) || 0) + (parseInt($(ce).css("paddingLeft"),10) || 0) - this.margins.left,
		co.top + (parseInt($(ce).css("borderTopWidth"),10) || 0) + (parseInt($(ce).css("paddingTop"),10) || 0) - this.margins.top,
		co.left+(over ? Math.max(ce.scrollWidth,ce.offsetWidth) : ce.offsetWidth) - (parseInt($(ce).css("borderLeftWidth"),10) || 0) - (parseInt($(ce).css("paddingRight"),10) || 0) - this.helperProportions.width - this.margins.left,
		co.top+(over ? Math.max(ce.scrollHeight,ce.offsetHeight) : ce.offsetHeight) - (parseInt($(ce).css("borderTopWidth"),10) || 0) - (parseInt($(ce).css("paddingBottom"),10) || 0) - this.helperProportions.height - this.margins.top
	  ];
	}

  },

  _convertPositionTo: function(d, pos) {

	if(!pos) {
	  pos = this.position;
	}
	var mod = d === "absolute" ? 1 : -1,
	  scroll = this.cssPosition === "absolute" && !(this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent,
	  scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName);

	return {
	  top: (
		pos.top	+																// The absolute mouse position
		this.offset.relative.top * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.top * mod -											// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : ( scrollIsRootNode ? 0 : scroll.scrollTop() ) ) * mod)
	  ),
	  left: (
		pos.left +																// The absolute mouse position
		this.offset.relative.left * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.left * mod	-										// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft() ) * mod)
	  )
	};

  },

  _generatePosition: function(event) {

	var top, left,
	  o = this.options,
	  pageX = event.pageX,
	  pageY = event.pageY,
	  scroll = this.cssPosition === "absolute" && !(this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, scrollIsRootNode = (/(html|body)/i).test(scroll[0].tagName);

	// This is another very weird special case that only happens for relative elements:
	// 1. If the css position is relative
	// 2. and the scroll parent is the document or similar to the offset parent
	// we have to refresh the relative offset during the scroll so there are no jumps
	if(this.cssPosition === "relative" && !(this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0])) {
	  this.offset.relative = this._getRelativeOffset();
	}

	/*
	 * - Position constraining -
	 * Constrain the position to a mix of grid, containment.
	 */

	if(this.originalPosition) { //If we are not dragging yet, we won't check for options

	  if(this.containment) {
		if(event.pageX - this.offset.click.left < this.containment[0]) {
		  pageX = this.containment[0] + this.offset.click.left;
		}
		if(event.pageY - this.offset.click.top < this.containment[1]) {
		  pageY = this.containment[1] + this.offset.click.top;
		}
		if(event.pageX - this.offset.click.left > this.containment[2]) {
		  pageX = this.containment[2] + this.offset.click.left;
		}
		if(event.pageY - this.offset.click.top > this.containment[3]) {
		  pageY = this.containment[3] + this.offset.click.top;
		}
	  }

	  if(o.grid) {
		top = this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1];
		pageY = this.containment ? ( (top - this.offset.click.top >= this.containment[1] && top - this.offset.click.top <= this.containment[3]) ? top : ((top - this.offset.click.top >= this.containment[1]) ? top - o.grid[1] : top + o.grid[1])) : top;

		left = this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0];
		pageX = this.containment ? ( (left - this.offset.click.left >= this.containment[0] && left - this.offset.click.left <= this.containment[2]) ? left : ((left - this.offset.click.left >= this.containment[0]) ? left - o.grid[0] : left + o.grid[0])) : left;
	  }

	}

	return {
	  top: (
		pageY -																// The absolute mouse position
		this.offset.click.top -													// Click offset (relative to the element)
		this.offset.relative.top	-											// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.top +												// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : ( scrollIsRootNode ? 0 : scroll.scrollTop() ) ))
	  ),
	  left: (
		pageX -																// The absolute mouse position
		this.offset.click.left -												// Click offset (relative to the element)
		this.offset.relative.left	-											// Only for relative positioned nodes: Relative offset from element to offset parent
		this.offset.parent.left +												// The offsetParent's offset without borders (offset + border)
		( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft() ))
	  )
	};

  },

  _rearrange: function(event, i, a, hardRefresh) {

	a ? a[0].appendChild(this.placeholder[0]) : i.item[0].parentNode.insertBefore(this.placeholder[0], (this.direction === "down" ? i.item[0] : i.item[0].nextSibling));

	//Various things done here to improve the performance:
	// 1. we create a setTimeout, that calls refreshPositions
	// 2. on the instance, we have a counter variable, that get's higher after every append
	// 3. on the local scope, we copy the counter variable, and check in the timeout, if it's still the same
	// 4. this lets only the last addition to the timeout stack through
	this.counter = this.counter ? ++this.counter : 1;
	var counter = this.counter;

	this._delay(function() {
	  if(counter === this.counter) {
		this.refreshPositions(!hardRefresh); //Precompute after each DOM insertion, NOT on mousemove
	  }
	});

  },

  _clear: function(event, noPropagation) {

	this.reverting = false;
	// We delay all events that have to be triggered to after the point where the placeholder has been removed and
	// everything else normalized again
	var i,
	  delayedTriggers = [];

	// We first have to update the dom position of the actual currentItem
	// Note: don't do it if the current item is already removed (by a user), or it gets reappended (see #4088)
	if(!this._noFinalSort && this.currentItem.parent().length) {
	  this.placeholder.before(this.currentItem);
	}
	this._noFinalSort = null;

	if(this.helper[0] === this.currentItem[0]) {
	  for(i in this._storedCSS) {
		if(this._storedCSS[i] === "auto" || this._storedCSS[i] === "static") {
		  this._storedCSS[i] = "";
		}
	  }
	  this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
	} else {
	  this.currentItem.show();
	}

	if(this.fromOutside && !noPropagation) {
	  delayedTriggers.push(function(event) { this._trigger("receive", event, this._uiHash(this.fromOutside)); });
	}
	if((this.fromOutside || this.domPosition.prev !== this.currentItem.prev().not(".ui-sortable-helper")[0] || this.domPosition.parent !== this.currentItem.parent()[0]) && !noPropagation) {
	  delayedTriggers.push(function(event) { this._trigger("update", event, this._uiHash()); }); //Trigger update callback if the DOM position has changed
	}

	// Check if the items Container has Changed and trigger appropriate
	// events.
	if (this !== this.currentContainer) {
	  if(!noPropagation) {
		delayedTriggers.push(function(event) { this._trigger("remove", event, this._uiHash()); });
		delayedTriggers.push((function(c) { return function(event) { c._trigger("receive", event, this._uiHash(this)); };  }).call(this, this.currentContainer));
		delayedTriggers.push((function(c) { return function(event) { c._trigger("update", event, this._uiHash(this));  }; }).call(this, this.currentContainer));
	  }
	}


	//Post events to containers
	function delayEvent( type, instance, container ) {
	  return function( event ) {
		container._trigger( type, event, instance._uiHash( instance ) );
	  };
	}
	for (i = this.containers.length - 1; i >= 0; i--){
	  if (!noPropagation) {
		delayedTriggers.push( delayEvent( "deactivate", this, this.containers[ i ] ) );
	  }
	  if(this.containers[i].containerCache.over) {
		delayedTriggers.push( delayEvent( "out", this, this.containers[ i ] ) );
		this.containers[i].containerCache.over = 0;
	  }
	}

	//Do what was originally in plugins
	if ( this.storedCursor ) {
	  this.document.find( "body" ).css( "cursor", this.storedCursor );
	  this.storedStylesheet.remove();
	}
	if(this._storedOpacity) {
	  this.helper.css("opacity", this._storedOpacity);
	}
	if(this._storedZIndex) {
	  this.helper.css("zIndex", this._storedZIndex === "auto" ? "" : this._storedZIndex);
	}

	this.dragging = false;
	if(this.cancelHelperRemoval) {
	  if(!noPropagation) {
		this._trigger("beforeStop", event, this._uiHash());
		for (i=0; i < delayedTriggers.length; i++) {
		  delayedTriggers[i].call(this, event);
		} //Trigger all delayed events
		this._trigger("stop", event, this._uiHash());
	  }

	  this.fromOutside = false;
	  return false;
	}

	if(!noPropagation) {
	  this._trigger("beforeStop", event, this._uiHash());
	}

	//$(this.placeholder[0]).remove(); would have been the jQuery way - unfortunately, it unbinds ALL events from the original node!
	this.placeholder[0].parentNode.removeChild(this.placeholder[0]);

	if(this.helper[0] !== this.currentItem[0]) {
	  this.helper.remove();
	}
	this.helper = null;

	if(!noPropagation) {
	  for (i=0; i < delayedTriggers.length; i++) {
		delayedTriggers[i].call(this, event);
	  } //Trigger all delayed events
	  this._trigger("stop", event, this._uiHash());
	}

	this.fromOutside = false;
	return true;

  },

  _trigger: function() {
	if ($.Widget.prototype._trigger.apply(this, arguments) === false) {
	  this.cancel();
	}
  },

  _uiHash: function(_inst) {
	var inst = _inst || this;
	return {
	  helper: inst.helper,
	  placeholder: inst.placeholder || $([]),
	  position: inst.position,
	  originalPosition: inst.originalPosition,
	  offset: inst.positionAbs,
	  item: inst.currentItem,
	  sender: _inst ? _inst.element : null
	};
  }

});

})(jQuery);
(function( $, undefined ) {

var uid = 0,
  hideProps = {},
  showProps = {};

hideProps.height = hideProps.paddingTop = hideProps.paddingBottom =
  hideProps.borderTopWidth = hideProps.borderBottomWidth = "hide";
showProps.height = showProps.paddingTop = showProps.paddingBottom =
  showProps.borderTopWidth = showProps.borderBottomWidth = "show";

$.widget( "ui.accordion", {
  version: "1.10.4",
  options: {
	active: 0,
	animate: {},
	collapsible: false,
	event: "click",
	header: "> li > :first-child,> :not(li):even",
	heightStyle: "auto",
	icons: {
	  activeHeader: "ui-icon-triangle-1-s",
	  header: "ui-icon-triangle-1-e"
	},

	// callbacks
	activate: null,
	beforeActivate: null
  },

  _create: function() {
	var options = this.options;
	this.prevShow = this.prevHide = $();
	this.element.addClass( "ui-accordion ui-widget ui-helper-reset" )
	  // ARIA
	  .attr( "role", "tablist" );

	// don't allow collapsible: false and active: false / null
	if ( !options.collapsible && (options.active === false || options.active == null) ) {
	  options.active = 0;
	}

	this._processPanels();
	// handle negative values
	if ( options.active < 0 ) {
	  options.active += this.headers.length;
	}
	this._refresh();
  },

  _getCreateEventData: function() {
	return {
	  header: this.active,
	  panel: !this.active.length ? $() : this.active.next(),
	  content: !this.active.length ? $() : this.active.next()
	};
  },

  _createIcons: function() {
	var icons = this.options.icons;
	if ( icons ) {
	  $( "<span>" )
		.addClass( "ui-accordion-header-icon ui-icon " + icons.header )
		.prependTo( this.headers );
	  this.active.children( ".ui-accordion-header-icon" )
		.removeClass( icons.header )
		.addClass( icons.activeHeader );
	  this.headers.addClass( "ui-accordion-icons" );
	}
  },

  _destroyIcons: function() {
	this.headers
	  .removeClass( "ui-accordion-icons" )
	  .children( ".ui-accordion-header-icon" )
		.remove();
  },

  _destroy: function() {
	var contents;

	// clean up main element
	this.element
	  .removeClass( "ui-accordion ui-widget ui-helper-reset" )
	  .removeAttr( "role" );

	// clean up headers
	this.headers
	  .removeClass( "ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top" )
	  .removeAttr( "role" )
	  .removeAttr( "aria-expanded" )
	  .removeAttr( "aria-selected" )
	  .removeAttr( "aria-controls" )
	  .removeAttr( "tabIndex" )
	  .each(function() {
		if ( /^ui-accordion/.test( this.id ) ) {
		  this.removeAttribute( "id" );
		}
	  });
	this._destroyIcons();

	// clean up content panels
	contents = this.headers.next()
	  .css( "display", "" )
	  .removeAttr( "role" )
	  .removeAttr( "aria-hidden" )
	  .removeAttr( "aria-labelledby" )
	  .removeClass( "ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled" )
	  .each(function() {
		if ( /^ui-accordion/.test( this.id ) ) {
		  this.removeAttribute( "id" );
		}
	  });
	if ( this.options.heightStyle !== "content" ) {
	  contents.css( "height", "" );
	}
  },

  _setOption: function( key, value ) {
	if ( key === "active" ) {
	  // _activate() will handle invalid values and update this.options
	  this._activate( value );
	  return;
	}

	if ( key === "event" ) {
	  if ( this.options.event ) {
		this._off( this.headers, this.options.event );
	  }
	  this._setupEvents( value );
	}

	this._super( key, value );

	// setting collapsible: false while collapsed; open first panel
	if ( key === "collapsible" && !value && this.options.active === false ) {
	  this._activate( 0 );
	}

	if ( key === "icons" ) {
	  this._destroyIcons();
	  if ( value ) {
		this._createIcons();
	  }
	}

	// #5332 - opacity doesn't cascade to positioned elements in IE
	// so we need to add the disabled class to the headers and panels
	if ( key === "disabled" ) {
	  this.headers.add( this.headers.next() )
		.toggleClass( "ui-state-disabled", !!value );
	}
  },

  _keydown: function( event ) {
	if ( event.altKey || event.ctrlKey ) {
	  return;
	}

	var keyCode = $.ui.keyCode,
	  length = this.headers.length,
	  currentIndex = this.headers.index( event.target ),
	  toFocus = false;

	switch ( event.keyCode ) {
	  case keyCode.RIGHT:
	  case keyCode.DOWN:
		toFocus = this.headers[ ( currentIndex + 1 ) % length ];
		break;
	  case keyCode.LEFT:
	  case keyCode.UP:
		toFocus = this.headers[ ( currentIndex - 1 + length ) % length ];
		break;
	  case keyCode.SPACE:
	  case keyCode.ENTER:
		this._eventHandler( event );
		break;
	  case keyCode.HOME:
		toFocus = this.headers[ 0 ];
		break;
	  case keyCode.END:
		toFocus = this.headers[ length - 1 ];
		break;
	}

	if ( toFocus ) {
	  $( event.target ).attr( "tabIndex", -1 );
	  $( toFocus ).attr( "tabIndex", 0 );
	  toFocus.focus();
	  event.preventDefault();
	}
  },

  _panelKeyDown : function( event ) {
	if ( event.keyCode === $.ui.keyCode.UP && event.ctrlKey ) {
	  $( event.currentTarget ).prev().focus();
	}
  },

  refresh: function() {
	var options = this.options;
	this._processPanels();

	// was collapsed or no panel
	if ( ( options.active === false && options.collapsible === true ) || !this.headers.length ) {
	  options.active = false;
	  this.active = $();
	// active false only when collapsible is true
	} else if ( options.active === false ) {
	  this._activate( 0 );
	// was active, but active panel is gone
	} else if ( this.active.length && !$.contains( this.element[ 0 ], this.active[ 0 ] ) ) {
	  // all remaining panel are disabled
	  if ( this.headers.length === this.headers.find(".ui-state-disabled").length ) {
		options.active = false;
		this.active = $();
	  // activate previous panel
	  } else {
		this._activate( Math.max( 0, options.active - 1 ) );
	  }
	// was active, active panel still exists
	} else {
	  // make sure active index is correct
	  options.active = this.headers.index( this.active );
	}

	this._destroyIcons();

	this._refresh();
  },

  _processPanels: function() {
	this.headers = this.element.find( this.options.header )
	  .addClass( "ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" );

	this.headers.next()
	  .addClass( "ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" )
	  .filter(":not(.ui-accordion-content-active)")
	  .hide();
  },

  _refresh: function() {
	var maxHeight,
	  options = this.options,
	  heightStyle = options.heightStyle,
	  parent = this.element.parent(),
	  accordionId = this.accordionId = "ui-accordion-" +
		(this.element.attr( "id" ) || ++uid);

	this.active = this._findActive( options.active )
	  .addClass( "ui-accordion-header-active ui-state-active ui-corner-top" )
	  .removeClass( "ui-corner-all" );
	this.active.next()
	  .addClass( "ui-accordion-content-active" )
	  .show();

	this.headers
	  .attr( "role", "tab" )
	  .each(function( i ) {
		var header = $( this ),
		  headerId = header.attr( "id" ),
		  panel = header.next(),
		  panelId = panel.attr( "id" );
		if ( !headerId ) {
		  headerId = accordionId + "-header-" + i;
		  header.attr( "id", headerId );
		}
		if ( !panelId ) {
		  panelId = accordionId + "-panel-" + i;
		  panel.attr( "id", panelId );
		}
		header.attr( "aria-controls", panelId );
		panel.attr( "aria-labelledby", headerId );
	  })
	  .next()
		.attr( "role", "tabpanel" );

	this.headers
	  .not( this.active )
	  .attr({
		"aria-selected": "false",
		"aria-expanded": "false",
		tabIndex: -1
	  })
	  .next()
		.attr({
		  "aria-hidden": "true"
		})
		.hide();

	// make sure at least one header is in the tab order
	if ( !this.active.length ) {
	  this.headers.eq( 0 ).attr( "tabIndex", 0 );
	} else {
	  this.active.attr({
		"aria-selected": "true",
		"aria-expanded": "true",
		tabIndex: 0
	  })
	  .next()
		.attr({
		  "aria-hidden": "false"
		});
	}

	this._createIcons();

	this._setupEvents( options.event );

	if ( heightStyle === "fill" ) {
	  maxHeight = parent.height();
	  this.element.siblings( ":visible" ).each(function() {
		var elem = $( this ),
		  position = elem.css( "position" );

		if ( position === "absolute" || position === "fixed" ) {
		  return;
		}
		maxHeight -= elem.outerHeight( true );
	  });

	  this.headers.each(function() {
		maxHeight -= $( this ).outerHeight( true );
	  });

	  this.headers.next()
		.each(function() {
		  $( this ).height( Math.max( 0, maxHeight -
			$( this ).innerHeight() + $( this ).height() ) );
		})
		.css( "overflow", "auto" );
	} else if ( heightStyle === "auto" ) {
	  maxHeight = 0;
	  this.headers.next()
		.each(function() {
		  maxHeight = Math.max( maxHeight, $( this ).css( "height", "" ).height() );
		})
		.height( maxHeight );
	}
  },

  _activate: function( index ) {
	var active = this._findActive( index )[ 0 ];

	// trying to activate the already active panel
	if ( active === this.active[ 0 ] ) {
	  return;
	}

	// trying to collapse, simulate a click on the currently active header
	active = active || this.active[ 0 ];

	this._eventHandler({
	  target: active,
	  currentTarget: active,
	  preventDefault: $.noop
	});
  },

  _findActive: function( selector ) {
	return typeof selector === "number" ? this.headers.eq( selector ) : $();
  },

  _setupEvents: function( event ) {
	var events = {
	  keydown: "_keydown"
	};
	if ( event ) {
	  $.each( event.split(" "), function( index, eventName ) {
		events[ eventName ] = "_eventHandler";
	  });
	}

	this._off( this.headers.add( this.headers.next() ) );
	this._on( this.headers, events );
	this._on( this.headers.next(), { keydown: "_panelKeyDown" });
	this._hoverable( this.headers );
	this._focusable( this.headers );
  },

  _eventHandler: function( event ) {
	var options = this.options,
	  active = this.active,
	  clicked = $( event.currentTarget ),
	  clickedIsActive = clicked[ 0 ] === active[ 0 ],
	  collapsing = clickedIsActive && options.collapsible,
	  toShow = collapsing ? $() : clicked.next(),
	  toHide = active.next(),
	  eventData = {
		oldHeader: active,
		oldPanel: toHide,
		newHeader: collapsing ? $() : clicked,
		newPanel: toShow
	  };

	event.preventDefault();

	if (
		// click on active header, but not collapsible
		( clickedIsActive && !options.collapsible ) ||
		// allow canceling activation
		( this._trigger( "beforeActivate", event, eventData ) === false ) ) {
	  return;
	}

	options.active = collapsing ? false : this.headers.index( clicked );

	// when the call to ._toggle() comes after the class changes
	// it causes a very odd bug in IE 8 (see #6720)
	this.active = clickedIsActive ? $() : clicked;
	this._toggle( eventData );

	// switch classes
	// corner classes on the previously active header stay after the animation
	active.removeClass( "ui-accordion-header-active ui-state-active" );
	if ( options.icons ) {
	  active.children( ".ui-accordion-header-icon" )
		.removeClass( options.icons.activeHeader )
		.addClass( options.icons.header );
	}

	if ( !clickedIsActive ) {
	  clicked
		.removeClass( "ui-corner-all" )
		.addClass( "ui-accordion-header-active ui-state-active ui-corner-top" );
	  if ( options.icons ) {
		clicked.children( ".ui-accordion-header-icon" )
		  .removeClass( options.icons.header )
		  .addClass( options.icons.activeHeader );
	  }

	  clicked
		.next()
		.addClass( "ui-accordion-content-active" );
	}
  },

  _toggle: function( data ) {
	var toShow = data.newPanel,
	  toHide = this.prevShow.length ? this.prevShow : data.oldPanel;

	// handle activating a panel during the animation for another activation
	this.prevShow.add( this.prevHide ).stop( true, true );
	this.prevShow = toShow;
	this.prevHide = toHide;

	if ( this.options.animate ) {
	  this._animate( toShow, toHide, data );
	} else {
	  toHide.hide();
	  toShow.show();
	  this._toggleComplete( data );
	}

	toHide.attr({
	  "aria-hidden": "true"
	});
	toHide.prev().attr( "aria-selected", "false" );
	// if we're switching panels, remove the old header from the tab order
	// if we're opening from collapsed state, remove the previous header from the tab order
	// if we're collapsing, then keep the collapsing header in the tab order
	if ( toShow.length && toHide.length ) {
	  toHide.prev().attr({
		"tabIndex": -1,
		"aria-expanded": "false"
	  });
	} else if ( toShow.length ) {
	  this.headers.filter(function() {
		return $( this ).attr( "tabIndex" ) === 0;
	  })
	  .attr( "tabIndex", -1 );
	}

	toShow
	  .attr( "aria-hidden", "false" )
	  .prev()
		.attr({
		  "aria-selected": "true",
		  tabIndex: 0,
		  "aria-expanded": "true"
		});
  },

  _animate: function( toShow, toHide, data ) {
	var total, easing, duration,
	  that = this,
	  adjust = 0,
	  down = toShow.length &&
		( !toHide.length || ( toShow.index() < toHide.index() ) ),
	  animate = this.options.animate || {},
	  options = down && animate.down || animate,
	  complete = function() {
		that._toggleComplete( data );
	  };

	if ( typeof options === "number" ) {
	  duration = options;
	}
	if ( typeof options === "string" ) {
	  easing = options;
	}
	// fall back from options to animation in case of partial down settings
	easing = easing || options.easing || animate.easing;
	duration = duration || options.duration || animate.duration;

	if ( !toHide.length ) {
	  return toShow.animate( showProps, duration, easing, complete );
	}
	if ( !toShow.length ) {
	  return toHide.animate( hideProps, duration, easing, complete );
	}

	total = toShow.show().outerHeight();
	toHide.animate( hideProps, {
	  duration: duration,
	  easing: easing,
	  step: function( now, fx ) {
		fx.now = Math.round( now );
	  }
	});
	toShow
	  .hide()
	  .animate( showProps, {
		duration: duration,
		easing: easing,
		complete: complete,
		step: function( now, fx ) {
		  fx.now = Math.round( now );
		  if ( fx.prop !== "height" ) {
			adjust += fx.now;
		  } else if ( that.options.heightStyle !== "content" ) {
			fx.now = Math.round( total - toHide.outerHeight() - adjust );
			adjust = 0;
		  }
		}
	  });
  },

  _toggleComplete: function( data ) {
	var toHide = data.oldPanel;

	toHide
	  .removeClass( "ui-accordion-content-active" )
	  .prev()
		.removeClass( "ui-corner-top" )
		.addClass( "ui-corner-all" );

	// Work around for rendering bug in IE (#5421)
	if ( toHide.length ) {
	  toHide.parent()[0].className = toHide.parent()[0].className;
	}
	this._trigger( "activate", null, data );
  }
});

})( jQuery );
(function( $, undefined ) {

$.widget( "ui.autocomplete", {
  version: "1.10.4",
  defaultElement: "<input>",
  options: {
	appendTo: null,
	autoFocus: false,
	delay: 300,
	minLength: 1,
	position: {
	  my: "left top",
	  at: "left bottom",
	  collision: "none"
	},
	source: null,

	// callbacks
	change: null,
	close: null,
	focus: null,
	open: null,
	response: null,
	search: null,
	select: null
  },

  requestIndex: 0,
  pending: 0,

  _create: function() {
	// Some browsers only repeat keydown events, not keypress events,
	// so we use the suppressKeyPress flag to determine if we've already
	// handled the keydown event. #7269
	// Unfortunately the code for & in keypress is the same as the up arrow,
	// so we use the suppressKeyPressRepeat flag to avoid handling keypress
	// events when we know the keydown event was used to modify the
	// search term. #7799
	var suppressKeyPress, suppressKeyPressRepeat, suppressInput,
	  nodeName = this.element[0].nodeName.toLowerCase(),
	  isTextarea = nodeName === "textarea",
	  isInput = nodeName === "input";

	this.isMultiLine =
	  // Textareas are always multi-line
	  isTextarea ? true :
	  // Inputs are always single-line, even if inside a contentEditable element
	  // IE also treats inputs as contentEditable
	  isInput ? false :
	  // All other element types are determined by whether or not they're contentEditable
	  this.element.prop( "isContentEditable" );

	this.valueMethod = this.element[ isTextarea || isInput ? "val" : "text" ];
	this.isNewMenu = true;

	this.element
	  .addClass( "ui-autocomplete-input" )
	  .attr( "autocomplete", "off" );

	this._on( this.element, {
	  keydown: function( event ) {
		if ( this.element.prop( "readOnly" ) ) {
		  suppressKeyPress = true;
		  suppressInput = true;
		  suppressKeyPressRepeat = true;
		  return;
		}

		suppressKeyPress = false;
		suppressInput = false;
		suppressKeyPressRepeat = false;
		var keyCode = $.ui.keyCode;
		switch( event.keyCode ) {
		case keyCode.PAGE_UP:
		  suppressKeyPress = true;
		  this._move( "previousPage", event );
		  break;
		case keyCode.PAGE_DOWN:
		  suppressKeyPress = true;
		  this._move( "nextPage", event );
		  break;
		case keyCode.UP:
		  suppressKeyPress = true;
		  this._keyEvent( "previous", event );
		  break;
		case keyCode.DOWN:
		  suppressKeyPress = true;
		  this._keyEvent( "next", event );
		  break;
		case keyCode.ENTER:
		case keyCode.NUMPAD_ENTER:
		  // when menu is open and has focus
		  if ( this.menu.active ) {
			// #6055 - Opera still allows the keypress to occur
			// which causes forms to submit
			suppressKeyPress = true;
			event.preventDefault();
			this.menu.select( event );
		  }
		  break;
		case keyCode.TAB:
		  if ( this.menu.active ) {
			this.menu.select( event );
		  }
		  break;
		case keyCode.ESCAPE:
		  if ( this.menu.element.is( ":visible" ) ) {
			this._value( this.term );
			this.close( event );
			// Different browsers have different default behavior for escape
			// Single press can mean undo or clear
			// Double press in IE means clear the whole form
			event.preventDefault();
		  }
		  break;
		default:
		  suppressKeyPressRepeat = true;
		  // search timeout should be triggered before the input value is changed
		  this._searchTimeout( event );
		  break;
		}
	  },
	  keypress: function( event ) {
		if ( suppressKeyPress ) {
		  suppressKeyPress = false;
		  if ( !this.isMultiLine || this.menu.element.is( ":visible" ) ) {
			event.preventDefault();
		  }
		  return;
		}
		if ( suppressKeyPressRepeat ) {
		  return;
		}

		// replicate some key handlers to allow them to repeat in Firefox and Opera
		var keyCode = $.ui.keyCode;
		switch( event.keyCode ) {
		case keyCode.PAGE_UP:
		  this._move( "previousPage", event );
		  break;
		case keyCode.PAGE_DOWN:
		  this._move( "nextPage", event );
		  break;
		case keyCode.UP:
		  this._keyEvent( "previous", event );
		  break;
		case keyCode.DOWN:
		  this._keyEvent( "next", event );
		  break;
		}
	  },
	  input: function( event ) {
		if ( suppressInput ) {
		  suppressInput = false;
		  event.preventDefault();
		  return;
		}
		this._searchTimeout( event );
	  },
	  focus: function() {
		this.selectedItem = null;
		this.previous = this._value();
	  },
	  blur: function( event ) {
		if ( this.cancelBlur ) {
		  delete this.cancelBlur;
		  return;
		}

		clearTimeout( this.searching );
		this.close( event );
		this._change( event );
	  }
	});

	this._initSource();
	this.menu = $( "<ul>" )
	  .addClass( "ui-autocomplete ui-front" )
	  .appendTo( this._appendTo() )
	  .menu({
		// disable ARIA support, the live region takes care of that
		role: null
	  })
	  .hide()
	  .data( "ui-menu" );

	this._on( this.menu.element, {
	  mousedown: function( event ) {
		// prevent moving focus out of the text field
		event.preventDefault();

		// IE doesn't prevent moving focus even with event.preventDefault()
		// so we set a flag to know when we should ignore the blur event
		this.cancelBlur = true;
		this._delay(function() {
		  delete this.cancelBlur;
		});

		// clicking on the scrollbar causes focus to shift to the body
		// but we can't detect a mouseup or a click immediately afterward
		// so we have to track the next mousedown and close the menu if
		// the user clicks somewhere outside of the autocomplete
		var menuElement = this.menu.element[ 0 ];
		if ( !$( event.target ).closest( ".ui-menu-item" ).length ) {
		  this._delay(function() {
			var that = this;
			this.document.one( "mousedown", function( event ) {
			  if ( event.target !== that.element[ 0 ] &&
				  event.target !== menuElement &&
				  !$.contains( menuElement, event.target ) ) {
				that.close();
			  }
			});
		  });
		}
	  },
	  menufocus: function( event, ui ) {
		// support: Firefox
		// Prevent accidental activation of menu items in Firefox (#7024 #9118)
		if ( this.isNewMenu ) {
		  this.isNewMenu = false;
		  if ( event.originalEvent && /^mouse/.test( event.originalEvent.type ) ) {
			this.menu.blur();

			this.document.one( "mousemove", function() {
			  $( event.target ).trigger( event.originalEvent );
			});

			return;
		  }
		}

		var item = ui.item.data( "ui-autocomplete-item" );
		if ( false !== this._trigger( "focus", event, { item: item } ) ) {
		  // use value to match what will end up in the input, if it was a key event
		  if ( event.originalEvent && /^key/.test( event.originalEvent.type ) ) {
			this._value( item.value );
		  }
		} else {
		  // Normally the input is populated with the item's value as the
		  // menu is navigated, causing screen readers to notice a change and
		  // announce the item. Since the focus event was canceled, this doesn't
		  // happen, so we update the live region so that screen readers can
		  // still notice the change and announce it.
		  this.liveRegion.text( item.value );
		}
	  },
	  menuselect: function( event, ui ) {
		var item = ui.item.data( "ui-autocomplete-item" ),
		  previous = this.previous;

		// only trigger when focus was lost (click on menu)
		if ( this.element[0] !== this.document[0].activeElement ) {
		  this.element.focus();
		  this.previous = previous;
		  // #6109 - IE triggers two focus events and the second
		  // is asynchronous, so we need to reset the previous
		  // term synchronously and asynchronously :-(
		  this._delay(function() {
			this.previous = previous;
			this.selectedItem = item;
		  });
		}

		if ( false !== this._trigger( "select", event, { item: item } ) ) {
		  this._value( item.value );
		}
		// reset the term after the select event
		// this allows custom select handling to work properly
		this.term = this._value();

		this.close( event );
		this.selectedItem = item;
	  }
	});

	this.liveRegion = $( "<span>", {
		role: "status",
		"aria-live": "polite"
	  })
	  .addClass( "ui-helper-hidden-accessible" )
	  .insertBefore( this.element );

	// turning off autocomplete prevents the browser from remembering the
	// value when navigating through history, so we re-enable autocomplete
	// if the page is unloaded before the widget is destroyed. #7790
	this._on( this.window, {
	  beforeunload: function() {
		this.element.removeAttr( "autocomplete" );
	  }
	});
  },

  _destroy: function() {
	clearTimeout( this.searching );
	this.element
	  .removeClass( "ui-autocomplete-input" )
	  .removeAttr( "autocomplete" );
	this.menu.element.remove();
	this.liveRegion.remove();
  },

  _setOption: function( key, value ) {
	this._super( key, value );
	if ( key === "source" ) {
	  this._initSource();
	}
	if ( key === "appendTo" ) {
	  this.menu.element.appendTo( this._appendTo() );
	}
	if ( key === "disabled" && value && this.xhr ) {
	  this.xhr.abort();
	}
  },

  _appendTo: function() {
	var element = this.options.appendTo;

	if ( element ) {
	  element = element.jquery || element.nodeType ?
		$( element ) :
		this.document.find( element ).eq( 0 );
	}

	if ( !element ) {
	  element = this.element.closest( ".ui-front" );
	}

	if ( !element.length ) {
	  element = this.document[0].body;
	}

	return element;
  },

  _initSource: function() {
	var array, url,
	  that = this;
	if ( $.isArray(this.options.source) ) {
	  array = this.options.source;
	  this.source = function( request, response ) {
		response( $.ui.autocomplete.filter( array, request.term ) );
	  };
	} else if ( typeof this.options.source === "string" ) {
	  url = this.options.source;
	  this.source = function( request, response ) {
		if ( that.xhr ) {
		  that.xhr.abort();
		}
		that.xhr = $.ajax({
		  url: url,
		  data: request,
		  dataType: "json",
		  success: function( data ) {
			response( data );
		  },
		  error: function() {
			response( [] );
		  }
		});
	  };
	} else {
	  this.source = this.options.source;
	}
  },

  _searchTimeout: function( event ) {
	clearTimeout( this.searching );
	this.searching = this._delay(function() {
	  // only search if the value has changed
	  if ( this.term !== this._value() ) {
		this.selectedItem = null;
		this.search( null, event );
	  }
	}, this.options.delay );
  },

  search: function( value, event ) {
	value = value != null ? value : this._value();

	// always save the actual value, not the one passed as an argument
	this.term = this._value();

	if ( value.length < this.options.minLength ) {
	  return this.close( event );
	}

	if ( this._trigger( "search", event ) === false ) {
	  return;
	}

	return this._search( value );
  },

  _search: function( value ) {
	this.pending++;
	this.element.addClass( "ui-autocomplete-loading" );
	this.cancelSearch = false;

	this.source( { term: value }, this._response() );
  },

  _response: function() {
	var index = ++this.requestIndex;

	return $.proxy(function( content ) {
	  if ( index === this.requestIndex ) {
		this.__response( content );
	  }

	  this.pending--;
	  if ( !this.pending ) {
		this.element.removeClass( "ui-autocomplete-loading" );
	  }
	}, this );
  },

  __response: function( content ) {
	if ( content ) {
	  content = this._normalize( content );
	}
	this._trigger( "response", null, { content: content } );
	if ( !this.options.disabled && content && content.length && !this.cancelSearch ) {
	  this._suggest( content );
	  this._trigger( "open" );
	} else {
	  // use ._close() instead of .close() so we don't cancel future searches
	  this._close();
	}
  },

  close: function( event ) {
	this.cancelSearch = true;
	this._close( event );
  },

  _close: function( event ) {
	if ( this.menu.element.is( ":visible" ) ) {
	  this.menu.element.hide();
	  this.menu.blur();
	  this.isNewMenu = true;
	  this._trigger( "close", event );
	}
  },

  _change: function( event ) {
	if ( this.previous !== this._value() ) {
	  this._trigger( "change", event, { item: this.selectedItem } );
	}
  },

  _normalize: function( items ) {
	// assume all items have the right format when the first item is complete
	if ( items.length && items[0].label && items[0].value ) {
	  return items;
	}
	return $.map( items, function( item ) {
	  if ( typeof item === "string" ) {
		return {
		  label: item,
		  value: item
		};
	  }
	  return $.extend({
		label: item.label || item.value,
		value: item.value || item.label
	  }, item );
	});
  },

  _suggest: function( items ) {
	var ul = this.menu.element.empty();
	this._renderMenu( ul, items );
	this.isNewMenu = true;
	this.menu.refresh();

	// size and position menu
	ul.show();
	this._resizeMenu();
	ul.position( $.extend({
	  of: this.element
	}, this.options.position ));

	if ( this.options.autoFocus ) {
	  this.menu.next();
	}
  },

  _resizeMenu: function() {
	var ul = this.menu.element;
	ul.outerWidth( Math.max(
	  // Firefox wraps long text (possibly a rounding bug)
	  // so we add 1px to avoid the wrapping (#7513)
	  ul.width( "" ).outerWidth() + 1,
	  this.element.outerWidth()
	) );
  },

  _renderMenu: function( ul, items ) {
	var that = this;
	$.each( items, function( index, item ) {
	  that._renderItemData( ul, item );
	});
  },

  _renderItemData: function( ul, item ) {
	return this._renderItem( ul, item ).data( "ui-autocomplete-item", item );
  },

  _renderItem: function( ul, item ) {
	return $( "<li>" )
	  .append( $( "<a>" ).text( item.label ) )
	  .appendTo( ul );
  },

  _move: function( direction, event ) {
	if ( !this.menu.element.is( ":visible" ) ) {
	  this.search( null, event );
	  return;
	}
	if ( this.menu.isFirstItem() && /^previous/.test( direction ) ||
		this.menu.isLastItem() && /^next/.test( direction ) ) {
	  this._value( this.term );
	  this.menu.blur();
	  return;
	}
	this.menu[ direction ]( event );
  },

  widget: function() {
	return this.menu.element;
  },

  _value: function() {
	return this.valueMethod.apply( this.element, arguments );
  },

  _keyEvent: function( keyEvent, event ) {
	if ( !this.isMultiLine || this.menu.element.is( ":visible" ) ) {
	  this._move( keyEvent, event );

	  // prevents moving cursor to beginning/end of the text field in some browsers
	  event.preventDefault();
	}
  }
});

$.extend( $.ui.autocomplete, {
  escapeRegex: function( value ) {
	return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
  },
  filter: function(array, term) {
	var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
	return $.grep( array, function(value) {
	  return matcher.test( value.label || value.value || value );
	});
  }
});


// live region extension, adding a `messages` option
// NOTE: This is an experimental API. We are still investigating
// a full solution for string manipulation and internationalization.
$.widget( "ui.autocomplete", $.ui.autocomplete, {
  options: {
	messages: {
	  noResults: "No search results.",
	  results: function( amount ) {
		return amount + ( amount > 1 ? " results are" : " result is" ) +
		  " available, use up and down arrow keys to navigate.";
	  }
	}
  },

  __response: function( content ) {
	var message;
	this._superApply( arguments );
	if ( this.options.disabled || this.cancelSearch ) {
	  return;
	}
	if ( content && content.length ) {
	  message = this.options.messages.results( content.length );
	} else {
	  message = this.options.messages.noResults;
	}
	this.liveRegion.text( message );
  }
});

}( jQuery ));
(function( $, undefined ) {

var lastActive,
  baseClasses = "ui-button ui-widget ui-state-default ui-corner-all",
  typeClasses = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",
  formResetHandler = function() {
	var form = $( this );
	setTimeout(function() {
	  form.find( ":ui-button" ).button( "refresh" );
	}, 1 );
  },
  radioGroup = function( radio ) {
	var name = radio.name,
	  form = radio.form,
	  radios = $( [] );
	if ( name ) {
	  name = name.replace( /'/g, "\\'" );
	  if ( form ) {
		radios = $( form ).find( "[name='" + name + "']" );
	  } else {
		radios = $( "[name='" + name + "']", radio.ownerDocument )
		  .filter(function() {
			return !this.form;
		  });
	  }
	}
	return radios;
  };

$.widget( "ui.button", {
  version: "1.10.4",
  defaultElement: "<button>",
  options: {
	disabled: null,
	text: true,
	label: null,
	icons: {
	  primary: null,
	  secondary: null
	}
  },
  _create: function() {
	this.element.closest( "form" )
	  .unbind( "reset" + this.eventNamespace )
	  .bind( "reset" + this.eventNamespace, formResetHandler );

	if ( typeof this.options.disabled !== "boolean" ) {
	  this.options.disabled = !!this.element.prop( "disabled" );
	} else {
	  this.element.prop( "disabled", this.options.disabled );
	}

	this._determineButtonType();
	this.hasTitle = !!this.buttonElement.attr( "title" );

	var that = this,
	  options = this.options,
	  toggleButton = this.type === "checkbox" || this.type === "radio",
	  activeClass = !toggleButton ? "ui-state-active" : "";

	if ( options.label === null ) {
	  options.label = (this.type === "input" ? this.buttonElement.val() : this.buttonElement.html());
	}

	this._hoverable( this.buttonElement );

	this.buttonElement
	  .addClass( baseClasses )
	  .attr( "role", "button" )
	  .bind( "mouseenter" + this.eventNamespace, function() {
		if ( options.disabled ) {
		  return;
		}
		if ( this === lastActive ) {
		  $( this ).addClass( "ui-state-active" );
		}
	  })
	  .bind( "mouseleave" + this.eventNamespace, function() {
		if ( options.disabled ) {
		  return;
		}
		$( this ).removeClass( activeClass );
	  })
	  .bind( "click" + this.eventNamespace, function( event ) {
		if ( options.disabled ) {
		  event.preventDefault();
		  event.stopImmediatePropagation();
		}
	  });

	// Can't use _focusable() because the element that receives focus
	// and the element that gets the ui-state-focus class are different
	this._on({
	  focus: function() {
		this.buttonElement.addClass( "ui-state-focus" );
	  },
	  blur: function() {
		this.buttonElement.removeClass( "ui-state-focus" );
	  }
	});

	if ( toggleButton ) {
	  this.element.bind( "change" + this.eventNamespace, function() {
		that.refresh();
	  });
	}

	if ( this.type === "checkbox" ) {
	  this.buttonElement.bind( "click" + this.eventNamespace, function() {
		if ( options.disabled ) {
		  return false;
		}
	  });
	} else if ( this.type === "radio" ) {
	  this.buttonElement.bind( "click" + this.eventNamespace, function() {
		if ( options.disabled ) {
		  return false;
		}
		$( this ).addClass( "ui-state-active" );
		that.buttonElement.attr( "aria-pressed", "true" );

		var radio = that.element[ 0 ];
		radioGroup( radio )
		  .not( radio )
		  .map(function() {
			return $( this ).button( "widget" )[ 0 ];
		  })
		  .removeClass( "ui-state-active" )
		  .attr( "aria-pressed", "false" );
	  });
	} else {
	  this.buttonElement
		.bind( "mousedown" + this.eventNamespace, function() {
		  if ( options.disabled ) {
			return false;
		  }
		  $( this ).addClass( "ui-state-active" );
		  lastActive = this;
		  that.document.one( "mouseup", function() {
			lastActive = null;
		  });
		})
		.bind( "mouseup" + this.eventNamespace, function() {
		  if ( options.disabled ) {
			return false;
		  }
		  $( this ).removeClass( "ui-state-active" );
		})
		.bind( "keydown" + this.eventNamespace, function(event) {
		  if ( options.disabled ) {
			return false;
		  }
		  if ( event.keyCode === $.ui.keyCode.SPACE || event.keyCode === $.ui.keyCode.ENTER ) {
			$( this ).addClass( "ui-state-active" );
		  }
		})
		// see #8559, we bind to blur here in case the button element loses
		// focus between keydown and keyup, it would be left in an "active" state
		.bind( "keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
		  $( this ).removeClass( "ui-state-active" );
		});

	  if ( this.buttonElement.is("a") ) {
		this.buttonElement.keyup(function(event) {
		  if ( event.keyCode === $.ui.keyCode.SPACE ) {
			// TODO pass through original event correctly (just as 2nd argument doesn't work)
			$( this ).click();
		  }
		});
	  }
	}

	// TODO: pull out $.Widget's handling for the disabled option into
	// $.Widget.prototype._setOptionDisabled so it's easy to proxy and can
	// be overridden by individual plugins
	this._setOption( "disabled", options.disabled );
	this._resetButton();
  },

  _determineButtonType: function() {
	var ancestor, labelSelector, checked;

	if ( this.element.is("[type=checkbox]") ) {
	  this.type = "checkbox";
	} else if ( this.element.is("[type=radio]") ) {
	  this.type = "radio";
	} else if ( this.element.is("input") ) {
	  this.type = "input";
	} else {
	  this.type = "button";
	}

	if ( this.type === "checkbox" || this.type === "radio" ) {
	  // we don't search against the document in case the element
	  // is disconnected from the DOM
	  ancestor = this.element.parents().last();
	  labelSelector = "label[for='" + this.element.attr("id") + "']";
	  this.buttonElement = ancestor.find( labelSelector );
	  if ( !this.buttonElement.length ) {
		ancestor = ancestor.length ? ancestor.siblings() : this.element.siblings();
		this.buttonElement = ancestor.filter( labelSelector );
		if ( !this.buttonElement.length ) {
		  this.buttonElement = ancestor.find( labelSelector );
		}
	  }
	  this.element.addClass( "ui-helper-hidden-accessible" );

	  checked = this.element.is( ":checked" );
	  if ( checked ) {
		this.buttonElement.addClass( "ui-state-active" );
	  }
	  this.buttonElement.prop( "aria-pressed", checked );
	} else {
	  this.buttonElement = this.element;
	}
  },

  widget: function() {
	return this.buttonElement;
  },

  _destroy: function() {
	this.element
	  .removeClass( "ui-helper-hidden-accessible" );
	this.buttonElement
	  .removeClass( baseClasses + " ui-state-active " + typeClasses )
	  .removeAttr( "role" )
	  .removeAttr( "aria-pressed" )
	  .html( this.buttonElement.find(".ui-button-text").html() );

	if ( !this.hasTitle ) {
	  this.buttonElement.removeAttr( "title" );
	}
  },

  _setOption: function( key, value ) {
	this._super( key, value );
	if ( key === "disabled" ) {
	  this.element.prop( "disabled", !!value );
	  if ( value ) {
		this.buttonElement.removeClass( "ui-state-focus" );
	  }
	  return;
	}
	this._resetButton();
  },

  refresh: function() {
	//See #8237 & #8828
	var isDisabled = this.element.is( "input, button" ) ? this.element.is( ":disabled" ) : this.element.hasClass( "ui-button-disabled" );

	if ( isDisabled !== this.options.disabled ) {
	  this._setOption( "disabled", isDisabled );
	}
	if ( this.type === "radio" ) {
	  radioGroup( this.element[0] ).each(function() {
		if ( $( this ).is( ":checked" ) ) {
		  $( this ).button( "widget" )
			.addClass( "ui-state-active" )
			.attr( "aria-pressed", "true" );
		} else {
		  $( this ).button( "widget" )
			.removeClass( "ui-state-active" )
			.attr( "aria-pressed", "false" );
		}
	  });
	} else if ( this.type === "checkbox" ) {
	  if ( this.element.is( ":checked" ) ) {
		this.buttonElement
		  .addClass( "ui-state-active" )
		  .attr( "aria-pressed", "true" );
	  } else {
		this.buttonElement
		  .removeClass( "ui-state-active" )
		  .attr( "aria-pressed", "false" );
	  }
	}
  },

  _resetButton: function() {
	if ( this.type === "input" ) {
	  if ( this.options.label ) {
		this.element.val( this.options.label );
	  }
	  return;
	}
	var buttonElement = this.buttonElement.removeClass( typeClasses ),
	  buttonText = $( "<span></span>", this.document[0] )
		.addClass( "ui-button-text" )
		.html( this.options.label )
		.appendTo( buttonElement.empty() )
		.text(),
	  icons = this.options.icons,
	  multipleIcons = icons.primary && icons.secondary,
	  buttonClasses = [];

	if ( icons.primary || icons.secondary ) {
	  if ( this.options.text ) {
		buttonClasses.push( "ui-button-text-icon" + ( multipleIcons ? "s" : ( icons.primary ? "-primary" : "-secondary" ) ) );
	  }

	  if ( icons.primary ) {
		buttonElement.prepend( "<span class='ui-button-icon-primary ui-icon " + icons.primary + "'></span>" );
	  }

	  if ( icons.secondary ) {
		buttonElement.append( "<span class='ui-button-icon-secondary ui-icon " + icons.secondary + "'></span>" );
	  }

	  if ( !this.options.text ) {
		buttonClasses.push( multipleIcons ? "ui-button-icons-only" : "ui-button-icon-only" );

		if ( !this.hasTitle ) {
		  buttonElement.attr( "title", $.trim( buttonText ) );
		}
	  }
	} else {
	  buttonClasses.push( "ui-button-text-only" );
	}
	buttonElement.addClass( buttonClasses.join( " " ) );
  }
});

$.widget( "ui.buttonset", {
  version: "1.10.4",
  options: {
	items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
  },

  _create: function() {
	this.element.addClass( "ui-buttonset" );
  },

  _init: function() {
	this.refresh();
  },

  _setOption: function( key, value ) {
	if ( key === "disabled" ) {
	  this.buttons.button( "option", key, value );
	}

	this._super( key, value );
  },

  refresh: function() {
	var rtl = this.element.css( "direction" ) === "rtl";

	this.buttons = this.element.find( this.options.items )
	  .filter( ":ui-button" )
		.button( "refresh" )
	  .end()
	  .not( ":ui-button" )
		.button()
	  .end()
	  .map(function() {
		return $( this ).button( "widget" )[ 0 ];
	  })
		.removeClass( "ui-corner-all ui-corner-left ui-corner-right" )
		.filter( ":first" )
		  .addClass( rtl ? "ui-corner-right" : "ui-corner-left" )
		.end()
		.filter( ":last" )
		  .addClass( rtl ? "ui-corner-left" : "ui-corner-right" )
		.end()
	  .end();
  },

  _destroy: function() {
	this.element.removeClass( "ui-buttonset" );
	this.buttons
	  .map(function() {
		return $( this ).button( "widget" )[ 0 ];
	  })
		.removeClass( "ui-corner-left ui-corner-right" )
	  .end()
	  .button( "destroy" );
  }
});

}( jQuery ) );
(function( $, undefined ) {

$.extend($.ui, { datepicker: { version: "1.10.4" } });

var PROP_NAME = "datepicker",
  instActive;

/* Date picker manager.
   Use the singleton instance of this class, $.datepicker, to interact with the date picker.
   Settings for (groups of) date pickers are maintained in an instance object,
   allowing multiple different settings on the same page. */

function Datepicker() {
  this._curInst = null; // The current instance in use
  this._keyEvent = false; // If the last event was a key event
  this._disabledInputs = []; // List of date picker inputs that have been disabled
  this._datepickerShowing = false; // True if the popup picker is showing , false if not
  this._inDialog = false; // True if showing within a "dialog", false if not
  this._mainDivId = "ui-datepicker-div"; // The ID of the main datepicker division
  this._inlineClass = "ui-datepicker-inline"; // The name of the inline marker class
  this._appendClass = "ui-datepicker-append"; // The name of the append marker class
  this._triggerClass = "ui-datepicker-trigger"; // The name of the trigger marker class
  this._dialogClass = "ui-datepicker-dialog"; // The name of the dialog marker class
  this._disableClass = "ui-datepicker-disabled"; // The name of the disabled covering marker class
  this._unselectableClass = "ui-datepicker-unselectable"; // The name of the unselectable cell marker class
  this._currentClass = "ui-datepicker-current-day"; // The name of the current day marker class
  this._dayOverClass = "ui-datepicker-days-cell-over"; // The name of the day hover marker class
  this.regional = []; // Available regional settings, indexed by language code
  this.regional[""] = { // Default regional settings
	closeText: "Done", // Display text for close link
	prevText: "Prev", // Display text for previous month link
	nextText: "Next", // Display text for next month link
	currentText: "Today", // Display text for current month link
	monthNames: ["January","February","March","April","May","June",
	  "July","August","September","October","November","December"], // Names of months for drop-down and formatting
	monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], // For formatting
	dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], // For formatting
	dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], // For formatting
	dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"], // Column headings for days starting at Sunday
	weekHeader: "Wk", // Column header for week of the year
	dateFormat: "mm/dd/yy", // See format options on parseDate
	firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
	isRTL: false, // True if right-to-left language, false if left-to-right
	showMonthAfterYear: false, // True if the year select precedes month, false for month then year
	yearSuffix: "" // Additional text to append to the year in the month headers
  };
  this._defaults = { // Global defaults for all the date picker instances
	showOn: "focus", // "focus" for popup on focus,
	  // "button" for trigger button, or "both" for either
	showAnim: "fadeIn", // Name of jQuery animation for popup
	showOptions: {}, // Options for enhanced animations
	defaultDate: null, // Used when field is blank: actual date,
	  // +/-number for offset from today, null for today
	appendText: "", // Display text following the input box, e.g. showing the format
	buttonText: "...", // Text for trigger button
	buttonImage: "", // URL for trigger button image
	buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
	hideIfNoPrevNext: false, // True to hide next/previous month links
	  // if not applicable, false to just disable them
	navigationAsDateFormat: false, // True if date formatting applied to prev/today/next links
	gotoCurrent: false, // True if today link goes back to current selection instead
	changeMonth: false, // True if month can be selected directly, false if only prev/next
	changeYear: false, // True if year can be selected directly, false if only prev/next
	yearRange: "c-10:c+10", // Range of years to display in drop-down,
	  // either relative to today's year (-nn:+nn), relative to currently displayed year
	  // (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
	showOtherMonths: false, // True to show dates in other months, false to leave blank
	selectOtherMonths: false, // True to allow selection of dates in other months, false for unselectable
	showWeek: false, // True to show week of the year, false to not show it
	calculateWeek: this.iso8601Week, // How to calculate the week of the year,
	  // takes a Date and returns the number of the week for it
	shortYearCutoff: "+10", // Short year values < this are in the current century,
	  // > this are in the previous century,
	  // string value starting with "+" for current year + value
	minDate: null, // The earliest selectable date, or null for no limit
	maxDate: null, // The latest selectable date, or null for no limit
	duration: "fast", // Duration of display/closure
	beforeShowDay: null, // Function that takes a date and returns an array with
	  // [0] = true if selectable, false if not, [1] = custom CSS class name(s) or "",
	  // [2] = cell title (optional), e.g. $.datepicker.noWeekends
	beforeShow: null, // Function that takes an input field and
	  // returns a set of custom settings for the date picker
	onSelect: null, // Define a callback function when a date is selected
	onChangeMonthYear: null, // Define a callback function when the month or year is changed
	onClose: null, // Define a callback function when the datepicker is closed
	numberOfMonths: 1, // Number of months to show at a time
	showCurrentAtPos: 0, // The position in multipe months at which to show the current month (starting at 0)
	stepMonths: 1, // Number of months to step back/forward
	stepBigMonths: 12, // Number of months to step back/forward for the big links
	altField: "", // Selector for an alternate field to store selected dates into
	altFormat: "", // The date format to use for the alternate field
	constrainInput: true, // The input is constrained by the current date format
	showButtonPanel: false, // True to show button panel, false to not show it
	autoSize: false, // True to size the input for the date format, false to leave as is
	disabled: false // The initial disabled state
  };
  $.extend(this._defaults, this.regional[""]);
  this.dpDiv = bindHover($("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));
}

$.extend(Datepicker.prototype, {
  /* Class name added to elements to indicate already configured with a date picker. */
  markerClassName: "hasDatepicker",

  //Keep track of the maximum number of rows displayed (see #7043)
  maxRows: 4,

  // TODO rename to "widget" when switching to widget factory
  _widgetDatepicker: function() {
	return this.dpDiv;
  },

  /* Override the default settings for all instances of the date picker.
   * @param  settings  object - the new settings to use as defaults (anonymous object)
   * @return the manager object
   */
  setDefaults: function(settings) {
	extendRemove(this._defaults, settings || {});
	return this;
  },

  /* Attach the date picker to a jQuery selection.
   * @param  target	element - the target input field or division or span
   * @param  settings  object - the new settings to use for this date picker instance (anonymous)
   */
  _attachDatepicker: function(target, settings) {
	var nodeName, inline, inst;
	nodeName = target.nodeName.toLowerCase();
	inline = (nodeName === "div" || nodeName === "span");
	if (!target.id) {
	  this.uuid += 1;
	  target.id = "dp" + this.uuid;
	}
	inst = this._newInst($(target), inline);
	inst.settings = $.extend({}, settings || {});
	if (nodeName === "input") {
	  this._connectDatepicker(target, inst);
	} else if (inline) {
	  this._inlineDatepicker(target, inst);
	}
  },

  /* Create a new instance object. */
  _newInst: function(target, inline) {
	var id = target[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"); // escape jQuery meta chars
	return {id: id, input: target, // associated target
	  selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
	  drawMonth: 0, drawYear: 0, // month being drawn
	  inline: inline, // is datepicker inline or not
	  dpDiv: (!inline ? this.dpDiv : // presentation div
	  bindHover($("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")))};
  },

  /* Attach the date picker to an input field. */
  _connectDatepicker: function(target, inst) {
	var input = $(target);
	inst.append = $([]);
	inst.trigger = $([]);
	if (input.hasClass(this.markerClassName)) {
	  return;
	}
	this._attachments(input, inst);
	input.addClass(this.markerClassName).keydown(this._doKeyDown).
	  keypress(this._doKeyPress).keyup(this._doKeyUp);
	this._autoSize(inst);
	$.data(target, PROP_NAME, inst);
	//If disabled option is true, disable the datepicker once it has been attached to the input (see ticket #5665)
	if( inst.settings.disabled ) {
	  this._disableDatepicker( target );
	}
  },

  /* Make attachments based on settings. */
  _attachments: function(input, inst) {
	var showOn, buttonText, buttonImage,
	  appendText = this._get(inst, "appendText"),
	  isRTL = this._get(inst, "isRTL");

	if (inst.append) {
	  inst.append.remove();
	}
	if (appendText) {
	  inst.append = $("<span class='" + this._appendClass + "'>" + appendText + "</span>");
	  input[isRTL ? "before" : "after"](inst.append);
	}

	input.unbind("focus", this._showDatepicker);

	if (inst.trigger) {
	  inst.trigger.remove();
	}

	showOn = this._get(inst, "showOn");
	if (showOn === "focus" || showOn === "both") { // pop-up date picker when in the marked field
	  input.focus(this._showDatepicker);
	}
	if (showOn === "button" || showOn === "both") { // pop-up date picker when button clicked
	  buttonText = this._get(inst, "buttonText");
	  buttonImage = this._get(inst, "buttonImage");
	  inst.trigger = $(this._get(inst, "buttonImageOnly") ?
		$("<img/>").addClass(this._triggerClass).
		  attr({ src: buttonImage, alt: buttonText, title: buttonText }) :
		$("<button type='button'></button>").addClass(this._triggerClass).
		  html(!buttonImage ? buttonText : $("<img/>").attr(
		  { src:buttonImage, alt:buttonText, title:buttonText })));
	  input[isRTL ? "before" : "after"](inst.trigger);
	  inst.trigger.click(function() {
		if ($.datepicker._datepickerShowing && $.datepicker._lastInput === input[0]) {
		  $.datepicker._hideDatepicker();
		} else if ($.datepicker._datepickerShowing && $.datepicker._lastInput !== input[0]) {
		  $.datepicker._hideDatepicker();
		  $.datepicker._showDatepicker(input[0]);
		} else {
		  $.datepicker._showDatepicker(input[0]);
		}
		return false;
	  });
	}
  },

  /* Apply the maximum length for the date format. */
  _autoSize: function(inst) {
	if (this._get(inst, "autoSize") && !inst.inline) {
	  var findMax, max, maxI, i,
		date = new Date(2009, 12 - 1, 20), // Ensure double digits
		dateFormat = this._get(inst, "dateFormat");

	  if (dateFormat.match(/[DM]/)) {
		findMax = function(names) {
		  max = 0;
		  maxI = 0;
		  for (i = 0; i < names.length; i++) {
			if (names[i].length > max) {
			  max = names[i].length;
			  maxI = i;
			}
		  }
		  return maxI;
		};
		date.setMonth(findMax(this._get(inst, (dateFormat.match(/MM/) ?
		  "monthNames" : "monthNamesShort"))));
		date.setDate(findMax(this._get(inst, (dateFormat.match(/DD/) ?
		  "dayNames" : "dayNamesShort"))) + 20 - date.getDay());
	  }
	  inst.input.attr("size", this._formatDate(inst, date).length);
	}
  },

  /* Attach an inline date picker to a div. */
  _inlineDatepicker: function(target, inst) {
	var divSpan = $(target);
	if (divSpan.hasClass(this.markerClassName)) {
	  return;
	}
	divSpan.addClass(this.markerClassName).append(inst.dpDiv);
	$.data(target, PROP_NAME, inst);
	this._setDate(inst, this._getDefaultDate(inst), true);
	this._updateDatepicker(inst);
	this._updateAlternate(inst);
	//If disabled option is true, disable the datepicker before showing it (see ticket #5665)
	if( inst.settings.disabled ) {
	  this._disableDatepicker( target );
	}
	// Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
	// http://bugs.jqueryui.com/ticket/7552 - A Datepicker created on a detached div has zero height
	inst.dpDiv.css( "display", "block" );
  },

  /* Pop-up the date picker in a "dialog" box.
   * @param  input element - ignored
   * @param  date	string or Date - the initial date to display
   * @param  onSelect  function - the function to call when a date is selected
   * @param  settings  object - update the dialog date picker instance's settings (anonymous object)
   * @param  pos int[2] - coordinates for the dialog's position within the screen or
   *					event - with x/y coordinates or
   *					leave empty for default (screen centre)
   * @return the manager object
   */
  _dialogDatepicker: function(input, date, onSelect, settings, pos) {
	var id, browserWidth, browserHeight, scrollX, scrollY,
	  inst = this._dialogInst; // internal instance

	if (!inst) {
	  this.uuid += 1;
	  id = "dp" + this.uuid;
	  this._dialogInput = $("<input type='text' id='" + id +
		"' style='position: absolute; top: -100px; width: 0px;'/>");
	  this._dialogInput.keydown(this._doKeyDown);
	  $("body").append(this._dialogInput);
	  inst = this._dialogInst = this._newInst(this._dialogInput, false);
	  inst.settings = {};
	  $.data(this._dialogInput[0], PROP_NAME, inst);
	}
	extendRemove(inst.settings, settings || {});
	date = (date && date.constructor === Date ? this._formatDate(inst, date) : date);
	this._dialogInput.val(date);

	this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null);
	if (!this._pos) {
	  browserWidth = document.documentElement.clientWidth;
	  browserHeight = document.documentElement.clientHeight;
	  scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
	  scrollY = document.documentElement.scrollTop || document.body.scrollTop;
	  this._pos = // should use actual width/height below
		[(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY];
	}

	// move input on screen for focus, but hidden behind dialog
	this._dialogInput.css("left", (this._pos[0] + 20) + "px").css("top", this._pos[1] + "px");
	inst.settings.onSelect = onSelect;
	this._inDialog = true;
	this.dpDiv.addClass(this._dialogClass);
	this._showDatepicker(this._dialogInput[0]);
	if ($.blockUI) {
	  $.blockUI(this.dpDiv);
	}
	$.data(this._dialogInput[0], PROP_NAME, inst);
	return this;
  },

  /* Detach a datepicker from its control.
   * @param  target	element - the target input field or division or span
   */
  _destroyDatepicker: function(target) {
	var nodeName,
	  $target = $(target),
	  inst = $.data(target, PROP_NAME);

	if (!$target.hasClass(this.markerClassName)) {
	  return;
	}

	nodeName = target.nodeName.toLowerCase();
	$.removeData(target, PROP_NAME);
	if (nodeName === "input") {
	  inst.append.remove();
	  inst.trigger.remove();
	  $target.removeClass(this.markerClassName).
		unbind("focus", this._showDatepicker).
		unbind("keydown", this._doKeyDown).
		unbind("keypress", this._doKeyPress).
		unbind("keyup", this._doKeyUp);
	} else if (nodeName === "div" || nodeName === "span") {
	  $target.removeClass(this.markerClassName).empty();
	}
  },

  /* Enable the date picker to a jQuery selection.
   * @param  target	element - the target input field or division or span
   */
  _enableDatepicker: function(target) {
	var nodeName, inline,
	  $target = $(target),
	  inst = $.data(target, PROP_NAME);

	if (!$target.hasClass(this.markerClassName)) {
	  return;
	}

	nodeName = target.nodeName.toLowerCase();
	if (nodeName === "input") {
	  target.disabled = false;
	  inst.trigger.filter("button").
		each(function() { this.disabled = false; }).end().
		filter("img").css({opacity: "1.0", cursor: ""});
	} else if (nodeName === "div" || nodeName === "span") {
	  inline = $target.children("." + this._inlineClass);
	  inline.children().removeClass("ui-state-disabled");
	  inline.find("select.ui-datepicker-month, select.ui-datepicker-year").
		prop("disabled", false);
	}
	this._disabledInputs = $.map(this._disabledInputs,
	  function(value) { return (value === target ? null : value); }); // delete entry
  },

  /* Disable the date picker to a jQuery selection.
   * @param  target	element - the target input field or division or span
   */
  _disableDatepicker: function(target) {
	var nodeName, inline,
	  $target = $(target),
	  inst = $.data(target, PROP_NAME);

	if (!$target.hasClass(this.markerClassName)) {
	  return;
	}

	nodeName = target.nodeName.toLowerCase();
	if (nodeName === "input") {
	  target.disabled = true;
	  inst.trigger.filter("button").
		each(function() { this.disabled = true; }).end().
		filter("img").css({opacity: "0.5", cursor: "default"});
	} else if (nodeName === "div" || nodeName === "span") {
	  inline = $target.children("." + this._inlineClass);
	  inline.children().addClass("ui-state-disabled");
	  inline.find("select.ui-datepicker-month, select.ui-datepicker-year").
		prop("disabled", true);
	}
	this._disabledInputs = $.map(this._disabledInputs,
	  function(value) { return (value === target ? null : value); }); // delete entry
	this._disabledInputs[this._disabledInputs.length] = target;
  },

  /* Is the first field in a jQuery collection disabled as a datepicker?
   * @param  target	element - the target input field or division or span
   * @return boolean - true if disabled, false if enabled
   */
  _isDisabledDatepicker: function(target) {
	if (!target) {
	  return false;
	}
	for (var i = 0; i < this._disabledInputs.length; i++) {
	  if (this._disabledInputs[i] === target) {
		return true;
	  }
	}
	return false;
  },

  /* Retrieve the instance data for the target control.
   * @param  target  element - the target input field or division or span
   * @return  object - the associated instance data
   * @throws  error if a jQuery problem getting data
   */
  _getInst: function(target) {
	try {
	  return $.data(target, PROP_NAME);
	}
	catch (err) {
	  throw "Missing instance data for this datepicker";
	}
  },

  /* Update or retrieve the settings for a date picker attached to an input field or division.
   * @param  target  element - the target input field or division or span
   * @param  name	object - the new settings to update or
   *				string - the name of the setting to change or retrieve,
   *				when retrieving also "all" for all instance settings or
   *				"defaults" for all global defaults
   * @param  value   any - the new value for the setting
   *				(omit if above is an object or to retrieve a value)
   */
  _optionDatepicker: function(target, name, value) {
	var settings, date, minDate, maxDate,
	  inst = this._getInst(target);

	if (arguments.length === 2 && typeof name === "string") {
	  return (name === "defaults" ? $.extend({}, $.datepicker._defaults) :
		(inst ? (name === "all" ? $.extend({}, inst.settings) :
		this._get(inst, name)) : null));
	}

	settings = name || {};
	if (typeof name === "string") {
	  settings = {};
	  settings[name] = value;
	}

	if (inst) {
	  if (this._curInst === inst) {
		this._hideDatepicker();
	  }

	  date = this._getDateDatepicker(target, true);
	  minDate = this._getMinMaxDate(inst, "min");
	  maxDate = this._getMinMaxDate(inst, "max");
	  extendRemove(inst.settings, settings);
	  // reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
	  if (minDate !== null && settings.dateFormat !== undefined && settings.minDate === undefined) {
		inst.settings.minDate = this._formatDate(inst, minDate);
	  }
	  if (maxDate !== null && settings.dateFormat !== undefined && settings.maxDate === undefined) {
		inst.settings.maxDate = this._formatDate(inst, maxDate);
	  }
	  if ( "disabled" in settings ) {
		if ( settings.disabled ) {
		  this._disableDatepicker(target);
		} else {
		  this._enableDatepicker(target);
		}
	  }
	  this._attachments($(target), inst);
	  this._autoSize(inst);
	  this._setDate(inst, date);
	  this._updateAlternate(inst);
	  this._updateDatepicker(inst);
	}
  },

  // change method deprecated
  _changeDatepicker: function(target, name, value) {
	this._optionDatepicker(target, name, value);
  },

  /* Redraw the date picker attached to an input field or division.
   * @param  target  element - the target input field or division or span
   */
  _refreshDatepicker: function(target) {
	var inst = this._getInst(target);
	if (inst) {
	  this._updateDatepicker(inst);
	}
  },

  /* Set the dates for a jQuery selection.
   * @param  target element - the target input field or division or span
   * @param  date	Date - the new date
   */
  _setDateDatepicker: function(target, date) {
	var inst = this._getInst(target);
	if (inst) {
	  this._setDate(inst, date);
	  this._updateDatepicker(inst);
	  this._updateAlternate(inst);
	}
  },

  /* Get the date(s) for the first entry in a jQuery selection.
   * @param  target element - the target input field or division or span
   * @param  noDefault boolean - true if no default date is to be used
   * @return Date - the current date
   */
  _getDateDatepicker: function(target, noDefault) {
	var inst = this._getInst(target);
	if (inst && !inst.inline) {
	  this._setDateFromField(inst, noDefault);
	}
	return (inst ? this._getDate(inst) : null);
  },

  /* Handle keystrokes. */
  _doKeyDown: function(event) {
	var onSelect, dateStr, sel,
	  inst = $.datepicker._getInst(event.target),
	  handled = true,
	  isRTL = inst.dpDiv.is(".ui-datepicker-rtl");

	inst._keyEvent = true;
	if ($.datepicker._datepickerShowing) {
	  switch (event.keyCode) {
		case 9: $.datepicker._hideDatepicker();
			handled = false;
			break; // hide on tab out
		case 13: sel = $("td." + $.datepicker._dayOverClass + ":not(." +
				  $.datepicker._currentClass + ")", inst.dpDiv);
			if (sel[0]) {
			  $.datepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]);
			}

			onSelect = $.datepicker._get(inst, "onSelect");
			if (onSelect) {
			  dateStr = $.datepicker._formatDate(inst);

			  // trigger custom callback
			  onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);
			} else {
			  $.datepicker._hideDatepicker();
			}

			return false; // don't submit the form
		case 27: $.datepicker._hideDatepicker();
			break; // hide on escape
		case 33: $.datepicker._adjustDate(event.target, (event.ctrlKey ?
			  -$.datepicker._get(inst, "stepBigMonths") :
			  -$.datepicker._get(inst, "stepMonths")), "M");
			break; // previous month/year on page up/+ ctrl
		case 34: $.datepicker._adjustDate(event.target, (event.ctrlKey ?
			  +$.datepicker._get(inst, "stepBigMonths") :
			  +$.datepicker._get(inst, "stepMonths")), "M");
			break; // next month/year on page down/+ ctrl
		case 35: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._clearDate(event.target);
			}
			handled = event.ctrlKey || event.metaKey;
			break; // clear on ctrl or command +end
		case 36: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._gotoToday(event.target);
			}
			handled = event.ctrlKey || event.metaKey;
			break; // current on ctrl or command +home
		case 37: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._adjustDate(event.target, (isRTL ? +1 : -1), "D");
			}
			handled = event.ctrlKey || event.metaKey;
			// -1 day on ctrl or command +left
			if (event.originalEvent.altKey) {
			  $.datepicker._adjustDate(event.target, (event.ctrlKey ?
				-$.datepicker._get(inst, "stepBigMonths") :
				-$.datepicker._get(inst, "stepMonths")), "M");
			}
			// next month/year on alt +left on Mac
			break;
		case 38: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._adjustDate(event.target, -7, "D");
			}
			handled = event.ctrlKey || event.metaKey;
			break; // -1 week on ctrl or command +up
		case 39: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._adjustDate(event.target, (isRTL ? -1 : +1), "D");
			}
			handled = event.ctrlKey || event.metaKey;
			// +1 day on ctrl or command +right
			if (event.originalEvent.altKey) {
			  $.datepicker._adjustDate(event.target, (event.ctrlKey ?
				+$.datepicker._get(inst, "stepBigMonths") :
				+$.datepicker._get(inst, "stepMonths")), "M");
			}
			// next month/year on alt +right
			break;
		case 40: if (event.ctrlKey || event.metaKey) {
			  $.datepicker._adjustDate(event.target, +7, "D");
			}
			handled = event.ctrlKey || event.metaKey;
			break; // +1 week on ctrl or command +down
		default: handled = false;
	  }
	} else if (event.keyCode === 36 && event.ctrlKey) { // display the date picker on ctrl+home
	  $.datepicker._showDatepicker(this);
	} else {
	  handled = false;
	}

	if (handled) {
	  event.preventDefault();
	  event.stopPropagation();
	}
  },

  /* Filter entered characters - based on date format. */
  _doKeyPress: function(event) {
	var chars, chr,
	  inst = $.datepicker._getInst(event.target);

	if ($.datepicker._get(inst, "constrainInput")) {
	  chars = $.datepicker._possibleChars($.datepicker._get(inst, "dateFormat"));
	  chr = String.fromCharCode(event.charCode == null ? event.keyCode : event.charCode);
	  return event.ctrlKey || event.metaKey || (chr < " " || !chars || chars.indexOf(chr) > -1);
	}
  },

  /* Synchronise manual entry and field/alternate field. */
  _doKeyUp: function(event) {
	var date,
	  inst = $.datepicker._getInst(event.target);

	if (inst.input.val() !== inst.lastVal) {
	  try {
		date = $.datepicker.parseDate($.datepicker._get(inst, "dateFormat"),
		  (inst.input ? inst.input.val() : null),
		  $.datepicker._getFormatConfig(inst));

		if (date) { // only if valid
		  $.datepicker._setDateFromField(inst);
		  $.datepicker._updateAlternate(inst);
		  $.datepicker._updateDatepicker(inst);
		}
	  }
	  catch (err) {
	  }
	}
	return true;
  },

  /* Pop-up the date picker for a given input field.
   * If false returned from beforeShow event handler do not show.
   * @param  input  element - the input field attached to the date picker or
   *					event - if triggered by focus
   */
  _showDatepicker: function(input) {
	input = input.target || input;
	if (input.nodeName.toLowerCase() !== "input") { // find from button/image trigger
	  input = $("input", input.parentNode)[0];
	}

	if ($.datepicker._isDisabledDatepicker(input) || $.datepicker._lastInput === input) { // already here
	  return;
	}

	var inst, beforeShow, beforeShowSettings, isFixed,
	  offset, showAnim, duration;

	inst = $.datepicker._getInst(input);
	if ($.datepicker._curInst && $.datepicker._curInst !== inst) {
	  $.datepicker._curInst.dpDiv.stop(true, true);
	  if ( inst && $.datepicker._datepickerShowing ) {
		$.datepicker._hideDatepicker( $.datepicker._curInst.input[0] );
	  }
	}

	beforeShow = $.datepicker._get(inst, "beforeShow");
	beforeShowSettings = beforeShow ? beforeShow.apply(input, [input, inst]) : {};
	if(beforeShowSettings === false){
	  return;
	}
	extendRemove(inst.settings, beforeShowSettings);

	inst.lastVal = null;
	$.datepicker._lastInput = input;
	$.datepicker._setDateFromField(inst);

	if ($.datepicker._inDialog) { // hide cursor
	  input.value = "";
	}
	if (!$.datepicker._pos) { // position below input
	  $.datepicker._pos = $.datepicker._findPos(input);
	  $.datepicker._pos[1] += input.offsetHeight; // add the height
	}

	isFixed = false;
	$(input).parents().each(function() {
	  isFixed |= $(this).css("position") === "fixed";
	  return !isFixed;
	});

	offset = {left: $.datepicker._pos[0], top: $.datepicker._pos[1]};
	$.datepicker._pos = null;
	//to avoid flashes on Firefox
	inst.dpDiv.empty();
	// determine sizing offscreen
	inst.dpDiv.css({position: "absolute", display: "block", top: "-1000px"});
	$.datepicker._updateDatepicker(inst);
	// fix width for dynamic number of date pickers
	// and adjust position before showing
	offset = $.datepicker._checkOffset(inst, offset, isFixed);
	inst.dpDiv.css({position: ($.datepicker._inDialog && $.blockUI ?
	  "static" : (isFixed ? "fixed" : "absolute")), display: "none",
	  left: offset.left + "px", top: offset.top + "px"});

	if (!inst.inline) {
	  showAnim = $.datepicker._get(inst, "showAnim");
	  duration = $.datepicker._get(inst, "duration");
	  inst.dpDiv.zIndex($(input).zIndex()+1);
	  $.datepicker._datepickerShowing = true;

	  if ( $.effects && $.effects.effect[ showAnim ] ) {
		inst.dpDiv.show(showAnim, $.datepicker._get(inst, "showOptions"), duration);
	  } else {
		inst.dpDiv[showAnim || "show"](showAnim ? duration : null);
	  }

	  if ( $.datepicker._shouldFocusInput( inst ) ) {
		inst.input.focus();
	  }

	  $.datepicker._curInst = inst;
	}
  },

  /* Generate the date picker content. */
  _updateDatepicker: function(inst) {
	this.maxRows = 4; //Reset the max number of rows being displayed (see #7043)
	instActive = inst; // for delegate hover events
	inst.dpDiv.empty().append(this._generateHTML(inst));
	this._attachHandlers(inst);
	inst.dpDiv.find("." + this._dayOverClass + " a").mouseover();

	var origyearshtml,
	  numMonths = this._getNumberOfMonths(inst),
	  cols = numMonths[1],
	  width = 17;

	inst.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
	if (cols > 1) {
	  inst.dpDiv.addClass("ui-datepicker-multi-" + cols).css("width", (width * cols) + "em");
	}
	inst.dpDiv[(numMonths[0] !== 1 || numMonths[1] !== 1 ? "add" : "remove") +
	  "Class"]("ui-datepicker-multi");
	inst.dpDiv[(this._get(inst, "isRTL") ? "add" : "remove") +
	  "Class"]("ui-datepicker-rtl");

	if (inst === $.datepicker._curInst && $.datepicker._datepickerShowing && $.datepicker._shouldFocusInput( inst ) ) {
	  inst.input.focus();
	}

	// deffered render of the years select (to avoid flashes on Firefox)
	if( inst.yearshtml ){
	  origyearshtml = inst.yearshtml;
	  setTimeout(function(){
		//assure that inst.yearshtml didn't change.
		if( origyearshtml === inst.yearshtml && inst.yearshtml ){
		  inst.dpDiv.find("select.ui-datepicker-year:first").replaceWith(inst.yearshtml);
		}
		origyearshtml = inst.yearshtml = null;
	  }, 0);
	}
  },

  // #6694 - don't focus the input if it's already focused
  // this breaks the change event in IE
  // Support: IE and jQuery <1.9
  _shouldFocusInput: function( inst ) {
	return inst.input && inst.input.is( ":visible" ) && !inst.input.is( ":disabled" ) && !inst.input.is( ":focus" );
  },

  /* Check positioning to remain on screen. */
  _checkOffset: function(inst, offset, isFixed) {
	var dpWidth = inst.dpDiv.outerWidth(),
	  dpHeight = inst.dpDiv.outerHeight(),
	  inputWidth = inst.input ? inst.input.outerWidth() : 0,
	  inputHeight = inst.input ? inst.input.outerHeight() : 0,
	  viewWidth = document.documentElement.clientWidth + (isFixed ? 0 : $(document).scrollLeft()),
	  viewHeight = document.documentElement.clientHeight + (isFixed ? 0 : $(document).scrollTop());

	offset.left -= (this._get(inst, "isRTL") ? (dpWidth - inputWidth) : 0);
	offset.left -= (isFixed && offset.left === inst.input.offset().left) ? $(document).scrollLeft() : 0;
	offset.top -= (isFixed && offset.top === (inst.input.offset().top + inputHeight)) ? $(document).scrollTop() : 0;

	// now check if datepicker is showing outside window viewport - move to a better place if so.
	offset.left -= Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
	  Math.abs(offset.left + dpWidth - viewWidth) : 0);
	offset.top -= Math.min(offset.top, (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
	  Math.abs(dpHeight + inputHeight) : 0);

	return offset;
  },

  /* Find an object's position on the screen. */
  _findPos: function(obj) {
	var position,
	  inst = this._getInst(obj),
	  isRTL = this._get(inst, "isRTL");

	while (obj && (obj.type === "hidden" || obj.nodeType !== 1 || $.expr.filters.hidden(obj))) {
	  obj = obj[isRTL ? "previousSibling" : "nextSibling"];
	}

	position = $(obj).offset();
	return [position.left, position.top];
  },

  /* Hide the date picker from view.
   * @param  input  element - the input field attached to the date picker
   */
  _hideDatepicker: function(input) {
	var showAnim, duration, postProcess, onClose,
	  inst = this._curInst;

	if (!inst || (input && inst !== $.data(input, PROP_NAME))) {
	  return;
	}

	if (this._datepickerShowing) {
	  showAnim = this._get(inst, "showAnim");
	  duration = this._get(inst, "duration");
	  postProcess = function() {
		$.datepicker._tidyDialog(inst);
	  };

	  // DEPRECATED: after BC for 1.8.x $.effects[ showAnim ] is not needed
	  if ( $.effects && ( $.effects.effect[ showAnim ] || $.effects[ showAnim ] ) ) {
		inst.dpDiv.hide(showAnim, $.datepicker._get(inst, "showOptions"), duration, postProcess);
	  } else {
		inst.dpDiv[(showAnim === "slideDown" ? "slideUp" :
		  (showAnim === "fadeIn" ? "fadeOut" : "hide"))]((showAnim ? duration : null), postProcess);
	  }

	  if (!showAnim) {
		postProcess();
	  }
	  this._datepickerShowing = false;

	  onClose = this._get(inst, "onClose");
	  if (onClose) {
		onClose.apply((inst.input ? inst.input[0] : null), [(inst.input ? inst.input.val() : ""), inst]);
	  }

	  this._lastInput = null;
	  if (this._inDialog) {
		this._dialogInput.css({ position: "absolute", left: "0", top: "-100px" });
		if ($.blockUI) {
		  $.unblockUI();
		  $("body").append(this.dpDiv);
		}
	  }
	  this._inDialog = false;
	}
  },

  /* Tidy up after a dialog display. */
  _tidyDialog: function(inst) {
	inst.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar");
  },

  /* Close date picker if clicked elsewhere. */
  _checkExternalClick: function(event) {
	if (!$.datepicker._curInst) {
	  return;
	}

	var $target = $(event.target),
	  inst = $.datepicker._getInst($target[0]);

	if ( ( ( $target[0].id !== $.datepicker._mainDivId &&
		$target.parents("#" + $.datepicker._mainDivId).length === 0 &&
		!$target.hasClass($.datepicker.markerClassName) &&
		!$target.closest("." + $.datepicker._triggerClass).length &&
		$.datepicker._datepickerShowing && !($.datepicker._inDialog && $.blockUI) ) ) ||
	  ( $target.hasClass($.datepicker.markerClassName) && $.datepicker._curInst !== inst ) ) {
		$.datepicker._hideDatepicker();
	}
  },

  /* Adjust one of the date sub-fields. */
  _adjustDate: function(id, offset, period) {
	var target = $(id),
	  inst = this._getInst(target[0]);

	if (this._isDisabledDatepicker(target[0])) {
	  return;
	}
	this._adjustInstDate(inst, offset +
	  (period === "M" ? this._get(inst, "showCurrentAtPos") : 0), // undo positioning
	  period);
	this._updateDatepicker(inst);
  },

  /* Action for current link. */
  _gotoToday: function(id) {
	var date,
	  target = $(id),
	  inst = this._getInst(target[0]);

	if (this._get(inst, "gotoCurrent") && inst.currentDay) {
	  inst.selectedDay = inst.currentDay;
	  inst.drawMonth = inst.selectedMonth = inst.currentMonth;
	  inst.drawYear = inst.selectedYear = inst.currentYear;
	} else {
	  date = new Date();
	  inst.selectedDay = date.getDate();
	  inst.drawMonth = inst.selectedMonth = date.getMonth();
	  inst.drawYear = inst.selectedYear = date.getFullYear();
	}
	this._notifyChange(inst);
	this._adjustDate(target);
  },

  /* Action for selecting a new month/year. */
  _selectMonthYear: function(id, select, period) {
	var target = $(id),
	  inst = this._getInst(target[0]);

	inst["selected" + (period === "M" ? "Month" : "Year")] =
	inst["draw" + (period === "M" ? "Month" : "Year")] =
	  parseInt(select.options[select.selectedIndex].value,10);

	this._notifyChange(inst);
	this._adjustDate(target);
  },

  /* Action for selecting a day. */
  _selectDay: function(id, month, year, td) {
	var inst,
	  target = $(id);

	if ($(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) {
	  return;
	}

	inst = this._getInst(target[0]);
	inst.selectedDay = inst.currentDay = $("a", td).html();
	inst.selectedMonth = inst.currentMonth = month;
	inst.selectedYear = inst.currentYear = year;
	this._selectDate(id, this._formatDate(inst,
	  inst.currentDay, inst.currentMonth, inst.currentYear));
  },

  /* Erase the input field and hide the date picker. */
  _clearDate: function(id) {
	var target = $(id);
	this._selectDate(target, "");
  },

  /* Update the input field with the selected date. */
  _selectDate: function(id, dateStr) {
	var onSelect,
	  target = $(id),
	  inst = this._getInst(target[0]);

	dateStr = (dateStr != null ? dateStr : this._formatDate(inst));
	if (inst.input) {
	  inst.input.val(dateStr);
	}
	this._updateAlternate(inst);

	onSelect = this._get(inst, "onSelect");
	if (onSelect) {
	  onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
	} else if (inst.input) {
	  inst.input.trigger("change"); // fire the change event
	}

	if (inst.inline){
	  this._updateDatepicker(inst);
	} else {
	  this._hideDatepicker();
	  this._lastInput = inst.input[0];
	  if (typeof(inst.input[0]) !== "object") {
		inst.input.focus(); // restore focus
	  }
	  this._lastInput = null;
	}
  },

  /* Update any alternate field to synchronise with the main field. */
  _updateAlternate: function(inst) {
	var altFormat, date, dateStr,
	  altField = this._get(inst, "altField");

	if (altField) { // update alternate field too
	  altFormat = this._get(inst, "altFormat") || this._get(inst, "dateFormat");
	  date = this._getDate(inst);
	  dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst));
	  $(altField).each(function() { $(this).val(dateStr); });
	}
  },

  /* Set as beforeShowDay function to prevent selection of weekends.
   * @param  date  Date - the date to customise
   * @return [boolean, string] - is this date selectable?, what is its CSS class?
   */
  noWeekends: function(date) {
	var day = date.getDay();
	return [(day > 0 && day < 6), ""];
  },

  /* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
   * @param  date  Date - the date to get the week for
   * @return  number - the number of the week within the year that contains this date
   */
  iso8601Week: function(date) {
	var time,
	  checkDate = new Date(date.getTime());

	// Find Thursday of this week starting on Monday
	checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));

	time = checkDate.getTime();
	checkDate.setMonth(0); // Compare with Jan 1
	checkDate.setDate(1);
	return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
  },

  /* Parse a string value into a date object.
   * See formatDate below for the possible formats.
   *
   * @param  format string - the expected format of the date
   * @param  value string - the date in the above format
   * @param  settings Object - attributes include:
   *					shortYearCutoff  number - the cutoff year for determining the century (optional)
   *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
   *					dayNames		string[7] - names of the days from Sunday (optional)
   *					monthNamesShort string[12] - abbreviated names of the months (optional)
   *					monthNames		string[12] - names of the months (optional)
   * @return  Date - the extracted date value or null if value is blank
   */
  parseDate: function (format, value, settings) {
	if (format == null || value == null) {
	  throw "Invalid arguments";
	}

	value = (typeof value === "object" ? value.toString() : value + "");
	if (value === "") {
	  return null;
	}

	var iFormat, dim, extra,
	  iValue = 0,
	  shortYearCutoffTemp = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff,
	  shortYearCutoff = (typeof shortYearCutoffTemp !== "string" ? shortYearCutoffTemp :
		new Date().getFullYear() % 100 + parseInt(shortYearCutoffTemp, 10)),
	  dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort,
	  dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames,
	  monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort,
	  monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames,
	  year = -1,
	  month = -1,
	  day = -1,
	  doy = -1,
	  literal = false,
	  date,
	  // Check whether a format character is doubled
	  lookAhead = function(match) {
		var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) === match);
		if (matches) {
		  iFormat++;
		}
		return matches;
	  },
	  // Extract a number from the string value
	  getNumber = function(match) {
		var isDoubled = lookAhead(match),
		  size = (match === "@" ? 14 : (match === "!" ? 20 :
		  (match === "y" && isDoubled ? 4 : (match === "o" ? 3 : 2)))),
		  digits = new RegExp("^\\d{1," + size + "}"),
		  num = value.substring(iValue).match(digits);
		if (!num) {
		  throw "Missing number at position " + iValue;
		}
		iValue += num[0].length;
		return parseInt(num[0], 10);
	  },
	  // Extract a name from the string value and convert to an index
	  getName = function(match, shortNames, longNames) {
		var index = -1,
		  names = $.map(lookAhead(match) ? longNames : shortNames, function (v, k) {
			return [ [k, v] ];
		  }).sort(function (a, b) {
			return -(a[1].length - b[1].length);
		  });

		$.each(names, function (i, pair) {
		  var name = pair[1];
		  if (value.substr(iValue, name.length).toLowerCase() === name.toLowerCase()) {
			index = pair[0];
			iValue += name.length;
			return false;
		  }
		});
		if (index !== -1) {
		  return index + 1;
		} else {
		  throw "Unknown name at position " + iValue;
		}
	  },
	  // Confirm that a literal character matches the string value
	  checkLiteral = function() {
		if (value.charAt(iValue) !== format.charAt(iFormat)) {
		  throw "Unexpected literal at position " + iValue;
		}
		iValue++;
	  };

	for (iFormat = 0; iFormat < format.length; iFormat++) {
	  if (literal) {
		if (format.charAt(iFormat) === "'" && !lookAhead("'")) {
		  literal = false;
		} else {
		  checkLiteral();
		}
	  } else {
		switch (format.charAt(iFormat)) {
		  case "d":
			day = getNumber("d");
			break;
		  case "D":
			getName("D", dayNamesShort, dayNames);
			break;
		  case "o":
			doy = getNumber("o");
			break;
		  case "m":
			month = getNumber("m");
			break;
		  case "M":
			month = getName("M", monthNamesShort, monthNames);
			break;
		  case "y":
			year = getNumber("y");
			break;
		  case "@":
			date = new Date(getNumber("@"));
			year = date.getFullYear();
			month = date.getMonth() + 1;
			day = date.getDate();
			break;
		  case "!":
			date = new Date((getNumber("!") - this._ticksTo1970) / 10000);
			year = date.getFullYear();
			month = date.getMonth() + 1;
			day = date.getDate();
			break;
		  case "'":
			if (lookAhead("'")){
			  checkLiteral();
			} else {
			  literal = true;
			}
			break;
		  default:
			checkLiteral();
		}
	  }
	}

	if (iValue < value.length){
	  extra = value.substr(iValue);
	  if (!/^\s+/.test(extra)) {
		throw "Extra/unparsed characters found in date: " + extra;
	  }
	}

	if (year === -1) {
	  year = new Date().getFullYear();
	} else if (year < 100) {
	  year += new Date().getFullYear() - new Date().getFullYear() % 100 +
		(year <= shortYearCutoff ? 0 : -100);
	}

	if (doy > -1) {
	  month = 1;
	  day = doy;
	  do {
		dim = this._getDaysInMonth(year, month - 1);
		if (day <= dim) {
		  break;
		}
		month++;
		day -= dim;
	  } while (true);
	}

	date = this._daylightSavingAdjust(new Date(year, month - 1, day));
	if (date.getFullYear() !== year || date.getMonth() + 1 !== month || date.getDate() !== day) {
	  throw "Invalid date"; // E.g. 31/02/00
	}
	return date;
  },

  /* Standard date formats. */
  ATOM: "yy-mm-dd", // RFC 3339 (ISO 8601)
  COOKIE: "D, dd M yy",
  ISO_8601: "yy-mm-dd",
  RFC_822: "D, d M y",
  RFC_850: "DD, dd-M-y",
  RFC_1036: "D, d M y",
  RFC_1123: "D, d M yy",
  RFC_2822: "D, d M yy",
  RSS: "D, d M y", // RFC 822
  TICKS: "!",
  TIMESTAMP: "@",
  W3C: "yy-mm-dd", // ISO 8601

  _ticksTo1970: (((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) +
	Math.floor(1970 / 400)) * 24 * 60 * 60 * 10000000),

  /* Format a date object into a string value.
   * The format can be combinations of the following:
   * d  - day of month (no leading zero)
   * dd - day of month (two digit)
   * o  - day of year (no leading zeros)
   * oo - day of year (three digit)
   * D  - day name short
   * DD - day name long
   * m  - month of year (no leading zero)
   * mm - month of year (two digit)
   * M  - month name short
   * MM - month name long
   * y  - year (two digit)
   * yy - year (four digit)
   * @ - Unix timestamp (ms since 01/01/1970)
   * ! - Windows ticks (100ns since 01/01/0001)
   * "..." - literal text
   * '' - single quote
   *
   * @param  format string - the desired format of the date
   * @param  date Date - the date value to format
   * @param  settings Object - attributes include:
   *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
   *					dayNames		string[7] - names of the days from Sunday (optional)
   *					monthNamesShort string[12] - abbreviated names of the months (optional)
   *					monthNames		string[12] - names of the months (optional)
   * @return  string - the date in the above format
   */
  formatDate: function (format, date, settings) {
	if (!date) {
	  return "";
	}

	var iFormat,
	  dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort,
	  dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames,
	  monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort,
	  monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames,
	  // Check whether a format character is doubled
	  lookAhead = function(match) {
		var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) === match);
		if (matches) {
		  iFormat++;
		}
		return matches;
	  },
	  // Format a number, with leading zero if necessary
	  formatNumber = function(match, value, len) {
		var num = "" + value;
		if (lookAhead(match)) {
		  while (num.length < len) {
			num = "0" + num;
		  }
		}
		return num;
	  },
	  // Format a name, short or long as requested
	  formatName = function(match, value, shortNames, longNames) {
		return (lookAhead(match) ? longNames[value] : shortNames[value]);
	  },
	  output = "",
	  literal = false;

	if (date) {
	  for (iFormat = 0; iFormat < format.length; iFormat++) {
		if (literal) {
		  if (format.charAt(iFormat) === "'" && !lookAhead("'")) {
			literal = false;
		  } else {
			output += format.charAt(iFormat);
		  }
		} else {
		  switch (format.charAt(iFormat)) {
			case "d":
			  output += formatNumber("d", date.getDate(), 2);
			  break;
			case "D":
			  output += formatName("D", date.getDay(), dayNamesShort, dayNames);
			  break;
			case "o":
			  output += formatNumber("o",
				Math.round((new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - new Date(date.getFullYear(), 0, 0).getTime()) / 86400000), 3);
			  break;
			case "m":
			  output += formatNumber("m", date.getMonth() + 1, 2);
			  break;
			case "M":
			  output += formatName("M", date.getMonth(), monthNamesShort, monthNames);
			  break;
			case "y":
			  output += (lookAhead("y") ? date.getFullYear() :
				(date.getYear() % 100 < 10 ? "0" : "") + date.getYear() % 100);
			  break;
			case "@":
			  output += date.getTime();
			  break;
			case "!":
			  output += date.getTime() * 10000 + this._ticksTo1970;
			  break;
			case "'":
			  if (lookAhead("'")) {
				output += "'";
			  } else {
				literal = true;
			  }
			  break;
			default:
			  output += format.charAt(iFormat);
		  }
		}
	  }
	}
	return output;
  },

  /* Extract all possible characters from the date format. */
  _possibleChars: function (format) {
	var iFormat,
	  chars = "",
	  literal = false,
	  // Check whether a format character is doubled
	  lookAhead = function(match) {
		var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) === match);
		if (matches) {
		  iFormat++;
		}
		return matches;
	  };

	for (iFormat = 0; iFormat < format.length; iFormat++) {
	  if (literal) {
		if (format.charAt(iFormat) === "'" && !lookAhead("'")) {
		  literal = false;
		} else {
		  chars += format.charAt(iFormat);
		}
	  } else {
		switch (format.charAt(iFormat)) {
		  case "d": case "m": case "y": case "@":
			chars += "0123456789";
			break;
		  case "D": case "M":
			return null; // Accept anything
		  case "'":
			if (lookAhead("'")) {
			  chars += "'";
			} else {
			  literal = true;
			}
			break;
		  default:
			chars += format.charAt(iFormat);
		}
	  }
	}
	return chars;
  },

  /* Get a setting value, defaulting if necessary. */
  _get: function(inst, name) {
	return inst.settings[name] !== undefined ?
	  inst.settings[name] : this._defaults[name];
  },

  /* Parse existing date and initialise date picker. */
  _setDateFromField: function(inst, noDefault) {
	if (inst.input.val() === inst.lastVal) {
	  return;
	}

	var dateFormat = this._get(inst, "dateFormat"),
	  dates = inst.lastVal = inst.input ? inst.input.val() : null,
	  defaultDate = this._getDefaultDate(inst),
	  date = defaultDate,
	  settings = this._getFormatConfig(inst);

	try {
	  date = this.parseDate(dateFormat, dates, settings) || defaultDate;
	} catch (event) {
	  dates = (noDefault ? "" : dates);
	}
	inst.selectedDay = date.getDate();
	inst.drawMonth = inst.selectedMonth = date.getMonth();
	inst.drawYear = inst.selectedYear = date.getFullYear();
	inst.currentDay = (dates ? date.getDate() : 0);
	inst.currentMonth = (dates ? date.getMonth() : 0);
	inst.currentYear = (dates ? date.getFullYear() : 0);
	this._adjustInstDate(inst);
  },

  /* Retrieve the default date shown on opening. */
  _getDefaultDate: function(inst) {
	return this._restrictMinMax(inst,
	  this._determineDate(inst, this._get(inst, "defaultDate"), new Date()));
  },

  /* A date may be specified as an exact value or a relative one. */
  _determineDate: function(inst, date, defaultDate) {
	var offsetNumeric = function(offset) {
		var date = new Date();
		date.setDate(date.getDate() + offset);
		return date;
	  },
	  offsetString = function(offset) {
		try {
		  return $.datepicker.parseDate($.datepicker._get(inst, "dateFormat"),
			offset, $.datepicker._getFormatConfig(inst));
		}
		catch (e) {
		  // Ignore
		}

		var date = (offset.toLowerCase().match(/^c/) ?
		  $.datepicker._getDate(inst) : null) || new Date(),
		  year = date.getFullYear(),
		  month = date.getMonth(),
		  day = date.getDate(),
		  pattern = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,
		  matches = pattern.exec(offset);

		while (matches) {
		  switch (matches[2] || "d") {
			case "d" : case "D" :
			  day += parseInt(matches[1],10); break;
			case "w" : case "W" :
			  day += parseInt(matches[1],10) * 7; break;
			case "m" : case "M" :
			  month += parseInt(matches[1],10);
			  day = Math.min(day, $.datepicker._getDaysInMonth(year, month));
			  break;
			case "y": case "Y" :
			  year += parseInt(matches[1],10);
			  day = Math.min(day, $.datepicker._getDaysInMonth(year, month));
			  break;
		  }
		  matches = pattern.exec(offset);
		}
		return new Date(year, month, day);
	  },
	  newDate = (date == null || date === "" ? defaultDate : (typeof date === "string" ? offsetString(date) :
		(typeof date === "number" ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : new Date(date.getTime()))));

	newDate = (newDate && newDate.toString() === "Invalid Date" ? defaultDate : newDate);
	if (newDate) {
	  newDate.setHours(0);
	  newDate.setMinutes(0);
	  newDate.setSeconds(0);
	  newDate.setMilliseconds(0);
	}
	return this._daylightSavingAdjust(newDate);
  },

  /* Handle switch to/from daylight saving.
   * Hours may be non-zero on daylight saving cut-over:
   * > 12 when midnight changeover, but then cannot generate
   * midnight datetime, so jump to 1AM, otherwise reset.
   * @param  date  (Date) the date to check
   * @return  (Date) the corrected date
   */
  _daylightSavingAdjust: function(date) {
	if (!date) {
	  return null;
	}
	date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0);
	return date;
  },

  /* Set the date(s) directly. */
  _setDate: function(inst, date, noChange) {
	var clear = !date,
	  origMonth = inst.selectedMonth,
	  origYear = inst.selectedYear,
	  newDate = this._restrictMinMax(inst, this._determineDate(inst, date, new Date()));

	inst.selectedDay = inst.currentDay = newDate.getDate();
	inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth();
	inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear();
	if ((origMonth !== inst.selectedMonth || origYear !== inst.selectedYear) && !noChange) {
	  this._notifyChange(inst);
	}
	this._adjustInstDate(inst);
	if (inst.input) {
	  inst.input.val(clear ? "" : this._formatDate(inst));
	}
  },

  /* Retrieve the date(s) directly. */
  _getDate: function(inst) {
	var startDate = (!inst.currentYear || (inst.input && inst.input.val() === "") ? null :
	  this._daylightSavingAdjust(new Date(
	  inst.currentYear, inst.currentMonth, inst.currentDay)));
	  return startDate;
  },

  /* Attach the onxxx handlers.  These are declared statically so
   * they work with static code transformers like Caja.
   */
  _attachHandlers: function(inst) {
	var stepMonths = this._get(inst, "stepMonths"),
	  id = "#" + inst.id.replace( /\\\\/g, "\\" );
	inst.dpDiv.find("[data-handler]").map(function () {
	  var handler = {
		prev: function () {
		  $.datepicker._adjustDate(id, -stepMonths, "M");
		},
		next: function () {
		  $.datepicker._adjustDate(id, +stepMonths, "M");
		},
		hide: function () {
		  $.datepicker._hideDatepicker();
		},
		today: function () {
		  $.datepicker._gotoToday(id);
		},
		selectDay: function () {
		  $.datepicker._selectDay(id, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this);
		  return false;
		},
		selectMonth: function () {
		  $.datepicker._selectMonthYear(id, this, "M");
		  return false;
		},
		selectYear: function () {
		  $.datepicker._selectMonthYear(id, this, "Y");
		  return false;
		}
	  };
	  $(this).bind(this.getAttribute("data-event"), handler[this.getAttribute("data-handler")]);
	});
  },

  /* Generate the HTML for the current state of the date picker. */
  _generateHTML: function(inst) {
	var maxDraw, prevText, prev, nextText, next, currentText, gotoDate,
	  controls, buttonPanel, firstDay, showWeek, dayNames, dayNamesMin,
	  monthNames, monthNamesShort, beforeShowDay, showOtherMonths,
	  selectOtherMonths, defaultDate, html, dow, row, group, col, selectedDate,
	  cornerClass, calender, thead, day, daysInMonth, leadDays, curRows, numRows,
	  printDate, dRow, tbody, daySettings, otherMonth, unselectable,
	  tempDate = new Date(),
	  today = this._daylightSavingAdjust(
		new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate())), // clear time
	  isRTL = this._get(inst, "isRTL"),
	  showButtonPanel = this._get(inst, "showButtonPanel"),
	  hideIfNoPrevNext = this._get(inst, "hideIfNoPrevNext"),
	  navigationAsDateFormat = this._get(inst, "navigationAsDateFormat"),
	  numMonths = this._getNumberOfMonths(inst),
	  showCurrentAtPos = this._get(inst, "showCurrentAtPos"),
	  stepMonths = this._get(inst, "stepMonths"),
	  isMultiMonth = (numMonths[0] !== 1 || numMonths[1] !== 1),
	  currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) :
		new Date(inst.currentYear, inst.currentMonth, inst.currentDay))),
	  minDate = this._getMinMaxDate(inst, "min"),
	  maxDate = this._getMinMaxDate(inst, "max"),
	  drawMonth = inst.drawMonth - showCurrentAtPos,
	  drawYear = inst.drawYear;

	if (drawMonth < 0) {
	  drawMonth += 12;
	  drawYear--;
	}
	if (maxDate) {
	  maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(),
		maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate()));
	  maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw);
	  while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) {
		drawMonth--;
		if (drawMonth < 0) {
		  drawMonth = 11;
		  drawYear--;
		}
	  }
	}
	inst.drawMonth = drawMonth;
	inst.drawYear = drawYear;

	prevText = this._get(inst, "prevText");
	prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText,
	  this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)),
	  this._getFormatConfig(inst)));

	prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ?
	  "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click'" +
	  " title='" + prevText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w") + "'>" + prevText + "</span></a>" :
	  (hideIfNoPrevNext ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+ prevText +"'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w") + "'>" + prevText + "</span></a>"));

	nextText = this._get(inst, "nextText");
	nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText,
	  this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)),
	  this._getFormatConfig(inst)));

	next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ?
	  "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click'" +
	  " title='" + nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e") + "'>" + nextText + "</span></a>" :
	  (hideIfNoPrevNext ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+ nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e") + "'>" + nextText + "</span></a>"));

	currentText = this._get(inst, "currentText");
	gotoDate = (this._get(inst, "gotoCurrent") && inst.currentDay ? currentDate : today);
	currentText = (!navigationAsDateFormat ? currentText :
	  this.formatDate(currentText, gotoDate, this._getFormatConfig(inst)));

	controls = (!inst.inline ? "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" +
	  this._get(inst, "closeText") + "</button>" : "");

	buttonPanel = (showButtonPanel) ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (isRTL ? controls : "") +
	  (this._isInRange(inst, gotoDate) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'" +
	  ">" + currentText + "</button>" : "") + (isRTL ? "" : controls) + "</div>" : "";

	firstDay = parseInt(this._get(inst, "firstDay"),10);
	firstDay = (isNaN(firstDay) ? 0 : firstDay);

	showWeek = this._get(inst, "showWeek");
	dayNames = this._get(inst, "dayNames");
	dayNamesMin = this._get(inst, "dayNamesMin");
	monthNames = this._get(inst, "monthNames");
	monthNamesShort = this._get(inst, "monthNamesShort");
	beforeShowDay = this._get(inst, "beforeShowDay");
	showOtherMonths = this._get(inst, "showOtherMonths");
	selectOtherMonths = this._get(inst, "selectOtherMonths");
	defaultDate = this._getDefaultDate(inst);
	html = "";
	dow;
	for (row = 0; row < numMonths[0]; row++) {
	  group = "";
	  this.maxRows = 4;
	  for (col = 0; col < numMonths[1]; col++) {
		selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay));
		cornerClass = " ui-corner-all";
		calender = "";
		if (isMultiMonth) {
		  calender += "<div class='ui-datepicker-group";
		  if (numMonths[1] > 1) {
			switch (col) {
			  case 0: calender += " ui-datepicker-group-first";
				cornerClass = " ui-corner-" + (isRTL ? "right" : "left"); break;
			  case numMonths[1]-1: calender += " ui-datepicker-group-last";
				cornerClass = " ui-corner-" + (isRTL ? "left" : "right"); break;
			  default: calender += " ui-datepicker-group-middle"; cornerClass = ""; break;
			}
		  }
		  calender += "'>";
		}
		calender += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + cornerClass + "'>" +
		  (/all|left/.test(cornerClass) && row === 0 ? (isRTL ? next : prev) : "") +
		  (/all|right/.test(cornerClass) && row === 0 ? (isRTL ? prev : next) : "") +
		  this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate,
		  row > 0 || col > 0, monthNames, monthNamesShort) + // draw month headers
		  "</div><table class='ui-datepicker-calendar'><thead>" +
		  "<tr>";
		thead = (showWeek ? "<th class='ui-datepicker-week-col'>" + this._get(inst, "weekHeader") + "</th>" : "");
		for (dow = 0; dow < 7; dow++) { // days of the week
		  day = (dow + firstDay) % 7;
		  thead += "<th" + ((dow + firstDay + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" +
			"<span title='" + dayNames[day] + "'>" + dayNamesMin[day] + "</span></th>";
		}
		calender += thead + "</tr></thead><tbody>";
		daysInMonth = this._getDaysInMonth(drawYear, drawMonth);
		if (drawYear === inst.selectedYear && drawMonth === inst.selectedMonth) {
		  inst.selectedDay = Math.min(inst.selectedDay, daysInMonth);
		}
		leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7;
		curRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
		numRows = (isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows); //If multiple months, use the higher number of rows (see #7043)
		this.maxRows = numRows;
		printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays));
		for (dRow = 0; dRow < numRows; dRow++) { // create date picker rows
		  calender += "<tr>";
		  tbody = (!showWeek ? "" : "<td class='ui-datepicker-week-col'>" +
			this._get(inst, "calculateWeek")(printDate) + "</td>");
		  for (dow = 0; dow < 7; dow++) { // create date picker days
			daySettings = (beforeShowDay ?
			  beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, ""]);
			otherMonth = (printDate.getMonth() !== drawMonth);
			unselectable = (otherMonth && !selectOtherMonths) || !daySettings[0] ||
			  (minDate && printDate < minDate) || (maxDate && printDate > maxDate);
			tbody += "<td class='" +
			  ((dow + firstDay + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + // highlight weekends
			  (otherMonth ? " ui-datepicker-other-month" : "") + // highlight days from other months
			  ((printDate.getTime() === selectedDate.getTime() && drawMonth === inst.selectedMonth && inst._keyEvent) || // user pressed key
			  (defaultDate.getTime() === printDate.getTime() && defaultDate.getTime() === selectedDate.getTime()) ?
			  // or defaultDate is current printedDate and defaultDate is selectedDate
			  " " + this._dayOverClass : "") + // highlight selected day
			  (unselectable ? " " + this._unselectableClass + " ui-state-disabled": "") +  // highlight unselectable days
			  (otherMonth && !showOtherMonths ? "" : " " + daySettings[1] + // highlight custom dates
			  (printDate.getTime() === currentDate.getTime() ? " " + this._currentClass : "") + // highlight selected day
			  (printDate.getTime() === today.getTime() ? " ui-datepicker-today" : "")) + "'" + // highlight today (if different)
			  ((!otherMonth || showOtherMonths) && daySettings[2] ? " title='" + daySettings[2].replace(/'/g, "&#39;") + "'" : "") + // cell title
			  (unselectable ? "" : " data-handler='selectDay' data-event='click' data-month='" + printDate.getMonth() + "' data-year='" + printDate.getFullYear() + "'") + ">" + // actions
			  (otherMonth && !showOtherMonths ? "&#xa0;" : // display for other months
			  (unselectable ? "<span class='ui-state-default'>" + printDate.getDate() + "</span>" : "<a class='ui-state-default" +
			  (printDate.getTime() === today.getTime() ? " ui-state-highlight" : "") +
			  (printDate.getTime() === currentDate.getTime() ? " ui-state-active" : "") + // highlight selected day
			  (otherMonth ? " ui-priority-secondary" : "") + // distinguish dates from other months
			  "' href='#'>" + printDate.getDate() + "</a>")) + "</td>"; // display selectable date
			printDate.setDate(printDate.getDate() + 1);
			printDate = this._daylightSavingAdjust(printDate);
		  }
		  calender += tbody + "</tr>";
		}
		drawMonth++;
		if (drawMonth > 11) {
		  drawMonth = 0;
		  drawYear++;
		}
		calender += "</tbody></table>" + (isMultiMonth ? "</div>" +
			  ((numMonths[0] > 0 && col === numMonths[1]-1) ? "<div class='ui-datepicker-row-break'></div>" : "") : "");
		group += calender;
	  }
	  html += group;
	}
	html += buttonPanel;
	inst._keyEvent = false;
	return html;
  },

  /* Generate the month and year header. */
  _generateMonthYearHeader: function(inst, drawMonth, drawYear, minDate, maxDate,
	  secondary, monthNames, monthNamesShort) {

	var inMinYear, inMaxYear, month, years, thisYear, determineYear, year, endYear,
	  changeMonth = this._get(inst, "changeMonth"),
	  changeYear = this._get(inst, "changeYear"),
	  showMonthAfterYear = this._get(inst, "showMonthAfterYear"),
	  html = "<div class='ui-datepicker-title'>",
	  monthHtml = "";

	// month selection
	if (secondary || !changeMonth) {
	  monthHtml += "<span class='ui-datepicker-month'>" + monthNames[drawMonth] + "</span>";
	} else {
	  inMinYear = (minDate && minDate.getFullYear() === drawYear);
	  inMaxYear = (maxDate && maxDate.getFullYear() === drawYear);
	  monthHtml += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";
	  for ( month = 0; month < 12; month++) {
		if ((!inMinYear || month >= minDate.getMonth()) && (!inMaxYear || month <= maxDate.getMonth())) {
		  monthHtml += "<option value='" + month + "'" +
			(month === drawMonth ? " selected='selected'" : "") +
			">" + monthNamesShort[month] + "</option>";
		}
	  }
	  monthHtml += "</select>";
	}

	if (!showMonthAfterYear) {
	  html += monthHtml + (secondary || !(changeMonth && changeYear) ? "&#xa0;" : "");
	}

	// year selection
	if ( !inst.yearshtml ) {
	  inst.yearshtml = "";
	  if (secondary || !changeYear) {
		html += "<span class='ui-datepicker-year'>" + drawYear + "</span>";
	  } else {
		// determine range of years to display
		years = this._get(inst, "yearRange").split(":");
		thisYear = new Date().getFullYear();
		determineYear = function(value) {
		  var year = (value.match(/c[+\-].*/) ? drawYear + parseInt(value.substring(1), 10) :
			(value.match(/[+\-].*/) ? thisYear + parseInt(value, 10) :
			parseInt(value, 10)));
		  return (isNaN(year) ? thisYear : year);
		};
		year = determineYear(years[0]);
		endYear = Math.max(year, determineYear(years[1] || ""));
		year = (minDate ? Math.max(year, minDate.getFullYear()) : year);
		endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear);
		inst.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";
		for (; year <= endYear; year++) {
		  inst.yearshtml += "<option value='" + year + "'" +
			(year === drawYear ? " selected='selected'" : "") +
			">" + year + "</option>";
		}
		inst.yearshtml += "</select>";

		html += inst.yearshtml;
		inst.yearshtml = null;
	  }
	}

	html += this._get(inst, "yearSuffix");
	if (showMonthAfterYear) {
	  html += (secondary || !(changeMonth && changeYear) ? "&#xa0;" : "") + monthHtml;
	}
	html += "</div>"; // Close datepicker_header
	return html;
  },

  /* Adjust one of the date sub-fields. */
  _adjustInstDate: function(inst, offset, period) {
	var year = inst.drawYear + (period === "Y" ? offset : 0),
	  month = inst.drawMonth + (period === "M" ? offset : 0),
	  day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) + (period === "D" ? offset : 0),
	  date = this._restrictMinMax(inst, this._daylightSavingAdjust(new Date(year, month, day)));

	inst.selectedDay = date.getDate();
	inst.drawMonth = inst.selectedMonth = date.getMonth();
	inst.drawYear = inst.selectedYear = date.getFullYear();
	if (period === "M" || period === "Y") {
	  this._notifyChange(inst);
	}
  },

  /* Ensure a date is within any min/max bounds. */
  _restrictMinMax: function(inst, date) {
	var minDate = this._getMinMaxDate(inst, "min"),
	  maxDate = this._getMinMaxDate(inst, "max"),
	  newDate = (minDate && date < minDate ? minDate : date);
	return (maxDate && newDate > maxDate ? maxDate : newDate);
  },

  /* Notify change of month/year. */
  _notifyChange: function(inst) {
	var onChange = this._get(inst, "onChangeMonthYear");
	if (onChange) {
	  onChange.apply((inst.input ? inst.input[0] : null),
		[inst.selectedYear, inst.selectedMonth + 1, inst]);
	}
  },

  /* Determine the number of months to show. */
  _getNumberOfMonths: function(inst) {
	var numMonths = this._get(inst, "numberOfMonths");
	return (numMonths == null ? [1, 1] : (typeof numMonths === "number" ? [1, numMonths] : numMonths));
  },

  /* Determine the current maximum date - ensure no time components are set. */
  _getMinMaxDate: function(inst, minMax) {
	return this._determineDate(inst, this._get(inst, minMax + "Date"), null);
  },

  /* Find the number of days in a given month. */
  _getDaysInMonth: function(year, month) {
	return 32 - this._daylightSavingAdjust(new Date(year, month, 32)).getDate();
  },

  /* Find the day of the week of the first of a month. */
  _getFirstDayOfMonth: function(year, month) {
	return new Date(year, month, 1).getDay();
  },

  /* Determines if we should allow a "next/prev" month display change. */
  _canAdjustMonth: function(inst, offset, curYear, curMonth) {
	var numMonths = this._getNumberOfMonths(inst),
	  date = this._daylightSavingAdjust(new Date(curYear,
	  curMonth + (offset < 0 ? offset : numMonths[0] * numMonths[1]), 1));

	if (offset < 0) {
	  date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth()));
	}
	return this._isInRange(inst, date);
  },

  /* Is the given date in the accepted range? */
  _isInRange: function(inst, date) {
	var yearSplit, currentYear,
	  minDate = this._getMinMaxDate(inst, "min"),
	  maxDate = this._getMinMaxDate(inst, "max"),
	  minYear = null,
	  maxYear = null,
	  years = this._get(inst, "yearRange");
	  if (years){
		yearSplit = years.split(":");
		currentYear = new Date().getFullYear();
		minYear = parseInt(yearSplit[0], 10);
		maxYear = parseInt(yearSplit[1], 10);
		if ( yearSplit[0].match(/[+\-].*/) ) {
		  minYear += currentYear;
		}
		if ( yearSplit[1].match(/[+\-].*/) ) {
		  maxYear += currentYear;
		}
	  }

	return ((!minDate || date.getTime() >= minDate.getTime()) &&
	  (!maxDate || date.getTime() <= maxDate.getTime()) &&
	  (!minYear || date.getFullYear() >= minYear) &&
	  (!maxYear || date.getFullYear() <= maxYear));
  },

  /* Provide the configuration settings for formatting/parsing. */
  _getFormatConfig: function(inst) {
	var shortYearCutoff = this._get(inst, "shortYearCutoff");
	shortYearCutoff = (typeof shortYearCutoff !== "string" ? shortYearCutoff :
	  new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
	return {shortYearCutoff: shortYearCutoff,
	  dayNamesShort: this._get(inst, "dayNamesShort"), dayNames: this._get(inst, "dayNames"),
	  monthNamesShort: this._get(inst, "monthNamesShort"), monthNames: this._get(inst, "monthNames")};
  },

  /* Format the given date for display. */
  _formatDate: function(inst, day, month, year) {
	if (!day) {
	  inst.currentDay = inst.selectedDay;
	  inst.currentMonth = inst.selectedMonth;
	  inst.currentYear = inst.selectedYear;
	}
	var date = (day ? (typeof day === "object" ? day :
	  this._daylightSavingAdjust(new Date(year, month, day))) :
	  this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
	return this.formatDate(this._get(inst, "dateFormat"), date, this._getFormatConfig(inst));
  }
});

/*
 * Bind hover events for datepicker elements.
 * Done via delegate so the binding only occurs once in the lifetime of the parent div.
 * Global instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
 */
function bindHover(dpDiv) {
  var selector = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
  return dpDiv.delegate(selector, "mouseout", function() {
	  $(this).removeClass("ui-state-hover");
	  if (this.className.indexOf("ui-datepicker-prev") !== -1) {
		$(this).removeClass("ui-datepicker-prev-hover");
	  }
	  if (this.className.indexOf("ui-datepicker-next") !== -1) {
		$(this).removeClass("ui-datepicker-next-hover");
	  }
	})
	.delegate(selector, "mouseover", function(){
	  if (!$.datepicker._isDisabledDatepicker( instActive.inline ? dpDiv.parent()[0] : instActive.input[0])) {
		$(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");
		$(this).addClass("ui-state-hover");
		if (this.className.indexOf("ui-datepicker-prev") !== -1) {
		  $(this).addClass("ui-datepicker-prev-hover");
		}
		if (this.className.indexOf("ui-datepicker-next") !== -1) {
		  $(this).addClass("ui-datepicker-next-hover");
		}
	  }
	});
}

/* jQuery extend now ignores nulls! */
function extendRemove(target, props) {
  $.extend(target, props);
  for (var name in props) {
	if (props[name] == null) {
	  target[name] = props[name];
	}
  }
  return target;
}

/* Invoke the datepicker functionality.
   @param  options  string - a command, optionally followed by additional parameters or
		  Object - settings for attaching new datepicker functionality
   @return  jQuery object */
$.fn.datepicker = function(options){

  /* Verify an empty collection wasn't passed - Fixes #6976 */
  if ( !this.length ) {
	return this;
  }

  /* Initialise the date picker. */
  if (!$.datepicker.initialized) {
	$(document).mousedown($.datepicker._checkExternalClick);
	$.datepicker.initialized = true;
  }

  /* Append datepicker main container to body if not exist. */
  if ($("#"+$.datepicker._mainDivId).length === 0) {
	$("body").append($.datepicker.dpDiv);
  }

  var otherArgs = Array.prototype.slice.call(arguments, 1);
  if (typeof options === "string" && (options === "isDisabled" || options === "getDate" || options === "widget")) {
	return $.datepicker["_" + options + "Datepicker"].
	  apply($.datepicker, [this[0]].concat(otherArgs));
  }
  if (options === "option" && arguments.length === 2 && typeof arguments[1] === "string") {
	return $.datepicker["_" + options + "Datepicker"].
	  apply($.datepicker, [this[0]].concat(otherArgs));
  }
  return this.each(function() {
	typeof options === "string" ?
	  $.datepicker["_" + options + "Datepicker"].
		apply($.datepicker, [this].concat(otherArgs)) :
	  $.datepicker._attachDatepicker(this, options);
  });
};

$.datepicker = new Datepicker(); // singleton instance
$.datepicker.initialized = false;
$.datepicker.uuid = new Date().getTime();
$.datepicker.version = "1.10.4";

})(jQuery);
(function( $, undefined ) {

var sizeRelatedOptions = {
	buttons: true,
	height: true,
	maxHeight: true,
	maxWidth: true,
	minHeight: true,
	minWidth: true,
	width: true
  },
  resizableRelatedOptions = {
	maxHeight: true,
	maxWidth: true,
	minHeight: true,
	minWidth: true
  };

$.widget( "ui.dialog", {
  version: "1.10.4",
  options: {
	appendTo: "body",
	autoOpen: true,
	buttons: [],
	closeOnEscape: true,
	closeText: "close",
	dialogClass: "",
	draggable: true,
	hide: null,
	height: "auto",
	maxHeight: null,
	maxWidth: null,
	minHeight: 150,
	minWidth: 150,
	modal: false,
	position: {
	  my: "center",
	  at: "center",
	  of: window,
	  collision: "fit",
	  // Ensure the titlebar is always visible
	  using: function( pos ) {
		var topOffset = $( this ).css( pos ).offset().top;
		if ( topOffset < 0 ) {
		  $( this ).css( "top", pos.top - topOffset );
		}
	  }
	},
	resizable: true,
	show: null,
	title: null,
	width: 300,

	// callbacks
	beforeClose: null,
	close: null,
	drag: null,
	dragStart: null,
	dragStop: null,
	focus: null,
	open: null,
	resize: null,
	resizeStart: null,
	resizeStop: null
  },

  _create: function() {
	this.originalCss = {
	  display: this.element[0].style.display,
	  width: this.element[0].style.width,
	  minHeight: this.element[0].style.minHeight,
	  maxHeight: this.element[0].style.maxHeight,
	  height: this.element[0].style.height
	};
	this.originalPosition = {
	  parent: this.element.parent(),
	  index: this.element.parent().children().index( this.element )
	};
	this.originalTitle = this.element.attr("title");
	this.options.title = this.options.title || this.originalTitle;

	this._createWrapper();

	this.element
	  .show()
	  .removeAttr("title")
	  .addClass("ui-dialog-content ui-widget-content")
	  .appendTo( this.uiDialog );

	this._createTitlebar();
	this._createButtonPane();

	if ( this.options.draggable && $.fn.draggable ) {
	  this._makeDraggable();
	}
	if ( this.options.resizable && $.fn.resizable ) {
	  this._makeResizable();
	}

	this._isOpen = false;
  },

  _init: function() {
	if ( this.options.autoOpen ) {
	  this.open();
	}
  },

  _appendTo: function() {
	var element = this.options.appendTo;
	if ( element && (element.jquery || element.nodeType) ) {
	  return $( element );
	}
	return this.document.find( element || "body" ).eq( 0 );
  },

  _destroy: function() {
	var next,
	  originalPosition = this.originalPosition;

	this._destroyOverlay();

	this.element
	  .removeUniqueId()
	  .removeClass("ui-dialog-content ui-widget-content")
	  .css( this.originalCss )
	  // Without detaching first, the following becomes really slow
	  .detach();

	this.uiDialog.stop( true, true ).remove();

	if ( this.originalTitle ) {
	  this.element.attr( "title", this.originalTitle );
	}

	next = originalPosition.parent.children().eq( originalPosition.index );
	// Don't try to place the dialog next to itself (#8613)
	if ( next.length && next[0] !== this.element[0] ) {
	  next.before( this.element );
	} else {
	  originalPosition.parent.append( this.element );
	}
  },

  widget: function() {
	return this.uiDialog;
  },

  disable: $.noop,
  enable: $.noop,

  close: function( event ) {
	var activeElement,
	  that = this;

	if ( !this._isOpen || this._trigger( "beforeClose", event ) === false ) {
	  return;
	}

	this._isOpen = false;
	this._destroyOverlay();

	if ( !this.opener.filter(":focusable").focus().length ) {

	  // support: IE9
	  // IE9 throws an "Unspecified error" accessing document.activeElement from an <iframe>
	  try {
		activeElement = this.document[ 0 ].activeElement;

		// Support: IE9, IE10
		// If the <body> is blurred, IE will switch windows, see #4520
		if ( activeElement && activeElement.nodeName.toLowerCase() !== "body" ) {

		  // Hiding a focused element doesn't trigger blur in WebKit
		  // so in case we have nothing to focus on, explicitly blur the active element
		  // https://bugs.webkit.org/show_bug.cgi?id=47182
		  $( activeElement ).blur();
		}
	  } catch ( error ) {}
	}

	this._hide( this.uiDialog, this.options.hide, function() {
	  that._trigger( "close", event );
	});
  },

  isOpen: function() {
	return this._isOpen;
  },

  moveToTop: function() {
	this._moveToTop();
  },

  _moveToTop: function( event, silent ) {
	var moved = !!this.uiDialog.nextAll(":visible").insertBefore( this.uiDialog ).length;
	if ( moved && !silent ) {
	  this._trigger( "focus", event );
	}
	return moved;
  },

  open: function() {
	var that = this;
	if ( this._isOpen ) {
	  if ( this._moveToTop() ) {
		this._focusTabbable();
	  }
	  return;
	}

	this._isOpen = true;
	this.opener = $( this.document[0].activeElement );

	this._size();
	this._position();
	this._createOverlay();
	this._moveToTop( null, true );
	this._show( this.uiDialog, this.options.show, function() {
	  that._focusTabbable();
	  that._trigger("focus");
	});

	this._trigger("open");
  },

  _focusTabbable: function() {
	// Set focus to the first match:
	// 1. First element inside the dialog matching [autofocus]
	// 2. Tabbable element inside the content element
	// 3. Tabbable element inside the buttonpane
	// 4. The close button
	// 5. The dialog itself
	var hasFocus = this.element.find("[autofocus]");
	if ( !hasFocus.length ) {
	  hasFocus = this.element.find(":tabbable");
	}
	if ( !hasFocus.length ) {
	  hasFocus = this.uiDialogButtonPane.find(":tabbable");
	}
	if ( !hasFocus.length ) {
	  hasFocus = this.uiDialogTitlebarClose.filter(":tabbable");
	}
	if ( !hasFocus.length ) {
	  hasFocus = this.uiDialog;
	}
	hasFocus.eq( 0 ).focus();
  },

  _keepFocus: function( event ) {
	function checkFocus() {
	  var activeElement = this.document[0].activeElement,
		isActive = this.uiDialog[0] === activeElement ||
		  $.contains( this.uiDialog[0], activeElement );
	  if ( !isActive ) {
		this._focusTabbable();
	  }
	}
	event.preventDefault();
	checkFocus.call( this );
	// support: IE
	// IE <= 8 doesn't prevent moving focus even with event.preventDefault()
	// so we check again later
	this._delay( checkFocus );
  },

  _createWrapper: function() {
	this.uiDialog = $("<div>")
	  .addClass( "ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " +
		this.options.dialogClass )
	  .hide()
	  .attr({
		// Setting tabIndex makes the div focusable
		tabIndex: -1,
		role: "dialog"
	  })
	  .appendTo( this._appendTo() );

	this._on( this.uiDialog, {
	  keydown: function( event ) {
		if ( this.options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode &&
			event.keyCode === $.ui.keyCode.ESCAPE ) {
		  event.preventDefault();
		  this.close( event );
		  return;
		}

		// prevent tabbing out of dialogs
		if ( event.keyCode !== $.ui.keyCode.TAB ) {
		  return;
		}
		var tabbables = this.uiDialog.find(":tabbable"),
		  first = tabbables.filter(":first"),
		  last  = tabbables.filter(":last");

		if ( ( event.target === last[0] || event.target === this.uiDialog[0] ) && !event.shiftKey ) {
		  first.focus( 1 );
		  event.preventDefault();
		} else if ( ( event.target === first[0] || event.target === this.uiDialog[0] ) && event.shiftKey ) {
		  last.focus( 1 );
		  event.preventDefault();
		}
	  },
	  mousedown: function( event ) {
		if ( this._moveToTop( event ) ) {
		  this._focusTabbable();
		}
	  }
	});

	// We assume that any existing aria-describedby attribute means
	// that the dialog content is marked up properly
	// otherwise we brute force the content as the description
	if ( !this.element.find("[aria-describedby]").length ) {
	  this.uiDialog.attr({
		"aria-describedby": this.element.uniqueId().attr("id")
	  });
	}
  },

  _createTitlebar: function() {
	var uiDialogTitle;

	this.uiDialogTitlebar = $("<div>")
	  .addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix")
	  .prependTo( this.uiDialog );
	this._on( this.uiDialogTitlebar, {
	  mousedown: function( event ) {
		// Don't prevent click on close button (#8838)
		// Focusing a dialog that is partially scrolled out of view
		// causes the browser to scroll it into view, preventing the click event
		if ( !$( event.target ).closest(".ui-dialog-titlebar-close") ) {
		  // Dialog isn't getting focus when dragging (#8063)
		  this.uiDialog.focus();
		}
	  }
	});

	// support: IE
	// Use type="button" to prevent enter keypresses in textboxes from closing the
	// dialog in IE (#9312)
	this.uiDialogTitlebarClose = $( "<button type='button'></button>" )
	  .button({
		label: this.options.closeText,
		icons: {
		  primary: "ui-icon-closethick"
		},
		text: false
	  })
	  .addClass("ui-dialog-titlebar-close")
	  .appendTo( this.uiDialogTitlebar );
	this._on( this.uiDialogTitlebarClose, {
	  click: function( event ) {
		event.preventDefault();
		this.close( event );
	  }
	});

	uiDialogTitle = $("<span>")
	  .uniqueId()
	  .addClass("ui-dialog-title")
	  .prependTo( this.uiDialogTitlebar );
	this._title( uiDialogTitle );

	this.uiDialog.attr({
	  "aria-labelledby": uiDialogTitle.attr("id")
	});
  },

  _title: function( title ) {
	if ( !this.options.title ) {
	  title.html("&#160;");
	}
	title.text( this.options.title );
  },

  _createButtonPane: function() {
	this.uiDialogButtonPane = $("<div>")
	  .addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");

	this.uiButtonSet = $("<div>")
	  .addClass("ui-dialog-buttonset")
	  .appendTo( this.uiDialogButtonPane );

	this._createButtons();
  },

  _createButtons: function() {
	var that = this,
	  buttons = this.options.buttons;

	// if we already have a button pane, remove it
	this.uiDialogButtonPane.remove();
	this.uiButtonSet.empty();

	if ( $.isEmptyObject( buttons ) || ($.isArray( buttons ) && !buttons.length) ) {
	  this.uiDialog.removeClass("ui-dialog-buttons");
	  return;
	}

	$.each( buttons, function( name, props ) {
	  var click, buttonOptions;
	  props = $.isFunction( props ) ?
		{ click: props, text: name } :
		props;
	  // Default to a non-submitting button
	  props = $.extend( { type: "button" }, props );
	  // Change the context for the click callback to be the main element
	  click = props.click;
	  props.click = function() {
		click.apply( that.element[0], arguments );
	  };
	  buttonOptions = {
		icons: props.icons,
		text: props.showText
	  };
	  delete props.icons;
	  delete props.showText;
	  $( "<button></button>", props )
		.button( buttonOptions )
		.appendTo( that.uiButtonSet );
	});
	this.uiDialog.addClass("ui-dialog-buttons");
	this.uiDialogButtonPane.appendTo( this.uiDialog );
  },

  _makeDraggable: function() {
	var that = this,
	  options = this.options;

	function filteredUi( ui ) {
	  return {
		position: ui.position,
		offset: ui.offset
	  };
	}

	this.uiDialog.draggable({
	  cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
	  handle: ".ui-dialog-titlebar",
	  containment: "document",
	  start: function( event, ui ) {
		$( this ).addClass("ui-dialog-dragging");
		that._blockFrames();
		that._trigger( "dragStart", event, filteredUi( ui ) );
	  },
	  drag: function( event, ui ) {
		that._trigger( "drag", event, filteredUi( ui ) );
	  },
	  stop: function( event, ui ) {
		options.position = [
		  ui.position.left - that.document.scrollLeft(),
		  ui.position.top - that.document.scrollTop()
		];
		$( this ).removeClass("ui-dialog-dragging");
		that._unblockFrames();
		that._trigger( "dragStop", event, filteredUi( ui ) );
	  }
	});
  },

  _makeResizable: function() {
	var that = this,
	  options = this.options,
	  handles = options.resizable,
	  // .ui-resizable has position: relative defined in the stylesheet
	  // but dialogs have to use absolute or fixed positioning
	  position = this.uiDialog.css("position"),
	  resizeHandles = typeof handles === "string" ?
		handles	:
		"n,e,s,w,se,sw,ne,nw";

	function filteredUi( ui ) {
	  return {
		originalPosition: ui.originalPosition,
		originalSize: ui.originalSize,
		position: ui.position,
		size: ui.size
	  };
	}

	this.uiDialog.resizable({
	  cancel: ".ui-dialog-content",
	  containment: "document",
	  alsoResize: this.element,
	  maxWidth: options.maxWidth,
	  maxHeight: options.maxHeight,
	  minWidth: options.minWidth,
	  minHeight: this._minHeight(),
	  handles: resizeHandles,
	  start: function( event, ui ) {
		$( this ).addClass("ui-dialog-resizing");
		that._blockFrames();
		that._trigger( "resizeStart", event, filteredUi( ui ) );
	  },
	  resize: function( event, ui ) {
		that._trigger( "resize", event, filteredUi( ui ) );
	  },
	  stop: function( event, ui ) {
		options.height = $( this ).height();
		options.width = $( this ).width();
		$( this ).removeClass("ui-dialog-resizing");
		that._unblockFrames();
		that._trigger( "resizeStop", event, filteredUi( ui ) );
	  }
	})
	.css( "position", position );
  },

  _minHeight: function() {
	var options = this.options;

	return options.height === "auto" ?
	  options.minHeight :
	  Math.min( options.minHeight, options.height );
  },

  _position: function() {
	// Need to show the dialog to get the actual offset in the position plugin
	var isVisible = this.uiDialog.is(":visible");
	if ( !isVisible ) {
	  this.uiDialog.show();
	}
	this.uiDialog.position( this.options.position );
	if ( !isVisible ) {
	  this.uiDialog.hide();
	}
  },

  _setOptions: function( options ) {
	var that = this,
	  resize = false,
	  resizableOptions = {};

	$.each( options, function( key, value ) {
	  that._setOption( key, value );

	  if ( key in sizeRelatedOptions ) {
		resize = true;
	  }
	  if ( key in resizableRelatedOptions ) {
		resizableOptions[ key ] = value;
	  }
	});

	if ( resize ) {
	  this._size();
	  this._position();
	}
	if ( this.uiDialog.is(":data(ui-resizable)") ) {
	  this.uiDialog.resizable( "option", resizableOptions );
	}
  },

  _setOption: function( key, value ) {
	var isDraggable, isResizable,
	  uiDialog = this.uiDialog;

	if ( key === "dialogClass" ) {
	  uiDialog
		.removeClass( this.options.dialogClass )
		.addClass( value );
	}

	if ( key === "disabled" ) {
	  return;
	}

	this._super( key, value );

	if ( key === "appendTo" ) {
	  this.uiDialog.appendTo( this._appendTo() );
	}

	if ( key === "buttons" ) {
	  this._createButtons();
	}

	if ( key === "closeText" ) {
	  this.uiDialogTitlebarClose.button({
		// Ensure that we always pass a string
		label: "" + value
	  });
	}

	if ( key === "draggable" ) {
	  isDraggable = uiDialog.is(":data(ui-draggable)");
	  if ( isDraggable && !value ) {
		uiDialog.draggable("destroy");
	  }

	  if ( !isDraggable && value ) {
		this._makeDraggable();
	  }
	}

	if ( key === "position" ) {
	  this._position();
	}

	if ( key === "resizable" ) {
	  // currently resizable, becoming non-resizable
	  isResizable = uiDialog.is(":data(ui-resizable)");
	  if ( isResizable && !value ) {
		uiDialog.resizable("destroy");
	  }

	  // currently resizable, changing handles
	  if ( isResizable && typeof value === "string" ) {
		uiDialog.resizable( "option", "handles", value );
	  }

	  // currently non-resizable, becoming resizable
	  if ( !isResizable && value !== false ) {
		this._makeResizable();
	  }
	}

	if ( key === "title" ) {
	  this._title( this.uiDialogTitlebar.find(".ui-dialog-title") );
	}
  },

  _size: function() {
	// If the user has resized the dialog, the .ui-dialog and .ui-dialog-content
	// divs will both have width and height set, so we need to reset them
	var nonContentHeight, minContentHeight, maxContentHeight,
	  options = this.options;

	// Reset content sizing
	this.element.show().css({
	  width: "auto",
	  minHeight: 0,
	  maxHeight: "none",
	  height: 0
	});

	if ( options.minWidth > options.width ) {
	  options.width = options.minWidth;
	}

	// reset wrapper sizing
	// determine the height of all the non-content elements
	nonContentHeight = this.uiDialog.css({
		height: "auto",
		width: options.width
	  })
	  .outerHeight();
	minContentHeight = Math.max( 0, options.minHeight - nonContentHeight );
	maxContentHeight = typeof options.maxHeight === "number" ?
	  Math.max( 0, options.maxHeight - nonContentHeight ) :
	  "none";

	if ( options.height === "auto" ) {
	  this.element.css({
		minHeight: minContentHeight,
		maxHeight: maxContentHeight,
		height: "auto"
	  });
	} else {
	  this.element.height( Math.max( 0, options.height - nonContentHeight ) );
	}

	if (this.uiDialog.is(":data(ui-resizable)") ) {
	  this.uiDialog.resizable( "option", "minHeight", this._minHeight() );
	}
  },

  _blockFrames: function() {
	this.iframeBlocks = this.document.find( "iframe" ).map(function() {
	  var iframe = $( this );

	  return $( "<div>" )
		.css({
		  position: "absolute",
		  width: iframe.outerWidth(),
		  height: iframe.outerHeight()
		})
		.appendTo( iframe.parent() )
		.offset( iframe.offset() )[0];
	});
  },

  _unblockFrames: function() {
	if ( this.iframeBlocks ) {
	  this.iframeBlocks.remove();
	  delete this.iframeBlocks;
	}
  },

  _allowInteraction: function( event ) {
	if ( $( event.target ).closest(".ui-dialog").length ) {
	  return true;
	}

	// TODO: Remove hack when datepicker implements
	// the .ui-front logic (#8989)
	return !!$( event.target ).closest(".ui-datepicker").length;
  },

  _createOverlay: function() {
	if ( !this.options.modal ) {
	  return;
	}

	var that = this,
	  widgetFullName = this.widgetFullName;
	if ( !$.ui.dialog.overlayInstances ) {
	  // Prevent use of anchors and inputs.
	  // We use a delay in case the overlay is created from an
	  // event that we're going to be cancelling. (#2804)
	  this._delay(function() {
		// Handle .dialog().dialog("close") (#4065)
		if ( $.ui.dialog.overlayInstances ) {
		  this.document.bind( "focusin.dialog", function( event ) {
			if ( !that._allowInteraction( event ) ) {
			  event.preventDefault();
			  $(".ui-dialog:visible:last .ui-dialog-content")
				.data( widgetFullName )._focusTabbable();
			}
		  });
		}
	  });
	}

	this.overlay = $("<div>")
	  .addClass("ui-widget-overlay ui-front")
	  .appendTo( this._appendTo() );
	this._on( this.overlay, {
	  mousedown: "_keepFocus"
	});
	$.ui.dialog.overlayInstances++;
  },

  _destroyOverlay: function() {
	if ( !this.options.modal ) {
	  return;
	}

	if ( this.overlay ) {
	  $.ui.dialog.overlayInstances--;

	  if ( !$.ui.dialog.overlayInstances ) {
		this.document.unbind( "focusin.dialog" );
	  }
	  this.overlay.remove();
	  this.overlay = null;
	}
  }
});

$.ui.dialog.overlayInstances = 0;

// DEPRECATED
if ( $.uiBackCompat !== false ) {
  // position option with array notation
  // just override with old implementation
  $.widget( "ui.dialog", $.ui.dialog, {
	_position: function() {
	  var position = this.options.position,
		myAt = [],
		offset = [ 0, 0 ],
		isVisible;

	  if ( position ) {
		if ( typeof position === "string" || (typeof position === "object" && "0" in position ) ) {
		  myAt = position.split ? position.split(" ") : [ position[0], position[1] ];
		  if ( myAt.length === 1 ) {
			myAt[1] = myAt[0];
		  }

		  $.each( [ "left", "top" ], function( i, offsetPosition ) {
			if ( +myAt[ i ] === myAt[ i ] ) {
			  offset[ i ] = myAt[ i ];
			  myAt[ i ] = offsetPosition;
			}
		  });

		  position = {
			my: myAt[0] + (offset[0] < 0 ? offset[0] : "+" + offset[0]) + " " +
			  myAt[1] + (offset[1] < 0 ? offset[1] : "+" + offset[1]),
			at: myAt.join(" ")
		  };
		}

		position = $.extend( {}, $.ui.dialog.prototype.options.position, position );
	  } else {
		position = $.ui.dialog.prototype.options.position;
	  }

	  // need to show the dialog to get the actual offset in the position plugin
	  isVisible = this.uiDialog.is(":visible");
	  if ( !isVisible ) {
		this.uiDialog.show();
	  }
	  this.uiDialog.position( position );
	  if ( !isVisible ) {
		this.uiDialog.hide();
	  }
	}
  });
}

}( jQuery ) );
(function( $, undefined ) {

$.widget( "ui.menu", {
  version: "1.10.4",
  defaultElement: "<ul>",
  delay: 300,
  options: {
	icons: {
	  submenu: "ui-icon-carat-1-e"
	},
	menus: "ul",
	position: {
	  my: "left top",
	  at: "right top"
	},
	role: "menu",

	// callbacks
	blur: null,
	focus: null,
	select: null
  },

  _create: function() {
	this.activeMenu = this.element;
	// flag used to prevent firing of the click handler
	// as the event bubbles up through nested menus
	this.mouseHandled = false;
	this.element
	  .uniqueId()
	  .addClass( "ui-menu ui-widget ui-widget-content ui-corner-all" )
	  .toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length )
	  .attr({
		role: this.options.role,
		tabIndex: 0
	  })
	  // need to catch all clicks on disabled menu
	  // not possible through _on
	  .bind( "click" + this.eventNamespace, $.proxy(function( event ) {
		if ( this.options.disabled ) {
		  event.preventDefault();
		}
	  }, this ));

	if ( this.options.disabled ) {
	  this.element
		.addClass( "ui-state-disabled" )
		.attr( "aria-disabled", "true" );
	}

	this._on({
	  // Prevent focus from sticking to links inside menu after clicking
	  // them (focus should always stay on UL during navigation).
	  "mousedown .ui-menu-item > a": function( event ) {
		event.preventDefault();
	  },
	  "click .ui-state-disabled > a": function( event ) {
		event.preventDefault();
	  },
	  "click .ui-menu-item:has(a)": function( event ) {
		var target = $( event.target ).closest( ".ui-menu-item" );
		if ( !this.mouseHandled && target.not( ".ui-state-disabled" ).length ) {
		  this.select( event );

		  // Only set the mouseHandled flag if the event will bubble, see #9469.
		  if ( !event.isPropagationStopped() ) {
			this.mouseHandled = true;
		  }

		  // Open submenu on click
		  if ( target.has( ".ui-menu" ).length ) {
			this.expand( event );
		  } else if ( !this.element.is( ":focus" ) && $( this.document[ 0 ].activeElement ).closest( ".ui-menu" ).length ) {

			// Redirect focus to the menu
			this.element.trigger( "focus", [ true ] );

			// If the active item is on the top level, let it stay active.
			// Otherwise, blur the active item since it is no longer visible.
			if ( this.active && this.active.parents( ".ui-menu" ).length === 1 ) {
			  clearTimeout( this.timer );
			}
		  }
		}
	  },
	  "mouseenter .ui-menu-item": function( event ) {
		var target = $( event.currentTarget );
		// Remove ui-state-active class from siblings of the newly focused menu item
		// to avoid a jump caused by adjacent elements both having a class with a border
		target.siblings().children( ".ui-state-active" ).removeClass( "ui-state-active" );
		this.focus( event, target );
	  },
	  mouseleave: "collapseAll",
	  "mouseleave .ui-menu": "collapseAll",
	  focus: function( event, keepActiveItem ) {
		// If there's already an active item, keep it active
		// If not, activate the first item
		var item = this.active || this.element.children( ".ui-menu-item" ).eq( 0 );

		if ( !keepActiveItem ) {
		  this.focus( event, item );
		}
	  },
	  blur: function( event ) {
		this._delay(function() {
		  if ( !$.contains( this.element[0], this.document[0].activeElement ) ) {
			this.collapseAll( event );
		  }
		});
	  },
	  keydown: "_keydown"
	});

	this.refresh();

	// Clicks outside of a menu collapse any open menus
	this._on( this.document, {
	  click: function( event ) {
		if ( !$( event.target ).closest( ".ui-menu" ).length ) {
		  this.collapseAll( event );
		}

		// Reset the mouseHandled flag
		this.mouseHandled = false;
	  }
	});
  },

  _destroy: function() {
	// Destroy (sub)menus
	this.element
	  .removeAttr( "aria-activedescendant" )
	  .find( ".ui-menu" ).addBack()
		.removeClass( "ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons" )
		.removeAttr( "role" )
		.removeAttr( "tabIndex" )
		.removeAttr( "aria-labelledby" )
		.removeAttr( "aria-expanded" )
		.removeAttr( "aria-hidden" )
		.removeAttr( "aria-disabled" )
		.removeUniqueId()
		.show();

	// Destroy menu items
	this.element.find( ".ui-menu-item" )
	  .removeClass( "ui-menu-item" )
	  .removeAttr( "role" )
	  .removeAttr( "aria-disabled" )
	  .children( "a" )
		.removeUniqueId()
		.removeClass( "ui-corner-all ui-state-hover" )
		.removeAttr( "tabIndex" )
		.removeAttr( "role" )
		.removeAttr( "aria-haspopup" )
		.children().each( function() {
		  var elem = $( this );
		  if ( elem.data( "ui-menu-submenu-carat" ) ) {
			elem.remove();
		  }
		});

	// Destroy menu dividers
	this.element.find( ".ui-menu-divider" ).removeClass( "ui-menu-divider ui-widget-content" );
  },

  _keydown: function( event ) {
	var match, prev, character, skip, regex,
	  preventDefault = true;

	function escape( value ) {
	  return value.replace( /[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&" );
	}

	switch ( event.keyCode ) {
	case $.ui.keyCode.PAGE_UP:
	  this.previousPage( event );
	  break;
	case $.ui.keyCode.PAGE_DOWN:
	  this.nextPage( event );
	  break;
	case $.ui.keyCode.HOME:
	  this._move( "first", "first", event );
	  break;
	case $.ui.keyCode.END:
	  this._move( "last", "last", event );
	  break;
	case $.ui.keyCode.UP:
	  this.previous( event );
	  break;
	case $.ui.keyCode.DOWN:
	  this.next( event );
	  break;
	case $.ui.keyCode.LEFT:
	  this.collapse( event );
	  break;
	case $.ui.keyCode.RIGHT:
	  if ( this.active && !this.active.is( ".ui-state-disabled" ) ) {
		this.expand( event );
	  }
	  break;
	case $.ui.keyCode.ENTER:
	case $.ui.keyCode.SPACE:
	  this._activate( event );
	  break;
	case $.ui.keyCode.ESCAPE:
	  this.collapse( event );
	  break;
	default:
	  preventDefault = false;
	  prev = this.previousFilter || "";
	  character = String.fromCharCode( event.keyCode );
	  skip = false;

	  clearTimeout( this.filterTimer );

	  if ( character === prev ) {
		skip = true;
	  } else {
		character = prev + character;
	  }

	  regex = new RegExp( "^" + escape( character ), "i" );
	  match = this.activeMenu.children( ".ui-menu-item" ).filter(function() {
		return regex.test( $( this ).children( "a" ).text() );
	  });
	  match = skip && match.index( this.active.next() ) !== -1 ?
		this.active.nextAll( ".ui-menu-item" ) :
		match;

	  // If no matches on the current filter, reset to the last character pressed
	  // to move down the menu to the first item that starts with that character
	  if ( !match.length ) {
		character = String.fromCharCode( event.keyCode );
		regex = new RegExp( "^" + escape( character ), "i" );
		match = this.activeMenu.children( ".ui-menu-item" ).filter(function() {
		  return regex.test( $( this ).children( "a" ).text() );
		});
	  }

	  if ( match.length ) {
		this.focus( event, match );
		if ( match.length > 1 ) {
		  this.previousFilter = character;
		  this.filterTimer = this._delay(function() {
			delete this.previousFilter;
		  }, 1000 );
		} else {
		  delete this.previousFilter;
		}
	  } else {
		delete this.previousFilter;
	  }
	}

	if ( preventDefault ) {
	  event.preventDefault();
	}
  },

  _activate: function( event ) {
	if ( !this.active.is( ".ui-state-disabled" ) ) {
	  if ( this.active.children( "a[aria-haspopup='true']" ).length ) {
		this.expand( event );
	  } else {
		this.select( event );
	  }
	}
  },

  refresh: function() {
	var menus,
	  icon = this.options.icons.submenu,
	  submenus = this.element.find( this.options.menus );

	this.element.toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length );

	// Initialize nested menus
	submenus.filter( ":not(.ui-menu)" )
	  .addClass( "ui-menu ui-widget ui-widget-content ui-corner-all" )
	  .hide()
	  .attr({
		role: this.options.role,
		"aria-hidden": "true",
		"aria-expanded": "false"
	  })
	  .each(function() {
		var menu = $( this ),
		  item = menu.prev( "a" ),
		  submenuCarat = $( "<span>" )
			.addClass( "ui-menu-icon ui-icon " + icon )
			.data( "ui-menu-submenu-carat", true );

		item
		  .attr( "aria-haspopup", "true" )
		  .prepend( submenuCarat );
		menu.attr( "aria-labelledby", item.attr( "id" ) );
	  });

	menus = submenus.add( this.element );

	// Don't refresh list items that are already adapted
	menus.children( ":not(.ui-menu-item):has(a)" )
	  .addClass( "ui-menu-item" )
	  .attr( "role", "presentation" )
	  .children( "a" )
		.uniqueId()
		.addClass( "ui-corner-all" )
		.attr({
		  tabIndex: -1,
		  role: this._itemRole()
		});

	// Initialize unlinked menu-items containing spaces and/or dashes only as dividers
	menus.children( ":not(.ui-menu-item)" ).each(function() {
	  var item = $( this );
	  // hyphen, em dash, en dash
	  if ( !/[^\-\u2014\u2013\s]/.test( item.text() ) ) {
		item.addClass( "ui-widget-content ui-menu-divider" );
	  }
	});

	// Add aria-disabled attribute to any disabled menu item
	menus.children( ".ui-state-disabled" ).attr( "aria-disabled", "true" );

	// If the active item has been removed, blur the menu
	if ( this.active && !$.contains( this.element[ 0 ], this.active[ 0 ] ) ) {
	  this.blur();
	}
  },

  _itemRole: function() {
	return {
	  menu: "menuitem",
	  listbox: "option"
	}[ this.options.role ];
  },

  _setOption: function( key, value ) {
	if ( key === "icons" ) {
	  this.element.find( ".ui-menu-icon" )
		.removeClass( this.options.icons.submenu )
		.addClass( value.submenu );
	}
	this._super( key, value );
  },

  focus: function( event, item ) {
	var nested, focused;
	this.blur( event, event && event.type === "focus" );

	this._scrollIntoView( item );

	this.active = item.first();
	focused = this.active.children( "a" ).addClass( "ui-state-focus" );
	// Only update aria-activedescendant if there's a role
	// otherwise we assume focus is managed elsewhere
	if ( this.options.role ) {
	  this.element.attr( "aria-activedescendant", focused.attr( "id" ) );
	}

	// Highlight active parent menu item, if any
	this.active
	  .parent()
	  .closest( ".ui-menu-item" )
	  .children( "a:first" )
	  .addClass( "ui-state-active" );

	if ( event && event.type === "keydown" ) {
	  this._close();
	} else {
	  this.timer = this._delay(function() {
		this._close();
	  }, this.delay );
	}

	nested = item.children( ".ui-menu" );
	if ( nested.length && event && ( /^mouse/.test( event.type ) ) ) {
	  this._startOpening(nested);
	}
	this.activeMenu = item.parent();

	this._trigger( "focus", event, { item: item } );
  },

  _scrollIntoView: function( item ) {
	var borderTop, paddingTop, offset, scroll, elementHeight, itemHeight;
	if ( this._hasScroll() ) {
	  borderTop = parseFloat( $.css( this.activeMenu[0], "borderTopWidth" ) ) || 0;
	  paddingTop = parseFloat( $.css( this.activeMenu[0], "paddingTop" ) ) || 0;
	  offset = item.offset().top - this.activeMenu.offset().top - borderTop - paddingTop;
	  scroll = this.activeMenu.scrollTop();
	  elementHeight = this.activeMenu.height();
	  itemHeight = item.height();

	  if ( offset < 0 ) {
		this.activeMenu.scrollTop( scroll + offset );
	  } else if ( offset + itemHeight > elementHeight ) {
		this.activeMenu.scrollTop( scroll + offset - elementHeight + itemHeight );
	  }
	}
  },

  blur: function( event, fromFocus ) {
	if ( !fromFocus ) {
	  clearTimeout( this.timer );
	}

	if ( !this.active ) {
	  return;
	}

	this.active.children( "a" ).removeClass( "ui-state-focus" );
	this.active = null;

	this._trigger( "blur", event, { item: this.active } );
  },

  _startOpening: function( submenu ) {
	clearTimeout( this.timer );

	// Don't open if already open fixes a Firefox bug that caused a .5 pixel
	// shift in the submenu position when mousing over the carat icon
	if ( submenu.attr( "aria-hidden" ) !== "true" ) {
	  return;
	}

	this.timer = this._delay(function() {
	  this._close();
	  this._open( submenu );
	}, this.delay );
  },

  _open: function( submenu ) {
	var position = $.extend({
	  of: this.active
	}, this.options.position );

	clearTimeout( this.timer );
	this.element.find( ".ui-menu" ).not( submenu.parents( ".ui-menu" ) )
	  .hide()
	  .attr( "aria-hidden", "true" );

	submenu
	  .show()
	  .removeAttr( "aria-hidden" )
	  .attr( "aria-expanded", "true" )
	  .position( position );
  },

  collapseAll: function( event, all ) {
	clearTimeout( this.timer );
	this.timer = this._delay(function() {
	  // If we were passed an event, look for the submenu that contains the event
	  var currentMenu = all ? this.element :
		$( event && event.target ).closest( this.element.find( ".ui-menu" ) );

	  // If we found no valid submenu ancestor, use the main menu to close all sub menus anyway
	  if ( !currentMenu.length ) {
		currentMenu = this.element;
	  }

	  this._close( currentMenu );

	  this.blur( event );
	  this.activeMenu = currentMenu;
	}, this.delay );
  },

  // With no arguments, closes the currently active menu - if nothing is active
  // it closes all menus.  If passed an argument, it will search for menus BELOW
  _close: function( startMenu ) {
	if ( !startMenu ) {
	  startMenu = this.active ? this.active.parent() : this.element;
	}

	startMenu
	  .find( ".ui-menu" )
		.hide()
		.attr( "aria-hidden", "true" )
		.attr( "aria-expanded", "false" )
	  .end()
	  .find( "a.ui-state-active" )
		.removeClass( "ui-state-active" );
  },

  collapse: function( event ) {
	var newItem = this.active &&
	  this.active.parent().closest( ".ui-menu-item", this.element );
	if ( newItem && newItem.length ) {
	  this._close();
	  this.focus( event, newItem );
	}
  },

  expand: function( event ) {
	var newItem = this.active &&
	  this.active
		.children( ".ui-menu " )
		.children( ".ui-menu-item" )
		.first();

	if ( newItem && newItem.length ) {
	  this._open( newItem.parent() );

	  // Delay so Firefox will not hide activedescendant change in expanding submenu from AT
	  this._delay(function() {
		this.focus( event, newItem );
	  });
	}
  },

  next: function( event ) {
	this._move( "next", "first", event );
  },

  previous: function( event ) {
	this._move( "prev", "last", event );
  },

  isFirstItem: function() {
	return this.active && !this.active.prevAll( ".ui-menu-item" ).length;
  },

  isLastItem: function() {
	return this.active && !this.active.nextAll( ".ui-menu-item" ).length;
  },

  _move: function( direction, filter, event ) {
	var next;
	if ( this.active ) {
	  if ( direction === "first" || direction === "last" ) {
		next = this.active
		  [ direction === "first" ? "prevAll" : "nextAll" ]( ".ui-menu-item" )
		  .eq( -1 );
	  } else {
		next = this.active
		  [ direction + "All" ]( ".ui-menu-item" )
		  .eq( 0 );
	  }
	}
	if ( !next || !next.length || !this.active ) {
	  next = this.activeMenu.children( ".ui-menu-item" )[ filter ]();
	}

	this.focus( event, next );
  },

  nextPage: function( event ) {
	var item, base, height;

	if ( !this.active ) {
	  this.next( event );
	  return;
	}
	if ( this.isLastItem() ) {
	  return;
	}
	if ( this._hasScroll() ) {
	  base = this.active.offset().top;
	  height = this.element.height();
	  this.active.nextAll( ".ui-menu-item" ).each(function() {
		item = $( this );
		return item.offset().top - base - height < 0;
	  });

	  this.focus( event, item );
	} else {
	  this.focus( event, this.activeMenu.children( ".ui-menu-item" )
		[ !this.active ? "first" : "last" ]() );
	}
  },

  previousPage: function( event ) {
	var item, base, height;
	if ( !this.active ) {
	  this.next( event );
	  return;
	}
	if ( this.isFirstItem() ) {
	  return;
	}
	if ( this._hasScroll() ) {
	  base = this.active.offset().top;
	  height = this.element.height();
	  this.active.prevAll( ".ui-menu-item" ).each(function() {
		item = $( this );
		return item.offset().top - base + height > 0;
	  });

	  this.focus( event, item );
	} else {
	  this.focus( event, this.activeMenu.children( ".ui-menu-item" ).first() );
	}
  },

  _hasScroll: function() {
	return this.element.outerHeight() < this.element.prop( "scrollHeight" );
  },

  select: function( event ) {
	// TODO: It should never be possible to not have an active item at this
	// point, but the tests don't trigger mouseenter before click.
	this.active = this.active || $( event.target ).closest( ".ui-menu-item" );
	var ui = { item: this.active };
	if ( !this.active.has( ".ui-menu" ).length ) {
	  this.collapseAll( event, true );
	}
	this._trigger( "select", event, ui );
  }
});

}( jQuery ));
(function( $, undefined ) {

$.widget( "ui.progressbar", {
  version: "1.10.4",
  options: {
	max: 100,
	value: 0,

	change: null,
	complete: null
  },

  min: 0,

  _create: function() {
	// Constrain initial value
	this.oldValue = this.options.value = this._constrainedValue();

	this.element
	  .addClass( "ui-progressbar ui-widget ui-widget-content ui-corner-all" )
	  .attr({
		// Only set static values, aria-valuenow and aria-valuemax are
		// set inside _refreshValue()
		role: "progressbar",
		"aria-valuemin": this.min
	  });

	this.valueDiv = $( "<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>" )
	  .appendTo( this.element );

	this._refreshValue();
  },

  _destroy: function() {
	this.element
	  .removeClass( "ui-progressbar ui-widget ui-widget-content ui-corner-all" )
	  .removeAttr( "role" )
	  .removeAttr( "aria-valuemin" )
	  .removeAttr( "aria-valuemax" )
	  .removeAttr( "aria-valuenow" );

	this.valueDiv.remove();
  },

  value: function( newValue ) {
	if ( newValue === undefined ) {
	  return this.options.value;
	}

	this.options.value = this._constrainedValue( newValue );
	this._refreshValue();
  },

  _constrainedValue: function( newValue ) {
	if ( newValue === undefined ) {
	  newValue = this.options.value;
	}

	this.indeterminate = newValue === false;

	// sanitize value
	if ( typeof newValue !== "number" ) {
	  newValue = 0;
	}

	return this.indeterminate ? false :
	  Math.min( this.options.max, Math.max( this.min, newValue ) );
  },

  _setOptions: function( options ) {
	// Ensure "value" option is set after other values (like max)
	var value = options.value;
	delete options.value;

	this._super( options );

	this.options.value = this._constrainedValue( value );
	this._refreshValue();
  },

  _setOption: function( key, value ) {
	if ( key === "max" ) {
	  // Don't allow a max less than min
	  value = Math.max( this.min, value );
	}

	this._super( key, value );
  },

  _percentage: function() {
	return this.indeterminate ? 100 : 100 * ( this.options.value - this.min ) / ( this.options.max - this.min );
  },

  _refreshValue: function() {
	var value = this.options.value,
	  percentage = this._percentage();

	this.valueDiv
	  .toggle( this.indeterminate || value > this.min )
	  .toggleClass( "ui-corner-right", value === this.options.max )
	  .width( percentage.toFixed(0) + "%" );

	this.element.toggleClass( "ui-progressbar-indeterminate", this.indeterminate );

	if ( this.indeterminate ) {
	  this.element.removeAttr( "aria-valuenow" );
	  if ( !this.overlayDiv ) {
		this.overlayDiv = $( "<div class='ui-progressbar-overlay'></div>" ).appendTo( this.valueDiv );
	  }
	} else {
	  this.element.attr({
		"aria-valuemax": this.options.max,
		"aria-valuenow": value
	  });
	  if ( this.overlayDiv ) {
		this.overlayDiv.remove();
		this.overlayDiv = null;
	  }
	}

	if ( this.oldValue !== value ) {
	  this.oldValue = value;
	  this._trigger( "change" );
	}
	if ( value === this.options.max ) {
	  this._trigger( "complete" );
	}
  }
});

})( jQuery );
(function( $, undefined ) {

// number of pages in a slider
// (how many times can you page up/down to go through the whole range)
var numPages = 5;

$.widget( "ui.slider", $.ui.mouse, {
  version: "1.10.4",
  widgetEventPrefix: "slide",

  options: {
	animate: false,
	distance: 0,
	max: 100,
	min: 0,
	orientation: "horizontal",
	range: false,
	step: 1,
	value: 0,
	values: null,

	// callbacks
	change: null,
	slide: null,
	start: null,
	stop: null
  },

  _create: function() {
	this._keySliding = false;
	this._mouseSliding = false;
	this._animateOff = true;
	this._handleIndex = null;
	this._detectOrientation();
	this._mouseInit();

	this.element
	  .addClass( "ui-slider" +
		" ui-slider-" + this.orientation +
		" ui-widget" +
		" ui-widget-content" +
		" ui-corner-all");

	this._refresh();
	this._setOption( "disabled", this.options.disabled );

	this._animateOff = false;
  },

  _refresh: function() {
	this._createRange();
	this._createHandles();
	this._setupEvents();
	this._refreshValue();
  },

  _createHandles: function() {
	var i, handleCount,
	  options = this.options,
	  existingHandles = this.element.find( ".ui-slider-handle" ).addClass( "ui-state-default ui-corner-all" ),
	  handle = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",
	  handles = [];

	handleCount = ( options.values && options.values.length ) || 1;

	if ( existingHandles.length > handleCount ) {
	  existingHandles.slice( handleCount ).remove();
	  existingHandles = existingHandles.slice( 0, handleCount );
	}

	for ( i = existingHandles.length; i < handleCount; i++ ) {
	  handles.push( handle );
	}

	this.handles = existingHandles.add( $( handles.join( "" ) ).appendTo( this.element ) );

	this.handle = this.handles.eq( 0 );

	this.handles.each(function( i ) {
	  $( this ).data( "ui-slider-handle-index", i );
	});
  },

  _createRange: function() {
	var options = this.options,
	  classes = "";

	if ( options.range ) {
	  if ( options.range === true ) {
		if ( !options.values ) {
		  options.values = [ this._valueMin(), this._valueMin() ];
		} else if ( options.values.length && options.values.length !== 2 ) {
		  options.values = [ options.values[0], options.values[0] ];
		} else if ( $.isArray( options.values ) ) {
		  options.values = options.values.slice(0);
		}
	  }

	  if ( !this.range || !this.range.length ) {
		this.range = $( "<div></div>" )
		  .appendTo( this.element );

		classes = "ui-slider-range" +
		// note: this isn't the most fittingly semantic framework class for this element,
		// but worked best visually with a variety of themes
		" ui-widget-header ui-corner-all";
	  } else {
		this.range.removeClass( "ui-slider-range-min ui-slider-range-max" )
		  // Handle range switching from true to min/max
		  .css({
			"left": "",
			"bottom": ""
		  });
	  }

	  this.range.addClass( classes +
		( ( options.range === "min" || options.range === "max" ) ? " ui-slider-range-" + options.range : "" ) );
	} else {
	  if ( this.range ) {
		this.range.remove();
	  }
	  this.range = null;
	}
  },

  _setupEvents: function() {
	var elements = this.handles.add( this.range ).filter( "a" );
	this._off( elements );
	this._on( elements, this._handleEvents );
	this._hoverable( elements );
	this._focusable( elements );
  },

  _destroy: function() {
	this.handles.remove();
	if ( this.range ) {
	  this.range.remove();
	}

	this.element
	  .removeClass( "ui-slider" +
		" ui-slider-horizontal" +
		" ui-slider-vertical" +
		" ui-widget" +
		" ui-widget-content" +
		" ui-corner-all" );

	this._mouseDestroy();
  },

  _mouseCapture: function( event ) {
	var position, normValue, distance, closestHandle, index, allowed, offset, mouseOverHandle,
	  that = this,
	  o = this.options;

	if ( o.disabled ) {
	  return false;
	}

	this.elementSize = {
	  width: this.element.outerWidth(),
	  height: this.element.outerHeight()
	};
	this.elementOffset = this.element.offset();

	position = { x: event.pageX, y: event.pageY };
	normValue = this._normValueFromMouse( position );
	distance = this._valueMax() - this._valueMin() + 1;
	this.handles.each(function( i ) {
	  var thisDistance = Math.abs( normValue - that.values(i) );
	  if (( distance > thisDistance ) ||
		( distance === thisDistance &&
		  (i === that._lastChangedValue || that.values(i) === o.min ))) {
		distance = thisDistance;
		closestHandle = $( this );
		index = i;
	  }
	});

	allowed = this._start( event, index );
	if ( allowed === false ) {
	  return false;
	}
	this._mouseSliding = true;

	this._handleIndex = index;

	closestHandle
	  .addClass( "ui-state-active" )
	  .focus();

	offset = closestHandle.offset();
	mouseOverHandle = !$( event.target ).parents().addBack().is( ".ui-slider-handle" );
	this._clickOffset = mouseOverHandle ? { left: 0, top: 0 } : {
	  left: event.pageX - offset.left - ( closestHandle.width() / 2 ),
	  top: event.pageY - offset.top -
		( closestHandle.height() / 2 ) -
		( parseInt( closestHandle.css("borderTopWidth"), 10 ) || 0 ) -
		( parseInt( closestHandle.css("borderBottomWidth"), 10 ) || 0) +
		( parseInt( closestHandle.css("marginTop"), 10 ) || 0)
	};

	if ( !this.handles.hasClass( "ui-state-hover" ) ) {
	  this._slide( event, index, normValue );
	}
	this._animateOff = true;
	return true;
  },

  _mouseStart: function() {
	return true;
  },

  _mouseDrag: function( event ) {
	var position = { x: event.pageX, y: event.pageY },
	  normValue = this._normValueFromMouse( position );

	this._slide( event, this._handleIndex, normValue );

	return false;
  },

  _mouseStop: function( event ) {
	this.handles.removeClass( "ui-state-active" );
	this._mouseSliding = false;

	this._stop( event, this._handleIndex );
	this._change( event, this._handleIndex );

	this._handleIndex = null;
	this._clickOffset = null;
	this._animateOff = false;

	return false;
  },

  _detectOrientation: function() {
	this.orientation = ( this.options.orientation === "vertical" ) ? "vertical" : "horizontal";
  },

  _normValueFromMouse: function( position ) {
	var pixelTotal,
	  pixelMouse,
	  percentMouse,
	  valueTotal,
	  valueMouse;

	if ( this.orientation === "horizontal" ) {
	  pixelTotal = this.elementSize.width;
	  pixelMouse = position.x - this.elementOffset.left - ( this._clickOffset ? this._clickOffset.left : 0 );
	} else {
	  pixelTotal = this.elementSize.height;
	  pixelMouse = position.y - this.elementOffset.top - ( this._clickOffset ? this._clickOffset.top : 0 );
	}

	percentMouse = ( pixelMouse / pixelTotal );
	if ( percentMouse > 1 ) {
	  percentMouse = 1;
	}
	if ( percentMouse < 0 ) {
	  percentMouse = 0;
	}
	if ( this.orientation === "vertical" ) {
	  percentMouse = 1 - percentMouse;
	}

	valueTotal = this._valueMax() - this._valueMin();
	valueMouse = this._valueMin() + percentMouse * valueTotal;

	return this._trimAlignValue( valueMouse );
  },

  _start: function( event, index ) {
	var uiHash = {
	  handle: this.handles[ index ],
	  value: this.value()
	};
	if ( this.options.values && this.options.values.length ) {
	  uiHash.value = this.values( index );
	  uiHash.values = this.values();
	}
	return this._trigger( "start", event, uiHash );
  },

  _slide: function( event, index, newVal ) {
	var otherVal,
	  newValues,
	  allowed;

	if ( this.options.values && this.options.values.length ) {
	  otherVal = this.values( index ? 0 : 1 );

	  if ( ( this.options.values.length === 2 && this.options.range === true ) &&
		  ( ( index === 0 && newVal > otherVal) || ( index === 1 && newVal < otherVal ) )
		) {
		newVal = otherVal;
	  }

	  if ( newVal !== this.values( index ) ) {
		newValues = this.values();
		newValues[ index ] = newVal;
		// A slide can be canceled by returning false from the slide callback
		allowed = this._trigger( "slide", event, {
		  handle: this.handles[ index ],
		  value: newVal,
		  values: newValues
		} );
		otherVal = this.values( index ? 0 : 1 );
		if ( allowed !== false ) {
		  this.values( index, newVal );
		}
	  }
	} else {
	  if ( newVal !== this.value() ) {
		// A slide can be canceled by returning false from the slide callback
		allowed = this._trigger( "slide", event, {
		  handle: this.handles[ index ],
		  value: newVal
		} );
		if ( allowed !== false ) {
		  this.value( newVal );
		}
	  }
	}
  },

  _stop: function( event, index ) {
	var uiHash = {
	  handle: this.handles[ index ],
	  value: this.value()
	};
	if ( this.options.values && this.options.values.length ) {
	  uiHash.value = this.values( index );
	  uiHash.values = this.values();
	}

	this._trigger( "stop", event, uiHash );
  },

  _change: function( event, index ) {
	if ( !this._keySliding && !this._mouseSliding ) {
	  var uiHash = {
		handle: this.handles[ index ],
		value: this.value()
	  };
	  if ( this.options.values && this.options.values.length ) {
		uiHash.value = this.values( index );
		uiHash.values = this.values();
	  }

	  //store the last changed value index for reference when handles overlap
	  this._lastChangedValue = index;

	  this._trigger( "change", event, uiHash );
	}
  },

  value: function( newValue ) {
	if ( arguments.length ) {
	  this.options.value = this._trimAlignValue( newValue );
	  this._refreshValue();
	  this._change( null, 0 );
	  return;
	}

	return this._value();
  },

  values: function( index, newValue ) {
	var vals,
	  newValues,
	  i;

	if ( arguments.length > 1 ) {
	  this.options.values[ index ] = this._trimAlignValue( newValue );
	  this._refreshValue();
	  this._change( null, index );
	  return;
	}

	if ( arguments.length ) {
	  if ( $.isArray( arguments[ 0 ] ) ) {
		vals = this.options.values;
		newValues = arguments[ 0 ];
		for ( i = 0; i < vals.length; i += 1 ) {
		  vals[ i ] = this._trimAlignValue( newValues[ i ] );
		  this._change( null, i );
		}
		this._refreshValue();
	  } else {
		if ( this.options.values && this.options.values.length ) {
		  return this._values( index );
		} else {
		  return this.value();
		}
	  }
	} else {
	  return this._values();
	}
  },

  _setOption: function( key, value ) {
	var i,
	  valsLength = 0;

	if ( key === "range" && this.options.range === true ) {
	  if ( value === "min" ) {
		this.options.value = this._values( 0 );
		this.options.values = null;
	  } else if ( value === "max" ) {
		this.options.value = this._values( this.options.values.length-1 );
		this.options.values = null;
	  }
	}

	if ( $.isArray( this.options.values ) ) {
	  valsLength = this.options.values.length;
	}

	$.Widget.prototype._setOption.apply( this, arguments );

	switch ( key ) {
	  case "orientation":
		this._detectOrientation();
		this.element
		  .removeClass( "ui-slider-horizontal ui-slider-vertical" )
		  .addClass( "ui-slider-" + this.orientation );
		this._refreshValue();
		break;
	  case "value":
		this._animateOff = true;
		this._refreshValue();
		this._change( null, 0 );
		this._animateOff = false;
		break;
	  case "values":
		this._animateOff = true;
		this._refreshValue();
		for ( i = 0; i < valsLength; i += 1 ) {
		  this._change( null, i );
		}
		this._animateOff = false;
		break;
	  case "min":
	  case "max":
		this._animateOff = true;
		this._refreshValue();
		this._animateOff = false;
		break;
	  case "range":
		this._animateOff = true;
		this._refresh();
		this._animateOff = false;
		break;
	}
  },

  //internal value getter
  // _value() returns value trimmed by min and max, aligned by step
  _value: function() {
	var val = this.options.value;
	val = this._trimAlignValue( val );

	return val;
  },

  //internal values getter
  // _values() returns array of values trimmed by min and max, aligned by step
  // _values( index ) returns single value trimmed by min and max, aligned by step
  _values: function( index ) {
	var val,
	  vals,
	  i;

	if ( arguments.length ) {
	  val = this.options.values[ index ];
	  val = this._trimAlignValue( val );

	  return val;
	} else if ( this.options.values && this.options.values.length ) {
	  // .slice() creates a copy of the array
	  // this copy gets trimmed by min and max and then returned
	  vals = this.options.values.slice();
	  for ( i = 0; i < vals.length; i+= 1) {
		vals[ i ] = this._trimAlignValue( vals[ i ] );
	  }

	  return vals;
	} else {
	  return [];
	}
  },

  // returns the step-aligned value that val is closest to, between (inclusive) min and max
  _trimAlignValue: function( val ) {
	if ( val <= this._valueMin() ) {
	  return this._valueMin();
	}
	if ( val >= this._valueMax() ) {
	  return this._valueMax();
	}
	var step = ( this.options.step > 0 ) ? this.options.step : 1,
	  valModStep = (val - this._valueMin()) % step,
	  alignValue = val - valModStep;

	if ( Math.abs(valModStep) * 2 >= step ) {
	  alignValue += ( valModStep > 0 ) ? step : ( -step );
	}

	// Since JavaScript has problems with large floats, round
	// the final value to 5 digits after the decimal point (see #4124)
	return parseFloat( alignValue.toFixed(5) );
  },

  _valueMin: function() {
	return this.options.min;
  },

  _valueMax: function() {
	return this.options.max;
  },

  _refreshValue: function() {
	var lastValPercent, valPercent, value, valueMin, valueMax,
	  oRange = this.options.range,
	  o = this.options,
	  that = this,
	  animate = ( !this._animateOff ) ? o.animate : false,
	  _set = {};

	if ( this.options.values && this.options.values.length ) {
	  this.handles.each(function( i ) {
		valPercent = ( that.values(i) - that._valueMin() ) / ( that._valueMax() - that._valueMin() ) * 100;
		_set[ that.orientation === "horizontal" ? "left" : "bottom" ] = valPercent + "%";
		$( this ).stop( 1, 1 )[ animate ? "animate" : "css" ]( _set, o.animate );
		if ( that.options.range === true ) {
		  if ( that.orientation === "horizontal" ) {
			if ( i === 0 ) {
			  that.range.stop( 1, 1 )[ animate ? "animate" : "css" ]( { left: valPercent + "%" }, o.animate );
			}
			if ( i === 1 ) {
			  that.range[ animate ? "animate" : "css" ]( { width: ( valPercent - lastValPercent ) + "%" }, { queue: false, duration: o.animate } );
			}
		  } else {
			if ( i === 0 ) {
			  that.range.stop( 1, 1 )[ animate ? "animate" : "css" ]( { bottom: ( valPercent ) + "%" }, o.animate );
			}
			if ( i === 1 ) {
			  that.range[ animate ? "animate" : "css" ]( { height: ( valPercent - lastValPercent ) + "%" }, { queue: false, duration: o.animate } );
			}
		  }
		}
		lastValPercent = valPercent;
	  });
	} else {
	  value = this.value();
	  valueMin = this._valueMin();
	  valueMax = this._valueMax();
	  valPercent = ( valueMax !== valueMin ) ?
		  ( value - valueMin ) / ( valueMax - valueMin ) * 100 :
		  0;
	  _set[ this.orientation === "horizontal" ? "left" : "bottom" ] = valPercent + "%";
	  this.handle.stop( 1, 1 )[ animate ? "animate" : "css" ]( _set, o.animate );

	  if ( oRange === "min" && this.orientation === "horizontal" ) {
		this.range.stop( 1, 1 )[ animate ? "animate" : "css" ]( { width: valPercent + "%" }, o.animate );
	  }
	  if ( oRange === "max" && this.orientation === "horizontal" ) {
		this.range[ animate ? "animate" : "css" ]( { width: ( 100 - valPercent ) + "%" }, { queue: false, duration: o.animate } );
	  }
	  if ( oRange === "min" && this.orientation === "vertical" ) {
		this.range.stop( 1, 1 )[ animate ? "animate" : "css" ]( { height: valPercent + "%" }, o.animate );
	  }
	  if ( oRange === "max" && this.orientation === "vertical" ) {
		this.range[ animate ? "animate" : "css" ]( { height: ( 100 - valPercent ) + "%" }, { queue: false, duration: o.animate } );
	  }
	}
  },

  _handleEvents: {
	keydown: function( event ) {
	  var allowed, curVal, newVal, step,
		index = $( event.target ).data( "ui-slider-handle-index" );

	  switch ( event.keyCode ) {
		case $.ui.keyCode.HOME:
		case $.ui.keyCode.END:
		case $.ui.keyCode.PAGE_UP:
		case $.ui.keyCode.PAGE_DOWN:
		case $.ui.keyCode.UP:
		case $.ui.keyCode.RIGHT:
		case $.ui.keyCode.DOWN:
		case $.ui.keyCode.LEFT:
		  event.preventDefault();
		  if ( !this._keySliding ) {
			this._keySliding = true;
			$( event.target ).addClass( "ui-state-active" );
			allowed = this._start( event, index );
			if ( allowed === false ) {
			  return;
			}
		  }
		  break;
	  }

	  step = this.options.step;
	  if ( this.options.values && this.options.values.length ) {
		curVal = newVal = this.values( index );
	  } else {
		curVal = newVal = this.value();
	  }

	  switch ( event.keyCode ) {
		case $.ui.keyCode.HOME:
		  newVal = this._valueMin();
		  break;
		case $.ui.keyCode.END:
		  newVal = this._valueMax();
		  break;
		case $.ui.keyCode.PAGE_UP:
		  newVal = this._trimAlignValue( curVal + ( (this._valueMax() - this._valueMin()) / numPages ) );
		  break;
		case $.ui.keyCode.PAGE_DOWN:
		  newVal = this._trimAlignValue( curVal - ( (this._valueMax() - this._valueMin()) / numPages ) );
		  break;
		case $.ui.keyCode.UP:
		case $.ui.keyCode.RIGHT:
		  if ( curVal === this._valueMax() ) {
			return;
		  }
		  newVal = this._trimAlignValue( curVal + step );
		  break;
		case $.ui.keyCode.DOWN:
		case $.ui.keyCode.LEFT:
		  if ( curVal === this._valueMin() ) {
			return;
		  }
		  newVal = this._trimAlignValue( curVal - step );
		  break;
	  }

	  this._slide( event, index, newVal );
	},
	click: function( event ) {
	  event.preventDefault();
	},
	keyup: function( event ) {
	  var index = $( event.target ).data( "ui-slider-handle-index" );

	  if ( this._keySliding ) {
		this._keySliding = false;
		this._stop( event, index );
		this._change( event, index );
		$( event.target ).removeClass( "ui-state-active" );
	  }
	}
  }

});

}(jQuery));
(function( $ ) {

function modifier( fn ) {
  return function() {
	var previous = this.element.val();
	fn.apply( this, arguments );
	this._refresh();
	if ( previous !== this.element.val() ) {
	  this._trigger( "change" );
	}
  };
}

$.widget( "ui.spinner", {
  version: "1.10.4",
  defaultElement: "<input>",
  widgetEventPrefix: "spin",
  options: {
	culture: null,
	icons: {
	  down: "ui-icon-triangle-1-s",
	  up: "ui-icon-triangle-1-n"
	},
	incremental: true,
	max: null,
	min: null,
	numberFormat: null,
	page: 10,
	step: 1,

	change: null,
	spin: null,
	start: null,
	stop: null
  },

  _create: function() {
	// handle string values that need to be parsed
	this._setOption( "max", this.options.max );
	this._setOption( "min", this.options.min );
	this._setOption( "step", this.options.step );

	// Only format if there is a value, prevents the field from being marked
	// as invalid in Firefox, see #9573.
	if ( this.value() !== "" ) {
	  // Format the value, but don't constrain.
	  this._value( this.element.val(), true );
	}

	this._draw();
	this._on( this._events );
	this._refresh();

	// turning off autocomplete prevents the browser from remembering the
	// value when navigating through history, so we re-enable autocomplete
	// if the page is unloaded before the widget is destroyed. #7790
	this._on( this.window, {
	  beforeunload: function() {
		this.element.removeAttr( "autocomplete" );
	  }
	});
  },

  _getCreateOptions: function() {
	var options = {},
	  element = this.element;

	$.each( [ "min", "max", "step" ], function( i, option ) {
	  var value = element.attr( option );
	  if ( value !== undefined && value.length ) {
		options[ option ] = value;
	  }
	});

	return options;
  },

  _events: {
	keydown: function( event ) {
	  if ( this._start( event ) && this._keydown( event ) ) {
		event.preventDefault();
	  }
	},
	keyup: "_stop",
	focus: function() {
	  this.previous = this.element.val();
	},
	blur: function( event ) {
	  if ( this.cancelBlur ) {
		delete this.cancelBlur;
		return;
	  }

	  this._stop();
	  this._refresh();
	  if ( this.previous !== this.element.val() ) {
		this._trigger( "change", event );
	  }
	},
	mousewheel: function( event, delta ) {
	  if ( !delta ) {
		return;
	  }
	  if ( !this.spinning && !this._start( event ) ) {
		return false;
	  }

	  this._spin( (delta > 0 ? 1 : -1) * this.options.step, event );
	  clearTimeout( this.mousewheelTimer );
	  this.mousewheelTimer = this._delay(function() {
		if ( this.spinning ) {
		  this._stop( event );
		}
	  }, 100 );
	  event.preventDefault();
	},
	"mousedown .ui-spinner-button": function( event ) {
	  var previous;

	  // We never want the buttons to have focus; whenever the user is
	  // interacting with the spinner, the focus should be on the input.
	  // If the input is focused then this.previous is properly set from
	  // when the input first received focus. If the input is not focused
	  // then we need to set this.previous based on the value before spinning.
	  previous = this.element[0] === this.document[0].activeElement ?
		this.previous : this.element.val();
	  function checkFocus() {
		var isActive = this.element[0] === this.document[0].activeElement;
		if ( !isActive ) {
		  this.element.focus();
		  this.previous = previous;
		  // support: IE
		  // IE sets focus asynchronously, so we need to check if focus
		  // moved off of the input because the user clicked on the button.
		  this._delay(function() {
			this.previous = previous;
		  });
		}
	  }

	  // ensure focus is on (or stays on) the text field
	  event.preventDefault();
	  checkFocus.call( this );

	  // support: IE
	  // IE doesn't prevent moving focus even with event.preventDefault()
	  // so we set a flag to know when we should ignore the blur event
	  // and check (again) if focus moved off of the input.
	  this.cancelBlur = true;
	  this._delay(function() {
		delete this.cancelBlur;
		checkFocus.call( this );
	  });

	  if ( this._start( event ) === false ) {
		return;
	  }

	  this._repeat( null, $( event.currentTarget ).hasClass( "ui-spinner-up" ) ? 1 : -1, event );
	},
	"mouseup .ui-spinner-button": "_stop",
	"mouseenter .ui-spinner-button": function( event ) {
	  // button will add ui-state-active if mouse was down while mouseleave and kept down
	  if ( !$( event.currentTarget ).hasClass( "ui-state-active" ) ) {
		return;
	  }

	  if ( this._start( event ) === false ) {
		return false;
	  }
	  this._repeat( null, $( event.currentTarget ).hasClass( "ui-spinner-up" ) ? 1 : -1, event );
	},
	// TODO: do we really want to consider this a stop?
	// shouldn't we just stop the repeater and wait until mouseup before
	// we trigger the stop event?
	"mouseleave .ui-spinner-button": "_stop"
  },

  _draw: function() {
	var uiSpinner = this.uiSpinner = this.element
	  .addClass( "ui-spinner-input" )
	  .attr( "autocomplete", "off" )
	  .wrap( this._uiSpinnerHtml() )
	  .parent()
		// add buttons
		.append( this._buttonHtml() );

	this.element.attr( "role", "spinbutton" );

	// button bindings
	this.buttons = uiSpinner.find( ".ui-spinner-button" )
	  .attr( "tabIndex", -1 )
	  .button()
	  .removeClass( "ui-corner-all" );

	// IE 6 doesn't understand height: 50% for the buttons
	// unless the wrapper has an explicit height
	if ( this.buttons.height() > Math.ceil( uiSpinner.height() * 0.5 ) &&
		uiSpinner.height() > 0 ) {
	  uiSpinner.height( uiSpinner.height() );
	}

	// disable spinner if element was already disabled
	if ( this.options.disabled ) {
	  this.disable();
	}
  },

  _keydown: function( event ) {
	var options = this.options,
	  keyCode = $.ui.keyCode;

	switch ( event.keyCode ) {
	case keyCode.UP:
	  this._repeat( null, 1, event );
	  return true;
	case keyCode.DOWN:
	  this._repeat( null, -1, event );
	  return true;
	case keyCode.PAGE_UP:
	  this._repeat( null, options.page, event );
	  return true;
	case keyCode.PAGE_DOWN:
	  this._repeat( null, -options.page, event );
	  return true;
	}

	return false;
  },

  _uiSpinnerHtml: function() {
	return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>";
  },

  _buttonHtml: function() {
	return "" +
	  "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'>" +
		"<span class='ui-icon " + this.options.icons.up + "'>&#9650;</span>" +
	  "</a>" +
	  "<a class='ui-spinner-button ui-spinner-down ui-corner-br'>" +
		"<span class='ui-icon " + this.options.icons.down + "'>&#9660;</span>" +
	  "</a>";
  },

  _start: function( event ) {
	if ( !this.spinning && this._trigger( "start", event ) === false ) {
	  return false;
	}

	if ( !this.counter ) {
	  this.counter = 1;
	}
	this.spinning = true;
	return true;
  },

  _repeat: function( i, steps, event ) {
	i = i || 500;

	clearTimeout( this.timer );
	this.timer = this._delay(function() {
	  this._repeat( 40, steps, event );
	}, i );

	this._spin( steps * this.options.step, event );
  },

  _spin: function( step, event ) {
	var value = this.value() || 0;

	if ( !this.counter ) {
	  this.counter = 1;
	}

	value = this._adjustValue( value + step * this._increment( this.counter ) );

	if ( !this.spinning || this._trigger( "spin", event, { value: value } ) !== false) {
	  this._value( value );
	  this.counter++;
	}
  },

  _increment: function( i ) {
	var incremental = this.options.incremental;

	if ( incremental ) {
	  return $.isFunction( incremental ) ?
		incremental( i ) :
		Math.floor( i*i*i/50000 - i*i/500 + 17*i/200 + 1 );
	}

	return 1;
  },

  _precision: function() {
	var precision = this._precisionOf( this.options.step );
	if ( this.options.min !== null ) {
	  precision = Math.max( precision, this._precisionOf( this.options.min ) );
	}
	return precision;
  },

  _precisionOf: function( num ) {
	var str = num.toString(),
	  decimal = str.indexOf( "." );
	return decimal === -1 ? 0 : str.length - decimal - 1;
  },

  _adjustValue: function( value ) {
	var base, aboveMin,
	  options = this.options;

	// make sure we're at a valid step
	// - find out where we are relative to the base (min or 0)
	base = options.min !== null ? options.min : 0;
	aboveMin = value - base;
	// - round to the nearest step
	aboveMin = Math.round(aboveMin / options.step) * options.step;
	// - rounding is based on 0, so adjust back to our base
	value = base + aboveMin;

	// fix precision from bad JS floating point math
	value = parseFloat( value.toFixed( this._precision() ) );

	// clamp the value
	if ( options.max !== null && value > options.max) {
	  return options.max;
	}
	if ( options.min !== null && value < options.min ) {
	  return options.min;
	}

	return value;
  },

  _stop: function( event ) {
	if ( !this.spinning ) {
	  return;
	}

	clearTimeout( this.timer );
	clearTimeout( this.mousewheelTimer );
	this.counter = 0;
	this.spinning = false;
	this._trigger( "stop", event );
  },

  _setOption: function( key, value ) {
	if ( key === "culture" || key === "numberFormat" ) {
	  var prevValue = this._parse( this.element.val() );
	  this.options[ key ] = value;
	  this.element.val( this._format( prevValue ) );
	  return;
	}

	if ( key === "max" || key === "min" || key === "step" ) {
	  if ( typeof value === "string" ) {
		value = this._parse( value );
	  }
	}
	if ( key === "icons" ) {
	  this.buttons.first().find( ".ui-icon" )
		.removeClass( this.options.icons.up )
		.addClass( value.up );
	  this.buttons.last().find( ".ui-icon" )
		.removeClass( this.options.icons.down )
		.addClass( value.down );
	}

	this._super( key, value );

	if ( key === "disabled" ) {
	  if ( value ) {
		this.element.prop( "disabled", true );
		this.buttons.button( "disable" );
	  } else {
		this.element.prop( "disabled", false );
		this.buttons.button( "enable" );
	  }
	}
  },

  _setOptions: modifier(function( options ) {
	this._super( options );
	this._value( this.element.val() );
  }),

  _parse: function( val ) {
	if ( typeof val === "string" && val !== "" ) {
	  val = window.Globalize && this.options.numberFormat ?
		Globalize.parseFloat( val, 10, this.options.culture ) : +val;
	}
	return val === "" || isNaN( val ) ? null : val;
  },

  _format: function( value ) {
	if ( value === "" ) {
	  return "";
	}
	return window.Globalize && this.options.numberFormat ?
	  Globalize.format( value, this.options.numberFormat, this.options.culture ) :
	  value;
  },

  _refresh: function() {
	this.element.attr({
	  "aria-valuemin": this.options.min,
	  "aria-valuemax": this.options.max,
	  // TODO: what should we do with values that can't be parsed?
	  "aria-valuenow": this._parse( this.element.val() )
	});
  },

  // update the value without triggering change
  _value: function( value, allowAny ) {
	var parsed;
	if ( value !== "" ) {
	  parsed = this._parse( value );
	  if ( parsed !== null ) {
		if ( !allowAny ) {
		  parsed = this._adjustValue( parsed );
		}
		value = this._format( parsed );
	  }
	}
	this.element.val( value );
	this._refresh();
  },

  _destroy: function() {
	this.element
	  .removeClass( "ui-spinner-input" )
	  .prop( "disabled", false )
	  .removeAttr( "autocomplete" )
	  .removeAttr( "role" )
	  .removeAttr( "aria-valuemin" )
	  .removeAttr( "aria-valuemax" )
	  .removeAttr( "aria-valuenow" );
	this.uiSpinner.replaceWith( this.element );
  },

  stepUp: modifier(function( steps ) {
	this._stepUp( steps );
  }),
  _stepUp: function( steps ) {
	if ( this._start() ) {
	  this._spin( (steps || 1) * this.options.step );
	  this._stop();
	}
  },

  stepDown: modifier(function( steps ) {
	this._stepDown( steps );
  }),
  _stepDown: function( steps ) {
	if ( this._start() ) {
	  this._spin( (steps || 1) * -this.options.step );
	  this._stop();
	}
  },

  pageUp: modifier(function( pages ) {
	this._stepUp( (pages || 1) * this.options.page );
  }),

  pageDown: modifier(function( pages ) {
	this._stepDown( (pages || 1) * this.options.page );
  }),

  value: function( newVal ) {
	if ( !arguments.length ) {
	  return this._parse( this.element.val() );
	}
	modifier( this._value ).call( this, newVal );
  },

  widget: function() {
	return this.uiSpinner;
  }
});

}( jQuery ) );
(function( $, undefined ) {

var tabId = 0,
  rhash = /#.*$/;

function getNextTabId() {
  return ++tabId;
}

function isLocal( anchor ) {
  // support: IE7
  // IE7 doesn't normalize the href property when set via script (#9317)
  anchor = anchor.cloneNode( false );

  return anchor.hash.length > 1 &&
	decodeURIComponent( anchor.href.replace( rhash, "" ) ) ===
	  decodeURIComponent( location.href.replace( rhash, "" ) );
}

$.widget( "ui.tabs", {
  version: "1.10.4",
  delay: 300,
  options: {
	active: null,
	collapsible: false,
	event: "click",
	heightStyle: "content",
	hide: null,
	show: null,

	// callbacks
	activate: null,
	beforeActivate: null,
	beforeLoad: null,
	load: null
  },

  _create: function() {
	var that = this,
	  options = this.options;

	this.running = false;

	this.element
	  .addClass( "ui-tabs ui-widget ui-widget-content ui-corner-all" )
	  .toggleClass( "ui-tabs-collapsible", options.collapsible )
	  // Prevent users from focusing disabled tabs via click
	  .delegate( ".ui-tabs-nav > li", "mousedown" + this.eventNamespace, function( event ) {
		if ( $( this ).is( ".ui-state-disabled" ) ) {
		  event.preventDefault();
		}
	  })
	  // support: IE <9
	  // Preventing the default action in mousedown doesn't prevent IE
	  // from focusing the element, so if the anchor gets focused, blur.
	  // We don't have to worry about focusing the previously focused
	  // element since clicking on a non-focusable element should focus
	  // the body anyway.
	  .delegate( ".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
		if ( $( this ).closest( "li" ).is( ".ui-state-disabled" ) ) {
		  this.blur();
		}
	  });

	this._processTabs();
	options.active = this._initialActive();

	// Take disabling tabs via class attribute from HTML
	// into account and update option properly.
	if ( $.isArray( options.disabled ) ) {
	  options.disabled = $.unique( options.disabled.concat(
		$.map( this.tabs.filter( ".ui-state-disabled" ), function( li ) {
		  return that.tabs.index( li );
		})
	  ) ).sort();
	}

	// check for length avoids error when initializing empty list
	if ( this.options.active !== false && this.anchors.length ) {
	  this.active = this._findActive( options.active );
	} else {
	  this.active = $();
	}

	this._refresh();

	if ( this.active.length ) {
	  this.load( options.active );
	}
  },

  _initialActive: function() {
	var active = this.options.active,
	  collapsible = this.options.collapsible,
	  locationHash = location.hash.substring( 1 );

	if ( active === null ) {
	  // check the fragment identifier in the URL
	  if ( locationHash ) {
		this.tabs.each(function( i, tab ) {
		  if ( $( tab ).attr( "aria-controls" ) === locationHash ) {
			active = i;
			return false;
		  }
		});
	  }

	  // check for a tab marked active via a class
	  if ( active === null ) {
		active = this.tabs.index( this.tabs.filter( ".ui-tabs-active" ) );
	  }

	  // no active tab, set to false
	  if ( active === null || active === -1 ) {
		active = this.tabs.length ? 0 : false;
	  }
	}

	// handle numbers: negative, out of range
	if ( active !== false ) {
	  active = this.tabs.index( this.tabs.eq( active ) );
	  if ( active === -1 ) {
		active = collapsible ? false : 0;
	  }
	}

	// don't allow collapsible: false and active: false
	if ( !collapsible && active === false && this.anchors.length ) {
	  active = 0;
	}

	return active;
  },

  _getCreateEventData: function() {
	return {
	  tab: this.active,
	  panel: !this.active.length ? $() : this._getPanelForTab( this.active )
	};
  },

  _tabKeydown: function( event ) {
	var focusedTab = $( this.document[0].activeElement ).closest( "li" ),
	  selectedIndex = this.tabs.index( focusedTab ),
	  goingForward = true;

	if ( this._handlePageNav( event ) ) {
	  return;
	}

	switch ( event.keyCode ) {
	  case $.ui.keyCode.RIGHT:
	  case $.ui.keyCode.DOWN:
		selectedIndex++;
		break;
	  case $.ui.keyCode.UP:
	  case $.ui.keyCode.LEFT:
		goingForward = false;
		selectedIndex--;
		break;
	  case $.ui.keyCode.END:
		selectedIndex = this.anchors.length - 1;
		break;
	  case $.ui.keyCode.HOME:
		selectedIndex = 0;
		break;
	  case $.ui.keyCode.SPACE:
		// Activate only, no collapsing
		event.preventDefault();
		clearTimeout( this.activating );
		this._activate( selectedIndex );
		return;
	  case $.ui.keyCode.ENTER:
		// Toggle (cancel delayed activation, allow collapsing)
		event.preventDefault();
		clearTimeout( this.activating );
		// Determine if we should collapse or activate
		this._activate( selectedIndex === this.options.active ? false : selectedIndex );
		return;
	  default:
		return;
	}

	// Focus the appropriate tab, based on which key was pressed
	event.preventDefault();
	clearTimeout( this.activating );
	selectedIndex = this._focusNextTab( selectedIndex, goingForward );

	// Navigating with control key will prevent automatic activation
	if ( !event.ctrlKey ) {
	  // Update aria-selected immediately so that AT think the tab is already selected.
	  // Otherwise AT may confuse the user by stating that they need to activate the tab,
	  // but the tab will already be activated by the time the announcement finishes.
	  focusedTab.attr( "aria-selected", "false" );
	  this.tabs.eq( selectedIndex ).attr( "aria-selected", "true" );

	  this.activating = this._delay(function() {
		this.option( "active", selectedIndex );
	  }, this.delay );
	}
  },

  _panelKeydown: function( event ) {
	if ( this._handlePageNav( event ) ) {
	  return;
	}

	// Ctrl+up moves focus to the current tab
	if ( event.ctrlKey && event.keyCode === $.ui.keyCode.UP ) {
	  event.preventDefault();
	  this.active.focus();
	}
  },

  // Alt+page up/down moves focus to the previous/next tab (and activates)
  _handlePageNav: function( event ) {
	if ( event.altKey && event.keyCode === $.ui.keyCode.PAGE_UP ) {
	  this._activate( this._focusNextTab( this.options.active - 1, false ) );
	  return true;
	}
	if ( event.altKey && event.keyCode === $.ui.keyCode.PAGE_DOWN ) {
	  this._activate( this._focusNextTab( this.options.active + 1, true ) );
	  return true;
	}
  },

  _findNextTab: function( index, goingForward ) {
	var lastTabIndex = this.tabs.length - 1;

	function constrain() {
	  if ( index > lastTabIndex ) {
		index = 0;
	  }
	  if ( index < 0 ) {
		index = lastTabIndex;
	  }
	  return index;
	}

	while ( $.inArray( constrain(), this.options.disabled ) !== -1 ) {
	  index = goingForward ? index + 1 : index - 1;
	}

	return index;
  },

  _focusNextTab: function( index, goingForward ) {
	index = this._findNextTab( index, goingForward );
	this.tabs.eq( index ).focus();
	return index;
  },

  _setOption: function( key, value ) {
	if ( key === "active" ) {
	  // _activate() will handle invalid values and update this.options
	  this._activate( value );
	  return;
	}

	if ( key === "disabled" ) {
	  // don't use the widget factory's disabled handling
	  this._setupDisabled( value );
	  return;
	}

	this._super( key, value);

	if ( key === "collapsible" ) {
	  this.element.toggleClass( "ui-tabs-collapsible", value );
	  // Setting collapsible: false while collapsed; open first panel
	  if ( !value && this.options.active === false ) {
		this._activate( 0 );
	  }
	}

	if ( key === "event" ) {
	  this._setupEvents( value );
	}

	if ( key === "heightStyle" ) {
	  this._setupHeightStyle( value );
	}
  },

  _tabId: function( tab ) {
	return tab.attr( "aria-controls" ) || "ui-tabs-" + getNextTabId();
  },

  _sanitizeSelector: function( hash ) {
	return hash ? hash.replace( /[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&" ) : "";
  },

  refresh: function() {
	var options = this.options,
	  lis = this.tablist.children( ":has(a[href])" );

	// get disabled tabs from class attribute from HTML
	// this will get converted to a boolean if needed in _refresh()
	options.disabled = $.map( lis.filter( ".ui-state-disabled" ), function( tab ) {
	  return lis.index( tab );
	});

	this._processTabs();

	// was collapsed or no tabs
	if ( options.active === false || !this.anchors.length ) {
	  options.active = false;
	  this.active = $();
	// was active, but active tab is gone
	} else if ( this.active.length && !$.contains( this.tablist[ 0 ], this.active[ 0 ] ) ) {
	  // all remaining tabs are disabled
	  if ( this.tabs.length === options.disabled.length ) {
		options.active = false;
		this.active = $();
	  // activate previous tab
	  } else {
		this._activate( this._findNextTab( Math.max( 0, options.active - 1 ), false ) );
	  }
	// was active, active tab still exists
	} else {
	  // make sure active index is correct
	  options.active = this.tabs.index( this.active );
	}

	this._refresh();
  },

  _refresh: function() {
	this._setupDisabled( this.options.disabled );
	this._setupEvents( this.options.event );
	this._setupHeightStyle( this.options.heightStyle );

	this.tabs.not( this.active ).attr({
	  "aria-selected": "false",
	  tabIndex: -1
	});
	this.panels.not( this._getPanelForTab( this.active ) )
	  .hide()
	  .attr({
		"aria-expanded": "false",
		"aria-hidden": "true"
	  });

	// Make sure one tab is in the tab order
	if ( !this.active.length ) {
	  this.tabs.eq( 0 ).attr( "tabIndex", 0 );
	} else {
	  this.active
		.addClass( "ui-tabs-active ui-state-active" )
		.attr({
		  "aria-selected": "true",
		  tabIndex: 0
		});
	  this._getPanelForTab( this.active )
		.show()
		.attr({
		  "aria-expanded": "true",
		  "aria-hidden": "false"
		});
	}
  },

  _processTabs: function() {
	var that = this;

	this.tablist = this._getList()
	  .addClass( "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" )
	  .attr( "role", "tablist" );

	this.tabs = this.tablist.find( "> li:has(a[href])" )
	  .addClass( "ui-state-default ui-corner-top" )
	  .attr({
		role: "tab",
		tabIndex: -1
	  });

	this.anchors = this.tabs.map(function() {
		return $( "a", this )[ 0 ];
	  })
	  .addClass( "ui-tabs-anchor" )
	  .attr({
		role: "presentation",
		tabIndex: -1
	  });

	this.panels = $();

	this.anchors.each(function( i, anchor ) {
	  var selector, panel, panelId,
		anchorId = $( anchor ).uniqueId().attr( "id" ),
		tab = $( anchor ).closest( "li" ),
		originalAriaControls = tab.attr( "aria-controls" );

	  // inline tab
	  if ( isLocal( anchor ) ) {
		selector = anchor.hash;
		panel = that.element.find( that._sanitizeSelector( selector ) );
	  // remote tab
	  } else {
		panelId = that._tabId( tab );
		selector = "#" + panelId;
		panel = that.element.find( selector );
		if ( !panel.length ) {
		  panel = that._createPanel( panelId );
		  panel.insertAfter( that.panels[ i - 1 ] || that.tablist );
		}
		panel.attr( "aria-live", "polite" );
	  }

	  if ( panel.length) {
		that.panels = that.panels.add( panel );
	  }
	  if ( originalAriaControls ) {
		tab.data( "ui-tabs-aria-controls", originalAriaControls );
	  }
	  tab.attr({
		"aria-controls": selector.substring( 1 ),
		"aria-labelledby": anchorId
	  });
	  panel.attr( "aria-labelledby", anchorId );
	});

	this.panels
	  .addClass( "ui-tabs-panel ui-widget-content ui-corner-bottom" )
	  .attr( "role", "tabpanel" );
  },

  // allow overriding how to find the list for rare usage scenarios (#7715)
  _getList: function() {
	return this.tablist || this.element.find( "ol,ul" ).eq( 0 );
  },

  _createPanel: function( id ) {
	return $( "<div>" )
	  .attr( "id", id )
	  .addClass( "ui-tabs-panel ui-widget-content ui-corner-bottom" )
	  .data( "ui-tabs-destroy", true );
  },

  _setupDisabled: function( disabled ) {
	if ( $.isArray( disabled ) ) {
	  if ( !disabled.length ) {
		disabled = false;
	  } else if ( disabled.length === this.anchors.length ) {
		disabled = true;
	  }
	}

	// disable tabs
	for ( var i = 0, li; ( li = this.tabs[ i ] ); i++ ) {
	  if ( disabled === true || $.inArray( i, disabled ) !== -1 ) {
		$( li )
		  .addClass( "ui-state-disabled" )
		  .attr( "aria-disabled", "true" );
	  } else {
		$( li )
		  .removeClass( "ui-state-disabled" )
		  .removeAttr( "aria-disabled" );
	  }
	}

	this.options.disabled = disabled;
  },

  _setupEvents: function( event ) {
	var events = {
	  click: function( event ) {
		event.preventDefault();
	  }
	};
	if ( event ) {
	  $.each( event.split(" "), function( index, eventName ) {
		events[ eventName ] = "_eventHandler";
	  });
	}

	this._off( this.anchors.add( this.tabs ).add( this.panels ) );
	this._on( this.anchors, events );
	this._on( this.tabs, { keydown: "_tabKeydown" } );
	this._on( this.panels, { keydown: "_panelKeydown" } );

	this._focusable( this.tabs );
	this._hoverable( this.tabs );
  },

  _setupHeightStyle: function( heightStyle ) {
	var maxHeight,
	  parent = this.element.parent();

	if ( heightStyle === "fill" ) {
	  maxHeight = parent.height();
	  maxHeight -= this.element.outerHeight() - this.element.height();

	  this.element.siblings( ":visible" ).each(function() {
		var elem = $( this ),
		  position = elem.css( "position" );

		if ( position === "absolute" || position === "fixed" ) {
		  return;
		}
		maxHeight -= elem.outerHeight( true );
	  });

	  this.element.children().not( this.panels ).each(function() {
		maxHeight -= $( this ).outerHeight( true );
	  });

	  this.panels.each(function() {
		$( this ).height( Math.max( 0, maxHeight -
		  $( this ).innerHeight() + $( this ).height() ) );
	  })
	  .css( "overflow", "auto" );
	} else if ( heightStyle === "auto" ) {
	  maxHeight = 0;
	  this.panels.each(function() {
		maxHeight = Math.max( maxHeight, $( this ).height( "" ).height() );
	  }).height( maxHeight );
	}
  },

  _eventHandler: function( event ) {
	var options = this.options,
	  active = this.active,
	  anchor = $( event.currentTarget ),
	  tab = anchor.closest( "li" ),
	  clickedIsActive = tab[ 0 ] === active[ 0 ],
	  collapsing = clickedIsActive && options.collapsible,
	  toShow = collapsing ? $() : this._getPanelForTab( tab ),
	  toHide = !active.length ? $() : this._getPanelForTab( active ),
	  eventData = {
		oldTab: active,
		oldPanel: toHide,
		newTab: collapsing ? $() : tab,
		newPanel: toShow
	  };

	event.preventDefault();

	if ( tab.hasClass( "ui-state-disabled" ) ||
		// tab is already loading
		tab.hasClass( "ui-tabs-loading" ) ||
		// can't switch durning an animation
		this.running ||
		// click on active header, but not collapsible
		( clickedIsActive && !options.collapsible ) ||
		// allow canceling activation
		( this._trigger( "beforeActivate", event, eventData ) === false ) ) {
	  return;
	}

	options.active = collapsing ? false : this.tabs.index( tab );

	this.active = clickedIsActive ? $() : tab;
	if ( this.xhr ) {
	  this.xhr.abort();
	}

	if ( !toHide.length && !toShow.length ) {
	  $.error( "jQuery UI Tabs: Mismatching fragment identifier." );
	}

	if ( toShow.length ) {
	  this.load( this.tabs.index( tab ), event );
	}
	this._toggle( event, eventData );
  },

  // handles show/hide for selecting tabs
  _toggle: function( event, eventData ) {
	var that = this,
	  toShow = eventData.newPanel,
	  toHide = eventData.oldPanel;

	this.running = true;

	function complete() {
	  that.running = false;
	  that._trigger( "activate", event, eventData );
	}

	function show() {
	  eventData.newTab.closest( "li" ).addClass( "ui-tabs-active ui-state-active" );

	  if ( toShow.length && that.options.show ) {
		that._show( toShow, that.options.show, complete );
	  } else {
		toShow.show();
		complete();
	  }
	}

	// start out by hiding, then showing, then completing
	if ( toHide.length && this.options.hide ) {
	  this._hide( toHide, this.options.hide, function() {
		eventData.oldTab.closest( "li" ).removeClass( "ui-tabs-active ui-state-active" );
		show();
	  });
	} else {
	  eventData.oldTab.closest( "li" ).removeClass( "ui-tabs-active ui-state-active" );
	  toHide.hide();
	  show();
	}

	toHide.attr({
	  "aria-expanded": "false",
	  "aria-hidden": "true"
	});
	eventData.oldTab.attr( "aria-selected", "false" );
	// If we're switching tabs, remove the old tab from the tab order.
	// If we're opening from collapsed state, remove the previous tab from the tab order.
	// If we're collapsing, then keep the collapsing tab in the tab order.
	if ( toShow.length && toHide.length ) {
	  eventData.oldTab.attr( "tabIndex", -1 );
	} else if ( toShow.length ) {
	  this.tabs.filter(function() {
		return $( this ).attr( "tabIndex" ) === 0;
	  })
	  .attr( "tabIndex", -1 );
	}

	toShow.attr({
	  "aria-expanded": "true",
	  "aria-hidden": "false"
	});
	eventData.newTab.attr({
	  "aria-selected": "true",
	  tabIndex: 0
	});
  },

  _activate: function( index ) {
	var anchor,
	  active = this._findActive( index );

	// trying to activate the already active panel
	if ( active[ 0 ] === this.active[ 0 ] ) {
	  return;
	}

	// trying to collapse, simulate a click on the current active header
	if ( !active.length ) {
	  active = this.active;
	}

	anchor = active.find( ".ui-tabs-anchor" )[ 0 ];
	this._eventHandler({
	  target: anchor,
	  currentTarget: anchor,
	  preventDefault: $.noop
	});
  },

  _findActive: function( index ) {
	return index === false ? $() : this.tabs.eq( index );
  },

  _getIndex: function( index ) {
	// meta-function to give users option to provide a href string instead of a numerical index.
	if ( typeof index === "string" ) {
	  index = this.anchors.index( this.anchors.filter( "[href$='" + index + "']" ) );
	}

	return index;
  },

  _destroy: function() {
	if ( this.xhr ) {
	  this.xhr.abort();
	}

	this.element.removeClass( "ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible" );

	this.tablist
	  .removeClass( "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" )
	  .removeAttr( "role" );

	this.anchors
	  .removeClass( "ui-tabs-anchor" )
	  .removeAttr( "role" )
	  .removeAttr( "tabIndex" )
	  .removeUniqueId();

	this.tabs.add( this.panels ).each(function() {
	  if ( $.data( this, "ui-tabs-destroy" ) ) {
		$( this ).remove();
	  } else {
		$( this )
		  .removeClass( "ui-state-default ui-state-active ui-state-disabled " +
			"ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel" )
		  .removeAttr( "tabIndex" )
		  .removeAttr( "aria-live" )
		  .removeAttr( "aria-busy" )
		  .removeAttr( "aria-selected" )
		  .removeAttr( "aria-labelledby" )
		  .removeAttr( "aria-hidden" )
		  .removeAttr( "aria-expanded" )
		  .removeAttr( "role" );
	  }
	});

	this.tabs.each(function() {
	  var li = $( this ),
		prev = li.data( "ui-tabs-aria-controls" );
	  if ( prev ) {
		li
		  .attr( "aria-controls", prev )
		  .removeData( "ui-tabs-aria-controls" );
	  } else {
		li.removeAttr( "aria-controls" );
	  }
	});

	this.panels.show();

	if ( this.options.heightStyle !== "content" ) {
	  this.panels.css( "height", "" );
	}
  },

  enable: function( index ) {
	var disabled = this.options.disabled;
	if ( disabled === false ) {
	  return;
	}

	if ( index === undefined ) {
	  disabled = false;
	} else {
	  index = this._getIndex( index );
	  if ( $.isArray( disabled ) ) {
		disabled = $.map( disabled, function( num ) {
		  return num !== index ? num : null;
		});
	  } else {
		disabled = $.map( this.tabs, function( li, num ) {
		  return num !== index ? num : null;
		});
	  }
	}
	this._setupDisabled( disabled );
  },

  disable: function( index ) {
	var disabled = this.options.disabled;
	if ( disabled === true ) {
	  return;
	}

	if ( index === undefined ) {
	  disabled = true;
	} else {
	  index = this._getIndex( index );
	  if ( $.inArray( index, disabled ) !== -1 ) {
		return;
	  }
	  if ( $.isArray( disabled ) ) {
		disabled = $.merge( [ index ], disabled ).sort();
	  } else {
		disabled = [ index ];
	  }
	}
	this._setupDisabled( disabled );
  },

  load: function( index, event ) {
	index = this._getIndex( index );
	var that = this,
	  tab = this.tabs.eq( index ),
	  anchor = tab.find( ".ui-tabs-anchor" ),
	  panel = this._getPanelForTab( tab ),
	  eventData = {
		tab: tab,
		panel: panel
	  };

	// not remote
	if ( isLocal( anchor[ 0 ] ) ) {
	  return;
	}

	this.xhr = $.ajax( this._ajaxSettings( anchor, event, eventData ) );

	// support: jQuery <1.8
	// jQuery <1.8 returns false if the request is canceled in beforeSend,
	// but as of 1.8, $.ajax() always returns a jqXHR object.
	if ( this.xhr && this.xhr.statusText !== "canceled" ) {
	  tab.addClass( "ui-tabs-loading" );
	  panel.attr( "aria-busy", "true" );

	  this.xhr
		.success(function( response ) {
		  // support: jQuery <1.8
		  // http://bugs.jquery.com/ticket/11778
		  setTimeout(function() {
			panel.html( response );
			that._trigger( "load", event, eventData );
		  }, 1 );
		})
		.complete(function( jqXHR, status ) {
		  // support: jQuery <1.8
		  // http://bugs.jquery.com/ticket/11778
		  setTimeout(function() {
			if ( status === "abort" ) {
			  that.panels.stop( false, true );
			}

			tab.removeClass( "ui-tabs-loading" );
			panel.removeAttr( "aria-busy" );

			if ( jqXHR === that.xhr ) {
			  delete that.xhr;
			}
		  }, 1 );
		});
	}
  },

  _ajaxSettings: function( anchor, event, eventData ) {
	var that = this;
	return {
	  url: anchor.attr( "href" ),
	  beforeSend: function( jqXHR, settings ) {
		return that._trigger( "beforeLoad", event,
		  $.extend( { jqXHR : jqXHR, ajaxSettings: settings }, eventData ) );
	  }
	};
  },

  _getPanelForTab: function( tab ) {
	var id = $( tab ).attr( "aria-controls" );
	return this.element.find( this._sanitizeSelector( "#" + id ) );
  }
});

})( jQuery );
(function( $ ) {

var increments = 0;

function addDescribedBy( elem, id ) {
  var describedby = (elem.attr( "aria-describedby" ) || "").split( /\s+/ );
  describedby.push( id );
  elem
	.data( "ui-tooltip-id", id )
	.attr( "aria-describedby", $.trim( describedby.join( " " ) ) );
}

function removeDescribedBy( elem ) {
  var id = elem.data( "ui-tooltip-id" ),
	describedby = (elem.attr( "aria-describedby" ) || "").split( /\s+/ ),
	index = $.inArray( id, describedby );
  if ( index !== -1 ) {
	describedby.splice( index, 1 );
  }

  elem.removeData( "ui-tooltip-id" );
  describedby = $.trim( describedby.join( " " ) );
  if ( describedby ) {
	elem.attr( "aria-describedby", describedby );
  } else {
	elem.removeAttr( "aria-describedby" );
  }
}

$.widget( "ui.tooltip", {
  version: "1.10.4",
  options: {
	content: function() {
	  // support: IE<9, Opera in jQuery <1.7
	  // .text() can't accept undefined, so coerce to a string
	  var title = $( this ).attr( "title" ) || "";
	  // Escape title, since we're going from an attribute to raw HTML
	  return $( "<a>" ).text( title ).html();
	},
	hide: true,
	// Disabled elements have inconsistent behavior across browsers (#8661)
	items: "[title]:not([disabled])",
	position: {
	  my: "left top+15",
	  at: "left bottom",
	  collision: "flipfit flip"
	},
	show: true,
	tooltipClass: null,
	track: false,

	// callbacks
	close: null,
	open: null
  },

  _create: function() {
	this._on({
	  mouseover: "open",
	  focusin: "open"
	});

	// IDs of generated tooltips, needed for destroy
	this.tooltips = {};
	// IDs of parent tooltips where we removed the title attribute
	this.parents = {};

	if ( this.options.disabled ) {
	  this._disable();
	}
  },

  _setOption: function( key, value ) {
	var that = this;

	if ( key === "disabled" ) {
	  this[ value ? "_disable" : "_enable" ]();
	  this.options[ key ] = value;
	  // disable element style changes
	  return;
	}

	this._super( key, value );

	if ( key === "content" ) {
	  $.each( this.tooltips, function( id, element ) {
		that._updateContent( element );
	  });
	}
  },

  _disable: function() {
	var that = this;

	// close open tooltips
	$.each( this.tooltips, function( id, element ) {
	  var event = $.Event( "blur" );
	  event.target = event.currentTarget = element[0];
	  that.close( event, true );
	});

	// remove title attributes to prevent native tooltips
	this.element.find( this.options.items ).addBack().each(function() {
	  var element = $( this );
	  if ( element.is( "[title]" ) ) {
		element
		  .data( "ui-tooltip-title", element.attr( "title" ) )
		  .attr( "title", "" );
	  }
	});
  },

  _enable: function() {
	// restore title attributes
	this.element.find( this.options.items ).addBack().each(function() {
	  var element = $( this );
	  if ( element.data( "ui-tooltip-title" ) ) {
		element.attr( "title", element.data( "ui-tooltip-title" ) );
	  }
	});
  },

  open: function( event ) {
	var that = this,
	  target = $( event ? event.target : this.element )
		// we need closest here due to mouseover bubbling,
		// but always pointing at the same event target
		.closest( this.options.items );

	// No element to show a tooltip for or the tooltip is already open
	if ( !target.length || target.data( "ui-tooltip-id" ) ) {
	  return;
	}

	if ( target.attr( "title" ) ) {
	  target.data( "ui-tooltip-title", target.attr( "title" ) );
	}

	target.data( "ui-tooltip-open", true );

	// kill parent tooltips, custom or native, for hover
	if ( event && event.type === "mouseover" ) {
	  target.parents().each(function() {
		var parent = $( this ),
		  blurEvent;
		if ( parent.data( "ui-tooltip-open" ) ) {
		  blurEvent = $.Event( "blur" );
		  blurEvent.target = blurEvent.currentTarget = this;
		  that.close( blurEvent, true );
		}
		if ( parent.attr( "title" ) ) {
		  parent.uniqueId();
		  that.parents[ this.id ] = {
			element: this,
			title: parent.attr( "title" )
		  };
		  parent.attr( "title", "" );
		}
	  });
	}

	this._updateContent( target, event );
  },

  _updateContent: function( target, event ) {
	var content,
	  contentOption = this.options.content,
	  that = this,
	  eventType = event ? event.type : null;

	if ( typeof contentOption === "string" ) {
	  return this._open( event, target, contentOption );
	}

	content = contentOption.call( target[0], function( response ) {
	  // ignore async response if tooltip was closed already
	  if ( !target.data( "ui-tooltip-open" ) ) {
		return;
	  }
	  // IE may instantly serve a cached response for ajax requests
	  // delay this call to _open so the other call to _open runs first
	  that._delay(function() {
		// jQuery creates a special event for focusin when it doesn't
		// exist natively. To improve performance, the native event
		// object is reused and the type is changed. Therefore, we can't
		// rely on the type being correct after the event finished
		// bubbling, so we set it back to the previous value. (#8740)
		if ( event ) {
		  event.type = eventType;
		}
		this._open( event, target, response );
	  });
	});
	if ( content ) {
	  this._open( event, target, content );
	}
  },

  _open: function( event, target, content ) {
	var tooltip, events, delayedShow,
	  positionOption = $.extend( {}, this.options.position );

	if ( !content ) {
	  return;
	}

	// Content can be updated multiple times. If the tooltip already
	// exists, then just update the content and bail.
	tooltip = this._find( target );
	if ( tooltip.length ) {
	  tooltip.find( ".ui-tooltip-content" ).html( content );
	  return;
	}

	// if we have a title, clear it to prevent the native tooltip
	// we have to check first to avoid defining a title if none exists
	// (we don't want to cause an element to start matching [title])
	//
	// We use removeAttr only for key events, to allow IE to export the correct
	// accessible attributes. For mouse events, set to empty string to avoid
	// native tooltip showing up (happens only when removing inside mouseover).
	if ( target.is( "[title]" ) ) {
	  if ( event && event.type === "mouseover" ) {
		target.attr( "title", "" );
	  } else {
		target.removeAttr( "title" );
	  }
	}

	tooltip = this._tooltip( target );
	addDescribedBy( target, tooltip.attr( "id" ) );
	tooltip.find( ".ui-tooltip-content" ).html( content );

	function position( event ) {
	  positionOption.of = event;
	  if ( tooltip.is( ":hidden" ) ) {
		return;
	  }
	  tooltip.position( positionOption );
	}
	if ( this.options.track && event && /^mouse/.test( event.type ) ) {
	  this._on( this.document, {
		mousemove: position
	  });
	  // trigger once to override element-relative positioning
	  position( event );
	} else {
	  tooltip.position( $.extend({
		of: target
	  }, this.options.position ) );
	}

	tooltip.hide();

	this._show( tooltip, this.options.show );
	// Handle tracking tooltips that are shown with a delay (#8644). As soon
	// as the tooltip is visible, position the tooltip using the most recent
	// event.
	if ( this.options.show && this.options.show.delay ) {
	  delayedShow = this.delayedShow = setInterval(function() {
		if ( tooltip.is( ":visible" ) ) {
		  position( positionOption.of );
		  clearInterval( delayedShow );
		}
	  }, $.fx.interval );
	}

	this._trigger( "open", event, { tooltip: tooltip } );

	events = {
	  keyup: function( event ) {
		if ( event.keyCode === $.ui.keyCode.ESCAPE ) {
		  var fakeEvent = $.Event(event);
		  fakeEvent.currentTarget = target[0];
		  this.close( fakeEvent, true );
		}
	  },
	  remove: function() {
		this._removeTooltip( tooltip );
	  }
	};
	if ( !event || event.type === "mouseover" ) {
	  events.mouseleave = "close";
	}
	if ( !event || event.type === "focusin" ) {
	  events.focusout = "close";
	}
	this._on( true, target, events );
  },

  close: function( event ) {
	var that = this,
	  target = $( event ? event.currentTarget : this.element ),
	  tooltip = this._find( target );

	// disabling closes the tooltip, so we need to track when we're closing
	// to avoid an infinite loop in case the tooltip becomes disabled on close
	if ( this.closing ) {
	  return;
	}

	// Clear the interval for delayed tracking tooltips
	clearInterval( this.delayedShow );

	// only set title if we had one before (see comment in _open())
	if ( target.data( "ui-tooltip-title" ) ) {
	  target.attr( "title", target.data( "ui-tooltip-title" ) );
	}

	removeDescribedBy( target );

	tooltip.stop( true );
	this._hide( tooltip, this.options.hide, function() {
	  that._removeTooltip( $( this ) );
	});

	target.removeData( "ui-tooltip-open" );
	this._off( target, "mouseleave focusout keyup" );
	// Remove 'remove' binding only on delegated targets
	if ( target[0] !== this.element[0] ) {
	  this._off( target, "remove" );
	}
	this._off( this.document, "mousemove" );

	if ( event && event.type === "mouseleave" ) {
	  $.each( this.parents, function( id, parent ) {
		$( parent.element ).attr( "title", parent.title );
		delete that.parents[ id ];
	  });
	}

	this.closing = true;
	this._trigger( "close", event, { tooltip: tooltip } );
	this.closing = false;
  },

  _tooltip: function( element ) {
	var id = "ui-tooltip-" + increments++,
	  tooltip = $( "<div>" )
		.attr({
		  id: id,
		  role: "tooltip"
		})
		.addClass( "ui-tooltip ui-widget ui-corner-all ui-widget-content " +
		  ( this.options.tooltipClass || "" ) );
	$( "<div>" )
	  .addClass( "ui-tooltip-content" )
	  .appendTo( tooltip );
	tooltip.appendTo( this.document[0].body );
	this.tooltips[ id ] = element;
	return tooltip;
  },

  _find: function( target ) {
	var id = target.data( "ui-tooltip-id" );
	return id ? $( "#" + id ) : $();
  },

  _removeTooltip: function( tooltip ) {
	tooltip.remove();
	delete this.tooltips[ tooltip.attr( "id" ) ];
  },

  _destroy: function() {
	var that = this;

	// close open tooltips
	$.each( this.tooltips, function( id, element ) {
	  // Delegate to close method to handle common cleanup
	  var event = $.Event( "blur" );
	  event.target = event.currentTarget = element[0];
	  that.close( event, true );

	  // Remove immediately; destroying an open tooltip doesn't use the
	  // hide animation
	  $( "#" + id ).remove();

	  // Restore the title
	  if ( element.data( "ui-tooltip-title" ) ) {
		element.attr( "title", element.data( "ui-tooltip-title" ) );
		element.removeData( "ui-tooltip-title" );
	  }
	});
  }
});

}( jQuery ) );
(function($, undefined) {

var dataSpace = "ui-effects-";

$.effects = {
  effect: {}
};

/*!
 * jQuery Color Animations v2.1.2
 * https://github.com/jquery/jquery-color
 *
 * Copyright 2013 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Wed Jan 16 08:47:09 2013 -0600
 */
(function( jQuery, undefined ) {

  var stepHooks = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",

  // plusequals test for += 100 -= 100
  rplusequals = /^([\-+])=\s*(\d+\.?\d*)/,
  // a set of RE's that can match strings and generate color tuples.
  stringParsers = [{
	  re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
	  parse: function( execResult ) {
		return [
		  execResult[ 1 ],
		  execResult[ 2 ],
		  execResult[ 3 ],
		  execResult[ 4 ]
		];
	  }
	}, {
	  re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
	  parse: function( execResult ) {
		return [
		  execResult[ 1 ] * 2.55,
		  execResult[ 2 ] * 2.55,
		  execResult[ 3 ] * 2.55,
		  execResult[ 4 ]
		];
	  }
	}, {
	  // this regex ignores A-F because it's compared against an already lowercased string
	  re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
	  parse: function( execResult ) {
		return [
		  parseInt( execResult[ 1 ], 16 ),
		  parseInt( execResult[ 2 ], 16 ),
		  parseInt( execResult[ 3 ], 16 )
		];
	  }
	}, {
	  // this regex ignores A-F because it's compared against an already lowercased string
	  re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
	  parse: function( execResult ) {
		return [
		  parseInt( execResult[ 1 ] + execResult[ 1 ], 16 ),
		  parseInt( execResult[ 2 ] + execResult[ 2 ], 16 ),
		  parseInt( execResult[ 3 ] + execResult[ 3 ], 16 )
		];
	  }
	}, {
	  re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
	  space: "hsla",
	  parse: function( execResult ) {
		return [
		  execResult[ 1 ],
		  execResult[ 2 ] / 100,
		  execResult[ 3 ] / 100,
		  execResult[ 4 ]
		];
	  }
	}],

  // jQuery.Color( )
  color = jQuery.Color = function( color, green, blue, alpha ) {
	return new jQuery.Color.fn.parse( color, green, blue, alpha );
  },
  spaces = {
	rgba: {
	  props: {
		red: {
		  idx: 0,
		  type: "byte"
		},
		green: {
		  idx: 1,
		  type: "byte"
		},
		blue: {
		  idx: 2,
		  type: "byte"
		}
	  }
	},

	hsla: {
	  props: {
		hue: {
		  idx: 0,
		  type: "degrees"
		},
		saturation: {
		  idx: 1,
		  type: "percent"
		},
		lightness: {
		  idx: 2,
		  type: "percent"
		}
	  }
	}
  },
  propTypes = {
	"byte": {
	  floor: true,
	  max: 255
	},
	"percent": {
	  max: 1
	},
	"degrees": {
	  mod: 360,
	  floor: true
	}
  },
  support = color.support = {},

  // element for support tests
  supportElem = jQuery( "<p>" )[ 0 ],

  // colors = jQuery.Color.names
  colors,

  // local aliases of functions called often
  each = jQuery.each;

// determine rgba support immediately
supportElem.style.cssText = "background-color:rgba(1,1,1,.5)";
support.rgba = supportElem.style.backgroundColor.indexOf( "rgba" ) > -1;

// define cache name and alpha properties
// for rgba and hsla spaces
each( spaces, function( spaceName, space ) {
  space.cache = "_" + spaceName;
  space.props.alpha = {
	idx: 3,
	type: "percent",
	def: 1
  };
});

function clamp( value, prop, allowEmpty ) {
  var type = propTypes[ prop.type ] || {};

  if ( value == null ) {
	return (allowEmpty || !prop.def) ? null : prop.def;
  }

  // ~~ is an short way of doing floor for positive numbers
  value = type.floor ? ~~value : parseFloat( value );

  // IE will pass in empty strings as value for alpha,
  // which will hit this case
  if ( isNaN( value ) ) {
	return prop.def;
  }

  if ( type.mod ) {
	// we add mod before modding to make sure that negatives values
	// get converted properly: -10 -> 350
	return (value + type.mod) % type.mod;
  }

  // for now all property types without mod have min and max
  return 0 > value ? 0 : type.max < value ? type.max : value;
}

function stringParse( string ) {
  var inst = color(),
	rgba = inst._rgba = [];

  string = string.toLowerCase();

  each( stringParsers, function( i, parser ) {
	var parsed,
	  match = parser.re.exec( string ),
	  values = match && parser.parse( match ),
	  spaceName = parser.space || "rgba";

	if ( values ) {
	  parsed = inst[ spaceName ]( values );

	  // if this was an rgba parse the assignment might happen twice
	  // oh well....
	  inst[ spaces[ spaceName ].cache ] = parsed[ spaces[ spaceName ].cache ];
	  rgba = inst._rgba = parsed._rgba;

	  // exit each( stringParsers ) here because we matched
	  return false;
	}
  });

  // Found a stringParser that handled it
  if ( rgba.length ) {

	// if this came from a parsed string, force "transparent" when alpha is 0
	// chrome, (and maybe others) return "transparent" as rgba(0,0,0,0)
	if ( rgba.join() === "0,0,0,0" ) {
	  jQuery.extend( rgba, colors.transparent );
	}
	return inst;
  }

  // named colors
  return colors[ string ];
}

color.fn = jQuery.extend( color.prototype, {
  parse: function( red, green, blue, alpha ) {
	if ( red === undefined ) {
	  this._rgba = [ null, null, null, null ];
	  return this;
	}
	if ( red.jquery || red.nodeType ) {
	  red = jQuery( red ).css( green );
	  green = undefined;
	}

	var inst = this,
	  type = jQuery.type( red ),
	  rgba = this._rgba = [];

	// more than 1 argument specified - assume ( red, green, blue, alpha )
	if ( green !== undefined ) {
	  red = [ red, green, blue, alpha ];
	  type = "array";
	}

	if ( type === "string" ) {
	  return this.parse( stringParse( red ) || colors._default );
	}

	if ( type === "array" ) {
	  each( spaces.rgba.props, function( key, prop ) {
		rgba[ prop.idx ] = clamp( red[ prop.idx ], prop );
	  });
	  return this;
	}

	if ( type === "object" ) {
	  if ( red instanceof color ) {
		each( spaces, function( spaceName, space ) {
		  if ( red[ space.cache ] ) {
			inst[ space.cache ] = red[ space.cache ].slice();
		  }
		});
	  } else {
		each( spaces, function( spaceName, space ) {
		  var cache = space.cache;
		  each( space.props, function( key, prop ) {

			// if the cache doesn't exist, and we know how to convert
			if ( !inst[ cache ] && space.to ) {

			  // if the value was null, we don't need to copy it
			  // if the key was alpha, we don't need to copy it either
			  if ( key === "alpha" || red[ key ] == null ) {
				return;
			  }
			  inst[ cache ] = space.to( inst._rgba );
			}

			// this is the only case where we allow nulls for ALL properties.
			// call clamp with alwaysAllowEmpty
			inst[ cache ][ prop.idx ] = clamp( red[ key ], prop, true );
		  });

		  // everything defined but alpha?
		  if ( inst[ cache ] && jQuery.inArray( null, inst[ cache ].slice( 0, 3 ) ) < 0 ) {
			// use the default of 1
			inst[ cache ][ 3 ] = 1;
			if ( space.from ) {
			  inst._rgba = space.from( inst[ cache ] );
			}
		  }
		});
	  }
	  return this;
	}
  },
  is: function( compare ) {
	var is = color( compare ),
	  same = true,
	  inst = this;

	each( spaces, function( _, space ) {
	  var localCache,
		isCache = is[ space.cache ];
	  if (isCache) {
		localCache = inst[ space.cache ] || space.to && space.to( inst._rgba ) || [];
		each( space.props, function( _, prop ) {
		  if ( isCache[ prop.idx ] != null ) {
			same = ( isCache[ prop.idx ] === localCache[ prop.idx ] );
			return same;
		  }
		});
	  }
	  return same;
	});
	return same;
  },
  _space: function() {
	var used = [],
	  inst = this;
	each( spaces, function( spaceName, space ) {
	  if ( inst[ space.cache ] ) {
		used.push( spaceName );
	  }
	});
	return used.pop();
  },
  transition: function( other, distance ) {
	var end = color( other ),
	  spaceName = end._space(),
	  space = spaces[ spaceName ],
	  startColor = this.alpha() === 0 ? color( "transparent" ) : this,
	  start = startColor[ space.cache ] || space.to( startColor._rgba ),
	  result = start.slice();

	end = end[ space.cache ];
	each( space.props, function( key, prop ) {
	  var index = prop.idx,
		startValue = start[ index ],
		endValue = end[ index ],
		type = propTypes[ prop.type ] || {};

	  // if null, don't override start value
	  if ( endValue === null ) {
		return;
	  }
	  // if null - use end
	  if ( startValue === null ) {
		result[ index ] = endValue;
	  } else {
		if ( type.mod ) {
		  if ( endValue - startValue > type.mod / 2 ) {
			startValue += type.mod;
		  } else if ( startValue - endValue > type.mod / 2 ) {
			startValue -= type.mod;
		  }
		}
		result[ index ] = clamp( ( endValue - startValue ) * distance + startValue, prop );
	  }
	});
	return this[ spaceName ]( result );
  },
  blend: function( opaque ) {
	// if we are already opaque - return ourself
	if ( this._rgba[ 3 ] === 1 ) {
	  return this;
	}

	var rgb = this._rgba.slice(),
	  a = rgb.pop(),
	  blend = color( opaque )._rgba;

	return color( jQuery.map( rgb, function( v, i ) {
	  return ( 1 - a ) * blend[ i ] + a * v;
	}));
  },
  toRgbaString: function() {
	var prefix = "rgba(",
	  rgba = jQuery.map( this._rgba, function( v, i ) {
		return v == null ? ( i > 2 ? 1 : 0 ) : v;
	  });

	if ( rgba[ 3 ] === 1 ) {
	  rgba.pop();
	  prefix = "rgb(";
	}

	return prefix + rgba.join() + ")";
  },
  toHslaString: function() {
	var prefix = "hsla(",
	  hsla = jQuery.map( this.hsla(), function( v, i ) {
		if ( v == null ) {
		  v = i > 2 ? 1 : 0;
		}

		// catch 1 and 2
		if ( i && i < 3 ) {
		  v = Math.round( v * 100 ) + "%";
		}
		return v;
	  });

	if ( hsla[ 3 ] === 1 ) {
	  hsla.pop();
	  prefix = "hsl(";
	}
	return prefix + hsla.join() + ")";
  },
  toHexString: function( includeAlpha ) {
	var rgba = this._rgba.slice(),
	  alpha = rgba.pop();

	if ( includeAlpha ) {
	  rgba.push( ~~( alpha * 255 ) );
	}

	return "#" + jQuery.map( rgba, function( v ) {

	  // default to 0 when nulls exist
	  v = ( v || 0 ).toString( 16 );
	  return v.length === 1 ? "0" + v : v;
	}).join("");
  },
  toString: function() {
	return this._rgba[ 3 ] === 0 ? "transparent" : this.toRgbaString();
  }
});
color.fn.parse.prototype = color.fn;

// hsla conversions adapted from:
// https://code.google.com/p/maashaack/source/browse/packages/graphics/trunk/src/graphics/colors/HUE2RGB.as?r=5021

function hue2rgb( p, q, h ) {
  h = ( h + 1 ) % 1;
  if ( h * 6 < 1 ) {
	return p + (q - p) * h * 6;
  }
  if ( h * 2 < 1) {
	return q;
  }
  if ( h * 3 < 2 ) {
	return p + (q - p) * ((2/3) - h) * 6;
  }
  return p;
}

spaces.hsla.to = function ( rgba ) {
  if ( rgba[ 0 ] == null || rgba[ 1 ] == null || rgba[ 2 ] == null ) {
	return [ null, null, null, rgba[ 3 ] ];
  }
  var r = rgba[ 0 ] / 255,
	g = rgba[ 1 ] / 255,
	b = rgba[ 2 ] / 255,
	a = rgba[ 3 ],
	max = Math.max( r, g, b ),
	min = Math.min( r, g, b ),
	diff = max - min,
	add = max + min,
	l = add * 0.5,
	h, s;

  if ( min === max ) {
	h = 0;
  } else if ( r === max ) {
	h = ( 60 * ( g - b ) / diff ) + 360;
  } else if ( g === max ) {
	h = ( 60 * ( b - r ) / diff ) + 120;
  } else {
	h = ( 60 * ( r - g ) / diff ) + 240;
  }

  // chroma (diff) == 0 means greyscale which, by definition, saturation = 0%
  // otherwise, saturation is based on the ratio of chroma (diff) to lightness (add)
  if ( diff === 0 ) {
	s = 0;
  } else if ( l <= 0.5 ) {
	s = diff / add;
  } else {
	s = diff / ( 2 - add );
  }
  return [ Math.round(h) % 360, s, l, a == null ? 1 : a ];
};

spaces.hsla.from = function ( hsla ) {
  if ( hsla[ 0 ] == null || hsla[ 1 ] == null || hsla[ 2 ] == null ) {
	return [ null, null, null, hsla[ 3 ] ];
  }
  var h = hsla[ 0 ] / 360,
	s = hsla[ 1 ],
	l = hsla[ 2 ],
	a = hsla[ 3 ],
	q = l <= 0.5 ? l * ( 1 + s ) : l + s - l * s,
	p = 2 * l - q;

  return [
	Math.round( hue2rgb( p, q, h + ( 1 / 3 ) ) * 255 ),
	Math.round( hue2rgb( p, q, h ) * 255 ),
	Math.round( hue2rgb( p, q, h - ( 1 / 3 ) ) * 255 ),
	a
  ];
};


each( spaces, function( spaceName, space ) {
  var props = space.props,
	cache = space.cache,
	to = space.to,
	from = space.from;

  // makes rgba() and hsla()
  color.fn[ spaceName ] = function( value ) {

	// generate a cache for this space if it doesn't exist
	if ( to && !this[ cache ] ) {
	  this[ cache ] = to( this._rgba );
	}
	if ( value === undefined ) {
	  return this[ cache ].slice();
	}

	var ret,
	  type = jQuery.type( value ),
	  arr = ( type === "array" || type === "object" ) ? value : arguments,
	  local = this[ cache ].slice();

	each( props, function( key, prop ) {
	  var val = arr[ type === "object" ? key : prop.idx ];
	  if ( val == null ) {
		val = local[ prop.idx ];
	  }
	  local[ prop.idx ] = clamp( val, prop );
	});

	if ( from ) {
	  ret = color( from( local ) );
	  ret[ cache ] = local;
	  return ret;
	} else {
	  return color( local );
	}
  };

  // makes red() green() blue() alpha() hue() saturation() lightness()
  each( props, function( key, prop ) {
	// alpha is included in more than one space
	if ( color.fn[ key ] ) {
	  return;
	}
	color.fn[ key ] = function( value ) {
	  var vtype = jQuery.type( value ),
		fn = ( key === "alpha" ? ( this._hsla ? "hsla" : "rgba" ) : spaceName ),
		local = this[ fn ](),
		cur = local[ prop.idx ],
		match;

	  if ( vtype === "undefined" ) {
		return cur;
	  }

	  if ( vtype === "function" ) {
		value = value.call( this, cur );
		vtype = jQuery.type( value );
	  }
	  if ( value == null && prop.empty ) {
		return this;
	  }
	  if ( vtype === "string" ) {
		match = rplusequals.exec( value );
		if ( match ) {
		  value = cur + parseFloat( match[ 2 ] ) * ( match[ 1 ] === "+" ? 1 : -1 );
		}
	  }
	  local[ prop.idx ] = value;
	  return this[ fn ]( local );
	};
  });
});

// add cssHook and .fx.step function for each named hook.
// accept a space separated string of properties
color.hook = function( hook ) {
  var hooks = hook.split( " " );
  each( hooks, function( i, hook ) {
	jQuery.cssHooks[ hook ] = {
	  set: function( elem, value ) {
		var parsed, curElem,
		  backgroundColor = "";

		if ( value !== "transparent" && ( jQuery.type( value ) !== "string" || ( parsed = stringParse( value ) ) ) ) {
		  value = color( parsed || value );
		  if ( !support.rgba && value._rgba[ 3 ] !== 1 ) {
			curElem = hook === "backgroundColor" ? elem.parentNode : elem;
			while (
			  (backgroundColor === "" || backgroundColor === "transparent") &&
			  curElem && curElem.style
			) {
			  try {
				backgroundColor = jQuery.css( curElem, "backgroundColor" );
				curElem = curElem.parentNode;
			  } catch ( e ) {
			  }
			}

			value = value.blend( backgroundColor && backgroundColor !== "transparent" ?
			  backgroundColor :
			  "_default" );
		  }

		  value = value.toRgbaString();
		}
		try {
		  elem.style[ hook ] = value;
		} catch( e ) {
		  // wrapped to prevent IE from throwing errors on "invalid" values like 'auto' or 'inherit'
		}
	  }
	};
	jQuery.fx.step[ hook ] = function( fx ) {
	  if ( !fx.colorInit ) {
		fx.start = color( fx.elem, hook );
		fx.end = color( fx.end );
		fx.colorInit = true;
	  }
	  jQuery.cssHooks[ hook ].set( fx.elem, fx.start.transition( fx.end, fx.pos ) );
	};
  });

};

color.hook( stepHooks );

jQuery.cssHooks.borderColor = {
  expand: function( value ) {
	var expanded = {};

	each( [ "Top", "Right", "Bottom", "Left" ], function( i, part ) {
	  expanded[ "border" + part + "Color" ] = value;
	});
	return expanded;
  }
};

// Basic color names only.
// Usage of any of the other color names requires adding yourself or including
// jquery.color.svg-names.js.
colors = jQuery.Color.names = {
  // 4.1. Basic color keywords
  aqua: "#00ffff",
  black: "#000000",
  blue: "#0000ff",
  fuchsia: "#ff00ff",
  gray: "#808080",
  green: "#008000",
  lime: "#00ff00",
  maroon: "#800000",
  navy: "#000080",
  olive: "#808000",
  purple: "#800080",
  red: "#ff0000",
  silver: "#c0c0c0",
  teal: "#008080",
  white: "#ffffff",
  yellow: "#ffff00",

  // 4.2.3. "transparent" color keyword
  transparent: [ null, null, null, 0 ],

  _default: "#ffffff"
};

})( jQuery );


/******************************************************************************/
/****************************** CLASS ANIMATIONS ******************************/
/******************************************************************************/
(function() {

var classAnimationActions = [ "add", "remove", "toggle" ],
  shorthandStyles = {
	border: 1,
	borderBottom: 1,
	borderColor: 1,
	borderLeft: 1,
	borderRight: 1,
	borderTop: 1,
	borderWidth: 1,
	margin: 1,
	padding: 1
  };

$.each([ "borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle" ], function( _, prop ) {
  $.fx.step[ prop ] = function( fx ) {
	if ( fx.end !== "none" && !fx.setAttr || fx.pos === 1 && !fx.setAttr ) {
	  jQuery.style( fx.elem, prop, fx.end );
	  fx.setAttr = true;
	}
  };
});

function getElementStyles( elem ) {
  var key, len,
	style = elem.ownerDocument.defaultView ?
	  elem.ownerDocument.defaultView.getComputedStyle( elem, null ) :
	  elem.currentStyle,
	styles = {};

  if ( style && style.length && style[ 0 ] && style[ style[ 0 ] ] ) {
	len = style.length;
	while ( len-- ) {
	  key = style[ len ];
	  if ( typeof style[ key ] === "string" ) {
		styles[ $.camelCase( key ) ] = style[ key ];
	  }
	}
  // support: Opera, IE <9
  } else {
	for ( key in style ) {
	  if ( typeof style[ key ] === "string" ) {
		styles[ key ] = style[ key ];
	  }
	}
  }

  return styles;
}


function styleDifference( oldStyle, newStyle ) {
  var diff = {},
	name, value;

  for ( name in newStyle ) {
	value = newStyle[ name ];
	if ( oldStyle[ name ] !== value ) {
	  if ( !shorthandStyles[ name ] ) {
		if ( $.fx.step[ name ] || !isNaN( parseFloat( value ) ) ) {
		  diff[ name ] = value;
		}
	  }
	}
  }

  return diff;
}

// support: jQuery <1.8
if ( !$.fn.addBack ) {
  $.fn.addBack = function( selector ) {
	return this.add( selector == null ?
	  this.prevObject : this.prevObject.filter( selector )
	);
  };
}

$.effects.animateClass = function( value, duration, easing, callback ) {
  var o = $.speed( duration, easing, callback );

  return this.queue( function() {
	var animated = $( this ),
	  baseClass = animated.attr( "class" ) || "",
	  applyClassChange,
	  allAnimations = o.children ? animated.find( "*" ).addBack() : animated;

	// map the animated objects to store the original styles.
	allAnimations = allAnimations.map(function() {
	  var el = $( this );
	  return {
		el: el,
		start: getElementStyles( this )
	  };
	});

	// apply class change
	applyClassChange = function() {
	  $.each( classAnimationActions, function(i, action) {
		if ( value[ action ] ) {
		  animated[ action + "Class" ]( value[ action ] );
		}
	  });
	};
	applyClassChange();

	// map all animated objects again - calculate new styles and diff
	allAnimations = allAnimations.map(function() {
	  this.end = getElementStyles( this.el[ 0 ] );
	  this.diff = styleDifference( this.start, this.end );
	  return this;
	});

	// apply original class
	animated.attr( "class", baseClass );

	// map all animated objects again - this time collecting a promise
	allAnimations = allAnimations.map(function() {
	  var styleInfo = this,
		dfd = $.Deferred(),
		opts = $.extend({}, o, {
		  queue: false,
		  complete: function() {
			dfd.resolve( styleInfo );
		  }
		});

	  this.el.animate( this.diff, opts );
	  return dfd.promise();
	});

	// once all animations have completed:
	$.when.apply( $, allAnimations.get() ).done(function() {

	  // set the final class
	  applyClassChange();

	  // for each animated element,
	  // clear all css properties that were animated
	  $.each( arguments, function() {
		var el = this.el;
		$.each( this.diff, function(key) {
		  el.css( key, "" );
		});
	  });

	  // this is guarnteed to be there if you use jQuery.speed()
	  // it also handles dequeuing the next anim...
	  o.complete.call( animated[ 0 ] );
	});
  });
};

$.fn.extend({
  addClass: (function( orig ) {
	return function( classNames, speed, easing, callback ) {
	  return speed ?
		$.effects.animateClass.call( this,
		  { add: classNames }, speed, easing, callback ) :
		orig.apply( this, arguments );
	};
  })( $.fn.addClass ),

  removeClass: (function( orig ) {
	return function( classNames, speed, easing, callback ) {
	  return arguments.length > 1 ?
		$.effects.animateClass.call( this,
		  { remove: classNames }, speed, easing, callback ) :
		orig.apply( this, arguments );
	};
  })( $.fn.removeClass ),

  toggleClass: (function( orig ) {
	return function( classNames, force, speed, easing, callback ) {
	  if ( typeof force === "boolean" || force === undefined ) {
		if ( !speed ) {
		  // without speed parameter
		  return orig.apply( this, arguments );
		} else {
		  return $.effects.animateClass.call( this,
			(force ? { add: classNames } : { remove: classNames }),
			speed, easing, callback );
		}
	  } else {
		// without force parameter
		return $.effects.animateClass.call( this,
		  { toggle: classNames }, force, speed, easing );
	  }
	};
  })( $.fn.toggleClass ),

  switchClass: function( remove, add, speed, easing, callback) {
	return $.effects.animateClass.call( this, {
	  add: add,
	  remove: remove
	}, speed, easing, callback );
  }
});

})();

/******************************************************************************/
/*********************************** EFFECTS **********************************/
/******************************************************************************/

(function() {

$.extend( $.effects, {
  version: "1.10.4",

  // Saves a set of properties in a data storage
  save: function( element, set ) {
	for( var i=0; i < set.length; i++ ) {
	  if ( set[ i ] !== null ) {
		element.data( dataSpace + set[ i ], element[ 0 ].style[ set[ i ] ] );
	  }
	}
  },

  // Restores a set of previously saved properties from a data storage
  restore: function( element, set ) {
	var val, i;
	for( i=0; i < set.length; i++ ) {
	  if ( set[ i ] !== null ) {
		val = element.data( dataSpace + set[ i ] );
		// support: jQuery 1.6.2
		// http://bugs.jquery.com/ticket/9917
		// jQuery 1.6.2 incorrectly returns undefined for any falsy value.
		// We can't differentiate between "" and 0 here, so we just assume
		// empty string since it's likely to be a more common value...
		if ( val === undefined ) {
		  val = "";
		}
		element.css( set[ i ], val );
	  }
	}
  },

  setMode: function( el, mode ) {
	if (mode === "toggle") {
	  mode = el.is( ":hidden" ) ? "show" : "hide";
	}
	return mode;
  },

  // Translates a [top,left] array into a baseline value
  // this should be a little more flexible in the future to handle a string & hash
  getBaseline: function( origin, original ) {
	var y, x;
	switch ( origin[ 0 ] ) {
	  case "top": y = 0; break;
	  case "middle": y = 0.5; break;
	  case "bottom": y = 1; break;
	  default: y = origin[ 0 ] / original.height;
	}
	switch ( origin[ 1 ] ) {
	  case "left": x = 0; break;
	  case "center": x = 0.5; break;
	  case "right": x = 1; break;
	  default: x = origin[ 1 ] / original.width;
	}
	return {
	  x: x,
	  y: y
	};
  },

  // Wraps the element around a wrapper that copies position properties
  createWrapper: function( element ) {

	// if the element is already wrapped, return it
	if ( element.parent().is( ".ui-effects-wrapper" )) {
	  return element.parent();
	}

	// wrap the element
	var props = {
		width: element.outerWidth(true),
		height: element.outerHeight(true),
		"float": element.css( "float" )
	  },
	  wrapper = $( "<div></div>" )
		.addClass( "ui-effects-wrapper" )
		.css({
		  fontSize: "100%",
		  background: "transparent",
		  border: "none",
		  margin: 0,
		  padding: 0
		}),
	  // Store the size in case width/height are defined in % - Fixes #5245
	  size = {
		width: element.width(),
		height: element.height()
	  },
	  active = document.activeElement;

	// support: Firefox
	// Firefox incorrectly exposes anonymous content
	// https://bugzilla.mozilla.org/show_bug.cgi?id=561664
	try {
	  active.id;
	} catch( e ) {
	  active = document.body;
	}

	element.wrap( wrapper );

	// Fixes #7595 - Elements lose focus when wrapped.
	if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
	  $( active ).focus();
	}

	wrapper = element.parent(); //Hotfix for jQuery 1.4 since some change in wrap() seems to actually lose the reference to the wrapped element

	// transfer positioning properties to the wrapper
	if ( element.css( "position" ) === "static" ) {
	  wrapper.css({ position: "relative" });
	  element.css({ position: "relative" });
	} else {
	  $.extend( props, {
		position: element.css( "position" ),
		zIndex: element.css( "z-index" )
	  });
	  $.each([ "top", "left", "bottom", "right" ], function(i, pos) {
		props[ pos ] = element.css( pos );
		if ( isNaN( parseInt( props[ pos ], 10 ) ) ) {
		  props[ pos ] = "auto";
		}
	  });
	  element.css({
		position: "relative",
		top: 0,
		left: 0,
		right: "auto",
		bottom: "auto"
	  });
	}
	element.css(size);

	return wrapper.css( props ).show();
  },

  removeWrapper: function( element ) {
	var active = document.activeElement;

	if ( element.parent().is( ".ui-effects-wrapper" ) ) {
	  element.parent().replaceWith( element );

	  // Fixes #7595 - Elements lose focus when wrapped.
	  if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
		$( active ).focus();
	  }
	}


	return element;
  },

  setTransition: function( element, list, factor, value ) {
	value = value || {};
	$.each( list, function( i, x ) {
	  var unit = element.cssUnit( x );
	  if ( unit[ 0 ] > 0 ) {
		value[ x ] = unit[ 0 ] * factor + unit[ 1 ];
	  }
	});
	return value;
  }
});

// return an effect options object for the given parameters:
function _normalizeArguments( effect, options, speed, callback ) {

  // allow passing all options as the first parameter
  if ( $.isPlainObject( effect ) ) {
	options = effect;
	effect = effect.effect;
  }

  // convert to an object
  effect = { effect: effect };

  // catch (effect, null, ...)
  if ( options == null ) {
	options = {};
  }

  // catch (effect, callback)
  if ( $.isFunction( options ) ) {
	callback = options;
	speed = null;
	options = {};
  }

  // catch (effect, speed, ?)
  if ( typeof options === "number" || $.fx.speeds[ options ] ) {
	callback = speed;
	speed = options;
	options = {};
  }

  // catch (effect, options, callback)
  if ( $.isFunction( speed ) ) {
	callback = speed;
	speed = null;
  }

  // add options to effect
  if ( options ) {
	$.extend( effect, options );
  }

  speed = speed || options.duration;
  effect.duration = $.fx.off ? 0 :
	typeof speed === "number" ? speed :
	speed in $.fx.speeds ? $.fx.speeds[ speed ] :
	$.fx.speeds._default;

  effect.complete = callback || options.complete;

  return effect;
}

function standardAnimationOption( option ) {
  // Valid standard speeds (nothing, number, named speed)
  if ( !option || typeof option === "number" || $.fx.speeds[ option ] ) {
	return true;
  }

  // Invalid strings - treat as "normal" speed
  if ( typeof option === "string" && !$.effects.effect[ option ] ) {
	return true;
  }

  // Complete callback
  if ( $.isFunction( option ) ) {
	return true;
  }

  // Options hash (but not naming an effect)
  if ( typeof option === "object" && !option.effect ) {
	return true;
  }

  // Didn't match any standard API
  return false;
}

$.fn.extend({
  effect: function( /* effect, options, speed, callback */ ) {
	var args = _normalizeArguments.apply( this, arguments ),
	  mode = args.mode,
	  queue = args.queue,
	  effectMethod = $.effects.effect[ args.effect ];

	if ( $.fx.off || !effectMethod ) {
	  // delegate to the original method (e.g., .show()) if possible
	  if ( mode ) {
		return this[ mode ]( args.duration, args.complete );
	  } else {
		return this.each( function() {
		  if ( args.complete ) {
			args.complete.call( this );
		  }
		});
	  }
	}

	function run( next ) {
	  var elem = $( this ),
		complete = args.complete,
		mode = args.mode;

	  function done() {
		if ( $.isFunction( complete ) ) {
		  complete.call( elem[0] );
		}
		if ( $.isFunction( next ) ) {
		  next();
		}
	  }

	  // If the element already has the correct final state, delegate to
	  // the core methods so the internal tracking of "olddisplay" works.
	  if ( elem.is( ":hidden" ) ? mode === "hide" : mode === "show" ) {
		elem[ mode ]();
		done();
	  } else {
		effectMethod.call( elem[0], args, done );
	  }
	}

	return queue === false ? this.each( run ) : this.queue( queue || "fx", run );
  },

  show: (function( orig ) {
	return function( option ) {
	  if ( standardAnimationOption( option ) ) {
		return orig.apply( this, arguments );
	  } else {
		var args = _normalizeArguments.apply( this, arguments );
		args.mode = "show";
		return this.effect.call( this, args );
	  }
	};
  })( $.fn.show ),

  hide: (function( orig ) {
	return function( option ) {
	  if ( standardAnimationOption( option ) ) {
		return orig.apply( this, arguments );
	  } else {
		var args = _normalizeArguments.apply( this, arguments );
		args.mode = "hide";
		return this.effect.call( this, args );
	  }
	};
  })( $.fn.hide ),

  toggle: (function( orig ) {
	return function( option ) {
	  if ( standardAnimationOption( option ) || typeof option === "boolean" ) {
		return orig.apply( this, arguments );
	  } else {
		var args = _normalizeArguments.apply( this, arguments );
		args.mode = "toggle";
		return this.effect.call( this, args );
	  }
	};
  })( $.fn.toggle ),

  // helper functions
  cssUnit: function(key) {
	var style = this.css( key ),
	  val = [];

	$.each( [ "em", "px", "%", "pt" ], function( i, unit ) {
	  if ( style.indexOf( unit ) > 0 ) {
		val = [ parseFloat( style ), unit ];
	  }
	});
	return val;
  }
});

})();

/******************************************************************************/
/*********************************** EASING ***********************************/
/******************************************************************************/

(function() {

// based on easing equations from Robert Penner (http://www.robertpenner.com/easing)

var baseEasings = {};

$.each( [ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function( i, name ) {
  baseEasings[ name ] = function( p ) {
	return Math.pow( p, i + 2 );
  };
});

$.extend( baseEasings, {
  Sine: function ( p ) {
	return 1 - Math.cos( p * Math.PI / 2 );
  },
  Circ: function ( p ) {
	return 1 - Math.sqrt( 1 - p * p );
  },
  Elastic: function( p ) {
	return p === 0 || p === 1 ? p :
	  -Math.pow( 2, 8 * (p - 1) ) * Math.sin( ( (p - 1) * 80 - 7.5 ) * Math.PI / 15 );
  },
  Back: function( p ) {
	return p * p * ( 3 * p - 2 );
  },
  Bounce: function ( p ) {
	var pow2,
	  bounce = 4;

	while ( p < ( ( pow2 = Math.pow( 2, --bounce ) ) - 1 ) / 11 ) {}
	return 1 / Math.pow( 4, 3 - bounce ) - 7.5625 * Math.pow( ( pow2 * 3 - 2 ) / 22 - p, 2 );
  }
});

$.each( baseEasings, function( name, easeIn ) {
  $.easing[ "easeIn" + name ] = easeIn;
  $.easing[ "easeOut" + name ] = function( p ) {
	return 1 - easeIn( 1 - p );
  };
  $.easing[ "easeInOut" + name ] = function( p ) {
	return p < 0.5 ?
	  easeIn( p * 2 ) / 2 :
	  1 - easeIn( p * -2 + 2 ) / 2;
  };
});

})();

})(jQuery);
(function( $, undefined ) {

var rvertical = /up|down|vertical/,
  rpositivemotion = /up|left|vertical|horizontal/;

$.effects.effect.blind = function( o, done ) {
  // Create element
  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "height", "width" ],
	mode = $.effects.setMode( el, o.mode || "hide" ),
	direction = o.direction || "up",
	vertical = rvertical.test( direction ),
	ref = vertical ? "height" : "width",
	ref2 = vertical ? "top" : "left",
	motion = rpositivemotion.test( direction ),
	animation = {},
	show = mode === "show",
	wrapper, distance, margin;

  // if already wrapped, the wrapper's properties are my property. #6245
  if ( el.parent().is( ".ui-effects-wrapper" ) ) {
	$.effects.save( el.parent(), props );
  } else {
	$.effects.save( el, props );
  }
  el.show();
  wrapper = $.effects.createWrapper( el ).css({
	overflow: "hidden"
  });

  distance = wrapper[ ref ]();
  margin = parseFloat( wrapper.css( ref2 ) ) || 0;

  animation[ ref ] = show ? distance : 0;
  if ( !motion ) {
	el
	  .css( vertical ? "bottom" : "right", 0 )
	  .css( vertical ? "top" : "left", "auto" )
	  .css({ position: "absolute" });

	animation[ ref2 ] = show ? margin : distance + margin;
  }

  // start at 0 if we are showing
  if ( show ) {
	wrapper.css( ref, 0 );
	if ( ! motion ) {
	  wrapper.css( ref2, margin + distance );
	}
  }

  // Animate
  wrapper.animate( animation, {
	duration: o.duration,
	easing: o.easing,
	queue: false,
	complete: function() {
	  if ( mode === "hide" ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	}
  });

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.bounce = function( o, done ) {
  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "height", "width" ],

	// defaults:
	mode = $.effects.setMode( el, o.mode || "effect" ),
	hide = mode === "hide",
	show = mode === "show",
	direction = o.direction || "up",
	distance = o.distance,
	times = o.times || 5,

	// number of internal animations
	anims = times * 2 + ( show || hide ? 1 : 0 ),
	speed = o.duration / anims,
	easing = o.easing,

	// utility:
	ref = ( direction === "up" || direction === "down" ) ? "top" : "left",
	motion = ( direction === "up" || direction === "left" ),
	i,
	upAnim,
	downAnim,

	// we will need to re-assemble the queue to stack our animations in place
	queue = el.queue(),
	queuelen = queue.length;

  // Avoid touching opacity to prevent clearType and PNG issues in IE
  if ( show || hide ) {
	props.push( "opacity" );
  }

  $.effects.save( el, props );
  el.show();
  $.effects.createWrapper( el ); // Create Wrapper

  // default distance for the BIGGEST bounce is the outer Distance / 3
  if ( !distance ) {
	distance = el[ ref === "top" ? "outerHeight" : "outerWidth" ]() / 3;
  }

  if ( show ) {
	downAnim = { opacity: 1 };
	downAnim[ ref ] = 0;

	// if we are showing, force opacity 0 and set the initial position
	// then do the "first" animation
	el.css( "opacity", 0 )
	  .css( ref, motion ? -distance * 2 : distance * 2 )
	  .animate( downAnim, speed, easing );
  }

  // start at the smallest distance if we are hiding
  if ( hide ) {
	distance = distance / Math.pow( 2, times - 1 );
  }

  downAnim = {};
  downAnim[ ref ] = 0;
  // Bounces up/down/left/right then back to 0 -- times * 2 animations happen here
  for ( i = 0; i < times; i++ ) {
	upAnim = {};
	upAnim[ ref ] = ( motion ? "-=" : "+=" ) + distance;

	el.animate( upAnim, speed, easing )
	  .animate( downAnim, speed, easing );

	distance = hide ? distance * 2 : distance / 2;
  }

  // Last Bounce when Hiding
  if ( hide ) {
	upAnim = { opacity: 0 };
	upAnim[ ref ] = ( motion ? "-=" : "+=" ) + distance;

	el.animate( upAnim, speed, easing );
  }

  el.queue(function() {
	if ( hide ) {
	  el.hide();
	}
	$.effects.restore( el, props );
	$.effects.removeWrapper( el );
	done();
  });

  // inject all the animations we just queued to be first in line (after "inprogress")
  if ( queuelen > 1) {
	queue.splice.apply( queue,
	  [ 1, 0 ].concat( queue.splice( queuelen, anims + 1 ) ) );
  }
  el.dequeue();

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.clip = function( o, done ) {
  // Create element
  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "height", "width" ],
	mode = $.effects.setMode( el, o.mode || "hide" ),
	show = mode === "show",
	direction = o.direction || "vertical",
	vert = direction === "vertical",
	size = vert ? "height" : "width",
	position = vert ? "top" : "left",
	animation = {},
	wrapper, animate, distance;

  // Save & Show
  $.effects.save( el, props );
  el.show();

  // Create Wrapper
  wrapper = $.effects.createWrapper( el ).css({
	overflow: "hidden"
  });
  animate = ( el[0].tagName === "IMG" ) ? wrapper : el;
  distance = animate[ size ]();

  // Shift
  if ( show ) {
	animate.css( size, 0 );
	animate.css( position, distance / 2 );
  }

  // Create Animation Object:
  animation[ size ] = show ? distance : 0;
  animation[ position ] = show ? 0 : distance / 2;

  // Animate
  animate.animate( animation, {
	queue: false,
	duration: o.duration,
	easing: o.easing,
	complete: function() {
	  if ( !show ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	}
  });

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.drop = function( o, done ) {

  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "opacity", "height", "width" ],
	mode = $.effects.setMode( el, o.mode || "hide" ),
	show = mode === "show",
	direction = o.direction || "left",
	ref = ( direction === "up" || direction === "down" ) ? "top" : "left",
	motion = ( direction === "up" || direction === "left" ) ? "pos" : "neg",
	animation = {
	  opacity: show ? 1 : 0
	},
	distance;

  // Adjust
  $.effects.save( el, props );
  el.show();
  $.effects.createWrapper( el );

  distance = o.distance || el[ ref === "top" ? "outerHeight": "outerWidth" ]( true ) / 2;

  if ( show ) {
	el
	  .css( "opacity", 0 )
	  .css( ref, motion === "pos" ? -distance : distance );
  }

  // Animation
  animation[ ref ] = ( show ?
	( motion === "pos" ? "+=" : "-=" ) :
	( motion === "pos" ? "-=" : "+=" ) ) +
	distance;

  // Animate
  el.animate( animation, {
	queue: false,
	duration: o.duration,
	easing: o.easing,
	complete: function() {
	  if ( mode === "hide" ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	}
  });
};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.explode = function( o, done ) {

  var rows = o.pieces ? Math.round( Math.sqrt( o.pieces ) ) : 3,
	cells = rows,
	el = $( this ),
	mode = $.effects.setMode( el, o.mode || "hide" ),
	show = mode === "show",

	// show and then visibility:hidden the element before calculating offset
	offset = el.show().css( "visibility", "hidden" ).offset(),

	// width and height of a piece
	width = Math.ceil( el.outerWidth() / cells ),
	height = Math.ceil( el.outerHeight() / rows ),
	pieces = [],

	// loop
	i, j, left, top, mx, my;

  // children animate complete:
  function childComplete() {
	pieces.push( this );
	if ( pieces.length === rows * cells ) {
	  animComplete();
	}
  }

  // clone the element for each row and cell.
  for( i = 0; i < rows ; i++ ) { // ===>
	top = offset.top + i * height;
	my = i - ( rows - 1 ) / 2 ;

	for( j = 0; j < cells ; j++ ) { // |||
	  left = offset.left + j * width;
	  mx = j - ( cells - 1 ) / 2 ;

	  // Create a clone of the now hidden main element that will be absolute positioned
	  // within a wrapper div off the -left and -top equal to size of our pieces
	  el
		.clone()
		.appendTo( "body" )
		.wrap( "<div></div>" )
		.css({
		  position: "absolute",
		  visibility: "visible",
		  left: -j * width,
		  top: -i * height
		})

	  // select the wrapper - make it overflow: hidden and absolute positioned based on
	  // where the original was located +left and +top equal to the size of pieces
		.parent()
		.addClass( "ui-effects-explode" )
		.css({
		  position: "absolute",
		  overflow: "hidden",
		  width: width,
		  height: height,
		  left: left + ( show ? mx * width : 0 ),
		  top: top + ( show ? my * height : 0 ),
		  opacity: show ? 0 : 1
		}).animate({
		  left: left + ( show ? 0 : mx * width ),
		  top: top + ( show ? 0 : my * height ),
		  opacity: show ? 1 : 0
		}, o.duration || 500, o.easing, childComplete );
	}
  }

  function animComplete() {
	el.css({
	  visibility: "visible"
	});
	$( pieces ).remove();
	if ( !show ) {
	  el.hide();
	}
	done();
  }
};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.fade = function( o, done ) {
  var el = $( this ),
	mode = $.effects.setMode( el, o.mode || "toggle" );

  el.animate({
	opacity: mode
  }, {
	queue: false,
	duration: o.duration,
	easing: o.easing,
	complete: done
  });
};

})( jQuery );
(function( $, undefined ) {

$.effects.effect.fold = function( o, done ) {

  // Create element
  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "height", "width" ],
	mode = $.effects.setMode( el, o.mode || "hide" ),
	show = mode === "show",
	hide = mode === "hide",
	size = o.size || 15,
	percent = /([0-9]+)%/.exec( size ),
	horizFirst = !!o.horizFirst,
	widthFirst = show !== horizFirst,
	ref = widthFirst ? [ "width", "height" ] : [ "height", "width" ],
	duration = o.duration / 2,
	wrapper, distance,
	animation1 = {},
	animation2 = {};

  $.effects.save( el, props );
  el.show();

  // Create Wrapper
  wrapper = $.effects.createWrapper( el ).css({
	overflow: "hidden"
  });
  distance = widthFirst ?
	[ wrapper.width(), wrapper.height() ] :
	[ wrapper.height(), wrapper.width() ];

  if ( percent ) {
	size = parseInt( percent[ 1 ], 10 ) / 100 * distance[ hide ? 0 : 1 ];
  }
  if ( show ) {
	wrapper.css( horizFirst ? {
	  height: 0,
	  width: size
	} : {
	  height: size,
	  width: 0
	});
  }

  // Animation
  animation1[ ref[ 0 ] ] = show ? distance[ 0 ] : size;
  animation2[ ref[ 1 ] ] = show ? distance[ 1 ] : 0;

  // Animate
  wrapper
	.animate( animation1, duration, o.easing )
	.animate( animation2, duration, o.easing, function() {
	  if ( hide ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	});

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.highlight = function( o, done ) {
  var elem = $( this ),
	props = [ "backgroundImage", "backgroundColor", "opacity" ],
	mode = $.effects.setMode( elem, o.mode || "show" ),
	animation = {
	  backgroundColor: elem.css( "backgroundColor" )
	};

  if (mode === "hide") {
	animation.opacity = 0;
  }

  $.effects.save( elem, props );

  elem
	.show()
	.css({
	  backgroundImage: "none",
	  backgroundColor: o.color || "#ffff99"
	})
	.animate( animation, {
	  queue: false,
	  duration: o.duration,
	  easing: o.easing,
	  complete: function() {
		if ( mode === "hide" ) {
		  elem.hide();
		}
		$.effects.restore( elem, props );
		done();
	  }
	});
};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.pulsate = function( o, done ) {
  var elem = $( this ),
	mode = $.effects.setMode( elem, o.mode || "show" ),
	show = mode === "show",
	hide = mode === "hide",
	showhide = ( show || mode === "hide" ),

	// showing or hiding leaves of the "last" animation
	anims = ( ( o.times || 5 ) * 2 ) + ( showhide ? 1 : 0 ),
	duration = o.duration / anims,
	animateTo = 0,
	queue = elem.queue(),
	queuelen = queue.length,
	i;

  if ( show || !elem.is(":visible")) {
	elem.css( "opacity", 0 ).show();
	animateTo = 1;
  }

  // anims - 1 opacity "toggles"
  for ( i = 1; i < anims; i++ ) {
	elem.animate({
	  opacity: animateTo
	}, duration, o.easing );
	animateTo = 1 - animateTo;
  }

  elem.animate({
	opacity: animateTo
  }, duration, o.easing);

  elem.queue(function() {
	if ( hide ) {
	  elem.hide();
	}
	done();
  });

  // We just queued up "anims" animations, we need to put them next in the queue
  if ( queuelen > 1 ) {
	queue.splice.apply( queue,
	  [ 1, 0 ].concat( queue.splice( queuelen, anims + 1 ) ) );
  }
  elem.dequeue();
};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.puff = function( o, done ) {
  var elem = $( this ),
	mode = $.effects.setMode( elem, o.mode || "hide" ),
	hide = mode === "hide",
	percent = parseInt( o.percent, 10 ) || 150,
	factor = percent / 100,
	original = {
	  height: elem.height(),
	  width: elem.width(),
	  outerHeight: elem.outerHeight(),
	  outerWidth: elem.outerWidth()
	};

  $.extend( o, {
	effect: "scale",
	queue: false,
	fade: true,
	mode: mode,
	complete: done,
	percent: hide ? percent : 100,
	from: hide ?
	  original :
	  {
		height: original.height * factor,
		width: original.width * factor,
		outerHeight: original.outerHeight * factor,
		outerWidth: original.outerWidth * factor
	  }
  });

  elem.effect( o );
};

$.effects.effect.scale = function( o, done ) {

  // Create element
  var el = $( this ),
	options = $.extend( true, {}, o ),
	mode = $.effects.setMode( el, o.mode || "effect" ),
	percent = parseInt( o.percent, 10 ) ||
	  ( parseInt( o.percent, 10 ) === 0 ? 0 : ( mode === "hide" ? 0 : 100 ) ),
	direction = o.direction || "both",
	origin = o.origin,
	original = {
	  height: el.height(),
	  width: el.width(),
	  outerHeight: el.outerHeight(),
	  outerWidth: el.outerWidth()
	},
	factor = {
	  y: direction !== "horizontal" ? (percent / 100) : 1,
	  x: direction !== "vertical" ? (percent / 100) : 1
	};

  // We are going to pass this effect to the size effect:
  options.effect = "size";
  options.queue = false;
  options.complete = done;

  // Set default origin and restore for show/hide
  if ( mode !== "effect" ) {
	options.origin = origin || ["middle","center"];
	options.restore = true;
  }

  options.from = o.from || ( mode === "show" ? {
	height: 0,
	width: 0,
	outerHeight: 0,
	outerWidth: 0
  } : original );
  options.to = {
	height: original.height * factor.y,
	width: original.width * factor.x,
	outerHeight: original.outerHeight * factor.y,
	outerWidth: original.outerWidth * factor.x
  };

  // Fade option to support puff
  if ( options.fade ) {
	if ( mode === "show" ) {
	  options.from.opacity = 0;
	  options.to.opacity = 1;
	}
	if ( mode === "hide" ) {
	  options.from.opacity = 1;
	  options.to.opacity = 0;
	}
  }

  // Animate
  el.effect( options );

};

$.effects.effect.size = function( o, done ) {

  // Create element
  var original, baseline, factor,
	el = $( this ),
	props0 = [ "position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity" ],

	// Always restore
	props1 = [ "position", "top", "bottom", "left", "right", "overflow", "opacity" ],

	// Copy for children
	props2 = [ "width", "height", "overflow" ],
	cProps = [ "fontSize" ],
	vProps = [ "borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom" ],
	hProps = [ "borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight" ],

	// Set options
	mode = $.effects.setMode( el, o.mode || "effect" ),
	restore = o.restore || mode !== "effect",
	scale = o.scale || "both",
	origin = o.origin || [ "middle", "center" ],
	position = el.css( "position" ),
	props = restore ? props0 : props1,
	zero = {
	  height: 0,
	  width: 0,
	  outerHeight: 0,
	  outerWidth: 0
	};

  if ( mode === "show" ) {
	el.show();
  }
  original = {
	height: el.height(),
	width: el.width(),
	outerHeight: el.outerHeight(),
	outerWidth: el.outerWidth()
  };

  if ( o.mode === "toggle" && mode === "show" ) {
	el.from = o.to || zero;
	el.to = o.from || original;
  } else {
	el.from = o.from || ( mode === "show" ? zero : original );
	el.to = o.to || ( mode === "hide" ? zero : original );
  }

  // Set scaling factor
  factor = {
	from: {
	  y: el.from.height / original.height,
	  x: el.from.width / original.width
	},
	to: {
	  y: el.to.height / original.height,
	  x: el.to.width / original.width
	}
  };

  // Scale the css box
  if ( scale === "box" || scale === "both" ) {

	// Vertical props scaling
	if ( factor.from.y !== factor.to.y ) {
	  props = props.concat( vProps );
	  el.from = $.effects.setTransition( el, vProps, factor.from.y, el.from );
	  el.to = $.effects.setTransition( el, vProps, factor.to.y, el.to );
	}

	// Horizontal props scaling
	if ( factor.from.x !== factor.to.x ) {
	  props = props.concat( hProps );
	  el.from = $.effects.setTransition( el, hProps, factor.from.x, el.from );
	  el.to = $.effects.setTransition( el, hProps, factor.to.x, el.to );
	}
  }

  // Scale the content
  if ( scale === "content" || scale === "both" ) {

	// Vertical props scaling
	if ( factor.from.y !== factor.to.y ) {
	  props = props.concat( cProps ).concat( props2 );
	  el.from = $.effects.setTransition( el, cProps, factor.from.y, el.from );
	  el.to = $.effects.setTransition( el, cProps, factor.to.y, el.to );
	}
  }

  $.effects.save( el, props );
  el.show();
  $.effects.createWrapper( el );
  el.css( "overflow", "hidden" ).css( el.from );

  // Adjust
  if (origin) { // Calculate baseline shifts
	baseline = $.effects.getBaseline( origin, original );
	el.from.top = ( original.outerHeight - el.outerHeight() ) * baseline.y;
	el.from.left = ( original.outerWidth - el.outerWidth() ) * baseline.x;
	el.to.top = ( original.outerHeight - el.to.outerHeight ) * baseline.y;
	el.to.left = ( original.outerWidth - el.to.outerWidth ) * baseline.x;
  }
  el.css( el.from ); // set top & left

  // Animate
  if ( scale === "content" || scale === "both" ) { // Scale the children

	// Add margins/font-size
	vProps = vProps.concat([ "marginTop", "marginBottom" ]).concat(cProps);
	hProps = hProps.concat([ "marginLeft", "marginRight" ]);
	props2 = props0.concat(vProps).concat(hProps);

	el.find( "*[width]" ).each( function(){
	  var child = $( this ),
		c_original = {
		  height: child.height(),
		  width: child.width(),
		  outerHeight: child.outerHeight(),
		  outerWidth: child.outerWidth()
		};
	  if (restore) {
		$.effects.save(child, props2);
	  }

	  child.from = {
		height: c_original.height * factor.from.y,
		width: c_original.width * factor.from.x,
		outerHeight: c_original.outerHeight * factor.from.y,
		outerWidth: c_original.outerWidth * factor.from.x
	  };
	  child.to = {
		height: c_original.height * factor.to.y,
		width: c_original.width * factor.to.x,
		outerHeight: c_original.height * factor.to.y,
		outerWidth: c_original.width * factor.to.x
	  };

	  // Vertical props scaling
	  if ( factor.from.y !== factor.to.y ) {
		child.from = $.effects.setTransition( child, vProps, factor.from.y, child.from );
		child.to = $.effects.setTransition( child, vProps, factor.to.y, child.to );
	  }

	  // Horizontal props scaling
	  if ( factor.from.x !== factor.to.x ) {
		child.from = $.effects.setTransition( child, hProps, factor.from.x, child.from );
		child.to = $.effects.setTransition( child, hProps, factor.to.x, child.to );
	  }

	  // Animate children
	  child.css( child.from );
	  child.animate( child.to, o.duration, o.easing, function() {

		// Restore children
		if ( restore ) {
		  $.effects.restore( child, props2 );
		}
	  });
	});
  }

  // Animate
  el.animate( el.to, {
	queue: false,
	duration: o.duration,
	easing: o.easing,
	complete: function() {
	  if ( el.to.opacity === 0 ) {
		el.css( "opacity", el.from.opacity );
	  }
	  if( mode === "hide" ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  if ( !restore ) {

		// we need to calculate our new positioning based on the scaling
		if ( position === "static" ) {
		  el.css({
			position: "relative",
			top: el.to.top,
			left: el.to.left
		  });
		} else {
		  $.each([ "top", "left" ], function( idx, pos ) {
			el.css( pos, function( _, str ) {
			  var val = parseInt( str, 10 ),
				toRef = idx ? el.to.left : el.to.top;

			  // if original was "auto", recalculate the new value from wrapper
			  if ( str === "auto" ) {
				return toRef + "px";
			  }

			  return val + toRef + "px";
			});
		  });
		}
	  }

	  $.effects.removeWrapper( el );
	  done();
	}
  });

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.shake = function( o, done ) {

  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "height", "width" ],
	mode = $.effects.setMode( el, o.mode || "effect" ),
	direction = o.direction || "left",
	distance = o.distance || 20,
	times = o.times || 3,
	anims = times * 2 + 1,
	speed = Math.round(o.duration/anims),
	ref = (direction === "up" || direction === "down") ? "top" : "left",
	positiveMotion = (direction === "up" || direction === "left"),
	animation = {},
	animation1 = {},
	animation2 = {},
	i,

	// we will need to re-assemble the queue to stack our animations in place
	queue = el.queue(),
	queuelen = queue.length;

  $.effects.save( el, props );
  el.show();
  $.effects.createWrapper( el );

  // Animation
  animation[ ref ] = ( positiveMotion ? "-=" : "+=" ) + distance;
  animation1[ ref ] = ( positiveMotion ? "+=" : "-=" ) + distance * 2;
  animation2[ ref ] = ( positiveMotion ? "-=" : "+=" ) + distance * 2;

  // Animate
  el.animate( animation, speed, o.easing );

  // Shakes
  for ( i = 1; i < times; i++ ) {
	el.animate( animation1, speed, o.easing ).animate( animation2, speed, o.easing );
  }
  el
	.animate( animation1, speed, o.easing )
	.animate( animation, speed / 2, o.easing )
	.queue(function() {
	  if ( mode === "hide" ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	});

  // inject all the animations we just queued to be first in line (after "inprogress")
  if ( queuelen > 1) {
	queue.splice.apply( queue,
	  [ 1, 0 ].concat( queue.splice( queuelen, anims + 1 ) ) );
  }
  el.dequeue();

};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.slide = function( o, done ) {

  // Create element
  var el = $( this ),
	props = [ "position", "top", "bottom", "left", "right", "width", "height" ],
	mode = $.effects.setMode( el, o.mode || "show" ),
	show = mode === "show",
	direction = o.direction || "left",
	ref = (direction === "up" || direction === "down") ? "top" : "left",
	positiveMotion = (direction === "up" || direction === "left"),
	distance,
	animation = {};

  // Adjust
  $.effects.save( el, props );
  el.show();
  distance = o.distance || el[ ref === "top" ? "outerHeight" : "outerWidth" ]( true );

  $.effects.createWrapper( el ).css({
	overflow: "hidden"
  });

  if ( show ) {
	el.css( ref, positiveMotion ? (isNaN(distance) ? "-" + distance : -distance) : distance );
  }

  // Animation
  animation[ ref ] = ( show ?
	( positiveMotion ? "+=" : "-=") :
	( positiveMotion ? "-=" : "+=")) +
	distance;

  // Animate
  el.animate( animation, {
	queue: false,
	duration: o.duration,
	easing: o.easing,
	complete: function() {
	  if ( mode === "hide" ) {
		el.hide();
	  }
	  $.effects.restore( el, props );
	  $.effects.removeWrapper( el );
	  done();
	}
  });
};

})(jQuery);
(function( $, undefined ) {

$.effects.effect.transfer = function( o, done ) {
  var elem = $( this ),
	target = $( o.to ),
	targetFixed = target.css( "position" ) === "fixed",
	body = $("body"),
	fixTop = targetFixed ? body.scrollTop() : 0,
	fixLeft = targetFixed ? body.scrollLeft() : 0,
	endPosition = target.offset(),
	animation = {
	  top: endPosition.top - fixTop ,
	  left: endPosition.left - fixLeft ,
	  height: target.innerHeight(),
	  width: target.innerWidth()
	},
	startPosition = elem.offset(),
	transfer = $( "<div class='ui-effects-transfer'></div>" )
	  .appendTo( document.body )
	  .addClass( o.className )
	  .css({
		top: startPosition.top - fixTop ,
		left: startPosition.left - fixLeft ,
		height: elem.innerHeight(),
		width: elem.innerWidth(),
		position: targetFixed ? "fixed" : "absolute"
	  })
	  .animate( animation, o.duration, o.easing, function() {
		transfer.remove();
		done();
	  });
};

})(jQuery);

//} FIM DE jquery-ui-1.10.4.custom.js

//{ AngularJs v1.6.6
/*
 AngularJS v1.6.6
 (c) 2010-2017 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(u){'use strict';function oe(a){if(E(a))t(a.objectMaxDepth)&&(Lc.objectMaxDepth=Ub(a.objectMaxDepth)?a.objectMaxDepth:NaN);else return Lc}function Ub(a){return Y(a)&&0<a}function M(a,b){b=b||Error;return function(){var d=arguments[0],c;c="["+(a?a+":":"")+d+"] http://errors.angularjs.org/1.6.6/"+(a?a+"/":"")+d;for(d=1;d<arguments.length;d++){c=c+(1==d?"?":"&")+"p"+(d-1)+"=";var e=encodeURIComponent,f;f=arguments[d];f="function"==typeof f?f.toString().replace(/ \{[\s\S]*$/,""):"undefined"==
typeof f?"undefined":"string"!=typeof f?JSON.stringify(f):f;c+=e(f)}return new b(c)}}function xa(a){if(null==a||$a(a))return!1;if(I(a)||D(a)||B&&a instanceof B)return!0;var b="length"in Object(a)&&a.length;return Y(b)&&(0<=b&&(b-1 in a||a instanceof Array)||"function"===typeof a.item)}function p(a,b,d){var c,e;if(a)if(A(a))for(c in a)"prototype"!==c&&"length"!==c&&"name"!==c&&a.hasOwnProperty(c)&&b.call(d,a[c],c,a);else if(I(a)||xa(a)){var f="object"!==typeof a;c=0;for(e=a.length;c<e;c++)(f||c in
a)&&b.call(d,a[c],c,a)}else if(a.forEach&&a.forEach!==p)a.forEach(b,d,a);else if(Mc(a))for(c in a)b.call(d,a[c],c,a);else if("function"===typeof a.hasOwnProperty)for(c in a)a.hasOwnProperty(c)&&b.call(d,a[c],c,a);else for(c in a)ra.call(a,c)&&b.call(d,a[c],c,a);return a}function Nc(a,b,d){for(var c=Object.keys(a).sort(),e=0;e<c.length;e++)b.call(d,a[c[e]],c[e]);return c}function Vb(a){return function(b,d){a(d,b)}}function pe(){return++sb}function Wb(a,b,d){for(var c=a.$$hashKey,e=0,f=b.length;e<f;++e){var g=
b[e];if(E(g)||A(g))for(var k=Object.keys(g),h=0,l=k.length;h<l;h++){var m=k[h],n=g[m];d&&E(n)?ea(n)?a[m]=new Date(n.valueOf()):ab(n)?a[m]=new RegExp(n):n.nodeName?a[m]=n.cloneNode(!0):Xb(n)?a[m]=n.clone():(E(a[m])||(a[m]=I(n)?[]:{}),Wb(a[m],[n],!0)):a[m]=n}}c?a.$$hashKey=c:delete a.$$hashKey;return a}function P(a){return Wb(a,ya.call(arguments,1),!1)}function qe(a){return Wb(a,ya.call(arguments,1),!0)}function Z(a){return parseInt(a,10)}function Yb(a,b){return P(Object.create(a),b)}function C(){}
function bb(a){return a}function ka(a){return function(){return a}}function Zb(a){return A(a.toString)&&a.toString!==ha}function w(a){return"undefined"===typeof a}function t(a){return"undefined"!==typeof a}function E(a){return null!==a&&"object"===typeof a}function Mc(a){return null!==a&&"object"===typeof a&&!Oc(a)}function D(a){return"string"===typeof a}function Y(a){return"number"===typeof a}function ea(a){return"[object Date]"===ha.call(a)}function $b(a){switch(ha.call(a)){case "[object Error]":return!0;
case "[object Exception]":return!0;case "[object DOMException]":return!0;default:return a instanceof Error}}function A(a){return"function"===typeof a}function ab(a){return"[object RegExp]"===ha.call(a)}function $a(a){return a&&a.window===a}function cb(a){return a&&a.$evalAsync&&a.$watch}function Na(a){return"boolean"===typeof a}function re(a){return a&&Y(a.length)&&se.test(ha.call(a))}function Xb(a){return!(!a||!(a.nodeName||a.prop&&a.attr&&a.find))}function te(a){var b={};a=a.split(",");var d;for(d=
0;d<a.length;d++)b[a[d]]=!0;return b}function za(a){return N(a.nodeName||a[0]&&a[0].nodeName)}function db(a,b){var d=a.indexOf(b);0<=d&&a.splice(d,1);return d}function pa(a,b,d){function c(a,b,c){c--;if(0>c)return"...";var d=b.$$hashKey,g;if(I(a)){g=0;for(var f=a.length;g<f;g++)b.push(e(a[g],c))}else if(Mc(a))for(g in a)b[g]=e(a[g],c);else if(a&&"function"===typeof a.hasOwnProperty)for(g in a)a.hasOwnProperty(g)&&(b[g]=e(a[g],c));else for(g in a)ra.call(a,g)&&(b[g]=e(a[g],c));d?b.$$hashKey=d:delete b.$$hashKey;
return b}function e(a,b){if(!E(a))return a;var d=g.indexOf(a);if(-1!==d)return k[d];if($a(a)||cb(a))throw qa("cpws");var d=!1,e=f(a);void 0===e&&(e=I(a)?[]:Object.create(Oc(a)),d=!0);g.push(a);k.push(e);return d?c(a,e,b):e}function f(a){switch(ha.call(a)){case "[object Int8Array]":case "[object Int16Array]":case "[object Int32Array]":case "[object Float32Array]":case "[object Float64Array]":case "[object Uint8Array]":case "[object Uint8ClampedArray]":case "[object Uint16Array]":case "[object Uint32Array]":return new a.constructor(e(a.buffer),
a.byteOffset,a.length);case "[object ArrayBuffer]":if(!a.slice){var b=new ArrayBuffer(a.byteLength);(new Uint8Array(b)).set(new Uint8Array(a));return b}return a.slice(0);case "[object Boolean]":case "[object Number]":case "[object String]":case "[object Date]":return new a.constructor(a.valueOf());case "[object RegExp]":return b=new RegExp(a.source,a.toString().match(/[^/]*$/)[0]),b.lastIndex=a.lastIndex,b;case "[object Blob]":return new a.constructor([a],{type:a.type})}if(A(a.cloneNode))return a.cloneNode(!0)}
var g=[],k=[];d=Ub(d)?d:NaN;if(b){if(re(b)||"[object ArrayBuffer]"===ha.call(b))throw qa("cpta");if(a===b)throw qa("cpi");I(b)?b.length=0:p(b,function(a,c){"$$hashKey"!==c&&delete b[c]});g.push(a);k.push(b);return c(a,b,d)}return e(a,d)}function ac(a,b){return a===b||a!==a&&b!==b}function sa(a,b){if(a===b)return!0;if(null===a||null===b)return!1;if(a!==a&&b!==b)return!0;var d=typeof a,c;if(d===typeof b&&"object"===d)if(I(a)){if(!I(b))return!1;if((d=a.length)===b.length){for(c=0;c<d;c++)if(!sa(a[c],
b[c]))return!1;return!0}}else{if(ea(a))return ea(b)?ac(a.getTime(),b.getTime()):!1;if(ab(a))return ab(b)?a.toString()===b.toString():!1;if(cb(a)||cb(b)||$a(a)||$a(b)||I(b)||ea(b)||ab(b))return!1;d=S();for(c in a)if("$"!==c.charAt(0)&&!A(a[c])){if(!sa(a[c],b[c]))return!1;d[c]=!0}for(c in b)if(!(c in d)&&"$"!==c.charAt(0)&&t(b[c])&&!A(b[c]))return!1;return!0}return!1}function eb(a,b,d){return a.concat(ya.call(b,d))}function Ra(a,b){var d=2<arguments.length?ya.call(arguments,2):[];return!A(b)||b instanceof
RegExp?b:d.length?function(){return arguments.length?b.apply(a,eb(d,arguments,0)):b.apply(a,d)}:function(){return arguments.length?b.apply(a,arguments):b.call(a)}}function Pc(a,b){var d=b;"string"===typeof a&&"$"===a.charAt(0)&&"$"===a.charAt(1)?d=void 0:$a(b)?d="$WINDOW":b&&u.document===b?d="$DOCUMENT":cb(b)&&(d="$SCOPE");return d}function fb(a,b){if(!w(a))return Y(b)||(b=b?2:null),JSON.stringify(a,Pc,b)}function Qc(a){return D(a)?JSON.parse(a):a}function Rc(a,b){a=a.replace(ue,"");var d=Date.parse("Jan 01, 1970 00:00:00 "+
a)/6E4;return T(d)?b:d}function bc(a,b,d){d=d?-1:1;var c=a.getTimezoneOffset();b=Rc(b,c);d*=b-c;a=new Date(a.getTime());a.setMinutes(a.getMinutes()+d);return a}function Aa(a){a=B(a).clone().empty();var b=B("<div>").append(a).html();try{return a[0].nodeType===Oa?N(b):b.match(/^(<[^>]+>)/)[1].replace(/^<([\w-]+)/,function(a,b){return"<"+N(b)})}catch(d){return N(b)}}function Sc(a){try{return decodeURIComponent(a)}catch(b){}}function Tc(a){var b={};p((a||"").split("&"),function(a){var c,e,f;a&&(e=a=a.replace(/\+/g,
"%20"),c=a.indexOf("="),-1!==c&&(e=a.substring(0,c),f=a.substring(c+1)),e=Sc(e),t(e)&&(f=t(f)?Sc(f):!0,ra.call(b,e)?I(b[e])?b[e].push(f):b[e]=[b[e],f]:b[e]=f))});return b}function cc(a){var b=[];p(a,function(a,c){I(a)?p(a,function(a){b.push(ia(c,!0)+(!0===a?"":"="+ia(a,!0)))}):b.push(ia(c,!0)+(!0===a?"":"="+ia(a,!0)))});return b.length?b.join("&"):""}function gb(a){return ia(a,!0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function ia(a,b){return encodeURIComponent(a).replace(/%40/gi,
"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%3B/gi,";").replace(/%20/g,b?"%20":"+")}function ve(a,b){var d,c,e=Ha.length;for(c=0;c<e;++c)if(d=Ha[c]+b,D(d=a.getAttribute(d)))return d;return null}function we(a,b){var d,c,e={};p(Ha,function(b){b+="app";!d&&a.hasAttribute&&a.hasAttribute(b)&&(d=a,c=a.getAttribute(b))});p(Ha,function(b){b+="app";var e;!d&&(e=a.querySelector("["+b.replace(":","\\:")+"]"))&&(d=e,c=e.getAttribute(b))});d&&(xe?(e.strictDi=null!==ve(d,"strict-di"),
b(d,c?[c]:[],e)):u.console.error("Angular: disabling automatic bootstrap. <script> protocol indicates an extension, document.location.href does not match."))}function Uc(a,b,d){E(d)||(d={});d=P({strictDi:!1},d);var c=function(){a=B(a);if(a.injector()){var c=a[0]===u.document?"document":Aa(a);throw qa("btstrpd",c.replace(/</,"&lt;").replace(/>/,"&gt;"));}b=b||[];b.unshift(["$provide",function(b){b.value("$rootElement",a)}]);d.debugInfoEnabled&&b.push(["$compileProvider",function(a){a.debugInfoEnabled(!0)}]);
b.unshift("ng");c=hb(b,d.strictDi);c.invoke(["$rootScope","$rootElement","$compile","$injector",function(a,b,c,d){a.$apply(function(){b.data("$injector",d);c(b)(a)})}]);return c},e=/^NG_ENABLE_DEBUG_INFO!/,f=/^NG_DEFER_BOOTSTRAP!/;u&&e.test(u.name)&&(d.debugInfoEnabled=!0,u.name=u.name.replace(e,""));if(u&&!f.test(u.name))return c();u.name=u.name.replace(f,"");$.resumeBootstrap=function(a){p(a,function(a){b.push(a)});return c()};A($.resumeDeferredBootstrap)&&$.resumeDeferredBootstrap()}function ye(){u.name=
"NG_ENABLE_DEBUG_INFO!"+u.name;u.location.reload()}function ze(a){a=$.element(a).injector();if(!a)throw qa("test");return a.get("$$testability")}function Vc(a,b){b=b||"_";return a.replace(Ae,function(a,c){return(c?b:"")+a.toLowerCase()})}function Be(){var a;if(!Wc){var b=tb();(la=w(b)?u.jQuery:b?u[b]:void 0)&&la.fn.on?(B=la,P(la.fn,{scope:Sa.scope,isolateScope:Sa.isolateScope,controller:Sa.controller,injector:Sa.injector,inheritedData:Sa.inheritedData}),a=la.cleanData,la.cleanData=function(b){for(var c,
e=0,f;null!=(f=b[e]);e++)(c=la._data(f,"events"))&&c.$destroy&&la(f).triggerHandler("$destroy");a(b)}):B=U;$.element=B;Wc=!0}}function ib(a,b,d){if(!a)throw qa("areq",b||"?",d||"required");return a}function ub(a,b,d){d&&I(a)&&(a=a[a.length-1]);ib(A(a),b,"not a function, got "+(a&&"object"===typeof a?a.constructor.name||"Object":typeof a));return a}function Ia(a,b){if("hasOwnProperty"===a)throw qa("badname",b);}function Xc(a,b,d){if(!b)return a;b=b.split(".");for(var c,e=a,f=b.length,g=0;g<f;g++)c=
b[g],a&&(a=(e=a)[c]);return!d&&A(a)?Ra(e,a):a}function vb(a){for(var b=a[0],d=a[a.length-1],c,e=1;b!==d&&(b=b.nextSibling);e++)if(c||a[e]!==b)c||(c=B(ya.call(a,0,e))),c.push(b);return c||a}function S(){return Object.create(null)}function dc(a){if(null==a)return"";switch(typeof a){case "string":break;case "number":a=""+a;break;default:a=!Zb(a)||I(a)||ea(a)?fb(a):a.toString()}return a}function Ce(a){function b(a,b,c){return a[b]||(a[b]=c())}var d=M("$injector"),c=M("ng");a=b(a,"angular",Object);a.$$minErr=
a.$$minErr||M;return b(a,"module",function(){var a={};return function(f,g,k){var h={};if("hasOwnProperty"===f)throw c("badname","module");g&&a.hasOwnProperty(f)&&(a[f]=null);return b(a,f,function(){function a(b,c,d,g){g||(g=e);return function(){g[d||"push"]([b,c,arguments]);return p}}function b(a,c,d){d||(d=e);return function(b,e){e&&A(e)&&(e.$$moduleName=f);d.push([a,c,arguments]);return p}}if(!g)throw d("nomod",f);var e=[],q=[],G=[],L=a("$injector","invoke","push",q),p={_invokeQueue:e,_configBlocks:q,
_runBlocks:G,info:function(a){if(t(a)){if(!E(a))throw c("aobj","value");h=a;return this}return h},requires:g,name:f,provider:b("$provide","provider"),factory:b("$provide","factory"),service:b("$provide","service"),value:a("$provide","value"),constant:a("$provide","constant","unshift"),decorator:b("$provide","decorator",q),animation:b("$animateProvider","register"),filter:b("$filterProvider","register"),controller:b("$controllerProvider","register"),directive:b("$compileProvider","directive"),component:b("$compileProvider",
"component"),config:L,run:function(a){G.push(a);return this}};k&&L(k);return p})}})}function ja(a,b){if(I(a)){b=b||[];for(var d=0,c=a.length;d<c;d++)b[d]=a[d]}else if(E(a))for(d in b=b||{},a)if("$"!==d.charAt(0)||"$"!==d.charAt(1))b[d]=a[d];return b||a}function De(a,b){var d=[];Ub(b)&&(a=$.copy(a,null,b));return JSON.stringify(a,function(a,b){b=Pc(a,b);if(E(b)){if(0<=d.indexOf(b))return"...";d.push(b)}return b})}function Ee(a){P(a,{errorHandlingConfig:oe,bootstrap:Uc,copy:pa,extend:P,merge:qe,equals:sa,
element:B,forEach:p,injector:hb,noop:C,bind:Ra,toJson:fb,fromJson:Qc,identity:bb,isUndefined:w,isDefined:t,isString:D,isFunction:A,isObject:E,isNumber:Y,isElement:Xb,isArray:I,version:Fe,isDate:ea,lowercase:N,uppercase:wb,callbacks:{$$counter:0},getTestability:ze,reloadWithDebugInfo:ye,$$minErr:M,$$csp:Ja,$$encodeUriSegment:gb,$$encodeUriQuery:ia,$$stringify:dc});ec=Ce(u);ec("ng",["ngLocale"],["$provide",function(a){a.provider({$$sanitizeUri:Ge});a.provider("$compile",Yc).directive({a:He,input:Zc,
textarea:Zc,form:Ie,script:Je,select:Ke,option:Le,ngBind:Me,ngBindHtml:Ne,ngBindTemplate:Oe,ngClass:Pe,ngClassEven:Qe,ngClassOdd:Re,ngCloak:Se,ngController:Te,ngForm:Ue,ngHide:Ve,ngIf:We,ngInclude:Xe,ngInit:Ye,ngNonBindable:Ze,ngPluralize:$e,ngRepeat:af,ngShow:bf,ngStyle:cf,ngSwitch:df,ngSwitchWhen:ef,ngSwitchDefault:ff,ngOptions:gf,ngTransclude:hf,ngModel:jf,ngList:kf,ngChange:lf,pattern:$c,ngPattern:$c,required:ad,ngRequired:ad,minlength:bd,ngMinlength:bd,maxlength:cd,ngMaxlength:cd,ngValue:mf,
ngModelOptions:nf}).directive({ngInclude:of}).directive(xb).directive(dd);a.provider({$anchorScroll:pf,$animate:qf,$animateCss:rf,$$animateJs:sf,$$animateQueue:tf,$$AnimateRunner:uf,$$animateAsyncRun:vf,$browser:wf,$cacheFactory:xf,$controller:yf,$document:zf,$$isDocumentHidden:Af,$exceptionHandler:Bf,$filter:ed,$$forceReflow:Cf,$interpolate:Df,$interval:Ef,$http:Ff,$httpParamSerializer:Gf,$httpParamSerializerJQLike:Hf,$httpBackend:If,$xhrFactory:Jf,$jsonpCallbacks:Kf,$location:Lf,$log:Mf,$parse:Nf,
$rootScope:Of,$q:Pf,$$q:Qf,$sce:Rf,$sceDelegate:Sf,$sniffer:Tf,$templateCache:Uf,$templateRequest:Vf,$$testability:Wf,$timeout:Xf,$window:Yf,$$rAF:Zf,$$jqLite:$f,$$Map:ag,$$cookieReader:bg})}]).info({angularVersion:"1.6.6"})}function jb(a,b){return b.toUpperCase()}function yb(a){return a.replace(cg,jb)}function fc(a){a=a.nodeType;return 1===a||!a||9===a}function fd(a,b){var d,c,e=b.createDocumentFragment(),f=[];if(gc.test(a)){d=e.appendChild(b.createElement("div"));c=(dg.exec(a)||["",""])[1].toLowerCase();
c=aa[c]||aa._default;d.innerHTML=c[1]+a.replace(eg,"<$1></$2>")+c[2];for(c=c[0];c--;)d=d.lastChild;f=eb(f,d.childNodes);d=e.firstChild;d.textContent=""}else f.push(b.createTextNode(a));e.textContent="";e.innerHTML="";p(f,function(a){e.appendChild(a)});return e}function U(a){if(a instanceof U)return a;var b;D(a)&&(a=Q(a),b=!0);if(!(this instanceof U)){if(b&&"<"!==a.charAt(0))throw hc("nosel");return new U(a)}if(b){b=u.document;var d;a=(d=fg.exec(a))?[b.createElement(d[1])]:(d=fd(a,b))?d.childNodes:
[];ic(this,a)}else A(a)?gd(a):ic(this,a)}function jc(a){return a.cloneNode(!0)}function zb(a,b){!b&&fc(a)&&B.cleanData([a]);a.querySelectorAll&&B.cleanData(a.querySelectorAll("*"))}function hd(a,b,d,c){if(t(c))throw hc("offargs");var e=(c=Ab(a))&&c.events,f=c&&c.handle;if(f)if(b){var g=function(b){var c=e[b];t(d)&&db(c||[],d);t(d)&&c&&0<c.length||(a.removeEventListener(b,f),delete e[b])};p(b.split(" "),function(a){g(a);Bb[a]&&g(Bb[a])})}else for(b in e)"$destroy"!==b&&a.removeEventListener(b,f),delete e[b]}
function kc(a,b){var d=a.ng339,c=d&&kb[d];c&&(b?delete c.data[b]:(c.handle&&(c.events.$destroy&&c.handle({},"$destroy"),hd(a)),delete kb[d],a.ng339=void 0))}function Ab(a,b){var d=a.ng339,d=d&&kb[d];b&&!d&&(a.ng339=d=++gg,d=kb[d]={events:{},data:{},handle:void 0});return d}function lc(a,b,d){if(fc(a)){var c,e=t(d),f=!e&&b&&!E(b),g=!b;a=(a=Ab(a,!f))&&a.data;if(e)a[yb(b)]=d;else{if(g)return a;if(f)return a&&a[yb(b)];for(c in b)a[yb(c)]=b[c]}}}function Cb(a,b){return a.getAttribute?-1<(" "+(a.getAttribute("class")||
"")+" ").replace(/[\n\t]/g," ").indexOf(" "+b+" "):!1}function Db(a,b){b&&a.setAttribute&&p(b.split(" "),function(b){a.setAttribute("class",Q((" "+(a.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").replace(" "+Q(b)+" "," ")))})}function Eb(a,b){if(b&&a.setAttribute){var d=(" "+(a.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ");p(b.split(" "),function(a){a=Q(a);-1===d.indexOf(" "+a+" ")&&(d+=a+" ")});a.setAttribute("class",Q(d))}}function ic(a,b){if(b)if(b.nodeType)a[a.length++]=b;else{var d=
b.length;if("number"===typeof d&&b.window!==b){if(d)for(var c=0;c<d;c++)a[a.length++]=b[c]}else a[a.length++]=b}}function id(a,b){return Fb(a,"$"+(b||"ngController")+"Controller")}function Fb(a,b,d){9===a.nodeType&&(a=a.documentElement);for(b=I(b)?b:[b];a;){for(var c=0,e=b.length;c<e;c++)if(t(d=B.data(a,b[c])))return d;a=a.parentNode||11===a.nodeType&&a.host}}function jd(a){for(zb(a,!0);a.firstChild;)a.removeChild(a.firstChild)}function Gb(a,b){b||zb(a);var d=a.parentNode;d&&d.removeChild(a)}function hg(a,
b){b=b||u;if("complete"===b.document.readyState)b.setTimeout(a);else B(b).on("load",a)}function gd(a){function b(){u.document.removeEventListener("DOMContentLoaded",b);u.removeEventListener("load",b);a()}"complete"===u.document.readyState?u.setTimeout(a):(u.document.addEventListener("DOMContentLoaded",b),u.addEventListener("load",b))}function kd(a,b){var d=Hb[b.toLowerCase()];return d&&ld[za(a)]&&d}function ig(a,b){var d=function(c,d){c.isDefaultPrevented=function(){return c.defaultPrevented};var f=
b[d||c.type],g=f?f.length:0;if(g){if(w(c.immediatePropagationStopped)){var k=c.stopImmediatePropagation;c.stopImmediatePropagation=function(){c.immediatePropagationStopped=!0;c.stopPropagation&&c.stopPropagation();k&&k.call(c)}}c.isImmediatePropagationStopped=function(){return!0===c.immediatePropagationStopped};var h=f.specialHandlerWrapper||jg;1<g&&(f=ja(f));for(var l=0;l<g;l++)c.isImmediatePropagationStopped()||h(a,c,f[l])}};d.elem=a;return d}function jg(a,b,d){d.call(a,b)}function kg(a,b,d){var c=
b.relatedTarget;c&&(c===a||lg.call(a,c))||d.call(a,b)}function $f(){this.$get=function(){return P(U,{hasClass:function(a,b){a.attr&&(a=a[0]);return Cb(a,b)},addClass:function(a,b){a.attr&&(a=a[0]);return Eb(a,b)},removeClass:function(a,b){a.attr&&(a=a[0]);return Db(a,b)}})}}function Pa(a,b){var d=a&&a.$$hashKey;if(d)return"function"===typeof d&&(d=a.$$hashKey()),d;d=typeof a;return d="function"===d||"object"===d&&null!==a?a.$$hashKey=d+":"+(b||pe)():d+":"+a}function md(){this._keys=[];this._values=
[];this._lastKey=NaN;this._lastIndex=-1}function nd(a){a=Function.prototype.toString.call(a).replace(mg,"");return a.match(ng)||a.match(og)}function pg(a){return(a=nd(a))?"function("+(a[1]||"").replace(/[\s\r\n]+/," ")+")":"fn"}function hb(a,b){function d(a){return function(b,c){if(E(b))p(b,Vb(a));else return a(b,c)}}function c(a,b){Ia(a,"service");if(A(b)||I(b))b=q.instantiate(b);if(!b.$get)throw Ba("pget",a);return n[a+"Provider"]=b}function e(a,b){return function(){var c=z.invoke(b,this);if(w(c))throw Ba("undef",
a);return c}}function f(a,b,d){return c(a,{$get:!1!==d?e(a,b):b})}function g(a){ib(w(a)||I(a),"modulesToLoad","not an array");var b=[],c;p(a,function(a){function d(a){var b,c;b=0;for(c=a.length;b<c;b++){var e=a[b],g=q.get(e[0]);g[e[1]].apply(g,e[2])}}if(!m.get(a)){m.set(a,!0);try{D(a)?(c=ec(a),z.modules[a]=c,b=b.concat(g(c.requires)).concat(c._runBlocks),d(c._invokeQueue),d(c._configBlocks)):A(a)?b.push(q.invoke(a)):I(a)?b.push(q.invoke(a)):ub(a,"module")}catch(e){throw I(a)&&(a=a[a.length-1]),e.message&&
e.stack&&-1===e.stack.indexOf(e.message)&&(e=e.message+"\n"+e.stack),Ba("modulerr",a,e.stack||e.message||e);}}});return b}function k(a,c){function d(b,e){if(a.hasOwnProperty(b)){if(a[b]===h)throw Ba("cdep",b+" <- "+l.join(" <- "));return a[b]}try{return l.unshift(b),a[b]=h,a[b]=c(b,e),a[b]}catch(g){throw a[b]===h&&delete a[b],g;}finally{l.shift()}}function e(a,c,g){var f=[];a=hb.$$annotate(a,b,g);for(var h=0,k=a.length;h<k;h++){var l=a[h];if("string"!==typeof l)throw Ba("itkn",l);f.push(c&&c.hasOwnProperty(l)?
c[l]:d(l,g))}return f}return{invoke:function(a,b,c,d){"string"===typeof c&&(d=c,c=null);c=e(a,c,d);I(a)&&(a=a[a.length-1]);d=a;if(Ca||"function"!==typeof d)d=!1;else{var g=d.$$ngIsClass;Na(g)||(g=d.$$ngIsClass=/^(?:class\b|constructor\()/.test(Function.prototype.toString.call(d)));d=g}return d?(c.unshift(null),new (Function.prototype.bind.apply(a,c))):a.apply(b,c)},instantiate:function(a,b,c){var d=I(a)?a[a.length-1]:a;a=e(a,b,c);a.unshift(null);return new (Function.prototype.bind.apply(d,a))},get:d,
annotate:hb.$$annotate,has:function(b){return n.hasOwnProperty(b+"Provider")||a.hasOwnProperty(b)}}}b=!0===b;var h={},l=[],m=new Ib,n={$provide:{provider:d(c),factory:d(f),service:d(function(a,b){return f(a,["$injector",function(a){return a.instantiate(b)}])}),value:d(function(a,b){return f(a,ka(b),!1)}),constant:d(function(a,b){Ia(a,"constant");n[a]=b;G[a]=b}),decorator:function(a,b){var c=q.get(a+"Provider"),d=c.$get;c.$get=function(){var a=z.invoke(d,c);return z.invoke(b,null,{$delegate:a})}}}},
q=n.$injector=k(n,function(a,b){$.isString(b)&&l.push(b);throw Ba("unpr",l.join(" <- "));}),G={},L=k(G,function(a,b){var c=q.get(a+"Provider",b);return z.invoke(c.$get,c,void 0,a)}),z=L;n.$injectorProvider={$get:ka(L)};z.modules=q.modules=S();var v=g(a),z=L.get("$injector");z.strictDi=b;p(v,function(a){a&&z.invoke(a)});return z}function pf(){var a=!0;this.disableAutoScrolling=function(){a=!1};this.$get=["$window","$location","$rootScope",function(b,d,c){function e(a){var b=null;Array.prototype.some.call(a,
function(a){if("a"===za(a))return b=a,!0});return b}function f(a){if(a){a.scrollIntoView();var c;c=g.yOffset;A(c)?c=c():Xb(c)?(c=c[0],c="fixed"!==b.getComputedStyle(c).position?0:c.getBoundingClientRect().bottom):Y(c)||(c=0);c&&(a=a.getBoundingClientRect().top,b.scrollBy(0,a-c))}else b.scrollTo(0,0)}function g(a){a=D(a)?a:Y(a)?a.toString():d.hash();var b;a?(b=k.getElementById(a))?f(b):(b=e(k.getElementsByName(a)))?f(b):"top"===a&&f(null):f(null)}var k=b.document;a&&c.$watch(function(){return d.hash()},
function(a,b){a===b&&""===a||hg(function(){c.$evalAsync(g)})});return g}]}function lb(a,b){if(!a&&!b)return"";if(!a)return b;if(!b)return a;I(a)&&(a=a.join(" "));I(b)&&(b=b.join(" "));return a+" "+b}function qg(a){D(a)&&(a=a.split(" "));var b=S();p(a,function(a){a.length&&(b[a]=!0)});return b}function Ka(a){return E(a)?a:{}}function rg(a,b,d,c){function e(a){try{a.apply(null,ya.call(arguments,1))}finally{if(L--,0===L)for(;z.length;)try{z.pop()()}catch(b){d.error(b)}}}function f(){y=null;k()}function g(){v=
J();v=w(v)?null:v;sa(v,K)&&(v=K);s=K=v}function k(){var a=s;g();if(Ta!==h.url()||a!==v)Ta=h.url(),s=v,p(H,function(a){a(h.url(),v)})}var h=this,l=a.location,m=a.history,n=a.setTimeout,q=a.clearTimeout,G={};h.isMock=!1;var L=0,z=[];h.$$completeOutstandingRequest=e;h.$$incOutstandingRequestCount=function(){L++};h.notifyWhenNoOutstandingRequests=function(a){0===L?a():z.push(a)};var v,s,Ta=l.href,ma=b.find("base"),y=null,J=c.history?function(){try{return m.state}catch(a){}}:C;g();h.url=function(b,d,e){w(e)&&
(e=null);l!==a.location&&(l=a.location);m!==a.history&&(m=a.history);if(b){var f=s===e;if(Ta===b&&(!c.history||f))return h;var k=Ta&&La(Ta)===La(b);Ta=b;s=e;!c.history||k&&f?(k||(y=b),d?l.replace(b):k?(d=l,e=b.indexOf("#"),e=-1===e?"":b.substr(e),d.hash=e):l.href=b,l.href!==b&&(y=b)):(m[d?"replaceState":"pushState"](e,"",b),g());y&&(y=b);return h}return y||l.href.replace(/%27/g,"'")};h.state=function(){return v};var H=[],ta=!1,K=null;h.onUrlChange=function(b){if(!ta){if(c.history)B(a).on("popstate",
f);B(a).on("hashchange",f);ta=!0}H.push(b);return b};h.$$applicationDestroyed=function(){B(a).off("hashchange popstate",f)};h.$$checkUrlChange=k;h.baseHref=function(){var a=ma.attr("href");return a?a.replace(/^(https?:)?\/\/[^/]*/,""):""};h.defer=function(a,b){var c;L++;c=n(function(){delete G[c];e(a)},b||0);G[c]=!0;return c};h.defer.cancel=function(a){return G[a]?(delete G[a],q(a),e(C),!0):!1}}function wf(){this.$get=["$window","$log","$sniffer","$document",function(a,b,d,c){return new rg(a,c,b,
d)}]}function xf(){this.$get=function(){function a(a,c){function e(a){a!==n&&(q?q===a&&(q=a.n):q=a,f(a.n,a.p),f(a,n),n=a,n.n=null)}function f(a,b){a!==b&&(a&&(a.p=b),b&&(b.n=a))}if(a in b)throw M("$cacheFactory")("iid",a);var g=0,k=P({},c,{id:a}),h=S(),l=c&&c.capacity||Number.MAX_VALUE,m=S(),n=null,q=null;return b[a]={put:function(a,b){if(!w(b)){if(l<Number.MAX_VALUE){var c=m[a]||(m[a]={key:a});e(c)}a in h||g++;h[a]=b;g>l&&this.remove(q.key);return b}},get:function(a){if(l<Number.MAX_VALUE){var b=
m[a];if(!b)return;e(b)}return h[a]},remove:function(a){if(l<Number.MAX_VALUE){var b=m[a];if(!b)return;b===n&&(n=b.p);b===q&&(q=b.n);f(b.n,b.p);delete m[a]}a in h&&(delete h[a],g--)},removeAll:function(){h=S();g=0;m=S();n=q=null},destroy:function(){m=k=h=null;delete b[a]},info:function(){return P({},k,{size:g})}}}var b={};a.info=function(){var a={};p(b,function(b,e){a[e]=b.info()});return a};a.get=function(a){return b[a]};return a}}function Uf(){this.$get=["$cacheFactory",function(a){return a("templates")}]}
function Yc(a,b){function d(a,b,c){var d=/^\s*([@&<]|=(\*?))(\??)\s*([\w$]*)\s*$/,e=S();p(a,function(a,g){if(a in n)e[g]=n[a];else{var f=a.match(d);if(!f)throw ba("iscp",b,g,a,c?"controller bindings definition":"isolate scope definition");e[g]={mode:f[1][0],collection:"*"===f[2],optional:"?"===f[3],attrName:f[4]||g};f[4]&&(n[a]=e[g])}});return e}function c(a){var b=a.charAt(0);if(!b||b!==N(b))throw ba("baddir",a);if(a!==a.trim())throw ba("baddir",a);}function e(a){var b=a.require||a.controller&&a.name;
!I(b)&&E(b)&&p(b,function(a,c){var d=a.match(l);a.substring(d[0].length)||(b[c]=d[0]+c)});return b}var f={},g=/^\s*directive:\s*([\w-]+)\s+(.*)$/,k=/(([\w-]+)(?::([^;]+))?;?)/,h=te("ngSrc,ngSrcset,src,srcset"),l=/^(?:(\^\^?)?(\?)?(\^\^?)?)?/,m=/^(on[a-z]+|formaction)$/,n=S();this.directive=function ma(b,d){ib(b,"name");Ia(b,"directive");D(b)?(c(b),ib(d,"directiveFactory"),f.hasOwnProperty(b)||(f[b]=[],a.factory(b+"Directive",["$injector","$exceptionHandler",function(a,c){var d=[];p(f[b],function(g,
f){try{var h=a.invoke(g);A(h)?h={compile:ka(h)}:!h.compile&&h.link&&(h.compile=ka(h.link));h.priority=h.priority||0;h.index=f;h.name=h.name||b;h.require=e(h);var k=h,l=h.restrict;if(l&&(!D(l)||!/[EACM]/.test(l)))throw ba("badrestrict",l,b);k.restrict=l||"EA";h.$$moduleName=g.$$moduleName;d.push(h)}catch(m){c(m)}});return d}])),f[b].push(d)):p(b,Vb(ma));return this};this.component=function y(a,b){function c(a){function e(b){return A(b)||I(b)?function(c,d){return a.invoke(b,this,{$element:c,$attrs:d})}:
b}var g=b.template||b.templateUrl?b.template:"",f={controller:d,controllerAs:sg(b.controller)||b.controllerAs||"$ctrl",template:e(g),templateUrl:e(b.templateUrl),transclude:b.transclude,scope:{},bindToController:b.bindings||{},restrict:"E",require:b.require};p(b,function(a,b){"$"===b.charAt(0)&&(f[b]=a)});return f}if(!D(a))return p(a,Vb(Ra(this,y))),this;var d=b.controller||function(){};p(b,function(a,b){"$"===b.charAt(0)&&(c[b]=a,A(d)&&(d[b]=a))});c.$inject=["$injector"];return this.directive(a,
c)};this.aHrefSanitizationWhitelist=function(a){return t(a)?(b.aHrefSanitizationWhitelist(a),this):b.aHrefSanitizationWhitelist()};this.imgSrcSanitizationWhitelist=function(a){return t(a)?(b.imgSrcSanitizationWhitelist(a),this):b.imgSrcSanitizationWhitelist()};var q=!0;this.debugInfoEnabled=function(a){return t(a)?(q=a,this):q};var G=!1;this.preAssignBindingsEnabled=function(a){return t(a)?(G=a,this):G};var L=!1;this.strictComponentBindingsEnabled=function(a){return t(a)?(L=a,this):L};var z=10;this.onChangesTtl=
function(a){return arguments.length?(z=a,this):z};var v=!0;this.commentDirectivesEnabled=function(a){return arguments.length?(v=a,this):v};var s=!0;this.cssClassDirectivesEnabled=function(a){return arguments.length?(s=a,this):s};this.$get=["$injector","$interpolate","$exceptionHandler","$templateRequest","$parse","$controller","$rootScope","$sce","$animate","$$sanitizeUri",function(a,b,c,e,n,F,R,x,W,r){function O(){try{if(!--Fa)throw ga=void 0,ba("infchng",z);R.$apply(function(){for(var a=[],b=0,
c=ga.length;b<c;++b)try{ga[b]()}catch(d){a.push(d)}ga=void 0;if(a.length)throw a;})}finally{Fa++}}function mc(a,b){if(b){var c=Object.keys(b),d,e,g;d=0;for(e=c.length;d<e;d++)g=c[d],this[g]=b[g]}else this.$attr={};this.$$element=a}function Ua(a,b,c){Ba.innerHTML="<span "+b+">";b=Ba.firstChild.attributes;var d=b[0];b.removeNamedItem(d.name);d.value=c;a.attributes.setNamedItem(d)}function na(a,b){try{a.addClass(b)}catch(c){}}function ca(a,b,c,d,e){a instanceof B||(a=B(a));var g=Va(a,b,a,c,d,e);ca.$$addScopeClass(a);
var f=null;return function(b,c,d){if(!a)throw ba("multilink");ib(b,"scope");e&&e.needsNewScope&&(b=b.$parent.$new());d=d||{};var h=d.parentBoundTranscludeFn,k=d.transcludeControllers;d=d.futureParentElement;h&&h.$$boundTransclude&&(h=h.$$boundTransclude);f||(f=(d=d&&d[0])?"foreignobject"!==za(d)&&ha.call(d).match(/SVG/)?"svg":"html":"html");d="html"!==f?B(ja(f,B("<div>").append(a).html())):c?Sa.clone.call(a):a;if(k)for(var l in k)d.data("$"+l+"Controller",k[l].instance);ca.$$addScopeInfo(d,b);c&&
c(d,b);g&&g(b,d,d,h);c||(a=g=null);return d}}function Va(a,b,c,d,e,g){function f(a,c,d,e){var g,k,l,m,q,n,H;if(s)for(H=Array(c.length),m=0;m<h.length;m+=3)g=h[m],H[g]=c[g];else H=c;m=0;for(q=h.length;m<q;)k=H[h[m++]],c=h[m++],g=h[m++],c?(c.scope?(l=a.$new(),ca.$$addScopeInfo(B(k),l)):l=a,n=c.transcludeOnThisElement?Ma(a,c.transclude,e):!c.templateOnThisElement&&e?e:!e&&b?Ma(a,b):null,c(g,l,k,d,n)):g&&g(a,k.childNodes,void 0,e)}for(var h=[],k=I(a)||a instanceof B,l,m,q,n,s,H=0;H<a.length;H++){l=new mc;
11===Ca&&Da(a,H,k);m=M(a[H],[],l,0===H?d:void 0,e);(g=m.length?Y(m,a[H],l,b,c,null,[],[],g):null)&&g.scope&&ca.$$addScopeClass(l.$$element);l=g&&g.terminal||!(q=a[H].childNodes)||!q.length?null:Va(q,g?(g.transcludeOnThisElement||!g.templateOnThisElement)&&g.transclude:b);if(g||l)h.push(H,g,l),n=!0,s=s||g;g=null}return n?f:null}function Da(a,b,c){var d=a[b],e=d.parentNode,g;if(d.nodeType===Oa)for(;;){g=e?d.nextSibling:a[b+1];if(!g||g.nodeType!==Oa)break;d.nodeValue+=g.nodeValue;g.parentNode&&g.parentNode.removeChild(g);
c&&g===a[b+1]&&a.splice(b+1,1)}}function Ma(a,b,c){function d(e,g,f,h,k){e||(e=a.$new(!1,k),e.$$transcluded=!0);return b(e,g,{parentBoundTranscludeFn:c,transcludeControllers:f,futureParentElement:h})}var e=d.$$slots=S(),g;for(g in b.$$slots)e[g]=b.$$slots[g]?Ma(a,b.$$slots[g],c):null;return d}function M(a,b,c,d,e){var g=c.$attr,f;switch(a.nodeType){case 1:f=za(a);T(b,Ea(f),"E",d,e);for(var h,l,m,q,n=a.attributes,s=0,H=n&&n.length;s<H;s++){var J=!1,G=!1;h=n[s];l=h.name;m=h.value;h=Ea(l);(q=Pa.test(h))&&
(l=l.replace(od,"").substr(8).replace(/_(.)/g,function(a,b){return b.toUpperCase()}));(h=h.match(Qa))&&$(h[1])&&(J=l,G=l.substr(0,l.length-5)+"end",l=l.substr(0,l.length-6));h=Ea(l.toLowerCase());g[h]=l;if(q||!c.hasOwnProperty(h))c[h]=m,kd(a,h)&&(c[h]=!0);xa(a,b,m,h,q);T(b,h,"A",d,e,J,G)}"input"===f&&"hidden"===a.getAttribute("type")&&a.setAttribute("autocomplete","off");if(!La)break;g=a.className;E(g)&&(g=g.animVal);if(D(g)&&""!==g)for(;a=k.exec(g);)h=Ea(a[2]),T(b,h,"C",d,e)&&(c[h]=Q(a[3])),g=g.substr(a.index+
a[0].length);break;case Oa:oa(b,a.nodeValue);break;case 8:if(!Ka)break;nc(a,b,c,d,e)}b.sort(ka);return b}function nc(a,b,c,d,e){try{var f=g.exec(a.nodeValue);if(f){var h=Ea(f[1]);T(b,h,"M",d,e)&&(c[h]=Q(f[2]))}}catch(k){}}function pd(a,b,c){var d=[],e=0;if(b&&a.hasAttribute&&a.hasAttribute(b)){do{if(!a)throw ba("uterdir",b,c);1===a.nodeType&&(a.hasAttribute(b)&&e++,a.hasAttribute(c)&&e--);d.push(a);a=a.nextSibling}while(0<e)}else d.push(a);return B(d)}function U(a,b,c){return function(d,e,g,f,h){e=
pd(e[0],b,c);return a(d,e,g,f,h)}}function V(a,b,c,d,e,g){var f;return a?ca(b,c,d,e,g):function(){f||(f=ca(b,c,d,e,g),b=c=g=null);return f.apply(this,arguments)}}function Y(a,b,d,e,g,f,h,k,l){function m(a,b,c,d){if(a){c&&(a=U(a,c,d));a.require=x.require;a.directiveName=W;if(K===x||x.$$isolateScope)a=ua(a,{isolateScope:!0});h.push(a)}if(b){c&&(b=U(b,c,d));b.require=x.require;b.directiveName=W;if(K===x||x.$$isolateScope)b=ua(b,{isolateScope:!0});k.push(b)}}function q(a,e,g,f,l){function m(a,b,c,d){var e;
cb(a)||(d=c,c=b,b=a,a=void 0);ta&&(e=L);c||(c=ta?fa.parent():fa);if(d){var g=l.$$slots[d];if(g)return g(a,b,e,c,O);if(w(g))throw ba("noslot",d,Aa(fa));}else return l(a,b,e,c,O)}var n,x,F,y,R,L,z,fa;b===g?(f=d,fa=d.$$element):(fa=B(g),f=new mc(fa,d));R=e;K?y=e.$new(!0):s&&(R=e.$parent);l&&(z=m,z.$$boundTransclude=l,z.isSlotFilled=function(a){return!!l.$$slots[a]});J&&(L=da(fa,f,z,J,y,e,K));K&&(ca.$$addScopeInfo(fa,y,!0,!(v&&(v===K||v===K.$$originalDirective))),ca.$$addScopeClass(fa,!0),y.$$isolateBindings=
K.$$isolateBindings,x=qa(e,f,y,y.$$isolateBindings,K),x.removeWatches&&y.$on("$destroy",x.removeWatches));for(n in L){x=J[n];F=L[n];var W=x.$$bindings.bindToController;if(G){F.bindingInfo=W?qa(R,f,F.instance,W,x):{};var r=F();r!==F.instance&&(F.instance=r,fa.data("$"+x.name+"Controller",r),F.bindingInfo.removeWatches&&F.bindingInfo.removeWatches(),F.bindingInfo=qa(R,f,F.instance,W,x))}else F.instance=F(),fa.data("$"+x.name+"Controller",F.instance),F.bindingInfo=qa(R,f,F.instance,W,x)}p(J,function(a,
b){var c=a.require;a.bindToController&&!I(c)&&E(c)&&P(L[b].instance,X(b,c,fa,L))});p(L,function(a){var b=a.instance;if(A(b.$onChanges))try{b.$onChanges(a.bindingInfo.initialChanges)}catch(d){c(d)}if(A(b.$onInit))try{b.$onInit()}catch(e){c(e)}A(b.$doCheck)&&(R.$watch(function(){b.$doCheck()}),b.$doCheck());A(b.$onDestroy)&&R.$on("$destroy",function(){b.$onDestroy()})});n=0;for(x=h.length;n<x;n++)F=h[n],wa(F,F.isolateScope?y:e,fa,f,F.require&&X(F.directiveName,F.require,fa,L),z);var O=e;K&&(K.template||
null===K.templateUrl)&&(O=y);a&&a(O,g.childNodes,void 0,l);for(n=k.length-1;0<=n;n--)F=k[n],wa(F,F.isolateScope?y:e,fa,f,F.require&&X(F.directiveName,F.require,fa,L),z);p(L,function(a){a=a.instance;A(a.$postLink)&&a.$postLink()})}l=l||{};for(var n=-Number.MAX_VALUE,s=l.newScopeDirective,J=l.controllerDirectives,K=l.newIsolateScopeDirective,v=l.templateDirective,y=l.nonTlbTranscludeDirective,R=!1,L=!1,ta=l.hasElementTranscludeDirective,F=d.$$element=B(b),x,W,z,r=e,O,t=!1,Jb=!1,u,Da=0,C=a.length;Da<
C;Da++){x=a[Da];var Ua=x.$$start,D=x.$$end;Ua&&(F=pd(b,Ua,D));z=void 0;if(n>x.priority)break;if(u=x.scope)x.templateUrl||(E(u)?(aa("new/isolated scope",K||s,x,F),K=x):aa("new/isolated scope",K,x,F)),s=s||x;W=x.name;if(!t&&(x.replace&&(x.templateUrl||x.template)||x.transclude&&!x.$$tlb)){for(u=Da+1;t=a[u++];)if(t.transclude&&!t.$$tlb||t.replace&&(t.templateUrl||t.template)){Jb=!0;break}t=!0}!x.templateUrl&&x.controller&&(J=J||S(),aa("'"+W+"' controller",J[W],x,F),J[W]=x);if(u=x.transclude)if(R=!0,
x.$$tlb||(aa("transclusion",y,x,F),y=x),"element"===u)ta=!0,n=x.priority,z=F,F=d.$$element=B(ca.$$createComment(W,d[W])),b=F[0],la(g,ya.call(z,0),b),z[0].$$parentNode=z[0].parentNode,r=V(Jb,z,e,n,f&&f.name,{nonTlbTranscludeDirective:y});else{var na=S();if(E(u)){z=[];var Va=S(),Ma=S();p(u,function(a,b){var c="?"===a.charAt(0);a=c?a.substring(1):a;Va[a]=b;na[b]=null;Ma[b]=c});p(F.contents(),function(a){var b=Va[Ea(za(a))];b?(Ma[b]=!0,na[b]=na[b]||[],na[b].push(a)):z.push(a)});p(Ma,function(a,b){if(!a)throw ba("reqslot",
b);});for(var N in na)na[N]&&(na[N]=V(Jb,na[N],e))}else z=B(jc(b)).contents();F.empty();r=V(Jb,z,e,void 0,void 0,{needsNewScope:x.$$isolateScope||x.$$newScope});r.$$slots=na}if(x.template)if(L=!0,aa("template",v,x,F),v=x,u=A(x.template)?x.template(F,d):x.template,u=Ia(u),x.replace){f=x;z=gc.test(u)?qd(ja(x.templateNamespace,Q(u))):[];b=z[0];if(1!==z.length||1!==b.nodeType)throw ba("tplrt",W,"");la(g,F,b);C={$attr:{}};u=M(b,[],C);var nc=a.splice(Da+1,a.length-(Da+1));(K||s)&&Z(u,K,s);a=a.concat(u).concat(nc);
ea(d,C);C=a.length}else F.html(u);if(x.templateUrl)L=!0,aa("template",v,x,F),v=x,x.replace&&(f=x),q=ia(a.splice(Da,a.length-Da),F,d,g,R&&r,h,k,{controllerDirectives:J,newScopeDirective:s!==x&&s,newIsolateScopeDirective:K,templateDirective:v,nonTlbTranscludeDirective:y}),C=a.length;else if(x.compile)try{O=x.compile(F,d,r);var T=x.$$originalDirective||x;A(O)?m(null,Ra(T,O),Ua,D):O&&m(Ra(T,O.pre),Ra(T,O.post),Ua,D)}catch($){c($,Aa(F))}x.terminal&&(q.terminal=!0,n=Math.max(n,x.priority))}q.scope=s&&!0===
s.scope;q.transcludeOnThisElement=R;q.templateOnThisElement=L;q.transclude=r;l.hasElementTranscludeDirective=ta;return q}function X(a,b,c,d){var e;if(D(b)){var g=b.match(l);b=b.substring(g[0].length);var f=g[1]||g[3],g="?"===g[2];"^^"===f?c=c.parent():e=(e=d&&d[b])&&e.instance;if(!e){var h="$"+b+"Controller";e=f?c.inheritedData(h):c.data(h)}if(!e&&!g)throw ba("ctreq",b,a);}else if(I(b))for(e=[],f=0,g=b.length;f<g;f++)e[f]=X(a,b[f],c,d);else E(b)&&(e={},p(b,function(b,g){e[g]=X(a,b,c,d)}));return e||
null}function da(a,b,c,d,e,g,f){var h=S(),k;for(k in d){var l=d[k],m={$scope:l===f||l.$$isolateScope?e:g,$element:a,$attrs:b,$transclude:c},n=l.controller;"@"===n&&(n=b[l.name]);m=F(n,m,!0,l.controllerAs);h[l.name]=m;a.data("$"+l.name+"Controller",m.instance)}return h}function Z(a,b,c){for(var d=0,e=a.length;d<e;d++)a[d]=Yb(a[d],{$$isolateScope:b,$$newScope:c})}function T(b,c,e,g,h,k,l){if(c===h)return null;var m=null;if(f.hasOwnProperty(c)){h=a.get(c+"Directive");for(var n=0,q=h.length;n<q;n++)if(c=
h[n],(w(g)||g>c.priority)&&-1!==c.restrict.indexOf(e)){k&&(c=Yb(c,{$$start:k,$$end:l}));if(!c.$$bindings){var s=m=c,H=c.name,J={isolateScope:null,bindToController:null};E(s.scope)&&(!0===s.bindToController?(J.bindToController=d(s.scope,H,!0),J.isolateScope={}):J.isolateScope=d(s.scope,H,!1));E(s.bindToController)&&(J.bindToController=d(s.bindToController,H,!0));if(J.bindToController&&!s.controller)throw ba("noctrl",H);m=m.$$bindings=J;E(m.isolateScope)&&(c.$$isolateBindings=m.isolateScope)}b.push(c);
m=c}}return m}function $(b){if(f.hasOwnProperty(b))for(var c=a.get(b+"Directive"),d=0,e=c.length;d<e;d++)if(b=c[d],b.multiElement)return!0;return!1}function ea(a,b){var c=b.$attr,d=a.$attr;p(a,function(d,e){"$"!==e.charAt(0)&&(b[e]&&b[e]!==d&&(d=d.length?d+(("style"===e?";":" ")+b[e]):b[e]),a.$set(e,d,!0,c[e]))});p(b,function(b,e){a.hasOwnProperty(e)||"$"===e.charAt(0)||(a[e]=b,"class"!==e&&"style"!==e&&(d[e]=c[e]))})}function ia(a,b,d,g,f,h,k,l){var m=[],n,q,s=b[0],J=a.shift(),x=Yb(J,{templateUrl:null,
transclude:null,replace:null,$$originalDirective:J}),G=A(J.templateUrl)?J.templateUrl(b,d):J.templateUrl,F=J.templateNamespace;b.empty();e(G).then(function(c){var e,H;c=Ia(c);if(J.replace){c=gc.test(c)?qd(ja(F,Q(c))):[];e=c[0];if(1!==c.length||1!==e.nodeType)throw ba("tplrt",J.name,G);c={$attr:{}};la(g,b,e);var K=M(e,[],c);E(J.scope)&&Z(K,!0);a=K.concat(a);ea(d,c)}else e=s,b.html(c);a.unshift(x);n=Y(a,e,d,f,b,J,h,k,l);p(g,function(a,c){a===e&&(g[c]=b[0])});for(q=Va(b[0].childNodes,f);m.length;){c=
m.shift();H=m.shift();var v=m.shift(),y=m.shift(),K=b[0];if(!c.$$destroyed){if(H!==s){var L=H.className;l.hasElementTranscludeDirective&&J.replace||(K=jc(e));la(v,B(H),K);na(B(K),L)}H=n.transcludeOnThisElement?Ma(c,n.transclude,y):y;n(q,c,K,g,H)}}m=null}).catch(function(a){$b(a)&&c(a)});return function(a,b,c,d,e){a=e;b.$$destroyed||(m?m.push(b,c,d,a):(n.transcludeOnThisElement&&(a=Ma(b,n.transclude,e)),n(q,b,c,d,a)))}}function ka(a,b){var c=b.priority-a.priority;return 0!==c?c:a.name!==b.name?a.name<
b.name?-1:1:a.index-b.index}function aa(a,b,c,d){function e(a){return a?" (module: "+a+")":""}if(b)throw ba("multidir",b.name,e(b.$$moduleName),c.name,e(c.$$moduleName),a,Aa(d));}function oa(a,c){var d=b(c,!0);d&&a.push({priority:0,compile:function(a){a=a.parent();var b=!!a.length;b&&ca.$$addBindingClass(a);return function(a,c){var e=c.parent();b||ca.$$addBindingClass(e);ca.$$addBindingInfo(e,d.expressions);a.$watch(d,function(a){c[0].nodeValue=a})}}})}function ja(a,b){a=N(a||"html");switch(a){case "svg":case "math":var c=
u.document.createElement("div");c.innerHTML="<"+a+">"+b+"</"+a+">";return c.childNodes[0].childNodes;default:return b}}function va(a,b){if("srcdoc"===b)return x.HTML;var c=za(a);if("src"===b||"ngSrc"===b){if(-1===["img","video","audio","source","track"].indexOf(c))return x.RESOURCE_URL}else if("xlinkHref"===b||"form"===c&&"action"===b||"link"===c&&"href"===b)return x.RESOURCE_URL}function xa(a,c,d,e,g){var f=va(a,e),k=h[e]||g,l=b(d,!g,f,k);if(l){if("multiple"===e&&"select"===za(a))throw ba("selmulti",
Aa(a));if(m.test(e))throw ba("nodomevents");c.push({priority:100,compile:function(){return{pre:function(a,c,g){c=g.$$observers||(g.$$observers=S());var h=g[e];h!==d&&(l=h&&b(h,!0,f,k),d=h);l&&(g[e]=l(a),(c[e]||(c[e]=[])).$$inter=!0,(g.$$observers&&g.$$observers[e].$$scope||a).$watch(l,function(a,b){"class"===e&&a!==b?g.$updateClass(a,b):g.$set(e,a)}))}}}})}}function la(a,b,c){var d=b[0],e=b.length,g=d.parentNode,f,h;if(a)for(f=0,h=a.length;f<h;f++)if(a[f]===d){a[f++]=c;h=f+e-1;for(var k=a.length;f<
k;f++,h++)h<k?a[f]=a[h]:delete a[f];a.length-=e-1;a.context===d&&(a.context=c);break}g&&g.replaceChild(c,d);a=u.document.createDocumentFragment();for(f=0;f<e;f++)a.appendChild(b[f]);B.hasData(d)&&(B.data(c,B.data(d)),B(d).off("$destroy"));B.cleanData(a.querySelectorAll("*"));for(f=1;f<e;f++)delete b[f];b[0]=c;b.length=1}function ua(a,b){return P(function(){return a.apply(null,arguments)},a,b)}function wa(a,b,d,e,g,f){try{a(b,d,e,g,f)}catch(h){c(h,Aa(d))}}function pa(a,b){if(L)throw ba("missingattr",
a,b);}function qa(a,c,d,e,g){function f(b,c,e){A(d.$onChanges)&&!ac(c,e)&&(ga||(a.$$postDigest(O),ga=[]),m||(m={},ga.push(h)),m[b]&&(e=m[b].previousValue),m[b]=new Kb(e,c))}function h(){d.$onChanges(m);m=void 0}var k=[],l={},m;p(e,function(e,h){var m=e.attrName,q=e.optional,s,H,x,G;switch(e.mode){case "@":q||ra.call(c,m)||(pa(m,g.name),d[h]=c[m]=void 0);q=c.$observe(m,function(a){if(D(a)||Na(a))f(h,a,d[h]),d[h]=a});c.$$observers[m].$$scope=a;s=c[m];D(s)?d[h]=b(s)(a):Na(s)&&(d[h]=s);l[h]=new Kb(oc,
d[h]);k.push(q);break;case "=":if(!ra.call(c,m)){if(q)break;pa(m,g.name);c[m]=void 0}if(q&&!c[m])break;H=n(c[m]);G=H.literal?sa:ac;x=H.assign||function(){s=d[h]=H(a);throw ba("nonassign",c[m],m,g.name);};s=d[h]=H(a);q=function(b){G(b,d[h])||(G(b,s)?x(a,b=d[h]):d[h]=b);return s=b};q.$stateful=!0;q=e.collection?a.$watchCollection(c[m],q):a.$watch(n(c[m],q),null,H.literal);k.push(q);break;case "<":if(!ra.call(c,m)){if(q)break;pa(m,g.name);c[m]=void 0}if(q&&!c[m])break;H=n(c[m]);var F=H.literal,v=d[h]=
H(a);l[h]=new Kb(oc,d[h]);q=a.$watch(H,function(a,b){if(b===a){if(b===v||F&&sa(b,v))return;b=v}f(h,a,b);d[h]=a},F);k.push(q);break;case "&":q||ra.call(c,m)||pa(m,g.name);H=c.hasOwnProperty(m)?n(c[m]):C;if(H===C&&q)break;d[h]=function(b){return H(a,b)}}});return{initialChanges:l,removeWatches:k.length&&function(){for(var a=0,b=k.length;a<b;++a)k[a]()}}}var Ja=/^\w/,Ba=u.document.createElement("div"),Ka=v,La=s,Fa=z,ga;mc.prototype={$normalize:Ea,$addClass:function(a){a&&0<a.length&&W.addClass(this.$$element,
a)},$removeClass:function(a){a&&0<a.length&&W.removeClass(this.$$element,a)},$updateClass:function(a,b){var c=rd(a,b);c&&c.length&&W.addClass(this.$$element,c);(c=rd(b,a))&&c.length&&W.removeClass(this.$$element,c)},$set:function(a,b,d,e){var g=kd(this.$$element[0],a),f=sd[a],h=a;g?(this.$$element.prop(a,b),e=g):f&&(this[f]=b,h=f);this[a]=b;e?this.$attr[a]=e:(e=this.$attr[a])||(this.$attr[a]=e=Vc(a,"-"));g=za(this.$$element);if("a"===g&&("href"===a||"xlinkHref"===a)||"img"===g&&"src"===a)this[a]=
b=r(b,"src"===a);else if("img"===g&&"srcset"===a&&t(b)){for(var g="",f=Q(b),k=/(\s+\d+x\s*,|\s+\d+w\s*,|\s+,|,\s+)/,k=/\s/.test(f)?k:/(,)/,f=f.split(k),k=Math.floor(f.length/2),l=0;l<k;l++)var m=2*l,g=g+r(Q(f[m]),!0),g=g+(" "+Q(f[m+1]));f=Q(f[2*l]).split(/\s/);g+=r(Q(f[0]),!0);2===f.length&&(g+=" "+Q(f[1]));this[a]=b=g}!1!==d&&(null===b||w(b)?this.$$element.removeAttr(e):Ja.test(e)?this.$$element.attr(e,b):Ua(this.$$element[0],e,b));(a=this.$$observers)&&p(a[h],function(a){try{a(b)}catch(d){c(d)}})},
$observe:function(a,b){var c=this,d=c.$$observers||(c.$$observers=S()),e=d[a]||(d[a]=[]);e.push(b);R.$evalAsync(function(){e.$$inter||!c.hasOwnProperty(a)||w(c[a])||b(c[a])});return function(){db(e,b)}}};var Ga=b.startSymbol(),Ha=b.endSymbol(),Ia="{{"===Ga&&"}}"===Ha?bb:function(a){return a.replace(/\{\{/g,Ga).replace(/}}/g,Ha)},Pa=/^ngAttr[A-Z]/,Qa=/^(.+)Start$/;ca.$$addBindingInfo=q?function(a,b){var c=a.data("$binding")||[];I(b)?c=c.concat(b):c.push(b);a.data("$binding",c)}:C;ca.$$addBindingClass=
q?function(a){na(a,"ng-binding")}:C;ca.$$addScopeInfo=q?function(a,b,c,d){a.data(c?d?"$isolateScopeNoTemplate":"$isolateScope":"$scope",b)}:C;ca.$$addScopeClass=q?function(a,b){na(a,b?"ng-isolate-scope":"ng-scope")}:C;ca.$$createComment=function(a,b){var c="";q&&(c=" "+(a||"")+": ",b&&(c+=b+" "));return u.document.createComment(c)};return ca}]}function Kb(a,b){this.previousValue=a;this.currentValue=b}function Ea(a){return a.replace(od,"").replace(tg,jb)}function rd(a,b){var d="",c=a.split(/\s+/),
e=b.split(/\s+/),f=0;a:for(;f<c.length;f++){for(var g=c[f],k=0;k<e.length;k++)if(g===e[k])continue a;d+=(0<d.length?" ":"")+g}return d}function qd(a){a=B(a);var b=a.length;if(1>=b)return a;for(;b--;){var d=a[b];(8===d.nodeType||d.nodeType===Oa&&""===d.nodeValue.trim())&&ug.call(a,b,1)}return a}function sg(a,b){if(b&&D(b))return b;if(D(a)){var d=td.exec(a);if(d)return d[3]}}function yf(){var a={},b=!1;this.has=function(b){return a.hasOwnProperty(b)};this.register=function(b,c){Ia(b,"controller");E(b)?
P(a,b):a[b]=c};this.allowGlobals=function(){b=!0};this.$get=["$injector","$window",function(d,c){function e(a,b,c,d){if(!a||!E(a.$scope))throw M("$controller")("noscp",d,b);a.$scope[b]=c}return function(f,g,k,h){var l,m,n;k=!0===k;h&&D(h)&&(n=h);if(D(f)){h=f.match(td);if(!h)throw ud("ctrlfmt",f);m=h[1];n=n||h[3];f=a.hasOwnProperty(m)?a[m]:Xc(g.$scope,m,!0)||(b?Xc(c,m,!0):void 0);if(!f)throw ud("ctrlreg",m);ub(f,m,!0)}if(k)return k=(I(f)?f[f.length-1]:f).prototype,l=Object.create(k||null),n&&e(g,n,
l,m||f.name),P(function(){var a=d.invoke(f,l,g,m);a!==l&&(E(a)||A(a))&&(l=a,n&&e(g,n,l,m||f.name));return l},{instance:l,identifier:n});l=d.instantiate(f,g,m);n&&e(g,n,l,m||f.name);return l}}]}function zf(){this.$get=["$window",function(a){return B(a.document)}]}function Af(){this.$get=["$document","$rootScope",function(a,b){function d(){e=c.hidden}var c=a[0],e=c&&c.hidden;a.on("visibilitychange",d);b.$on("$destroy",function(){a.off("visibilitychange",d)});return function(){return e}}]}function Bf(){this.$get=
["$log",function(a){return function(b,d){a.error.apply(a,arguments)}}]}function pc(a){return E(a)?ea(a)?a.toISOString():fb(a):a}function Gf(){this.$get=function(){return function(a){if(!a)return"";var b=[];Nc(a,function(a,c){null===a||w(a)||A(a)||(I(a)?p(a,function(a){b.push(ia(c)+"="+ia(pc(a)))}):b.push(ia(c)+"="+ia(pc(a))))});return b.join("&")}}}function Hf(){this.$get=function(){return function(a){function b(a,e,f){null===a||w(a)||(I(a)?p(a,function(a,c){b(a,e+"["+(E(a)?c:"")+"]")}):E(a)&&!ea(a)?
Nc(a,function(a,c){b(a,e+(f?"":"[")+c+(f?"":"]"))}):d.push(ia(e)+"="+ia(pc(a))))}if(!a)return"";var d=[];b(a,"",!0);return d.join("&")}}}function qc(a,b){if(D(a)){var d=a.replace(vg,"").trim();if(d){var c=b("Content-Type"),c=c&&0===c.indexOf(vd),e;(e=c)||(e=(e=d.match(wg))&&xg[e[0]].test(d));if(e)try{a=Qc(d)}catch(f){if(!c)return a;throw rc("baddata",a,f);}}}return a}function wd(a){var b=S(),d;D(a)?p(a.split("\n"),function(a){d=a.indexOf(":");var e=N(Q(a.substr(0,d)));a=Q(a.substr(d+1));e&&(b[e]=
b[e]?b[e]+", "+a:a)}):E(a)&&p(a,function(a,d){var f=N(d),g=Q(a);f&&(b[f]=b[f]?b[f]+", "+g:g)});return b}function xd(a){var b;return function(d){b||(b=wd(a));return d?(d=b[N(d)],void 0===d&&(d=null),d):b}}function yd(a,b,d,c){if(A(c))return c(a,b,d);p(c,function(c){a=c(a,b,d)});return a}function Ff(){var a=this.defaults={transformResponse:[qc],transformRequest:[function(a){return E(a)&&"[object File]"!==ha.call(a)&&"[object Blob]"!==ha.call(a)&&"[object FormData]"!==ha.call(a)?fb(a):a}],headers:{common:{Accept:"application/json, text/plain, */*"},
post:ja(sc),put:ja(sc),patch:ja(sc)},xsrfCookieName:"XSRF-TOKEN",xsrfHeaderName:"X-XSRF-TOKEN",paramSerializer:"$httpParamSerializer",jsonpCallbackParam:"callback"},b=!1;this.useApplyAsync=function(a){return t(a)?(b=!!a,this):b};var d=this.interceptors=[];this.$get=["$browser","$httpBackend","$$cookieReader","$cacheFactory","$rootScope","$q","$injector","$sce",function(c,e,f,g,k,h,l,m){function n(b){function d(a,b){for(var c=0,e=b.length;c<e;){var g=b[c++],f=b[c++];a=a.then(g,f)}b.length=0;return a}
function e(a,b){var c,d={};p(a,function(a,e){A(a)?(c=a(b),null!=c&&(d[e]=c)):d[e]=a});return d}function g(a){var b=P({},a);b.data=yd(a.data,a.headers,a.status,f.transformResponse);a=a.status;return 200<=a&&300>a?b:h.reject(b)}if(!E(b))throw M("$http")("badreq",b);if(!D(m.valueOf(b.url)))throw M("$http")("badreq",b.url);var f=P({method:"get",transformRequest:a.transformRequest,transformResponse:a.transformResponse,paramSerializer:a.paramSerializer,jsonpCallbackParam:a.jsonpCallbackParam},b);f.headers=
function(b){var c=a.headers,d=P({},b.headers),g,f,h,c=P({},c.common,c[N(b.method)]);a:for(g in c){f=N(g);for(h in d)if(N(h)===f)continue a;d[g]=c[g]}return e(d,ja(b))}(b);f.method=wb(f.method);f.paramSerializer=D(f.paramSerializer)?l.get(f.paramSerializer):f.paramSerializer;c.$$incOutstandingRequestCount();var k=[],n=[];b=h.resolve(f);p(v,function(a){(a.request||a.requestError)&&k.unshift(a.request,a.requestError);(a.response||a.responseError)&&n.push(a.response,a.responseError)});b=d(b,k);b=b.then(function(b){var c=
b.headers,d=yd(b.data,xd(c),void 0,b.transformRequest);w(d)&&p(c,function(a,b){"content-type"===N(b)&&delete c[b]});w(b.withCredentials)&&!w(a.withCredentials)&&(b.withCredentials=a.withCredentials);return q(b,d).then(g,g)});b=d(b,n);return b=b.finally(function(){c.$$completeOutstandingRequest(C)})}function q(c,d){function g(a){if(a){var c={};p(a,function(a,d){c[d]=function(c){function d(){a(c)}b?k.$applyAsync(d):k.$$phase?d():k.$apply(d)}});return c}}function l(a,c,d,e,g){function f(){q(c,a,d,e,
g)}R&&(200<=a&&300>a?R.put(O,[a,c,wd(d),e,g]):R.remove(O));b?k.$applyAsync(f):(f(),k.$$phase||k.$apply())}function q(a,b,d,e,g){b=-1<=b?b:0;(200<=b&&300>b?K.resolve:K.reject)({data:a,status:b,headers:xd(d),config:c,statusText:e,xhrStatus:g})}function H(a){q(a.data,a.status,ja(a.headers()),a.statusText,a.xhrStatus)}function v(){var a=n.pendingRequests.indexOf(c);-1!==a&&n.pendingRequests.splice(a,1)}var K=h.defer(),F=K.promise,R,x,W=c.headers,r="jsonp"===N(c.method),O=c.url;r?O=m.getTrustedResourceUrl(O):
D(O)||(O=m.valueOf(O));O=G(O,c.paramSerializer(c.params));r&&(O=L(O,c.jsonpCallbackParam));n.pendingRequests.push(c);F.then(v,v);!c.cache&&!a.cache||!1===c.cache||"GET"!==c.method&&"JSONP"!==c.method||(R=E(c.cache)?c.cache:E(a.cache)?a.cache:z);R&&(x=R.get(O),t(x)?x&&A(x.then)?x.then(H,H):I(x)?q(x[1],x[0],ja(x[2]),x[3],x[4]):q(x,200,{},"OK","complete"):R.put(O,F));w(x)&&((x=zd(c.url)?f()[c.xsrfCookieName||a.xsrfCookieName]:void 0)&&(W[c.xsrfHeaderName||a.xsrfHeaderName]=x),e(c.method,O,d,l,W,c.timeout,
c.withCredentials,c.responseType,g(c.eventHandlers),g(c.uploadEventHandlers)));return F}function G(a,b){0<b.length&&(a+=(-1===a.indexOf("?")?"?":"&")+b);return a}function L(a,b){if(/[&?][^=]+=JSON_CALLBACK/.test(a))throw rc("badjsonp",a);if((new RegExp("[&?]"+b+"=")).test(a))throw rc("badjsonp",b,a);return a+=(-1===a.indexOf("?")?"?":"&")+b+"=JSON_CALLBACK"}var z=g("$http");a.paramSerializer=D(a.paramSerializer)?l.get(a.paramSerializer):a.paramSerializer;var v=[];p(d,function(a){v.unshift(D(a)?l.get(a):
l.invoke(a))});n.pendingRequests=[];(function(a){p(arguments,function(a){n[a]=function(b,c){return n(P({},c||{},{method:a,url:b}))}})})("get","delete","head","jsonp");(function(a){p(arguments,function(a){n[a]=function(b,c,d){return n(P({},d||{},{method:a,url:b,data:c}))}})})("post","put","patch");n.defaults=a;return n}]}function Jf(){this.$get=function(){return function(){return new u.XMLHttpRequest}}}function If(){this.$get=["$browser","$jsonpCallbacks","$document","$xhrFactory",function(a,b,d,c){return yg(a,
c,a.defer,b,d[0])}]}function yg(a,b,d,c,e){function f(a,b,d){a=a.replace("JSON_CALLBACK",b);var f=e.createElement("script"),m=null;f.type="text/javascript";f.src=a;f.async=!0;m=function(a){f.removeEventListener("load",m);f.removeEventListener("error",m);e.body.removeChild(f);f=null;var g=-1,G="unknown";a&&("load"!==a.type||c.wasCalled(b)||(a={type:"error"}),G=a.type,g="error"===a.type?404:200);d&&d(g,G)};f.addEventListener("load",m);f.addEventListener("error",m);e.body.appendChild(f);return m}return function(e,
k,h,l,m,n,q,G,L,z){function v(){ma&&ma();y&&y.abort()}function s(a,b,c,e,g,f){t(H)&&d.cancel(H);ma=y=null;a(b,c,e,g,f)}k=k||a.url();if("jsonp"===N(e))var r=c.createCallback(k),ma=f(k,r,function(a,b){var d=200===a&&c.getResponse(r);s(l,a,d,"",b,"complete");c.removeCallback(r)});else{var y=b(e,k);y.open(e,k,!0);p(m,function(a,b){t(a)&&y.setRequestHeader(b,a)});y.onload=function(){var a=y.statusText||"",b="response"in y?y.response:y.responseText,c=1223===y.status?204:y.status;0===c&&(c=b?200:"file"===
ua(k).protocol?404:0);s(l,c,b,y.getAllResponseHeaders(),a,"complete")};y.onerror=function(){s(l,-1,null,null,"","error")};y.onabort=function(){s(l,-1,null,null,"","abort")};y.ontimeout=function(){s(l,-1,null,null,"","timeout")};p(L,function(a,b){y.addEventListener(b,a)});p(z,function(a,b){y.upload.addEventListener(b,a)});q&&(y.withCredentials=!0);if(G)try{y.responseType=G}catch(J){if("json"!==G)throw J;}y.send(w(h)?null:h)}if(0<n)var H=d(v,n);else n&&A(n.then)&&n.then(v)}}function Df(){var a="{{",
b="}}";this.startSymbol=function(b){return b?(a=b,this):a};this.endSymbol=function(a){return a?(b=a,this):b};this.$get=["$parse","$exceptionHandler","$sce",function(d,c,e){function f(a){return"\\\\\\"+a}function g(c){return c.replace(n,a).replace(q,b)}function k(a,b,c,d){var e=a.$watch(function(a){e();return d(a)},b,c);return e}function h(f,h,q,n){function s(a){try{var b=a;a=q?e.getTrusted(q,b):e.valueOf(b);return n&&!t(a)?a:dc(a)}catch(d){c(Fa.interr(f,d))}}if(!f.length||-1===f.indexOf(a)){var p;
h||(h=g(f),p=ka(h),p.exp=f,p.expressions=[],p.$$watchDelegate=k);return p}n=!!n;var r,y,J=0,H=[],ta=[];p=f.length;for(var K=[],F=[];J<p;)if(-1!==(r=f.indexOf(a,J))&&-1!==(y=f.indexOf(b,r+l)))J!==r&&K.push(g(f.substring(J,r))),J=f.substring(r+l,y),H.push(J),ta.push(d(J,s)),J=y+m,F.push(K.length),K.push("");else{J!==p&&K.push(g(f.substring(J)));break}q&&1<K.length&&Fa.throwNoconcat(f);if(!h||H.length){var R=function(a){for(var b=0,c=H.length;b<c;b++){if(n&&w(a[b]))return;K[F[b]]=a[b]}return K.join("")};
return P(function(a){var b=0,d=H.length,e=Array(d);try{for(;b<d;b++)e[b]=ta[b](a);return R(e)}catch(g){c(Fa.interr(f,g))}},{exp:f,expressions:H,$$watchDelegate:function(a,b){var c;return a.$watchGroup(ta,function(d,e){var g=R(d);A(b)&&b.call(this,g,d!==e?c:g,a);c=g})}})}}var l=a.length,m=b.length,n=new RegExp(a.replace(/./g,f),"g"),q=new RegExp(b.replace(/./g,f),"g");h.startSymbol=function(){return a};h.endSymbol=function(){return b};return h}]}function Ef(){this.$get=["$rootScope","$window","$q",
"$$q","$browser",function(a,b,d,c,e){function f(f,h,l,m){function n(){q?f.apply(null,G):f(v)}var q=4<arguments.length,G=q?ya.call(arguments,4):[],L=b.setInterval,p=b.clearInterval,v=0,s=t(m)&&!m,r=(s?c:d).defer(),ma=r.promise;l=t(l)?l:0;ma.$$intervalId=L(function(){s?e.defer(n):a.$evalAsync(n);r.notify(v++);0<l&&v>=l&&(r.resolve(v),p(ma.$$intervalId),delete g[ma.$$intervalId]);s||a.$apply()},h);g[ma.$$intervalId]=r;return ma}var g={};f.cancel=function(a){return a&&a.$$intervalId in g?(g[a.$$intervalId].promise.$$state.pur=
!0,g[a.$$intervalId].reject("canceled"),b.clearInterval(a.$$intervalId),delete g[a.$$intervalId],!0):!1};return f}]}function tc(a){a=a.split("/");for(var b=a.length;b--;)a[b]=gb(a[b]);return a.join("/")}function Ad(a,b){var d=ua(a);b.$$protocol=d.protocol;b.$$host=d.hostname;b.$$port=Z(d.port)||zg[d.protocol]||null}function Bd(a,b){if(Ag.test(a))throw mb("badpath",a);var d="/"!==a.charAt(0);d&&(a="/"+a);var c=ua(a);b.$$path=decodeURIComponent(d&&"/"===c.pathname.charAt(0)?c.pathname.substring(1):
c.pathname);b.$$search=Tc(c.search);b.$$hash=decodeURIComponent(c.hash);b.$$path&&"/"!==b.$$path.charAt(0)&&(b.$$path="/"+b.$$path)}function uc(a,b){return a.slice(0,b.length)===b}function va(a,b){if(uc(b,a))return b.substr(a.length)}function La(a){var b=a.indexOf("#");return-1===b?a:a.substr(0,b)}function nb(a){return a.replace(/(#.+)|#$/,"$1")}function vc(a,b,d){this.$$html5=!0;d=d||"";Ad(a,this);this.$$parse=function(a){var d=va(b,a);if(!D(d))throw mb("ipthprfx",a,b);Bd(d,this);this.$$path||(this.$$path=
"/");this.$$compose()};this.$$compose=function(){var a=cc(this.$$search),d=this.$$hash?"#"+gb(this.$$hash):"";this.$$url=tc(this.$$path)+(a?"?"+a:"")+d;this.$$absUrl=b+this.$$url.substr(1);this.$$urlUpdatedByLocation=!0};this.$$parseLinkUrl=function(c,e){if(e&&"#"===e[0])return this.hash(e.slice(1)),!0;var f,g;t(f=va(a,c))?(g=f,g=d&&t(f=va(d,f))?b+(va("/",f)||f):a+g):t(f=va(b,c))?g=b+f:b===c+"/"&&(g=b);g&&this.$$parse(g);return!!g}}function wc(a,b,d){Ad(a,this);this.$$parse=function(c){var e=va(a,
c)||va(b,c),f;w(e)||"#"!==e.charAt(0)?this.$$html5?f=e:(f="",w(e)&&(a=c,this.replace())):(f=va(d,e),w(f)&&(f=e));Bd(f,this);c=this.$$path;var e=a,g=/^\/[A-Z]:(\/.*)/;uc(f,e)&&(f=f.replace(e,""));g.exec(f)||(c=(f=g.exec(c))?f[1]:c);this.$$path=c;this.$$compose()};this.$$compose=function(){var b=cc(this.$$search),e=this.$$hash?"#"+gb(this.$$hash):"";this.$$url=tc(this.$$path)+(b?"?"+b:"")+e;this.$$absUrl=a+(this.$$url?d+this.$$url:"");this.$$urlUpdatedByLocation=!0};this.$$parseLinkUrl=function(b,d){return La(a)===
La(b)?(this.$$parse(b),!0):!1}}function Cd(a,b,d){this.$$html5=!0;wc.apply(this,arguments);this.$$parseLinkUrl=function(c,e){if(e&&"#"===e[0])return this.hash(e.slice(1)),!0;var f,g;a===La(c)?f=c:(g=va(b,c))?f=a+d+g:b===c+"/"&&(f=b);f&&this.$$parse(f);return!!f};this.$$compose=function(){var b=cc(this.$$search),e=this.$$hash?"#"+gb(this.$$hash):"";this.$$url=tc(this.$$path)+(b?"?"+b:"")+e;this.$$absUrl=a+d+this.$$url;this.$$urlUpdatedByLocation=!0}}function Lb(a){return function(){return this[a]}}
function Dd(a,b){return function(d){if(w(d))return this[a];this[a]=b(d);this.$$compose();return this}}function Lf(){var a="!",b={enabled:!1,requireBase:!0,rewriteLinks:!0};this.hashPrefix=function(b){return t(b)?(a=b,this):a};this.html5Mode=function(a){if(Na(a))return b.enabled=a,this;if(E(a)){Na(a.enabled)&&(b.enabled=a.enabled);Na(a.requireBase)&&(b.requireBase=a.requireBase);if(Na(a.rewriteLinks)||D(a.rewriteLinks))b.rewriteLinks=a.rewriteLinks;return this}return b};this.$get=["$rootScope","$browser",
"$sniffer","$rootElement","$window",function(d,c,e,f,g){function k(a,b,d){var e=l.url(),g=l.$$state;try{c.url(a,b,d),l.$$state=c.state()}catch(f){throw l.url(e),l.$$state=g,f;}}function h(a,b){d.$broadcast("$locationChangeSuccess",l.absUrl(),a,l.$$state,b)}var l,m;m=c.baseHref();var n=c.url(),q;if(b.enabled){if(!m&&b.requireBase)throw mb("nobase");q=n.substring(0,n.indexOf("/",n.indexOf("//")+2))+(m||"/");m=e.history?vc:Cd}else q=La(n),m=wc;var G=q.substr(0,La(q).lastIndexOf("/")+1);l=new m(q,G,"#"+
a);l.$$parseLinkUrl(n,n);l.$$state=c.state();var p=/^\s*(javascript|mailto):/i;f.on("click",function(a){var e=b.rewriteLinks;if(e&&!a.ctrlKey&&!a.metaKey&&!a.shiftKey&&2!==a.which&&2!==a.button){for(var h=B(a.target);"a"!==za(h[0]);)if(h[0]===f[0]||!(h=h.parent())[0])return;if(!D(e)||!w(h.attr(e))){var e=h.prop("href"),k=h.attr("href")||h.attr("xlink:href");E(e)&&"[object SVGAnimatedString]"===e.toString()&&(e=ua(e.animVal).href);p.test(e)||!e||h.attr("target")||a.isDefaultPrevented()||!l.$$parseLinkUrl(e,
k)||(a.preventDefault(),l.absUrl()!==c.url()&&(d.$apply(),g.angular["ff-684208-preventDefault"]=!0))}}});nb(l.absUrl())!==nb(n)&&c.url(l.absUrl(),!0);var z=!0;c.onUrlChange(function(a,b){uc(a,G)?(d.$evalAsync(function(){var c=l.absUrl(),e=l.$$state,g;a=nb(a);l.$$parse(a);l.$$state=b;g=d.$broadcast("$locationChangeStart",a,c,b,e).defaultPrevented;l.absUrl()===a&&(g?(l.$$parse(c),l.$$state=e,k(c,!1,e)):(z=!1,h(c,e)))}),d.$$phase||d.$digest()):g.location.href=a});d.$watch(function(){if(z||l.$$urlUpdatedByLocation){l.$$urlUpdatedByLocation=
!1;var a=nb(c.url()),b=nb(l.absUrl()),g=c.state(),f=l.$$replace,m=a!==b||l.$$html5&&e.history&&g!==l.$$state;if(z||m)z=!1,d.$evalAsync(function(){var b=l.absUrl(),c=d.$broadcast("$locationChangeStart",b,a,l.$$state,g).defaultPrevented;l.absUrl()===b&&(c?(l.$$parse(a),l.$$state=g):(m&&k(b,f,g===l.$$state?null:l.$$state),h(a,g)))})}l.$$replace=!1});return l}]}function Mf(){var a=!0,b=this;this.debugEnabled=function(b){return t(b)?(a=b,this):a};this.$get=["$window",function(d){function c(a){$b(a)&&(a.stack&&
f?a=a.message&&-1===a.stack.indexOf(a.message)?"Error: "+a.message+"\n"+a.stack:a.stack:a.sourceURL&&(a=a.message+"\n"+a.sourceURL+":"+a.line));return a}function e(a){var b=d.console||{},e=b[a]||b.log||C;return function(){var a=[];p(arguments,function(b){a.push(c(b))});return Function.prototype.apply.call(e,b,a)}}var f=Ca||/\bEdge\//.test(d.navigator&&d.navigator.userAgent);return{log:e("log"),info:e("info"),warn:e("warn"),error:e("error"),debug:function(){var c=e("debug");return function(){a&&c.apply(b,
arguments)}}()}}]}function Bg(a){return a+""}function Cg(a,b){return"undefined"!==typeof a?a:b}function Ed(a,b){return"undefined"===typeof a?b:"undefined"===typeof b?a:a+b}function Dg(a,b){switch(a.type){case r.MemberExpression:if(a.computed)return!1;break;case r.UnaryExpression:return 1;case r.BinaryExpression:return"+"!==a.operator?1:!1;case r.CallExpression:return!1}return void 0===b?Fd:b}function V(a,b,d){var c,e,f=a.isPure=Dg(a,d);switch(a.type){case r.Program:c=!0;p(a.body,function(a){V(a.expression,
b,f);c=c&&a.expression.constant});a.constant=c;break;case r.Literal:a.constant=!0;a.toWatch=[];break;case r.UnaryExpression:V(a.argument,b,f);a.constant=a.argument.constant;a.toWatch=a.argument.toWatch;break;case r.BinaryExpression:V(a.left,b,f);V(a.right,b,f);a.constant=a.left.constant&&a.right.constant;a.toWatch=a.left.toWatch.concat(a.right.toWatch);break;case r.LogicalExpression:V(a.left,b,f);V(a.right,b,f);a.constant=a.left.constant&&a.right.constant;a.toWatch=a.constant?[]:[a];break;case r.ConditionalExpression:V(a.test,
b,f);V(a.alternate,b,f);V(a.consequent,b,f);a.constant=a.test.constant&&a.alternate.constant&&a.consequent.constant;a.toWatch=a.constant?[]:[a];break;case r.Identifier:a.constant=!1;a.toWatch=[a];break;case r.MemberExpression:V(a.object,b,f);a.computed&&V(a.property,b,f);a.constant=a.object.constant&&(!a.computed||a.property.constant);a.toWatch=a.constant?[]:[a];break;case r.CallExpression:c=d=a.filter?!b(a.callee.name).$stateful:!1;e=[];p(a.arguments,function(a){V(a,b,f);c=c&&a.constant;e.push.apply(e,
a.toWatch)});a.constant=c;a.toWatch=d?e:[a];break;case r.AssignmentExpression:V(a.left,b,f);V(a.right,b,f);a.constant=a.left.constant&&a.right.constant;a.toWatch=[a];break;case r.ArrayExpression:c=!0;e=[];p(a.elements,function(a){V(a,b,f);c=c&&a.constant;e.push.apply(e,a.toWatch)});a.constant=c;a.toWatch=e;break;case r.ObjectExpression:c=!0;e=[];p(a.properties,function(a){V(a.value,b,f);c=c&&a.value.constant;e.push.apply(e,a.value.toWatch);a.computed&&(V(a.key,b,!1),c=c&&a.key.constant,e.push.apply(e,
a.key.toWatch))});a.constant=c;a.toWatch=e;break;case r.ThisExpression:a.constant=!1;a.toWatch=[];break;case r.LocalsExpression:a.constant=!1,a.toWatch=[]}}function Gd(a){if(1===a.length){a=a[0].expression;var b=a.toWatch;return 1!==b.length?b:b[0]!==a?b:void 0}}function Hd(a){return a.type===r.Identifier||a.type===r.MemberExpression}function Id(a){if(1===a.body.length&&Hd(a.body[0].expression))return{type:r.AssignmentExpression,left:a.body[0].expression,right:{type:r.NGValueParameter},operator:"="}}
function Jd(a){this.$filter=a}function Kd(a){this.$filter=a}function xc(a,b,d){this.ast=new r(a,d);this.astCompiler=d.csp?new Kd(b):new Jd(b)}function yc(a){return A(a.valueOf)?a.valueOf():Eg.call(a)}function Nf(){var a=S(),b={"true":!0,"false":!1,"null":null,undefined:void 0},d,c;this.addLiteral=function(a,c){b[a]=c};this.setIdentifierFns=function(a,b){d=a;c=b;return this};this.$get=["$filter",function(e){function f(a,b,c){return null==a||null==b?a===b:"object"!==typeof a||(a=yc(a),"object"!==typeof a||
c)?a===b||a!==a&&b!==b:!1}function g(a,b,c,d,e){var g=d.inputs,h;if(1===g.length){var k=f,g=g[0];return a.$watch(function(a){var b=g(a);f(b,k,g.isPure)||(h=d(a,void 0,void 0,[b]),k=b&&yc(b));return h},b,c,e)}for(var l=[],m=[],n=0,p=g.length;n<p;n++)l[n]=f,m[n]=null;return a.$watch(function(a){for(var b=!1,c=0,e=g.length;c<e;c++){var k=g[c](a);if(b||(b=!f(k,l[c],g[c].isPure)))m[c]=k,l[c]=k&&yc(k)}b&&(h=d(a,void 0,void 0,m));return h},b,c,e)}function k(a,b,c,d,e){function f(a){return d(a)}function h(a,
c,d){l=a;A(b)&&b(a,c,d);t(a)&&d.$$postDigest(function(){t(l)&&k()})}var k,l;return k=d.inputs?g(a,h,c,d,e):a.$watch(f,h,c)}function h(a,b,c,d){function e(a){var b=!0;p(a,function(a){t(a)||(b=!1)});return b}var g,f;return g=a.$watch(function(a){return d(a)},function(a,c,d){f=a;A(b)&&b(a,c,d);e(a)&&d.$$postDigest(function(){e(f)&&g()})},c)}function l(a,b,c,d){var e=a.$watch(function(a){e();return d(a)},b,c);return e}function m(a,b){if(!b)return a;var c=a.$$watchDelegate,d=!1,e=c!==h&&c!==k?function(c,
e,g,f){g=d&&f?f[0]:a(c,e,g,f);return b(g,c,e)}:function(c,d,e,g){e=a(c,d,e,g);c=b(e,c,d);return t(e)?c:e},d=!a.inputs;c&&c!==g?(e.$$watchDelegate=c,e.inputs=a.inputs):b.$stateful||(e.$$watchDelegate=g,e.inputs=a.inputs?a.inputs:[a]);e.inputs&&(e.inputs=e.inputs.map(function(a){return a.isPure===Fd?function(b){return a(b)}:a}));return e}var n={csp:Ja().noUnsafeEval,literals:pa(b),isIdentifierStart:A(d)&&d,isIdentifierContinue:A(c)&&c};return function(b,c){var d,f,p;switch(typeof b){case "string":return p=
b=b.trim(),d=a[p],d||(":"===b.charAt(0)&&":"===b.charAt(1)&&(f=!0,b=b.substring(2)),d=new zc(n),d=(new xc(d,e,n)).parse(b),d.constant?d.$$watchDelegate=l:f?d.$$watchDelegate=d.literal?h:k:d.inputs&&(d.$$watchDelegate=g),a[p]=d),m(d,c);case "function":return m(b,c);default:return m(C,c)}}}]}function Pf(){var a=!0;this.$get=["$rootScope","$exceptionHandler",function(b,d){return Ld(function(a){b.$evalAsync(a)},d,a)}];this.errorOnUnhandledRejections=function(b){return t(b)?(a=b,this):a}}function Qf(){var a=
!0;this.$get=["$browser","$exceptionHandler",function(b,d){return Ld(function(a){b.defer(a)},d,a)}];this.errorOnUnhandledRejections=function(b){return t(b)?(a=b,this):a}}function Ld(a,b,d){function c(){return new e}function e(){var a=this.promise=new f;this.resolve=function(b){h(a,b)};this.reject=function(b){m(a,b)};this.notify=function(b){q(a,b)}}function f(){this.$$state={status:0}}function g(){for(;!t&&u.length;){var a=u.shift();if(!a.pur){a.pur=!0;var c=a.value,c="Possibly unhandled rejection: "+
("function"===typeof c?c.toString().replace(/ \{[\s\S]*$/,""):w(c)?"undefined":"string"!==typeof c?De(c,void 0):c);$b(a.value)?b(a.value,c):b(c)}}}function k(b){!d||b.pending||2!==b.status||b.pur||(0===t&&0===u.length&&a(g),u.push(b));!b.processScheduled&&b.pending&&(b.processScheduled=!0,++t,a(function(){var c,e,f;f=b.pending;b.processScheduled=!1;b.pending=void 0;try{for(var k=0,l=f.length;k<l;++k){b.pur=!0;e=f[k][0];c=f[k][b.status];try{A(c)?h(e,c(b.value)):1===b.status?h(e,b.value):m(e,b.value)}catch(n){m(e,
n)}}}finally{--t,d&&0===t&&a(g)}}))}function h(a,b){a.$$state.status||(b===a?n(a,s("qcycle",b)):l(a,b))}function l(a,b){function c(b){f||(f=!0,l(a,b))}function d(b){f||(f=!0,n(a,b))}function e(b){q(a,b)}var g,f=!1;try{if(E(b)||A(b))g=b.then;A(g)?(a.$$state.status=-1,g.call(b,c,d,e)):(a.$$state.value=b,a.$$state.status=1,k(a.$$state))}catch(h){d(h)}}function m(a,b){a.$$state.status||n(a,b)}function n(a,b){a.$$state.value=b;a.$$state.status=2;k(a.$$state)}function q(c,d){var e=c.$$state.pending;0>=
c.$$state.status&&e&&e.length&&a(function(){for(var a,c,g=0,f=e.length;g<f;g++){c=e[g][0];a=e[g][3];try{q(c,A(a)?a(d):d)}catch(h){b(h)}}})}function G(a){var b=new f;m(b,a);return b}function r(a,b,c){var d=null;try{A(c)&&(d=c())}catch(e){return G(e)}return d&&A(d.then)?d.then(function(){return b(a)},G):b(a)}function z(a,b,c,d){var e=new f;h(e,a);return e.then(b,c,d)}function v(a){if(!A(a))throw s("norslvr",a);var b=new f;a(function(a){h(b,a)},function(a){m(b,a)});return b}var s=M("$q",TypeError),t=
0,u=[];P(f.prototype,{then:function(a,b,c){if(w(a)&&w(b)&&w(c))return this;var d=new f;this.$$state.pending=this.$$state.pending||[];this.$$state.pending.push([d,a,b,c]);0<this.$$state.status&&k(this.$$state);return d},"catch":function(a){return this.then(null,a)},"finally":function(a,b){return this.then(function(b){return r(b,y,a)},function(b){return r(b,G,a)},b)}});var y=z;v.prototype=f.prototype;v.defer=c;v.reject=G;v.when=z;v.resolve=y;v.all=function(a){var b=new f,c=0,d=I(a)?[]:{};p(a,function(a,
e){c++;z(a).then(function(a){d[e]=a;--c||h(b,d)},function(a){m(b,a)})});0===c&&h(b,d);return b};v.race=function(a){var b=c();p(a,function(a){z(a).then(b.resolve,b.reject)});return b.promise};return v}function Zf(){this.$get=["$window","$timeout",function(a,b){var d=a.requestAnimationFrame||a.webkitRequestAnimationFrame,c=a.cancelAnimationFrame||a.webkitCancelAnimationFrame||a.webkitCancelRequestAnimationFrame,e=!!d,f=e?function(a){var b=d(a);return function(){c(b)}}:function(a){var c=b(a,16.66,!1);
return function(){b.cancel(c)}};f.supported=e;return f}]}function Of(){function a(a){function b(){this.$$watchers=this.$$nextSibling=this.$$childHead=this.$$childTail=null;this.$$listeners={};this.$$listenerCount={};this.$$watchersCount=0;this.$id=++sb;this.$$ChildScope=null}b.prototype=a;return b}var b=10,d=M("$rootScope"),c=null,e=null;this.digestTtl=function(a){arguments.length&&(b=a);return b};this.$get=["$exceptionHandler","$parse","$browser",function(f,g,k){function h(a){a.currentScope.$$destroyed=
!0}function l(a){9===Ca&&(a.$$childHead&&l(a.$$childHead),a.$$nextSibling&&l(a.$$nextSibling));a.$parent=a.$$nextSibling=a.$$prevSibling=a.$$childHead=a.$$childTail=a.$root=a.$$watchers=null}function m(){this.$id=++sb;this.$$phase=this.$parent=this.$$watchers=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=null;this.$root=this;this.$$destroyed=!1;this.$$listeners={};this.$$listenerCount={};this.$$watchersCount=0;this.$$isolateBindings=null}function n(a){if(s.$$phase)throw d("inprog",
s.$$phase);s.$$phase=a}function q(a,b){do a.$$watchersCount+=b;while(a=a.$parent)}function G(a,b,c){do a.$$listenerCount[c]-=b,0===a.$$listenerCount[c]&&delete a.$$listenerCount[c];while(a=a.$parent)}function r(){}function z(){for(;y.length;)try{y.shift()()}catch(a){f(a)}e=null}function v(){null===e&&(e=k.defer(function(){s.$apply(z)}))}m.prototype={constructor:m,$new:function(b,c){var d;c=c||this;b?(d=new m,d.$root=this.$root):(this.$$ChildScope||(this.$$ChildScope=a(this)),d=new this.$$ChildScope);
d.$parent=c;d.$$prevSibling=c.$$childTail;c.$$childHead?(c.$$childTail.$$nextSibling=d,c.$$childTail=d):c.$$childHead=c.$$childTail=d;(b||c!==this)&&d.$on("$destroy",h);return d},$watch:function(a,b,d,e){var f=g(a);if(f.$$watchDelegate)return f.$$watchDelegate(this,b,d,f,a);var h=this,k=h.$$watchers,l={fn:b,last:r,get:f,exp:e||a,eq:!!d};c=null;A(b)||(l.fn=C);k||(k=h.$$watchers=[],k.$$digestWatchIndex=-1);k.unshift(l);k.$$digestWatchIndex++;q(this,1);return function(){var a=db(k,l);0<=a&&(q(h,-1),
a<k.$$digestWatchIndex&&k.$$digestWatchIndex--);c=null}},$watchGroup:function(a,b){function c(){h=!1;k?(k=!1,b(e,e,f)):b(e,d,f)}var d=Array(a.length),e=Array(a.length),g=[],f=this,h=!1,k=!0;if(!a.length){var l=!0;f.$evalAsync(function(){l&&b(e,e,f)});return function(){l=!1}}if(1===a.length)return this.$watch(a[0],function(a,c,g){e[0]=a;d[0]=c;b(e,a===c?e:d,g)});p(a,function(a,b){var k=f.$watch(a,function(a,g){e[b]=a;d[b]=g;h||(h=!0,f.$evalAsync(c))});g.push(k)});return function(){for(;g.length;)g.shift()()}},
$watchCollection:function(a,b){function c(a){e=a;var b,d,g,h;if(!w(e)){if(E(e))if(xa(e))for(f!==n&&(f=n,p=f.length=0,l++),a=e.length,p!==a&&(l++,f.length=p=a),b=0;b<a;b++)h=f[b],g=e[b],d=h!==h&&g!==g,d||h===g||(l++,f[b]=g);else{f!==q&&(f=q={},p=0,l++);a=0;for(b in e)ra.call(e,b)&&(a++,g=e[b],h=f[b],b in f?(d=h!==h&&g!==g,d||h===g||(l++,f[b]=g)):(p++,f[b]=g,l++));if(p>a)for(b in l++,f)ra.call(e,b)||(p--,delete f[b])}else f!==e&&(f=e,l++);return l}}c.$stateful=!0;var d=this,e,f,h,k=1<b.length,l=0,m=
g(a,c),n=[],q={},s=!0,p=0;return this.$watch(m,function(){s?(s=!1,b(e,e,d)):b(e,h,d);if(k)if(E(e))if(xa(e)){h=Array(e.length);for(var a=0;a<e.length;a++)h[a]=e[a]}else for(a in h={},e)ra.call(e,a)&&(h[a]=e[a]);else h=e})},$digest:function(){var a,g,h,l,m,q,p,G=b,y,v=[],w,B;n("$digest");k.$$checkUrlChange();this===s&&null!==e&&(k.defer.cancel(e),z());c=null;do{p=!1;y=this;for(q=0;q<t.length;q++){try{B=t[q],l=B.fn,l(B.scope,B.locals)}catch(C){f(C)}c=null}t.length=0;a:do{if(q=y.$$watchers)for(q.$$digestWatchIndex=
q.length;q.$$digestWatchIndex--;)try{if(a=q[q.$$digestWatchIndex])if(m=a.get,(g=m(y))!==(h=a.last)&&!(a.eq?sa(g,h):T(g)&&T(h)))p=!0,c=a,a.last=a.eq?pa(g,null):g,l=a.fn,l(g,h===r?g:h,y),5>G&&(w=4-G,v[w]||(v[w]=[]),v[w].push({msg:A(a.exp)?"fn: "+(a.exp.name||a.exp.toString()):a.exp,newVal:g,oldVal:h}));else if(a===c){p=!1;break a}}catch(E){f(E)}if(!(q=y.$$watchersCount&&y.$$childHead||y!==this&&y.$$nextSibling))for(;y!==this&&!(q=y.$$nextSibling);)y=y.$parent}while(y=q);if((p||t.length)&&!G--)throw s.$$phase=
null,d("infdig",b,v);}while(p||t.length);for(s.$$phase=null;J<u.length;)try{u[J++]()}catch(D){f(D)}u.length=J=0;k.$$checkUrlChange()},$destroy:function(){if(!this.$$destroyed){var a=this.$parent;this.$broadcast("$destroy");this.$$destroyed=!0;this===s&&k.$$applicationDestroyed();q(this,-this.$$watchersCount);for(var b in this.$$listenerCount)G(this,this.$$listenerCount[b],b);a&&a.$$childHead===this&&(a.$$childHead=this.$$nextSibling);a&&a.$$childTail===this&&(a.$$childTail=this.$$prevSibling);this.$$prevSibling&&
(this.$$prevSibling.$$nextSibling=this.$$nextSibling);this.$$nextSibling&&(this.$$nextSibling.$$prevSibling=this.$$prevSibling);this.$destroy=this.$digest=this.$apply=this.$evalAsync=this.$applyAsync=C;this.$on=this.$watch=this.$watchGroup=function(){return C};this.$$listeners={};this.$$nextSibling=null;l(this)}},$eval:function(a,b){return g(a)(this,b)},$evalAsync:function(a,b){s.$$phase||t.length||k.defer(function(){t.length&&s.$digest()});t.push({scope:this,fn:g(a),locals:b})},$$postDigest:function(a){u.push(a)},
$apply:function(a){try{n("$apply");try{return this.$eval(a)}finally{s.$$phase=null}}catch(b){f(b)}finally{try{s.$digest()}catch(c){throw f(c),c;}}},$applyAsync:function(a){function b(){c.$eval(a)}var c=this;a&&y.push(b);a=g(a);v()},$on:function(a,b){var c=this.$$listeners[a];c||(this.$$listeners[a]=c=[]);c.push(b);var d=this;do d.$$listenerCount[a]||(d.$$listenerCount[a]=0),d.$$listenerCount[a]++;while(d=d.$parent);var e=this;return function(){var d=c.indexOf(b);-1!==d&&(c[d]=null,G(e,1,a))}},$emit:function(a,
b){var c=[],d,e=this,g=!1,h={name:a,targetScope:e,stopPropagation:function(){g=!0},preventDefault:function(){h.defaultPrevented=!0},defaultPrevented:!1},k=eb([h],arguments,1),l,m;do{d=e.$$listeners[a]||c;h.currentScope=e;l=0;for(m=d.length;l<m;l++)if(d[l])try{d[l].apply(null,k)}catch(n){f(n)}else d.splice(l,1),l--,m--;if(g)return h.currentScope=null,h;e=e.$parent}while(e);h.currentScope=null;return h},$broadcast:function(a,b){var c=this,d=this,e={name:a,targetScope:this,preventDefault:function(){e.defaultPrevented=
!0},defaultPrevented:!1};if(!this.$$listenerCount[a])return e;for(var g=eb([e],arguments,1),h,k;c=d;){e.currentScope=c;d=c.$$listeners[a]||[];h=0;for(k=d.length;h<k;h++)if(d[h])try{d[h].apply(null,g)}catch(l){f(l)}else d.splice(h,1),h--,k--;if(!(d=c.$$listenerCount[a]&&c.$$childHead||c!==this&&c.$$nextSibling))for(;c!==this&&!(d=c.$$nextSibling);)c=c.$parent}e.currentScope=null;return e}};var s=new m,t=s.$$asyncQueue=[],u=s.$$postDigestQueue=[],y=s.$$applyAsyncQueue=[],J=0;return s}]}function Ge(){var a=
/^\s*(https?|ftp|mailto|tel|file):/,b=/^\s*((https?|ftp|file|blob):|data:image\/)/;this.aHrefSanitizationWhitelist=function(b){return t(b)?(a=b,this):a};this.imgSrcSanitizationWhitelist=function(a){return t(a)?(b=a,this):b};this.$get=function(){return function(d,c){var e=c?b:a,f;f=ua(d).href;return""===f||f.match(e)?d:"unsafe:"+f}}}function Fg(a){if("self"===a)return a;if(D(a)){if(-1<a.indexOf("***"))throw wa("iwcard",a);a=Md(a).replace(/\\\*\\\*/g,".*").replace(/\\\*/g,"[^:/.?&;]*");return new RegExp("^"+
a+"$")}if(ab(a))return new RegExp("^"+a.source+"$");throw wa("imatcher");}function Nd(a){var b=[];t(a)&&p(a,function(a){b.push(Fg(a))});return b}function Sf(){this.SCE_CONTEXTS=oa;var a=["self"],b=[];this.resourceUrlWhitelist=function(b){arguments.length&&(a=Nd(b));return a};this.resourceUrlBlacklist=function(a){arguments.length&&(b=Nd(a));return b};this.$get=["$injector",function(d){function c(a,b){return"self"===a?zd(b):!!a.exec(b.href)}function e(a){var b=function(a){this.$$unwrapTrustedValue=
function(){return a}};a&&(b.prototype=new a);b.prototype.valueOf=function(){return this.$$unwrapTrustedValue()};b.prototype.toString=function(){return this.$$unwrapTrustedValue().toString()};return b}var f=function(a){throw wa("unsafe");};d.has("$sanitize")&&(f=d.get("$sanitize"));var g=e(),k={};k[oa.HTML]=e(g);k[oa.CSS]=e(g);k[oa.URL]=e(g);k[oa.JS]=e(g);k[oa.RESOURCE_URL]=e(k[oa.URL]);return{trustAs:function(a,b){var c=k.hasOwnProperty(a)?k[a]:null;if(!c)throw wa("icontext",a,b);if(null===b||w(b)||
""===b)return b;if("string"!==typeof b)throw wa("itype",a);return new c(b)},getTrusted:function(d,e){if(null===e||w(e)||""===e)return e;var g=k.hasOwnProperty(d)?k[d]:null;if(g&&e instanceof g)return e.$$unwrapTrustedValue();if(d===oa.RESOURCE_URL){var g=ua(e.toString()),n,q,p=!1;n=0;for(q=a.length;n<q;n++)if(c(a[n],g)){p=!0;break}if(p)for(n=0,q=b.length;n<q;n++)if(c(b[n],g)){p=!1;break}if(p)return e;throw wa("insecurl",e.toString());}if(d===oa.HTML)return f(e);throw wa("unsafe");},valueOf:function(a){return a instanceof
g?a.$$unwrapTrustedValue():a}}}]}function Rf(){var a=!0;this.enabled=function(b){arguments.length&&(a=!!b);return a};this.$get=["$parse","$sceDelegate",function(b,d){if(a&&8>Ca)throw wa("iequirks");var c=ja(oa);c.isEnabled=function(){return a};c.trustAs=d.trustAs;c.getTrusted=d.getTrusted;c.valueOf=d.valueOf;a||(c.trustAs=c.getTrusted=function(a,b){return b},c.valueOf=bb);c.parseAs=function(a,d){var e=b(d);return e.literal&&e.constant?e:b(d,function(b){return c.getTrusted(a,b)})};var e=c.parseAs,
f=c.getTrusted,g=c.trustAs;p(oa,function(a,b){var d=N(b);c[("parse_as_"+d).replace(Ac,jb)]=function(b){return e(a,b)};c[("get_trusted_"+d).replace(Ac,jb)]=function(b){return f(a,b)};c[("trust_as_"+d).replace(Ac,jb)]=function(b){return g(a,b)}});return c}]}function Tf(){this.$get=["$window","$document",function(a,b){var d={},c=!((!a.nw||!a.nw.process)&&a.chrome&&(a.chrome.app&&a.chrome.app.runtime||!a.chrome.app&&a.chrome.runtime&&a.chrome.runtime.id))&&a.history&&a.history.pushState,e=Z((/android (\d+)/.exec(N((a.navigator||
{}).userAgent))||[])[1]),f=/Boxee/i.test((a.navigator||{}).userAgent),g=b[0]||{},k=g.body&&g.body.style,h=!1,l=!1;k&&(h=!!("transition"in k||"webkitTransition"in k),l=!!("animation"in k||"webkitAnimation"in k));return{history:!(!c||4>e||f),hasEvent:function(a){if("input"===a&&Ca)return!1;if(w(d[a])){var b=g.createElement("div");d[a]="on"+a in b}return d[a]},csp:Ja(),transitions:h,animations:l,android:e}}]}function Vf(){var a;this.httpOptions=function(b){return b?(a=b,this):a};this.$get=["$exceptionHandler",
"$templateCache","$http","$q","$sce",function(b,d,c,e,f){function g(k,h){g.totalPendingRequests++;if(!D(k)||w(d.get(k)))k=f.getTrustedResourceUrl(k);var l=c.defaults&&c.defaults.transformResponse;I(l)?l=l.filter(function(a){return a!==qc}):l===qc&&(l=null);return c.get(k,P({cache:d,transformResponse:l},a)).finally(function(){g.totalPendingRequests--}).then(function(a){d.put(k,a.data);return a.data},function(a){h||(a=Gg("tpload",k,a.status,a.statusText),b(a));return e.reject(a)})}g.totalPendingRequests=
0;return g}]}function Wf(){this.$get=["$rootScope","$browser","$location",function(a,b,d){return{findBindings:function(a,b,d){a=a.getElementsByClassName("ng-binding");var g=[];p(a,function(a){var c=$.element(a).data("$binding");c&&p(c,function(c){d?(new RegExp("(^|\\s)"+Md(b)+"(\\s|\\||$)")).test(c)&&g.push(a):-1!==c.indexOf(b)&&g.push(a)})});return g},findModels:function(a,b,d){for(var g=["ng-","data-ng-","ng\\:"],k=0;k<g.length;++k){var h=a.querySelectorAll("["+g[k]+"model"+(d?"=":"*=")+'"'+b+'"]');
if(h.length)return h}},getLocation:function(){return d.url()},setLocation:function(b){b!==d.url()&&(d.url(b),a.$digest())},whenStable:function(a){b.notifyWhenNoOutstandingRequests(a)}}}]}function Xf(){this.$get=["$rootScope","$browser","$q","$$q","$exceptionHandler",function(a,b,d,c,e){function f(f,h,l){A(f)||(l=h,h=f,f=C);var m=ya.call(arguments,3),n=t(l)&&!l,q=(n?c:d).defer(),p=q.promise,r;r=b.defer(function(){try{q.resolve(f.apply(null,m))}catch(b){q.reject(b),e(b)}finally{delete g[p.$$timeoutId]}n||
a.$apply()},h);p.$$timeoutId=r;g[r]=q;return p}var g={};f.cancel=function(a){return a&&a.$$timeoutId in g?(g[a.$$timeoutId].promise.$$state.pur=!0,g[a.$$timeoutId].reject("canceled"),delete g[a.$$timeoutId],b.defer.cancel(a.$$timeoutId)):!1};return f}]}function ua(a){Ca&&(X.setAttribute("href",a),a=X.href);X.setAttribute("href",a);return{href:X.href,protocol:X.protocol?X.protocol.replace(/:$/,""):"",host:X.host,search:X.search?X.search.replace(/^\?/,""):"",hash:X.hash?X.hash.replace(/^#/,""):"",hostname:X.hostname,
port:X.port,pathname:"/"===X.pathname.charAt(0)?X.pathname:"/"+X.pathname}}function zd(a){a=D(a)?ua(a):a;return a.protocol===Od.protocol&&a.host===Od.host}function Yf(){this.$get=ka(u)}function Pd(a){function b(a){try{return decodeURIComponent(a)}catch(b){return a}}var d=a[0]||{},c={},e="";return function(){var a,g,k,h,l;try{a=d.cookie||""}catch(m){a=""}if(a!==e)for(e=a,a=e.split("; "),c={},k=0;k<a.length;k++)g=a[k],h=g.indexOf("="),0<h&&(l=b(g.substring(0,h)),w(c[l])&&(c[l]=b(g.substring(h+1))));
return c}}function bg(){this.$get=Pd}function ed(a){function b(d,c){if(E(d)){var e={};p(d,function(a,c){e[c]=b(c,a)});return e}return a.factory(d+"Filter",c)}this.register=b;this.$get=["$injector",function(a){return function(b){return a.get(b+"Filter")}}];b("currency",Qd);b("date",Rd);b("filter",Hg);b("json",Ig);b("limitTo",Jg);b("lowercase",Kg);b("number",Sd);b("orderBy",Td);b("uppercase",Lg)}function Hg(){return function(a,b,d,c){if(!xa(a)){if(null==a)return a;throw M("filter")("notarray",a);}c=
c||"$";var e;switch(Bc(b)){case "function":break;case "boolean":case "null":case "number":case "string":e=!0;case "object":b=Mg(b,d,c,e);break;default:return a}return Array.prototype.filter.call(a,b)}}function Mg(a,b,d,c){var e=E(a)&&d in a;!0===b?b=sa:A(b)||(b=function(a,b){if(w(a))return!1;if(null===a||null===b)return a===b;if(E(b)||E(a)&&!Zb(a))return!1;a=N(""+a);b=N(""+b);return-1!==a.indexOf(b)});return function(f){return e&&!E(f)?ga(f,a[d],b,d,!1):ga(f,a,b,d,c)}}function ga(a,b,d,c,e,f){var g=
Bc(a),k=Bc(b);if("string"===k&&"!"===b.charAt(0))return!ga(a,b.substring(1),d,c,e);if(I(a))return a.some(function(a){return ga(a,b,d,c,e)});switch(g){case "object":var h;if(e){for(h in a)if(h.charAt&&"$"!==h.charAt(0)&&ga(a[h],b,d,c,!0))return!0;return f?!1:ga(a,b,d,c,!1)}if("object"===k){for(h in b)if(f=b[h],!A(f)&&!w(f)&&(g=h===c,!ga(g?a:a[h],f,d,c,g,g)))return!1;return!0}return d(a,b);case "function":return!1;default:return d(a,b)}}function Bc(a){return null===a?"null":typeof a}function Qd(a){var b=
a.NUMBER_FORMATS;return function(a,c,e){w(c)&&(c=b.CURRENCY_SYM);w(e)&&(e=b.PATTERNS[1].maxFrac);return null==a?a:Ud(a,b.PATTERNS[1],b.GROUP_SEP,b.DECIMAL_SEP,e).replace(/\u00A4/g,c)}}function Sd(a){var b=a.NUMBER_FORMATS;return function(a,c){return null==a?a:Ud(a,b.PATTERNS[0],b.GROUP_SEP,b.DECIMAL_SEP,c)}}function Ng(a){var b=0,d,c,e,f,g;-1<(c=a.indexOf(Vd))&&(a=a.replace(Vd,""));0<(e=a.search(/e/i))?(0>c&&(c=e),c+=+a.slice(e+1),a=a.substring(0,e)):0>c&&(c=a.length);for(e=0;a.charAt(e)===Cc;e++);
if(e===(g=a.length))d=[0],c=1;else{for(g--;a.charAt(g)===Cc;)g--;c-=e;d=[];for(f=0;e<=g;e++,f++)d[f]=+a.charAt(e)}c>Wd&&(d=d.splice(0,Wd-1),b=c-1,c=1);return{d:d,e:b,i:c}}function Og(a,b,d,c){var e=a.d,f=e.length-a.i;b=w(b)?Math.min(Math.max(d,f),c):+b;d=b+a.i;c=e[d];if(0<d){e.splice(Math.max(a.i,d));for(var g=d;g<e.length;g++)e[g]=0}else for(f=Math.max(0,f),a.i=1,e.length=Math.max(1,d=b+1),e[0]=0,g=1;g<d;g++)e[g]=0;if(5<=c)if(0>d-1){for(c=0;c>d;c--)e.unshift(0),a.i++;e.unshift(1);a.i++}else e[d-
1]++;for(;f<Math.max(0,b);f++)e.push(0);if(b=e.reduceRight(function(a,b,c,d){b+=a;d[c]=b%10;return Math.floor(b/10)},0))e.unshift(b),a.i++}function Ud(a,b,d,c,e){if(!D(a)&&!Y(a)||isNaN(a))return"";var f=!isFinite(a),g=!1,k=Math.abs(a)+"",h="";if(f)h="\u221e";else{g=Ng(k);Og(g,e,b.minFrac,b.maxFrac);h=g.d;k=g.i;e=g.e;f=[];for(g=h.reduce(function(a,b){return a&&!b},!0);0>k;)h.unshift(0),k++;0<k?f=h.splice(k,h.length):(f=h,h=[0]);k=[];for(h.length>=b.lgSize&&k.unshift(h.splice(-b.lgSize,h.length).join(""));h.length>
b.gSize;)k.unshift(h.splice(-b.gSize,h.length).join(""));h.length&&k.unshift(h.join(""));h=k.join(d);f.length&&(h+=c+f.join(""));e&&(h+="e+"+e)}return 0>a&&!g?b.negPre+h+b.negSuf:b.posPre+h+b.posSuf}function Mb(a,b,d,c){var e="";if(0>a||c&&0>=a)c?a=-a+1:(a=-a,e="-");for(a=""+a;a.length<b;)a=Cc+a;d&&(a=a.substr(a.length-b));return e+a}function da(a,b,d,c,e){d=d||0;return function(f){f=f["get"+a]();if(0<d||f>-d)f+=d;0===f&&-12===d&&(f=12);return Mb(f,b,c,e)}}function ob(a,b,d){return function(c,e){var f=
c["get"+a](),g=wb((d?"STANDALONE":"")+(b?"SHORT":"")+a);return e[g][f]}}function Xd(a){var b=(new Date(a,0,1)).getDay();return new Date(a,0,(4>=b?5:12)-b)}function Yd(a){return function(b){var d=Xd(b.getFullYear());b=+new Date(b.getFullYear(),b.getMonth(),b.getDate()+(4-b.getDay()))-+d;b=1+Math.round(b/6048E5);return Mb(b,a)}}function Dc(a,b){return 0>=a.getFullYear()?b.ERAS[0]:b.ERAS[1]}function Rd(a){function b(a){var b;if(b=a.match(d)){a=new Date(0);var f=0,g=0,k=b[8]?a.setUTCFullYear:a.setFullYear,
h=b[8]?a.setUTCHours:a.setHours;b[9]&&(f=Z(b[9]+b[10]),g=Z(b[9]+b[11]));k.call(a,Z(b[1]),Z(b[2])-1,Z(b[3]));f=Z(b[4]||0)-f;g=Z(b[5]||0)-g;k=Z(b[6]||0);b=Math.round(1E3*parseFloat("0."+(b[7]||0)));h.call(a,f,g,k,b)}return a}var d=/^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;return function(c,d,f){var g="",k=[],h,l;d=d||"mediumDate";d=a.DATETIME_FORMATS[d]||d;D(c)&&(c=Pg.test(c)?Z(c):b(c));Y(c)&&(c=new Date(c));if(!ea(c)||!isFinite(c.getTime()))return c;
for(;d;)(l=Qg.exec(d))?(k=eb(k,l,1),d=k.pop()):(k.push(d),d=null);var m=c.getTimezoneOffset();f&&(m=Rc(f,m),c=bc(c,f,!0));p(k,function(b){h=Rg[b];g+=h?h(c,a.DATETIME_FORMATS,m):"''"===b?"'":b.replace(/(^'|'$)/g,"").replace(/''/g,"'")});return g}}function Ig(){return function(a,b){w(b)&&(b=2);return fb(a,b)}}function Jg(){return function(a,b,d){b=Infinity===Math.abs(Number(b))?Number(b):Z(b);if(T(b))return a;Y(a)&&(a=a.toString());if(!xa(a))return a;d=!d||isNaN(d)?0:Z(d);d=0>d?Math.max(0,a.length+
d):d;return 0<=b?Ec(a,d,d+b):0===d?Ec(a,b,a.length):Ec(a,Math.max(0,d+b),d)}}function Ec(a,b,d){return D(a)?a.slice(b,d):ya.call(a,b,d)}function Td(a){function b(b){return b.map(function(b){var c=1,d=bb;if(A(b))d=b;else if(D(b)){if("+"===b.charAt(0)||"-"===b.charAt(0))c="-"===b.charAt(0)?-1:1,b=b.substring(1);if(""!==b&&(d=a(b),d.constant))var e=d(),d=function(a){return a[e]}}return{get:d,descending:c}})}function d(a){switch(typeof a){case "number":case "boolean":case "string":return!0;default:return!1}}
function c(a,b){var c=0,d=a.type,h=b.type;if(d===h){var h=a.value,l=b.value;"string"===d?(h=h.toLowerCase(),l=l.toLowerCase()):"object"===d&&(E(h)&&(h=a.index),E(l)&&(l=b.index));h!==l&&(c=h<l?-1:1)}else c=d<h?-1:1;return c}return function(a,f,g,k){if(null==a)return a;if(!xa(a))throw M("orderBy")("notarray",a);I(f)||(f=[f]);0===f.length&&(f=["+"]);var h=b(f),l=g?-1:1,m=A(k)?k:c;a=Array.prototype.map.call(a,function(a,b){return{value:a,tieBreaker:{value:b,type:"number",index:b},predicateValues:h.map(function(c){var e=
c.get(a);c=typeof e;if(null===e)c="string",e="null";else if("object"===c)a:{if(A(e.valueOf)&&(e=e.valueOf(),d(e)))break a;Zb(e)&&(e=e.toString(),d(e))}return{value:e,type:c,index:b}})}});a.sort(function(a,b){for(var d=0,e=h.length;d<e;d++){var g=m(a.predicateValues[d],b.predicateValues[d]);if(g)return g*h[d].descending*l}return(m(a.tieBreaker,b.tieBreaker)||c(a.tieBreaker,b.tieBreaker))*l});return a=a.map(function(a){return a.value})}}function Qa(a){A(a)&&(a={link:a});a.restrict=a.restrict||"AC";
return ka(a)}function Nb(a,b,d,c,e){this.$$controls=[];this.$error={};this.$$success={};this.$pending=void 0;this.$name=e(b.name||b.ngForm||"")(d);this.$dirty=!1;this.$valid=this.$pristine=!0;this.$submitted=this.$invalid=!1;this.$$parentForm=Ob;this.$$element=a;this.$$animate=c;Zd(this)}function Zd(a){a.$$classCache={};a.$$classCache[$d]=!(a.$$classCache[pb]=a.$$element.hasClass(pb))}function ae(a){function b(a,b,c){c&&!a.$$classCache[b]?(a.$$animate.addClass(a.$$element,b),a.$$classCache[b]=!0):
!c&&a.$$classCache[b]&&(a.$$animate.removeClass(a.$$element,b),a.$$classCache[b]=!1)}function d(a,c,d){c=c?"-"+Vc(c,"-"):"";b(a,pb+c,!0===d);b(a,$d+c,!1===d)}var c=a.set,e=a.unset;a.clazz.prototype.$setValidity=function(a,g,k){w(g)?(this.$pending||(this.$pending={}),c(this.$pending,a,k)):(this.$pending&&e(this.$pending,a,k),be(this.$pending)&&(this.$pending=void 0));Na(g)?g?(e(this.$error,a,k),c(this.$$success,a,k)):(c(this.$error,a,k),e(this.$$success,a,k)):(e(this.$error,a,k),e(this.$$success,a,
k));this.$pending?(b(this,"ng-pending",!0),this.$valid=this.$invalid=void 0,d(this,"",null)):(b(this,"ng-pending",!1),this.$valid=be(this.$error),this.$invalid=!this.$valid,d(this,"",this.$valid));g=this.$pending&&this.$pending[a]?void 0:this.$error[a]?!1:this.$$success[a]?!0:null;d(this,a,g);this.$$parentForm.$setValidity(a,g,this)}}function be(a){if(a)for(var b in a)if(a.hasOwnProperty(b))return!1;return!0}function Fc(a){a.$formatters.push(function(b){return a.$isEmpty(b)?b:b.toString()})}function Wa(a,
b,d,c,e,f){var g=N(b[0].type);if(!e.android){var k=!1;b.on("compositionstart",function(){k=!0});b.on("compositionend",function(){k=!1;l()})}var h,l=function(a){h&&(f.defer.cancel(h),h=null);if(!k){var e=b.val();a=a&&a.type;"password"===g||d.ngTrim&&"false"===d.ngTrim||(e=Q(e));(c.$viewValue!==e||""===e&&c.$$hasNativeValidators)&&c.$setViewValue(e,a)}};if(e.hasEvent("input"))b.on("input",l);else{var m=function(a,b,c){h||(h=f.defer(function(){h=null;b&&b.value===c||l(a)}))};b.on("keydown",function(a){var b=
a.keyCode;91===b||15<b&&19>b||37<=b&&40>=b||m(a,this,this.value)});if(e.hasEvent("paste"))b.on("paste cut",m)}b.on("change",l);if(ce[g]&&c.$$hasNativeValidators&&g===d.type)b.on("keydown wheel mousedown",function(a){if(!h){var b=this.validity,c=b.badInput,d=b.typeMismatch;h=f.defer(function(){h=null;b.badInput===c&&b.typeMismatch===d||l(a)})}});c.$render=function(){var a=c.$isEmpty(c.$viewValue)?"":c.$viewValue;b.val()!==a&&b.val(a)}}function Pb(a,b){return function(d,c){var e,f;if(ea(d))return d;
if(D(d)){'"'===d.charAt(0)&&'"'===d.charAt(d.length-1)&&(d=d.substring(1,d.length-1));if(Sg.test(d))return new Date(d);a.lastIndex=0;if(e=a.exec(d))return e.shift(),f=c?{yyyy:c.getFullYear(),MM:c.getMonth()+1,dd:c.getDate(),HH:c.getHours(),mm:c.getMinutes(),ss:c.getSeconds(),sss:c.getMilliseconds()/1E3}:{yyyy:1970,MM:1,dd:1,HH:0,mm:0,ss:0,sss:0},p(e,function(a,c){c<b.length&&(f[b[c]]=+a)}),new Date(f.yyyy,f.MM-1,f.dd,f.HH,f.mm,f.ss||0,1E3*f.sss||0)}return NaN}}function qb(a,b,d,c){return function(e,
f,g,k,h,l,m){function n(a){return a&&!(a.getTime&&a.getTime()!==a.getTime())}function q(a){return t(a)&&!ea(a)?d(a)||void 0:a}Gc(e,f,g,k);Wa(e,f,g,k,h,l);var p=k&&k.$options.getOption("timezone"),r;k.$$parserName=a;k.$parsers.push(function(a){if(k.$isEmpty(a))return null;if(b.test(a))return a=d(a,r),p&&(a=bc(a,p)),a});k.$formatters.push(function(a){if(a&&!ea(a))throw rb("datefmt",a);if(n(a))return(r=a)&&p&&(r=bc(r,p,!0)),m("date")(a,c,p);r=null;return""});if(t(g.min)||g.ngMin){var z;k.$validators.min=
function(a){return!n(a)||w(z)||d(a)>=z};g.$observe("min",function(a){z=q(a);k.$validate()})}if(t(g.max)||g.ngMax){var v;k.$validators.max=function(a){return!n(a)||w(v)||d(a)<=v};g.$observe("max",function(a){v=q(a);k.$validate()})}}}function Gc(a,b,d,c){(c.$$hasNativeValidators=E(b[0].validity))&&c.$parsers.push(function(a){var c=b.prop("validity")||{};return c.badInput||c.typeMismatch?void 0:a})}function de(a){a.$$parserName="number";a.$parsers.push(function(b){if(a.$isEmpty(b))return null;if(Tg.test(b))return parseFloat(b)});
a.$formatters.push(function(b){if(!a.$isEmpty(b)){if(!Y(b))throw rb("numfmt",b);b=b.toString()}return b})}function Xa(a){t(a)&&!Y(a)&&(a=parseFloat(a));return T(a)?void 0:a}function Hc(a){var b=a.toString(),d=b.indexOf(".");return-1===d?-1<a&&1>a&&(a=/e-(\d+)$/.exec(b))?Number(a[1]):0:b.length-d-1}function ee(a,b,d){a=Number(a);var c=(a|0)!==a,e=(b|0)!==b,f=(d|0)!==d;if(c||e||f){var g=c?Hc(a):0,k=e?Hc(b):0,h=f?Hc(d):0,g=Math.max(g,k,h),g=Math.pow(10,g);a*=g;b*=g;d*=g;c&&(a=Math.round(a));e&&(b=Math.round(b));
f&&(d=Math.round(d))}return 0===(a-b)%d}function fe(a,b,d,c,e){if(t(c)){a=a(c);if(!a.constant)throw rb("constexpr",d,c);return a(b)}return e}function Ic(a,b){function d(a,b){if(!a||!a.length)return[];if(!b||!b.length)return a;var c=[],d=0;a:for(;d<a.length;d++){for(var e=a[d],f=0;f<b.length;f++)if(e===b[f])continue a;c.push(e)}return c}function c(a){var b=a;I(a)?b=a.map(c).join(" "):E(a)&&(b=Object.keys(a).filter(function(b){return a[b]}).join(" "));return b}function e(a){var b=a;if(I(a))b=a.map(e);
else if(E(a)){var c=!1,b=Object.keys(a).filter(function(b){b=a[b];!c&&w(b)&&(c=!0);return b});c&&b.push(void 0)}return b}a="ngClass"+a;var f;return["$parse",function(g){return{restrict:"AC",link:function(k,h,l){function m(a,b){var c=[];p(a,function(a){if(0<b||s[a])s[a]=(s[a]||0)+b,s[a]===+(0<b)&&c.push(a)});return c.join(" ")}function n(a){if(a===b){var c=w,c=m(c&&c.split(" "),1);l.$addClass(c)}else c=w,c=m(c&&c.split(" "),-1),l.$removeClass(c);u=a}function q(a){a=c(a);a!==w&&r(a)}function r(a){if(u===
b){var c=w&&w.split(" "),e=a&&a.split(" "),g=d(c,e),c=d(e,c),g=m(g,-1),c=m(c,1);l.$addClass(c);l.$removeClass(g)}w=a}var t=l[a].trim(),z=":"===t.charAt(0)&&":"===t.charAt(1),t=g(t,z?e:c),v=z?q:r,s=h.data("$classCounts"),u=!0,w;s||(s=S(),h.data("$classCounts",s));"ngClass"!==a&&(f||(f=g("$index",function(a){return a&1})),k.$watch(f,n));k.$watch(t,v,z)}}}]}function Qb(a,b,d,c,e,f,g,k,h){this.$modelValue=this.$viewValue=Number.NaN;this.$$rawModelValue=void 0;this.$validators={};this.$asyncValidators=
{};this.$parsers=[];this.$formatters=[];this.$viewChangeListeners=[];this.$untouched=!0;this.$touched=!1;this.$pristine=!0;this.$dirty=!1;this.$valid=!0;this.$invalid=!1;this.$error={};this.$$success={};this.$pending=void 0;this.$name=h(d.name||"",!1)(a);this.$$parentForm=Ob;this.$options=Rb;this.$$parsedNgModel=e(d.ngModel);this.$$parsedNgModelAssign=this.$$parsedNgModel.assign;this.$$ngModelGet=this.$$parsedNgModel;this.$$ngModelSet=this.$$parsedNgModelAssign;this.$$pendingDebounce=null;this.$$parserValid=
void 0;this.$$currentValidationRunId=0;Object.defineProperty(this,"$$scope",{value:a});this.$$attr=d;this.$$element=c;this.$$animate=f;this.$$timeout=g;this.$$parse=e;this.$$q=k;this.$$exceptionHandler=b;Zd(this);Ug(this)}function Ug(a){a.$$scope.$watch(function(b){b=a.$$ngModelGet(b);if(b!==a.$modelValue&&(a.$modelValue===a.$modelValue||b===b)){a.$modelValue=a.$$rawModelValue=b;a.$$parserValid=void 0;for(var d=a.$formatters,c=d.length,e=b;c--;)e=d[c](e);a.$viewValue!==e&&(a.$$updateEmptyClasses(e),
a.$viewValue=a.$$lastCommittedViewValue=e,a.$render(),a.$$runValidators(a.$modelValue,a.$viewValue,C))}return b})}function Jc(a){this.$$options=a}function ge(a,b){p(b,function(b,c){t(a[c])||(a[c]=b)})}function Ga(a,b){a.prop("selected",b);a.attr("selected",b)}var Lc={objectMaxDepth:5},Vg=/^\/(.+)\/([a-z]*)$/,ra=Object.prototype.hasOwnProperty,N=function(a){return D(a)?a.toLowerCase():a},wb=function(a){return D(a)?a.toUpperCase():a},Ca,B,la,ya=[].slice,ug=[].splice,Wg=[].push,ha=Object.prototype.toString,
Oc=Object.getPrototypeOf,qa=M("ng"),$=u.angular||(u.angular={}),ec,sb=0;Ca=u.document.documentMode;var T=Number.isNaN||function(a){return a!==a};C.$inject=[];bb.$inject=[];var I=Array.isArray,se=/^\[object (?:Uint8|Uint8Clamped|Uint16|Uint32|Int8|Int16|Int32|Float32|Float64)Array]$/,Q=function(a){return D(a)?a.trim():a},Md=function(a){return a.replace(/([-()[\]{}+?*.$^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08")},Ja=function(){if(!t(Ja.rules)){var a=u.document.querySelector("[ng-csp]")||u.document.querySelector("[data-ng-csp]");
if(a){var b=a.getAttribute("ng-csp")||a.getAttribute("data-ng-csp");Ja.rules={noUnsafeEval:!b||-1!==b.indexOf("no-unsafe-eval"),noInlineStyle:!b||-1!==b.indexOf("no-inline-style")}}else{a=Ja;try{new Function(""),b=!1}catch(d){b=!0}a.rules={noUnsafeEval:b,noInlineStyle:!1}}}return Ja.rules},tb=function(){if(t(tb.name_))return tb.name_;var a,b,d=Ha.length,c,e;for(b=0;b<d;++b)if(c=Ha[b],a=u.document.querySelector("["+c.replace(":","\\:")+"jq]")){e=a.getAttribute(c+"jq");break}return tb.name_=e},ue=/:/g,
Ha=["ng-","data-ng-","ng:","x-ng-"],xe=function(a){var b=a.currentScript;if(!b)return!0;if(!(b instanceof u.HTMLScriptElement||b instanceof u.SVGScriptElement))return!1;b=b.attributes;return[b.getNamedItem("src"),b.getNamedItem("href"),b.getNamedItem("xlink:href")].every(function(b){if(!b)return!0;if(!b.value)return!1;var c=a.createElement("a");c.href=b.value;if(a.location.origin===c.origin)return!0;switch(c.protocol){case "http:":case "https:":case "ftp:":case "blob:":case "file:":case "data:":return!0;
default:return!1}})}(u.document),Ae=/[A-Z]/g,Wc=!1,Oa=3,Fe={full:"1.6.6",major:1,minor:6,dot:6,codeName:"interdimensional-cable"};U.expando="ng339";var kb=U.cache={},gg=1;U._data=function(a){return this.cache[a[this.expando]]||{}};var cg=/-([a-z])/g,Xg=/^-ms-/,Bb={mouseleave:"mouseout",mouseenter:"mouseover"},hc=M("jqLite"),fg=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,gc=/<|&#?\w+;/,dg=/<([\w:-]+)/,eg=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,aa={option:[1,'<select multiple="multiple">',
"</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};aa.optgroup=aa.option;aa.tbody=aa.tfoot=aa.colgroup=aa.caption=aa.thead;aa.th=aa.td;var lg=u.Node.prototype.contains||function(a){return!!(this.compareDocumentPosition(a)&16)},Sa=U.prototype={ready:gd,toString:function(){var a=[];p(this,function(b){a.push(""+b)});return"["+a.join(", ")+"]"},
eq:function(a){return 0<=a?B(this[a]):B(this[this.length+a])},length:0,push:Wg,sort:[].sort,splice:[].splice},Hb={};p("multiple selected checked disabled readOnly required open".split(" "),function(a){Hb[N(a)]=a});var ld={};p("input select option textarea button form details".split(" "),function(a){ld[a]=!0});var sd={ngMinlength:"minlength",ngMaxlength:"maxlength",ngMin:"min",ngMax:"max",ngPattern:"pattern",ngStep:"step"};p({data:lc,removeData:kc,hasData:function(a){for(var b in kb[a.ng339])return!0;
return!1},cleanData:function(a){for(var b=0,d=a.length;b<d;b++)kc(a[b])}},function(a,b){U[b]=a});p({data:lc,inheritedData:Fb,scope:function(a){return B.data(a,"$scope")||Fb(a.parentNode||a,["$isolateScope","$scope"])},isolateScope:function(a){return B.data(a,"$isolateScope")||B.data(a,"$isolateScopeNoTemplate")},controller:id,injector:function(a){return Fb(a,"$injector")},removeAttr:function(a,b){a.removeAttribute(b)},hasClass:Cb,css:function(a,b,d){b=yb(b.replace(Xg,"ms-"));if(t(d))a.style[b]=d;
else return a.style[b]},attr:function(a,b,d){var c=a.nodeType;if(c!==Oa&&2!==c&&8!==c&&a.getAttribute){var c=N(b),e=Hb[c];if(t(d))null===d||!1===d&&e?a.removeAttribute(b):a.setAttribute(b,e?c:d);else return a=a.getAttribute(b),e&&null!==a&&(a=c),null===a?void 0:a}},prop:function(a,b,d){if(t(d))a[b]=d;else return a[b]},text:function(){function a(a,d){if(w(d)){var c=a.nodeType;return 1===c||c===Oa?a.textContent:""}a.textContent=d}a.$dv="";return a}(),val:function(a,b){if(w(b)){if(a.multiple&&"select"===
za(a)){var d=[];p(a.options,function(a){a.selected&&d.push(a.value||a.text)});return d}return a.value}a.value=b},html:function(a,b){if(w(b))return a.innerHTML;zb(a,!0);a.innerHTML=b},empty:jd},function(a,b){U.prototype[b]=function(b,c){var e,f,g=this.length;if(a!==jd&&w(2===a.length&&a!==Cb&&a!==id?b:c)){if(E(b)){for(e=0;e<g;e++)if(a===lc)a(this[e],b);else for(f in b)a(this[e],f,b[f]);return this}e=a.$dv;g=w(e)?Math.min(g,1):g;for(f=0;f<g;f++){var k=a(this[f],b,c);e=e?e+k:k}return e}for(e=0;e<g;e++)a(this[e],
b,c);return this}});p({removeData:kc,on:function(a,b,d,c){if(t(c))throw hc("onargs");if(fc(a)){c=Ab(a,!0);var e=c.events,f=c.handle;f||(f=c.handle=ig(a,e));c=0<=b.indexOf(" ")?b.split(" "):[b];for(var g=c.length,k=function(b,c,g){var k=e[b];k||(k=e[b]=[],k.specialHandlerWrapper=c,"$destroy"===b||g||a.addEventListener(b,f));k.push(d)};g--;)b=c[g],Bb[b]?(k(Bb[b],kg),k(b,void 0,!0)):k(b)}},off:hd,one:function(a,b,d){a=B(a);a.on(b,function e(){a.off(b,d);a.off(b,e)});a.on(b,d)},replaceWith:function(a,
b){var d,c=a.parentNode;zb(a);p(new U(b),function(b){d?c.insertBefore(b,d.nextSibling):c.replaceChild(b,a);d=b})},children:function(a){var b=[];p(a.childNodes,function(a){1===a.nodeType&&b.push(a)});return b},contents:function(a){return a.contentDocument||a.childNodes||[]},append:function(a,b){var d=a.nodeType;if(1===d||11===d){b=new U(b);for(var d=0,c=b.length;d<c;d++)a.appendChild(b[d])}},prepend:function(a,b){if(1===a.nodeType){var d=a.firstChild;p(new U(b),function(b){a.insertBefore(b,d)})}},
wrap:function(a,b){var d=B(b).eq(0).clone()[0],c=a.parentNode;c&&c.replaceChild(d,a);d.appendChild(a)},remove:Gb,detach:function(a){Gb(a,!0)},after:function(a,b){var d=a,c=a.parentNode;if(c){b=new U(b);for(var e=0,f=b.length;e<f;e++){var g=b[e];c.insertBefore(g,d.nextSibling);d=g}}},addClass:Eb,removeClass:Db,toggleClass:function(a,b,d){b&&p(b.split(" "),function(b){var e=d;w(e)&&(e=!Cb(a,b));(e?Eb:Db)(a,b)})},parent:function(a){return(a=a.parentNode)&&11!==a.nodeType?a:null},next:function(a){return a.nextElementSibling},
find:function(a,b){return a.getElementsByTagName?a.getElementsByTagName(b):[]},clone:jc,triggerHandler:function(a,b,d){var c,e,f=b.type||b,g=Ab(a);if(g=(g=g&&g.events)&&g[f])c={preventDefault:function(){this.defaultPrevented=!0},isDefaultPrevented:function(){return!0===this.defaultPrevented},stopImmediatePropagation:function(){this.immediatePropagationStopped=!0},isImmediatePropagationStopped:function(){return!0===this.immediatePropagationStopped},stopPropagation:C,type:f,target:a},b.type&&(c=P(c,
b)),b=ja(g),e=d?[c].concat(d):[c],p(b,function(b){c.isImmediatePropagationStopped()||b.apply(a,e)})}},function(a,b){U.prototype[b]=function(b,c,e){for(var f,g=0,k=this.length;g<k;g++)w(f)?(f=a(this[g],b,c,e),t(f)&&(f=B(f))):ic(f,a(this[g],b,c,e));return t(f)?f:this}});U.prototype.bind=U.prototype.on;U.prototype.unbind=U.prototype.off;var Yg=Object.create(null);md.prototype={_idx:function(a){if(a===this._lastKey)return this._lastIndex;this._lastKey=a;return this._lastIndex=this._keys.indexOf(a)},_transformKey:function(a){return T(a)?
Yg:a},get:function(a){a=this._transformKey(a);a=this._idx(a);if(-1!==a)return this._values[a]},set:function(a,b){a=this._transformKey(a);var d=this._idx(a);-1===d&&(d=this._lastIndex=this._keys.length);this._keys[d]=a;this._values[d]=b},delete:function(a){a=this._transformKey(a);a=this._idx(a);if(-1===a)return!1;this._keys.splice(a,1);this._values.splice(a,1);this._lastKey=NaN;this._lastIndex=-1;return!0}};var Ib=md,ag=[function(){this.$get=[function(){return Ib}]}],ng=/^([^(]+?)=>/,og=/^[^(]*\(\s*([^)]*)\)/m,
Zg=/,/,$g=/^\s*(_?)(\S+?)\1\s*$/,mg=/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg,Ba=M("$injector");hb.$$annotate=function(a,b,d){var c;if("function"===typeof a){if(!(c=a.$inject)){c=[];if(a.length){if(b)throw D(d)&&d||(d=a.name||pg(a)),Ba("strictdi",d);b=nd(a);p(b[1].split(Zg),function(a){a.replace($g,function(a,b,d){c.push(d)})})}a.$inject=c}}else I(a)?(b=a.length-1,ub(a[b],"fn"),c=a.slice(0,b)):ub(a,"fn",!0);return c};var he=M("$animate"),sf=function(){this.$get=C},tf=function(){var a=new Ib,b=[];this.$get=
["$$AnimateRunner","$rootScope",function(d,c){function e(a,b,c){var d=!1;b&&(b=D(b)?b.split(" "):I(b)?b:[],p(b,function(b){b&&(d=!0,a[b]=c)}));return d}function f(){p(b,function(b){var c=a.get(b);if(c){var d=qg(b.attr("class")),e="",f="";p(c,function(a,b){a!==!!d[b]&&(a?e+=(e.length?" ":"")+b:f+=(f.length?" ":"")+b)});p(b,function(a){e&&Eb(a,e);f&&Db(a,f)});a.delete(b)}});b.length=0}return{enabled:C,on:C,off:C,pin:C,push:function(g,k,h,l){l&&l();h=h||{};h.from&&g.css(h.from);h.to&&g.css(h.to);if(h.addClass||
h.removeClass)if(k=h.addClass,l=h.removeClass,h=a.get(g)||{},k=e(h,k,!0),l=e(h,l,!1),k||l)a.set(g,h),b.push(g),1===b.length&&c.$$postDigest(f);g=new d;g.complete();return g}}}]},qf=["$provide",function(a){var b=this,d=null,c=null;this.$$registeredAnimations=Object.create(null);this.register=function(c,d){if(c&&"."!==c.charAt(0))throw he("notcsel",c);var g=c+"-animation";b.$$registeredAnimations[c.substr(1)]=g;a.factory(g,d)};this.customFilter=function(a){1===arguments.length&&(c=A(a)?a:null);return c};
this.classNameFilter=function(a){if(1===arguments.length&&(d=a instanceof RegExp?a:null)&&/[(\s|\/)]ng-animate[(\s|\/)]/.test(d.toString()))throw d=null,he("nongcls","ng-animate");return d};this.$get=["$$animateQueue",function(a){function b(a,c,d){if(d){var e;a:{for(e=0;e<d.length;e++){var f=d[e];if(1===f.nodeType){e=f;break a}}e=void 0}!e||e.parentNode||e.previousElementSibling||(d=null)}d?d.after(a):c.prepend(a)}return{on:a.on,off:a.off,pin:a.pin,enabled:a.enabled,cancel:function(a){a.end&&a.end()},
enter:function(c,d,h,l){d=d&&B(d);h=h&&B(h);d=d||h.parent();b(c,d,h);return a.push(c,"enter",Ka(l))},move:function(c,d,h,l){d=d&&B(d);h=h&&B(h);d=d||h.parent();b(c,d,h);return a.push(c,"move",Ka(l))},leave:function(b,c){return a.push(b,"leave",Ka(c),function(){b.remove()})},addClass:function(b,c,d){d=Ka(d);d.addClass=lb(d.addclass,c);return a.push(b,"addClass",d)},removeClass:function(b,c,d){d=Ka(d);d.removeClass=lb(d.removeClass,c);return a.push(b,"removeClass",d)},setClass:function(b,c,d,f){f=Ka(f);
f.addClass=lb(f.addClass,c);f.removeClass=lb(f.removeClass,d);return a.push(b,"setClass",f)},animate:function(b,c,d,f,m){m=Ka(m);m.from=m.from?P(m.from,c):c;m.to=m.to?P(m.to,d):d;m.tempClasses=lb(m.tempClasses,f||"ng-inline-animate");return a.push(b,"animate",m)}}}]}],vf=function(){this.$get=["$$rAF",function(a){function b(b){d.push(b);1<d.length||a(function(){for(var a=0;a<d.length;a++)d[a]();d=[]})}var d=[];return function(){var a=!1;b(function(){a=!0});return function(d){a?d():b(d)}}}]},uf=function(){this.$get=
["$q","$sniffer","$$animateAsyncRun","$$isDocumentHidden","$timeout",function(a,b,d,c,e){function f(a){this.setHost(a);var b=d();this._doneCallbacks=[];this._tick=function(a){c()?e(a,0,!1):b(a)};this._state=0}f.chain=function(a,b){function c(){if(d===a.length)b(!0);else a[d](function(a){!1===a?b(!1):(d++,c())})}var d=0;c()};f.all=function(a,b){function c(f){e=e&&f;++d===a.length&&b(e)}var d=0,e=!0;p(a,function(a){a.done(c)})};f.prototype={setHost:function(a){this.host=a||{}},done:function(a){2===
this._state?a():this._doneCallbacks.push(a)},progress:C,getPromise:function(){if(!this.promise){var b=this;this.promise=a(function(a,c){b.done(function(b){!1===b?c():a()})})}return this.promise},then:function(a,b){return this.getPromise().then(a,b)},"catch":function(a){return this.getPromise()["catch"](a)},"finally":function(a){return this.getPromise()["finally"](a)},pause:function(){this.host.pause&&this.host.pause()},resume:function(){this.host.resume&&this.host.resume()},end:function(){this.host.end&&
this.host.end();this._resolve(!0)},cancel:function(){this.host.cancel&&this.host.cancel();this._resolve(!1)},complete:function(a){var b=this;0===b._state&&(b._state=1,b._tick(function(){b._resolve(a)}))},_resolve:function(a){2!==this._state&&(p(this._doneCallbacks,function(b){b(a)}),this._doneCallbacks.length=0,this._state=2)}};return f}]},rf=function(){this.$get=["$$rAF","$q","$$AnimateRunner",function(a,b,d){return function(b,e){function f(){a(function(){g.addClass&&(b.addClass(g.addClass),g.addClass=
null);g.removeClass&&(b.removeClass(g.removeClass),g.removeClass=null);g.to&&(b.css(g.to),g.to=null);k||h.complete();k=!0});return h}var g=e||{};g.$$prepared||(g=pa(g));g.cleanupStyles&&(g.from=g.to=null);g.from&&(b.css(g.from),g.from=null);var k,h=new d;return{start:f,end:f}}}]},ba=M("$compile"),oc=new function(){};Yc.$inject=["$provide","$$sanitizeUriProvider"];Kb.prototype.isFirstChange=function(){return this.previousValue===oc};var od=/^((?:x|data)[:\-_])/i,tg=/[:\-_]+(.)/g,ud=M("$controller"),
td=/^(\S+)(\s+as\s+([\w$]+))?$/,Cf=function(){this.$get=["$document",function(a){return function(b){b?!b.nodeType&&b instanceof B&&(b=b[0]):b=a[0].body;return b.offsetWidth+1}}]},vd="application/json",sc={"Content-Type":vd+";charset=utf-8"},wg=/^\[|^\{(?!\{)/,xg={"[":/]$/,"{":/}$/},vg=/^\)]\}',?\n/,rc=M("$http"),Fa=$.$interpolateMinErr=M("$interpolate");Fa.throwNoconcat=function(a){throw Fa("noconcat",a);};Fa.interr=function(a,b){return Fa("interr",a,b.toString())};var Kf=function(){this.$get=function(){function a(a){var b=
function(a){b.data=a;b.called=!0};b.id=a;return b}var b=$.callbacks,d={};return{createCallback:function(c){c="_"+(b.$$counter++).toString(36);var e="angular.callbacks."+c,f=a(c);d[e]=b[c]=f;return e},wasCalled:function(a){return d[a].called},getResponse:function(a){return d[a].data},removeCallback:function(a){delete b[d[a].id];delete d[a]}}}},ah=/^([^?#]*)(\?([^#]*))?(#(.*))?$/,zg={http:80,https:443,ftp:21},mb=M("$location"),Ag=/^\s*[\\/]{2,}/,bh={$$absUrl:"",$$html5:!1,$$replace:!1,absUrl:Lb("$$absUrl"),
url:function(a){if(w(a))return this.$$url;var b=ah.exec(a);(b[1]||""===a)&&this.path(decodeURIComponent(b[1]));(b[2]||b[1]||""===a)&&this.search(b[3]||"");this.hash(b[5]||"");return this},protocol:Lb("$$protocol"),host:Lb("$$host"),port:Lb("$$port"),path:Dd("$$path",function(a){a=null!==a?a.toString():"";return"/"===a.charAt(0)?a:"/"+a}),search:function(a,b){switch(arguments.length){case 0:return this.$$search;case 1:if(D(a)||Y(a))a=a.toString(),this.$$search=Tc(a);else if(E(a))a=pa(a,{}),p(a,function(b,
c){null==b&&delete a[c]}),this.$$search=a;else throw mb("isrcharg");break;default:w(b)||null===b?delete this.$$search[a]:this.$$search[a]=b}this.$$compose();return this},hash:Dd("$$hash",function(a){return null!==a?a.toString():""}),replace:function(){this.$$replace=!0;return this}};p([Cd,wc,vc],function(a){a.prototype=Object.create(bh);a.prototype.state=function(b){if(!arguments.length)return this.$$state;if(a!==vc||!this.$$html5)throw mb("nostate");this.$$state=w(b)?null:b;this.$$urlUpdatedByLocation=
!0;return this}});var Ya=M("$parse"),Eg={}.constructor.prototype.valueOf,Sb=S();p("+ - * / % === !== == != < > <= >= && || ! = |".split(" "),function(a){Sb[a]=!0});var ch={n:"\n",f:"\f",r:"\r",t:"\t",v:"\v","'":"'",'"':'"'},zc=function(a){this.options=a};zc.prototype={constructor:zc,lex:function(a){this.text=a;this.index=0;for(this.tokens=[];this.index<this.text.length;)if(a=this.text.charAt(this.index),'"'===a||"'"===a)this.readString(a);else if(this.isNumber(a)||"."===a&&this.isNumber(this.peek()))this.readNumber();
else if(this.isIdentifierStart(this.peekMultichar()))this.readIdent();else if(this.is(a,"(){}[].,;:?"))this.tokens.push({index:this.index,text:a}),this.index++;else if(this.isWhitespace(a))this.index++;else{var b=a+this.peek(),d=b+this.peek(2),c=Sb[b],e=Sb[d];Sb[a]||c||e?(a=e?d:c?b:a,this.tokens.push({index:this.index,text:a,operator:!0}),this.index+=a.length):this.throwError("Unexpected next character ",this.index,this.index+1)}return this.tokens},is:function(a,b){return-1!==b.indexOf(a)},peek:function(a){a=
a||1;return this.index+a<this.text.length?this.text.charAt(this.index+a):!1},isNumber:function(a){return"0"<=a&&"9">=a&&"string"===typeof a},isWhitespace:function(a){return" "===a||"\r"===a||"\t"===a||"\n"===a||"\v"===a||"\u00a0"===a},isIdentifierStart:function(a){return this.options.isIdentifierStart?this.options.isIdentifierStart(a,this.codePointAt(a)):this.isValidIdentifierStart(a)},isValidIdentifierStart:function(a){return"a"<=a&&"z">=a||"A"<=a&&"Z">=a||"_"===a||"$"===a},isIdentifierContinue:function(a){return this.options.isIdentifierContinue?
this.options.isIdentifierContinue(a,this.codePointAt(a)):this.isValidIdentifierContinue(a)},isValidIdentifierContinue:function(a,b){return this.isValidIdentifierStart(a,b)||this.isNumber(a)},codePointAt:function(a){return 1===a.length?a.charCodeAt(0):(a.charCodeAt(0)<<10)+a.charCodeAt(1)-56613888},peekMultichar:function(){var a=this.text.charAt(this.index),b=this.peek();if(!b)return a;var d=a.charCodeAt(0),c=b.charCodeAt(0);return 55296<=d&&56319>=d&&56320<=c&&57343>=c?a+b:a},isExpOperator:function(a){return"-"===
a||"+"===a||this.isNumber(a)},throwError:function(a,b,d){d=d||this.index;b=t(b)?"s "+b+"-"+this.index+" ["+this.text.substring(b,d)+"]":" "+d;throw Ya("lexerr",a,b,this.text);},readNumber:function(){for(var a="",b=this.index;this.index<this.text.length;){var d=N(this.text.charAt(this.index));if("."===d||this.isNumber(d))a+=d;else{var c=this.peek();if("e"===d&&this.isExpOperator(c))a+=d;else if(this.isExpOperator(d)&&c&&this.isNumber(c)&&"e"===a.charAt(a.length-1))a+=d;else if(!this.isExpOperator(d)||
c&&this.isNumber(c)||"e"!==a.charAt(a.length-1))break;else this.throwError("Invalid exponent")}this.index++}this.tokens.push({index:b,text:a,constant:!0,value:Number(a)})},readIdent:function(){var a=this.index;for(this.index+=this.peekMultichar().length;this.index<this.text.length;){var b=this.peekMultichar();if(!this.isIdentifierContinue(b))break;this.index+=b.length}this.tokens.push({index:a,text:this.text.slice(a,this.index),identifier:!0})},readString:function(a){var b=this.index;this.index++;
for(var d="",c=a,e=!1;this.index<this.text.length;){var f=this.text.charAt(this.index),c=c+f;if(e)"u"===f?(e=this.text.substring(this.index+1,this.index+5),e.match(/[\da-f]{4}/i)||this.throwError("Invalid unicode escape [\\u"+e+"]"),this.index+=4,d+=String.fromCharCode(parseInt(e,16))):d+=ch[f]||f,e=!1;else if("\\"===f)e=!0;else{if(f===a){this.index++;this.tokens.push({index:b,text:c,constant:!0,value:d});return}d+=f}this.index++}this.throwError("Unterminated quote",b)}};var r=function(a,b){this.lexer=
a;this.options=b};r.Program="Program";r.ExpressionStatement="ExpressionStatement";r.AssignmentExpression="AssignmentExpression";r.ConditionalExpression="ConditionalExpression";r.LogicalExpression="LogicalExpression";r.BinaryExpression="BinaryExpression";r.UnaryExpression="UnaryExpression";r.CallExpression="CallExpression";r.MemberExpression="MemberExpression";r.Identifier="Identifier";r.Literal="Literal";r.ArrayExpression="ArrayExpression";r.Property="Property";r.ObjectExpression="ObjectExpression";
r.ThisExpression="ThisExpression";r.LocalsExpression="LocalsExpression";r.NGValueParameter="NGValueParameter";r.prototype={ast:function(a){this.text=a;this.tokens=this.lexer.lex(a);a=this.program();0!==this.tokens.length&&this.throwError("is an unexpected token",this.tokens[0]);return a},program:function(){for(var a=[];;)if(0<this.tokens.length&&!this.peek("}",")",";","]")&&a.push(this.expressionStatement()),!this.expect(";"))return{type:r.Program,body:a}},expressionStatement:function(){return{type:r.ExpressionStatement,
expression:this.filterChain()}},filterChain:function(){for(var a=this.expression();this.expect("|");)a=this.filter(a);return a},expression:function(){return this.assignment()},assignment:function(){var a=this.ternary();if(this.expect("=")){if(!Hd(a))throw Ya("lval");a={type:r.AssignmentExpression,left:a,right:this.assignment(),operator:"="}}return a},ternary:function(){var a=this.logicalOR(),b,d;return this.expect("?")&&(b=this.expression(),this.consume(":"))?(d=this.expression(),{type:r.ConditionalExpression,
test:a,alternate:b,consequent:d}):a},logicalOR:function(){for(var a=this.logicalAND();this.expect("||");)a={type:r.LogicalExpression,operator:"||",left:a,right:this.logicalAND()};return a},logicalAND:function(){for(var a=this.equality();this.expect("&&");)a={type:r.LogicalExpression,operator:"&&",left:a,right:this.equality()};return a},equality:function(){for(var a=this.relational(),b;b=this.expect("==","!=","===","!==");)a={type:r.BinaryExpression,operator:b.text,left:a,right:this.relational()};
return a},relational:function(){for(var a=this.additive(),b;b=this.expect("<",">","<=",">=");)a={type:r.BinaryExpression,operator:b.text,left:a,right:this.additive()};return a},additive:function(){for(var a=this.multiplicative(),b;b=this.expect("+","-");)a={type:r.BinaryExpression,operator:b.text,left:a,right:this.multiplicative()};return a},multiplicative:function(){for(var a=this.unary(),b;b=this.expect("*","/","%");)a={type:r.BinaryExpression,operator:b.text,left:a,right:this.unary()};return a},
unary:function(){var a;return(a=this.expect("+","-","!"))?{type:r.UnaryExpression,operator:a.text,prefix:!0,argument:this.unary()}:this.primary()},primary:function(){var a;this.expect("(")?(a=this.filterChain(),this.consume(")")):this.expect("[")?a=this.arrayDeclaration():this.expect("{")?a=this.object():this.selfReferential.hasOwnProperty(this.peek().text)?a=pa(this.selfReferential[this.consume().text]):this.options.literals.hasOwnProperty(this.peek().text)?a={type:r.Literal,value:this.options.literals[this.consume().text]}:
this.peek().identifier?a=this.identifier():this.peek().constant?a=this.constant():this.throwError("not a primary expression",this.peek());for(var b;b=this.expect("(","[",".");)"("===b.text?(a={type:r.CallExpression,callee:a,arguments:this.parseArguments()},this.consume(")")):"["===b.text?(a={type:r.MemberExpression,object:a,property:this.expression(),computed:!0},this.consume("]")):"."===b.text?a={type:r.MemberExpression,object:a,property:this.identifier(),computed:!1}:this.throwError("IMPOSSIBLE");
return a},filter:function(a){a=[a];for(var b={type:r.CallExpression,callee:this.identifier(),arguments:a,filter:!0};this.expect(":");)a.push(this.expression());return b},parseArguments:function(){var a=[];if(")"!==this.peekToken().text){do a.push(this.filterChain());while(this.expect(","))}return a},identifier:function(){var a=this.consume();a.identifier||this.throwError("is not a valid identifier",a);return{type:r.Identifier,name:a.text}},constant:function(){return{type:r.Literal,value:this.consume().value}},
arrayDeclaration:function(){var a=[];if("]"!==this.peekToken().text){do{if(this.peek("]"))break;a.push(this.expression())}while(this.expect(","))}this.consume("]");return{type:r.ArrayExpression,elements:a}},object:function(){var a=[],b;if("}"!==this.peekToken().text){do{if(this.peek("}"))break;b={type:r.Property,kind:"init"};this.peek().constant?(b.key=this.constant(),b.computed=!1,this.consume(":"),b.value=this.expression()):this.peek().identifier?(b.key=this.identifier(),b.computed=!1,this.peek(":")?
(this.consume(":"),b.value=this.expression()):b.value=b.key):this.peek("[")?(this.consume("["),b.key=this.expression(),this.consume("]"),b.computed=!0,this.consume(":"),b.value=this.expression()):this.throwError("invalid key",this.peek());a.push(b)}while(this.expect(","))}this.consume("}");return{type:r.ObjectExpression,properties:a}},throwError:function(a,b){throw Ya("syntax",b.text,a,b.index+1,this.text,this.text.substring(b.index));},consume:function(a){if(0===this.tokens.length)throw Ya("ueoe",
this.text);var b=this.expect(a);b||this.throwError("is unexpected, expecting ["+a+"]",this.peek());return b},peekToken:function(){if(0===this.tokens.length)throw Ya("ueoe",this.text);return this.tokens[0]},peek:function(a,b,d,c){return this.peekAhead(0,a,b,d,c)},peekAhead:function(a,b,d,c,e){if(this.tokens.length>a){a=this.tokens[a];var f=a.text;if(f===b||f===d||f===c||f===e||!(b||d||c||e))return a}return!1},expect:function(a,b,d,c){return(a=this.peek(a,b,d,c))?(this.tokens.shift(),a):!1},selfReferential:{"this":{type:r.ThisExpression},
$locals:{type:r.LocalsExpression}}};var Fd=2;Jd.prototype={compile:function(a){var b=this;this.state={nextId:0,filters:{},fn:{vars:[],body:[],own:{}},assign:{vars:[],body:[],own:{}},inputs:[]};V(a,b.$filter);var d="",c;this.stage="assign";if(c=Id(a))this.state.computing="assign",d=this.nextId(),this.recurse(c,d),this.return_(d),d="fn.assign="+this.generateFunction("assign","s,v,l");c=Gd(a.body);b.stage="inputs";p(c,function(a,c){var d="fn"+c;b.state[d]={vars:[],body:[],own:{}};b.state.computing=d;
var k=b.nextId();b.recurse(a,k);b.return_(k);b.state.inputs.push({name:d,isPure:a.isPure});a.watchId=c});this.state.computing="fn";this.stage="main";this.recurse(a);a='"'+this.USE+" "+this.STRICT+'";\n'+this.filterPrefix()+"var fn="+this.generateFunction("fn","s,l,a,i")+d+this.watchFns()+"return fn;";a=(new Function("$filter","getStringValue","ifDefined","plus",a))(this.$filter,Bg,Cg,Ed);this.state=this.stage=void 0;return a},USE:"use",STRICT:"strict",watchFns:function(){var a=[],b=this.state.inputs,
d=this;p(b,function(b){a.push("var "+b.name+"="+d.generateFunction(b.name,"s"));b.isPure&&a.push(b.name,".isPure="+JSON.stringify(b.isPure)+";")});b.length&&a.push("fn.inputs=["+b.map(function(a){return a.name}).join(",")+"];");return a.join("")},generateFunction:function(a,b){return"function("+b+"){"+this.varsPrefix(a)+this.body(a)+"};"},filterPrefix:function(){var a=[],b=this;p(this.state.filters,function(d,c){a.push(d+"=$filter("+b.escape(c)+")")});return a.length?"var "+a.join(",")+";":""},varsPrefix:function(a){return this.state[a].vars.length?
"var "+this.state[a].vars.join(",")+";":""},body:function(a){return this.state[a].body.join("")},recurse:function(a,b,d,c,e,f){var g,k,h=this,l,m,n;c=c||C;if(!f&&t(a.watchId))b=b||this.nextId(),this.if_("i",this.lazyAssign(b,this.computedMember("i",a.watchId)),this.lazyRecurse(a,b,d,c,e,!0));else switch(a.type){case r.Program:p(a.body,function(b,c){h.recurse(b.expression,void 0,void 0,function(a){k=a});c!==a.body.length-1?h.current().body.push(k,";"):h.return_(k)});break;case r.Literal:m=this.escape(a.value);
this.assign(b,m);c(b||m);break;case r.UnaryExpression:this.recurse(a.argument,void 0,void 0,function(a){k=a});m=a.operator+"("+this.ifDefined(k,0)+")";this.assign(b,m);c(m);break;case r.BinaryExpression:this.recurse(a.left,void 0,void 0,function(a){g=a});this.recurse(a.right,void 0,void 0,function(a){k=a});m="+"===a.operator?this.plus(g,k):"-"===a.operator?this.ifDefined(g,0)+a.operator+this.ifDefined(k,0):"("+g+")"+a.operator+"("+k+")";this.assign(b,m);c(m);break;case r.LogicalExpression:b=b||this.nextId();
h.recurse(a.left,b);h.if_("&&"===a.operator?b:h.not(b),h.lazyRecurse(a.right,b));c(b);break;case r.ConditionalExpression:b=b||this.nextId();h.recurse(a.test,b);h.if_(b,h.lazyRecurse(a.alternate,b),h.lazyRecurse(a.consequent,b));c(b);break;case r.Identifier:b=b||this.nextId();d&&(d.context="inputs"===h.stage?"s":this.assign(this.nextId(),this.getHasOwnProperty("l",a.name)+"?l:s"),d.computed=!1,d.name=a.name);h.if_("inputs"===h.stage||h.not(h.getHasOwnProperty("l",a.name)),function(){h.if_("inputs"===
h.stage||"s",function(){e&&1!==e&&h.if_(h.isNull(h.nonComputedMember("s",a.name)),h.lazyAssign(h.nonComputedMember("s",a.name),"{}"));h.assign(b,h.nonComputedMember("s",a.name))})},b&&h.lazyAssign(b,h.nonComputedMember("l",a.name)));c(b);break;case r.MemberExpression:g=d&&(d.context=this.nextId())||this.nextId();b=b||this.nextId();h.recurse(a.object,g,void 0,function(){h.if_(h.notNull(g),function(){a.computed?(k=h.nextId(),h.recurse(a.property,k),h.getStringValue(k),e&&1!==e&&h.if_(h.not(h.computedMember(g,
k)),h.lazyAssign(h.computedMember(g,k),"{}")),m=h.computedMember(g,k),h.assign(b,m),d&&(d.computed=!0,d.name=k)):(e&&1!==e&&h.if_(h.isNull(h.nonComputedMember(g,a.property.name)),h.lazyAssign(h.nonComputedMember(g,a.property.name),"{}")),m=h.nonComputedMember(g,a.property.name),h.assign(b,m),d&&(d.computed=!1,d.name=a.property.name))},function(){h.assign(b,"undefined")});c(b)},!!e);break;case r.CallExpression:b=b||this.nextId();a.filter?(k=h.filter(a.callee.name),l=[],p(a.arguments,function(a){var b=
h.nextId();h.recurse(a,b);l.push(b)}),m=k+"("+l.join(",")+")",h.assign(b,m),c(b)):(k=h.nextId(),g={},l=[],h.recurse(a.callee,k,g,function(){h.if_(h.notNull(k),function(){p(a.arguments,function(b){h.recurse(b,a.constant?void 0:h.nextId(),void 0,function(a){l.push(a)})});m=g.name?h.member(g.context,g.name,g.computed)+"("+l.join(",")+")":k+"("+l.join(",")+")";h.assign(b,m)},function(){h.assign(b,"undefined")});c(b)}));break;case r.AssignmentExpression:k=this.nextId();g={};this.recurse(a.left,void 0,
g,function(){h.if_(h.notNull(g.context),function(){h.recurse(a.right,k);m=h.member(g.context,g.name,g.computed)+a.operator+k;h.assign(b,m);c(b||m)})},1);break;case r.ArrayExpression:l=[];p(a.elements,function(b){h.recurse(b,a.constant?void 0:h.nextId(),void 0,function(a){l.push(a)})});m="["+l.join(",")+"]";this.assign(b,m);c(b||m);break;case r.ObjectExpression:l=[];n=!1;p(a.properties,function(a){a.computed&&(n=!0)});n?(b=b||this.nextId(),this.assign(b,"{}"),p(a.properties,function(a){a.computed?
(g=h.nextId(),h.recurse(a.key,g)):g=a.key.type===r.Identifier?a.key.name:""+a.key.value;k=h.nextId();h.recurse(a.value,k);h.assign(h.member(b,g,a.computed),k)})):(p(a.properties,function(b){h.recurse(b.value,a.constant?void 0:h.nextId(),void 0,function(a){l.push(h.escape(b.key.type===r.Identifier?b.key.name:""+b.key.value)+":"+a)})}),m="{"+l.join(",")+"}",this.assign(b,m));c(b||m);break;case r.ThisExpression:this.assign(b,"s");c(b||"s");break;case r.LocalsExpression:this.assign(b,"l");c(b||"l");break;
case r.NGValueParameter:this.assign(b,"v"),c(b||"v")}},getHasOwnProperty:function(a,b){var d=a+"."+b,c=this.current().own;c.hasOwnProperty(d)||(c[d]=this.nextId(!1,a+"&&("+this.escape(b)+" in "+a+")"));return c[d]},assign:function(a,b){if(a)return this.current().body.push(a,"=",b,";"),a},filter:function(a){this.state.filters.hasOwnProperty(a)||(this.state.filters[a]=this.nextId(!0));return this.state.filters[a]},ifDefined:function(a,b){return"ifDefined("+a+","+this.escape(b)+")"},plus:function(a,
b){return"plus("+a+","+b+")"},return_:function(a){this.current().body.push("return ",a,";")},if_:function(a,b,d){if(!0===a)b();else{var c=this.current().body;c.push("if(",a,"){");b();c.push("}");d&&(c.push("else{"),d(),c.push("}"))}},not:function(a){return"!("+a+")"},isNull:function(a){return a+"==null"},notNull:function(a){return a+"!=null"},nonComputedMember:function(a,b){var d=/[^$_a-zA-Z0-9]/g;return/^[$_a-zA-Z][$_a-zA-Z0-9]*$/.test(b)?a+"."+b:a+'["'+b.replace(d,this.stringEscapeFn)+'"]'},computedMember:function(a,
b){return a+"["+b+"]"},member:function(a,b,d){return d?this.computedMember(a,b):this.nonComputedMember(a,b)},getStringValue:function(a){this.assign(a,"getStringValue("+a+")")},lazyRecurse:function(a,b,d,c,e,f){var g=this;return function(){g.recurse(a,b,d,c,e,f)}},lazyAssign:function(a,b){var d=this;return function(){d.assign(a,b)}},stringEscapeRegex:/[^ a-zA-Z0-9]/g,stringEscapeFn:function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)},escape:function(a){if(D(a))return"'"+a.replace(this.stringEscapeRegex,
this.stringEscapeFn)+"'";if(Y(a))return a.toString();if(!0===a)return"true";if(!1===a)return"false";if(null===a)return"null";if("undefined"===typeof a)return"undefined";throw Ya("esc");},nextId:function(a,b){var d="v"+this.state.nextId++;a||this.current().vars.push(d+(b?"="+b:""));return d},current:function(){return this.state[this.state.computing]}};Kd.prototype={compile:function(a){var b=this;V(a,b.$filter);var d,c;if(d=Id(a))c=this.recurse(d);d=Gd(a.body);var e;d&&(e=[],p(d,function(a,c){var d=
b.recurse(a);d.isPure=a.isPure;a.input=d;e.push(d);a.watchId=c}));var f=[];p(a.body,function(a){f.push(b.recurse(a.expression))});a=0===a.body.length?C:1===a.body.length?f[0]:function(a,b){var c;p(f,function(d){c=d(a,b)});return c};c&&(a.assign=function(a,b,d){return c(a,d,b)});e&&(a.inputs=e);return a},recurse:function(a,b,d){var c,e,f=this,g;if(a.input)return this.inputs(a.input,a.watchId);switch(a.type){case r.Literal:return this.value(a.value,b);case r.UnaryExpression:return e=this.recurse(a.argument),
this["unary"+a.operator](e,b);case r.BinaryExpression:return c=this.recurse(a.left),e=this.recurse(a.right),this["binary"+a.operator](c,e,b);case r.LogicalExpression:return c=this.recurse(a.left),e=this.recurse(a.right),this["binary"+a.operator](c,e,b);case r.ConditionalExpression:return this["ternary?:"](this.recurse(a.test),this.recurse(a.alternate),this.recurse(a.consequent),b);case r.Identifier:return f.identifier(a.name,b,d);case r.MemberExpression:return c=this.recurse(a.object,!1,!!d),a.computed||
(e=a.property.name),a.computed&&(e=this.recurse(a.property)),a.computed?this.computedMember(c,e,b,d):this.nonComputedMember(c,e,b,d);case r.CallExpression:return g=[],p(a.arguments,function(a){g.push(f.recurse(a))}),a.filter&&(e=this.$filter(a.callee.name)),a.filter||(e=this.recurse(a.callee,!0)),a.filter?function(a,c,d,f){for(var n=[],q=0;q<g.length;++q)n.push(g[q](a,c,d,f));a=e.apply(void 0,n,f);return b?{context:void 0,name:void 0,value:a}:a}:function(a,c,d,f){var n=e(a,c,d,f),q;if(null!=n.value){q=
[];for(var p=0;p<g.length;++p)q.push(g[p](a,c,d,f));q=n.value.apply(n.context,q)}return b?{value:q}:q};case r.AssignmentExpression:return c=this.recurse(a.left,!0,1),e=this.recurse(a.right),function(a,d,f,g){var n=c(a,d,f,g);a=e(a,d,f,g);n.context[n.name]=a;return b?{value:a}:a};case r.ArrayExpression:return g=[],p(a.elements,function(a){g.push(f.recurse(a))}),function(a,c,d,e){for(var f=[],q=0;q<g.length;++q)f.push(g[q](a,c,d,e));return b?{value:f}:f};case r.ObjectExpression:return g=[],p(a.properties,
function(a){a.computed?g.push({key:f.recurse(a.key),computed:!0,value:f.recurse(a.value)}):g.push({key:a.key.type===r.Identifier?a.key.name:""+a.key.value,computed:!1,value:f.recurse(a.value)})}),function(a,c,d,e){for(var f={},q=0;q<g.length;++q)g[q].computed?f[g[q].key(a,c,d,e)]=g[q].value(a,c,d,e):f[g[q].key]=g[q].value(a,c,d,e);return b?{value:f}:f};case r.ThisExpression:return function(a){return b?{value:a}:a};case r.LocalsExpression:return function(a,c){return b?{value:c}:c};case r.NGValueParameter:return function(a,
c,d){return b?{value:d}:d}}},"unary+":function(a,b){return function(d,c,e,f){d=a(d,c,e,f);d=t(d)?+d:0;return b?{value:d}:d}},"unary-":function(a,b){return function(d,c,e,f){d=a(d,c,e,f);d=t(d)?-d:-0;return b?{value:d}:d}},"unary!":function(a,b){return function(d,c,e,f){d=!a(d,c,e,f);return b?{value:d}:d}},"binary+":function(a,b,d){return function(c,e,f,g){var k=a(c,e,f,g);c=b(c,e,f,g);k=Ed(k,c);return d?{value:k}:k}},"binary-":function(a,b,d){return function(c,e,f,g){var k=a(c,e,f,g);c=b(c,e,f,g);
k=(t(k)?k:0)-(t(c)?c:0);return d?{value:k}:k}},"binary*":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)*b(c,e,f,g);return d?{value:c}:c}},"binary/":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)/b(c,e,f,g);return d?{value:c}:c}},"binary%":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)%b(c,e,f,g);return d?{value:c}:c}},"binary===":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)===b(c,e,f,g);return d?{value:c}:c}},"binary!==":function(a,b,d){return function(c,e,f,g){c=a(c,
e,f,g)!==b(c,e,f,g);return d?{value:c}:c}},"binary==":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)==b(c,e,f,g);return d?{value:c}:c}},"binary!=":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)!=b(c,e,f,g);return d?{value:c}:c}},"binary<":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)<b(c,e,f,g);return d?{value:c}:c}},"binary>":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)>b(c,e,f,g);return d?{value:c}:c}},"binary<=":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,
g)<=b(c,e,f,g);return d?{value:c}:c}},"binary>=":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)>=b(c,e,f,g);return d?{value:c}:c}},"binary&&":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)&&b(c,e,f,g);return d?{value:c}:c}},"binary||":function(a,b,d){return function(c,e,f,g){c=a(c,e,f,g)||b(c,e,f,g);return d?{value:c}:c}},"ternary?:":function(a,b,d,c){return function(e,f,g,k){e=a(e,f,g,k)?b(e,f,g,k):d(e,f,g,k);return c?{value:e}:e}},value:function(a,b){return function(){return b?{context:void 0,
name:void 0,value:a}:a}},identifier:function(a,b,d){return function(c,e,f,g){c=e&&a in e?e:c;d&&1!==d&&c&&null==c[a]&&(c[a]={});e=c?c[a]:void 0;return b?{context:c,name:a,value:e}:e}},computedMember:function(a,b,d,c){return function(e,f,g,k){var h=a(e,f,g,k),l,m;null!=h&&(l=b(e,f,g,k),l+="",c&&1!==c&&h&&!h[l]&&(h[l]={}),m=h[l]);return d?{context:h,name:l,value:m}:m}},nonComputedMember:function(a,b,d,c){return function(e,f,g,k){e=a(e,f,g,k);c&&1!==c&&e&&null==e[b]&&(e[b]={});f=null!=e?e[b]:void 0;
return d?{context:e,name:b,value:f}:f}},inputs:function(a,b){return function(d,c,e,f){return f?f[b]:a(d,c,e)}}};xc.prototype={constructor:xc,parse:function(a){a=this.ast.ast(a);var b=this.astCompiler.compile(a);b.literal=0===a.body.length||1===a.body.length&&(a.body[0].expression.type===r.Literal||a.body[0].expression.type===r.ArrayExpression||a.body[0].expression.type===r.ObjectExpression);b.constant=a.constant;return b}};var wa=M("$sce"),oa={HTML:"html",CSS:"css",URL:"url",RESOURCE_URL:"resourceUrl",
JS:"js"},Ac=/_([a-z])/g,Gg=M("$compile"),X=u.document.createElement("a"),Od=ua(u.location.href);Pd.$inject=["$document"];ed.$inject=["$provide"];var Wd=22,Vd=".",Cc="0";Qd.$inject=["$locale"];Sd.$inject=["$locale"];var Rg={yyyy:da("FullYear",4,0,!1,!0),yy:da("FullYear",2,0,!0,!0),y:da("FullYear",1,0,!1,!0),MMMM:ob("Month"),MMM:ob("Month",!0),MM:da("Month",2,1),M:da("Month",1,1),LLLL:ob("Month",!1,!0),dd:da("Date",2),d:da("Date",1),HH:da("Hours",2),H:da("Hours",1),hh:da("Hours",2,-12),h:da("Hours",
1,-12),mm:da("Minutes",2),m:da("Minutes",1),ss:da("Seconds",2),s:da("Seconds",1),sss:da("Milliseconds",3),EEEE:ob("Day"),EEE:ob("Day",!0),a:function(a,b){return 12>a.getHours()?b.AMPMS[0]:b.AMPMS[1]},Z:function(a,b,d){a=-1*d;return a=(0<=a?"+":"")+(Mb(Math[0<a?"floor":"ceil"](a/60),2)+Mb(Math.abs(a%60),2))},ww:Yd(2),w:Yd(1),G:Dc,GG:Dc,GGG:Dc,GGGG:function(a,b){return 0>=a.getFullYear()?b.ERANAMES[0]:b.ERANAMES[1]}},Qg=/((?:[^yMLdHhmsaZEwG']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|L+|d+|H+|h+|m+|s+|a|Z|G+|w+))([\s\S]*)/,
Pg=/^-?\d+$/;Rd.$inject=["$locale"];var Kg=ka(N),Lg=ka(wb);Td.$inject=["$parse"];var He=ka({restrict:"E",compile:function(a,b){if(!b.href&&!b.xlinkHref)return function(a,b){if("a"===b[0].nodeName.toLowerCase()){var e="[object SVGAnimatedString]"===ha.call(b.prop("href"))?"xlink:href":"href";b.on("click",function(a){b.attr(e)||a.preventDefault()})}}}}),xb={};p(Hb,function(a,b){function d(a,d,e){a.$watch(e[c],function(a){e.$set(b,!!a)})}if("multiple"!==a){var c=Ea("ng-"+b),e=d;"checked"===a&&(e=function(a,
b,e){e.ngModel!==e[c]&&d(a,b,e)});xb[c]=function(){return{restrict:"A",priority:100,link:e}}}});p(sd,function(a,b){xb[b]=function(){return{priority:100,link:function(a,c,e){if("ngPattern"===b&&"/"===e.ngPattern.charAt(0)&&(c=e.ngPattern.match(Vg))){e.$set("ngPattern",new RegExp(c[1],c[2]));return}a.$watch(e[b],function(a){e.$set(b,a)})}}}});p(["src","srcset","href"],function(a){var b=Ea("ng-"+a);xb[b]=function(){return{priority:99,link:function(d,c,e){var f=a,g=a;"href"===a&&"[object SVGAnimatedString]"===
ha.call(c.prop("href"))&&(g="xlinkHref",e.$attr[g]="xlink:href",f=null);e.$observe(b,function(b){b?(e.$set(g,b),Ca&&f&&c.prop(f,e[g])):"href"===a&&e.$set(g,null)})}}}});var Ob={$addControl:C,$$renameControl:function(a,b){a.$name=b},$removeControl:C,$setValidity:C,$setDirty:C,$setPristine:C,$setSubmitted:C};Nb.$inject=["$element","$attrs","$scope","$animate","$interpolate"];Nb.prototype={$rollbackViewValue:function(){p(this.$$controls,function(a){a.$rollbackViewValue()})},$commitViewValue:function(){p(this.$$controls,
function(a){a.$commitViewValue()})},$addControl:function(a){Ia(a.$name,"input");this.$$controls.push(a);a.$name&&(this[a.$name]=a);a.$$parentForm=this},$$renameControl:function(a,b){var d=a.$name;this[d]===a&&delete this[d];this[b]=a;a.$name=b},$removeControl:function(a){a.$name&&this[a.$name]===a&&delete this[a.$name];p(this.$pending,function(b,d){this.$setValidity(d,null,a)},this);p(this.$error,function(b,d){this.$setValidity(d,null,a)},this);p(this.$$success,function(b,d){this.$setValidity(d,null,
a)},this);db(this.$$controls,a);a.$$parentForm=Ob},$setDirty:function(){this.$$animate.removeClass(this.$$element,Za);this.$$animate.addClass(this.$$element,Tb);this.$dirty=!0;this.$pristine=!1;this.$$parentForm.$setDirty()},$setPristine:function(){this.$$animate.setClass(this.$$element,Za,Tb+" ng-submitted");this.$dirty=!1;this.$pristine=!0;this.$submitted=!1;p(this.$$controls,function(a){a.$setPristine()})},$setUntouched:function(){p(this.$$controls,function(a){a.$setUntouched()})},$setSubmitted:function(){this.$$animate.addClass(this.$$element,
"ng-submitted");this.$submitted=!0;this.$$parentForm.$setSubmitted()}};ae({clazz:Nb,set:function(a,b,d){var c=a[b];c?-1===c.indexOf(d)&&c.push(d):a[b]=[d]},unset:function(a,b,d){var c=a[b];c&&(db(c,d),0===c.length&&delete a[b])}});var ie=function(a){return["$timeout","$parse",function(b,d){function c(a){return""===a?d('this[""]').assign:d(a).assign||C}return{name:"form",restrict:a?"EAC":"E",require:["form","^^?form"],controller:Nb,compile:function(d,f){d.addClass(Za).addClass(pb);var g=f.name?"name":
a&&f.ngForm?"ngForm":!1;return{pre:function(a,d,e,f){var n=f[0];if(!("action"in e)){var q=function(b){a.$apply(function(){n.$commitViewValue();n.$setSubmitted()});b.preventDefault()};d[0].addEventListener("submit",q);d.on("$destroy",function(){b(function(){d[0].removeEventListener("submit",q)},0,!1)})}(f[1]||n.$$parentForm).$addControl(n);var p=g?c(n.$name):C;g&&(p(a,n),e.$observe(g,function(b){n.$name!==b&&(p(a,void 0),n.$$parentForm.$$renameControl(n,b),p=c(n.$name),p(a,n))}));d.on("$destroy",function(){n.$$parentForm.$removeControl(n);
p(a,void 0);P(n,Ob)})}}}}}]},Ie=ie(),Ue=ie(!0),Sg=/^\d{4,}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+(?:[+-][0-2]\d:[0-5]\d|Z)$/,dh=/^[a-z][a-z\d.+-]*:\/*(?:[^:@]+(?::[^@]+)?@)?(?:[^\s:/?#]+|\[[a-f\d:]+])(?::\d+)?(?:\/[^?#]*)?(?:\?[^#]*)?(?:#.*)?$/i,eh=/^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/,Tg=/^\s*(-|\+)?(\d+|(\d*(\.\d*)))([eE][+-]?\d+)?\s*$/,je=/^(\d{4,})-(\d{2})-(\d{2})$/,
ke=/^(\d{4,})-(\d\d)-(\d\d)T(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,Kc=/^(\d{4,})-W(\d\d)$/,le=/^(\d{4,})-(\d\d)$/,me=/^(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,ce=S();p(["date","datetime-local","month","time","week"],function(a){ce[a]=!0});var ne={text:function(a,b,d,c,e,f){Wa(a,b,d,c,e,f);Fc(c)},date:qb("date",je,Pb(je,["yyyy","MM","dd"]),"yyyy-MM-dd"),"datetime-local":qb("datetimelocal",ke,Pb(ke,"yyyy MM dd HH mm ss sss".split(" ")),"yyyy-MM-ddTHH:mm:ss.sss"),time:qb("time",me,Pb(me,["HH","mm",
"ss","sss"]),"HH:mm:ss.sss"),week:qb("week",Kc,function(a,b){if(ea(a))return a;if(D(a)){Kc.lastIndex=0;var d=Kc.exec(a);if(d){var c=+d[1],e=+d[2],f=d=0,g=0,k=0,h=Xd(c),e=7*(e-1);b&&(d=b.getHours(),f=b.getMinutes(),g=b.getSeconds(),k=b.getMilliseconds());return new Date(c,0,h.getDate()+e,d,f,g,k)}}return NaN},"yyyy-Www"),month:qb("month",le,Pb(le,["yyyy","MM"]),"yyyy-MM"),number:function(a,b,d,c,e,f){Gc(a,b,d,c);de(c);Wa(a,b,d,c,e,f);var g,k;if(t(d.min)||d.ngMin)c.$validators.min=function(a){return c.$isEmpty(a)||
w(g)||a>=g},d.$observe("min",function(a){g=Xa(a);c.$validate()});if(t(d.max)||d.ngMax)c.$validators.max=function(a){return c.$isEmpty(a)||w(k)||a<=k},d.$observe("max",function(a){k=Xa(a);c.$validate()});if(t(d.step)||d.ngStep){var h;c.$validators.step=function(a,b){return c.$isEmpty(b)||w(h)||ee(b,g||0,h)};d.$observe("step",function(a){h=Xa(a);c.$validate()})}},url:function(a,b,d,c,e,f){Wa(a,b,d,c,e,f);Fc(c);c.$$parserName="url";c.$validators.url=function(a,b){var d=a||b;return c.$isEmpty(d)||dh.test(d)}},
email:function(a,b,d,c,e,f){Wa(a,b,d,c,e,f);Fc(c);c.$$parserName="email";c.$validators.email=function(a,b){var d=a||b;return c.$isEmpty(d)||eh.test(d)}},radio:function(a,b,d,c){var e=!d.ngTrim||"false"!==Q(d.ngTrim);w(d.name)&&b.attr("name",++sb);b.on("click",function(a){var g;b[0].checked&&(g=d.value,e&&(g=Q(g)),c.$setViewValue(g,a&&a.type))});c.$render=function(){var a=d.value;e&&(a=Q(a));b[0].checked=a===c.$viewValue};d.$observe("value",c.$render)},range:function(a,b,d,c,e,f){function g(a,c){b.attr(a,
d[a]);d.$observe(a,c)}function k(a){n=Xa(a);T(c.$modelValue)||(m?(a=b.val(),n>a&&(a=n,b.val(a)),c.$setViewValue(a)):c.$validate())}function h(a){q=Xa(a);T(c.$modelValue)||(m?(a=b.val(),q<a&&(b.val(q),a=q<n?n:q),c.$setViewValue(a)):c.$validate())}function l(a){p=Xa(a);T(c.$modelValue)||(m&&c.$viewValue!==b.val()?c.$setViewValue(b.val()):c.$validate())}Gc(a,b,d,c);de(c);Wa(a,b,d,c,e,f);var m=c.$$hasNativeValidators&&"range"===b[0].type,n=m?0:void 0,q=m?100:void 0,p=m?1:void 0,r=b[0].validity;a=t(d.min);
e=t(d.max);f=t(d.step);var z=c.$render;c.$render=m&&t(r.rangeUnderflow)&&t(r.rangeOverflow)?function(){z();c.$setViewValue(b.val())}:z;a&&(c.$validators.min=m?function(){return!0}:function(a,b){return c.$isEmpty(b)||w(n)||b>=n},g("min",k));e&&(c.$validators.max=m?function(){return!0}:function(a,b){return c.$isEmpty(b)||w(q)||b<=q},g("max",h));f&&(c.$validators.step=m?function(){return!r.stepMismatch}:function(a,b){return c.$isEmpty(b)||w(p)||ee(b,n||0,p)},g("step",l))},checkbox:function(a,b,d,c,e,
f,g,k){var h=fe(k,a,"ngTrueValue",d.ngTrueValue,!0),l=fe(k,a,"ngFalseValue",d.ngFalseValue,!1);b.on("click",function(a){c.$setViewValue(b[0].checked,a&&a.type)});c.$render=function(){b[0].checked=c.$viewValue};c.$isEmpty=function(a){return!1===a};c.$formatters.push(function(a){return sa(a,h)});c.$parsers.push(function(a){return a?h:l})},hidden:C,button:C,submit:C,reset:C,file:C},Zc=["$browser","$sniffer","$filter","$parse",function(a,b,d,c){return{restrict:"E",require:["?ngModel"],link:{pre:function(e,
f,g,k){k[0]&&(ne[N(g.type)]||ne.text)(e,f,g,k[0],b,a,d,c)}}}}],fh=/^(true|false|\d+)$/,mf=function(){function a(a,d,c){var e=t(c)?c:9===Ca?"":null;a.prop("value",e);d.$set("value",c)}return{restrict:"A",priority:100,compile:function(b,d){return fh.test(d.ngValue)?function(b,d,f){b=b.$eval(f.ngValue);a(d,f,b)}:function(b,d,f){b.$watch(f.ngValue,function(b){a(d,f,b)})}}}},Me=["$compile",function(a){return{restrict:"AC",compile:function(b){a.$$addBindingClass(b);return function(b,c,e){a.$$addBindingInfo(c,
e.ngBind);c=c[0];b.$watch(e.ngBind,function(a){c.textContent=dc(a)})}}}}],Oe=["$interpolate","$compile",function(a,b){return{compile:function(d){b.$$addBindingClass(d);return function(c,d,f){c=a(d.attr(f.$attr.ngBindTemplate));b.$$addBindingInfo(d,c.expressions);d=d[0];f.$observe("ngBindTemplate",function(a){d.textContent=w(a)?"":a})}}}}],Ne=["$sce","$parse","$compile",function(a,b,d){return{restrict:"A",compile:function(c,e){var f=b(e.ngBindHtml),g=b(e.ngBindHtml,function(b){return a.valueOf(b)});
d.$$addBindingClass(c);return function(b,c,e){d.$$addBindingInfo(c,e.ngBindHtml);b.$watch(g,function(){var d=f(b);c.html(a.getTrustedHtml(d)||"")})}}}}],lf=ka({restrict:"A",require:"ngModel",link:function(a,b,d,c){c.$viewChangeListeners.push(function(){a.$eval(d.ngChange)})}}),Pe=Ic("",!0),Re=Ic("Odd",0),Qe=Ic("Even",1),Se=Qa({compile:function(a,b){b.$set("ngCloak",void 0);a.removeClass("ng-cloak")}}),Te=[function(){return{restrict:"A",scope:!0,controller:"@",priority:500}}],dd={},gh={blur:!0,focus:!0};
p("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "),function(a){var b=Ea("ng-"+a);dd[b]=["$parse","$rootScope",function(d,c){return{restrict:"A",compile:function(e,f){var g=d(f[b]);return function(b,d){d.on(a,function(d){var e=function(){g(b,{$event:d})};gh[a]&&c.$$phase?b.$evalAsync(e):b.$apply(e)})}}}}]});var We=["$animate","$compile",function(a,b){return{multiElement:!0,transclude:"element",priority:600,
terminal:!0,restrict:"A",$$tlb:!0,link:function(d,c,e,f,g){var k,h,l;d.$watch(e.ngIf,function(d){d?h||g(function(d,f){h=f;d[d.length++]=b.$$createComment("end ngIf",e.ngIf);k={clone:d};a.enter(d,c.parent(),c)}):(l&&(l.remove(),l=null),h&&(h.$destroy(),h=null),k&&(l=vb(k.clone),a.leave(l).done(function(a){!1!==a&&(l=null)}),k=null))})}}}],Xe=["$templateRequest","$anchorScroll","$animate",function(a,b,d){return{restrict:"ECA",priority:400,terminal:!0,transclude:"element",controller:$.noop,compile:function(c,
e){var f=e.ngInclude||e.src,g=e.onload||"",k=e.autoscroll;return function(c,e,m,n,q){var p=0,r,z,v,s=function(){z&&(z.remove(),z=null);r&&(r.$destroy(),r=null);v&&(d.leave(v).done(function(a){!1!==a&&(z=null)}),z=v,v=null)};c.$watch(f,function(f){var m=function(a){!1===a||!t(k)||k&&!c.$eval(k)||b()},y=++p;f?(a(f,!0).then(function(a){if(!c.$$destroyed&&y===p){var b=c.$new();n.template=a;a=q(b,function(a){s();d.enter(a,null,e).done(m)});r=b;v=a;r.$emit("$includeContentLoaded",f);c.$eval(g)}},function(){c.$$destroyed||
y!==p||(s(),c.$emit("$includeContentError",f))}),c.$emit("$includeContentRequested",f)):(s(),n.template=null)})}}}}],of=["$compile",function(a){return{restrict:"ECA",priority:-400,require:"ngInclude",link:function(b,d,c,e){ha.call(d[0]).match(/SVG/)?(d.empty(),a(fd(e.template,u.document).childNodes)(b,function(a){d.append(a)},{futureParentElement:d})):(d.html(e.template),a(d.contents())(b))}}}],Ye=Qa({priority:450,compile:function(){return{pre:function(a,b,d){a.$eval(d.ngInit)}}}}),kf=function(){return{restrict:"A",
priority:100,require:"ngModel",link:function(a,b,d,c){var e=d.ngList||", ",f="false"!==d.ngTrim,g=f?Q(e):e;c.$parsers.push(function(a){if(!w(a)){var b=[];a&&p(a.split(g),function(a){a&&b.push(f?Q(a):a)});return b}});c.$formatters.push(function(a){if(I(a))return a.join(e)});c.$isEmpty=function(a){return!a||!a.length}}}},pb="ng-valid",$d="ng-invalid",Za="ng-pristine",Tb="ng-dirty",rb=M("ngModel");Qb.$inject="$scope $exceptionHandler $attrs $element $parse $animate $timeout $q $interpolate".split(" ");
Qb.prototype={$$initGetterSetters:function(){if(this.$options.getOption("getterSetter")){var a=this.$$parse(this.$$attr.ngModel+"()"),b=this.$$parse(this.$$attr.ngModel+"($$$p)");this.$$ngModelGet=function(b){var c=this.$$parsedNgModel(b);A(c)&&(c=a(b));return c};this.$$ngModelSet=function(a,c){A(this.$$parsedNgModel(a))?b(a,{$$$p:c}):this.$$parsedNgModelAssign(a,c)}}else if(!this.$$parsedNgModel.assign)throw rb("nonassign",this.$$attr.ngModel,Aa(this.$$element));},$render:C,$isEmpty:function(a){return w(a)||
""===a||null===a||a!==a},$$updateEmptyClasses:function(a){this.$isEmpty(a)?(this.$$animate.removeClass(this.$$element,"ng-not-empty"),this.$$animate.addClass(this.$$element,"ng-empty")):(this.$$animate.removeClass(this.$$element,"ng-empty"),this.$$animate.addClass(this.$$element,"ng-not-empty"))},$setPristine:function(){this.$dirty=!1;this.$pristine=!0;this.$$animate.removeClass(this.$$element,Tb);this.$$animate.addClass(this.$$element,Za)},$setDirty:function(){this.$dirty=!0;this.$pristine=!1;this.$$animate.removeClass(this.$$element,
Za);this.$$animate.addClass(this.$$element,Tb);this.$$parentForm.$setDirty()},$setUntouched:function(){this.$touched=!1;this.$untouched=!0;this.$$animate.setClass(this.$$element,"ng-untouched","ng-touched")},$setTouched:function(){this.$touched=!0;this.$untouched=!1;this.$$animate.setClass(this.$$element,"ng-touched","ng-untouched")},$rollbackViewValue:function(){this.$$timeout.cancel(this.$$pendingDebounce);this.$viewValue=this.$$lastCommittedViewValue;this.$render()},$validate:function(){if(!T(this.$modelValue)){var a=
this.$$lastCommittedViewValue,b=this.$$rawModelValue,d=this.$valid,c=this.$modelValue,e=this.$options.getOption("allowInvalid"),f=this;this.$$runValidators(b,a,function(a){e||d===a||(f.$modelValue=a?b:void 0,f.$modelValue!==c&&f.$$writeModelToScope())})}},$$runValidators:function(a,b,d){function c(){var c=!0;p(h.$validators,function(d,e){var g=Boolean(d(a,b));c=c&&g;f(e,g)});return c?!0:(p(h.$asyncValidators,function(a,b){f(b,null)}),!1)}function e(){var c=[],d=!0;p(h.$asyncValidators,function(e,
g){var h=e(a,b);if(!h||!A(h.then))throw rb("nopromise",h);f(g,void 0);c.push(h.then(function(){f(g,!0)},function(){d=!1;f(g,!1)}))});c.length?h.$$q.all(c).then(function(){g(d)},C):g(!0)}function f(a,b){k===h.$$currentValidationRunId&&h.$setValidity(a,b)}function g(a){k===h.$$currentValidationRunId&&d(a)}this.$$currentValidationRunId++;var k=this.$$currentValidationRunId,h=this;(function(){var a=h.$$parserName||"parse";if(w(h.$$parserValid))f(a,null);else return h.$$parserValid||(p(h.$validators,function(a,
b){f(b,null)}),p(h.$asyncValidators,function(a,b){f(b,null)})),f(a,h.$$parserValid),h.$$parserValid;return!0})()?c()?e():g(!1):g(!1)},$commitViewValue:function(){var a=this.$viewValue;this.$$timeout.cancel(this.$$pendingDebounce);if(this.$$lastCommittedViewValue!==a||""===a&&this.$$hasNativeValidators)this.$$updateEmptyClasses(a),this.$$lastCommittedViewValue=a,this.$pristine&&this.$setDirty(),this.$$parseAndValidate()},$$parseAndValidate:function(){var a=this.$$lastCommittedViewValue,b=this;if(this.$$parserValid=
w(a)?void 0:!0)for(var d=0;d<this.$parsers.length;d++)if(a=this.$parsers[d](a),w(a)){this.$$parserValid=!1;break}T(this.$modelValue)&&(this.$modelValue=this.$$ngModelGet(this.$$scope));var c=this.$modelValue,e=this.$options.getOption("allowInvalid");this.$$rawModelValue=a;e&&(this.$modelValue=a,b.$modelValue!==c&&b.$$writeModelToScope());this.$$runValidators(a,this.$$lastCommittedViewValue,function(d){e||(b.$modelValue=d?a:void 0,b.$modelValue!==c&&b.$$writeModelToScope())})},$$writeModelToScope:function(){this.$$ngModelSet(this.$$scope,
this.$modelValue);p(this.$viewChangeListeners,function(a){try{a()}catch(b){this.$$exceptionHandler(b)}},this)},$setViewValue:function(a,b){this.$viewValue=a;this.$options.getOption("updateOnDefault")&&this.$$debounceViewValueCommit(b)},$$debounceViewValueCommit:function(a){var b=this.$options.getOption("debounce");Y(b[a])?b=b[a]:Y(b["default"])&&(b=b["default"]);this.$$timeout.cancel(this.$$pendingDebounce);var d=this;0<b?this.$$pendingDebounce=this.$$timeout(function(){d.$commitViewValue()},b):this.$$scope.$root.$$phase?
this.$commitViewValue():this.$$scope.$apply(function(){d.$commitViewValue()})},$overrideModelOptions:function(a){this.$options=this.$options.createChild(a)}};ae({clazz:Qb,set:function(a,b){a[b]=!0},unset:function(a,b){delete a[b]}});var jf=["$rootScope",function(a){return{restrict:"A",require:["ngModel","^?form","^?ngModelOptions"],controller:Qb,priority:1,compile:function(b){b.addClass(Za).addClass("ng-untouched").addClass(pb);return{pre:function(a,b,e,f){var g=f[0];b=f[1]||g.$$parentForm;if(f=f[2])g.$options=
f.$options;g.$$initGetterSetters();b.$addControl(g);e.$observe("name",function(a){g.$name!==a&&g.$$parentForm.$$renameControl(g,a)});a.$on("$destroy",function(){g.$$parentForm.$removeControl(g)})},post:function(b,c,e,f){function g(){k.$setTouched()}var k=f[0];if(k.$options.getOption("updateOn"))c.on(k.$options.getOption("updateOn"),function(a){k.$$debounceViewValueCommit(a&&a.type)});c.on("blur",function(){k.$touched||(a.$$phase?b.$evalAsync(g):b.$apply(g))})}}}}}],Rb,hh=/(\s+|^)default(\s+|$)/;Jc.prototype=
{getOption:function(a){return this.$$options[a]},createChild:function(a){var b=!1;a=P({},a);p(a,function(d,c){"$inherit"===d?"*"===c?b=!0:(a[c]=this.$$options[c],"updateOn"===c&&(a.updateOnDefault=this.$$options.updateOnDefault)):"updateOn"===c&&(a.updateOnDefault=!1,a[c]=Q(d.replace(hh,function(){a.updateOnDefault=!0;return" "})))},this);b&&(delete a["*"],ge(a,this.$$options));ge(a,Rb.$$options);return new Jc(a)}};Rb=new Jc({updateOn:"",updateOnDefault:!0,debounce:0,getterSetter:!1,allowInvalid:!1,
timezone:null});var nf=function(){function a(a,d){this.$$attrs=a;this.$$scope=d}a.$inject=["$attrs","$scope"];a.prototype={$onInit:function(){var a=this.parentCtrl?this.parentCtrl.$options:Rb,d=this.$$scope.$eval(this.$$attrs.ngModelOptions);this.$options=a.createChild(d)}};return{restrict:"A",priority:10,require:{parentCtrl:"?^^ngModelOptions"},bindToController:!0,controller:a}},Ze=Qa({terminal:!0,priority:1E3}),ih=M("ngOptions"),jh=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?(?:\s+disable\s+when\s+([\s\S]+?))?\s+for\s+(?:([$\w][$\w]*)|(?:\(\s*([$\w][$\w]*)\s*,\s*([$\w][$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,
gf=["$compile","$document","$parse",function(a,b,d){function c(a,b,c){function e(a,b,c,d,f){this.selectValue=a;this.viewValue=b;this.label=c;this.group=d;this.disabled=f}function f(a){var b;if(!p&&xa(a))b=a;else{b=[];for(var c in a)a.hasOwnProperty(c)&&"$"!==c.charAt(0)&&b.push(c)}return b}var n=a.match(jh);if(!n)throw ih("iexp",a,Aa(b));var q=n[5]||n[7],p=n[6];a=/ as /.test(n[0])&&n[1];var r=n[9];b=d(n[2]?n[1]:q);var z=a&&d(a)||b,t=r&&d(r),s=r?function(a,b){return t(c,b)}:function(a){return Pa(a)},
w=function(a,b){return s(a,A(a,b))},u=d(n[2]||n[1]),y=d(n[3]||""),J=d(n[4]||""),H=d(n[8]),B={},A=p?function(a,b){B[p]=b;B[q]=a;return B}:function(a){B[q]=a;return B};return{trackBy:r,getTrackByValue:w,getWatchables:d(H,function(a){var b=[];a=a||[];for(var d=f(a),e=d.length,g=0;g<e;g++){var k=a===d?g:d[g],l=a[k],k=A(l,k),l=s(l,k);b.push(l);if(n[2]||n[1])l=u(c,k),b.push(l);n[4]&&(k=J(c,k),b.push(k))}return b}),getOptions:function(){for(var a=[],b={},d=H(c)||[],g=f(d),k=g.length,n=0;n<k;n++){var q=d===
g?n:g[n],p=A(d[q],q),t=z(c,p),q=s(t,p),v=u(c,p),G=y(c,p),p=J(c,p),t=new e(q,t,v,G,p);a.push(t);b[q]=t}return{items:a,selectValueMap:b,getOptionFromViewValue:function(a){return b[w(a)]},getViewValueFromOption:function(a){return r?pa(a.viewValue):a.viewValue}}}}}var e=u.document.createElement("option"),f=u.document.createElement("optgroup");return{restrict:"A",terminal:!0,require:["select","ngModel"],link:{pre:function(a,b,c,d){d[0].registerOption=C},post:function(d,k,h,l){function m(a){var b=(a=s.getOptionFromViewValue(a))&&
a.element;b&&!b.selected&&(b.selected=!0);return a}function n(a,b){a.element=b;b.disabled=a.disabled;a.label!==b.label&&(b.label=a.label,b.textContent=a.label);b.value=a.selectValue}var q=l[0],r=l[1],w=h.multiple;l=0;for(var z=k.children(),v=z.length;l<v;l++)if(""===z[l].value){q.hasEmptyOption=!0;q.emptyOption=z.eq(l);break}k.empty();l=!!q.emptyOption;B(e.cloneNode(!1)).val("?");var s,u=c(h.ngOptions,k,d),A=b[0].createDocumentFragment();q.generateUnknownOptionValue=function(a){return"?"};w?(q.writeValue=
function(a){if(s){var b=a&&a.map(m)||[];s.items.forEach(function(a){a.element.selected&&-1===Array.prototype.indexOf.call(b,a)&&(a.element.selected=!1)})}},q.readValue=function(){var a=k.val()||[],b=[];p(a,function(a){(a=s.selectValueMap[a])&&!a.disabled&&b.push(s.getViewValueFromOption(a))});return b},u.trackBy&&d.$watchCollection(function(){if(I(r.$viewValue))return r.$viewValue.map(function(a){return u.getTrackByValue(a)})},function(){r.$render()})):(q.writeValue=function(a){if(s){var b=k[0].options[k[0].selectedIndex],
c=s.getOptionFromViewValue(a);b&&b.removeAttribute("selected");c?(k[0].value!==c.selectValue&&(q.removeUnknownOption(),k[0].value=c.selectValue,c.element.selected=!0),c.element.setAttribute("selected","selected")):q.selectUnknownOrEmptyOption(a)}},q.readValue=function(){var a=s.selectValueMap[k.val()];return a&&!a.disabled?(q.unselectEmptyOption(),q.removeUnknownOption(),s.getViewValueFromOption(a)):null},u.trackBy&&d.$watch(function(){return u.getTrackByValue(r.$viewValue)},function(){r.$render()}));
l&&(a(q.emptyOption)(d),k.prepend(q.emptyOption),8===q.emptyOption[0].nodeType?(q.hasEmptyOption=!1,q.registerOption=function(a,b){""===b.val()&&(q.hasEmptyOption=!0,q.emptyOption=b,q.emptyOption.removeClass("ng-scope"),r.$render(),b.on("$destroy",function(){var a=q.$isEmptyOptionSelected();q.hasEmptyOption=!1;q.emptyOption=void 0;a&&r.$render()}))}):q.emptyOption.removeClass("ng-scope"));d.$watchCollection(u.getWatchables,function(){var a=s&&q.readValue();if(s)for(var b=s.items.length-1;0<=b;b--){var c=
s.items[b];t(c.group)?Gb(c.element.parentNode):Gb(c.element)}s=u.getOptions();var d={};s.items.forEach(function(a){var b;if(t(a.group)){b=d[a.group];b||(b=f.cloneNode(!1),A.appendChild(b),b.label=null===a.group?"null":a.group,d[a.group]=b);var c=e.cloneNode(!1);b.appendChild(c);n(a,c)}else b=e.cloneNode(!1),A.appendChild(b),n(a,b)});k[0].appendChild(A);r.$render();r.$isEmpty(a)||(b=q.readValue(),(u.trackBy||w?sa(a,b):a===b)||(r.$setViewValue(b),r.$render()))})}}}}],$e=["$locale","$interpolate","$log",
function(a,b,d){var c=/{}/g,e=/^when(Minus)?(.+)$/;return{link:function(f,g,k){function h(a){g.text(a||"")}var l=k.count,m=k.$attr.when&&g.attr(k.$attr.when),n=k.offset||0,q=f.$eval(m)||{},r={},t=b.startSymbol(),z=b.endSymbol(),v=t+l+"-"+n+z,s=$.noop,u;p(k,function(a,b){var c=e.exec(b);c&&(c=(c[1]?"-":"")+N(c[2]),q[c]=g.attr(k.$attr[b]))});p(q,function(a,d){r[d]=b(a.replace(c,v))});f.$watch(l,function(b){var c=parseFloat(b),e=T(c);e||c in q||(c=a.pluralCat(c-n));c===u||e&&T(u)||(s(),e=r[c],w(e)?(null!=
b&&d.debug("ngPluralize: no rule defined for '"+c+"' in "+m),s=C,h()):s=f.$watch(e,h),u=c)})}}}],af=["$parse","$animate","$compile",function(a,b,d){var c=M("ngRepeat"),e=function(a,b,c,d,e,m,n){a[c]=d;e&&(a[e]=m);a.$index=b;a.$first=0===b;a.$last=b===n-1;a.$middle=!(a.$first||a.$last);a.$odd=!(a.$even=0===(b&1))};return{restrict:"A",multiElement:!0,transclude:"element",priority:1E3,terminal:!0,$$tlb:!0,compile:function(f,g){var k=g.ngRepeat,h=d.$$createComment("end ngRepeat",k),l=k.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
if(!l)throw c("iexp",k);var m=l[1],n=l[2],q=l[3],r=l[4],l=m.match(/^(?:(\s*[$\w]+)|\(\s*([$\w]+)\s*,\s*([$\w]+)\s*\))$/);if(!l)throw c("iidexp",m);var t=l[3]||l[1],z=l[2];if(q&&(!/^[$a-zA-Z_][$a-zA-Z0-9_]*$/.test(q)||/^(null|undefined|this|\$index|\$first|\$middle|\$last|\$even|\$odd|\$parent|\$root|\$id)$/.test(q)))throw c("badident",q);var v,s,u,w,y={$id:Pa};r?v=a(r):(u=function(a,b){return Pa(b)},w=function(a){return a});return function(a,d,f,g,l){v&&(s=function(b,c,d){z&&(y[z]=b);y[t]=c;y.$index=
d;return v(a,y)});var m=S();a.$watchCollection(n,function(f){var g,n,r=d[0],v,y=S(),B,A,G,C,E,D,I;q&&(a[q]=f);if(xa(f))E=f,n=s||u;else for(I in n=s||w,E=[],f)ra.call(f,I)&&"$"!==I.charAt(0)&&E.push(I);B=E.length;I=Array(B);for(g=0;g<B;g++)if(A=f===E?g:E[g],G=f[A],C=n(A,G,g),m[C])D=m[C],delete m[C],y[C]=D,I[g]=D;else{if(y[C])throw p(I,function(a){a&&a.scope&&(m[a.id]=a)}),c("dupes",k,C,G);I[g]={id:C,scope:void 0,clone:void 0};y[C]=!0}for(v in m){D=m[v];C=vb(D.clone);b.leave(C);if(C[0].parentNode)for(g=
0,n=C.length;g<n;g++)C[g].$$NG_REMOVED=!0;D.scope.$destroy()}for(g=0;g<B;g++)if(A=f===E?g:E[g],G=f[A],D=I[g],D.scope){v=r;do v=v.nextSibling;while(v&&v.$$NG_REMOVED);D.clone[0]!==v&&b.move(vb(D.clone),null,r);r=D.clone[D.clone.length-1];e(D.scope,g,t,G,z,A,B)}else l(function(a,c){D.scope=c;var d=h.cloneNode(!1);a[a.length++]=d;b.enter(a,null,r);r=d;D.clone=a;y[D.id]=D;e(D.scope,g,t,G,z,A,B)});m=y})}}}}],bf=["$animate",function(a){return{restrict:"A",multiElement:!0,link:function(b,d,c){b.$watch(c.ngShow,
function(b){a[b?"removeClass":"addClass"](d,"ng-hide",{tempClasses:"ng-hide-animate"})})}}}],Ve=["$animate",function(a){return{restrict:"A",multiElement:!0,link:function(b,d,c){b.$watch(c.ngHide,function(b){a[b?"addClass":"removeClass"](d,"ng-hide",{tempClasses:"ng-hide-animate"})})}}}],cf=Qa(function(a,b,d){a.$watch(d.ngStyle,function(a,d){d&&a!==d&&p(d,function(a,c){b.css(c,"")});a&&b.css(a)},!0)}),df=["$animate","$compile",function(a,b){return{require:"ngSwitch",controller:["$scope",function(){this.cases=
{}}],link:function(d,c,e,f){var g=[],k=[],h=[],l=[],m=function(a,b){return function(c){!1!==c&&a.splice(b,1)}};d.$watch(e.ngSwitch||e.on,function(c){for(var d,e;h.length;)a.cancel(h.pop());d=0;for(e=l.length;d<e;++d){var r=vb(k[d].clone);l[d].$destroy();(h[d]=a.leave(r)).done(m(h,d))}k.length=0;l.length=0;(g=f.cases["!"+c]||f.cases["?"])&&p(g,function(c){c.transclude(function(d,e){l.push(e);var f=c.element;d[d.length++]=b.$$createComment("end ngSwitchWhen");k.push({clone:d});a.enter(d,f.parent(),
f)})})})}}}],ef=Qa({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(a,b,d,c,e){a=d.ngSwitchWhen.split(d.ngSwitchWhenSeparator).sort().filter(function(a,b,c){return c[b-1]!==a});p(a,function(a){c.cases["!"+a]=c.cases["!"+a]||[];c.cases["!"+a].push({transclude:e,element:b})})}}),ff=Qa({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(a,b,d,c,e){c.cases["?"]=c.cases["?"]||[];c.cases["?"].push({transclude:e,element:b})}}),kh=M("ngTransclude"),
hf=["$compile",function(a){return{restrict:"EAC",terminal:!0,compile:function(b){var d=a(b.contents());b.empty();return function(a,b,f,g,k){function h(){d(a,function(a){b.append(a)})}if(!k)throw kh("orphan",Aa(b));f.ngTransclude===f.$attr.ngTransclude&&(f.ngTransclude="");f=f.ngTransclude||f.ngTranscludeSlot;k(function(a,c){var d;if(d=a.length)a:{d=0;for(var f=a.length;d<f;d++){var g=a[d];if(g.nodeType!==Oa||g.nodeValue.trim()){d=!0;break a}}d=void 0}d?b.append(a):(h(),c.$destroy())},null,f);f&&!k.isSlotFilled(f)&&
h()}}}}],Je=["$templateCache",function(a){return{restrict:"E",terminal:!0,compile:function(b,d){"text/ng-template"===d.type&&a.put(d.id,b[0].text)}}}],lh={$setViewValue:C,$render:C},mh=["$element","$scope",function(a,b){function d(){g||(g=!0,b.$$postDigest(function(){g=!1;e.ngModelCtrl.$render()}))}function c(a){k||(k=!0,b.$$postDigest(function(){b.$$destroyed||(k=!1,e.ngModelCtrl.$setViewValue(e.readValue()),a&&e.ngModelCtrl.$render())}))}var e=this,f=new Ib;e.selectValueMap={};e.ngModelCtrl=lh;
e.multiple=!1;e.unknownOption=B(u.document.createElement("option"));e.hasEmptyOption=!1;e.emptyOption=void 0;e.renderUnknownOption=function(b){b=e.generateUnknownOptionValue(b);e.unknownOption.val(b);a.prepend(e.unknownOption);Ga(e.unknownOption,!0);a.val(b)};e.updateUnknownOption=function(b){b=e.generateUnknownOptionValue(b);e.unknownOption.val(b);Ga(e.unknownOption,!0);a.val(b)};e.generateUnknownOptionValue=function(a){return"? "+Pa(a)+" ?"};e.removeUnknownOption=function(){e.unknownOption.parent()&&
e.unknownOption.remove()};e.selectEmptyOption=function(){e.emptyOption&&(a.val(""),Ga(e.emptyOption,!0))};e.unselectEmptyOption=function(){e.hasEmptyOption&&Ga(e.emptyOption,!1)};b.$on("$destroy",function(){e.renderUnknownOption=C});e.readValue=function(){var b=a.val(),b=b in e.selectValueMap?e.selectValueMap[b]:b;return e.hasOption(b)?b:null};e.writeValue=function(b){var c=a[0].options[a[0].selectedIndex];c&&Ga(B(c),!1);e.hasOption(b)?(e.removeUnknownOption(),c=Pa(b),a.val(c in e.selectValueMap?
c:b),Ga(B(a[0].options[a[0].selectedIndex]),!0)):e.selectUnknownOrEmptyOption(b)};e.addOption=function(a,b){if(8!==b[0].nodeType){Ia(a,'"option value"');""===a&&(e.hasEmptyOption=!0,e.emptyOption=b);var c=f.get(a)||0;f.set(a,c+1);d()}};e.removeOption=function(a){var b=f.get(a);b&&(1===b?(f.delete(a),""===a&&(e.hasEmptyOption=!1,e.emptyOption=void 0)):f.set(a,b-1))};e.hasOption=function(a){return!!f.get(a)};e.$hasEmptyOption=function(){return e.hasEmptyOption};e.$isUnknownOptionSelected=function(){return a[0].options[0]===
e.unknownOption[0]};e.$isEmptyOptionSelected=function(){return e.hasEmptyOption&&a[0].options[a[0].selectedIndex]===e.emptyOption[0]};e.selectUnknownOrEmptyOption=function(a){null==a&&e.emptyOption?(e.removeUnknownOption(),e.selectEmptyOption()):e.unknownOption.parent().length?e.updateUnknownOption(a):e.renderUnknownOption(a)};var g=!1,k=!1;e.registerOption=function(a,b,f,g,k){if(f.$attr.ngValue){var p,r=NaN;f.$observe("value",function(a){var d,f=b.prop("selected");t(r)&&(e.removeOption(p),delete e.selectValueMap[r],
d=!0);r=Pa(a);p=a;e.selectValueMap[r]=a;e.addOption(a,b);b.attr("value",r);d&&f&&c()})}else g?f.$observe("value",function(a){e.readValue();var d,f=b.prop("selected");t(p)&&(e.removeOption(p),d=!0);p=a;e.addOption(a,b);d&&f&&c()}):k?a.$watch(k,function(a,d){f.$set("value",a);var g=b.prop("selected");d!==a&&e.removeOption(d);e.addOption(a,b);d&&g&&c()}):e.addOption(f.value,b);f.$observe("disabled",function(a){if("true"===a||a&&b.prop("selected"))e.multiple?c(!0):(e.ngModelCtrl.$setViewValue(null),e.ngModelCtrl.$render())});
b.on("$destroy",function(){var a=e.readValue(),b=f.value;e.removeOption(b);d();(e.multiple&&a&&-1!==a.indexOf(b)||a===b)&&c(!0)})}}],Ke=function(){return{restrict:"E",require:["select","?ngModel"],controller:mh,priority:1,link:{pre:function(a,b,d,c){var e=c[0],f=c[1];if(f){if(e.ngModelCtrl=f,b.on("change",function(){e.removeUnknownOption();a.$apply(function(){f.$setViewValue(e.readValue())})}),d.multiple){e.multiple=!0;e.readValue=function(){var a=[];p(b.find("option"),function(b){b.selected&&!b.disabled&&
(b=b.value,a.push(b in e.selectValueMap?e.selectValueMap[b]:b))});return a};e.writeValue=function(a){p(b.find("option"),function(b){var c=!!a&&(-1!==Array.prototype.indexOf.call(a,b.value)||-1!==Array.prototype.indexOf.call(a,e.selectValueMap[b.value]));c!==b.selected&&Ga(B(b),c)})};var g,k=NaN;a.$watch(function(){k!==f.$viewValue||sa(g,f.$viewValue)||(g=ja(f.$viewValue),f.$render());k=f.$viewValue});f.$isEmpty=function(a){return!a||0===a.length}}}else e.registerOption=C},post:function(a,b,d,c){var e=
c[1];if(e){var f=c[0];e.$render=function(){f.writeValue(e.$viewValue)}}}}}},Le=["$interpolate",function(a){return{restrict:"E",priority:100,compile:function(b,d){var c,e;t(d.ngValue)||(t(d.value)?c=a(d.value,!0):(e=a(b.text(),!0))||d.$set("value",b.text()));return function(a,b,d){var h=b.parent();(h=h.data("$selectController")||h.parent().data("$selectController"))&&h.registerOption(a,b,d,c,e)}}}}],ad=function(){return{restrict:"A",require:"?ngModel",link:function(a,b,d,c){c&&(d.required=!0,c.$validators.required=
function(a,b){return!d.required||!c.$isEmpty(b)},d.$observe("required",function(){c.$validate()}))}}},$c=function(){return{restrict:"A",require:"?ngModel",link:function(a,b,d,c){if(c){var e,f=d.ngPattern||d.pattern;d.$observe("pattern",function(a){D(a)&&0<a.length&&(a=new RegExp("^"+a+"$"));if(a&&!a.test)throw M("ngPattern")("noregexp",f,a,Aa(b));e=a||void 0;c.$validate()});c.$validators.pattern=function(a,b){return c.$isEmpty(b)||w(e)||e.test(b)}}}}},cd=function(){return{restrict:"A",require:"?ngModel",
link:function(a,b,d,c){if(c){var e=-1;d.$observe("maxlength",function(a){a=Z(a);e=T(a)?-1:a;c.$validate()});c.$validators.maxlength=function(a,b){return 0>e||c.$isEmpty(b)||b.length<=e}}}}},bd=function(){return{restrict:"A",require:"?ngModel",link:function(a,b,d,c){if(c){var e=0;d.$observe("minlength",function(a){e=Z(a)||0;c.$validate()});c.$validators.minlength=function(a,b){return c.$isEmpty(b)||b.length>=e}}}}};u.angular.bootstrap?u.console&&console.log("WARNING: Tried to load angular more than once."):
(Be(),Ee($),$.module("ngLocale",[],["$provide",function(a){function b(a){a+="";var b=a.indexOf(".");return-1==b?0:a.length-b-1}a.value("$locale",{DATETIME_FORMATS:{AMPMS:["AM","PM"],DAY:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),ERANAMES:["Before Christ","Anno Domini"],ERAS:["BC","AD"],FIRSTDAYOFWEEK:6,MONTH:"January February March April May June July August September October November December".split(" "),SHORTDAY:"Sun Mon Tue Wed Thu Fri Sat".split(" "),SHORTMONTH:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
STANDALONEMONTH:"January February March April May June July August September October November December".split(" "),WEEKENDRANGE:[5,6],fullDate:"EEEE, MMMM d, y",longDate:"MMMM d, y",medium:"MMM d, y h:mm:ss a",mediumDate:"MMM d, y",mediumTime:"h:mm:ss a","short":"M/d/yy h:mm a",shortDate:"M/d/yy",shortTime:"h:mm a"},NUMBER_FORMATS:{CURRENCY_SYM:"$",DECIMAL_SEP:".",GROUP_SEP:",",PATTERNS:[{gSize:3,lgSize:3,maxFrac:3,minFrac:0,minInt:1,negPre:"-",negSuf:"",posPre:"",posSuf:""},{gSize:3,lgSize:3,maxFrac:2,
minFrac:2,minInt:1,negPre:"-\u00a4",negSuf:"",posPre:"\u00a4",posSuf:""}]},id:"en-us",localeID:"en_US",pluralCat:function(a,c){var e=a|0,f=c;void 0===f&&(f=Math.min(b(a),3));Math.pow(10,f);return 1==e&&0==f?"one":"other"}})}]),B(function(){we(u.document,Uc)}))})(window);!window.angular.$$csp().noInlineStyle&&window.angular.element(document.head).prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}</style>');
//# sourceMappingURL=angular.min.js.map
//}

//{ AngularJs Routes v1.6.6
	/*
 AngularJS v1.6.6
 (c) 2010-2017 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(J,d){'use strict';function A(d){k&&d.get("$route")}function B(t,u,g){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,f,b,c,m){function v(){l&&(g.cancel(l),l=null);n&&(n.$destroy(),n=null);p&&(l=g.leave(p),l.done(function(a){!1!==a&&(l=null)}),p=null)}function E(){var b=t.current&&t.current.locals;if(d.isDefined(b&&b.$template)){var b=a.$new(),c=t.current;p=m(b,function(b){g.enter(b,null,p||f).done(function(b){!1===b||!d.isDefined(w)||w&&!a.$eval(w)||u()});
v()});n=c.scope=b;n.$emit("$viewContentLoaded");n.$eval(k)}else v()}var n,p,l,w=b.autoscroll,k=b.onload||"";a.$on("$routeChangeSuccess",E);E()}}}function C(d,k,g){return{restrict:"ECA",priority:-400,link:function(a,f){var b=g.current,c=b.locals;f.html(c.$template);var m=d(f.contents());if(b.controller){c.$scope=a;var v=k(b.controller,c);b.controllerAs&&(a[b.controllerAs]=v);f.data("$ngControllerController",v);f.children().data("$ngControllerController",v)}a[b.resolveAs||"$resolve"]=c;m(a)}}}var x,
y,F,G,z=d.module("ngRoute",[]).info({angularVersion:"1.6.6"}).provider("$route",function(){function t(a,f){return d.extend(Object.create(a),f)}function u(a,d){var b=d.caseInsensitiveMatch,c={originalPath:a,regexp:a},g=c.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)(\*\?|[?*])?/g,function(a,b,d,c){a="?"===c||"*?"===c?"?":null;c="*"===c||"*?"===c?"*":null;g.push({name:d,optional:!!a});b=b||"";return""+(a?"":b)+"(?:"+(a?b:"")+(c&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([/$*])/g,
"\\$1");c.regexp=new RegExp("^"+a+"$",b?"i":"");return c}x=d.isArray;y=d.isObject;F=d.isDefined;G=d.noop;var g={};this.when=function(a,f){var b;b=void 0;if(x(f)){b=b||[];for(var c=0,m=f.length;c<m;c++)b[c]=f[c]}else if(y(f))for(c in b=b||{},f)if("$"!==c.charAt(0)||"$"!==c.charAt(1))b[c]=f[c];b=b||f;d.isUndefined(b.reloadOnSearch)&&(b.reloadOnSearch=!0);d.isUndefined(b.caseInsensitiveMatch)&&(b.caseInsensitiveMatch=this.caseInsensitiveMatch);g[a]=d.extend(b,a&&u(a,b));a&&(c="/"===a[a.length-1]?a.substr(0,
a.length-1):a+"/",g[c]=d.extend({redirectTo:a},u(c,b)));return this};this.caseInsensitiveMatch=!1;this.otherwise=function(a){"string"===typeof a&&(a={redirectTo:a});this.when(null,a);return this};k=!0;this.eagerInstantiationEnabled=function(a){return F(a)?(k=a,this):k};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$templateRequest","$sce","$browser",function(a,f,b,c,m,k,u,n){function p(e){var h=q.current;(y=(s=C())&&h&&s.$$route===h.$$route&&d.equals(s.pathParams,h.pathParams)&&
!s.reloadOnSearch&&!D)||!h&&!s||a.$broadcast("$routeChangeStart",s,h).defaultPrevented&&e&&e.preventDefault()}function l(){var e=q.current,h=s;if(y)e.params=h.params,d.copy(e.params,b),a.$broadcast("$routeUpdate",e);else if(h||e){D=!1;q.current=h;var H=c.resolve(h);n.$$incOutstandingRequestCount();H.then(w).then(z).then(function(c){return c&&H.then(A).then(function(c){h===q.current&&(h&&(h.locals=c,d.copy(h.params,b)),a.$broadcast("$routeChangeSuccess",h,e))})}).catch(function(b){h===q.current&&a.$broadcast("$routeChangeError",
h,e,b)}).finally(function(){n.$$completeOutstandingRequest(G)})}}function w(e){var a={route:e,hasRedirection:!1};if(e)if(e.redirectTo)if(d.isString(e.redirectTo))a.path=x(e.redirectTo,e.params),a.search=e.params,a.hasRedirection=!0;else{var b=f.path(),g=f.search();e=e.redirectTo(e.pathParams,b,g);d.isDefined(e)&&(a.url=e,a.hasRedirection=!0)}else if(e.resolveRedirectTo)return c.resolve(m.invoke(e.resolveRedirectTo)).then(function(e){d.isDefined(e)&&(a.url=e,a.hasRedirection=!0);return a});return a}
function z(a){var b=!0;if(a.route!==q.current)b=!1;else if(a.hasRedirection){var d=f.url(),c=a.url;c?f.url(c).replace():c=f.path(a.path).search(a.search).replace().url();c!==d&&(b=!1)}return b}function A(a){if(a){var b=d.extend({},a.resolve);d.forEach(b,function(a,e){b[e]=d.isString(a)?m.get(a):m.invoke(a,null,null,e)});a=B(a);d.isDefined(a)&&(b.$template=a);return c.all(b)}}function B(a){var b,c;d.isDefined(b=a.template)?d.isFunction(b)&&(b=b(a.params)):d.isDefined(c=a.templateUrl)&&(d.isFunction(c)&&
(c=c(a.params)),d.isDefined(c)&&(a.loadedTemplateUrl=u.valueOf(c),b=k(c)));return b}function C(){var a,b;d.forEach(g,function(c,g){var r;if(r=!b){var k=f.path();r=c.keys;var m={};if(c.regexp)if(k=c.regexp.exec(k)){for(var l=1,n=k.length;l<n;++l){var p=r[l-1],q=k[l];p&&q&&(m[p.name]=q)}r=m}else r=null;else r=null;r=a=r}r&&(b=t(c,{params:d.extend({},f.search(),a),pathParams:a}),b.$$route=c)});return b||g[null]&&t(g[null],{params:{},pathParams:{}})}function x(a,b){var c=[];d.forEach((a||"").split(":"),
function(a,d){if(0===d)c.push(a);else{var e=a.match(/(\w+)(?:[?*])?(.*)/),f=e[1];c.push(b[f]);c.push(e[2]||"");delete b[f]}});return c.join("")}var D=!1,s,y,q={routes:g,reload:function(){D=!0;var b={defaultPrevented:!1,preventDefault:function(){this.defaultPrevented=!0;D=!1}};a.$evalAsync(function(){p(b);b.defaultPrevented||l()})},updateParams:function(a){if(this.current&&this.current.$$route)a=d.extend({},this.current.params,a),f.path(x(this.current.$$route.originalPath,a)),f.search(a);else throw I("norout");
}};a.$on("$locationChangeStart",p);a.$on("$locationChangeSuccess",l);return q}]}).run(A),I=d.$$minErr("ngRoute"),k;A.$inject=["$injector"];z.provider("$routeParams",function(){this.$get=function(){return{}}});z.directive("ngView",B);z.directive("ngView",C);B.$inject=["$route","$anchorScroll","$animate"];C.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map
//}

//{ ng-grid
/***********************************************
* ng-grid JavaScript Library
* Authors: https://github.com/angular-ui/ng-grid/blob/master/README.md
* License: MIT (http://www.opensource.org/licenses/mit-license.php)
* Compiled At: 07/06/2013 13:50
***********************************************/
(function(window, $) {
'use strict';
// the # of rows we want to add to the top and bottom of the rendered grid rows
var EXCESS_ROWS = 6;
var SCROLL_THRESHOLD = 4;
var ASC = "asc";
// constant for sorting direction
var DESC = "desc";
// constant for sorting direction
var NG_FIELD = '_ng_field_';
var NG_DEPTH = '_ng_depth_';
var NG_HIDDEN = '_ng_hidden_';
var NG_COLUMN = '_ng_column_';
var CUSTOM_FILTERS = /CUSTOM_FILTERS/g;
var COL_FIELD = /COL_FIELD/g;
var DISPLAY_CELL_TEMPLATE = /DISPLAY_CELL_TEMPLATE/g;
var EDITABLE_CELL_TEMPLATE = /EDITABLE_CELL_TEMPLATE/g;
var TEMPLATE_REGEXP = /<.+>/;
window.ngGrid = {};
window.ngGrid.i18n = {};

// Declare app level module which depends on filters, and services
var ngGridServices = angular.module('ngGrid.services', []);
var ngGridDirectives = angular.module('ngGrid.directives', []);
var ngGridFilters = angular.module('ngGrid.filters', []);
// initialization of services into the main module
angular.module('ngGrid', ['ngGrid.services', 'ngGrid.directives', 'ngGrid.filters']);
//set event binding on the grid so we can select using the up/down keys
var ngMoveSelectionHandler = function($scope, elm, evt, grid) {
	if ($scope.selectionProvider.selectedItems === undefined) {
		return true;
	}

	var charCode = evt.which || evt.keyCode,
		newColumnIndex,
		lastInRow = false,
		firstInRow = false,
		rowIndex = $scope.selectionProvider.lastClickedRow === undefined ? 1 : $scope.selectionProvider.lastClickedRow.rowIndex,
		visibleCols = $scope.columns.filter(function(c) { return c.visible; }),
		pinnedCols = $scope.columns.filter(function(c) { return c.pinned; });

	if ($scope.col) {
		newColumnIndex = visibleCols.indexOf($scope.col);
	}

	if (charCode !== 37 && charCode !== 38 && charCode !== 39 && charCode !== 40 && charCode !== 9 && charCode !== 13) {
		return true;
	}

	if ($scope.enableCellSelection) {
		if (charCode === 9) { //tab key
			evt.preventDefault();
		}

		var focusedOnFirstColumn = $scope.showSelectionCheckbox ? $scope.col.index === 1 : $scope.col.index === 0;
		var focusedOnFirstVisibleColumns = $scope.$index === 1 || $scope.$index === 0;
		var focusedOnLastVisibleColumns = $scope.$index === ($scope.renderedColumns.length - 1) || $scope.$index === ($scope.renderedColumns.length - 2);
		var focusedOnLastColumn = visibleCols.indexOf($scope.col) === (visibleCols.length - 1);
		var focusedOnLastPinnedColumn = pinnedCols.indexOf($scope.col) === (pinnedCols.length - 1);

		if (charCode === 37 || charCode === 9 && evt.shiftKey) {
			var scrollTo = 0;

			if (!focusedOnFirstColumn) {
				newColumnIndex -= 1;
			}

			if (focusedOnFirstVisibleColumns) {
				if (focusedOnFirstColumn && charCode === 9 && evt.shiftKey){
					scrollTo = grid.$canvas.width();
					newColumnIndex = visibleCols.length - 1;
					firstInRow = true;
				}
				else {
					scrollTo = grid.$viewport.scrollLeft() - $scope.col.width;
				}
			}
			else if (pinnedCols.length > 0) {
				scrollTo = grid.$viewport.scrollLeft() - visibleCols[newColumnIndex].width;
			}

			grid.$viewport.scrollLeft(scrollTo);

		}
		else if (charCode === 39 || charCode ===  9 && !evt.shiftKey) {
			if (focusedOnLastVisibleColumns) {
				if (focusedOnLastColumn && charCode ===  9 && !evt.shiftKey) {
					grid.$viewport.scrollLeft(0);
					newColumnIndex = $scope.showSelectionCheckbox ? 1 : 0;
					lastInRow = true;
				}
				else {
					grid.$viewport.scrollLeft(grid.$viewport.scrollLeft() + $scope.col.width);
				}
			}
			else if (focusedOnLastPinnedColumn) {
				grid.$viewport.scrollLeft(0);
			}

			if (!focusedOnLastColumn) {
				newColumnIndex += 1;
			}
		}
	}

	var items;
	if ($scope.configGroups.length > 0) {
		items = grid.rowFactory.parsedData.filter(function (row) {
			return !row.isAggRow;
		});
	}
	else {
		items = grid.filteredRows;
	}

	var offset = 0;
	if (rowIndex !== 0 && (charCode === 38 || charCode === 13 && evt.shiftKey || charCode === 9 && evt.shiftKey && firstInRow)) { //arrow key up or shift enter or tab key and first item in row
		offset = -1;
	}
	else if (rowIndex !== items.length - 1 && (charCode === 40 || charCode === 13 && !evt.shiftKey || charCode === 9 && lastInRow)) {//arrow key down, enter, or tab key and last item in row?
		offset = 1;
	}

	if (offset) {
		var r = items[rowIndex + offset];
		if (r.beforeSelectionChange(r, evt)) {
			r.continueSelection(evt);
			$scope.$emit('ngGridEventDigestGridParent');

			if ($scope.selectionProvider.lastClickedRow.renderedRowIndex >= $scope.renderedRows.length - EXCESS_ROWS - 2) {
				grid.$viewport.scrollTop(grid.$viewport.scrollTop() + $scope.rowHeight);
			}
			else if ($scope.selectionProvider.lastClickedRow.renderedRowIndex <= EXCESS_ROWS + 2) {
				grid.$viewport.scrollTop(grid.$viewport.scrollTop() - $scope.rowHeight);
			}
	  }
	}

	if ($scope.enableCellSelection) {
		setTimeout(function(){
			$scope.domAccessProvider.focusCellElement($scope, $scope.renderedColumns.indexOf(visibleCols[newColumnIndex]));
		}, 3);
	}

	return false;
};

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(elt /*, from*/) {
		var len = this.length >>> 0;
		var from = Number(arguments[1]) || 0;
		from = (from < 0) ? Math.ceil(from) : Math.floor(from);
		if (from < 0) {
			from += len;
		}
		for (; from < len; from++) {
			if (from in this && this[from] === elt) {
				return from;
			}
		}
		return -1;
	};
}
if (!Array.prototype.filter) {
	Array.prototype.filter = function(fun /*, thisp */) {
		"use strict";
		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== "function") {
			throw new TypeError();
		}
		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i]; // in case fun mutates this
				if (fun.call(thisp, val, i, t)) {
					res.push(val);
				}
			}
		}
		return res;
	};
}
ngGridFilters.filter('checkmark', function() {
	return function(input) {
		return input ? '\u2714' : '\u2718';
	};
});
ngGridFilters.filter('ngColumns', function() {
	return function(input) {
		return input.filter(function(col) {
			return !col.isAggCol;
		});
	};
});
angular.module('ngGrid.services').factory('$domUtilityService',['$utilityService', function($utils) {
	var domUtilityService = {};
	var regexCache = {};
	var getWidths = function() {
		var $testContainer = $('<div></div>');
		$testContainer.appendTo('body');
		// 1. Run all the following measurements on startup!
		//measure Scroll Bars
		$testContainer.height(100).width(100).css("position", "absolute").css("overflow", "scroll");
		$testContainer.append('<div style="height: 400px; width: 400px;"></div>');
		domUtilityService.ScrollH = ($testContainer.height() - $testContainer[0].clientHeight);
		domUtilityService.ScrollW = ($testContainer.width() - $testContainer[0].clientWidth);
		$testContainer.empty();
		//clear styles
		$testContainer.attr('style', '');
		//measure letter sizes using a pretty typical font size and fat font-family
		$testContainer.append('<span style="font-family: Verdana, Helvetica, Sans-Serif; font-size: 14px;"><strong>M</strong></span>');
		domUtilityService.LetterW = $testContainer.children().first().width();
		$testContainer.remove();
	};
	domUtilityService.eventStorage = {};
	domUtilityService.AssignGridContainers = function($scope, rootEl, grid) {
		grid.$root = $(rootEl);
		//Headers
		grid.$topPanel = grid.$root.find(".ngTopPanel");
		grid.$groupPanel = grid.$root.find(".ngGroupPanel");
		grid.$headerContainer = grid.$topPanel.find(".ngHeaderContainer");
		$scope.$headerContainer = grid.$headerContainer;

		grid.$headerScroller = grid.$topPanel.find(".ngHeaderScroller");
		grid.$headers = grid.$headerScroller.children();
		//Viewport
		grid.$viewport = grid.$root.find(".ngViewport");
		//Canvas
		grid.$canvas = grid.$viewport.find(".ngCanvas");
		//Footers
		grid.$footerPanel = grid.$root.find(".ngFooterPanel");

		$scope.$watch(function () {
			return grid.$viewport.scrollLeft();
		}, function (newLeft) {
			return grid.$headerContainer.scrollLeft(newLeft);
		});
		domUtilityService.UpdateGridLayout($scope, grid);
	};
	domUtilityService.getRealWidth = function (obj) {
		var width = 0;
		var props = { visibility: "hidden", display: "block" };
		var hiddenParents = obj.parents().andSelf().not(':visible');
		$.swap(hiddenParents[0], props, function () {
			width = obj.outerWidth();
		});
		return width;
	};
	domUtilityService.UpdateGridLayout = function($scope, grid) {
		//catch this so we can return the viewer to their original scroll after the resize!
		var scrollTop = grid.$viewport.scrollTop();
		grid.elementDims.rootMaxW = grid.$root.width();
		if (grid.$root.is(':hidden')) {
			grid.elementDims.rootMaxW = domUtilityService.getRealWidth(grid.$root);
		}
		grid.elementDims.rootMaxH = grid.$root.height();
		//check to see if anything has changed
		grid.refreshDomSizes();
		$scope.adjustScrollTop(scrollTop, true); //ensure that the user stays scrolled where they were
	};
	domUtilityService.numberOfGrids = 0;
	domUtilityService.BuildStyles = function($scope, grid, digest) {
		var rowHeight = grid.config.rowHeight,
			$style = grid.$styleSheet,
			gridId = grid.gridId,
			css,
			cols = $scope.columns,
			sumWidth = 0;

		if (!$style) {
			$style = $('#' + gridId);
			if (!$style[0]) {
				$style = $("<style id='" + gridId + "' type='text/css' rel='stylesheet' />").appendTo(grid.$root);
			}
		}
		$style.empty();
		var trw = $scope.totalRowWidth();
		css = "." + gridId + " .ngCanvas { width: " + trw + "px; }" +
			"." + gridId + " .ngRow { width: " + trw + "px; }" +
			"." + gridId + " .ngCanvas { width: " + trw + "px; }" +
			"." + gridId + " .ngHeaderScroller { width: " + (trw + domUtilityService.ScrollH) + "px}";

		for (var i = 0; i < cols.length; i++) {
			var col = cols[i];
			if (col.visible !== false) {
				css += "." + gridId + " .col" + i + " { width: " + col.width + "px; left: " + sumWidth + "px; height: " + rowHeight + "px }" +
					"." + gridId + " .colt" + i + " { width: " + col.width + "px; }";
				sumWidth += col.width;
			}
		}

		if ($utils.isIe) { // IE
			$style[0].styleSheet.cssText = css;
		}

		else {
			$style[0].appendChild(document.createTextNode(css));
		}

		grid.$styleSheet = $style;

		$scope.adjustScrollLeft(grid.$viewport.scrollLeft());
		if (digest) {
			domUtilityService.digest($scope);
		}
	};
	domUtilityService.setColLeft = function(col, colLeft, grid) {
		if (grid.$styleSheet) {
			var regex = regexCache[col.index];
			if (!regex) {
				regex = regexCache[col.index] = new RegExp(".col" + col.index + " { width: [0-9]+px; left: [0-9]+px");
			}
			var str = grid.$styleSheet.html();
			var newStr = str.replace(regex, ".col" + col.index + " { width: " + col.width + "px; left: " + colLeft + "px");
			if ($utils.isIe) { // IE
				setTimeout(function() {
					grid.$styleSheet.html(newStr);
				});
			}
			else {
				grid.$styleSheet.html(newStr);
			}
		}
	};
	domUtilityService.setColLeft.immediate = 1;
	domUtilityService.RebuildGrid = function($scope, grid){
		domUtilityService.UpdateGridLayout($scope, grid);
		if (grid.config.maintainColumnRatios == null || grid.config.maintainColumnRatios) {
			grid.configureColumnWidths();
		}
		$scope.adjustScrollLeft(grid.$viewport.scrollLeft());
		domUtilityService.BuildStyles($scope, grid, true);
	};

	domUtilityService.digest = function($scope) {
		if (!$scope.$root.$$phase) {
			$scope.$digest();
		}
	};
	domUtilityService.ScrollH = 17; // default in IE, Chrome, & most browsers
	domUtilityService.ScrollW = 17; // default in IE, Chrome, & most browsers
	domUtilityService.LetterW = 10;
	getWidths();
	return domUtilityService;
}]);
angular.module('ngGrid.services').factory('$sortService', ['$parse', function($parse) {
	var sortService = {};
	sortService.colSortFnCache = {}; // cache of sorting functions. Once we create them, we don't want to keep re-doing it
	// this takes an piece of data from the cell and tries to determine its type and what sorting
	// function to use for it
	// @item - the cell data
	sortService.guessSortFn = function(item) {
		var itemType = typeof(item);
		//check for numbers and booleans
		switch (itemType) {
			case "number":
				return sortService.sortNumber;
			case "boolean":
				return sortService.sortBool;
			case "string":
				// if number string return number string sort fn. else return the str
				return item.match(/^[-+]?[Ã‚Â£$Ã‚Â¤]?[\d,.]+%?$/) ? sortService.sortNumberStr : sortService.sortAlpha;
			default:
				//check if the item is a valid Date
				if (Object.prototype.toString.call(item) === '[object Date]') {
					return sortService.sortDate;
				}
				else {
					//finally just sort the basic sort...
					return sortService.basicSort;
				}
		}
	};
	//#region Sorting Functions
	sortService.basicSort = function(a, b) {
		if (a === b) {
			return 0;
		}
		if (a < b) {
			return -1;
		}
		return 1;
	};
	sortService.sortNumber = function(a, b) {
		return a - b;
	};
	sortService.sortNumberStr = function(a, b) {
		var numA, numB, badA = false, badB = false;
		numA = parseFloat(a.replace(/[^0-9.-]/g, ''));
		if (isNaN(numA)) {
			badA = true;
		}
		numB = parseFloat(b.replace(/[^0-9.-]/g, ''));
		if (isNaN(numB)) {
			badB = true;
		}
		// we want bad ones to get pushed to the bottom... which effectively is "greater than"
		if (badA && badB) {
			return 0;
		}
		if (badA) {
			return 1;
		}
		if (badB) {
			return -1;
		}
		return numA - numB;
	};
	sortService.sortAlpha = function(a, b) {
		var strA = a.toLowerCase(),
			strB = b.toLowerCase();
		return strA === strB ? 0 : (strA < strB ? -1 : 1);
	};
	sortService.sortDate = function(a, b) {
		var timeA = a.getTime(),
			timeB = b.getTime();
		return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
	};
	sortService.sortBool = function(a, b) {
		if (a && b) {
			return 0;
		}
		if (!a && !b) {
			return 0;
		} else {
			return a ? 1 : -1;
		}
	};
	//#endregion
	// the core sorting logic trigger
	sortService.sortData = function(sortInfo, data /*datasource*/) {
		// first make sure we are even supposed to do work
		if (!data || !sortInfo) {
			return;
		}
		var l = sortInfo.fields.length,
			order = sortInfo.fields,
			col,
			direction,
			// IE9 HACK.... omg, I can't reference data array within the sort fn below. has to be a separate reference....!!!!
			d = data.slice(0);
		//now actually sort the data
		data.sort(function (itemA, itemB) {
			var tem = 0,
				indx = 0,
				sortFn;
			while (tem === 0 && indx < l) {
				// grab the metadata for the rest of the logic
				col = sortInfo.columns[indx];
				direction = sortInfo.directions[indx];
				sortFn = sortService.getSortFn(col, d);

				var propA = $parse(order[indx])(itemA);
				var propB = $parse(order[indx])(itemB);
				// we want to allow zero values to be evaluated in the sort function
				if ((!propA && propA !== 0) || (!propB && propB !== 0)) {
					// we want to force nulls and such to the bottom when we sort... which effectively is "greater than"
					if (!propB && !propA) {
						tem = 0;
					}
					else if (!propA) {
						tem = 1;
					}
					else if (!propB) {
						tem = -1;
					}
				}
				else {
					tem = sortFn(propA, propB);
				}
				indx++;
			}
			//made it this far, we don't have to worry about null & undefined
			if (direction === ASC) {
				return tem;
			} else {
				return 0 - tem;
			}
		});
	};
	sortService.Sort = function(sortInfo, data) {
		if (sortService.isSorting) {
			return;
		}
		sortService.isSorting = true;
		sortService.sortData(sortInfo, data);
		sortService.isSorting = false;
	};
	sortService.getSortFn = function(col, data) {
		var sortFn, item;
		//see if we already figured out what to use to sort the column
		if (sortService.colSortFnCache[col.field]) {
			sortFn = sortService.colSortFnCache[col.field];
		}
		else if (col.sortingAlgorithm !== undefined) {
			sortFn = col.sortingAlgorithm;
			sortService.colSortFnCache[col.field] = col.sortingAlgorithm;
		}
		else { // try and guess what sort function to use
			item = data[0];
			if (!item) {
				return sortFn;
			}
			sortFn = sortService.guessSortFn($parse(col.field)(item));
			//cache it
			if (sortFn) {
				sortService.colSortFnCache[col.field] = sortFn;
			} else {
				// we assign the alpha sort because anything that is null/undefined will never get passed to
				// the actual sorting function. It will get caught in our null check and returned to be sorted
				// down to the bottom
				sortFn = sortService.sortAlpha;
			}
		}
		return sortFn;
	};
	return sortService;
}]);

angular.module('ngGrid.services').factory('$utilityService', ['$parse', function ($parse) {
	var funcNameRegex = /function (.{1,})\(/;
	var utils = {
		visualLength: function(node) {
			var elem = document.getElementById('testDataLength');
			if (!elem) {
				elem = document.createElement('SPAN');
				elem.id = "testDataLength";
				elem.style.visibility = "hidden";
				document.body.appendChild(elem);
			}
			$(elem).css('font', $(node).css('font'));
			$(elem).css('font-size', $(node).css('font-size'));
			$(elem).css('font-family', $(node).css('font-family'));
			elem.innerHTML = $(node).text();
			return elem.offsetWidth;
		},
		forIn: function(obj, action) {
			for (var prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					action(obj[prop], prop);
				}
			}
		},
		evalProperty: function (entity, path) {
			return $parse(path)(entity);
		},
		endsWith: function(str, suffix) {
			if (!str || !suffix || typeof str !== "string") {
				return false;
			}
			return str.indexOf(suffix, str.length - suffix.length) !== -1;
		},
		isNullOrUndefined: function(obj) {
			if (obj === undefined || obj === null) {
				return true;
			}
			return false;
		},
		getElementsByClassName: function(cl) {
			var retnode = [];
			var myclass = new RegExp('\\b' + cl + '\\b');
			var elem = document.getElementsByTagName('*');
			for (var i = 0; i < elem.length; i++) {
				var classes = elem[i].className;
				if (myclass.test(classes)) {
					retnode.push(elem[i]);
				}
			}
			return retnode;
		},
		newId: (function() {
			var seedId = new Date().getTime();
			return function() {
				return seedId += 1;
			};
		})(),
		seti18n: function($scope, language) {
			var $langPack = window.ngGrid.i18n[language];
			for (var label in $langPack) {
				$scope.i18n[label] = $langPack[label];
			}
		},
		getInstanceType: function (o) {
			var results = (funcNameRegex).exec(o.constructor.toString());
			if (results && results.length > 1) {
				var instanceType = results[1].replace(/^\s+|\s+$/g, ""); // Trim surrounding whitespace; IE appears to add a space at the end
				return instanceType;
			}
			else {
				return "";
			}
		},
		// Detect IE versions for bug workarounds (uses IE conditionals, not UA string, for robustness)
		// Note that, since IE 10 does not support conditional comments, the following logic only detects IE < 10.
		// Currently this is by design, since IE 10+ behaves correctly when treated as a standard browser.
		// If there is a future need to detect specific versions of IE10+, we will amend this.
		ieVersion: (function() {
			var version = 3, div = document.createElement('div'), iElems = div.getElementsByTagName('i');

			// Keep constructing conditional HTML blocks until we hit one that resolves to an empty fragment
			do{
				div.innerHTML = '<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->';
			}while(iElems[0]);
			return version > 4 ? version : undefined;
		})()
	};

	$.extend(utils, {
		isIe: (function() {
			return utils.ieVersion !== undefined;
		})()
	});
	return utils;
}]);

var ngAggregate = function (aggEntity, rowFactory, rowHeight, groupInitState) {
	this.rowIndex = 0;
	this.offsetTop = this.rowIndex * rowHeight;
	this.entity = aggEntity;
	this.label = aggEntity.gLabel;
	this.field = aggEntity.gField;
	this.depth = aggEntity.gDepth;
	this.parent = aggEntity.parent;
	this.children = aggEntity.children;
	this.aggChildren = aggEntity.aggChildren;
	this.aggIndex = aggEntity.aggIndex;
	this.collapsed = groupInitState;
	this.groupInitState = groupInitState;
	this.rowFactory = rowFactory;
	this.rowHeight = rowHeight;
	this.isAggRow = true;
	this.offsetLeft = aggEntity.gDepth * 25;
	this.aggLabelFilter = aggEntity.aggLabelFilter;
};

ngAggregate.prototype.toggleExpand = function () {
	this.collapsed = this.collapsed ? false : true;
	if (this.orig) {
		this.orig.collapsed = this.collapsed;
	}
	this.notifyChildren();
};
ngAggregate.prototype.setExpand = function (state) {
	this.collapsed = state;
	this.notifyChildren();
};
ngAggregate.prototype.notifyChildren = function () {
	var longest = Math.max(this.rowFactory.aggCache.length, this.children.length);
	for (var i = 0; i < longest; i++) {
		if (this.aggChildren[i]) {
			this.aggChildren[i].entity[NG_HIDDEN] = this.collapsed;
			if (this.collapsed) {
				this.aggChildren[i].setExpand(this.collapsed);
			}
		}
		if (this.children[i]) {
			this.children[i][NG_HIDDEN] = this.collapsed;
		}
		if (i > this.aggIndex && this.rowFactory.aggCache[i]) {
			var agg = this.rowFactory.aggCache[i];
			var offset = (30 * this.children.length);
			agg.offsetTop = this.collapsed ? agg.offsetTop - offset : agg.offsetTop + offset;
		}
	}
	this.rowFactory.renderedChange();
};
ngAggregate.prototype.aggClass = function () {
	return this.collapsed ? "ngAggArrowCollapsed" : "ngAggArrowExpanded";
};
ngAggregate.prototype.totalChildren = function () {
	if (this.aggChildren.length > 0) {
		var i = 0;
		var recurse = function (cur) {
			if (cur.aggChildren.length > 0) {
				angular.forEach(cur.aggChildren, function (a) {
					recurse(a);
				});
			} else {
				i += cur.children.length;
			}
		};
		recurse(this);
		return i;
	} else {
		return this.children.length;
	}
};
ngAggregate.prototype.copy = function () {
	var ret = new ngAggregate(this.entity, this.rowFactory, this.rowHeight, this.groupInitState);
	ret.orig = this;
	return ret;
};
var ngColumn = function (config, $scope, grid, domUtilityService, $templateCache, $utils) {
	var self = this,
		colDef = config.colDef,
		delay = 500,
		clicks = 0,
		timer = null;
	self.colDef = config.colDef;
	self.width = colDef.width;
	self.groupIndex = 0;
	self.isGroupedBy = false;
	self.minWidth = !colDef.minWidth ? 50 : colDef.minWidth;
	self.maxWidth = !colDef.maxWidth ? 9000 : colDef.maxWidth;

	// TODO: Use the column's definition for enabling cell editing
	// self.enableCellEdit = config.enableCellEdit || colDef.enableCellEdit;
	self.enableCellEdit = colDef.enableCellEdit !== undefined ? colDef.enableCellEdit : (config.enableCellEdit || config.enableCellEditOnFocus);

	self.headerRowHeight = config.headerRowHeight;

	// Use colDef.displayName as long as it's not undefined, otherwise default to the field name
	self.displayName = (colDef.displayName === undefined) ? colDef.field : colDef.displayName;

	self.index = config.index;
	self.isAggCol = config.isAggCol;
	self.cellClass = colDef.cellClass;
	self.sortPriority = undefined;
	self.cellFilter = colDef.cellFilter ? colDef.cellFilter : "";
	self.field = colDef.field;
	self.aggLabelFilter = colDef.cellFilter || colDef.aggLabelFilter;
	self.visible = $utils.isNullOrUndefined(colDef.visible) || colDef.visible;
	self.sortable = false;
	self.resizable = false;
	self.pinnable = false;
	self.pinned = (config.enablePinning && colDef.pinned);
	self.originalIndex = config.originalIndex == null ? self.index : config.originalIndex;
	self.groupable = $utils.isNullOrUndefined(colDef.groupable) || colDef.groupable;
	if (config.enableSort) {
		self.sortable = $utils.isNullOrUndefined(colDef.sortable) || colDef.sortable;
	}
	if (config.enableResize) {
		self.resizable = $utils.isNullOrUndefined(colDef.resizable) || colDef.resizable;
	}
	if (config.enablePinning) {
		self.pinnable = $utils.isNullOrUndefined(colDef.pinnable) || colDef.pinnable;
	}
	self.sortDirection = undefined;
	self.sortingAlgorithm = colDef.sortFn;
	self.headerClass = colDef.headerClass;
	self.cursor = self.sortable ? 'pointer' : 'default';
	self.headerCellTemplate = colDef.headerCellTemplate || $templateCache.get('headerCellTemplate.html');
	self.cellTemplate = colDef.cellTemplate || $templateCache.get('cellTemplate.html').replace(CUSTOM_FILTERS, self.cellFilter ? "|" + self.cellFilter : "");
	if(self.enableCellEdit) {
		self.cellEditTemplate = $templateCache.get('cellEditTemplate.html');
		self.editableCellTemplate = colDef.editableCellTemplate || $templateCache.get('editableCellTemplate.html');
	}
	if (colDef.cellTemplate && !TEMPLATE_REGEXP.test(colDef.cellTemplate)) {
		self.cellTemplate = $.ajax({
			type: "GET",
			url: colDef.cellTemplate,
			async: false
		}).responseText;
	}
	if (self.enableCellEdit && colDef.editableCellTemplate && !TEMPLATE_REGEXP.test(colDef.editableCellTemplate)) {
		self.editableCellTemplate = $.ajax({
			type: "GET",
			url: colDef.editableCellTemplate,
			async: false
		}).responseText;
	}
	if (colDef.headerCellTemplate && !TEMPLATE_REGEXP.test(colDef.headerCellTemplate)) {
		self.headerCellTemplate = $.ajax({
			type: "GET",
			url: colDef.headerCellTemplate,
			async: false
		}).responseText;
	}
	self.colIndex = function () {
		var classes = self.pinned ? "pinned " : "";
		classes += "col" + self.index + " colt" + self.index;
		if (self.cellClass) {
			classes += " " + self.cellClass;
		}
		return classes;
	};
	self.groupedByClass = function() {
		return self.isGroupedBy ? "ngGroupedByIcon" : "ngGroupIcon";
	};
	self.toggleVisible = function() {
		self.visible = !self.visible;
	};
	self.showSortButtonUp = function() {
		return self.sortable ? self.sortDirection === DESC : self.sortable;
	};
	self.showSortButtonDown = function() {
		return self.sortable ? self.sortDirection === ASC : self.sortable;
	};
	self.noSortVisible = function() {
		return !self.sortDirection;
	};
	self.sort = function(evt) {
		if (!self.sortable) {
			return true; // column sorting is disabled, do nothing
		}
		var dir = self.sortDirection === ASC ? DESC : ASC;
		self.sortDirection = dir;
		config.sortCallback(self, evt);
		return false;
	};
	self.gripClick = function() {
		clicks++; //count clicks
		if (clicks === 1) {
			timer = setTimeout(function() {
				//Here you can add a single click action.
				clicks = 0; //after action performed, reset counter
			}, delay);
		} else {
			clearTimeout(timer); //prevent single-click action
			config.resizeOnDataCallback(self); //perform double-click action
			clicks = 0; //after action performed, reset counter
		}
	};
	self.gripOnMouseDown = function(event) {
		$scope.isColumnResizing = true;
		if (event.ctrlKey && !self.pinned) {
			self.toggleVisible();
			domUtilityService.BuildStyles($scope, grid);
			return true;
		}
		event.target.parentElement.style.cursor = 'col-resize';
		self.startMousePosition = event.clientX;
		self.origWidth = self.width;
		$(document).mousemove(self.onMouseMove);
		$(document).mouseup(self.gripOnMouseUp);
		return false;
	};
	self.onMouseMove = function(event) {
		var diff = event.clientX - self.startMousePosition;
		var newWidth = diff + self.origWidth;
		self.width = (newWidth < self.minWidth ? self.minWidth : (newWidth > self.maxWidth ? self.maxWidth : newWidth));
		$scope.hasUserChangedGridColumnWidths = true;
		domUtilityService.BuildStyles($scope, grid);
		return false;
	};
	self.gripOnMouseUp = function (event) {
		$(document).off('mousemove', self.onMouseMove);
		$(document).off('mouseup', self.gripOnMouseUp);
		event.target.parentElement.style.cursor = 'default';
		domUtilityService.digest($scope);
		$scope.isColumnResizing = false;
		return false;
	};
	self.copy = function() {
		var ret = new ngColumn(config, $scope, grid, domUtilityService, $templateCache);
		ret.isClone = true;
		ret.orig = self;
		return ret;
	};
	self.setVars = function (fromCol) {
		self.orig = fromCol;
		self.width = fromCol.width;
		self.groupIndex = fromCol.groupIndex;
		self.isGroupedBy = fromCol.isGroupedBy;
		self.displayName = fromCol.displayName;
		self.index = fromCol.index;
		self.isAggCol = fromCol.isAggCol;
		self.cellClass = fromCol.cellClass;
		self.cellFilter = fromCol.cellFilter;
		self.field = fromCol.field;
		self.aggLabelFilter = fromCol.aggLabelFilter;
		self.visible = fromCol.visible;
		self.sortable = fromCol.sortable;
		self.resizable = fromCol.resizable;
		self.pinnable = fromCol.pinnable;
		self.pinned = fromCol.pinned;
		self.originalIndex = fromCol.originalIndex;
		self.sortDirection = fromCol.sortDirection;
		self.sortingAlgorithm = fromCol.sortingAlgorithm;
		self.headerClass = fromCol.headerClass;
		self.headerCellTemplate = fromCol.headerCellTemplate;
		self.cellTemplate = fromCol.cellTemplate;
		self.cellEditTemplate = fromCol.cellEditTemplate;
	};
};

var ngDimension = function (options) {
	this.outerHeight = null;
	this.outerWidth = null;
	$.extend(this, options);
};
var ngDomAccessProvider = function (grid) {
	this.previousColumn = null;
	this.grid = grid;

};

ngDomAccessProvider.prototype.changeUserSelect = function (elm, value) {
	elm.css({
		'-webkit-touch-callout': value,
		'-webkit-user-select': value,
		'-khtml-user-select': value,
		'-moz-user-select': value === 'none' ? '-moz-none' : value,
		'-ms-user-select': value,
		'user-select': value
	});
};
ngDomAccessProvider.prototype.focusCellElement = function ($scope, index) {
	if ($scope.selectionProvider.lastClickedRow) {
		var columnIndex = index !== undefined ? index : this.previousColumn;
		var elm = $scope.selectionProvider.lastClickedRow.clone ? $scope.selectionProvider.lastClickedRow.clone.elm : $scope.selectionProvider.lastClickedRow.elm;
		if (columnIndex !== undefined && elm) {
			var columns = angular.element(elm[0].children).filter(function () { return this.nodeType !== 8; }); //Remove html comments for IE8
			var i = Math.max(Math.min($scope.renderedColumns.length - 1, columnIndex), 0);
			if (this.grid.config.showSelectionCheckbox && angular.element(columns[i]).scope() && angular.element(columns[i]).scope().col.index === 0) {
				i = 1; //don't want to focus on checkbox
			}
			if (columns[i]) {
				columns[i].children[1].children[0].focus();
			}
			this.previousColumn = columnIndex;
		}
	}
};
ngDomAccessProvider.prototype.selectionHandlers = function ($scope, elm) {
	var doingKeyDown = false;
	var self = this;
	elm.bind('keydown', function (evt) {
		if (evt.keyCode === 16) { //shift key
			self.changeUserSelect(elm, 'none', evt);
			return true;
		} else if (!doingKeyDown) {
			doingKeyDown = true;
			var ret = ngMoveSelectionHandler($scope, elm, evt, self.grid);
			doingKeyDown = false;
			return ret;
		}
		return true;
	});
	elm.bind('keyup', function (evt) {
		if (evt.keyCode === 16) { //shift key
			self.changeUserSelect(elm, 'text', evt);
		}
		return true;
	});
};
var ngEventProvider = function (grid, $scope, domUtilityService, $timeout) {
	var self = this;
	// The init method gets called during the ng-grid directive execution.
	self.colToMove = undefined;
	self.groupToMove = undefined;
	self.assignEvents = function() {
		// Here we set the onmousedown event handler to the header container.
		if (grid.config.jqueryUIDraggable && !grid.config.enablePinning) {
			grid.$groupPanel.droppable({
				addClasses: false,
				drop: function(event) {
					self.onGroupDrop(event);
				}
			});
		} else {
			grid.$groupPanel.on('mousedown', self.onGroupMouseDown).on('dragover', self.dragOver).on('drop', self.onGroupDrop);
			grid.$headerScroller.on('mousedown', self.onHeaderMouseDown).on('dragover', self.dragOver);
			if (grid.config.enableColumnReordering && !grid.config.enablePinning) {
				grid.$headerScroller.on('drop', self.onHeaderDrop);
			}
		}
		$scope.$watch('renderedColumns', function() {
			$timeout(self.setDraggables);
		});
	};
	self.dragStart = function(evt){
	  //FireFox requires there to be dataTransfer if you want to drag and drop.
	  evt.dataTransfer.setData('text', ''); //cannot be empty string
	};
	self.dragOver = function(evt) {
		evt.preventDefault();
	};
	//For JQueryUI
	self.setDraggables = function() {
		if (!grid.config.jqueryUIDraggable) {
			//Fix for FireFox. Instead of using jQuery on('dragstart', function) on find, we have to use addEventListeners for each column.
			var columns = grid.$root.find('.ngHeaderSortColumn'); //have to iterate if using addEventListener
			angular.forEach(columns, function(col){
				if(col.className && col.className.indexOf("ngHeaderSortColumn") !== -1){
					col.setAttribute('draggable', 'true');
					//jQuery 'on' function doesn't have  dataTransfer as part of event in handler unless added to event props, which is not recommended
					//See more here: http://api.jquery.com/category/events/event-object/
					if (col.addEventListener) { //IE8 doesn't have drag drop or event listeners
						col.addEventListener('dragstart', self.dragStart);
					}
				}
			});
			if (navigator.userAgent.indexOf("MSIE") !== -1){
				//call native IE dragDrop() to start dragging
				grid.$root.find('.ngHeaderSortColumn').bind('selectstart', function () {
					this.dragDrop();
					return false;
				});
			}
		} else {
			grid.$root.find('.ngHeaderSortColumn').draggable({
				helper: 'clone',
				appendTo: 'body',
				stack: 'div',
				addClasses: false,
				start: function(event) {
					self.onHeaderMouseDown(event);
				}
			}).droppable({
				drop: function(event) {
					self.onHeaderDrop(event);
				}
			});
		}
	};
	self.onGroupMouseDown = function(event) {
		var groupItem = $(event.target);
		// Get the scope from the header container
		if (groupItem[0].className !== 'ngRemoveGroup') {
			var groupItemScope = angular.element(groupItem).scope();
			if (groupItemScope) {
				// set draggable events
				if (!grid.config.jqueryUIDraggable) {
					groupItem.attr('draggable', 'true');
					if(this.addEventListener){//IE8 doesn't have drag drop or event listeners
						this.addEventListener('dragstart', self.dragStart);
					}
					if (navigator.userAgent.indexOf("MSIE") !== -1){
						//call native IE dragDrop() to start dragging
						groupItem.bind('selectstart', function () {
							this.dragDrop();
							return false;
						});
					}
				}
				// Save the column for later.
				self.groupToMove = { header: groupItem, groupName: groupItemScope.group, index: groupItemScope.$index };
			}
		} else {
			self.groupToMove = undefined;
		}
	};
	self.onGroupDrop = function(event) {
		event.stopPropagation();
		// clear out the colToMove object
		var groupContainer;
		var groupScope;
		if (self.groupToMove) {
			// Get the closest header to where we dropped
			groupContainer = $(event.target).closest('.ngGroupElement'); // Get the scope from the header.
			if (groupContainer.context.className === 'ngGroupPanel') {
				$scope.configGroups.splice(self.groupToMove.index, 1);
				$scope.configGroups.push(self.groupToMove.groupName);
			} else {
				groupScope = angular.element(groupContainer).scope();
				if (groupScope) {
					// If we have the same column, do nothing.
					if (self.groupToMove.index !== groupScope.$index) {
						// Splice the columns
						$scope.configGroups.splice(self.groupToMove.index, 1);
						$scope.configGroups.splice(groupScope.$index, 0, self.groupToMove.groupName);
					}
				}
			}
			self.groupToMove = undefined;
			grid.fixGroupIndexes();
		} else if (self.colToMove) {
			if ($scope.configGroups.indexOf(self.colToMove.col) === -1) {
				groupContainer = $(event.target).closest('.ngGroupElement'); // Get the scope from the header.
				if (groupContainer.context.className === 'ngGroupPanel' || groupContainer.context.className === 'ngGroupPanelDescription ng-binding') {
					$scope.groupBy(self.colToMove.col);
				} else {
					groupScope = angular.element(groupContainer).scope();
					if (groupScope) {
						// Splice the columns
						$scope.removeGroup(groupScope.$index);
					}
				}
			}
			self.colToMove = undefined;
		}
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};
	//Header functions
	self.onHeaderMouseDown = function(event) {
		// Get the closest header container from where we clicked.
		var headerContainer = $(event.target).closest('.ngHeaderSortColumn');
		// Get the scope from the header container
		var headerScope = angular.element(headerContainer).scope();
		if (headerScope) {
			// Save the column for later.
			self.colToMove = { header: headerContainer, col: headerScope.col };
		}
	};
	self.onHeaderDrop = function(event) {
		if (!self.colToMove || self.colToMove.col.pinned) {
			return;
		}
		// Get the closest header to where we dropped
		var headerContainer = $(event.target).closest('.ngHeaderSortColumn');
		// Get the scope from the header.
		var headerScope = angular.element(headerContainer).scope();
		if (headerScope) {
			// If we have the same column, do nothing.
			if (self.colToMove.col === headerScope.col) {
				return;
			}
			// Splice the columns
			$scope.columns.splice(self.colToMove.col.index, 1);
			$scope.columns.splice(headerScope.col.index, 0, self.colToMove.col);
			grid.fixColumnIndexes();
			// clear out the colToMove object
			self.colToMove = undefined;
			domUtilityService.digest($scope);
		}
	};

	self.assignGridEventHandlers = function() {
		//Chrome and firefox both need a tab index so the grid can recieve focus.
		//need to give the grid a tabindex if it doesn't already have one so
		//we'll just give it a tab index of the corresponding gridcache index
		//that way we'll get the same result every time it is run.
		//configurable within the options.
		if (grid.config.tabIndex === -1) {
			grid.$viewport.attr('tabIndex', domUtilityService.numberOfGrids);
			domUtilityService.numberOfGrids++;
		} else {
			grid.$viewport.attr('tabIndex', grid.config.tabIndex);
		}
		// resize on window resize
		var windowThrottle;
		$(window).resize(function(){
			clearTimeout(windowThrottle);
			windowThrottle = setTimeout(function() {
				//in function for IE8 compatibility
				domUtilityService.RebuildGrid($scope,grid);
			}, 100);
		});
		// resize on parent resize as well.
		var parentThrottle;
		$(grid.$root.parent()).on('resize', function() {
			clearTimeout(parentThrottle);
			parentThrottle = setTimeout(function() {
				//in function for IE8 compatibility
				domUtilityService.RebuildGrid($scope,grid);
			}, 100);
		});
	};
	// In this example we want to assign grid events.
	self.assignGridEventHandlers();
	self.assignEvents();
};

var ngFooter = function ($scope, grid) {
	$scope.maxRows = function () {
		var ret = Math.max($scope.totalServerItems, grid.data.length);
		return ret;
	};

	$scope.multiSelect = (grid.config.enableRowSelection && grid.config.multiSelect);
	$scope.selectedItemCount = grid.selectedItemCount;
	$scope.maxPages = function () {
		return Math.ceil($scope.maxRows() / $scope.pagingOptions.pageSize);
	};

	$scope.pageForward = function() {
		var page = $scope.pagingOptions.currentPage;
		if ($scope.totalServerItems > 0) {
			$scope.pagingOptions.currentPage = Math.min(page + 1, $scope.maxPages());
		} else {
			$scope.pagingOptions.currentPage++;
		}
	};

	$scope.pageBackward = function() {
		var page = $scope.pagingOptions.currentPage;
		$scope.pagingOptions.currentPage = Math.max(page - 1, 1);
	};

	$scope.pageToFirst = function() {
		$scope.pagingOptions.currentPage = 1;
	};

	$scope.pageToLast = function() {
		var maxPages = $scope.maxPages();
		$scope.pagingOptions.currentPage = maxPages;
	};

	$scope.cantPageForward = function() {
		var curPage = $scope.pagingOptions.currentPage;
		var maxPages = $scope.maxPages();
		if ($scope.totalServerItems > 0) {
			return curPage >= maxPages;
		} else {
			return grid.data.length < 1;
		}

	};
	$scope.cantPageToLast = function() {
		if ($scope.totalServerItems > 0) {
			return $scope.cantPageForward();
		} else {
			return true;
		}
	};

	$scope.cantPageBackward = function() {
		var curPage = $scope.pagingOptions.currentPage;
		return curPage <= 1;
	};
};
/// <reference path="footer.js" />
/// <reference path="../services/SortService.js" />
/// <reference path="../../lib/jquery-1.8.2.min" />
var ngGrid = function ($scope, options, sortService, domUtilityService, $filter, $templateCache, $utils, $timeout, $parse, $http, $q) {
	var defaults = {
		//Define an aggregate template to customize the rows when grouped. See github wiki for more details.
		aggregateTemplate: undefined,

		//Callback for when you want to validate something after selection.
		afterSelectionChange: function() {
		},

		/* Callback if you want to inspect something before selection,
		return false if you want to cancel the selection. return true otherwise.
		If you need to wait for an async call to proceed with selection you can
		use rowItem.changeSelection(event) method after returning false initially.
		Note: when shift+ Selecting multiple items in the grid this will only get called
		once and the rowItem will be an array of items that are queued to be selected. */
		beforeSelectionChange: function() {
			return true;
		},

		//checkbox templates.
		checkboxCellTemplate: undefined,
		checkboxHeaderTemplate: undefined,

		//definitions of columns as an array [], if not defines columns are auto-generated. See github wiki for more details.
		columnDefs: undefined,

		//*Data being displayed in the grid. Each item in the array is mapped to a row being displayed.
		data: [],

		//Data updated callback, fires every time the data is modified from outside the grid.
		dataUpdated: function() {
		},

		//Enables cell editing.
		enableCellEdit: false,

		//Enables cell editing on focus
		enableCellEditOnFocus: false,

		//Enables cell selection.
		enableCellSelection: false,

		//Enable or disable resizing of columns
		enableColumnResize: false,

		//Enable or disable reordering of columns
		enableColumnReordering: false,

		//Enable or disable HEAVY column virtualization. This turns off selection checkboxes and column pinning and is designed for spreadsheet-like data.
		enableColumnHeavyVirt: false,

		//Enables the server-side paging feature
		enablePaging: false,

		//Enable column pinning
		enablePinning: false,

		//To be able to have selectable rows in grid.
		enableRowSelection: true,

		//Enables or disables sorting in grid.
		enableSorting: true,

		//Enables or disables text highlighting in grid by adding the "unselectable" class (See CSS file)
		enableHighlighting: false,

		// string list of properties to exclude when auto-generating columns.
		excludeProperties: [],

		/* filterOptions -
		filterText: The text bound to the built-in search box.
		useExternalFilter: Bypass internal filtering if you want to roll your own filtering mechanism but want to use builtin search box.
		*/
		filterOptions: {
			filterText: "",
			useExternalFilter: false
		},

		//Defining the height of the footer in pixels.
		footerRowHeight: 55,

		// the template for the column menu and filter, including the button.
		footerTemplate: undefined,

		//Initial fields to group data by. Array of field names, not displayName.
		groups: [],

		// set the initial state of aggreagate grouping. "true" means they will be collapsed when grouping changes, "false" means they will be expanded by default.
		groupsCollapsedByDefault: true,

		//The height of the header row in pixels.
		headerRowHeight: 30,

		//Define a header row template for further customization. See github wiki for more details.
		headerRowTemplate: undefined,

		/*Enables the use of jquery UI reaggable/droppable plugin. requires jqueryUI to work if enabled.
		Useful if you want drag + drop but your users insist on crappy browsers. */
		jqueryUIDraggable: false,

		//Enable the use jqueryUIThemes
		jqueryUITheme: false,

		//Prevent unselections when in single selection mode.
		keepLastSelected: true,

		/*Maintains the column widths while resizing.
		Defaults to true when using *'s or undefined widths. Can be ovverriden by setting to false.*/
		maintainColumnRatios: undefined,

		// the template for the column menu and filter, including the button.
		menuTemplate: undefined,

		//Set this to false if you only want one item selected at a time
		multiSelect: true,

		// pagingOptions -
		pagingOptions: {
			// pageSizes: list of available page sizes.
			pageSizes: [250, 500, 1000],
			//pageSize: currently selected page size.
			pageSize: 250,
			//currentPage: the uhm... current page.
			currentPage: 1
		},

		//the selection checkbox is pinned to the left side of the viewport or not.
		pinSelectionCheckbox: false,

		//Array of plugin functions to register in ng-grid
		plugins: [],

		//User defined unique ID field that allows for better handling of selections and for server-side paging
		primaryKey: undefined,

		//Row height of rows in grid.
		rowHeight: 30,

		//Define a row template to customize output. See github wiki for more details.
		rowTemplate: undefined,

		//all of the items selected in the grid. In single select mode there will only be one item in the array.
		selectedItems: [],

		//Disable row selections by clicking on the row and only when the checkbox is clicked.
		selectWithCheckboxOnly: false,

		/*Enables menu to choose which columns to display and group by.
		If both showColumnMenu and showFilter are false the menu button will not display.*/
		showColumnMenu: false,

		/*Enables display of the filterbox in the column menu.
		If both showColumnMenu and showFilter are false the menu button will not display.*/
		showFilter: false,

		//Show or hide the footer alltogether the footer is enabled by default
		showFooter: false,

		//Show the dropzone for drag and drop grouping
		showGroupPanel: false,

		//Row selection check boxes appear as the first column.
		showSelectionCheckbox: false,

		/*Define a sortInfo object to specify a default sorting state.
		You can also observe this variable to utilize server-side sorting (see useExternalSorting).
		Syntax is sortinfo: { fields: ['fieldName1',' fieldName2'], direction: 'ASC'/'asc' || 'desc'/'DESC'}*/
		sortInfo: {fields: [], columns: [], directions: [] },

		//Set the tab index of the Vieport.
		tabIndex: -1,

		//totalServerItems: Total items are on the server.
		totalServerItems: 0,

		/*Prevents the internal sorting from executing.
		The sortInfo object will be updated with the sorting information so you can handle sorting (see sortInfo)*/
		useExternalSorting: false,

		/*i18n language support. choose from the installed or included languages, en, fr, sp, etc...*/
		i18n: 'en',

		//the threshold in rows to force virtualization on
		virtualizationThreshold: 50
	},
		self = this;
	self.maxCanvasHt = 0;
	//self vars
	self.config = $.extend(defaults, window.ngGrid.config, options);

	// override conflicting settings
	self.config.showSelectionCheckbox = (self.config.showSelectionCheckbox && self.config.enableColumnHeavyVirt === false);
	self.config.enablePinning = (self.config.enablePinning && self.config.enableColumnHeavyVirt === false);
	self.config.selectWithCheckboxOnly = (self.config.selectWithCheckboxOnly && self.config.showSelectionCheckbox !== false);
	self.config.pinSelectionCheckbox = self.config.enablePinning;

	if (typeof options.columnDefs === "string") {
		self.config.columnDefs = $scope.$eval(options.columnDefs);
	}
	self.rowCache = [];
	self.rowMap = [];
	self.gridId = "ng" + $utils.newId();
	self.$root = null; //this is the root element that is passed in with the binding handler
	self.$groupPanel = null;
	self.$topPanel = null;
	self.$headerContainer = null;
	self.$headerScroller = null;
	self.$headers = null;
	self.$viewport = null;
	self.$canvas = null;
	self.rootDim = self.config.gridDim;
	self.data = [];
	self.lateBindColumns = false;
	self.filteredRows = [];

	self.initTemplates = function() {
		var templates = ['rowTemplate', 'aggregateTemplate', 'headerRowTemplate', 'checkboxCellTemplate', 'checkboxHeaderTemplate', 'menuTemplate', 'footerTemplate'];

		var promises = [];
		angular.forEach(templates, function(template) {
			promises.push( self.getTemplate(template) );
		});

		return $q.all(promises);
	};

	//Templates
	// test templates for urls and get the tempaltes via synchronous ajax calls
	self.getTemplate = function (key) {
		var t = self.config[key];
		var uKey = self.gridId + key + ".html";
		var p = $q.defer();
		if (t && !TEMPLATE_REGEXP.test(t)) {
			$http.get(t, {
				cache: $templateCache
			})
			.success(function(data){
				$templateCache.put(uKey, data);
				p.resolve();
			})
			.error(function(err){
				p.reject("Could not load template: " + t);
			});
		} else if (t) {
			$templateCache.put(uKey, t);
			p.resolve();
		} else {
			var dKey = key + ".html";
			$templateCache.put(uKey, $templateCache.get(dKey));
			p.resolve();
		}

		return p.promise;
	};

	if (typeof self.config.data === "object") {
		self.data = self.config.data; // we cannot watch for updates if you don't pass the string name
	}
	self.calcMaxCanvasHeight = function() {
		var calculatedHeight;
		if(self.config.groups.length > 0){
			calculatedHeight = self.rowFactory.parsedData.filter(function(e) {
				return !e[NG_HIDDEN];
			}).length * self.config.rowHeight;
		} else {
			calculatedHeight = self.filteredRows.length * self.config.rowHeight;
		}
		return calculatedHeight;
	};
	self.elementDims = {
		scrollW: 0,
		scrollH: 0,
		rowIndexCellW: 25,
		rowSelectedCellW: 25,
		rootMaxW: 0,
		rootMaxH: 0
	};
	//self funcs
	self.setRenderedRows = function (newRows) {
		$scope.renderedRows.length = newRows.length;
		for (var i = 0; i < newRows.length; i++) {
			if (!$scope.renderedRows[i] || (newRows[i].isAggRow || $scope.renderedRows[i].isAggRow)) {
				$scope.renderedRows[i] = newRows[i].copy();
				$scope.renderedRows[i].collapsed = newRows[i].collapsed;
				if (!newRows[i].isAggRow) {
					$scope.renderedRows[i].setVars(newRows[i]);
				}
			} else {
				$scope.renderedRows[i].setVars(newRows[i]);
			}
			$scope.renderedRows[i].rowIndex = newRows[i].rowIndex;
			$scope.renderedRows[i].offsetTop = newRows[i].offsetTop;
			$scope.renderedRows[i].selected = newRows[i].selected;
			newRows[i].renderedRowIndex = i;
		}
		self.refreshDomSizes();
		$scope.$emit('ngGridEventRows', newRows);
	};
	self.minRowsToRender = function() {
		var viewportH = $scope.viewportDimHeight() || 1;
		return Math.floor(viewportH / self.config.rowHeight);
	};
	self.refreshDomSizes = function() {
		var dim = new ngDimension();
		dim.outerWidth = self.elementDims.rootMaxW;
		dim.outerHeight = self.elementDims.rootMaxH;
		self.rootDim = dim;
		self.maxCanvasHt = self.calcMaxCanvasHeight();
	};
	self.buildColumnDefsFromData = function () {
		self.config.columnDefs = [];
		var item = self.data[0];
		if (!item) {
			self.lateBoundColumns = true;
			return;
		}
		$utils.forIn(item, function (prop, propName) {
			if (self.config.excludeProperties.indexOf(propName) === -1) {
				self.config.columnDefs.push({
					field: propName
				});
			}
		});
	};
	self.buildColumns = function() {
		var columnDefs = self.config.columnDefs,
			cols = [];
		if (!columnDefs) {
			self.buildColumnDefsFromData();
			columnDefs = self.config.columnDefs;
		}
		if (self.config.showSelectionCheckbox) {
			cols.push(new ngColumn({
				colDef: {
					field: '\u2714',
					width: self.elementDims.rowSelectedCellW,
					sortable: false,
					resizable: false,
					groupable: false,
					headerCellTemplate: $templateCache.get($scope.gridId + 'checkboxHeaderTemplate.html'),
					cellTemplate: $templateCache.get($scope.gridId + 'checkboxCellTemplate.html'),
					pinned: self.config.pinSelectionCheckbox
				},
				index: 0,
				headerRowHeight: self.config.headerRowHeight,
				sortCallback: self.sortData,
				resizeOnDataCallback: self.resizeOnData,
				enableResize: self.config.enableColumnResize,
				enableSort: self.config.enableSorting,
				enablePinning: self.config.enablePinning
			}, $scope, self, domUtilityService, $templateCache, $utils));
		}
		if (columnDefs.length > 0) {
			var checkboxOffset = self.config.showSelectionCheckbox ? 1 : 0;
			var groupOffset = $scope.configGroups.length;
			$scope.configGroups.length = 0;
			angular.forEach(columnDefs, function(colDef, i) {
				i += checkboxOffset;
				var column = new ngColumn({
					colDef: colDef,
					index: i + groupOffset,
					originalIndex: i,
					headerRowHeight: self.config.headerRowHeight,
					sortCallback: self.sortData,
					resizeOnDataCallback: self.resizeOnData,
					enableResize: self.config.enableColumnResize,
					enableSort: self.config.enableSorting,
					enablePinning: self.config.enablePinning,
					enableCellEdit: self.config.enableCellEdit || self.config.enableCellEditOnFocus
				}, $scope, self, domUtilityService, $templateCache, $utils);
				var indx = self.config.groups.indexOf(colDef.field);
				if (indx !== -1) {
					column.isGroupedBy = true;
					$scope.configGroups.splice(indx, 0, column);
					column.groupIndex = $scope.configGroups.length;
				}
				cols.push(column);
			});
			$scope.columns = cols;
			if (self.config.groups.length > 0) {
				self.rowFactory.getGrouping(self.config.groups);
			}
		}
	};
	self.configureColumnWidths = function() {
		var asterisksArray = [],
			percentArray = [],
			asteriskNum = 0,
			totalWidth = 0;

		// When rearranging columns, their index in $scope.columns will no longer match the original column order from columnDefs causing
		// their width config to be out of sync. We can use "originalIndex" on the ngColumns to get hold of the correct setup from columnDefs, but to
		// avoid O(n) lookups in $scope.columns per column we setup a map.
		var indexMap = {};
		// Build a map of columnDefs column indices -> ngColumn indices (via the "originalIndex" property on ngColumns).
		angular.forEach($scope.columns, function(ngCol, i) {
			// Disregard columns created by grouping (the grouping columns don't match a column from columnDefs)
			if (!$utils.isNullOrUndefined(ngCol.originalIndex)) {
				var origIndex = ngCol.originalIndex;
				if (self.config.showSelectionCheckbox) {
					//if visible, takes up 25 pixels
					if(ngCol.originalIndex === 0 && ngCol.visible){
						totalWidth += 25;
					}
					// The originalIndex will be offset 1 when including the selection column
					origIndex--;
				}
				indexMap[origIndex] = i;
			}
		});

		angular.forEach(self.config.columnDefs, function(colDef, i) {
				// Get the ngColumn that matches the current column from columnDefs
			var ngColumn = $scope.columns[indexMap[i]];

			colDef.index = i;

			var isPercent = false, t;
			//if width is not defined, set it to a single star
			if ($utils.isNullOrUndefined(colDef.width)) {
				colDef.width = "*";
			} else { // get column width
				isPercent = isNaN(colDef.width) ? $utils.endsWith(colDef.width, "%") : false;
				t = isPercent ? colDef.width : parseInt(colDef.width, 10);
			}

			 // check if it is a number
			if (isNaN(t) && !$scope.hasUserChangedGridColumnWidths) {
				t = colDef.width;
				// figure out if the width is defined or if we need to calculate it
				if (t === 'auto') { // set it for now until we have data and subscribe when it changes so we can set the width.
					ngColumn.width = ngColumn.minWidth;
					totalWidth += ngColumn.width;
					var temp = ngColumn;

					$scope.$on("ngGridEventData", function () {
						self.resizeOnData(temp);
					});
					return;
				} else if (t.indexOf("*") !== -1) { //  we need to save it until the end to do the calulations on the remaining width.
					if (ngColumn.visible !== false) {
						asteriskNum += t.length;
					}
					asterisksArray.push(colDef);
					return;
				} else if (isPercent) { // If the width is a percentage, save it until the very last.
					percentArray.push(colDef);
					return;
				} else { // we can't parse the width so lets throw an error.
					throw "unable to parse column width, use percentage (\"10%\",\"20%\", etc...) or \"*\" to use remaining width of grid";
				}
			} else if (ngColumn.visible !== false) {
				totalWidth += ngColumn.width = parseInt(ngColumn.width, 10);
			}
		});

		// Now we check if we saved any percentage columns for calculating last
		if (percentArray.length > 0) {
			//If they specificy for maintain column ratios to be false in grid config, then it will remain false. If not specifiied or true, will be true.
			self.config.maintainColumnRatios = self.config.maintainColumnRatios !== false;
			// If any columns with % widths have been hidden, then let other % based columns use their width
			var percentWidth = 0; // The total % value for all columns setting their width using % (will e.g. be 40 for 2 columns with 20% each)
			var hiddenPercent = 0; // The total % value for all columns setting their width using %, but which have been hidden
			angular.forEach(percentArray, function(colDef) {
				// Get the ngColumn that matches the current column from columnDefs
				var ngColumn = $scope.columns[indexMap[colDef.index]];
				var t = colDef.width;
				var percent = parseInt(t.slice(0, -1), 10) / 100;
				percentWidth += percent;

				if (!ngColumn.visible) {
					hiddenPercent += percent;
				}
			});
			var percentWidthUsed = percentWidth - hiddenPercent;

			// do the math
			angular.forEach(percentArray, function(colDef) {
				// Get the ngColumn that matches the current column from columnDefs
				var ngColumn = $scope.columns[indexMap[colDef.index]];

				// Calc the % relative to the amount of % reserved for the visible columns (that use % based widths)
				var t = colDef.width;
				var percent = parseInt(t.slice(0, -1), 10) / 100;
				if (hiddenPercent > 0) {
					percent = percent / percentWidthUsed;
				}
				else {
					percent = percent / percentWidth;
				}

				var pixelsForPercentBasedWidth = self.rootDim.outerWidth * percentWidth;
				ngColumn.width = Math.floor(pixelsForPercentBasedWidth * percent);
				totalWidth += ngColumn.width;
			});
		}

		// check if we saved any asterisk columns for calculating later
		if (asterisksArray.length > 0) {
			//If they specificy for maintain column ratios to be false in grid config, then it will remain false. If not specifiied or true, will be true.
			self.config.maintainColumnRatios = self.config.maintainColumnRatios !== false;
			// get the remaining width
			var remainingWidth = self.rootDim.outerWidth - totalWidth;
			// are we overflowing vertically?
			if (self.maxCanvasHt > $scope.viewportDimHeight()) {
				//compensate for scrollbar
				remainingWidth -= domUtilityService.ScrollW;
			}
			// calculate the weight of each asterisk rounded down
			var asteriskVal = Math.floor(remainingWidth / asteriskNum);

			// set the width of each column based on the number of stars
			angular.forEach(asterisksArray, function(colDef, i) {
				// Get the ngColumn that matches the current column from columnDefs
				var ngColumn = $scope.columns[indexMap[colDef.index]];
				ngColumn.width = asteriskVal * colDef.width.length;
				if (ngColumn.visible !== false) {
					totalWidth += ngColumn.width;
				}

				var isLast = (i === (asterisksArray.length - 1));
				//if last asterisk and doesn't fill width of grid, add the difference
				if(isLast && totalWidth < self.rootDim.outerWidth){
					var gridWidthDifference = self.rootDim.outerWidth - totalWidth;
					if(self.maxCanvasHt > $scope.viewportDimHeight()){
						gridWidthDifference -= domUtilityService.ScrollW;
					}
					ngColumn.width += gridWidthDifference;
				}
			});
		}
	};
	self.init = function() {
		return self.initTemplates().then(function(){
			//factories and services
			$scope.selectionProvider = new ngSelectionProvider(self, $scope, $parse);
			$scope.domAccessProvider = new ngDomAccessProvider(self);
			self.rowFactory = new ngRowFactory(self, $scope, domUtilityService, $templateCache, $utils);
			self.searchProvider = new ngSearchProvider($scope, self, $filter);
			self.styleProvider = new ngStyleProvider($scope, self);
			$scope.$watch('configGroups', function(a) {
			  var tempArr = [];
			  angular.forEach(a, function(item) {
				tempArr.push(item.field || item);
			  });
			  self.config.groups = tempArr;
			  self.rowFactory.filteredRowsChanged();
			  $scope.$emit('ngGridEventGroups', a);
			}, true);
			$scope.$watch('columns', function (a) {
				if(!$scope.isColumnResizing){
					domUtilityService.RebuildGrid($scope, self);
				}
				$scope.$emit('ngGridEventColumns', a);
			}, true);
			$scope.$watch(function() {
				return options.i18n;
			}, function(newLang) {
				$utils.seti18n($scope, newLang);
			});
			self.maxCanvasHt = self.calcMaxCanvasHeight();

			if (self.config.sortInfo.fields && self.config.sortInfo.fields.length > 0) {
				$scope.$watch(function() {
					return self.config.sortInfo;
				}, function(sortInfo){
					if (!sortService.isSorting) {
						self.sortColumnsInit();
						$scope.$emit('ngGridEventSorted', self.config.sortInfo);
					}
				},true);
			}
		});

		// var p = $q.defer();
		// p.resolve();
		// return p.promise;
	};

	self.resizeOnData = function(col) {
		// we calculate the longest data.
		var longest = col.minWidth;
		var arr = $utils.getElementsByClassName('col' + col.index);
		angular.forEach(arr, function(elem, index) {
			var i;
			if (index === 0) {
				var kgHeaderText = $(elem).find('.ngHeaderText');
				i = $utils.visualLength(kgHeaderText) + 10; // +10 some margin
			} else {
				var ngCellText = $(elem).find('.ngCellText');
				i = $utils.visualLength(ngCellText) + 10; // +10 some margin
			}
			if (i > longest) {
				longest = i;
			}
		});
		col.width = col.longest = Math.min(col.maxWidth, longest + 7); // + 7 px to make it look decent.
		domUtilityService.BuildStyles($scope, self, true);
	};
	self.lastSortedColumns = [];
	self.sortData = function(col, evt) {
		if (evt && evt.shiftKey && self.config.sortInfo) {
			var indx = self.config.sortInfo.columns.indexOf(col);
			if (indx === -1) {
				if (self.config.sortInfo.columns.length === 1) {
					self.config.sortInfo.columns[0].sortPriority = 1;
				}
				self.config.sortInfo.columns.push(col);
				col.sortPriority = self.config.sortInfo.columns.length;
				self.config.sortInfo.fields.push(col.field);
				self.config.sortInfo.directions.push(col.sortDirection);
				self.lastSortedColumns.push(col);
			} else {
				self.config.sortInfo.directions[indx] = col.sortDirection;
			}
		} else {
			var isArr = $.isArray(col);
			self.config.sortInfo.columns.length = 0;
			self.config.sortInfo.fields.length = 0;
			self.config.sortInfo.directions.length = 0;
			var push = function (c) {
				self.config.sortInfo.columns.push(c);
				self.config.sortInfo.fields.push(c.field);
				self.config.sortInfo.directions.push(c.sortDirection);
				self.lastSortedColumns.push(c);
			};
			if (isArr) {
				self.clearSortingData();
				angular.forEach(col, function (c, i) {
					c.sortPriority = i + 1;
					push(c);
				});
			} else {
				self.clearSortingData(col);
				col.sortPriority = undefined;
				push(col);
			}
		}
		self.sortActual();
		self.searchProvider.evalFilter();
		$scope.$emit('ngGridEventSorted', self.config.sortInfo);
	};
	self.sortColumnsInit = function() {
		if (self.config.sortInfo.columns) {
			self.config.sortInfo.columns.length = 0;
		} else {
			self.config.sortInfo.columns = [];
		}
		angular.forEach($scope.columns, function(c) {
			var i = self.config.sortInfo.fields.indexOf(c.field);
			if (i !== -1) {
				c.sortDirection = self.config.sortInfo.directions[i] || 'asc';
				self.config.sortInfo.columns[i] = c;
			}
		});
		angular.forEach(self.config.sortInfo.columns, function(c){
			self.sortData(c);
		});
	};
	self.sortActual = function() {
		if (!self.config.useExternalSorting) {
			var tempData = self.data.slice(0);
			angular.forEach(tempData, function(item, i) {
				var e = self.rowMap[i];
				if (e !== undefined) {
					var v = self.rowCache[e];
					if (v !== undefined) {
						item.preSortSelected = v.selected;
						item.preSortIndex = i;
					}
				}
			});
			sortService.Sort(self.config.sortInfo, tempData);
			angular.forEach(tempData, function(item, i) {
				self.rowCache[i].entity = item;
				self.rowCache[i].selected = item.preSortSelected;
				self.rowMap[item.preSortIndex] = i;
				delete item.preSortSelected;
				delete item.preSortIndex;
			});
		}
	};

	self.clearSortingData = function (col) {
		if (!col) {
			angular.forEach(self.lastSortedColumns, function (c) {
				c.sortDirection = "";
				c.sortPriority = null;
			});
			self.lastSortedColumns = [];
		} else {
			angular.forEach(self.lastSortedColumns, function (c) {
				if (col.index !== c.index) {
					c.sortDirection = "";
					c.sortPriority = null;
				}
			});
			self.lastSortedColumns[0] = col;
			self.lastSortedColumns.length = 1;
		}
	};
	self.fixColumnIndexes = function() {
		//fix column indexes
		for (var i = 0; i < $scope.columns.length; i++) {
			$scope.columns[i].index = i;
		}
	};
	self.fixGroupIndexes = function() {
		angular.forEach($scope.configGroups, function(item, i) {
			item.groupIndex = i + 1;
		});
	};
	//$scope vars
	$scope.elementsNeedMeasuring = true;
	$scope.columns = [];
	$scope.renderedRows = [];
	$scope.renderedColumns = [];
	$scope.headerRow = null;
	$scope.rowHeight = self.config.rowHeight;
	$scope.jqueryUITheme = self.config.jqueryUITheme;
	$scope.showSelectionCheckbox = self.config.showSelectionCheckbox;
	$scope.enableCellSelection = self.config.enableCellSelection;
	$scope.enableCellEditOnFocus = self.config.enableCellEditOnFocus;
	$scope.footer = null;
	$scope.selectedItems = self.config.selectedItems;
	$scope.multiSelect = self.config.multiSelect;
	$scope.showFooter = self.config.showFooter;
	$scope.footerRowHeight = $scope.showFooter ? self.config.footerRowHeight : 0;
	$scope.showColumnMenu = self.config.showColumnMenu;
	$scope.showMenu = false;
	$scope.configGroups = [];
	$scope.gridId = self.gridId;
	//Paging
	$scope.enablePaging = self.config.enablePaging;
	$scope.pagingOptions = self.config.pagingOptions;

	//i18n support
	$scope.i18n = {};
	$utils.seti18n($scope, self.config.i18n);
	$scope.adjustScrollLeft = function (scrollLeft) {
		var colwidths = 0,
			totalLeft = 0,
			x = $scope.columns.length,
			newCols = [],
			dcv = !self.config.enableColumnHeavyVirt;
		var r = 0;
		var addCol = function (c) {
			if (dcv) {
				newCols.push(c);
			} else {
				if (!$scope.renderedColumns[r]) {
					$scope.renderedColumns[r] = c.copy();
				} else {
					$scope.renderedColumns[r].setVars(c);
				}
			}
			r++;
		};
		for (var i = 0; i < x; i++) {
			var col = $scope.columns[i];
			if (col.visible !== false) {
				var w = col.width + colwidths;
				if (col.pinned) {
					addCol(col);
					var newLeft = i > 0 ? (scrollLeft + totalLeft) : scrollLeft;
					domUtilityService.setColLeft(col, newLeft, self);
					totalLeft += col.width;
				} else {
					if (w >= scrollLeft) {
						if (colwidths <= scrollLeft + self.rootDim.outerWidth) {
							addCol(col);
						}
					}
				}
				colwidths += col.width;
			}
		}
		if (dcv) {
			$scope.renderedColumns = newCols;
		}
	};
	self.prevScrollTop = 0;
	self.prevScrollIndex = 0;
	$scope.adjustScrollTop = function(scrollTop, force) {
		if (self.prevScrollTop === scrollTop && !force) {
			return;
		}
		if (scrollTop > 0 && self.$viewport[0].scrollHeight - scrollTop <= self.$viewport.outerHeight()) {
			$scope.$emit('ngGridEventScroll');
		}
		var rowIndex = Math.floor(scrollTop / self.config.rowHeight);
		var newRange;
		if (self.filteredRows.length > self.config.virtualizationThreshold) {
			// Have we hit the threshold going down?
			if (self.prevScrollTop < scrollTop && rowIndex < self.prevScrollIndex + SCROLL_THRESHOLD) {
				return;
			}
			//Have we hit the threshold going up?
			if (self.prevScrollTop > scrollTop && rowIndex > self.prevScrollIndex - SCROLL_THRESHOLD) {
				return;
			}
			newRange = new ngRange(Math.max(0, rowIndex - EXCESS_ROWS), rowIndex + self.minRowsToRender() + EXCESS_ROWS);
		} else {
			var maxLen = $scope.configGroups.length > 0 ? self.rowFactory.parsedData.length : self.data.length;
			newRange = new ngRange(0, Math.max(maxLen, self.minRowsToRender() + EXCESS_ROWS));
		}
		self.prevScrollTop = scrollTop;
		self.rowFactory.UpdateViewableRange(newRange);
		self.prevScrollIndex = rowIndex;
	};

	//scope funcs
	$scope.toggleShowMenu = function() {
		$scope.showMenu = !$scope.showMenu;
	};
	$scope.toggleSelectAll = function(state, selectOnlyVisible) {
		$scope.selectionProvider.toggleSelectAll(state, false, selectOnlyVisible);
	};
	$scope.totalFilteredItemsLength = function() {
		return self.filteredRows.length;
	};
	$scope.showGroupPanel = function() {
		return self.config.showGroupPanel;
	};
	$scope.topPanelHeight = function() {
		return self.config.showGroupPanel === true ? self.config.headerRowHeight + 32 : self.config.headerRowHeight;
	};

	$scope.viewportDimHeight = function() {
		return Math.max(0, self.rootDim.outerHeight - $scope.topPanelHeight() - $scope.footerRowHeight - 2);
	};
	$scope.groupBy = function (col) {
		if (self.data.length < 1 || !col.groupable  || !col.field) {
			return;
		}
		//first sort the column
		if (!col.sortDirection) {
			col.sort({ shiftKey: $scope.configGroups.length > 0 ? true : false });
		}

		var indx = $scope.configGroups.indexOf(col);
		if (indx === -1) {
			col.isGroupedBy = true;
			$scope.configGroups.push(col);
			col.groupIndex = $scope.configGroups.length;
		} else {
			$scope.removeGroup(indx);
		}
		self.$viewport.scrollTop(0);
		domUtilityService.digest($scope);
	};
	$scope.removeGroup = function(index) {
		var col = $scope.columns.filter(function(item) {
			return item.groupIndex === (index + 1);
		})[0];
		col.isGroupedBy = false;
		col.groupIndex = 0;
		if ($scope.columns[index].isAggCol) {
			$scope.columns.splice(index, 1);
			$scope.configGroups.splice(index, 1);
			self.fixGroupIndexes();
		}
		if ($scope.configGroups.length === 0) {
			self.fixColumnIndexes();
			domUtilityService.digest($scope);
		}
		$scope.adjustScrollLeft(0);
	};
	$scope.togglePin = function (col) {
		var indexFrom = col.index;
		var indexTo = 0;
		for (var i = 0; i < $scope.columns.length; i++) {
			if (!$scope.columns[i].pinned) {
				break;
			}
			indexTo++;
		}
		if (col.pinned) {
			indexTo = Math.max(col.originalIndex, indexTo - 1);
		}
		col.pinned = !col.pinned;
		// Splice the columns
		$scope.columns.splice(indexFrom, 1);
		$scope.columns.splice(indexTo, 0, col);
		self.fixColumnIndexes();
		// Finally, rebuild the CSS styles.
		domUtilityService.BuildStyles($scope, self, true);
		self.$viewport.scrollLeft(self.$viewport.scrollLeft() - col.width);
	};
	$scope.totalRowWidth = function() {
		var totalWidth = 0,
			cols = $scope.columns;
		for (var i = 0; i < cols.length; i++) {
			if (cols[i].visible !== false) {
				totalWidth += cols[i].width;
			}
		}
		return totalWidth;
	};
	$scope.headerScrollerDim = function() {
		var viewportH = $scope.viewportDimHeight(),
			maxHeight = self.maxCanvasHt,
			vScrollBarIsOpen = (maxHeight > viewportH),
			newDim = new ngDimension();

		newDim.autoFitHeight = true;
		newDim.outerWidth = $scope.totalRowWidth();
		if (vScrollBarIsOpen) {
			newDim.outerWidth += self.elementDims.scrollW;
		} else if ((maxHeight - viewportH) <= self.elementDims.scrollH) { //if the horizontal scroll is open it forces the viewport to be smaller
			newDim.outerWidth += self.elementDims.scrollW;
		}
		return newDim;
	};
};

var ngRange = function (top, bottom) {
	this.topRow = top;
	this.bottomRow = bottom;
};
var ngRow = function (entity, config, selectionProvider, rowIndex, $utils) {
  this.entity = entity;
  this.config = config;
  this.selectionProvider = selectionProvider;
  this.rowIndex = rowIndex;
  this.utils = $utils;
  this.selected = selectionProvider.getSelection(entity);
  this.cursor = this.config.enableRowSelection ? 'pointer' : 'default';
  this.beforeSelectionChange = config.beforeSelectionChangeCallback;
  this.afterSelectionChange = config.afterSelectionChangeCallback;
  this.offsetTop = this.rowIndex * config.rowHeight;
  this.rowDisplayIndex = 0;
};

ngRow.prototype.setSelection = function (isSelected) {
  this.selectionProvider.setSelection(this, isSelected);
  this.selectionProvider.lastClickedRow = this;
};
ngRow.prototype.continueSelection = function (event) {
  this.selectionProvider.ChangeSelection(this, event);
};
ngRow.prototype.ensureEntity = function (expected) {
  if (this.entity !== expected) {
	// Update the entity and determine our selected property
	this.entity = expected;
	this.selected = this.selectionProvider.getSelection(this.entity);
  }
};
ngRow.prototype.toggleSelected = function (event) {
  if (!this.config.enableRowSelection && !this.config.enableCellSelection) {
	return true;
  }
  var element = event.target || event;
  //check and make sure its not the bubbling up of our checked 'click' event
  if (element.type === "checkbox" && element.parentElement.className !== "ngSelectionCell ng-scope") {
	return true;
  }
  if (this.config.selectWithCheckboxOnly && element.type !== "checkbox") {
	this.selectionProvider.lastClickedRow = this;
	return true;
  }
  if (this.beforeSelectionChange(this, event)) {
	this.continueSelection(event);
  }
  return false;
};
ngRow.prototype.alternatingRowClass = function () {
  var isEven = (this.rowIndex % 2) === 0;
  var classes = {
	'ngRow' : true,
	'selected': this.selected,
	'even': isEven,
	'odd': !isEven,
	'ui-state-default': this.config.jqueryUITheme && isEven,
	'ui-state-active': this.config.jqueryUITheme && !isEven
  };
  return classes;
};
ngRow.prototype.getProperty = function (path) {
  return this.utils.evalProperty(this.entity, path);
};
ngRow.prototype.copy = function () {
  this.clone = new ngRow(this.entity, this.config, this.selectionProvider, this.rowIndex, this.utils);
  this.clone.isClone = true;
  this.clone.elm = this.elm;
  this.clone.orig = this;
  return this.clone;
};
ngRow.prototype.setVars = function (fromRow) {
  fromRow.clone = this;
  this.entity = fromRow.entity;
  this.selected = fromRow.selected;
	this.orig = fromRow;
};
var ngRowFactory = function (grid, $scope, domUtilityService, $templateCache, $utils) {
	var self = this;
	// we cache rows when they are built, and then blow the cache away when sorting
	self.aggCache = {};
	self.parentCache = []; // Used for grouping and is cleared each time groups are calulated.
	self.dataChanged = true;
	self.parsedData = [];
	self.rowConfig = {};
	self.selectionProvider = $scope.selectionProvider;
	self.rowHeight = 30;
	self.numberOfAggregates = 0;
	self.groupedData = undefined;
	self.rowHeight = grid.config.rowHeight;
	self.rowConfig = {
		enableRowSelection: grid.config.enableRowSelection,
		rowClasses: grid.config.rowClasses,
		selectedItems: $scope.selectedItems,
		selectWithCheckboxOnly: grid.config.selectWithCheckboxOnly,
		beforeSelectionChangeCallback: grid.config.beforeSelectionChange,
		afterSelectionChangeCallback: grid.config.afterSelectionChange,
		jqueryUITheme: grid.config.jqueryUITheme,
		enableCellSelection: grid.config.enableCellSelection,
		rowHeight: grid.config.rowHeight
	};

	self.renderedRange = new ngRange(0, grid.minRowsToRender() + EXCESS_ROWS);

	// @entity - the data item
	// @rowIndex - the index of the row
	self.buildEntityRow = function(entity, rowIndex) {
		// build the row
		return new ngRow(entity, self.rowConfig, self.selectionProvider, rowIndex, $utils);
	};

	self.buildAggregateRow = function(aggEntity, rowIndex) {
		var agg = self.aggCache[aggEntity.aggIndex]; // first check to see if we've already built it
		if (!agg) {
			// build the row
			agg = new ngAggregate(aggEntity, self, self.rowConfig.rowHeight, grid.config.groupsCollapsedByDefault);
			self.aggCache[aggEntity.aggIndex] = agg;
		}
		agg.rowIndex = rowIndex;
		agg.offsetTop = rowIndex * self.rowConfig.rowHeight;
		return agg;
	};
	self.UpdateViewableRange = function(newRange) {
		self.renderedRange = newRange;
		self.renderedChange();
	};
	self.filteredRowsChanged = function() {
		// check for latebound autogenerated columns
		if (grid.lateBoundColumns && grid.filteredRows.length > 0) {
			grid.config.columnDefs = undefined;
			grid.buildColumns();
			grid.lateBoundColumns = false;
			$scope.$evalAsync(function() {
				$scope.adjustScrollLeft(0);
			});
		}
		self.dataChanged = true;
		if (grid.config.groups.length > 0) {
			self.getGrouping(grid.config.groups);
		}
		self.UpdateViewableRange(self.renderedRange);
	};

	self.renderedChange = function() {
		if (!self.groupedData || grid.config.groups.length < 1) {
			self.renderedChangeNoGroups();
			grid.refreshDomSizes();
			return;
		}
		self.wasGrouped = true;
		self.parentCache = [];
		var x = 0;
		var temp = self.parsedData.filter(function (e) {
			if (e.isAggRow) {
				if (e.parent && e.parent.collapsed) {
					return false;
				}
				return true;
			}
			if (!e[NG_HIDDEN]) {
				e.rowIndex = x++;
			}
			return !e[NG_HIDDEN];
		});
		self.totalRows = temp.length;
		var rowArr = [];
		for (var i = self.renderedRange.topRow; i < self.renderedRange.bottomRow; i++) {
			if (temp[i]) {
				temp[i].offsetTop = i * grid.config.rowHeight;
				rowArr.push(temp[i]);
			}
		}
		grid.setRenderedRows(rowArr);
	};

	self.renderedChangeNoGroups = function () {
		var rowArr = [];
		for (var i = self.renderedRange.topRow; i < self.renderedRange.bottomRow; i++) {
			if (grid.filteredRows[i]) {
				grid.filteredRows[i].rowIndex = i;
				grid.filteredRows[i].offsetTop = i * grid.config.rowHeight;
				rowArr.push(grid.filteredRows[i]);
			}
		}
		grid.setRenderedRows(rowArr);
	};

	self.fixRowCache = function () {
		var newLen = grid.data.length;
		var diff = newLen - grid.rowCache.length;
		if (diff < 0) {
			grid.rowCache.length = grid.rowMap.length = newLen;
		} else {
			for (var i = grid.rowCache.length; i < newLen; i++) {
				grid.rowCache[i] = grid.rowFactory.buildEntityRow(grid.data[i], i);
			}
		}
	};

	//magical recursion. it works. I swear it. I figured it out in the shower one day.
	self.parseGroupData = function(g) {
		if (g.values) {
			for (var x = 0; x < g.values.length; x++){
				// get the last parent in the array because that's where our children want to be
				self.parentCache[self.parentCache.length - 1].children.push(g.values[x]);
				//add the row to our return array
				self.parsedData.push(g.values[x]);
			}
		} else {
			for (var prop in g) {
				// exclude the meta properties.
				if (prop === NG_FIELD || prop === NG_DEPTH || prop === NG_COLUMN) {
					continue;
				} else if (g.hasOwnProperty(prop)) {
					//build the aggregate row
					var agg = self.buildAggregateRow({
						gField: g[NG_FIELD],
						gLabel: prop,
						gDepth: g[NG_DEPTH],
						isAggRow: true,
						'_ng_hidden_': false,
						children: [],
						aggChildren: [],
						aggIndex: self.numberOfAggregates,
						aggLabelFilter: g[NG_COLUMN].aggLabelFilter
					}, 0);
					self.numberOfAggregates++;
					//set the aggregate parent to the parent in the array that is one less deep.
					agg.parent = self.parentCache[agg.depth - 1];
					// if we have a parent, set the parent to not be collapsed and append the current agg to its children
					if (agg.parent) {
						agg.parent.collapsed = false;
						agg.parent.aggChildren.push(agg);
					}
					// add the aggregate row to the parsed data.
					self.parsedData.push(agg);
					// the current aggregate now the parent of the current depth
					self.parentCache[agg.depth] = agg;
					// dig deeper for more aggregates or children.
					self.parseGroupData(g[prop]);
				}
			}
		}
	};
	//Shuffle the data into their respective groupings.
	self.getGrouping = function(groups) {
		self.aggCache = [];
		self.numberOfAggregates = 0;
		self.groupedData = {};
		// Here we set the onmousedown event handler to the header container.
		var rows = grid.filteredRows,
			maxDepth = groups.length,
			cols = $scope.columns;

		function filterCols(cols, group) {
			return cols.filter(function(c) {
				return c.field === group;
			});
		}

		for (var x = 0; x < rows.length; x++) {
			var model = rows[x].entity;
			if (!model) {
				return;
			}
			rows[x][NG_HIDDEN] = grid.config.groupsCollapsedByDefault;
			var ptr = self.groupedData;

			for (var y = 0; y < groups.length; y++) {
				var group = groups[y];

				var col = filterCols(cols, group)[0];

				var val = $utils.evalProperty(model, group);
				val = val ? val.toString() : 'null';
				if (!ptr[val]) {
					ptr[val] = {};
				}
				if (!ptr[NG_FIELD]) {
					ptr[NG_FIELD] = group;
				}
				if (!ptr[NG_DEPTH]) {
					ptr[NG_DEPTH] = y;
				}
				if (!ptr[NG_COLUMN]) {
					ptr[NG_COLUMN] = col;
				}
				ptr = ptr[val];
			}
			if (!ptr.values) {
				ptr.values = [];
			}
			ptr.values.push(rows[x]);
		}

		//moved out of above loops due to if no data initially, but has initial grouping, columns won't be added
		if(cols.length > 0) {
			for (var z = 0; z < groups.length; z++) {
				if (!cols[z].isAggCol && z <= maxDepth) {
					cols.splice(0, 0, new ngColumn({
						colDef: {
							field: '',
							width: 25,
							sortable: false,
							resizable: false,
							headerCellTemplate: '<div class="ngAggHeader"></div>',
							pinned: grid.config.pinSelectionCheckbox

						},
						enablePinning: grid.config.enablePinning,
						isAggCol: true,
						headerRowHeight: grid.config.headerRowHeight

					}, $scope, grid, domUtilityService, $templateCache, $utils));
				}
			}
		}

		grid.fixColumnIndexes();
		$scope.adjustScrollLeft(0);
		self.parsedData.length = 0;
		self.parseGroupData(self.groupedData);
		self.fixRowCache();
	};

	if (grid.config.groups.length > 0 && grid.filteredRows.length > 0) {
		self.getGrouping(grid.config.groups);
	}
};
var ngSearchProvider = function ($scope, grid, $filter) {
	var self = this,
		searchConditions = [];

	self.extFilter = grid.config.filterOptions.useExternalFilter;
	$scope.showFilter = grid.config.showFilter;
	$scope.filterText = '';

	self.fieldMap = {};

	var searchEntireRow = function(condition, item, fieldMap){
		var result;
		for (var prop in item) {
			if (item.hasOwnProperty(prop)) {
				var c = fieldMap[prop.toLowerCase()];
				if (!c) {
					continue;
				}
				var pVal = item[prop];
				if(typeof pVal === 'object'){
					return searchEntireRow(condition, pVal, c);
				} else {
					var f = null,
						s = null;
					if (c && c.cellFilter) {
						s = c.cellFilter.split(':');
						f = $filter(s[0]);
					}
					if (pVal !== null && pVal !== undefined) {
						if (typeof f === "function") {
							var filterRes = f(pVal, s[1]).toString();
							result = condition.regex.test(filterRes);
						} else {
							result = condition.regex.test(pVal.toString());
						}
						if (result) {
							return true;
						}
					}
				}
			}
		}
		return false;
	};

	var searchColumn = function(condition, item){
		var result;
		var col = self.fieldMap[condition.columnDisplay];
		if (!col) {
			return false;
		}
		var sp = col.cellFilter.split(':');
		var filter = col.cellFilter ? $filter(sp[0]) : null;
		var value = item[condition.column] || item[col.field.split('.')[0]];
		if (value === null || value === undefined) {
			return false;
		}
		if (typeof filter === "function") {
			var filterResults = filter(typeof value === "object" ? evalObject(value, col.field) : value, sp[1]).toString();
			result = condition.regex.test(filterResults);
		}
		else {
			result = condition.regex.test(typeof value === "object" ? evalObject(value, col.field).toString() : value.toString());
		}
		if (result) {
			return true;
		}
		return false;
	};

	var filterFunc = function(item) {
		for (var x = 0, len = searchConditions.length; x < len; x++) {
			var condition = searchConditions[x];
			var result;
			if (!condition.column) {
				result = searchEntireRow(condition, item, self.fieldMap);
			} else {
				result = searchColumn(condition, item);
			}
			if(!result) {
				return false;
			}
		}
		return true;
	};

	self.evalFilter = function () {
		if (searchConditions.length === 0) {
			grid.filteredRows = grid.rowCache;
		} else {
			grid.filteredRows = grid.rowCache.filter(function(row) {
				return filterFunc(row.entity);
			});
		}
		for (var i = 0; i < grid.filteredRows.length; i++)
		{
			grid.filteredRows[i].rowIndex = i;

		}
		grid.rowFactory.filteredRowsChanged();
	};

	//Traversing through the object to find the value that we want. If fail, then return the original object.
	var evalObject = function (obj, columnName) {
		if (typeof obj !== "object" || typeof columnName !== "string") {
			return obj;
		}
		var args = columnName.split('.');
		var cObj = obj;
		if (args.length > 1) {
			for (var i = 1, len = args.length; i < len; i++) {
				cObj = cObj[args[i]];
				if (!cObj) {
					return obj;
				}
			}
			return cObj;
		}
		return obj;
	};
	var getRegExp = function (str, modifiers) {
		try {
			return new RegExp(str, modifiers);
		} catch (err) {
			//Escape all RegExp metacharacters.
			return new RegExp(str.replace(/(\^|\$|\(|\)|<|>|\[|\]|\{|\}|\\|\||\.|\*|\+|\?)/g, '\\$1'));
		}
	};
	var buildSearchConditions = function (a) {
		//reset.
		searchConditions = [];
		var qStr;
		if (!(qStr = $.trim(a))) {
			return;
		}
		var columnFilters = qStr.split(";");
		for (var i = 0; i < columnFilters.length; i++) {
			var args = columnFilters[i].split(':');
			if (args.length > 1) {
				var columnName = $.trim(args[0]);
				var columnValue = $.trim(args[1]);
				if (columnName && columnValue) {
					searchConditions.push({
						column: columnName,
						columnDisplay: columnName.replace(/\s+/g, '').toLowerCase(),
						regex: getRegExp(columnValue, 'i')
					});
				}
			} else {
				var val = $.trim(args[0]);
				if (val) {
					searchConditions.push({
						column: '',
						regex: getRegExp(val, 'i')
					});
				}
			}
		}
	};

	if (!self.extFilter) {
		$scope.$watch('columns', function (cs) {
			for (var i = 0; i < cs.length; i++) {
				var col = cs[i];
				if (col.field) {
					if(col.field.match(/\./g)){
						var properties = col.field.split('.');
						var currentProperty = self.fieldMap;
						for(var j = 0; j < properties.length - 1; j++) {
							currentProperty[ properties[j] ] =  currentProperty[ properties[j] ] || {};
							currentProperty = currentProperty[properties[j]];
						}
						currentProperty[ properties[properties.length - 1] ] = col;
					} else {
						self.fieldMap[col.field.toLowerCase()] = col;
					}
				}
				if (col.displayName) {
					self.fieldMap[col.displayName.toLowerCase().replace(/\s+/g, '')] = col;
				}
			}
		});
	}

	$scope.$watch(
		function () {
			return grid.config.filterOptions.filterText;
		},
		function (a) {
			$scope.filterText = a;
		}
	);

	$scope.$watch('filterText', function(a){
		if (!self.extFilter) {
			$scope.$emit('ngGridEventFilter', a);
			buildSearchConditions(a);
			self.evalFilter();
		}
	});
};
var ngSelectionProvider = function (grid, $scope, $parse) {
	var self = this;
	self.multi = grid.config.multiSelect;
	self.selectedItems = grid.config.selectedItems;
	self.selectedIndex = grid.config.selectedIndex;
	self.lastClickedRow = undefined;
	self.ignoreSelectedItemChanges = false; // flag to prevent circular event loops keeping single-select var in sync
	self.pKeyParser = $parse(grid.config.primaryKey);

	// function to manage the selection action of a data item (entity)
	self.ChangeSelection = function (rowItem, evt) {
		// ctrl-click + shift-click multi-selections
		// up/down key navigation in multi-selections
		var charCode = evt.which || evt.keyCode;
		var isUpDownKeyPress = (charCode === 40 || charCode === 38);

		if (evt && evt.shiftKey && !evt.keyCode && self.multi && grid.config.enableRowSelection) {
			if (self.lastClickedRow) {
				var rowsArr;
				if ($scope.configGroups.length > 0) {
					rowsArr = grid.rowFactory.parsedData.filter(function(row) {
						return !row.isAggRow;
					});
				}
				else {
					rowsArr = grid.filteredRows;
				}

				var thisIndx = rowItem.rowIndex;
				var prevIndx = self.lastClickedRowIndex;

				if (thisIndx === prevIndx) {
					return false;
				}

				if (thisIndx < prevIndx) {
					thisIndx = thisIndx ^ prevIndx;
					prevIndx = thisIndx ^ prevIndx;
					thisIndx = thisIndx ^ prevIndx;
					thisIndx--;
				}
				else {
					prevIndx++;
				}

				var rows = [];
				for (; prevIndx <= thisIndx; prevIndx++) {
					rows.push(rowsArr[prevIndx]);
				}

				if (rows[rows.length - 1].beforeSelectionChange(rows, evt)) {
					for (var i = 0; i < rows.length; i++) {
						var ri = rows[i];
						var selectionState = ri.selected;
						ri.selected = !selectionState;
						if (ri.clone) {
							ri.clone.selected = ri.selected;
						}
						var index = self.selectedItems.indexOf(ri.entity);
						if (index === -1) {
							self.selectedItems.push(ri.entity);
						}
						else {
							self.selectedItems.splice(index, 1);
						}
					}
					rows[rows.length - 1].afterSelectionChange(rows, evt);
				}
				self.lastClickedRow = rowItem;
				self.lastClickedRowIndex = rowItem.rowIndex;

				return true;
			}
		}
		else if (!self.multi) {
			if (self.lastClickedRow === rowItem) {
				self.setSelection(self.lastClickedRow, grid.config.keepLastSelected ? true : !rowItem.selected);
			} else {
				if (self.lastClickedRow) {
					self.setSelection(self.lastClickedRow, false);
				}
				self.setSelection(rowItem, !rowItem.selected);
			}
		}
		else if (!evt.keyCode || isUpDownKeyPress && !grid.config.selectWithCheckboxOnly) {
			self.setSelection(rowItem, !rowItem.selected);
		}
		self.lastClickedRow = rowItem;
		self.lastClickedRowIndex = rowItem.rowIndex;
		return true;
	};

	self.getSelection = function (entity) {
		var isSelected = false;
		if (grid.config.primaryKey) {
			var val = self.pKeyParser(entity);
			angular.forEach(self.selectedItems, function (c) {
				if (val === self.pKeyParser(c)) {
					isSelected = true;
				}
			});
		}
		else {
			isSelected = self.selectedItems.indexOf(entity) !== -1;
		}
		return isSelected;
	};

	// just call this func and hand it the rowItem you want to select (or de-select)
	self.setSelection = function (rowItem, isSelected) {
		if(grid.config.enableRowSelection){
			if (!isSelected) {
				var indx = self.selectedItems.indexOf(rowItem.entity);
				if (indx !== -1) {
					self.selectedItems.splice(indx, 1);
				}
			}
			else {
				if (self.selectedItems.indexOf(rowItem.entity) === -1) {
					if (!self.multi && self.selectedItems.length > 0) {
						self.toggleSelectAll(false, true);
					}
					self.selectedItems.push(rowItem.entity);
				}
			}
			rowItem.selected = isSelected;
			if (rowItem.orig) {
				rowItem.orig.selected = isSelected;
			}
			if (rowItem.clone) {
				rowItem.clone.selected = isSelected;
			}
			rowItem.afterSelectionChange(rowItem);
		}
	};

	// @return - boolean indicating if all items are selected or not
	// @val - boolean indicating whether to select all/de-select all
	self.toggleSelectAll = function (checkAll, bypass, selectFiltered) {
		var rows = selectFiltered ? grid.filteredRows : grid.rowCache;
		if (bypass || grid.config.beforeSelectionChange(rows, checkAll)) {
			var selectedlength = self.selectedItems.length;
			if (selectedlength > 0) {
				self.selectedItems.length = 0;
			}
			for (var i = 0; i < rows.length; i++) {
				rows[i].selected = checkAll;
				if (rows[i].clone) {
					rows[i].clone.selected = checkAll;
				}
				if (checkAll) {
					self.selectedItems.push(rows[i].entity);
				}
			}
			if (!bypass) {
				grid.config.afterSelectionChange(rows, checkAll);
			}
		}
	};
};
var ngStyleProvider = function($scope, grid) {
	$scope.headerCellStyle = function(col) {
		return { "height": col.headerRowHeight + "px" };
	};
	$scope.rowStyle = function (row) {
		var ret = { "top": row.offsetTop + "px", "height": $scope.rowHeight + "px" };
		if (row.isAggRow) {
			ret.left = row.offsetLeft;
		}
		return ret;
	};
	$scope.canvasStyle = function() {
		return { "height": grid.maxCanvasHt + "px" };
	};
	$scope.headerScrollerStyle = function() {
		return { "height": grid.config.headerRowHeight + "px" };
	};
	$scope.topPanelStyle = function() {
		return { "width": grid.rootDim.outerWidth + "px", "height": $scope.topPanelHeight() + "px" };
	};
	$scope.headerStyle = function() {
		return { "width": grid.rootDim.outerWidth + "px", "height": grid.config.headerRowHeight + "px" };
	};
	$scope.groupPanelStyle = function () {
		return { "width": grid.rootDim.outerWidth + "px", "height": "32px" };
	};
	$scope.viewportStyle = function() {
		return { "width": grid.rootDim.outerWidth + "px", "height": $scope.viewportDimHeight() + "px" };
	};
	$scope.footerStyle = function() {
		return { "width": grid.rootDim.outerWidth + "px", "height": $scope.footerRowHeight + "px" };
	};
};
ngGridDirectives.directive('ngCellHasFocus', ['$domUtilityService',
	function (domUtilityService) {
		var focusOnInputElement = function($scope, elm) {
			$scope.isFocused = true;
			domUtilityService.digest($scope);

			$scope.$broadcast('ngGridEventStartCellEdit');

			$scope.$on('ngGridEventEndCellEdit', function() {
				$scope.isFocused = false;
				domUtilityService.digest($scope);
			});
		};

		return function($scope, elm) {
			var isFocused = false;
			var isCellEditableOnMouseDown = false;

			$scope.editCell = function() {
				if(!$scope.enableCellEditOnFocus) {
					setTimeout(function() {
						focusOnInputElement($scope,elm);
					}, 0);
				}
			};
			elm.bind('mousedown', function(evt) {
				if($scope.enableCellEditOnFocus) {
					isCellEditableOnMouseDown = true;
				} else {
					elm.focus();
				}
				return true;
			});
			elm.bind('click', function(evt) {
				if($scope.enableCellEditOnFocus) {
					evt.preventDefault();
					isCellEditableOnMouseDown = false;
					focusOnInputElement($scope,elm);
				}
			});
			elm.bind('focus', function(evt) {
				isFocused = true;
				if($scope.enableCellEditOnFocus && !isCellEditableOnMouseDown) {
					focusOnInputElement($scope,elm);
				}
				return true;
			});
			elm.bind('blur', function() {
				isFocused = false;
				return true;
			});
			elm.bind('keydown', function(evt) {
				if(!$scope.enableCellEditOnFocus) {
					if (isFocused && evt.keyCode !== 37 && evt.keyCode !== 38 && evt.keyCode !== 39 && evt.keyCode !== 40 && evt.keyCode !== 9 && !evt.shiftKey && evt.keyCode !== 13) {
						focusOnInputElement($scope,elm);
					}
					if (isFocused && evt.shiftKey && (evt.keyCode >= 65 && evt.keyCode <= 90)) {
						focusOnInputElement($scope, elm);
					}
					if (evt.keyCode === 27) {
						elm.focus();
					}
				}
				return true;
			});
		};
	}]);
ngGridDirectives.directive('ngCellText',
  function () {
	  return function(scope, elm) {
		  elm.bind('mouseover', function(evt) {
			  evt.preventDefault();
			  elm.css({
				  'cursor': 'text'
			  });
		  });
		  elm.bind('mouseleave', function(evt) {
			  evt.preventDefault();
			  elm.css({
				  'cursor': 'default'
			  });
		  });
	  };
  });
ngGridDirectives.directive('ngCell', ['$compile', '$domUtilityService', function ($compile, domUtilityService) {
	var ngCell = {
		scope: false,
		compile: function() {
			return {
				pre: function($scope, iElement) {
					var html;
					var cellTemplate = $scope.col.cellTemplate.replace(COL_FIELD, 'row.entity.' + $scope.col.field);

					if ($scope.col.enableCellEdit) {
						html =  $scope.col.cellEditTemplate;
						html = html.replace(DISPLAY_CELL_TEMPLATE, cellTemplate);
						html = html.replace(EDITABLE_CELL_TEMPLATE, $scope.col.editableCellTemplate.replace(COL_FIELD, 'row.entity.' + $scope.col.field));
					} else {
						html = cellTemplate;
					}

					var cellElement = $compile(html)($scope);

					if ($scope.enableCellSelection && cellElement[0].className.indexOf('ngSelectionCell') === -1) {
						cellElement[0].setAttribute('tabindex', 0);
						cellElement.addClass('ngCellElement');
					}

					iElement.append(cellElement);
				},
				post: function($scope, iElement) {
					if ($scope.enableCellSelection) {
						$scope.domAccessProvider.selectionHandlers($scope, iElement);
					}

					$scope.$on('ngGridEventDigestCell', function() {
						domUtilityService.digest($scope);
					});
				}
			};
		}
	};

	return ngCell;
}]);
/*
 * Defines the ui-if tag. This removes/adds an element from the dom depending on a condition
 * Originally created by @tigbro, for the @jquery-mobile-angular-adapter
 * https://github.com/tigbro/jquery-mobile-angular-adapter
 */
ngGridDirectives.directive('ngEditCellIf', [function () {
  return {
	transclude: 'element',
	priority: 1000,
	terminal: true,
	restrict: 'A',
	compile: function (e, a, transclude) {
	  return function (scope, element, attr) {

		var childElement;
		var childScope;

		scope.$watch(attr['ngEditCellIf'], function (newValue) {
		  if (childElement) {
			childElement.remove();
			childElement = undefined;
		  }
		  if (childScope) {
			childScope.$destroy();
			childScope = undefined;
		  }

		  if (newValue) {
			childScope = scope.$new();
			transclude(childScope, function (clone) {
			  childElement = clone;
			  element.after(clone);
			});
		  }
		});
	  };
	}
  };
}]);
ngGridDirectives.directive('ngGridFooter', ['$compile', '$templateCache', function ($compile, $templateCache) {
	var ngGridFooter = {
		scope: false,
		compile: function () {
			return {
				pre: function ($scope, iElement) {
					if (iElement.children().length === 0) {
						iElement.append($compile($templateCache.get($scope.gridId + 'footerTemplate.html'))($scope));
					}
				}
			};
		}
	};
	return ngGridFooter;
}]);
ngGridDirectives.directive('ngGridMenu', ['$compile', '$templateCache', function ($compile, $templateCache) {
	var ngGridMenu = {
		scope: false,
		compile: function () {
			return {
				pre: function ($scope, iElement) {
					if (iElement.children().length === 0) {
						iElement.append($compile($templateCache.get($scope.gridId + 'menuTemplate.html'))($scope));
					}
				}
			};
		}
	};
	return ngGridMenu;
}]);
ngGridDirectives.directive('ngGrid', ['$compile', '$filter', '$templateCache', '$sortService', '$domUtilityService', '$utilityService', '$timeout', '$parse', '$http', '$q', function ($compile, $filter, $templateCache, sortService, domUtilityService, $utils, $timeout, $parse, $http, $q) {
	var ngGridDirective = {
		scope: true,
		compile: function() {
			return {
				pre: function($scope, iElement, iAttrs) {
					var $element = $(iElement);
					var options = $scope.$eval(iAttrs.ngGrid);
					options.gridDim = new ngDimension({ outerHeight: $($element).height(), outerWidth: $($element).width() });

					var grid = new ngGrid($scope, options, sortService, domUtilityService, $filter, $templateCache, $utils, $timeout, $parse, $http, $q);
					return grid.init().then(function() {
						// if columndefs are a string of a property ont he scope watch for changes and rebuild columns.
						if (typeof options.columnDefs === "string") {
							$scope.$parent.$watch(options.columnDefs, function (a) {
								if (!a) {
									grid.refreshDomSizes();
									grid.buildColumns();
									return;
								}
								// we have to set this to false in case we want to autogenerate columns with no initial data.
								grid.lateBoundColumns = false;
								$scope.columns = [];
								grid.config.columnDefs = a;
								grid.buildColumns();
								grid.eventProvider.assignEvents();
								domUtilityService.RebuildGrid($scope, grid);
							}, true);
						}
						else {
							grid.buildColumns();
						}

						// Watch totalServerItems if it's a string
						if (typeof options.totalServerItems === "string") {
							$scope.$parent.$watch(options.totalServerItems, function (newTotal, oldTotal) {
								// If the newTotal is not defined (like during init, set the value to 0)
								if (!angular.isDefined(newTotal)) {
									$scope.totalServerItems = 0;
								}
								// Otherwise set the value to the new total
								else {
									$scope.totalServerItems = newTotal;
								}
							});
						}
						// If it's NOT a string, then just set totalServerItems to 0 since they should only be setting this if using a string
						else {
							$scope.totalServerItems = 0;
						}

						// if it is a string we can watch for data changes. otherwise you won't be able to update the grid data
						if (typeof options.data === "string") {
							var dataWatcher = function (a) {
								// make a temporary copy of the data
								grid.data = $.extend([], a);
								grid.rowFactory.fixRowCache();
								angular.forEach(grid.data, function (item, j) {
									var indx = grid.rowMap[j] || j;
									if (grid.rowCache[indx]) {
										grid.rowCache[indx].ensureEntity(item);
									}
									grid.rowMap[indx] = j;
								});
								grid.searchProvider.evalFilter();
								grid.configureColumnWidths();
								grid.refreshDomSizes();
								if (grid.config.sortInfo.fields.length > 0) {
									grid.sortColumnsInit();
									$scope.$emit('ngGridEventSorted', grid.config.sortInfo);
								}
								$scope.$emit("ngGridEventData", grid.gridId);
							};
							$scope.$parent.$watch(options.data, dataWatcher);
							$scope.$parent.$watch(options.data + '.length', function() {
								dataWatcher($scope.$eval(options.data));
							});
						}

						grid.footerController = new ngFooter($scope, grid);
						//set the right styling on the container
						iElement.addClass("ngGrid").addClass(grid.gridId.toString());
						if (!options.enableHighlighting) {
							iElement.addClass("unselectable");
						}
						if (options.jqueryUITheme) {
							iElement.addClass('ui-widget');
						}
						iElement.append($compile($templateCache.get('gridTemplate.html'))($scope)); // make sure that if any of these change, we re-fire the calc logic
						//walk the element's graph and the correct properties on the grid
						domUtilityService.AssignGridContainers($scope, iElement, grid);
						//now use the manager to assign the event handlers
						grid.eventProvider = new ngEventProvider(grid, $scope, domUtilityService, $timeout);

						// method for user to select a specific row programatically
						options.selectRow = function (rowIndex, state) {
							if (grid.rowCache[rowIndex]) {
								if (grid.rowCache[rowIndex].clone) {
									grid.rowCache[rowIndex].clone.setSelection(state ? true : false);
								}
								grid.rowCache[rowIndex].setSelection(state ? true : false);
							}
						};
						// method for user to select the row by data item programatically
						options.selectItem = function (itemIndex, state) {
							options.selectRow(grid.rowMap[itemIndex], state);
						};
						// method for user to set the select all state.
						options.selectAll = function (state) {
							$scope.toggleSelectAll(state);
						};
						// method for user to set the select all state on visible items.
						options.selectVisible = function (state) {
							$scope.toggleSelectAll(state, true);
						};
						// method for user to set the groups programatically
						options.groupBy = function (field) {
							if (field) {
								$scope.groupBy($scope.columns.filter(function(c) {
									return c.field === field;
								})[0]);
							} else {
								var arr = $.extend(true, [], $scope.configGroups);
								angular.forEach(arr, $scope.groupBy);
							}
						};
						// method for user to set the sort field programatically
						options.sortBy = function (field) {
							var col = $scope.columns.filter(function (c) {
								return c.field === field;
							})[0];
							if (col) {
								col.sort();
							}
						};
						// the grid Id, entity, scope for convenience
						options.gridId = grid.gridId;
						options.ngGrid = grid;
						options.$gridScope = $scope;
						options.$gridServices = { SortService: sortService, DomUtilityService: domUtilityService, UtilityService: $utils };
						$scope.$on('ngGridEventDigestGrid', function(){
							domUtilityService.digest($scope.$parent);
						});

						$scope.$on('ngGridEventDigestGridParent', function(){
							domUtilityService.digest($scope.$parent);
						});
						// set up the columns
						$scope.$evalAsync(function() {
							$scope.adjustScrollLeft(0);
						});
						//initialize plugins.
						angular.forEach(options.plugins, function (p) {
							if (typeof p === "function") {
								p = new p(); //If p is a function, then we assume it is a class.
							}
							p.init($scope.$new(), grid, options.$gridServices);
							options.plugins[$utils.getInstanceType(p)] = p;
						});
						//send initi finalize notification.
						if (typeof options.init === "function") {
							options.init(grid, $scope);
						}
						return null;
					});
				}
			};
		}
	};
	return ngGridDirective;
}]);

ngGridDirectives.directive('ngHeaderCell', ['$compile', function($compile) {
	var ngHeaderCell = {
		scope: false,
		compile: function() {
			return {
				pre: function($scope, iElement) {
					iElement.append($compile($scope.col.headerCellTemplate)($scope));
				}
			};
		}
	};
	return ngHeaderCell;
}]);
ngGridDirectives.directive('ngInput', [function() {
	return {
		require: 'ngModel',
		link: function (scope, elm, attrs, ngModel) {
			// Store the initial cell value so we can reset to it if need be
			var oldCellValue;
			var dereg = scope.$watch('ngModel', function() {
				oldCellValue = ngModel.$modelValue;
				dereg(); // only run this watch once, we don't want to overwrite our stored value when the input changes
			});

			elm.bind('keydown', function(evt) {
				switch (evt.keyCode) {
					case 37: // Left arrow
					case 38: // Up arrow
					case 39: // Right arrow
					case 40: // Down arrow
						evt.stopPropagation();
						break;
					case 27: // Esc (reset to old value)
						if (!scope.$$phase) {
							scope.$apply(function() {
								ngModel.$setViewValue(oldCellValue);
								elm.blur();
							});
						}
						break;
					case 13: // Enter (Leave Field)
						if(scope.enableCellEditOnFocus && scope.totalFilteredItemsLength() - 1 > scope.row.rowIndex && scope.row.rowIndex > 0  || scope.enableCellEdit) {
							elm.blur();
						}
						break;
				}

				return true;
			});

			elm.bind('click', function(evt) {
				evt.stopPropagation();
			});

			elm.bind('mousedown', function(evt) {
				evt.stopPropagation();
			});

			scope.$on('ngGridEventStartCellEdit', function () {
				elm.focus();
				elm.select();
			});

			angular.element(elm).bind('blur', function () {
				scope.$emit('ngGridEventEndCellEdit');
			});
		}
	};
}]);
ngGridDirectives.directive('ngRow', ['$compile', '$domUtilityService', '$templateCache', function ($compile, domUtilityService, $templateCache) {
	var ngRow = {
		scope: false,
		compile: function() {
			return {
				pre: function($scope, iElement) {
					$scope.row.elm = iElement;
					if ($scope.row.clone) {
						$scope.row.clone.elm = iElement;
					}
					if ($scope.row.isAggRow) {
						var html = $templateCache.get($scope.gridId + 'aggregateTemplate.html');
						if ($scope.row.aggLabelFilter) {
							html = html.replace(CUSTOM_FILTERS, '| ' + $scope.row.aggLabelFilter);
						} else {
							html = html.replace(CUSTOM_FILTERS, "");
						}
						iElement.append($compile(html)($scope));
					} else {
						iElement.append($compile($templateCache.get($scope.gridId + 'rowTemplate.html'))($scope));
					}
		  $scope.$on('ngGridEventDigestRow', function(){
			domUtilityService.digest($scope);
		  });
				}
			};
		}
	};
	return ngRow;
}]);
ngGridDirectives.directive('ngViewport', [function() {
	return function($scope, elm) {
		var isMouseWheelActive;
		var prevScollLeft;
		var prevScollTop = 0;
		elm.bind('scroll', function(evt) {
			var scrollLeft = evt.target.scrollLeft,
				scrollTop = evt.target.scrollTop;
			if ($scope.$headerContainer) {
				$scope.$headerContainer.scrollLeft(scrollLeft);
			}
			$scope.adjustScrollLeft(scrollLeft);
			$scope.adjustScrollTop(scrollTop);
			if (!$scope.$root.$$phase) {
				$scope.$digest();
			}
			prevScollLeft = scrollLeft;
			prevScollTop = scrollTop;
			isMouseWheelActive = false;
			return true;
		});
		elm.bind("mousewheel DOMMouseScroll", function() {
			isMouseWheelActive = true;
			if (elm.focus) { elm.focus(); }
			return true;
		});
		if (!$scope.enableCellSelection) {
			$scope.domAccessProvider.selectionHandlers($scope, elm);
		}
	};
}]);
window.ngGrid.i18n['da'] = {
	ngAggregateLabel: 'artikler',
	ngGroupPanelDescription: 'GrupÃƒÂ©r rÃƒÂ¦kker udfra en kolonne ved at trÃƒÂ¦kke dens overskift hertil.',
	ngSearchPlaceHolder: 'SÃƒÂ¸g...',
	ngMenuText: 'VÃƒÂ¦lg kolonner:',
	ngShowingItemsLabel: 'Viste rÃƒÂ¦kker:',
	ngTotalItemsLabel: 'RÃƒÂ¦kker totalt:',
	ngSelectedItemsLabel: 'Valgte rÃƒÂ¦kker:',
	ngPageSizeLabel: 'Side stÃƒÂ¸rrelse:',
	ngPagerFirstTitle: 'FÃƒÂ¸rste side',
	ngPagerNextTitle: 'NÃƒÂ¦ste side',
	ngPagerPrevTitle: 'Forrige side',
	ngPagerLastTitle: 'Sidste side'
};
window.ngGrid.i18n['de'] = {
	ngAggregateLabel: 'artikel',
	ngGroupPanelDescription: 'Ziehen Sie eine SpaltenÃƒÂ¼berschrift hier und legen Sie es der Gruppe nach dieser Spalte.',
	ngSearchPlaceHolder: 'Suche...',
	ngMenuText: 'Spalten auswÃƒÂ¤hlen:',
	ngShowingItemsLabel: 'Zeige Artikel:',
	ngTotalItemsLabel: 'Meiste Artikel:',
	ngSelectedItemsLabel: 'AusgewÃƒÂ¤hlte Artikel:',
	ngPageSizeLabel: 'GrÃƒÂ¶ÃƒÅ¸e Seite:',
	ngPagerFirstTitle: 'Erste Page',
	ngPagerNextTitle: 'NÃƒÂ¤chste Page',
	ngPagerPrevTitle: 'Vorherige Page',
	ngPagerLastTitle: 'Letzte Page'
};
window.ngGrid.i18n['en'] = {
	ngAggregateLabel: 'items',
	ngGroupPanelDescription: 'Drag a column header here and drop it to group by that column.',
	ngSearchPlaceHolder: 'Search...',
	ngMenuText: 'Choose Columns:',
	ngShowingItemsLabel: 'Showing Items:',
	ngTotalItemsLabel: 'Total Items:',
	ngSelectedItemsLabel: 'Selected Items:',
	ngPageSizeLabel: 'Page Size:',
	ngPagerFirstTitle: 'First Page',
	ngPagerNextTitle: 'Next Page',
	ngPagerPrevTitle: 'Previous Page',
	ngPagerLastTitle: 'Last Page'
};
window.ngGrid.i18n['es'] = {
	ngAggregateLabel: 'ArtÃƒÂ­culos',
	ngGroupPanelDescription: 'Arrastre un encabezado de columna aquÃƒÂ­ y soltarlo para agrupar por esa columna.',
	ngSearchPlaceHolder: 'Buscar...',
	ngMenuText: 'Elegir columnas:',
	ngShowingItemsLabel: 'ArtÃƒÂ­culos Mostrando:',
	ngTotalItemsLabel: 'ArtÃƒÂ­culos Totales:',
	ngSelectedItemsLabel: 'ArtÃƒÂ­culos Seleccionados:',
	ngPageSizeLabel: 'TamaÃƒÂ±o de PÃƒÂ¡gina:',
	ngPagerFirstTitle: 'Primera PÃƒÂ¡gina',
	ngPagerNextTitle: 'PÃƒÂ¡gina Siguiente',
	ngPagerPrevTitle: 'PÃƒÂ¡gina Anterior',
	ngPagerLastTitle: 'ÃƒÅ¡ltima PÃƒÂ¡gina'
};
window.ngGrid.i18n['fr'] = {
	ngAggregateLabel: 'articles',
	ngGroupPanelDescription: 'Faites glisser un en-tÃƒÂªte de colonne ici et dÃƒÂ©posez-le vers un groupe par cette colonne.',
	ngSearchPlaceHolder: 'Recherche...',
	ngMenuText: 'Choisir des colonnes:',
	ngShowingItemsLabel: 'Articles Affichage des:',
	ngTotalItemsLabel: 'Nombre total d\'articles:',
	ngSelectedItemsLabel: 'Ãƒâ€°lÃƒÂ©ments Articles:',
	ngPageSizeLabel: 'Taille de page:',
	ngPagerFirstTitle: 'PremiÃƒÂ¨re page',
	ngPagerNextTitle: 'Page Suivante',
	ngPagerPrevTitle: 'Page prÃƒÂ©cÃƒÂ©dente',
	ngPagerLastTitle: 'DerniÃƒÂ¨re page'
};
window.ngGrid.i18n['pt-br'] = {
	ngAggregateLabel: 'items',
	ngGroupPanelDescription: 'Arraste e solte uma coluna aqui para agrupar por essa coluna',
	ngSearchPlaceHolder: 'Procurar...',
	ngMenuText: 'Selecione as colunas:',
	ngShowingItemsLabel: 'Mostrando os Items:',
	ngTotalItemsLabel: 'Total de Items:',
	ngSelectedItemsLabel: 'Items Selecionados:',
	ngPageSizeLabel: 'Tamanho da PÃƒÂ¡gina:',
	ngPagerFirstTitle: 'Primeira PÃƒÂ¡gina',
	ngPagerNextTitle: 'PrÃƒÂ³xima PÃƒÂ¡gina',
	ngPagerPrevTitle: 'PÃƒÂ¡gina Anterior',
	ngPagerLastTitle: 'ÃƒÅ¡ltima PÃƒÂ¡gina'
};
window.ngGrid.i18n['zh-cn'] = {
	ngAggregateLabel: 'Ã¦ÂÂ¡Ã§â€ºÂ®',
	ngGroupPanelDescription: 'Ã¦â€¹â€“Ã¦â€ºÂ³Ã¨Â¡Â¨Ã¥Â¤Â´Ã¥Ë†Â°Ã¦Â­Â¤Ã¥Â¤â€žÃ¤Â»Â¥Ã¨Â¿â€ºÃ¨Â¡Å’Ã¥Ë†â€ Ã§Â»â€ž',
	ngSearchPlaceHolder: 'Ã¦ÂÅ“Ã§Â´Â¢...',
	ngMenuText: 'Ã¦â€¢Â°Ã¦ÂÂ®Ã¥Ë†â€ Ã§Â»â€žÃ¤Â¸Å½Ã©â‚¬â€°Ã¦â€¹Â©Ã¥Ë†â€”Ã¯Â¼Å¡',
	ngShowingItemsLabel: 'Ã¥Â½â€œÃ¥â€°ÂÃ¦ËœÂ¾Ã§Â¤ÂºÃ¦ÂÂ¡Ã§â€ºÂ®Ã¯Â¼Å¡',
	ngTotalItemsLabel: 'Ã¦ÂÂ¡Ã§â€ºÂ®Ã¦â‚¬Â»Ã¦â€¢Â°Ã¯Â¼Å¡',
	ngSelectedItemsLabel: 'Ã©â‚¬â€°Ã¤Â¸Â­Ã¦ÂÂ¡Ã§â€ºÂ®Ã¯Â¼Å¡',
	ngPageSizeLabel: 'Ã¦Â¯ÂÃ©Â¡ÂµÃ¦ËœÂ¾Ã§Â¤ÂºÃ¦â€¢Â°Ã¯Â¼Å¡',
	ngPagerFirstTitle: 'Ã¥â€ºÅ¾Ã¥Ë†Â°Ã©Â¦â€“Ã©Â¡Âµ',
	ngPagerNextTitle: 'Ã¤Â¸â€¹Ã¤Â¸â‚¬Ã©Â¡Âµ',
	ngPagerPrevTitle: 'Ã¤Â¸Å Ã¤Â¸â‚¬Ã©Â¡Âµ',
	ngPagerLastTitle: 'Ã¥â€°ÂÃ¥Â¾â‚¬Ã¥Â°Â¾Ã©Â¡Âµ'
};

window.ngGrid.i18n['zh-tw'] = {
	ngAggregateLabel: 'Ã§Â­â€ ',
	ngGroupPanelDescription: 'Ã¦â€¹â€“Ã¦â€¹â€°Ã¨Â¡Â¨Ã© Â­Ã¥Ë†Â°Ã¦Â­Â¤Ã¨â„¢â€¢Ã¤Â»Â¥Ã©â‚¬Â²Ã¨Â¡Å’Ã¥Ë†â€ Ã§Âµâ€ž',
	ngSearchPlaceHolder: 'Ã¦ÂÅ“Ã¥Â°â€¹...',
	ngMenuText: 'Ã©ÂÂ¸Ã¦â€œâ€¡Ã¦Â¬â€žÃ¤Â½ÂÃ¯Â¼Å¡',
	ngShowingItemsLabel: 'Ã§â€ºÂ®Ã¥â€°ÂÃ©Â¡Â¯Ã§Â¤ÂºÃ§Â­â€ Ã¦â€¢Â¸Ã¯Â¼Å¡',
	ngTotalItemsLabel: 'Ã§Â¸Â½Ã§Â­â€ Ã¦â€¢Â¸Ã¯Â¼Å¡',
	ngSelectedItemsLabel: 'Ã©ÂÂ¸Ã¥Ââ€“Ã§Â­â€ Ã¦â€¢Â¸Ã¯Â¼Å¡',
	ngPageSizeLabel: 'Ã¦Â¯ÂÃ© ÂÃ©Â¡Â¯Ã§Â¤ÂºÃ¯Â¼Å¡',
	ngPagerFirstTitle: 'Ã§Â¬Â¬Ã¤Â¸â‚¬Ã© Â',
	ngPagerNextTitle: 'Ã¤Â¸â€¹Ã¤Â¸â‚¬Ã© Â',
	ngPagerPrevTitle: 'Ã¤Â¸Å Ã¤Â¸â‚¬Ã© Â',
	ngPagerLastTitle: 'Ã¦Å“â‚¬Ã¥Â¾Å’Ã© Â'
};

angular.module("ngGrid").run(["$templateCache", function($templateCache) {

  $templateCache.put("aggregateTemplate.html",
	"<div ng-click=\"row.toggleExpand()\" ng-style=\"rowStyle(row)\" class=\"ngAggregate\">" +
	"    <span class=\"ngAggregateText\">{{row.label CUSTOM_FILTERS}} ({{row.totalChildren()}} {{AggItemsLabel}})</span>" +
	"    <div class=\"{{row.aggClass()}}\"></div>" +
	"</div>" +
	""
  );

  $templateCache.put("cellEditTemplate.html",
	"<div ng-cell-has-focus ng-dblclick=\"editCell()\">" +
	"	<div ng-edit-cell-if=\"!isFocused\">	" +
	"		DISPLAY_CELL_TEMPLATE" +
	"	</div>" +
	"	<div ng-edit-cell-if=\"isFocused\">" +
	"		EDITABLE_CELL_TEMPLATE" +
	"	</div>" +
	"</div>"
  );

  $templateCache.put("cellTemplate.html",
	"<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD CUSTOM_FILTERS}}</span></div>"
  );

  $templateCache.put("checkboxCellTemplate.html",
	"<div class=\"ngSelectionCell\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-checked=\"row.selected\" /></div>"
  );

  $templateCache.put("checkboxHeaderTemplate.html",
	"<input class=\"ngSelectionHeader\" type=\"checkbox\" ng-show=\"multiSelect\" ng-model=\"allSelected\" ng-change=\"toggleSelectAll(allSelected, true)\"/>"
  );

  $templateCache.put("editableCellTemplate.html",
	"<input ng-class=\"'colt' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\" />"
  );

  $templateCache.put("footerTemplate.html",
	"<div ng-show=\"showFooter\" class=\"ngFooterPanel\" ng-class=\"{'ui-widget-content': jqueryUITheme, 'ui-corner-bottom': jqueryUITheme}\" ng-style=\"footerStyle()\">" +
	"    <div class=\"ngTotalSelectContainer\" >" +
	"        <div class=\"ngFooterTotalItems\" ng-class=\"{'ngNoMultiSelect': !multiSelect}\" >" +
	"            <span class=\"ngLabel\">{{i18n.ngTotalItemsLabel}} {{maxRows()}}</span><span ng-show=\"filterText.length > 0\" class=\"ngLabel\">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span>" +
	"        </div>" +
	"        <div class=\"ngFooterSelectedItems\" ng-show=\"multiSelect\">" +
	"            <span class=\"ngLabel\">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span>" +
	"        </div>" +
	"    </div>" +
	"    <div class=\"ngPagerContainer\" style=\"float: right; margin-top: 10px;\" ng-show=\"enablePaging\" ng-class=\"{'ngNoMultiSelect': !multiSelect}\">" +
	"        <div style=\"float:left; margin-right: 10px;\" class=\"ngRowCountPicker\">" +
	"            <span style=\"float: left; margin-top: 3px;\" class=\"ngLabel\">{{i18n.ngPageSizeLabel}}</span>" +
	"            <select style=\"float: left;height: 27px; width: 100px\" ng-model=\"pagingOptions.pageSize\" >" +
	"                <option ng-repeat=\"size in pagingOptions.pageSizes\">{{size}}</option>" +
	"            </select>" +
	"        </div>" +
	"        <div style=\"float:left; margin-right: 10px; line-height:25px;\" class=\"ngPagerControl\" style=\"float: left; min-width: 135px;\">" +
	"            <button class=\"ngPagerButton\" ng-click=\"pageToFirst()\" ng-disabled=\"cantPageBackward()\" title=\"{{i18n.ngPagerFirstTitle}}\"><div class=\"ngPagerFirstTriangle\"><div class=\"ngPagerFirstBar\"></div></div></button>" +
	"            <button class=\"ngPagerButton\" ng-click=\"pageBackward()\" ng-disabled=\"cantPageBackward()\" title=\"{{i18n.ngPagerPrevTitle}}\"><div class=\"ngPagerFirstTriangle ngPagerPrevTriangle\"></div></button>" +
	"            <input class=\"ngPagerCurrent\" min=\"1\" max=\"{{maxPages()}}\" type=\"number\" style=\"width:50px; height: 24px; margin-top: 1px; padding: 0 4px;\" ng-model=\"pagingOptions.currentPage\"/>" +
	"            <button class=\"ngPagerButton\" ng-click=\"pageForward()\" ng-disabled=\"cantPageForward()\" title=\"{{i18n.ngPagerNextTitle}}\"><div class=\"ngPagerLastTriangle ngPagerNextTriangle\"></div></button>" +
	"            <button class=\"ngPagerButton\" ng-click=\"pageToLast()\" ng-disabled=\"cantPageToLast()\" title=\"{{i18n.ngPagerLastTitle}}\"><div class=\"ngPagerLastTriangle\"><div class=\"ngPagerLastBar\"></div></div></button>" +
	"        </div>" +
	"    </div>" +
	"</div>"
  );

  $templateCache.put("gridTemplate.html",
	"<div class=\"ngTopPanel\" ng-class=\"{'ui-widget-header':jqueryUITheme, 'ui-corner-top': jqueryUITheme}\" ng-style=\"topPanelStyle()\">" +
	"    <div class=\"ngGroupPanel\" ng-show=\"showGroupPanel()\" ng-style=\"groupPanelStyle()\">" +
	"        <div class=\"ngGroupPanelDescription\" ng-show=\"configGroups.length == 0\">{{i18n.ngGroupPanelDescription}}</div>" +
	"        <ul ng-show=\"configGroups.length > 0\" class=\"ngGroupList\">" +
	"            <li class=\"ngGroupItem\" ng-repeat=\"group in configGroups\">" +
	"                <span class=\"ngGroupElement\">" +
	"                    <span class=\"ngGroupName\">{{group.displayName}}" +
	"                        <span ng-click=\"removeGroup($index)\" class=\"ngRemoveGroup\">x</span>" +
	"                    </span>" +
	"                    <span ng-hide=\"$last\" class=\"ngGroupArrow\"></span>" +
	"                </span>" +
	"            </li>" +
	"        </ul>" +
	"    </div>" +
	"    <div class=\"ngHeaderContainer\" ng-style=\"headerStyle()\">" +
	"        <div class=\"ngHeaderScroller\" ng-style=\"headerScrollerStyle()\" ng-include=\"gridId + 'headerRowTemplate.html'\"></div>" +
	"    </div>" +
	"    <div ng-grid-menu></div>" +
	"</div>" +
	"<div class=\"ngViewport\" unselectable=\"on\" ng-viewport ng-class=\"{'ui-widget-content': jqueryUITheme}\" ng-style=\"viewportStyle()\">" +
	"    <div class=\"ngCanvas\" ng-style=\"canvasStyle()\">" +
	"        <div ng-style=\"rowStyle(row)\" ng-repeat=\"row in renderedRows\" ng-click=\"row.toggleSelected($event)\" ng-class=\"row.alternatingRowClass()\" ng-row></div>" +
	"    </div>" +
	"</div>" +
	"<div ng-grid-footer></div>" +
	""
  );

  $templateCache.put("headerCellTemplate.html",
	"<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
	"    <div ng-click=\"col.sort($event)\" ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>" +
	"    <div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
	"    <div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div>" +
	"    <div class=\"ngSortPriority\">{{col.sortPriority}}</div>" +
	"    <div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div>" +
	"</div>" +
	"<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>"
  );

  $templateCache.put("headerRowTemplate.html",
	"<div ng-style=\"{ height: col.headerRowHeight }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngHeaderCell\">" +
	"	<div class=\"ngVerticalBar\" ng-style=\"{height: col.headerRowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>" +
	"	<div ng-header-cell></div>" +
	"</div>"
  );

  $templateCache.put("menuTemplate.html",
	"<div ng-show=\"showColumnMenu || showFilter\"  class=\"ngHeaderButton\" ng-click=\"toggleShowMenu()\">" +
	"    <div class=\"ngHeaderButtonArrow\"></div>" +
	"</div>" +
	"<div ng-show=\"showMenu\" class=\"ngColMenu\">" +
	"    <div ng-show=\"showFilter\">" +
	"        <input placeholder=\"{{i18n.ngSearchPlaceHolder}}\" type=\"text\" ng-model=\"filterText\"/>" +
	"    </div>" +
	"    <div ng-show=\"showColumnMenu\">" +
	"        <span class=\"ngMenuText\">{{i18n.ngMenuText}}</span>" +
	"        <ul class=\"ngColList\">" +
	"            <li class=\"ngColListItem\" ng-repeat=\"col in columns | ngColumns\">" +
	"                <label><input ng-disabled=\"col.pinned\" type=\"checkbox\" class=\"ngColListCheckbox\" ng-model=\"col.visible\"/>{{col.displayName}}</label>" +
	"				<a title=\"Group By\" ng-class=\"col.groupedByClass()\" ng-show=\"col.groupable && col.visible\" ng-click=\"groupBy(col)\"></a>" +
	"				<span class=\"ngGroupingNumber\" ng-show=\"col.groupIndex > 0\">{{col.groupIndex}}</span>          " +
	"            </li>" +
	"        </ul>" +
	"    </div>" +
	"</div>"
  );

  $templateCache.put("rowTemplate.html",
	"<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">" +
	"	<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>" +
	"	<div ng-cell></div>" +
	"</div>"
  );

}]);

}(window, jQuery));

//} FIM DE NgGRID

//{ ngLocale
angular.module("ngLocale", [], ["$provide", function($provide) {
var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
$provide.value("$locale", {"DATETIME_FORMATS":{"MONTH":["janeiro","fevereiro","março","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro"],"SHORTMONTH":["jan","fev","mar","abr","mai","jun","jul","ago","set","out","nov","dez"],"DAY":["domingo","segunda-feira","terça-feira","quarta-feira","quinta-feira","sexta-feira","sábado"],"SHORTDAY":["dom","seg","ter","qua","qui","sex","sÃƒÂ¡b"],"AMPMS":["AM","PM"],"medium":"dd/MM/yyyy HH:mm:ss","short":"dd/MM/yy HH:mm","fullDate":"EEEE, d 'de' MMMM 'de' y","longDate":"d 'de' MMMM 'de' y","mediumDate":"dd/MM/yyyy","shortDate":"dd/MM/yy","mediumTime":"HH:mm:ss","shortTime":"HH:mm"},"NUMBER_FORMATS":{"DECIMAL_SEP":",","GROUP_SEP":".","PATTERNS":[{"minInt":1,"minFrac":0,"macFrac":0,"posPre":"","posSuf":"","negPre":"-","negSuf":"","gSize":3,"lgSize":3,"maxFrac":3},{"minInt":1,"minFrac":2,"macFrac":0,"posPre":"\u00A4","posSuf":"","negPre":"(\u00A4","negSuf":")","gSize":3,"lgSize":3,"maxFrac":2}],"CURRENCY_SYM":"R$"},"pluralCat":function (n) {  if (n == 1) {    return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;},"id":"pt-br"});
}]);

//}FIM DE Angular-locale_pt-br

//{ Angular Sanitize v1.6.6
	/*
 AngularJS v1.6.6
 (c) 2010-2017 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(s,d){'use strict';function J(d){var k=[];w(k,B).chars(d);return k.join("")}var x=d.$$minErr("$sanitize"),C,k,D,E,p,B,F,G,w;d.module("ngSanitize",[]).provider("$sanitize",function(){function g(a,e){var c={},b=a.split(","),f;for(f=0;f<b.length;f++)c[e?p(b[f]):b[f]]=!0;return c}function K(a){for(var e={},c=0,b=a.length;c<b;c++){var f=a[c];e[f.name]=f.value}return e}function H(a){return a.replace(/&/g,"&amp;").replace(L,function(a){var c=a.charCodeAt(0);a=a.charCodeAt(1);return"&#"+(1024*(c-
55296)+(a-56320)+65536)+";"}).replace(M,function(a){return"&#"+a.charCodeAt(0)+";"}).replace(/</g,"&lt;").replace(/>/g,"&gt;")}function I(a){for(;a;){if(a.nodeType===s.Node.ELEMENT_NODE)for(var e=a.attributes,c=0,b=e.length;c<b;c++){var f=e[c],h=f.name.toLowerCase();if("xmlns:ns1"===h||0===h.lastIndexOf("ns1:",0))a.removeAttributeNode(f),c--,b--}(e=a.firstChild)&&I(e);a=t("nextSibling",a)}}function t(a,e){var c=e[a];if(c&&F.call(e,c))throw x("elclob",e.outerHTML||e.outerText);return c}var y=!1;this.$get=
["$$sanitizeUri",function(a){y&&k(n,z);return function(e){var c=[];G(e,w(c,function(b,c){return!/^unsafe:/.test(a(b,c))}));return c.join("")}}];this.enableSvg=function(a){return E(a)?(y=a,this):y};C=d.bind;k=d.extend;D=d.forEach;E=d.isDefined;p=d.lowercase;B=d.noop;G=function(a,e){null===a||void 0===a?a="":"string"!==typeof a&&(a=""+a);var c=u(a);if(!c)return"";var b=5;do{if(0===b)throw x("uinput");b--;a=c.innerHTML;c=u(a)}while(a!==c.innerHTML);for(b=c.firstChild;b;){switch(b.nodeType){case 1:e.start(b.nodeName.toLowerCase(),
K(b.attributes));break;case 3:e.chars(b.textContent)}var f;if(!(f=b.firstChild)&&(1===b.nodeType&&e.end(b.nodeName.toLowerCase()),f=t("nextSibling",b),!f))for(;null==f;){b=t("parentNode",b);if(b===c)break;f=t("nextSibling",b);1===b.nodeType&&e.end(b.nodeName.toLowerCase())}b=f}for(;b=c.firstChild;)c.removeChild(b)};w=function(a,e){var c=!1,b=C(a,a.push);return{start:function(a,h){a=p(a);!c&&A[a]&&(c=a);c||!0!==n[a]||(b("<"),b(a),D(h,function(c,h){var d=p(h),g="img"===a&&"src"===d||"background"===
d;!0!==v[d]||!0===m[d]&&!e(c,g)||(b(" "),b(h),b('="'),b(H(c)),b('"'))}),b(">"))},end:function(a){a=p(a);c||!0!==n[a]||!0===h[a]||(b("</"),b(a),b(">"));a==c&&(c=!1)},chars:function(a){c||b(H(a))}}};F=s.Node.prototype.contains||function(a){return!!(this.compareDocumentPosition(a)&16)};var L=/[\uD800-\uDBFF][\uDC00-\uDFFF]/g,M=/([^#-~ |!])/g,h=g("area,br,col,hr,img,wbr"),q=g("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),l=g("rp,rt"),r=k({},l,q),q=k({},q,g("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,section,table,ul")),
l=k({},l,g("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var")),z=g("circle,defs,desc,ellipse,font-face,font-face-name,font-face-src,g,glyph,hkern,image,linearGradient,line,marker,metadata,missing-glyph,mpath,path,polygon,polyline,radialGradient,rect,stop,svg,switch,text,title,tspan"),A=g("script,style"),n=k({},h,q,l,r),m=g("background,cite,href,longdesc,src,xlink:href"),r=g("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,size,span,start,summary,tabindex,target,title,type,valign,value,vspace,width"),
l=g("accent-height,accumulate,additive,alphabetic,arabic-form,ascent,baseProfile,bbox,begin,by,calcMode,cap-height,class,color,color-rendering,content,cx,cy,d,dx,dy,descent,display,dur,end,fill,fill-rule,font-family,font-size,font-stretch,font-style,font-variant,font-weight,from,fx,fy,g1,g2,glyph-name,gradientUnits,hanging,height,horiz-adv-x,horiz-origin-x,ideographic,k,keyPoints,keySplines,keyTimes,lang,marker-end,marker-mid,marker-start,markerHeight,markerUnits,markerWidth,mathematical,max,min,offset,opacity,orient,origin,overline-position,overline-thickness,panose-1,path,pathLength,points,preserveAspectRatio,r,refX,refY,repeatCount,repeatDur,requiredExtensions,requiredFeatures,restart,rotate,rx,ry,slope,stemh,stemv,stop-color,stop-opacity,strikethrough-position,strikethrough-thickness,stroke,stroke-dasharray,stroke-dashoffset,stroke-linecap,stroke-linejoin,stroke-miterlimit,stroke-opacity,stroke-width,systemLanguage,target,text-anchor,to,transform,type,u1,u2,underline-position,underline-thickness,unicode,unicode-range,units-per-em,values,version,viewBox,visibility,width,widths,x,x-height,x1,x2,xlink:actuate,xlink:arcrole,xlink:role,xlink:show,xlink:title,xlink:type,xml:base,xml:lang,xml:space,xmlns,xmlns:xlink,y,y1,y2,zoomAndPan",
!0),v=k({},m,l,r),u=function(a,e){function c(b){b="<remove></remove>"+b;try{var c=(new a.DOMParser).parseFromString(b,"text/html").body;c.firstChild.remove();return c}catch(e){}}function b(a){d.innerHTML=a;e.documentMode&&I(d);return d}var h;if(e&&e.implementation)h=e.implementation.createHTMLDocument("inert");else throw x("noinert");var d=(h.documentElement||h.getDocumentElement()).querySelector("body");d.innerHTML='<svg><g onload="this.parentNode.remove()"></g></svg>';return d.querySelector("svg")?
(d.innerHTML='<svg><p><style><img src="</style><img src=x onerror=alert(1)//">',d.querySelector("svg img")?c:b):function(b){b="<remove></remove>"+b;try{b=encodeURI(b)}catch(c){return}var e=new a.XMLHttpRequest;e.responseType="document";e.open("GET","data:text/html;charset=utf-8,"+b,!1);e.send(null);b=e.response.body;b.firstChild.remove();return b}}(s,s.document)}).info({angularVersion:"1.6.6"});d.module("ngSanitize").filter("linky",["$sanitize",function(g){var k=/((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"\u201d\u2019]/i,
p=/^mailto:/i,s=d.$$minErr("linky"),t=d.isDefined,y=d.isFunction,w=d.isObject,x=d.isString;return function(d,q,l){function r(a){a&&m.push(J(a))}function z(a,d){var c,b=A(a);m.push("<a ");for(c in b)m.push(c+'="'+b[c]+'" ');!t(q)||"target"in b||m.push('target="',q,'" ');m.push('href="',a.replace(/"/g,"&quot;"),'">');r(d);m.push("</a>")}if(null==d||""===d)return d;if(!x(d))throw s("notstring",d);for(var A=y(l)?l:w(l)?function(){return l}:function(){return{}},n=d,m=[],v,u;d=n.match(k);)v=d[0],d[2]||
d[4]||(v=(d[3]?"http://":"mailto:")+v),u=d.index,r(n.substr(0,u)),z(v,d[0].replace(p,"")),n=n.substring(u+d[0].length);r(n);return g(m.join(""))}}])})(window,window.angular);
//# sourceMappingURL=angular-sanitize.min.js.map
//}