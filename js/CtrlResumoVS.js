/*
 * CtrlResumoVendas
 */
function CtrlResumoVS($scope, $globals) {

    win.title = "Resumo de vendas"; //27/02/2014
    $scope.versao = versao;
    $scope.rotas = rotas;
    $scope.limite_registros = 0;

    //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
    $scope.filtros = {
		naoAbordado: false,
        isHFiltroED: false
    };

    //08/02/2014    
    travaBotaoFiltro(0, $scope, "#pag-resumo-vendas", "Resumo de vendas");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.vendas = [];
    $scope.csv = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "vendas",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.total_valores;
    $scope.total_qtds;
    $scope.ordenacao = ['data_hora', 'grupo'];
    $scope.decrescente = false;

    $scope.extrator = false;
    $scope.status_extrator;

    $scope.log = [];


    /*// Filtro: região
    $scope.regioes = regioes;*/

    // Filtro: ddd
    $scope.ddds = cache.ddds;

    // Filtro: status_vendas
    $scope.estatos = status_vendas;

    // Filtro: erros_vendas
    $scope.erros = cache.erros;

    //filtros_usados
    $scope.filtros_usados = "";


    $scope.filtro_aplicacoes = [];

    $scope.valor = "produto";
	$scope.valor2 = "padrao";
    $scope.viewFormat = null;

    // Filtro: produto
    $scope.filtro_produtos = [];


    $scope.aba = 2;

    function geraColunas() {
        var array = [];
		
		
		if ($scope.valor2 === "hora") {
				array.push({
					field: "data_hora_BR",
					displayName: "Data e hora",
					width: 145,
					pinned: false
				});
		} else if ($scope.valor2 === "dia") {
				array.push({
					field: "data_hora_BR",
					displayName: "Data",
					width: 85,
					pinned: false
				});
		}
		
		
		if($scope.valor === "resultado"){

			array.push({
					field: "codaplicacao",
					displayName: "Aplicação",
					width: 122,
					pinned: false
				});
			array.push({
				field: "qtd_venda",
				displayName: "Qtd",
				width: 59,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			});
			array.push({
					field: "grupo",
					displayName: "Grupo",
					width: 182,
					pinned: false
				}, {
					field: "descricao",
					displayName: "Descrição",
					width: 247,
					pinned: false
				});
			if (dominioUsuario.toUpperCase() !== "CONTAX-BR") {
				array.push({
					field: "valor",
					displayName: "Valor",
					width: 111,
					pinned: false,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
				});
			}
			array.push({
					field: "resultado",
					displayName: "Resultado",
					width: 150,
					pinned: false
				});
				
			array.push({
					field: "erro",
					displayName: "Erro",
					width: 60,
					pinned: false
				});
				
			array.push({
					field: "descErro",
					displayName: "Descrição erro",
					width: 255,
					pinned: false
				});
				
			if ($('.filtro-empresaRecarga').val() !== null) {
				array.push({
					field: "parceiro",
					displayName: "Parceiro",
					width: 250,
					pinned: false
				});
			}
			
			
		}else if($scope.valor === "operador"){

			array.push({
					field: "operador",
					displayName: "Operador",
					width: 125,
					pinned: false
				});
			array.push({
					field: "codaplicacao",
					displayName: "Aplicação",
					width: 122,
					pinned: false
				});
			array.push({
				field: "qtd_venda",
				displayName: "Qtd",
				width: 59,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			});
			array.push({
					field: "grupo",
					displayName: "Grupo",
					width: 182,
					pinned: false
				}, {
					field: "descricao",
					displayName: "Descrição",
					width: 247,
					pinned: false
				});
			if (dominioUsuario.toUpperCase() !== "CONTAX-BR") {
				array.push({
					field: "valor",
					displayName: "Valor",
					width: 111,
					pinned: false,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
				});
			}
			array.push({
					field: "resultado",
					displayName: "Resultado",
					width: 150,
					pinned: false
				});
				
			array.push({
					field: "erro",
					displayName: "Erro",
					width: 60,
					pinned: false
				});
				
			array.push({
					field: "descErro",
					displayName: "Descrição erro",
					width: 255,
					pinned: false
				});
				
			if ($('.filtro-empresaRecarga').val() !== null) {
				array.push({
					field: "parceiro",
					displayName: "Parceiro",
					width: 250,
					pinned: false
				});
			}
			
			
		}else if ($scope.valor === "produto") {
				array.push({
					field: "grupo",
					displayName: "Grupo",
					width: 200,
					pinned: false
				}, {
					field: "descricao",
					displayName: "Descrição",
					width: 300,
					pinned: false
				});
			
			if (dominioUsuario.toUpperCase() !== "CONTAX-BR") {
				array.push({
					field: "valor",
					displayName: "Valor",
					width: 150,
					pinned: false,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
				});
			}
			array.push({
				field: "qtd_venda",
				displayName: "Qtd",
				width: 150,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			});
			if ($('.filtro-empresaRecarga').val() !== null) {
				array.push({
					field: "parceiro",
					displayName: "Parceiro",
					width: 250,
					pinned: false
				});
			}
		}
        return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = horaAnterior();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-resumo-vendas");


        //19/03/2014
        componenteDataHora($scope, $view);

        carregaAplicacoes($view, false, true, $scope); // $view, false, true, $scope
        carregaResultados($view);
        carregaErros($view,true);
        carregaEmpresasRecarga($view,true);
        carregaSegmentosPorAplicacao($scope, $view, true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });

        carregaGruposProdutoPorAplicacao($scope, $view, true);
        // Popula lista de  grupo de produtos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaGruposProdutoPorAplicacao($scope, $view)
            carregaProdutosPorGrupo($scope, $view)
        });

        carregaDDDs($view);

        carregaProdutosPorGrupo($scope, $view, true);
        // Popula lista de  produtos a partir dos grupo de produtos selecionados
        $view.on("change", "select.filtro-grupo-produto", function () {
            carregaProdutosPorGrupo($scope, $view)
        });

        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        $view.on("change", "select.filtro-grupo-produto", function () {
            Estalo.filtros.filtro_grupos_produtos = $(this).val() || [];
        });

        $view.on("change", "select.filtro-produto", function () {
            Estalo.filtros.filtro_produtos = $(this).val() || [];
        });

        $view.on("change", "select.filtro-erro", function () {
            Estalo.filtros.filtro_erros = $(this).val() || [];
        });

        $view.on("change", "select.filtro-status", function () {
            Estalo.filtros.filtro_resultados = $(this).val() || [];
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

		$view.on("change", ".navbar-form.radio-responsivo rpd input", function () {
			$scope.valor = $(this).val();			
		});
		
		$view.on("change", ".navbar-form.radio-responsivo rdh input", function () {
			$scope.valor2 = $(this).val();			
		});		

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function () {
            marcaTodosIndependente($('.filtro-ddd'), 'ddds')
        });

        // Marca todos os produtos
        $view.on("click", "#alinkProd", function () {
            marcaTodosIndependente($('.filtro-produto'), 'produtos')
        });

        // Marca todos os erros
        $view.on("click", "#alinkErr", function () {
            marcaTodosIndependente($('.filtro-erro'), 'erros')
        });

        // Marca todos os resultados
        $view.on("click", "#alinkRes", function () {
            marcaTodosIndependente($('.filtro-status'), 'resultados')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });

        // Marca todos os parceiros de recarga
        $view.on("click", "#alinkRecarga", function () {
            marcaTodosIndependente($('.filtro-empresaRecarga'), 'resultados')
        });

        //Marcar todas grupos
        $view.on("click", "#alinkGru", function () {
            marcaTodosGrupos($('.filtro-grupo-produto'), $view, $scope)
        });

        $view.find("select.filtro-status").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} resultados',
            showSubtext: true
        });

        $view.find("select.filtro-erro").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} erros',
            showSubtext: true
        });

        $view.find("select.filtro-empresaRecarga").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} parceiros',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-grupo-produto").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} grupos'
        });

        $view.find("select.filtro-produto").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} produtos'
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-ddd').removeClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-status').mouseover(function () {
            $('div.btn-group.filtro-status').addClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '-52px'
            });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-status').mouseout(function () {
            $('div.btn-group.filtro-status').removeClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.btn-group.filtro-ddd>div>ul').css({
                    'max-height': '600px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-erro').mouseover(function () {
            $('div.btn-group.filtro-erro').addClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '-330px'
            });
            $('div.btn-group.filtro-erro>div>ul').css({
                'max-width': '450px',
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-erro').mouseout(function () {
            $('div.btn-group.filtro-erro').removeClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-erro>div>ul').css({
                'max-width': '450px',
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px'
            });
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseover(function () {
            $('div.btn-group.filtro-empresaRecarga').addClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '-30px'
            });
            $('div.btn-group.filtro-empresaRecarga>div>ul').css({
                'max-width': '450px',
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseout(function () {
            $('div.btn-group.filtro-empresaRecarga').removeClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-empresaRecarga>div>ul').css({
                'max-width': '450px',
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px'
            });
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-grupo-produto').addClass('open');
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseout(function () {
            $('div.btn-group.filtro-grupo-produto').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-produto').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-produto .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-produto').addClass('open');
                $('div.btn-group.filtro-produto>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-produto').mouseout(function () {
            $('div.btn-group.filtro-produto').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-vendas");
            //22/03/2014 Testa se data início é maior que a data fim
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null && ((dominioUsuario.toUpperCase().match(/CONTAX/g) !== null) || (dominioUsuario.toUpperCase().match(/TEL/g) !== null) || (dominioUsuario.toUpperCase().match(/ITB/g) !== null))) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaVendas.apply(this);
        });



        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            $('#alinkDDD').removeAttr("disabled").selectpicker('refresh');
            $('select.filtro-ddd').removeAttr("disabled").selectpicker('refresh');
            componenteDataHora($scope, $view, "venda");

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');

            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }


        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-vendas");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
        $('#alinkDDD').removeAttr("disabled").selectpicker('refresh');
        $('select.filtro-ddd').removeAttr("disabled").selectpicker('refresh');
    });






    // Lista vendas conforme filtros
    $scope.listaVendas = function () {

        $globals.numeroDeRegistros = 0;

        $scope.filtros_usados = "";

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_produtos = $view.find("select.filtro-produto").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_grupo_produtos = $view.find("select.filtro-grupo-produto").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_status = $view.find("select.filtro-status").val() || [];
        var filtro_erros = $view.find("select.filtro-erro").val() || [];
        var filtro_parceiros = $view.find("select.filtro-empresaRecarga").val() || [];
        if (filtro_status.indexOf("0") !== -1) {
            filtro_status.push("20");
        }

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR(dataConsoliReal(data_fim));
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
        if (filtro_produtos.length > 0) {
            $scope.filtros_usados += " Produtos: " + filtro_produtos;
        }
        if (filtro_grupo_produtos.length > 0) {
            $scope.filtros_usados += " Grupos: " + filtro_grupo_produtos;
        }
        if (filtro_ddds.length > 0) {
            $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }
        if (filtro_status.length > 0) {
            $scope.filtros_usados += " Status: " + filtro_status;
        }
        if (filtro_erros.length > 0) {
            $scope.filtros_usados += " Erros: " + filtro_erros;
        }
        if (filtro_parceiros.length > 0) {
            $scope.filtros_usados += " Parceiros: " + filtro_parceiros;
        }

        $scope.vendas = [];
        $scope.csv = [];
        var stmt = "";
        var executaQuery = "";
        var sufixo = "";
        if (filtro_segmentos.length > 0) {
            sufixo = "seg";
        }

        var tabela = filtro_parceiros.length > 0 ? "ResumoVendas" : "ResumoVendaProduto" + sufixo;
        var campoRecarga = filtro_parceiros.length > 0 ? ",CodPlataforma" : "";
        /*if ($scope.valor === "dia") {

            stmt = db.use + "SELECT convert(date,RVP.DatReferencia)," +
                " sum(ValorVendido) as valor," +
                " Sum(QtdVendas) as qtd_vendas" + campoRecarga +
                " from " + db.prefixo + tabela + " as RVP" +
                " left outer join " + db.prefixo + "ProdutosURA as PU" +
                " on RVP.CodProduto = PU.CodProduto" +
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            }

            stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            stmt += restringe_consulta("RVP.CodPlataforma", filtro_parceiros, true)
            stmt += " group by convert(date,RVP.DatReferencia)" + campoRecarga
            stmt += " order by convert(date,RVP.DatReferencia) ASC";

            executaQuery = executaQuery2;
            $scope.viewFormat = "Dia";

        } else if ($scope.valor === "hora") {

            stmt = db.use + "SELECT RVP.DatReferencia," +
                " sum(ValorVendido) as valor," +
                " Sum(QtdVendas) as qtd_vendas" + campoRecarga +
                " from " + db.prefixo + tabela + " as RVP" +
                " left outer join " + db.prefixo + "ProdutosURA as PU" +
                " on RVP.CodProduto = PU.CodProduto" +
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            }

            stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            stmt += restringe_consulta("RVP.CodPlataforma", filtro_parceiros, true)
            stmt += " group by RVP.DatReferencia" + campoRecarga
            stmt += " order by RVP.DatReferencia ASC";

            executaQuery = executaQuery3;
            $scope.viewFormat = "Hora";
			
			
			


        } else */
		
				
		var dat = "";
		
		if($scope.valor2 === "dia"){
			dat  = "convert(date,RVP.DatReferencia),";
		}else if($scope.valor2 === "hora"){
			dat  = "RVP.DatReferencia,";			
		}
		
		if ($scope.valor === "produto") {

            stmt = db.use + "SELECT " + dat +
			    " GrupoProduto, Descricao," +
                " sum(ValorVendido) as valor," +
                " Sum(QtdVendas) as qtd_vendas" + campoRecarga +
                " from " + db.prefixo + tabela + " as RVP" +
                " left outer join " + db.prefixo + "ProdutosURA as PU" +
                " on RVP.CodProduto = PU.CodProduto" +
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            }

            stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            stmt += restringe_consulta("RVP.CodPlataforma", filtro_parceiros, true)
            stmt += " group by "+dat+" GrupoProduto, Descricao" + campoRecarga
            stmt += " order by GrupoProduto ASC";

            executaQuery = executaQuery1;
            $scope.viewFormat = "Grupo";
        } else if($scope.valor === "resultado") {
			stmt = db.use + "SELECT "+dat+"codaplicacao, Sum(QtdVendas) as qtd_vendas,GrupoProduto, Descricao," +
                " sum(ValorVendido) as valor,CAST(resultado as varchar(16)) as resultado, CAST(CodErro as varchar(16)) as CodErro " + campoRecarga +           
                " from " + db.prefixo + tabela + " as RVP" +
                " left outer join " + db.prefixo + "ProdutosURA as PU" +
                " on RVP.CodProduto = PU.CodProduto" +
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            }

            stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            stmt += restringe_consulta("RVP.CodPlataforma", filtro_parceiros, true)
            stmt += " GROUP BY "+dat+"codaplicacao,GrupoProduto, Descricao,resultado,CodErro" + campoRecarga;
			
			if ($scope.filtros.naoAbordado) {
				stmt += " union all "
				stmt +=  "SELECT "+dat+"codaplicacao, Sum(QtdChamadas) as qtd_vendas,'' as GrupoProduto,'' as Descricao," +
                " 0 as valor,status as resultado, '' as CodErro " + (campoRecarga !== "" ? ",'' as CodPlataforma" : "") +           
                " from " + db.prefixo + "ResumoNaoVendas as RVP" +         
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
				stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
				}  
				stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true) 
				stmt += " GROUP BY "+dat+"codaplicacao,status"
				 
			}
			
            stmt += " order by "+dat+"resultado ASC";

            executaQuery = executaQuery4;
            $scope.viewFormat = "Resultado";
			
		}else{
			stmt = db.use + "SELECT "+dat+"codaplicacao, Sum(QtdVendas) as qtd_vendas,GrupoProduto, Descricao," +
                " sum(ValorVendido) as valor,CAST(resultado as varchar(16)) as resultado, CAST(CodErro as varchar(16)) as CodErro,CodOperador " + campoRecarga +           
                " from " + db.prefixo + "ResumoVendasOperador as RVP" +
                " left outer join " + db.prefixo + "ProdutosURA as PU" +
                " on RVP.CodProduto = PU.CodProduto" +
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            }

            stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            stmt += restringe_consulta("RVP.CodPlataforma", filtro_parceiros, true)
            stmt += " GROUP BY "+dat+"codaplicacao,GrupoProduto, Descricao,resultado,CodErro,CodOperador" + campoRecarga;
			
			if ($scope.filtros.naoAbordado) {
				stmt += " union all "
				stmt +=  "SELECT "+dat+"codaplicacao, Sum(QtdChamadas) as qtd_vendas,'' as GrupoProduto,'' as Descricao," +
                " 0 as valor,status as resultado, '' as CodErro, CodOperador " + (campoRecarga !== "" ? ",'' as CodPlataforma" : "") +           
                " from " + db.prefixo + "ResumoNaoVendasOperador as RVP" +         
                " WHERE 1 = 1" +
                " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
				stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

				if (filtro_segmentos.length > 0) {
					stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
				}  
				stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true) 
				stmt += " GROUP BY "+dat+"codaplicacao,status,CodOperador"
				 
			}
			
            stmt += " order by "+dat+"resultado,CodOperador ASC";

            executaQuery = executaQuery5;
            $scope.viewFormat = "Operador";
			
		}

        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery1(columns) {
			var data_hora = columns[""] !== undefined ? columns[""].value : columns["DatReferencia"] !== undefined ? columns["DatReferencia"].value : undefined;
			var data_hora_BR = undefined;
			if($scope.valor2 === "hora"){
				data_hora_BR = columns[""] !== undefined ? formataDataHoraBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataHoraBR(columns["DatReferencia"].value) : undefined;
			}else{
                data_hora_BR = columns[""] !== undefined ? formataDataBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataBR(columns["DatReferencia"].value) : undefined;
			}			
			//if(columns[""] !== undefined) columns[""].metadata.colName = "DatReferencia";
			
                var grupo = columns["GrupoProduto"].value,
                descricao = columns["Descricao"].value,
                valor = +columns["valor"].value.toFixed(2), //valor = toFixed(columns[2].value, 2), //Não ordena
                qtd_venda = +columns["qtd_vendas"].value;

            $scope.total_valores = $scope.total_valores + valor;
            $scope.total_qtds = $scope.total_qtds + qtd_venda;

            if (filtro_parceiros.length > 0) {
                $scope.vendas.push({
					data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
                    grupo: grupo,
                    descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    qtd_venda: qtd_venda,
                    parceiro: columns["CodPlataforma"].value
                });

                $scope.csv.push([
					data_hora_BR,
                    grupo,
                    descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    qtd_venda,
                    columns["CodPlataforma"].value
                ]);
            } else {
                $scope.vendas.push({
					data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
                    grupo: grupo,
                    descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    qtd_venda: qtd_venda
                });

                $scope.csv.push([
					data_hora_BR,
                    grupo,
                    descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    qtd_venda
                ]);
            }
			
			if($scope.valor2 === "padrao") $scope.csv[$scope.csv.length-1].shift();

        }

        function executaQuery4(columns) {
			var data_hora = columns[""] !== undefined ? columns[""].value : columns["DatReferencia"] !== undefined ? columns["DatReferencia"].value : undefined;
			var data_hora_BR = undefined;
			if($scope.valor2 === "hora"){
				data_hora_BR = columns[""] !== undefined ? formataDataHoraBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataHoraBR(columns["DatReferencia"].value) : undefined;
			}else{
                data_hora_BR = columns[""] !== undefined ? formataDataBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataBR(columns["DatReferencia"].value) : undefined;
			}			
			//if(columns[""] !== undefined) columns[""].metadata.colName = "DatReferencia";
			
				var codaplicacao = columns["codaplicacao"].value,
				qtd_venda = +columns["qtd_vendas"].value
				grupo = columns["GrupoProduto"].value,
				descricao = columns["Descricao"].value,
                valor = +columns["valor"].value.toFixed(2), //valor = toFixed(columns[2].value, 2), //Não ordena
                resultado = obtemNomeStatusVenda(columns["resultado"].value),  //status_vendas.indice[columns[6].value] !== undefined ? status_vendas.indice[columns[6].value].nome : '';
				erro = columns["CodErro"].value,
				descErro = obtemNomeErroVenda(columns["CodErro"].value,grupo);

            $scope.total_valores = $scope.total_valores + valor;
            $scope.total_qtds = $scope.total_qtds + qtd_venda;

            if (filtro_parceiros.length > 0) {
                $scope.vendas.push({
                    data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
					codaplicacao: codaplicacao,
					qtd_venda: qtd_venda,
					grupo: grupo,
					descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado: resultado,
					erro: erro,
					descErro:descErro,
                    parceiro: columns["CodPlataforma"].value
                });

                $scope.csv.push([                    
                    data_hora_BR,
					codaplicacao,
					qtd_venda,
					grupo,
					descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado,
					erro,
					descErro,
                    columns["CodPlataforma"].value
                ]);
            } else {
                $scope.vendas.push({
                    data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
					codaplicacao: codaplicacao,
					qtd_venda: qtd_venda,
					grupo: grupo,
					descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado: resultado,
					erro: erro,
					descErro: descErro
					
                });

                $scope.csv.push([                    
                    data_hora_BR,
					codaplicacao,
					qtd_venda,
					grupo,
					descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado,
					erro,
					descErro					
                ]);
            }
			
			if($scope.valor2 === "padrao") $scope.csv[$scope.csv.length-1].shift();

        }
		
		function executaQuery5(columns) {

            var data_hora = columns[""] !== undefined ? columns[""].value : columns["DatReferencia"] !== undefined ? columns["DatReferencia"].value : undefined;
			var data_hora_BR = undefined;
			if($scope.valor2 === "hora"){
				data_hora_BR = columns[""] !== undefined ? formataDataHoraBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataHoraBR(columns["DatReferencia"].value) : undefined;
			}else{
                data_hora_BR = columns[""] !== undefined ? formataDataBRString(columns[""].value) : columns["DatReferencia"] !== undefined ? formataDataBR(columns["DatReferencia"].value) : undefined;
			}
			//if(columns[""] !== undefined) columns[""].metadata.colName = "DatReferencia";
			
				var codaplicacao = columns["codaplicacao"].value,
				qtd_venda = +columns["qtd_vendas"].value
				grupo = columns["GrupoProduto"].value,
				descricao = columns["Descricao"].value,
                valor = +columns["valor"].value.toFixed(2), //valor = toFixed(columns[2].value, 2), //Não ordena
                resultado = obtemNomeStatusVenda(columns["resultado"].value),  //status_vendas.indice[columns[6].value] !== undefined ? status_vendas.indice[columns[6].value].nome : '';
				erro = columns["CodErro"].value,
				descErro = obtemNomeErroVenda(columns["CodErro"].value,grupo),
				operador = columns["CodOperador"].value;

            $scope.total_valores = $scope.total_valores + valor;
            $scope.total_qtds = $scope.total_qtds + qtd_venda;

            if (filtro_parceiros.length > 0) {
                $scope.vendas.push({
                    data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
					codaplicacao: codaplicacao,
					qtd_venda: qtd_venda,
					grupo: grupo,
					descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado: resultado,
					erro: erro,
					descErro:descErro,
					operador: operador,
                    parceiro: columns["CodPlataforma"].value
                });

                $scope.csv.push([                    
                    data_hora_BR,
					codaplicacao,
					qtd_venda,
					grupo,
					descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado,
					erro,
					descErro,
					operador,
                    columns["CodPlataforma"].value
                ]);
            } else {
                $scope.vendas.push({
                    data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
					codaplicacao: codaplicacao,
					qtd_venda: qtd_venda,
					grupo: grupo,
					descricao: descricao,
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado: resultado,
					erro: erro,
					descErro: descErro,
					operador: operador
					
                });

                $scope.csv.push([                    
                    data_hora_BR,
					codaplicacao,
					qtd_venda,
					grupo,
					descricao,
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA",
                    resultado,
					erro,
					descErro,
					operador
                ]);
            }
			
			if($scope.valor2 === "padrao") $scope.csv[$scope.csv.length-1].shift();

        }



        $scope.total_valores = 0;
        $scope.total_qtds = 0;

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');

			if ($scope.viewFormat === 'Resultado') {
				
				$scope.vendas.push({
                    data_hora: "",
                    data_hora_BR: "",
					codaplicacao: "",
					qtd_venda: $scope.total_qtds,
					grupo: "",
					descricao: "TOTAL",
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    resultado: "",
					erro:"",
					descErro:""
                });

                $scope.csv.push([                    
                    "",
					"",
					$scope.total_qtds,
					"",
					"TOTAL",
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    "",
					"",
					""
                ]);
				
            } else if ($scope.viewFormat === 'Operador') {
				
				$scope.vendas.push({
                    data_hora: "",
                    data_hora_BR: "",
					codaplicacao: "",
					operador: "",
					qtd_venda: $scope.total_qtds,
					grupo: "",
					descricao: "TOTAL",
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    resultado: "",
					erro:"",
					descErro:""
                });

                $scope.csv.push([
					"",
					"",
					$scope.total_qtds,
					"",
					"TOTAL",
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    "",
					"",
					"",
					""
                ]);
				
            } else {
                $('#pag-resumo-vendas table').css('width', '1185px');
                $scope.vendas.push({
					data_hora: "",
                    data_hora_BR: "",
                    grupo: "",
                    descricao: "TOTAL",
                    valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    qtd_venda: $scope.total_qtds
                });

                $scope.csv.push([
					"",
                    "",
                    "TOTAL",
                    dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA",
                    $scope.total_qtds
                ]);
            }
			
			if($scope.valor2 === "padrao") $scope.csv[$scope.csv.length-1].shift();

            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
        });


        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

    };
	
		//Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		
		var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var colunas = [];
        for (i = 0; i < $scope.colunas.length; i++) {
			colunas.push($scope.colunas[i].displayName);
        }
        var dadosExcel = [colunas].concat($scope.csv);
        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);
        var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'resumoVendas_' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);
		
		setTimeout(function() {
            $btn_exportar.button('reset');
        }, 500);

	}



    // Exportar planilha XLSX
	/*
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var template = "";
        $scope.viewFormat === "Dia" ? template = 'tResumoVSPorDia' :
            $scope.viewFormat === "Hora" ? template = 'tResumoVSPorDia' :
			$scope.viewFormat === "Resultado" ? template = 'tResumoVSResultado' :
			$scope.viewFormat === "Operador" ? template = 'tResumoVSOperador' :
            template = 'tResumoVS';

        //15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
            " WHERE NomeRelatorio='" + template + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = 'tResumoVS.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = 'resumoVendas_' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.vendas,
                    total: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : ["NA"],
                    qtd: $scope.total_qtds
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
            userLog(stmt, 'Exportar XLSX', 2, err)
            //?
        });

    };*/
}
CtrlResumoVS.$inject = ['$scope', '$globals'];