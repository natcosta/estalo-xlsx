function multiAreaChart(data, dimensions, svgElem, totalWidth, totalHeight, options) {
  options || (options = {});

  var margin = {top: 30, right: 60, bottom: 150, left: 60},
      xTickFormat = options.xTickFormat || function (d) { return d3.time.format("%H")(d) + "h"; },
      yTickFormat = options.yTickFormat || function (d) { return d > 0 && d % 1000 === 0 ? (d / 1000) + " mil" : (d !== 0 ? d + "" : ""); },
      showLegend = options.showLegend,
      maxDataPoint = options.maxDataPoint,
      chartType = options.chartType || 'area',
      legendRectSize = 18,
      legendSpacing  = 4;

  if (showLegend === undefined) {
    showLegend = false;
  }

  var startDate = data[0][0].date;
  var endDate = data[0][data[0].length - 1].date;

  // figure out the maximum data point
  if (maxDataPoint === undefined) {
    maxDataPoint = 0;
    data.forEach(function (d0) {
      d0.forEach(function (d) {
        dimensions.forEach(function (dim) {
          var v = d[dim.field];
          if (v > maxDataPoint) {
            maxDataPoint = v;
          }
        });
      });
    });
  }

  function draw(totalWidth, totalHeight) {
    svgElem.selectAll("g").remove();

    if (totalWidth === 0) {
      totalWidth = svgElem[0][0].parentNode.getBoundingClientRect().width;
    }

    var width = totalWidth - margin.left - margin.right,
        height = totalHeight - margin.top - margin.bottom;

    var svg = svgElem
        .attr("width", totalWidth)
        .attr("height", totalHeight + legendRectSize + 2 * legendSpacing + 30);

    // Obter o título de cada gráfico
    var titles = data.map(function (d) { return d[0].title; });

    var chartHeight = height * (1 / data.length);

    // Color scale
    //var color = d3.scale.category10();

    var charts = [];
    data.forEach(function (d, i) {
      charts.push(new Chart({
        data: d,
        id: i,
        name: titles[i],
        width: width,
        height: height * (1 / data.length),
        maxDataPoint: maxDataPoint,
        svg: svg,
        margin: margin,
        showBottomAxis: (i === titles.length - 1),
        showEmptyBottomAxis: (i !== titles.length - 1)
      }));
    });

    if (showLegend) {
      var legend = svg.selectAll('.legend')
          .data(dimensions)
        .enter()
          .append("g")
          .attr("transform", function (d, i) {
            var x = totalWidth - 100 - ((dimensions.length - 1 - i) * 70);
            var y = 650; // totalHeight + legendSpacing;
            return "translate(" + x + "," + y + ")";
          });

      legend.append("rect")
          .attr("width", legendRectSize)
          .attr("height", legendRectSize)
          .style("fill", function (dim) { return dim.color; })
          .style("stroke", function (dim) { return dim.color; });

      legend.append("text")
          .attr("class", "legend")
          .attr("x", legendRectSize + legendSpacing)
          .attr("y", legendRectSize - legendSpacing)
          .text(function (dim) { return dim.field.substring(10); });
    }
  }

  function Chart(options) {
    this.data = options.data;
    this.width = options.width;
    this.height = options.height;
    this.maxDataPoint = options.maxDataPoint;
    this.svg = options.svg;
    this.id = options.id;
    this.name = options.name;
    this.margin = options.margin;
    this.showBottomAxis = options.showBottomAxis;
    this.showEmptyBottomAxis = options.showEmptyBottomAxis;

    var localName = this.name;

    var that = this;

    /* XScale is time based */
    this.xScale = d3.time.scale()
        .range([0, this.width])
        .domain(d3.extent(this.data.map(function (d) { return d.date; })));

    /* YScale is linear based on the maxData Point we found earlier */
    this.yScale = d3.scale.linear()
        .range([this.height, 0])
        .domain([0, this.maxDataPoint]);

    var xS = this.xScale;
    var yS = this.yScale;
/*
    // This isn't required - it simply creates a mask. If this wasn't here,
    // when we zoom/panned, we'd see the chart go off to the left under the y-axis
    this.svg.append("defs").append("clipPath")
        .attr("id", "clip-" + this.id)
        .append("rect")
          .attr("width", this.width)
          .attr("height", this.height);
*/
    // assign it a class so we can assign a fill color and position it on the page
    this.chartContainer = this.svg.append("g")
        .attr("class", this.name.toLowerCase())
        .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + (this.height * this.id) + (10 * this.id)) + ")");

    var seriesData = dimensions.map(function (dim) {
      var a = that.data.map(function (d) {
        return { date: d.date, value: d[dim.field], title: dim.field };
      });
      a.title = dim.field;
      a.color = dim.color;
      return a;
    });

    var series = this.chartContainer.selectAll("g")
        .data(seriesData)
      .enter().append("g");

    var bla;
    if (chartType === 'area') {
      bla = d3.svg.area()
          //.interpolate("basis")
          .x(function (d) { return xS(d.date); })
          .y0(this.height)
          .y1(function (d) { return yS(d.value); });
    } else {
      bla = d3.svg.line()
          .x(function (d) { return xS(d.date); })
          .y(function (d) { return yS(d.value); });
    }

    series.append("path")
        .attr("class", function (d) { return "chart " + d.title + (chartType === 'area' ? "" : " line"); })
        //.style("fill", function (d, i) { return color(i); })
        .style((chartType === 'area' ? "fill" : "stroke"), function (d) { return d.color || null; })
        //.attr("clip-path", "url(#clip-" + this.id + ")")
        .attr("d", bla);

    this.xAxisTop = d3.svg.axis()
        .scale(this.xScale)
        .orient("top")
        //.tickFormat(xTickFormat);
        .tickFormat(function (d, i) { return that.width < 410 && i % 2 == 0 ? "" : xTickFormat(d); });
    this.xAxisBottom = d3.svg.axis()
        .scale(this.xScale)
        .orient("bottom")
        //.tickFormat(xTickFormat);
        .tickFormat(function (d, i) { return that.width < 410 && i % 2 == 0 ? "" : xTickFormat(d); });

    // Criar um eixo superior apenas para o primeiro gráfico
    if (this.id == 0) {
      this.chartContainer.append("g")
          .attr("class", "x axis top")
          .attr("transform", "translate(0,0)")
          .call(this.xAxisTop);
    }

    // Criar um eixo inferior apenas para o último gráfico
    if (this.showBottomAxis) {
      this.chartContainer.append("g")
          .attr("class", "x axis bottom")
          .attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxisBottom);
    } else if (this.showEmptyBottomAxis) {
      this.xAxisBottom.ticks(0);
      this.chartContainer.append("g")
          .attr("class", "x axis bottom")
          .attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxisBottom);
    }

    this.yAxis = d3.svg.axis().scale(this.yScale).orient("left").ticks(4).tickFormat(yTickFormat);

    this.chartContainer.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(-15,0)")
        .call(this.yAxis);

    this.chartContainer.append("text")
        .attr("class","title")
        .attr("transform", "translate(15,40)")
        .text(this.name);
  }

  draw(totalWidth, totalHeight);

  return draw;
}