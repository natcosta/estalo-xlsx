﻿﻿/*
 * CtrlChamadas
 */
var changeFalhas = false;

function CtrlChamadas($scope, $globals, $compile, $timeout, $window) {
	
	try{		
		//cache.carregaEstaloExe();
	}catch(ex){
		console.log(ex);
	}

    win.title = "Detalhamento de chamadas"; //Alex 27/02/2014
    $scope.versao = versao;
    $scope.rotas = rotas;
    
    var tempData = "";
	var csvExportado = "";
	var blocoCSV = 1000;
    var novaMacro;
	

    $scope.limite_registros = 25000;
    var tempoLimite = 1;


    $scope.visaoLimpa = testaPermissoesAclBotoes('pag-chamadas','info_finais');	
	$scope.visaoPrompt = testaPermissoesAclBotoes('pag-chamadas','VisaoPrompt');
	$scope.notificationAlways = true;
	
    $scope.estadoEnx = false;
    $scope.estadoEnxAll = $scope.visaoLimpa;
    $scope.itemEnx = false;
    $scope.itemEnxAll = $scope.visaoLimpa;


    travaBotaoFiltro(0, $scope, "#pag-chamadas", "Consulta realizada de 5 em 5 minutos. Limite para exportação xlsx: "+$scope.limite_registros+" registros.");
    $('body').css({
        backgroundImage: '',
        backgroundColor: "White",
        transition: "all 1s linear"
    });
	
	$scope.nonodigito = false;
    $scope.chamadas = [];
    $scope.csv = [];
    $scope.ordenacao = ['data_hora', 'chamador'];
    $scope.decrescente = false;
    $scope.ocultar = {
        tratado: false,
        log_formatado: false,
        log_original: true
    };

    $scope.colunas = [];
    $scope.gridDados = {
        data: "chamadas",
        rowTemplate: '<div ng-dblclick="formataXML(row.entity)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.chamada_atual = undefined;
    $scope.callLog = "";
    $scope.logxml = [];
    $scope.logxml_iit = [];
    $scope.log = [];

    $scope.aplicacoes = [];
    $scope.segmentos = [];
    $scope.falhas = [];
    

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        falhas: [],
        chamador: "",
        isUltimoED: false,         
        nps: false
    };

    $scope.logValidado = {};
    $scope.extratorArquivoPorData = false;
    $scope.extratorInvalidos = false;
    $scope.msgAlerta = false;
    $scope.callFlow = false;  


    function geraColunas() {

        var array = [];
        array.push({
            field: "data_hora_BR",
            displayName: "Data",
            width: 150,
            pinned: true
        }, {
            field: "chamador",
            displayName: "Chamador",
            width: 100,
            pinned: false
        });
        $scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));

        if (!$scope.ocultar.tratado) {
            array.push({
                field: "tratado",
                displayName: "Tratado",
                width: 100,
                pinned: false
            });
        }

        array.push({
            field: "aplicacao",
            displayName: "Aplicação",
            width: 130,
            pinned: false
        }, {
            field: "segmento",
            displayName: "Segmento",
            width: 100,
            pinned: false
        }, {
            field: "encerramento",
            displayName: "Encerramento",
            width: 150,
            pinned: false
        }, {
            field: "estado_final",
            displayName: "Último estado",
            width: 200,
            pinned: false
        }, {
            field: "duracao",
            displayName: "Duração",
            width: 100,
            pinned: false
        },{
            field: "eps",
            displayName: "EPS",
            width: 100,
            pinned: false
        },
        {
            field: "regraDR3",
            displayName: "Regra DR3",
            width: 123,
            pinned: false
        });

        if ($scope.filtros.nps === true) {
            array.push({
                field: "npss",
                displayName: "Satisf.",
                width: 80,
                pinned: false
            }, {
                field: "npsr",
                displayName: "Recom.",
                width: 80,
                pinned: false
            });
        }

        array.push({
            field: "ucid",
            displayName: "Log",
            width: 65,
            cellClass: "grid-align",
            pinned: false,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>'
        });

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;
    var numanis = [];

    var invalidsNumanis = [];

    $scope.$on('$viewContentLoaded', function() {	
	
		//Tocar prompts
		if(!fs.existsSync(tempDir3() + 'ffmpeg.dll') && $scope.visaoPrompt > 0) cache.carregaPromptsTocar();				
        //Utilizado para verificar se há conexão		
        if (true) testaConexao2($(".bar"));

        $('.log-original').css('overflow', 'hidden');
        $('#modalAlerta').on('hidden', function() {
            $("#modalAlerta").css({
                'margin-top': '0px',
                'width': '0px',
                'z-index': '0'
            });

            if ($scope.msgAlerta) {
                var fs = require("fs");
                var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
                fs.writeFileSync(tempDir3() + 'versao.txt', pegaDataVersao);
            }

            $('div.notification').fadeIn(500);
        });
        $('#pag-chamadas .grid').css("margin-top", "100px")
        $view = $("#pag-chamadas");

        $(".dropdown-menu").mousemove(function(event) {
            if ($("#modalAlerta").attr("aria-hidden") == "false") {
                $('#listaM').remove();
            } else {
                $("#modalAlerta").remove();
            }
        });

        //minuteStep: 5

        componenteDataHora($scope, $view);

        // Carregar filtros
        carregaAplicacoes($view, false, false, $scope);
        carregaSites($view);
        carregaFalhas2($view,true);
        carregaJsons($view,true);
        carregaSegmentosPorAplicacao($scope, $view, true);

        // Guardar filtros              
        viewOnChangeGuardaFiltros ($('select'), $scope, $view);
        
        // Marcar todos
        viewOnClickMarcaTodos($("[id^=alink]"),$scope,$view);
  				
        $('.filtro-aplicacao >.dropdown-menu li a').click(function(){
            $scope.filtroEscolhido.ic = [];
            $scope.$apply();
        })

        $("#import2").click(function() {
            $('#modalConfirma').css("z-index", "1045");
        });

        $("#confirma").click(function() {
            $("#browser").trigger("click");
        });

        $("#confirma").click(function() {
            $('#modalConfirma').css("z-index", "-999");
        });

        $('#browser').change(function() {
            numanis = fs.readFileSync($("#browser").val()).toString().split('\r\n');
            var n1 = [];
            var n2 = [];

            if (numanis.length > 0) {
                for (var i = 0; i < numanis.length; i++) {
                    if (n1.indexOf(numanis[i].trim()) < 0 && numanis[i].trim() !== "" && numanis[i].trim().length >= 10 && numanis[i].trim().length <= 11) {
                        n1.push(numanis[i].trim());
                    } else if (numanis[i].trim() !== "") {
                        n2.push(numanis[i].trim());
                    }
                }
                if (numanis.length > 0) {
                    numanis = n1;
                    invalidsNumanis = n2;
                    $('#import2').css('background', '#A9D0F5').selectpicker('refresh');
                } else {
                    numanis = [];
                    invalidsNumanis = [];
                    $("#browser").val("");
                    alert("Arquivo fora de especificação. O padrão é DDD + 8 ou 9 dígitos para cada linha.");
                }
            }
        });        

        $("#pag-chamadas").on("click", ".filtro-falhas div.dropdown-menu.open ul li", function() {
            if (changeFalhas === false) {
                Estalo.filtros.filtro_falhas = [];
                $('select.filtro-falhas').val('').selectpicker('refresh');
            }
            changeFalhas = false;
        });

        $("#pag-chamadas").on("click", ".dropdown-menu.opcao", function() {
            if ($('.filtro-chamador').attr('placeholder') === "CPF/CNPJ" || $('.filtro-chamador').attr('placeholder') === "Contrato") {
                $('#callFlow').attr('disabled', true);
                $('#callFlow').prop('checked', false);
                $scope.callFlow = false;
            } else {
                $('#callFlow').attr('disabled', false);
            }
        });
              
        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function() {
            limpaProgressBar($scope, "#pag-chamadas");
            // Testar se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function() {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            var filtro_jsons = $view.find("select.filtro-jsons").val() || [];

            if (filtro_jsons.length > 1 && $('#contextVal').val() === "") {
                pv = [];
                for (var i = 1; i < filtro_jsons.length; i++) {
                    pv.push(';');
                }
                $('#contextVal').val(pv.join(""));
            }

            var contexto = $('#contextVal').val().split(';');
            if (contexto[0] === "" && contexto.length === 1 && filtro_jsons.length === 0) contexto = [];

            if (filtro_jsons.length !== contexto.length) {
                setTimeout(function() {
                    atualizaInfo($scope, '<font color = "White">O número de variáveis de contexto deve ser o mesmo que os valores pretendidos separados por ponto e vírgula</font>');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                    $view.find(".btn-exportar").prop("disabled", "disabled");
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();
            $scope.listaChamadas.apply(this);

                // Informação sobre consolidação
                /*delay($(".filtro-aplicacao >.dropdown-menu li a"), function() {
                    cache.apls.forEach(function(apl) {
                        if (apl.nome === aplFocus) {
                            var stmt = db.use + " SELECT 'Aplicação " + apl.codigo + " atualizada até',MAX(DataHora) FROM HistoricoConsolidacao WHERE CodAplicacao = '" + apl.codigo + "'";
                            executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function(dado) {
                                console.log(dado);
                            });
                        }
                    });
                });*/
        });


        $view.on("dblclick", "div.ng-scope.ngRow", function() {
            $scope.$apply(function() {
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_prerouting = true;
                $scope.ocultar.log_preroutingret = true;
                $scope.ocultar.log_formatado = false;
                $scope.ocultar.log_prompts = true;

            });
            $view.find("#modal-log").modal('show');
        });

        $view.on("click", "tr.chamada", function() {
            var that = $(this);
            $('tr.chamada.marcado').toggleClass('marcado');
            $scope.$apply(function() {
                that.toggleClass('marcado');
            });
        });


        $view.on("click", ".btn-log-prerouting", function(ev) {
            killThreads();
            ev.preventDefault();

            $scope.consultaCallLog($scope.chamada_atual);

            $scope.$apply(function() {
                $scope.ocultar.log_formatado = true;
                $scope.ocultar.log_prerouting = false;
                $scope.ocultar.log_preroutingret = true;
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_prompts = true;

            });
        });

        $view.on("click", ".btn-log-preroutingret", function(ev) {
            killThreads();
            ev.preventDefault();

            $scope.consultaCallLog($scope.chamada_atual);

            $scope.$apply(function() {
                $scope.ocultar.log_formatado = true;
                $scope.ocultar.log_prerouting = true;
                $scope.ocultar.log_preroutingret = false;
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_prompts = true;

            });
        });

        $view.on("click", ".btn-log-original", function(ev) {
            ev.preventDefault();

            $scope.consultaCallLog($scope.chamada_atual);

            $scope.$apply(function() {
                $scope.ocultar.log_formatado = true;
                $scope.ocultar.log_prerouting = true;
                $scope.ocultar.log_preroutingret = true;
                $scope.ocultar.log_original = false;
                $scope.ocultar.log_prompts = true;

            });
        });

        $view.on("click", ".btn-log-formatado", function(ev) {
            killThreads();
            ev.preventDefault();

            $scope.formataXML($scope.chamada_atual);

            $scope.$apply(function() {
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_prerouting = true;
                $scope.ocultar.log_preroutingret = true;
                $scope.ocultar.log_formatado = false;
                $scope.ocultar.log_prompts = true;

            });

            $('.ng-scope .prompt span').removeClass('bubble');
            $('.ng-scope .prompt span').removeClass('ura');
            $('.ng-scope .resposta span').removeClass('bubble');
            $('.ng-scope .resposta span').removeClass('me'); 
            $('.img_play').remove();
        });

        $view.on("click", ".btn-log-prompts", function(ev) {
            killThreads();
            ev.preventDefault();

            $scope.formataXML($scope.chamada_atual);

            $scope.$apply(function() {
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_prerouting = true;
                $scope.ocultar.log_preroutingret = true;
                $scope.ocultar.log_formatado = true;
                $scope.ocultar.log_prompts = false;

            });

            $('.ng-scope .prompt span').addClass('bubble');
            $('.ng-scope .prompt span').addClass('ura');
            $('.ng-scope .resposta span').addClass('bubble');
            $('.ng-scope .resposta span').addClass('me'); 
            if ($('.ura .img_play').length == 0) $('.ura').append('<img src="' + tempDir3() + 'img/play.png" class="img_play" onclick="playerPrompt(this)">');

        });

        $view.on("click", ".btn-log-formatadoEdEnx", function(ev) {
            killThreads();
            $('.btn-log-formatadoAllEdEnx').attr('checked', false);
            $scope.estadoEnxAll = false
            $scope.formataXML($scope.chamada_atual, true);
            $scope.$apply();
        });

        $view.on("click", ".btn-log-formatadoAllEdEnx", function(ev) {
            killThreads();
            $('.btn-log-formatadoEdEnx').attr('checked', false);
            $scope.estadoEnx = false
            $scope.formataXML($scope.chamada_atual, true);
            $scope.$apply();
        });

        $view.on("click", ".btn-log-formatadoIcEnx", function(ev) {
            killThreads();
            $('.btn-log-formatadoAllIcEnx').attr('checked', false);
            $scope.itemEnxAll = false;
            $scope.formataXML($scope.chamada_atual, true);
            $scope.$apply();
        });

        $view.on("click", ".btn-log-formatadoAllIcEnx", function(ev) {
            killThreads();
            $('.btn-log-formatadoIcEnx').attr('checked', false);
            $scope.itemEnx = false;
            $scope.formataXML($scope.chamada_atual, true);
            $scope.$apply();
        });

        $view.on("click", ".btn-exportar", function() {
            $scope.exportaXLSX.apply(this);
        });

        $view.on("click", ".btn-limpar-filtros", function() {
            $scope.porRegEDDD = false;
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function() {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function() {
                $btn_limpar.button('reset');                
            }, 500);
        }

        // Botão agora
        $view.on("click", ".btn-agora", function() {
            $scope.agora.apply(this);
        });

        $scope.agora = function() {
            iniciaAgora($view, $scope);
        };
		
		$view.on("click", ".btn-exportarCSV-interface", function() {
			var newName = formataDataHoraMilis(new Date()).replace(/[^0-9]/g,'');
			if(csvExportado === ''){
				fs.appendFileSync(tempDir3() + 'dados_chamadas.csv', tempData,'ascii');				
				fs.renameSync(tempDir3() + 'dados_chamadas.csv',	tempDir3() + 'export_'+newName+'.csv');
				childProcess.exec(tempDir3() + 'export_'+newName+'.csv');
				csvExportado = tempDir3() + 'export_'+newName+'.csv';
			}else{
				alert("Dado já foi exportado("+csvExportado+").");
			}
			
		});

        // Botão abortar
        $view.on("click", ".abortar", function() {
            killThreads();     
            var tempReso = $(".row-fluid").outerHeight() + 10;  
            $scope.abortar.apply(this);
			var $btn_exportar = $view.find(".btn-exportar");
			var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
			var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
			$btn_exportar.prop("disabled", false);
			$btn_exportar_csv.prop("disabled", false);
			$btn_exportar_dropdown.prop("disabled", false);			
        });

        $scope.abortar = function() {
            abortar($scope, "#pag-chamadas");
        };
		
		if($scope.notificationAlways){
			$view.on("mouseover",".grid",function(){$('div.notification').fadeIn(500)})
		}

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });



    // Lista chamadas conforme filtros
    $scope.listaChamadas = function() {
		blocoCSV = 1000;
		csvExportado = '';
		if (fs.existsSync(tempDir3() + 'dados_chamadas.csv')) fs.unlinkSync(tempDir3() + 'dados_chamadas.csv');			
		var header = '';
		for (var item in $scope.colunas) {
			if(typeof($scope.colunas[item]) === "object"){
				if($scope.colunas[item].field !== "ucid"){
					header += $scope.colunas[item].displayName +';';
				}else{
					header += 'transferID;';
				}
			}                
		}			
		header += 'CTXID;Satis.;Recom.;\r\n';
		tempData = header;
		
        var placeholder = $scope.filtros.chamador;
        $globals.numeroDeRegistros = 0;
        $scope.ordenacao = ['data_hora', 'chamador'];
        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var filtro_chamadores = $scope.filtros.chamador;
        if (filtro_chamadores === "x") filtro_chamadores = "";
        if (filtro_chamadores.match(/^\d*x\d*$/)) {
            filtro_chamadores = filtro_chamadores.replace("x", "%");
        }

        filtro_chamadores = filtro_chamadores !== "" ? [filtro_chamadores] : [];

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

        if (placeholder === "" && !$scope.callFlow && filtro_aplicacoes.length === 0 && numanis.length === 0 && invalidsNumanis.length === 0) {
            atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
            $btn_gerar.button('reset');
            return;
        }

        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_jsons = $view.find("select.filtro-jsons").val() || [];
        var contex = $('#contextVal').val().split(';');

        if (filtro_jsons.length === 0 && contex[0] === "") contex = [];

        var filtro_valJsons = contex || [];
        var filtro_falhas = $view.find("select.filtro-falhas").val() || [];
        var texto = "",
            ed = $scope.filtroEscolhido.ed || [], 
            ic = $scope.filtroEscolhido.ic ||  [];
        $scope.filtros.isUltimoED === true ? texto = "Último estado" : texto = "Estados de diálogo";
		
		
		var edsSelect = [];
		if(ed !== undefined){
			if(ed.length > 0){
				filtro_aplicacoes.forEach(function(apl){
				var eds = cache.aplicacoes[apl].estados.filter(function(c){ if($scope.filtroEscolhido.ed.indexOf(c.nome)>=0) return c });
				eds.forEach(function(e){edsSelect.push(e.codigo)});
				});
				unique2(edsSelect);
			}
		}				
        /*var edNome = $scope.filtroEscolhido.ed ? $scope.filtroEscolhido.ed.map(function(e) {
            return '<se na="' + obtemNomeEstado($('.filtro-aplicacao').val(), e) + '"';
        }) : [];*/

        $scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));

        if ($scope.ocultar.tratado) {
            $('#pag-chamadas table').css({
                'margin-top': '40px',
                'margin-bottom': '100px',
                'max-width': '1190px',
                'margin-left': 'auto',
                'margin-right': 'auto'
            });
            $scope.$apply();
        } else {
            $('#pag-chamadas table').css({
                'margin-top': '40px',
                'margin-bottom': '100px',
                'max-width': '1310px'
            });
            $scope.$apply();
        }

        // filtros usados
        $scope.filtros_usados = "";

        if (placeholder === "") {
            $scope.filtros_usados += " Período: " + formataDataHoraBR($scope.periodo.inicio) +
                " até " + formataDataHoraBR($scope.periodo.fim);
        }

        if (numanis.length > 0 && $('#data').prop("checked") == false) {
            $scope.filtros_usados = "";
        }

        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_falhas != "Falhas") {
            $scope.filtros_usados += " Falha: " + filtro_falhas;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
        if (filtro_jsons.length > 0) {
            $scope.filtros_usados += " Variáveis: " + filtro_jsons;
        }
        if (filtro_valJsons.length > 0 && filtro_valJsons[0] !== "") {
            $scope.filtros_usados += " Valores: " + filtro_valJsons;
        }

        if (filtro_chamadores != "") {
            $scope.filtros_usados += ($('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? " Chamador: " :
                $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' ? " Telefone tratado: " : $('.filtro-chamador').attr('placeholder') === 'Contrato' ? " Contrato: " :
                $('.filtro-chamador').attr('placeholder') === 'Protocolo' ? " Protocolo: " : " CPF/CNPJ: ") + filtro_chamadores;
        }
        if (ed.length > 0 && !$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
            $scope.filtros_usados += " " + texto + ": " + $scope.filtroEscolhido.ed.join(',');
        }
        if (ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
            $scope.filtros_usados += " Items de controle: " + ic;
        }

        $scope.chamadas = [];
        $scope.csv = [];

        var xml = "";
        var nps = "";
        var tabela = data_ini.getMonth() === data_fim.getMonth() ? ( filtro_chamadores != "" ? "IVRCDR" : "IVRCDR_"+_02d(data_ini.getMonth()+1)) : "IVRCDR";
	
        if (ic.length > 0) {
            var likes = ic.map(function(codIC) {
                return "XMLIVR LIKE '%<ty>pc</ty><va>" + codIC + "</va>%'";
            });
            xml = ", CASE WHEN " + likes.join(" OR ") + " THEN 0 ELSE 1 END AS XMLIVR";
        }

        if ($scope.filtros.nps === true) {
            nps = ', respostaPesq';
        }

        var valor = placeholder !== "" ? 10000 : 25000;
        var stmt = db.use +
            " SELECT" + testaLimite(valor) +
            "   IVR.CodUCIDIVR, IVR.DataHora_Inicio, NumANI, ClienteTratado, IVR.CodAplicacao," +
            "   QtdSegsDuracao, SegmentoChamada, IndicSucessoGeral, CodUltEstadoRec " + xml +
            ", IndicDelig, transferid, ctxid, CodSite" + nps +
            (filtro_falhas !== "Falhas" ? ', ErroAp, ErroCtg, SemPmt, SemDig, ErroVendas' : '') +
            (filtro_jsons.length > 0 ? ', PreRoteamentoRet + PreRoteamento + Contexto AS PreRoteamentoRet' : '') +
			((placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ')?', NumCPFCNPJ':'') +
            " ,reconhecimentoVoz, Sequencia_EstadoDialogo, PreRoteamentoRet as PreRoteamentoRetJSON" +
            " FROM " + db.prefixo + tabela + " as IVR";

        if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
            stmt += " JOIN CONTRATO_CHAMADA AS CC" +
                "   ON CC.CodUCIDIVR = IVR.CodUCIDIVR AND CC.CodAplicacao = IVR.CodAplicacao";
        }
        stmt += " WHERE 1 = 1";

        if (placeholder === "") {
            stmt += "   AND IVR.DataHora_Inicio >= '" + formataDataHora($scope.periodo.inicio) + "'" +
                "   AND IVR.DataHora_Inicio <= '" + formataDataHora($scope.periodo.fim) + "'";
        }

        if (filtro_aplicacoes.map === undefined) {
            filtro_aplicacoes = [filtro_aplicacoes];
        }
        stmt += restringe_consulta("IVR.CodAplicacao", filtro_aplicacoes, true);

        // Testar se filtra por chamador ou cliente tratado
        if (filtro_chamadores.length > 0) {
            var campoTelefone = "";
            var nonoDigito = false;

            if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
                campoTelefone = "NumAni";
                nonoDigito = true;
            } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
                campoTelefone = "ClienteTratado";
                nonoDigito = true;
            } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ') {                
				campoTelefone = "NumCPFCNPJ";
            } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
                campoTelefone = "CC.CONTRATO";
            } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Protocolo') {
                campoTelefone = "Protocolo";
            }

            if (filtro_chamadores[0].indexOf("%") !== -1) {
                stmt += restringe_consulta_like(campoTelefone, filtro_chamadores);
            } else {
                // Tratar nono dígito
                if (nonoDigito && (filtro_chamadores[0].length === 11 || filtro_chamadores[0].length === 10)) {
                    if (filtro_chamadores[0].length === 10) {
                        var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2), filtro_chamadores[0].substring(0, 2) + 9);
                    }
                    if (filtro_chamadores[0].length === 11) {
                        var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2) + 9, filtro_chamadores[0].substring(0, 2));
                    }
                    filtro_chamadores.push(newNum);
                }
				stmt += " AND "+ campoTelefone + " = '"+filtro_chamadores[0]+"'";
                //stmt += restringe_consulta(campoTelefone, filtro_chamadores, true);
            }
        }else{
			stmt += " order by dataHora_Inicio, numANI";
		}

        stmtOriginal = stmt;
        dataHoraPesquisa = formataDataHora(new Date());

        if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' || placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
            var tipoTel = $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? "NumAni" : "ClienteTratado";
            var telCallFLow = false;
            for (var i = 0; i < filtro_chamadores.length; i++) {
                if (obtemTelCallFlow(filtro_chamadores[i]) == true) {
                    telCallFLow = true;
                }
            }           
            if (telCallFLow) {
                stmtOriginal = stmtOriginal.replace('IVRCDR', 'chamadascallflow') + " ";
            }
        }

        var stmts = [];

        if (numanis.length > 0 && placeholder === "") {
            var tipoTel = $('#2h').prop("checked") ? "Telefone Chamador" : "Telefone Tratado";
            stmts = arrayNumAnisQuery(stmt, numanis, tipoTel, $scope.extratorArquivoPorData, data_ini, data_fim);
            $('#import2').css('background', '#f5f5f5').selectpicker('refresh');
            if($scope.extratorInvalidos){
                for (var i = 0; i < invalidsNumanis.length; i++) {
                    $scope.chamadas.push({
                        UCID: "",
                        data_hora: "",
                        data_hora_BR: "",
                        data: "",
                        chamador: $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' ? invalidsNumanis[i] : "",
                        tratado: $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? invalidsNumanis[i] : "",
                        cod_aplicacao: "",
                        aplicacao: "",
                        duracao: "",
                        segmento: "",
                        encerramento: "",
                        estados: "",
                        estado_final: "",
                        transferid: "",
                        ctxid: "",
                        reconhecimentoVoz: ""
                    });

                }
            }

            numanis = [];
            invalidsNumanis = [];
            $("#browser").val("");
        } else {
            if (placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' || 
                placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
                //stmt = "select getdate()/*" + stmtOriginal + "*/";
				var  stmtFinal = stmtOriginal;
				if($scope.nonodigito) stmtFinal +=" union all "+ stmtOriginal.replace(campoTelefone+" = '"+filtro_chamadores[0]+"'",campoTelefone+" = '"+filtro_chamadores[1]+"'");				
                stmts = [stmtFinal];//, "select getdate()"];//, stmt, "select getdate()/*" + stmtOriginal + "*/"];
            } else {
                if (ed.indexOf("0") < 0) {
                    if (placeholder === "") {
                        //Por 5 minutos
                        stmts = arrayCincoMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
                    } else {
                        stmts.push(stmt);
                    }
                } else {
                    if (placeholder === "") {
                        //Por minuto
                        stmts = arrayMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
                    } else {
                        stmts.push(stmt);
                    }
                }
            }
        }

        var executaQuery = "";        

        if (placeholder !== "") {
            executaQuery = executaQuery3;
            log(stmts[0]);
            disparo = new Date();
        } else {
            if ($scope.filtros.isUltimoED === true) {
                executaQuery = executaQuery2;
                log("Último ED => " + stmt);
                disparo = new Date();
            } else {
                executaQuery = executaQuery1;
                log(stmt);
                disparo = new Date();
            }
        }

        var controle = 0;
        var total = 0;
        var contador = 0;
        var tempoIni = 0;
        var tempoFim = 0;
        var tempoDecorrido = 0;
        var ucids = [];        
        var disparo;        
        var chamadasTemp = [];
        //preRotRetList = [];
        // Padrão
        function executaQuery1(columns) {
            // preRotRetJSON1 = columns["PreRoteamentoRetJSON"].value !== "" ? columns["PreRoteamentoRetJSON"].value : "";
            // preRotRetJSON1 = preRotRetJSON1 ? JSON.parse(preRotRetJSON1.replace(/[{]/g, '{"').replace(/[,]/g, ',"').replace(/[:]/g, '":')) : "";
            // preRotRetList.push(preRotRetJSON1);
            contador++;
            var UCID = columns[0].value,
                data_hora = formataDataHora(columns[1].value),
                data_hora_BR = formataDataHoraBR(columns[1].value),
                data = columns[1].value;            

            var chamador = columns[2].value,
                tratado = columns[3].value,
                cod_aplicacao = columns[4].value,
                aplicacao = obtemNomeAplicacao(columns[4].value),
                duracao = columns[5].value,
                cod_segmento = columns[6].value,
                segmento = cod_segmento,
                encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
                estados =  columns["Sequencia_EstadoDialogo"].value.split(","),
                estado_final = obtemEstado(columns[4].value, columns[8].value) !== undefined ? obtemEstado(columns[4].value, columns[8].value).nome : "",
                transferid = columns["transferid"].value,
                reconhecimentoVoz = columns["reconhecimentoVoz"].value,
                ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
                codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
                preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
                xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",                
                npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
                npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";
            
			var preroteamentoretJSON = columns["PreRoteamentoRetJSON"].value !== "" ? columns["PreRoteamentoRetJSON"].value : "";
            
			var eps = "";
            var regraDR3 = "";			
			try{
                preroteamentoretJSON = preroteamentoretJSON ? JSON.parse(preroteamentoretJSON.replace(/[{]/g, '{"').replace(/[,]/g, ',"').replace(/[:]/g, '":')) : "";
				 eps = preroteamentoretJSON && preroteamentoretJSON['a0'] ? preroteamentoretJSON['a0'] : "";
				 regraDR3 = preroteamentoretJSON && preroteamentoretJSON['aa'] ? preroteamentoretJSON['aa'] : "";
            }catch(ex){
            }
           
     
            if (filtro_falhas != "Falhas") {
                var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
                    erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
                    erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
                    errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
                    errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
            }

            if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
                segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
            }

            chamadasTemp.push({
                UCID: UCID,
                data_hora: data_hora,
                data_hora_BR: data_hora_BR,
                data: data,
                chamador: chamador,
                tratado: tratado,
                cod_aplicacao: cod_aplicacao,
                aplicacao: aplicacao,
                duracao: duracao,
                segmento: segmento,
                cod_segmento: cod_segmento,
                encerramento: encerramento,
                reconhecimentoVoz: reconhecimentoVoz,
                estados: estados,
                estado_final: estado_final,
                transferid: transferid,
                ctxid: ctxid,
                codsite: codsite,
                preroteamentoret: preroteamentoret,
                xmlivr: xmlivr,
                npss: npss,
                npsr: npsr,
                erroap: erroap,
                erroctg: erroctg,
                erropmt: erropmt,
                errodig: errodig,
                errovendas: errovendas,
                eps : eps,
                regraDR3 : regraDR3
            });
        }

        // Último estado
        function executaQuery2(columns) {         
            var UCID = columns[0].value,
                data_hora = formataDataHora(columns[1].value),
                data_hora_BR = formataDataHoraBR(columns[1].value),
                data = columns[1].value;
            var chamador = columns[2].value,
                tratado = columns[3].value,
                cod_aplicacao = columns[4].value,
                aplicacao = obtemNomeAplicacao(columns[4].value),
                duracao = columns[5].value,
                cod_segmento = columns[6].value,
                segmento = cod_segmento,
                encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
                estados = columns["Sequencia_EstadoDialogo"].value.split(","),
                estado_final = obtemEstado(columns[4].value, columns[8].value) !== undefined ? obtemEstado(columns[4].value, columns[8].value).nome : "",
                cod_estado_final = columns[8].value,
                transferid = columns["transferid"].value,
                reconhecimentoVoz = columns["reconhecimentoVoz"].value,
                ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
                codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
                preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
                xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",
                npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
                npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";
            var preroteamentoretJSON = columns["PreRoteamentoRetJSON"].value !== "" ? columns["PreRoteamentoRetJSON"].value : "";
            
			var eps = "";
            var regraDR3 = "";			
			try{
                preroteamentoretJSON = preroteamentoretJSON ? JSON.parse(preroteamentoretJSON.replace(/[{]/g, '{"').replace(/[,]/g, ',"').replace(/[:]/g, '":')) : "";
				eps = preroteamentoretJSON && preroteamentoretJSON['a0'] ? preroteamentoretJSON['a0'] : "";
				regraDR3 = preroteamentoretJSON && preroteamentoretJSON['aa'] ? preroteamentoretJSON['aa'] : "";
            }catch(ex){
            }

            if (filtro_falhas != "Falhas") {
                var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
                    erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
                    erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
                    errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
                    errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
            }

            if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
                segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
            }         

            var filtro_eds = edsSelect;   

            if (filtro_eds[0] === cod_estado_final){
                chamadasTemp.push({
                    UCID: UCID,
                    data_hora: data_hora,
                    data_hora_BR: data_hora_BR,
                    data: data,
                    chamador: chamador,
                    tratado: tratado,
                    cod_aplicacao: cod_aplicacao,
                    aplicacao: aplicacao,
                    duracao: duracao,
                    segmento: segmento,
                    cod_segmento: cod_segmento,
                    encerramento: encerramento,
                    reconhecimentoVoz: reconhecimentoVoz,
                    estados: estados,
                    estado_final: estado_final,
                    transferid: transferid,
                    ctxid: ctxid,
                    codsite: codsite,
                    preroteamentoret: preroteamentoret,
                    xmlivr: xmlivr,
                    npss: npss,
                    npsr: npsr,
                    erroap: erroap,
                    erroctg: erroctg,
                    erropmt: erropmt,
                    errodig: errodig,
                    errovendas: errovendas,
                    eps: eps,
                    regraDR3: regraDR3
                });
            }
        }

        // Consulta por telefone, cpf ou protocolo
        function executaQuery3(columns) {          
            //preRotRetList.push(preRotRetJSON3);
            if (columns["CodUCIDIVR"] !== undefined) {
                var UCID = columns[0].value,
                    data_hora = formataDataHora(columns[1].value),
                    data_hora_BR = formataDataHoraBR(columns[1].value),
                    data = columns[1].value;
                var chamador = columns[2].value,
                    tratado = columns[3].value,
                    cod_aplicacao = columns[4].value,
                    aplicacao = obtemNomeAplicacao(columns[4].value),
                    duracao = columns[5].value,
                    cod_segmento = columns[6].value,
                    segmento = cod_segmento,
                    encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
                    estados = columns["Sequencia_EstadoDialogo"].value.split(","),
                    estado_final = obtemEstado(columns[4].value, columns[8].value) !== undefined ? obtemEstado(columns[4].value, columns[8].value).nome : "",
                    transferid = columns["transferid"].value,
                    reconhecimentoVoz = columns["reconhecimentoVoz"].value,
                    ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
                    codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
                    preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
                    xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",                    
                    npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
                    npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";
                var preroteamentoretJSON = columns["PreRoteamentoRetJSON"].value !== "" ? columns["PreRoteamentoRetJSON"].value : "";
                
				var eps = "";
				var regraDR3 = "";			
				try{
					preroteamentoretJSON = preroteamentoretJSON ? JSON.parse(preroteamentoretJSON.replace(/[{]/g, '{"').replace(/[,]/g, ',"').replace(/[:]/g, '":')) : "";
					 eps = preroteamentoretJSON && preroteamentoretJSON['a0'] ? preroteamentoretJSON['a0'] : "";
					 regraDR3 = preroteamentoretJSON && preroteamentoretJSON['aa'] ? preroteamentoretJSON['aa'] : "";
				}catch(ex){
				}

                if (filtro_falhas != "Falhas") {
                    var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
                        erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
                        erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
                        errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
                        errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
                }

                if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
                    segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
                }
      
                if (ucids.indexOf(UCID) < 0) {
                    if (placeholder !== "") {    

                        var s = {
                            UCID: UCID,
                            data_hora: data_hora,
                            data_hora_BR: data_hora_BR,
                            data: data,
                            chamador: chamador,
                            tratado: tratado,
                            cod_aplicacao: cod_aplicacao,
                            aplicacao: aplicacao,
                            duracao: duracao,
                            segmento: obtemNomeSegmento(segmento),
                            encerramento: encerramento,
                            reconhecimentoVoz: reconhecimentoVoz,
                            estados: estados,
                            estado_final: estado_final,
                            transferid: transferid,
                            ctxid: ctxid,
                            codsite: codsite,
                            preroteamentoret: preroteamentoret,
                            xmlivr: xmlivr,
                            npss: npss,
                            npsr: npsr,
                            erroap: erroap,
                            erroctg: erroctg,
                            erropmt: erropmt,
                            errodig: errodig,
                            errovendas: errovendas,
                            eps: eps,
                            regraDR3: regraDR3
                        };


                        $scope.chamadas.push(s);

                        var a = [];
                        for (var item in $scope.colunas) {
                            a.push(s[$scope.colunas[item].field]);
                        }
                        $scope.csv.push(a);

                        tempData += s.data_hora_BR + ';' +
						s.chamador + ';' +
						s.tratado + ';' +
						s.aplicacao + ';' +						
						s.segmento + ';' +
						s.encerramento + ';' +
						s.estado_final + ';' +
                        s.duracao + ';' +
                        s.eps + ';' + 
                        s.regraDR3 + ';' + 
						(s.transferid !== "" ? s.transferid : "NA") + ';' +
						(s.ctxid !== "" ? "'"+s.ctxid : "NA") + ';' +
						(s.npss !== "" ? s.npss : "NA") + ';' +
                        (s.npsr !== "" ? s.npsr : "NA") + ';' + '\r\n';

                        $scope.$apply(function() {
                            $scope.ordenacao = ['data_hora', 'chamador'];

                            if (placeholder !== "" || stmts.lenght == 1) {
                                var arrSort = $scope.chamadas;
                                arrSort.sort(function(a, b) {
                                    return (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0);
                                });
                                $scope.chamadas = arrSort;
                            }
                        });
                    } else {
                        chamadasTemp.push({
                            UCID: UCID,
                            data_hora: data_hora,
                            data_hora_BR: data_hora_BR,
                            data: data,
                            chamador: chamador,
                            tratado: tratado,
                            cod_aplicacao: cod_aplicacao,
                            aplicacao: aplicacao,
                            duracao: duracao,
                            segmento: segmento,
                            cod_segmento: cod_segmento,
                            encerramento: encerramento,
                            reconhecimentoVoz: reconhecimentoVoz,
                            estados: estados,
                            estado_final: estado_final,
                            transferid: transferid,
                            ctxid: ctxid,
                            codsite: codsite,
                            preroteamentoret: preroteamentoret,
                            xmlivr: xmlivr,
                            npss: npss,
                            npsr: npsr,
                            erroap: erroap,
                            erroctg: erroctg,
                            erropmt: erropmt,
                            errodig: errodig,
                            errovendas: errovendas,
                            eps: eps,
                            regraDR3: regraDR3
                        });
                    }

                    ucids.push(UCID);
                }
            } else {
                if (columns['status'] !== undefined) {
                    contador++;
                    console.log("TIMER-> " + contador + " " + aplStatusMail);

                    if (contador > tempoLimite) {
                        controle++;
                    }

                    if (columns['status'].value !== null && columns['status'].value !== 2) {
                        columns['status'].value === 1 ? console.log('uar 1') : console.log('uar 0');
                        controle++;
                    }
                }
            }
        }

        if (placeholder === "") {
            //Data final do penúltimo array com base na hora final do filtro, último array é uma flag
            //Por hora
            if (ed.indexOf("0") < 0) {
                if (stmts.length <= 1) {
                    notificate("Utilize o campo texto para buscar um único telefone.", $scope);
                    $(".filtro-chamador").slideUp(300).slideDown(400);
                    killThreads();
                    var tempReso = $(".row-fluid").outerHeight() + 10;
            
                    $scope.abortar.apply(this);
                    return false;
                } else {
             
                    if (stmts[stmts.length - 2].toString().match(/IVR.DataHora_Inicio <= \'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\'/g) !== null) {
                        stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(/IVR.DataHora_Inicio <= \'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\'/g, "IVR.DataHora_Inicio <= \'" + formataDataHora(new Date(data_fim)) + "\' ")];                        
                    }
                }

            } else {
                //Data final do penúltimo array com base na hora final do filtro, último array é uma flag
                //Por minuto
                stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(formataDataHora(precisaoSegundoFim(data_fim)), formataDataHora(data_fim))];
            }
        }
		
		
	    function executaTestesDeInsercao() {
            //{PROJETO TABS
            var tempReso = $(".row-fluid").outerHeight() + 10;
      

            for (var c = 0; c < chamadasTemp.length; c++) {
                var inserir = true;

                if (ic.length > 0 && ed.length == 0) {
                    inserir = (chamadasTemp[c].xmlivr === 0);
                } else if (ed.length > 0 && ic.length == 0) {
                    for (var i = 0; i < $scope.filtroEscolhido.ed.length; i++) {
                        inserir = (chamadasTemp[c].estados.indexOf(edsSelect[i]) >= 0);
                        if (inserir) break;
                    }
                } else if (ed.length > 0 && ic.length > 0) {
                    for (var i = 0; i < ed.length; i++) {
                        inserir = (chamadasTemp[c].estados.indexOf(edsSelect[i]) >= 0);
                        if (inserir) break;
                    }
                    if (inserir) {
                        inserir = (chamadasTemp[c].xmlivr === 0);
                    }
                }

                // Site
                if (filtro_sites.length > 0 && inserir) {
                    inserir = (filtro_sites.indexOf(chamadasTemp[c].codsite) >= 0);
                }

                // Segmento
                if (filtro_segmentos.length > 0 && inserir) {
                    inserir = (filtro_segmentos.indexOf(chamadasTemp[c].cod_segmento) >= 0);
                }

                if (filtro_falhas !== "Falhas" && inserir) {
                    inserir = (chamadasTemp[c].erroap === 1 && filtro_falhas === "ErroAp" || chamadasTemp[c].erroctg === 1 && filtro_falhas === "ErroCtg" || chamadasTemp[c].erropmt === 1 && filtro_falhas === "SemPmt" || chamadasTemp[c].errodig === 1 && filtro_falhas === "SemDig" || chamadasTemp[c].errovendas === 1 && filtro_falhas === "ErroVendas");
                }

                if (filtro_jsons.length > 0 && inserir) {
                    var strs = $('#contextVal').val().split(';');
                    for (var i = 0; i < filtro_jsons.length; i++) {
                        var s = (strs[i] !== "" && strs[i] !== undefined) ? '"' + strs[i] + '"' : "";
                        inserir = ((chamadasTemp[c].preroteamentoret.toUpperCase().replace("|", "PIPE")).match((filtro_jsons[i] + ':' + s).toUpperCase().replace("|", "PIPE")) !== null);
                    }
                }

                if ($scope.filtros.nps === true && inserir) {
                    inserir = (chamadasTemp[c].npss !== "" || chamadasTemp[c].npsr !== "");
                }

                if (inserir) {       

                    var s = {
                        UCID: chamadasTemp[c].UCID,
                        data_hora: chamadasTemp[c].data_hora,
                        data_hora_BR: chamadasTemp[c].data_hora_BR,
                        data: chamadasTemp[c].data,
                        chamador: chamadasTemp[c].chamador,
                        tratado: chamadasTemp[c].tratado,
                        cod_aplicacao: chamadasTemp[c].cod_aplicacao,
                        aplicacao: chamadasTemp[c].aplicacao,
                        duracao: chamadasTemp[c].duracao,
                        segmento: obtemNomeSegmento(chamadasTemp[c].segmento),
                        encerramento: chamadasTemp[c].encerramento,
                        reconhecimentoVoz: chamadasTemp[c].reconhecimentoVoz,
                        estados: chamadasTemp[c].estados,
                        estado_final: chamadasTemp[c].estado_final,
                        transferid: chamadasTemp[c].transferid,
                        ctxid: chamadasTemp[c].ctxid,
                        npss: chamadasTemp[c].npss,
                        npsr: chamadasTemp[c].npsr,
                        eps: chamadasTemp[c].eps,
                        regraDR3: chamadasTemp[c].regraDR3

                    }
					
					if ($scope.chamadas.length < $scope.limite_registros) {						
						$scope.chamadas.push(s);					
					}						
										
					tempData += s.data_hora_BR + ';' +
						s.chamador + ';' +
						s.tratado + ';' +
						s.aplicacao + ';' +						
						s.segmento + ';' +
						s.encerramento + ';' +
						s.estado_final + ';' +
                        s.duracao + ';' +
                        s.eps + ';' + 
                        s.regraDR3 + ';' + 
						(s.transferid !== "" ? s.transferid : "NA") + ';' +
						(s.ctxid !== "" ? "'"+s.ctxid : "NA") + ';' +
						(s.npss !== "" ? s.npss : "NA") + ';' +
                        (s.npsr !== "" ? s.npsr : "NA") + ';' + '\r\n';
							
							
					if (tempData.length > 240000) {							
						fs.appendFileSync(tempDir3() + 'dados_chamadas.csv', tempData,'ascii');
						tempData = "";
					}                    
                }
            }
        }


        //padrão
        function proxima(s) {

            tempoIni = tempoFim;
            tempoFim = contador;
            tempoDecorrido = tempoFim - tempoIni;

            chamadasTemp = [];

            db.query(s, executaQuery, function(err, num_rows) {
				
				
				if(controle % 10 == 0 || controle === stmts.length - 1) console.log("Executando query-> " + s + " " + num_rows);

                num_rows !== undefined ? total += num_rows : total += 0;

                var telCallFLow = false;
                for (var i = 0; i < filtro_chamadores.length; i++) {
                    if (obtemTelCallFlow(filtro_chamadores[i]) === true) {
                        telCallFLow = true;
                    }
                }
				if (placeholder !== "") {
                    retornaStatusQuery($scope.chamadas.length, $scope);
                    var tempoPassado = new Date() - disparo;
                    log("Secs: " + militosec(tempoPassado) + " " + stmts[0]);
                    $btn_gerar.button('reset');
					stmts = [];
                    if ($scope.chamadas.length > 0) {
						fs.appendFileSync(tempDir3() + 'dados_chamadas.csv', tempData,'ascii');
						tempData = "";
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
                    } else {
                        $btn_exportar.prop("disabled", "disabled");
                        $btn_exportar_csv.prop("disabled", "disabled");
                        $btn_exportar_dropdown.prop("disabled", "disabled");
                    }
                } else {
					
                    $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, stmts.length) + " Pesquisados: " + total + ($scope.chamadas.length < $scope.limite_registros ? "" : " Inserindo em arquivo...") + (tempoDecorrido > 15000 ? " A consulta está demorando mais do que o esperado. " : "")).selectpicker('refresh');
                    controle++;
                    
                    if (controle % 6 === 0) {
                        $scope.$apply();
                    }

                    // Executa dbquery enquanto array de querys menor que length-1
                    if (controle < stmts.length - 1) {
                        if (placeholder === "") {
                            executaTestesDeInsercao();
                        }

                        proxima(stmts[controle].toString());
                    }

                    //Evento fim
                    if (controle === stmts.length - 1) {
                        if (placeholder === "") {
                            executaTestesDeInsercao();
                        }

                        if (err) {
                            retornaStatusQuery(undefined, $scope);
                            var tempoPassado = new Date() - disparo;
                            log("Secs: " + militosec(tempoPassado) + " " + stmt);

                        } else {
                            retornaStatusQuery($scope.chamadas.length, $scope);
                            var tempoPassado = new Date() - disparo;
                            log("Secs: " + militosec(tempoPassado) + " " + stmt);
                        }
                        console.log(err);
                        $btn_gerar.button('reset');
                        if ($scope.chamadas.length > 0) {
							fs.appendFileSync(tempDir3() + 'dados_chamadas.csv', tempData,'ascii');
							tempData = "";
                            $btn_exportar.prop("disabled", false);
                            $btn_exportar_csv.prop("disabled", false);
                            $btn_exportar_dropdown.prop("disabled", false);							
                            $('#grid').css('display', 'block');
                        } else {
                            $btn_exportar.prop("disabled", true);
							$btn_exportar_csv.prop("disabled", true);
							$btn_exportar_dropdown.prop("disabled", true);
                            $('#grid').css('display', 'none');
                        }
                    }
                }
            });
        }

        // Disparo inicial
        proxima(stmts[0].toString());

    };

    // Consulta o log da chamada
    $scope.consultaCallLog = function(chamada) {
		
        // Se a chamada acabou de ser consultada (incluindo o log original), não há nada a fazer
        if (chamada === $scope.chamada_atual && $scope.callLog !== "" && $scope.logxml !== "") return true;
		
		//Consulta por coducid interno
		if(($('.searchtool_text').val()).match(/[0-9A-Z]{14}|[0-9A-Z]{15}|[0-9A-Z]{16}/) !== null) chamada.UCID = ($('.searchtool_text').val());

        var tabela = "CallLog_";
        if (unique2(cache.apls.map(function(a) {
                if (a.nvp) return a.codigo;
            })).indexOf(chamada.cod_aplicacao) >= 0) {
            tabela = "CallLogNVP_";
        }

        if (chamada.cod_aplicacao === "MENUOI") {
            tabela = "CallLogM4U_";
        }
        if (chamada.cod_aplicacao.match('TELAUNICA')) {
            tabela = "CallLogTelaUnica_";
        }
        if (chamada.cod_aplicacao.match('S2S')) {
            tabela = "CruzamentoS2S";
        }

        $scope.callLog = '';
        $scope.$apply();

        var verificaLogs = "sp_spaceused " + tabela;
        if (tabela != "CruzamentoS2S") {
            verificaLogs += chamada.data_hora.substr(5, 2);
        }
        mssqlQueryTedious(verificaLogs, function(err, retorno) {
            if (err) console.log(err)
 
            if (retorno[0][1].value == 0) {
                var mesString = formataDataMesString(chamada.data_hora);
                $scope.callLog = 'Não há dados de log original de ' + mesString + ' no banco de dados.'
                $scope.$apply();
                
            } else {
                if (tabela === "CruzamentoS2S" || tabela === "CallLogTelaUnica_") {
                    var stmtCallLog = "SELECT TOP 1 " + (tabela === "CruzamentoS2S" ? "conteudoFinal" : "ConteudoOriginal") + " AS CONTEUDOORIGINAL " +
                        " FROM " + (tabela === "CallLogTelaUnica_" ? "CallLogTelaUnica_" + chamada.data_hora.substr(5, 2) : tabela) + "" +
                        " WHERE " +
                        " CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "' " +
                        (tabela === "CruzamentoS2S" ? "OR CodUCIDOriginal = '" + chamada.UCID + "'" : "OR CodUCID = '" + chamada.ctxid + "'");

                    console.log("Chamada: ");
                    console.log(chamada);
       
                } else {
                    var stmtCallLog = "SELECT TOP 1 Comprimido AS ARQUIVO " +
                        "FROM " + tabela + "" + chamada.data_hora.substr(5, 2) +
                        " WHERE CodUCID = '" + chamada.UCID + "'" +
                        "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'" +
                        " ORDER BY LEN(CodUCID)";
                }

                console.log(stmtCallLog + " Final");

                $scope.chamadaCallLogData = chamada.data;
                $scope.tabelaCallLog = tabela;
        			
				mssqlQueryTedious(stmtCallLog, function (erro, result) {
					var arquivoB;
					if (result.length > 0) {
						for (var i = 0; i < result.length; i++){
							arquivoB = result[i][0].value;
						}
						if (tabela === "CruzamentoS2S" || tabela === "CallLogTelaUnica_") {
							$scope.$apply(function() {	
								$scope.callLog = arquivoB;
								$scope.logValidado = validaLogGeral($scope.callLog);							
								$('#myModalLabel').text("Log da chamada");
								search(".log-original");
							});
							
						}else{
							var baseFileZip = chamada.UCID;
							var buffZip = toBuffer(toArrayBuffer(arquivoB));								
							fs.writeFileSync(baseFileZip +"_temp", buffZip, 'binary');	
							require('zlib').unzip(new Buffer(fs.readFileSync(baseFileZip +"_temp")), function(err, buf) {     
								if (err) console.log(err);
								$scope.$apply(function() {	
									$scope.callLog = buf.toString();
									$scope.logValidado = validaLogGeral($scope.callLog);								
									$('#myModalLabel').text("Log da chamada");
									search(".log-original");
								});								
								try{fs.unlinkSync(baseFileZip +"_temp");}catch(ex){}
							});
						}						
					}
				});	
            }
        }, function(err, num_rows) {});        
        return true;
    };

    $scope.infos = [];

    // Consulta e formata o XML da chamada
    $scope.formataXML = function(chamada, check) {
        $('#myModalLabel').text("Log da chamada");

        //miniQuery(chamada.cod_aplicacao);
		try{
			delete cache.aplicacoes[chamada.cod_aplicacao].prompts;			
		}catch(ex){
		}


        // Se a chamada acabou de ser consultada, não há nada a fazer
        if (check === undefined) {
            if (chamada === $scope.chamada_atual && logCarregado) return true;
        }

        // Limpar o log original da chamada consultada anteriormente
        $scope.chamada_atual = chamada;
        $scope.callLog = "";
        $scope.ocultar.log_original = true;
        $scope.ocultar.log_prerouting = true;
        $scope.ocultar.log_preroutingret = true;
        $scope.ocultar.log_formatado = false;
        $scope.ocultar.log_prompts = true;
        $scope.logxml = "";
        $scope.logxml_iit = "";
        var chamada_par = "";
        // Buscar o log em XML
        var stmt = "SELECT XMLIVR,PREROTEAMENTO,PREROTEAMENTORET,RECONHECIMENTOVOZ, NumCPFCNPJ FROM IVRCDR WHERE CodUCIDIVR = '" + chamada.UCID + "'";
        log(stmt);

        var xml = "",
            routing = "",
            routingRet = "",
            recVoz = "";
            numCPFCNPJ = "";
			
	    mssqlQueryTedious(stmt, function (erro, result) {			
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){			
					xml = result[i]["XMLIVR"].value;
					routing = result[i]["PREROTEAMENTO"].value;
					routingRet = result[i]["PREROTEAMENTORET"].value;
					parseXMLPrompts(result[i]["XMLIVR"].value, chamada);
					numCPFCNPJ = result[i]["NumCPFCNPJ"].value;  
					recVoz = result[i]["RECONHECIMENTOVOZ"].value;
				}
			}
		});

        //Buscar chamada par (origem e destino de IIT)
        var camposQuery = "   CodUCIDIVR, DataHora_Inicio, NumANI, ClienteTratado, CodAplicacao," +
        "   QtdSegsDuracao, SegmentoChamada, IndicSucessoGeral, CodUltEstadoRec," + 
        " IndicDelig, transferid, ctxid, CodSite,XMLIVR,Sequencia_EstadoDialogo,reconhecimentoVoz,PreRoteamentoRet";

        if (chamada.encerramento == "Transf.URA"){
            var queryIIT = "SELECT top 1 " + camposQuery + " FROM IVRCDR WHERE clienteTratado = '" + chamada.tratado + "'";
            queryIIT += " and CodUCIDIVR <> '" + chamada.UCID + "'"; 
            queryIIT += " and dataHora_Inicio between '" + chamada.data_hora+ "' and  DATEADD(Second,300,'" + chamada.data_hora + "')";
           
        }else{
            var queryIIT = "SELECT top 1 " + camposQuery + " FROM IVRCDR WHERE clienteTratado = '" + chamada.tratado + "'";
            queryIIT += " and CodUCIDIVR <> '" + chamada.UCID + "' and IndicDelig = 'IIT'"; 
            queryIIT += " and dataHora_Inicio between DATEADD(Second,-300,'" + chamada.data_hora + "') and '" + chamada.data_hora+ "'";
           
        }
        mssqlQueryTedious(queryIIT, function(err, retorno) {
            if (retorno[0] != undefined) 
            chamada_par = obterInfoChamada(retorno[0]);
            if (chamada_par != "") parseXMLPrompts(chamada_par.xmlivr, chamada_par);
        }, function(err, num_rows) {});
           
        var teste2 = setInterval(function() {
     
            if (cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') &&
                cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controle') &&
                cache.aplicacoes[chamada.cod_aplicacao].itens_controle['indice'] !== undefined) {
                clearInterval(teste2);

                $('div.modal-header h3').text("Log da chamada");
                $scope.$apply(function() {
                    // Formatar o log em XML
                    if ($scope.estadoEnx === true) {
                        if (chamada.encerramento == "Transf.URA"){
                            $scope.logxml_iit = parseXML(RemoverEDsExcedentes(xml), chamada, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                            if (chamada_par != "") $scope.logxml = parseXML(RemoverEDsExcedentes(chamada_par.xmlivr), chamada_par, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                        }else{
                            $scope.logxml = parseXML(RemoverEDsExcedentes(xml), chamada, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                            if (chamada_par != "") $scope.logxml_iit = parseXML(RemoverEDsExcedentes(chamada_par.xmlivr), chamada_par, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                        }
                    } else {
                        if (chamada.encerramento == "Transf.URA"){
                            $scope.logxml_iit = parseXML(xml, chamada, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                            if (chamada_par != "")  $scope.logxml = parseXML(chamada_par.xmlivr, chamada_par, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                        }else{
                            $scope.logxml = parseXML(xml, chamada, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                            if (chamada_par != "")  $scope.logxml_iit = parseXML(chamada_par.xmlivr, chamada_par, $scope.itemEnx, $scope.itemEnxAll,numCPFCNPJ,routing,routingRet);
                        }
                    }
		    
                    $scope.prerouting = obterRouting(routing);
                    $scope.preroutingret = obterRouting(routingRet);

                    /*if (fs.existsSync(tempDir3() + chamada.UCID + '_temp.txt')) {
                        fs.unlinkSync(tempDir3() + chamada.UCID + '_temp.txt');
                    }*/

                });
            } else {
                $('div.modal-header h3').text("Carregando...");
            }
        }, 1000);
        return true;
    };

    function obterInfoChamada(columns) {
        var UCID = columns[0].value,
            data_hora = formataDataHora(columns[1].value),
            data_hora_BR = formataDataHoraBR(columns[1].value),
            data = columns[1].value;            

        var chamador = columns[2].value,
            tratado = columns[3].value,
            cod_aplicacao = columns[4].value,
            aplicacao = obtemNomeAplicacao(columns[4].value),
            duracao = columns[5].value,
            cod_segmento = columns[6].value,
            segmento = cod_segmento,
            encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
            estados =  columns["Sequencia_EstadoDialogo"].value.split(","),
            estado_final = obtemEstado(columns[4].value, columns[8].value) !== undefined ? obtemEstado(columns[4].value, columns[8].value).nome : "",
            transferid = columns["transferid"].value,
            reconhecimentoVoz = columns["reconhecimentoVoz"].value,
            ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
            codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
            preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
            xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "";            
     

        if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
            segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
        }

        var call = {
            UCID: UCID,
            data_hora: data_hora,
            data_hora_BR: data_hora_BR,
            data: data,
            chamador: chamador,
            tratado: tratado,
            cod_aplicacao: cod_aplicacao,
            aplicacao: aplicacao,
            duracao: duracao,
            segmento: segmento,
            cod_segmento: cod_segmento,
            encerramento: encerramento,
            reconhecimentoVoz: reconhecimentoVoz,
            estados: estados,
            estado_final: estado_final,
            transferid: transferid,
            ctxid: ctxid,
            codsite: codsite,
            preroteamentoret: preroteamentoret,
            xmlivr: xmlivr
        };

        return call;

    }

    function obterRouting(valor) {
        if (valor === "" || valor === undefined) {
            return "Não há dados";
        }

        valor = valor.replace("}", "").replace("{", "");
        var obj = valor.split(",");

        for (var i = 0; i < obj.length; i++) {
            var cod = obj[i].substring(0, 2);
            var nome = obtemNomeVarJSon(cod);
            obj[i] = obj[i].replace(cod, nome).toUpperCase();
        }

        var resultado = obj.sort().toString();
        while (resultado.indexOf(",") > 0) {
            resultado = resultado.replace(",", "\n");
        }

        return resultado;
    }

    function obterRecVoz(valor) {
        if (valor === "" || valor == null) {
            return "";
        }
        if (valor.indexOf(",") == -1) return valor;
        valor = valor.replace("}", "").replace("{", "");
        valor = valor.replace("el\":", "Elocução:".bold());
        valor = valor.replace("re\":", "Resposta:".bold());
        valor = valor.replace("rc\":", "Reconhecimento:".bold());
        valor = valor.replace("du\":", "Duração:".bold());
        valor = valor.replace("sc\":", "Score:".bold());
        var obj = valor.split(",");
        var num = obj[0].replace("co\":", "");
        num = parseFloat(num.replace("\"", ""));
        obj[0] = obj[0].replace("co\":", "Confiança:".bold());
        var perc = num * 100;
        obj[0] = obj[0].replace(num, perc) + '%';
        var resultado = obj[1].toString() + "\n" + obj[2].toString() + "\n" + obj[0].toString() + '"';
        var i;
        for (i = 3; i < obj.length; i++) {
            resultado = resultado + "\n" + obj[i].toString();
        }
        return resultado;
    }

    // Exporta planilha XLSX
    $scope.exportaXLSX = function() {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

           // TEMPLATE
            var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo" +
                " FROM " + db.prefixo + "ArquivoRelatorios" +
                " WHERE NomeRelatorio='tChamadas'";
            log(stmt);
            db.query(stmt, function(columns) {
                var dataAtualizacao = columns[0].value,
                    nomeRelatorio = columns[1].value,
                    arquivo = columns[2].value;

                var milis = new Date();
                var baseFile = 'tChamadas.xlsx';
                var buffer = toBuffer(toArrayBuffer(arquivo));
                fs.writeFileSync(baseFile, buffer, 'binary');
                var file = 'detalhamentoChamadas_' + formataDataHoraMilis(milis) + '.xlsx';

                var newData;

                fs.readFile(baseFile, function(err, data) {
                    // Create a template
                    var t = new XlsxTemplate(data);

                    // Perform substitution
                    t.substitute(1, {
                        filtros: $scope.filtros_usados,
                        planDados: $scope.chamadas
                    });

                    // Get binary data
                    newData = t.generate();

                    if (!fs.existsSync(file)) {
                        fs.writeFileSync(file, newData, 'binary');
                        childProcess.exec(file);
                    }
                });

                
            }, function(err, num_rows) {                
                //?
            });
        /*}*/
        setTimeout(function() {
            $btn_exportar.button('reset');
        }, 500);
    };


    //Plugin jQuery
    jQuery.extend({
        highlight: function(node, re, nodeName, className) {
            if (node.nodeType === 3) {
                var match = node.data.match(re);
                if (match) {
                    var highlight = document.createElement(nodeName || 'span');
                    highlight.className = className || 'highlight';
                    var wordNode = node.splitText(match.index);
                    wordNode.splitText(match[0].length);
                    var wordClone = wordNode.cloneNode(true);
                    highlight.appendChild(wordClone);
                    wordNode.parentNode.replaceChild(highlight, wordNode);
                    return 1; //skip added node in parent
                }
            } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
                for (var i = 0; i < node.childNodes.length; i++) {
                    i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
                }
            }
            return 0;
        }
    });

    jQuery.fn.unhighlight = function(options) {
        var settings = {
            className: 'highlight',
            element: 'span'
        };
        jQuery.extend(settings, options);

        return this.find(settings.element + "." + settings.className).each(function() {
            var parent = this.parentNode;
            parent.replaceChild(this.firstChild, this);
            parent.normalize();
        }).end();
    };

    jQuery.fn.highlight = function(words, options) {
        var settings = {
            className: 'highlight',
            element: 'span',
            caseSensitive: false,
            wordsOnly: false
        };
        jQuery.extend(settings, options);

        if (words.constructor === String) {
            words = [words];
        }
        words = jQuery.grep(words, function(word, i) {
            return word != '';
        });
        words = jQuery.map(words, function(word, i) {
            return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        });
        if (words.length == 0) {
            return this;
        };

        var flag = settings.caseSensitive ? "" : "i";
        var pattern = "(" + words.join("|") + ")";
        if (settings.wordsOnly) {
            pattern = "\\b" + pattern + "\\b";
        }
        var re = new RegExp(pattern, flag);

        return this.each(function() {
            jQuery.highlight(this, re, settings.element, settings.className);
        });
    };

    //Fim do Plugin

    /* Função de busca */
    function searchAndHighlight(searchTerm, selector, resultElement) {
        if (searchTerm && !(searchTerm === " ")) {
            var selector = selector;


            if (selector == "" || !selector) {
                if ($("div.log-formatado").css('display') == 'block') {
                    selector = "div.log-formatado";
                }
                if ($("div.log-original").css('display') == 'block') {
                    selector = "div.log-original";
                }
                if ($("div.log-prerouting").css('display') == 'block') {
                    selector = "div.log-prerouting";
                }
                if ($("div.log-preroutingret").css('display') == 'block') {
                    selector = "div.log-preroutingret";
                }
            }
            //searchTerm = searchTerm.trim();

            $(selector).highlight(searchTerm, {
                className: 'match'
            });
            var matches = $('.match');
            // console.log("Matches: "+matches.length+" searchTerm "+searchTerm);
            // Funções dependentes de resultados a seguir
            if (matches != null && matches.length > 0) {

                try {
                    resultElement.text(matches.length + ' encontrados'); // Conta a quantidade de elementos encontrados
                } catch (er) {
                    console.log("Erro na função de pesquisa " + er);
                }

                // Marca o primeiro elemento MATCH como highlighted ao efetuar uma pesquisa
                $('.match:first').addClass('highlighted');

                var i = 0;

                // Função responsável por "scrollar" e marcar o próximo elemento match como highlighted
                $('.searchtool_next').on('click', function() {

                    i++;
                    //console.log("posicao",i)
                    if (i >= $('.match').length) i = 0;
                    
                    //Remove o atual e marca o próximo elemento match
                    $('.match').removeClass('highlighted');
                    $('.match').eq(i).addClass('highlighted');

                    // Cálculo final até a posição do elemento marcado highlighted
                    if(selector === "div.log-original"){
                        var container = $('.genericText'),
                        scrollTo = $('.match').eq(i);
                    }else{
                        var container = $('.modal-body'),
                        scrollTo = $('.match').eq(i);
                    }
                    
                        
                    var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

                    container.scrollTop(
                        wheretogo - 125
                    );
                    $('.highlighted').focus();

                    // Fim do cálculo e scroll
                });

                // Função para "Scrollar" e "Marcar" o anterior
                $('.searchtool_prev').on('click', function() {

                    i--;
                    if (i < 0) i = $('.match').length - 1;
                    //Remove o atual e marca o elemento anterior
                    $('.match').removeClass('highlighted');
                    $('.match').eq(i).addClass('highlighted');
                    //Cálculo final da posição para o scroll
                    
                    if(selector === "div.log-original"){
                        var container = $('.genericText'),
                        scrollTo = $('.match').eq(i);
                    }else{
                        var container = $('.modal-body'),
                        scrollTo = $('.match').eq(i);
                    }
                    var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

                    container.scrollTop(
                        wheretogo - 125
                    );

                    $('.highlighted').focus();

                });


                if ($('.highlighted:first').length) { //if match found, scroll to where the first one appears
                    $(window).scrollTop($('.highlighted:first').position().top);
                }
                return true;
            }
        }
        return false;
    }

    // Função responsável por checar atualizações no INPUT text
    $('.searchtool_text').each(function() {

        // ResultElement é o elemento onde a quantidade de resultados é exibida
        var resultElement = $('.search_results');
        var searchElement = $(this);

        search();
        // Save current value of element
        $(this).data('oldVal', $(this));

        // Look for changes in the value
        $(this).bind("propertychange keyup input paste", function(event) {

            // If value has changed...
            if ($(this).data('oldVal') != $(this).val()) {
                // Updated stored value
                $(this).data('oldVal', $(this).val());

                $("div .log-original").unhighlight({
                    className: 'match'
                });
                $("div .log-formatado").unhighlight({
                    className: 'match'
                });
                $("div .log-prerouting").unhighlight({
                    className: 'match'
                });
                $("div .log-preroutingret").unhighlight({
                    className: 'match'
                });
                if (!searchAndHighlight($(this).val(), "", resultElement)) {
                    resultElement.html('Sem resultados.');
                }
            }
        });
    }).delay(300);

    function clearSearchTool(searchElement, resultElement) {

        // Função responsável por resetar a função de pesquisa
        //console.log("Limpou a ferramenta de pesquisa de logs");

        if (resultElement == "" || resultElement == null) {
            resultElement = $('.search_results');
        }
        $(resultElement).html('');
        $(searchElement).val('');
        $(searchElement).attr("placeholder", "Buscar...");
        $("div .log-original").unhighlight({
            className: 'match'
        });
        $("div .log-formatado").unhighlight({
            className: 'match'
        });
        $("div .log-prerouting").unhighlight({
            className: 'match'
        });
        $("div .log-preroutingret").unhighlight({
            className: 'match'
        });
    };

    $(".changeSearchLog").click(function() {
        var resultElement = $('.search_results');
        clearSearchTool("", resultElement);
        //console.log("Chamou a função para limpar");
    });

    $("#modal-log").on('hidden', function() {
        var resultElement = $('.search_results');
        var searchElement = $('.searchtool_text');
        clearSearchTool(searchElement, resultElement);
        //console.log("Chamou a função para limpar");
    });

    /*Fim da função de busca*/

    //{ EDs Input Controle
    var modificaAlertaEDs = function(x) {
        if (x == "Elemento repetido ou número máximo de EDs acumulados") {
            $('.notification .ng-binding').text(x).selectpicker('refresh');
            setTimeout(function() {
                $('.notification .ng-binding').text("").selectpicker('refresh');
            }, 3000);
        } else {
            $("#filtro-ed").attr("placeholder", x);
        }
    };
    var verificaRepetidoEDs = function(x, item) {
        console.log(x);
        if (x.length == 3) return true;
        for (var z in x) {
            try {
                if (x[z].localeCompare(item) == 0) return true;
            } catch (err) {
                console.log(err);
            }
            try {
                if (x[z + 1].localeCompare(item) == 0) return true;
            } catch (err) {
                console.log(err);
            }
        }
        return false;
    };

    var arr2 = [];
    $('#filtro-ed').typeahead({
        source: $scope.options5opt,
        minLength: 3,
        items: 20,
        updater: function(item) {
            var temp;
            if (arr2.length < 1) {
                temp = item.split(" ");
                arr2.push(temp[0]);
                $scope.filtroEscolhido.ed = arr2;
                console.log($scope.filtroEscolhido.ed);
                modificaAlertaEDs(arr2[0]);
                return '';
            } else {
                temp = item.split(" ");
                if (verificaRepetidoEDs(arr2, temp[0])) {
                    modificaAlertaEDs("Elemento repetido ou número máximo de EDs acumulados");
                    setTimeout(function() {
                        if (arr2.length == 3) {
                            modificaAlertaEDs(arr2[0] + ", " + arr2[1] + ", " + arr2[2]);
                        }
                        if (arr2.length == 2) {
                            modificaAlertaEDs(arr2[0] + ", " + arr2[1]);
                        }
                        if (arr2.length == 1) {
                            modificaAlertaEDs(arr2[0]);
                        }
                    }, 3000);
                } else {
                    temp = item.split(" ");
                    arr2.push(temp[0]);
                    //$myTextarea.css("color","white");
                    if (arr2.length == 3) {
                        modificaAlertaEDs(arr2[0] + ", " + arr2[1] + ", " + arr2[2]);
                    }
                    if (arr2.length == 2) {
                        modificaAlertaEDs(arr2[0] + ", " + arr2[1]);
                    }
                    if (arr2.length == 1) {
                        modificaAlertaEDs(arr2[0]);
                    }
                    $scope.filtroEscolhido.ed = arr2;
                    console.log($scope.filtroEscolhido.ed);
                }
                return '';
            }
            $scope.filtroEscolhido.ed = arr2;
            console.log($scope.filtroEscolhido.ed);
        }
    });
    //} Fim EDs Input Controle

    //{ ICs Input Controle
    var modificaAlertaICs = function(x) {
        if (x == "Elemento repetido ou número máximo de ICs acumulados") {
            $('.notification .ng-binding').text(x).selectpicker('refresh');
            setTimeout(function() {
                $('.notification .ng-binding').text("").selectpicker('refresh');
            }, 3000);
        } else {
            $("#filtro-ic").attr("placeholder", x);
        }

    };
    var verificaRepetidoICs = function(x, item) {
        console.log(x);
        if (x.length == 3) return true;
        for (var z in x) {
            try {
                if (x[z].localeCompare(item) == 0) return true;
            } catch (err) {
                console.log(err);
            }
            try {
                if (x[z + 1].localeCompare(item) == 0) return true;
            } catch (err) {
                console.log(err);
            }
        }
        return false;
    };

    var vetorFiltroIc = [];


    function search(selector) {
        var resultElement = $('.search_results');
        var searchElement = $(".searchtool_text");
        clearSearchTool("", resultElement);


        if (selector == "" || !selector) {
            if ($("div.log-formatado").css('display') == 'block') {
                selector = "div.log-formatado";             
            }
            if ($("div.log-original").css('display') == 'block') {
                selector = "div.log-original";
            }
            if ($("div.log-prerouting").css('display') == 'block') {
                selector = "div.log-prerouting";
            }
            if ($("div.log-preroutingret").css('display') == 'block') {
                selector = "div.log-preroutingret";
      
            }
        }

        if (searchElement.val() != "" && searchElement.val() != undefined && searchElement.val() != null) {
            try {
                $(selector).highlight(searchElement.val(), {
                    className: 'match'
                });
                $('.match:first').addClass('highlighted');
                var matches = $(".match");
                resultElement.text(matches.length + ' encontrados');

            } catch (err) {
                console.log("Error: " + err);
            }
        }
    }


    //} Fim ICs Input Controle

    // { Função timeoutsearch
    /// Paramêtro DIV é o elemento jQuery a ser inserido (ex: .log-formatado)
    function waitForSearch(div) {
        if ($(div).length && $(div).is(":visible")) {
            setTimeout(function() {
                //console.log("Preparando Log Original...");
                if ($('div.modal-header h3').text() == "Carregando...") {
                    waitForSearch(div);
                } else {
                    search(div);
                }
            }, 1000);
        } else {
            //console.log("Div requisitada: "+div+" não existe ou está invisível.");
        }
    }
    //} fim da timeoutsearch

    $scope.$watch('callLog', function(newValue, oldValue) {
        if (newValue) {
            //console.log("NewValue: "+newValue.length);
        }
        waitForSearch(".log-original");
        //console.log("Realizou a pesquisa devido ao CallLog :"+$(".searchtool_text").val());
    });



    $scope.$watch('logxml', function(newValue, oldValue) {
        if (newValue) {
            //console.log("NewValue: "+newValue.length);
        }

        //console.log("Realizou a pesquisa devido ao CallLog :"+$(".searchtool_text").val());
        waitForSearch(".log-formatado");
    });


    $scope.$watch('ocultar.log_formatado', function(newValue, oldValue) {
        //console.log("Ocultar LOG FORMATADO?: "+$scope.ocultar.log_formatado);
        //console.log("Old Value: "+oldValue+" new value: "+newValue);
        if (newValue == false) {
            search(".log-formatado");
            //console.log("Realizou a pesquisa em Log Formatado!");
        }
    });

    $scope.$watch('ocultar.log_original', function(newValue, oldValue) {
        //console.log("Ocultar LOG ORIGINAL?: "+$scope.ocultar.log_original);
        //console.log("Old Value: "+oldValue+" new value: "+newValue);
        if (newValue == false) {
            search(".log-original");
            //console.log("Realizou a pesquisa em LOG ORIGINAL por :"+$(".searchtool_text").val());
        }
    });

    $scope.$watch('ocultar.log_prerouting', function(newValue, oldValue) {
        //console.log("Ocultar LOG prerouting?: "+$scope.ocultar.log_prerouting);
        //console.log("Old Value: "+oldValue+" new value: "+newValue);
        waitForSearch(".log-prerouting");
    });


    $scope.$watch('ocultar.log_preroutingret', function(newValue, oldValue) {
        //console.log("Ocultar LOG preroutingret?: "+$scope.ocultar.log_preroutingret);
        //console.log("Old Value: "+oldValue+" new value: "+newValue);
        waitForSearch(".log-preroutingret");
    });

    $scope.$watch('ocultar.log_prompts', function(newValue, oldValue) {
        //console.log("Ocultar LOG preroutingret?: "+$scope.ocultar.log_preroutingret);
        //console.log("Old Value: "+oldValue+" new value: "+newValue);
        waitForSearch(".log_prompts");
    });
 


    $scope.abreAjuda = function(link) {
        abreAjuda(link);
    }

    $scope.addItem = function(data) {
        console.log("AddITEM codigo: " + data.codigo + " Desc: " + data.desc);
        iccode = data.codigo;
        if ($scope.outputICS.indexOf(iccode) == -1) {
            console.log("Botao adicionado para: " + iccode);

            icbutton = '<button type="button" ' +
                'class="helperButton ng-binding ng-scope ' + iccode + '" ' +
                'ng-click="removeItem(\'' + iccode + '\');" ' +
                ' >×&nbsp;&nbsp; ' + iccode + '</button>';
            icelem = $(icbutton);
            targetElement = $(".helperButton").parent();

            angular.element(targetElement).injector().invoke(function($compile) {
                var escope = angular.element(targetElement).scope();
                targetElement.append($compile(icelem)($scope));
                // Finally, refresh the watch expressions in the new element
                $scope.$apply();
            });

            function logArrayElements(element, index, array) {
                console.log("a[" + index + "] = Codigo: " + element.codigo + " Desc: " + element.desc);
            }
            $scope.outputICS.forEach(logArrayElements);

        } else {
            $("." + iccode + "").remove();
            console.log("Tentou remover: ." + iccode);
            //$scope.outputICS.remove(data.codigo);
        }
        console.log("Output ICS: " + $scope.outputICS);
        console.log("Output ICS Length: " + $scope.outputICS.length);
    }

    $scope.removeItem = function(codigo) {
        if ($scope.outputICS.indexOf(codigo) == -1 && $("." + codigo).length <= 0) {
            function logArrayElements(element, index, array) {
            }
            $scope.outputICS.forEach(logArrayElements);

        } else {

            function removerIC(value) {
                return value.codigo == codigo;
            }

            $scope.outputICS = $scope.outputICS.filter(removerIC);

            $timeout(function() {
                angular.element($(".select" + codigo).parent().parent().parent().parent()).triggerHandler('click');
            }, 0);
        }
    }
    $scope.windowInnerWidth = window.innerWidth;
    window.addEventListener('resize', function() {
        $scope.windowInnerWidth = window.innerWidth;
        $scope.$apply();

    }, true);

    $("input").blur(function() {
        $(this).val(escapeSqlChars($(this).val()));

    });

    $("#exportarLog").click(function() {
        exportaTXT($scope.callLog, tempDir3() + "export_" + Date.now(), "txt", true);
    })

}
CtrlChamadas.$inject = ['$scope', '$globals', '$compile', '$timeout'];