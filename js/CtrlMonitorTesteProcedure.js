function CtrlMonitorTesteProcedure($scope, $globals) {

    win.title = "Teste de Procedure";
    $scope.versao = versao;
    $scope.rotas = rotas;

    $scope.localeGridText = {
            contains: 'Contém',
            notContains: 'Não contém',
            equals: 'Igual',
            notEquals: 'Diferente',
            startsWith: 'Começa com',
            endsWith: 'Termina com'
    }

    var $view;

    $scope.gridProcedures = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
            columnDefs: [
                    { headerName: 'Nome', field: 'nome' },
                    { headerName: 'Atualizado em', field: 'atualizacao' }
            ],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true,
            onRowSelected: function(event) {
                    if (event.node.selected) {
                            limpaProgressBar($scope, "#pag-monitor-teste-procedure");
                            
                            $scope.gridParametros.rowData = [];
                            $scope.gridParametros.columnDefs = [];

                            $scope.gridResultado.rowData = [];
                            $scope.gridResultado.columnDefs = [];
                            
                            $scope.listarParametros.apply(this, [event.node.data.nome]);
                            $scope.prepararResultado.apply(this, [event.node.data.nome]);
                    }
            }
    };

    $scope.gridParametros = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado</span>',
            columnDefs: [
                    { headerName: 'Parâmetro', field: 'parametro' },
                    { headerName: 'Valor', field: 'valor', editable: true },
            ],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true,
            stopEditingWhenGridLosesFocus: true,
            singleClickEdit: true
    }

    $scope.gridResultado = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado</span>',
            columnDefs: [],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true
    }

    $scope.dados = [];
    $scope.csv = [];
    $scope.difPorAplicacao = [];
    $scope.difAplicacoes = [];

    $scope.$on('$viewContentLoaded', function () {
            $view = $("#pag-monitor-teste-procedure");

            // limpaProgressBar($scope, "#pag-monitor-teste-procedure");
            $scope.gridProcedures.rowData = [];
            $scope.listarProcedure.apply(this);

            /*

            $view.on("click", ".btn-gerar-teradata", function() {
                    limpaProgressBar($scope, "#pag-monitor-teste-procedure");
                    $scope.gridProcedures.rowData = [];
                    $scope.listarProcedure.apply(this)

            });

            $view.on("click", ".btn-gerar-mailing", function() {
                    limpaProgressBar($scope, "#pag-monitor-teste-procedure");
                    $scope.gridParametros.rowData = [];
                    $scope.listarParametros.apply(this)

            }); */

            $view.on("click", ".btn-exportar", function() {
                    $scope.exportaXLSX.apply(this);
            });

            // Botão abortar Alex 23/05/2014
            $view.on("click", ".abortar", function() {
                    $scope.abortar.apply(this);
            });

            //Alex 23/05/2014
            $scope.abortar = function() {
                    abortar($scope, "#pag-monitor-teste-procedure");
            }

            $view.on("click", ".btn-gerar", function () {
                $scope.listarResultados.apply(this);
            });
    });

    $scope.listarProcedure = function() {
        var stmt = db.use;

        stmt += ' SELECT';
        stmt += '     ID, CONVERT(VARCHAR, DataIni, 103) + \' \' + CONVERT(VARCHAR, DataIni, 8) AS DataIni';
        stmt += ' FROM';
        stmt += '     ControleExtratores';
        stmt += ' WHERE';
        stmt += '     ID IN(\'TNLImportaTbMailingFibra\', \'TNLImportaTbMailingFibraMigracao\')';
       
            db.query(stmt, function (columns) {
                console.log(stmt)
                var name;

                switch (columns[0].value) {
                        case 'TNLImportaTBMAILINGFIBRA':
                                name = 'uspMailingFibra';

                                break;
                        case 'TNLImportaTBMAILINGFIBRATEST':
                                name = 'uspMailingFibraTest';

                                break;
                        case 'TNLImportaTBMAILINGFIBRAMIGRACAO':
                                name = 'uspMailingFibraMigracao';

                                break;
                }

                $scope.gridProcedures.rowData.push(
                        {
                                nome: name,
                                atualizacao: columns[1].value
                        }
                )

                    /*

                    var codAplicacao = columns[0].value,
                    dataHora = columns[1].value,
                    qtdChamadas = columns[2].value,
                    qtdChamadas2 = columns[3].value,
                    diferenca = columns[4].value,
                    diferencaPercent = columns[5].value

            $scope.dados.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
            });

            $scope.gridResultado.rowData.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
            });

            $scope.csv.push([
                    codAplicacao,
                    formataDataHoraBR(dataHora),
                    qtdChamadas,
                    qtdChamadas2,
                    diferenca,
                    diferencaPercent
            ]);

            if ($scope.difPorAplicacao[codAplicacao]) {
                    $scope.difPorAplicacao[codAplicacao] += formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = true;
            } else {
                    $scope.difPorAplicacao[codAplicacao] = formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = false;
            }
                    */
            }, function (err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar diferenças: ' + err);
                    } else {
                        $scope.gridProcedures.api.setRowData($scope.gridProcedures.rowData);
                        $scope.gridProcedures.api.refreshCells({force: true}); 

                        $scope.gridProcedures.api.hideOverlay();
                        
                        retornaStatusQuery($scope.gridProcedures.rowData.length, $scope);

                        $scope.gridProcedures.api.setRowData($scope.gridProcedures.rowData);
                        $scope.gridProcedures.api.refreshCells({force: true});
                    }

            });

            /*

        $scope.gridProcedures.rowData.push({
                nome: 'uspMailingFibra',
                atualizacao: ''
        },
        {
                nome: 'uspMailingFibraMigracao',
                atualizacao: ''
        });

        $scope.gridProcedures.api.setRowData($scope.gridProcedures.rowData);
        $scope.gridProcedures.api.refreshCells({force: true}); */
    }

    $scope.listarParametros = function(procedure) {
        $scope.gridParametros.suppressHorizontalScroll = false;

        switch(procedure) {
                case 'uspMailingFibra': {
                        $scope.gridParametros.rowData.push(
                                {
                                        parametro: '@bundleId',
                                        valor: ''
                                },
                                {
                                        parametro: '@documento',
                                        valor: ''
                                },
                                {
                                        parametro: '@fixo',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelTit',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelDep',
                                        valor: ''
                                },
                                {
                                        parametro: '@telefoneContato',
                                        valor: ''
                                }
                        );

                        break;
                }
                case 'uspMailingFibraTest': {
                        $scope.gridParametros.rowData.push(
                                {
                                        parametro: '@bundleId',
                                        valor: ''
                                },
                                {
                                        parametro: '@documento',
                                        valor: ''
                                },
                                {
                                        parametro: '@fixo',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelTit',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelDep',
                                        valor: ''
                                },
                                {
                                        parametro: '@telefoneContato',
                                        valor: ''
                                }
                        );

                        break;
                }
                case 'uspMailingFibraMigracao': {
                        $scope.gridParametros.rowData.push(
                                {
                                        parametro: '@bundleId',
                                        valor: ''
                                },
                                {
                                        parametro: '@documento',
                                        valor: ''
                                },
                                {
                                        parametro: '@fixo',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelTit',
                                        valor: ''
                                },
                                {
                                        parametro: '@movelDep',
                                        valor: ''
                                },
                                {
                                        parametro: '@telefoneContato',
                                        valor: ''
                                }
                        );

                        break;
                }
        }

        $scope.gridParametros.api.setRowData($scope.gridParametros.rowData);
        $scope.gridParametros.api.refreshCells({force: true});
    }

    $scope.prepararResultado = function (procedure) {
        $scope.gridResultado.suppressHorizontalScroll = false;

        switch(procedure) {
                case 'uspMailingFibra': {
                        $scope.gridResultado.columnDefs.push(
                                { headerName: 'NUMERO_DOCUMENTO', field: 'NUMERO_DOCUMENTO' },
                                { headerName: 'ACESSO_GPON', field: 'ACESSO_GPON' },
                                { headerName: 'IND_COMBO', field: 'IND_COMBO' },
                                { headerName: 'CLASSE_PRODUTO', field: 'CLASSE_PRODUTO' },
                                { headerName: 'ID_BUNDLE', field: 'ID_BUNDLE' },
                                { headerName: 'FIXO', field: 'FIXO' },
                                { headerName: 'DATA_INSTALACAO_PRODUTO', field: 'DATA_INSTALACAO_PRODUTO' },
                                { headerName: 'TELEFONE_CONTATO', field: 'TELEFONE_CONTATO' },
                                { headerName: 'MOVEL_TIT', field: 'MOVEL_TIT' },
                                { headerName: 'MOVEL_DEP', field: 'MOVEL_DEP' },
                                { headerName: 'AGING', field: 'AGING' }
                        );

                        break;
                }
                case 'uspMailingFibraTest': {
                        $scope.gridResultado.columnDefs.push(
                                { headerName: 'NUMERO_DOCUMENTO', field: 'NUMERO_DOCUMENTO' },
                                { headerName: 'ACESSO_GPON', field: 'ACESSO_GPON' },
                                { headerName: 'IND_COMBO', field: 'IND_COMBO' },
                                { headerName: 'CLASSE_PRODUTO', field: 'CLASSE_PRODUTO' },
                                { headerName: 'ID_BUNDLE', field: 'ID_BUNDLE' },
                                { headerName: 'FIXO', field: 'FIXO' },
                                { headerName: 'DATA_INSTALACAO_PRODUTO', field: 'DATA_INSTALACAO_PRODUTO' },
                                { headerName: 'TELEFONE_CONTATO', field: 'TELEFONE_CONTATO' },
                                { headerName: 'MOVEL_TIT', field: 'MOVEL_TIT' },
                                { headerName: 'MOVEL_DEP', field: 'MOVEL_DEP' },
                                { headerName: 'AGING', field: 'AGING' }
                        );

                        break;
                }
                case 'uspMailingFibraMigracao': {
                        $scope.gridResultado.columnDefs.push(
                                { headerName: 'NUMERO_DOCUMENTO', field: 'NUMERO_DOCUMENTO' },
                                { headerName: 'ACESSO_GPON', field: 'ACESSO_GPON' },
                                { headerName: 'IND_COMBO', field: 'IND_COMBO' },
                                { headerName: 'CLASSE_PRODUTO', field: 'CLASSE_PRODUTO' },
                                { headerName: 'ID_BUNDLE', field: 'ID_BUNDLE' },
                                { headerName: 'FIXO', field: 'FIXO' },
                                { headerName: 'DATA_INSTALACAO_PRODUTO', field: 'DATA_INSTALACAO_PRODUTO' },
                                { headerName: 'TELEFONE_CONTATO', field: 'TELEFONE_CONTATO' },
                                { headerName: 'MOVEL_TIT', field: 'MOVEL_TIT' },
                                { headerName: 'MOVEL_DEP', field: 'MOVEL_DEP' },
                                { headerName: 'STATUS', field: 'STATUS' },
                                { headerName: 'AGING', field: 'AGING' }
                        );

                        break;
                }
        }

        $scope.gridResultado.api.setColumnDefs($scope.gridResultado.columnDefs);
        $scope.gridResultado.api.setRowData($scope.gridResultado.rowData);
        $scope.gridResultado.api.refreshCells({force: true});
    }

    $scope.listarResultados = function () {
        var stmt = db2.use;

        if (!$scope.gridProcedures.api.getSelectedRows()) {
                alert('Ao menos uma procedure deve ser escolhida');   
        }

        var selectedProcedure = $scope.gridProcedures.api.getSelectedRows()[0].nome;

        stmt += 'EXECUTE [' + selectedProcedure + ']';

        stmt += $scope.gridParametros.rowData.reduce(function (accumulator, value, index) {
                if (value.valor) {
                        accumulator += ' ' + value.parametro + '=\'' + value.valor + '\'';
                }

                return accumulator;
        }, '');

            $scope.gridResultado.rowData = [];
            $scope.gridResultado.api.setRowData([]);

            var $btn_exportar = $view.find(".btn-exportar");
            
            $btn_exportar.prop("disabled", true);

            var $btn_gerar = $(this);

            $btn_gerar.button("loading");
			
                        log(stmt);

            dateinicial = new Date()
            db2.query(stmt, function (columns) {
                $scope.gridResultado.rowData.push(
                        columns.reduce(function(accumulator, row, index) {
                                accumulator[row.metadata.colName] = row.value;

                                return accumulator;
                        }, [])
                )

                console.log($scope.gridResultado.rowData);

                    /*

                    var codAplicacao = columns[0].value,
                    dataHora = columns[1].value,
                    qtdChamadas = columns[2].value,
                    qtdChamadas2 = columns[3].value,
                    diferenca = columns[4].value,
                    diferencaPercent = columns[5].value

            $scope.dados.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
            });

            $scope.gridResultado.rowData.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
            });

            $scope.csv.push([
                    codAplicacao,
                    formataDataHoraBR(dataHora),
                    qtdChamadas,
                    qtdChamadas2,
                    diferenca,
                    diferencaPercent
            ]);

            if ($scope.difPorAplicacao[codAplicacao]) {
                    $scope.difPorAplicacao[codAplicacao] += formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = true;
            } else {
                    $scope.difPorAplicacao[codAplicacao] = formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = false;
            }
                    */
            }, function (err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar diferenças: ' + err);

                            $btn_gerar.button("reset");

                            if ($scope.dados.length > 0) {
                                    $btn_exportar.prop("disabled", false);
                            }
                    } else {

                            $scope.gridResultado.api.hideOverlay();
                            retornaStatusQuery($scope.gridResultado.rowData.length, $scope);

                            $scope.gridResultado.api.setRowData($scope.gridResultado.rowData);
                            $scope.gridResultado.api.refreshCells({force: true});

                            for (var i in $scope.difAplicacoes) {
                                    console.log('Iterando para ' + i);
                                    if ($scope.difAplicacoes[i]) console.log(i + ' encontrado');
                            }
                            $btn_gerar.button("reset");
                            if ($scope.dados.length > 0) {
                                    $btn_exportar.prop("disabled", false);
                            }
                            var datefim = new Date();
                            var diffMilissegundos = datefim - dateinicial;
                            texto = 'Tempo de resposta procedure: '+ diffMilissegundos+' ms'
                            document.getElementById("texto").innerHTML = texto;
                            console.log(diffMilissegundos)
                    }

            });

    }

    function reverteDataBR(dataBR) {
            return dataBR.replace(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}:\d{2})/, '$3-$2-$1 $4');
    }

    function formataHoraZero(dataHora) {
            return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 00:00:00');
    }

    function formataHoraCheia(dataHora) {
            return dataHora.replace(/(\d{4}-\d{2}-\d{2} \d{2}).+/g, '$1:00:00');
    }
}

CtrlMonitorTesteProcedure.$inject = ['$scope', '$globals'];
