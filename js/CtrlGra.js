function CtrlGra($scope, $globals) {

    $scope.regraParametroUF = false;

    //Alex 06/02/2014 - Para permissões especiais
    $scope.login = loginUsuario;
    $scope.dominio = dominioUsuario;
    $scope.historico = false;
    $scope.anormal = false;

    win.title = "Ferramenta de Alteração de Mensagens Contingenciais de Chuva e Energia Elétrica"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 18/07/2017
    $scope.editarGra = false;
    $scope.historicoGra = false;
    var p = cache.aclbotoes.map(function (b) { return b.idview; }).indexOf('pag-gra');
    if (p >= 0) {
        var teste = cache.aclbotoes[p].permissoes.split(',');
        for (var i = 0; i < teste.length; i++) {
            if (teste[i] === 'historico') {
                $scope.historicoGra = true;
                //console.log("Libera histórico GRA");
            } else if (teste[i] === 'editar') {
                $scope.editarGra = true;
                //console.log("Libera editar GRA");
            }
        }
    }

    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-gra", "Selecione ao menos um DDD para habilitar uma nova ocorrência.");
    //Filtros

    $scope.ddds = cache.ddds;
    $scope.ufs = cache.ufs;
    $scope.gras = [];
    $scope.parametros = [];
    $scope.gra = {};
    $scope.decrescente = false;
    $scope.ordenacao = ["codgra"];
    var prazo_param;
    var tmr;


    $scope.colunas = [];
    $scope.gridDados = {
        data: "gras",
        rowTemplate: '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="{expirado: row.getProperty(\'expirado\')  == true }" class="ngCell {{col.cellClass}} {{col.colIndex()}}" ng-cell></div>',
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };


    $scope.mostraHistorico = false;


    $scope.preencheGras;

    $scope.mudaDiasParamNormal = Function;
    $scope.listar;
    var param = [];


    function geraColunas() {
        var array = [];

        array.push({ displayName: "GRA", field: "codgra", width: 100, pinned: false },
            { displayName: "Data inicial", field: "dataini", width: 160, pinned: false }
        );
        if ($scope.mostraHistorico === false) {
            array.push({ displayName: "Data final", field: "datafim", width: 100, pinned: false });
        } else {
            array.push({ displayName: "Login", field: "login", width: 100, pinned: false });
        }
        array.push({ displayName: "UF", field: "uf", width: 100, pinned: false },
            { displayName: "DDD", field: "ddd", width: 100, pinned: false },
            { displayName: "Parâmetro", field: "parametro", width: 150, pinned: false },
            //{ displayName: "Ação", field: "acao", width: 100, pinned: false },
            { displayName: "Prazo", field: "prazo", width: 100, pinned: false },
            { displayName: "UFs greve", field: "ufs", width: 100, pinned: false },
            { displayName: "Obs", field: "obs", width: 590, pinned: false }
        );
        return array;
    }


    // Filtros: data e hora
    var agora = new Date();
    var inicio, fim;
    if (Estalo.filtros.filtro_data_hora[0] !== undefined) {
        inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]);

    } else {
        inicio = hoje(); Estalo.filtros.filtro_data_hora[0];
    }
    if (Estalo.filtros.filtro_data_hora[1] !== undefined) {
        fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]);
    } else {
        fim = amanha();
    }

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
          max: dat_consoli*/
    };

    $scope.agenda = new Date(2015, 12, 2, 0, 0, 0);

    Estalo.filtros.filtro_data_hora[0] = $scope.periodo.inicio;
    Estalo.filtros.filtro_data_hora[1] = $scope.periodo.fim;


    //var historicoGrasHeader= "CODGRA DATAHORA LOGIN PROBLEMA DIAS ACAO OBS";
    //Filtros > CodGra, Problema (Descrição) , quantidade de dias, ação, Obs)

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-gra");
  
        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        $('span.btnSearch') && $('span.btnSearch').hide();

        //$('#dataini').css('visibility', 'hidden');

        //19/03/2014
        componenteDataMaisHora($scope, $view);
        componenteHora($scope, $view);


        carregaUFs($view,true);
        carregaUFsModal($view);
        carregaParametros($view,true);

        $('div.modal.fade') && $('div.modal.fade').hide();

        $scope.preencheGras = function () {
            var $sel_gras = $view.find("#selectGra");
            var $sel_gras_Modal = $view.find("#selectGraModal");


            var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
            var filtro_ufs = $view.find("select.filtro-uf").val() || [];

            if (filtro_ddds.length === 0) {
                $sel_gras
                    .html("")
                    .prop("disabled", true)
                    .selectpicker('refresh');
                $('#alinkGRA').attr('disabled', 'disabled');  //Bernardo 20-02-2014
                return;
            }


            var filtro_gras = {};
            ($sel_gras.val() || []).forEach(function (codigo) {
                filtro_gras[codigo] = true;
            });

            var options = [];
            var v = [];
            filtro_ddds.forEach(function (codigo) { v = v.concat(unique2(cache.gras.map(function (gra) { if (gra.ddd === codigo) { return gra.codigo } })) || []).sort(); });
            unique(v).forEach((function (filtro_gras) {
                return function (codigo) {
                    var gra = cache.gras.indice[codigo];
                    var selec = filtro_gras[codigo] ? ' selected=""' : '';
                    //var selec =  ' selected=""';// Gilberto 05/06
                    options.push('<option value="' + gra.codigo + '"' + selec + '>' + gra.nome + '</option>');
                };
            })(filtro_gras));

            //filtro_ufs.forEach(function (codigo) { v = v.concat(unique2(cache.gras.map(function(gra){ if(gra.uf === codigo){ return gra.codigo } })) || []).sort(); });
            //unique(v).forEach((function (filtro_gras) {
            //	return function (codigo) {
            //		var gra = cache.gras.indice[codigo];
            //		var selec = filtro_gras[codigo] ? ' selected=""' : '';
            //		//var selec =  ' selected=""';// Gilberto 05/06
            //		options.push('<option value="' + gra.codigo + '"' + selec + '>' + gra.nome + '</option>');
            //	};
            //})(filtro_gras));

            $sel_gras.html(options.join());
            $sel_gras_Modal.html(options.join());

            if (options.length > 0) {
                $sel_gras
                    .prop("disabled", false)
                    .selectpicker('refresh');
                $('#alinkGRA').removeAttr('disabled'); //Bernardo 20-02-2014
            }

            $sel_gras_Modal
                .prop("disabled", false)
                .selectpicker('refresh');

            var $btn = $view.find(".btn-cadastro");

            if (filtro_ddds.length === 0) {
                $btn.attr("disabled", true);
            } else {
                $btn.attr("disabled", false);
            }

            var $btn = $view.find(".btn-atualizarGRAS");
            $btn.prop("disabled", true);

        };// Fim de PreencheGras


        carregaDDDsPorUF($scope, $view, true);

        // Popula lista de  ddds a partir das UFs selecionadas
        $view.on("change", "select.filtro-uf", function () { carregaDDDsPorUF($scope, $view) });

        // Popula lista de  gras a partir dos ddds selecionados
        $view.on("change", "select.filtro-ddd", $scope.preencheGras);

        $view.on("change", "#selectGra", function () {
            // desabilitar o botão se alguma alteração for feita.
            var $btn = $view.find(".btn-atualizarGRAS");
            $btn.prop("disabled", true);
        });

        $view.on("change", "#selectParam", function () {
            // desabilitar o botão se alguma alteração for feita.
            var $btn = $view.find(".btn-atualizarGRAS");
            $btn.prop("disabled", true);

        });

        $view.on("change", "#selectParamModal", function () {
            console.log($('#selectParamModal').val());
            var parametro = $('#selectParamModal').val();
            var obj = cache.parametros.filter(function (par) { if (par.codigo == parametro) { return par; } });
            var prazo = obj[0].prazo;
            definirPrazo(prazo);
            var frase = obj[0].frase1;
            $('#txtFrase').val(frase);

            //25/07/2017 Alex
            if (parametro === 'generico84' || parametro === 'generico85') {
                $scope.regraParametroUF = true;
                $('.ufmodal').removeClass('ng-hide');
            } else {
                $scope.regraParametroUF = false;
                $('.ufmodal').addClass('ng-hide');
            }
        });

        $view.on("change", "#txtDias", function () {

            var parametro = $('#selectParamModal').val();
            var obj = cache.parametros.filter(function (par) { if (par.codigo == parametro) { return par; } });
            if ($('#txtDias').val() !== "0") {
                var frase = obj[0].frase2.toString().replace("XX", $('#txtDias').val());
            } else {
                var frase = obj[0].frase1;
            }

            $('#txtFrase').val(frase);

        });

        $view.on("change", "#selHoras", function () {
            console.log($('#selHoras').val());
            var parametro = $('#selectParamModal').val();
            var obj = cache.parametros.filter(function (par) { if (par.codigo == parametro) { return par; } });
            if ($('#selHoras').val() !== "0") {
                var frase = obj[0].frase2.toString().replace("XX", $('#selHoras').val());
            } else {
                var frase = obj[0].frase1;
            }

            $('#txtFrase').val(frase);

        });

        $view.on("change", "#selMin", function () {
            console.log($('#selMin').val());
            var parametro = $('#selectParamModal').val();
            var obj = cache.parametros.filter(function (par) { if (par.codigo == parametro) { return par; } });
            if ($('#selMin').val() !== "0") {
                var frase = obj[0].frase2.toString().replace("XX", $('#selMin').val());
            } else {
                var frase = obj[0].frase1;
            }

            $('#txtFrase').val(frase);

        });


        $("#myModal").mousemove(function (event) {
            if ($('#agenda').prop('disabled') == false) {
                var parametro = $('#selectParamModal').val();
                var obj = cache.parametros.filter(function (par) { if (par.codigo == parametro) { return par; } });
                if ($('#agenda').val() !== "00:00:00") {
                    var frase = obj[0].frase2.toString().replace("XX", $('#agenda').val());
                } else {
                    var frase = obj[0].frase1;
                }

                $('#txtFrase').val(frase);
            }
        });






        $scope.listar = function () {

            try {
                if ($scope.mostraHistorico) {
                    $scope.listarHistoricoGra();
                    $scope.historico = true;
                } else {
                    $scope.listarGrasAtivos();
                    $scope.historico = false;
                }
            } catch (e) {
                $btn_gerar.button('reset');
                console.log('erro ao tentar listar os gras');
            }
        };

        var filtro_gra = {};


        //Popula lista de UFs
        var options = [];

        $scope.ufs.forEach(function (uf) {
            options.push('<option value="' + uf.codigo + '">' + uf.nome + '</option>');
        });

        $view.find("select.filtro-uf").html(options.join());


        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-uf").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} UFs',
            showSubtext: true
        });

        $view.find("select.filtro-uf-Modal").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} UFs',
            showSubtext: true
        });

        $view.find("select.filtro-gras").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} GRAs',
            showSubtext: true
        });

        $view.find("select.filtro-param").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Parâmetros',
            showSubtext: true
        });

        $view.find("select.filtro-parametros").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Parâmetros',
            showSubtext: true
        });

        $view.on('change', "#checkHistorico", function () {
            var $btn = $view.find(".btn-atualizarGRAS");
            $btn.prop("disabled", true);
        });


        //Bernardo 20-02-2014 Marcar todas as ufs
        $view.on("click", "#alinkUf", function () { marcaTodasUFs($('.filtro-uf'), $view, $scope) });



        //Bernardo 20-02-2014 Marcar todos os DDDs
        $view.on("click", "#alinkDDD", function () {

            //Desmarcando todas
            var valor = $('#alinkDDD').attr("data-var");
            var $sel_gras = $view.find("select.filtro-gras");
            var $sel_ddds = $view.find("select.filtro-ddd");

            if (valor === "1") {
                $('.filtro-gras').selectpicker('deselectAll');
                $('#alinkDDD').attr("data-var", "0");
                $('#alinkGRA').attr('disabled', 'disabled');
                $sel_gras
                    .html("")
                    .prop("disabled", true)
                    .selectpicker('refresh');

                $('.filtro-ddd').selectpicker('deselectAll');
                $('#alinkDDD').attr("data-var", "0");

                var $btn = $view.find(".btn-cadastro");
                $btn.prop("disabled", true);



            } else {

                var $btn = $view.find(".btn-cadastro");
                $btn.prop("disabled", false);


                //Marcando todas
                $('.filtro-ddd').selectpicker('selectAll');
                $('#alinkDDD').attr("data-var", "1");
                $('#alinkGRA').removeAttr('disabled');


                //Preenchendo lista com todos os gras
                var options = [];
                cache.gras.forEach(function (gra) {
                    options.push('<option value="' + gra.codigo + '">' + gra.nome + '</option>');
                });
                $sel_gras.html(options.join());

                $sel_gras
                    //.html("")
                    .prop("disabled", false)
                    .selectpicker('refresh');
            }
        });


        //Marcar todos os GRAs
        $view.on("click", "#alinkGRA", function () {
            var valor = $('#alinkGRA').attr("data-var");
            if (valor === "1") {
                $('.filtro-gras').selectpicker('deselectAll');
                $('#alinkGRA').attr("data-var", "0");
            } else {
                $('.filtro-gras').selectpicker('selectAll');
                $('#alinkGRA').attr("data-var", "1");
            }
        });

        //Marcar todos os parâmetros
        $view.on("click", "#alinkParam", function () {
            var valor = $('#alinkParam').attr("data-var");
            if (valor === "1") {
                $('.filtro-parametros').selectpicker('deselectAll');
                $('#alinkParam').attr("data-var", "0");
            } else {
                $('.filtro-parametros').selectpicker('selectAll');
                $('#alinkParam').attr("data-var", "1");
            }
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-uf').mouseover(function () {
            $('div.btn-group.filtro-uf').addClass('open');
            $('div.btn-group.filtro-uf>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-uf').mouseout(function () {
            $('div.btn-group.filtro-uf').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-gras').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-gras .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-gras').addClass('open');
                $('div.btn-group.filtro-gras>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-gras').mouseout(function () {
            $('div.btn-group.filtro-gras').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-parametros').mouseover(function () {
            $('div.btn-group.filtro-parametros').addClass('open');
            $('div.btn-group.filtro-parametros>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-parametros').mouseout(function () {
            $('div.btn-group.filtro-parametros').removeClass('open');
        });



        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-gra");
            $scope.colunas = geraColunas();
            $scope.listar.apply(this);
        });

        $view.on("click", ".btn-primary", function () {
            //limpaProgressBar($scope, "#pag-gra");
            $('#modalConfirma').modal('toggle');
            $('#myModal').modal('toggle');

            //$scope.salvar.apply(this);
        });

        $view.on("click", ".btn-atualizarGRAS", function () {
            $scope.atualizarGRAS.apply(this);

        });//unobtrusive


        $view.on("change", "#reparoProgramado", function () {

            //        	($scope.$apply(function(){
            //        		if($scope.reparoProgramado){
            //            		//$('#dataini').css('visibility','hidden');
            //            	}else{
            //            		//$('#dataini').css('visibility','visible');
            //            	}
            //        	}))();
        });

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        $view.on("click", ".btn-limpar-filtros", function () {

            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            $('select.filtro-gras').val("").selectpicker('refresh');
            $('#selectGra').selectpicker('deselectAll').attr("disabled", false);
            carregaDDDsPorUF($scope, $view);

            $('select.filtro-parametros').val("").selectpicker('refresh');
            $('.filtro-codGra').val("");
            iniciaFiltros();
            //componenteDataHora($scope, $view, "gra");



            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //$scope.filtros.isHFiltroED = false;
                //$scope.filtros.isHFiltroIC = false;
                $scope.$apply();
            }, 500);
        }



        //Alex 23/05/2014
        $scope.abortar = function () {
            //estava ERRADO.
            abortar($scope, "#pag-gra");
        }


        function definirPrazo(valor) {
            switch (valor) {
                case "N":
                    //$scope.anormal = false;
                    $('#txtDias').val(0);
                    $('#agenda').val("00:00:00");
                    $('#selMin').val(0);
                    $('#selHoras').val(0);
                    //txtDias.prop('disabled', 'disabled');
                    $('#txtDataFim').prop('disabled', true);
                    $('#spDataFim').css("pointer-events", "none");
                    $('#txtDias').prop("disabled", true);
                    $('#agenda').prop('disabled', true);
                    $('#selMin').prop('disabled', true);
                    $('#selHoras').prop('disabled', true);
                    $('#spAgenda').css("pointer-events", "none");
                    prazo_param = valor;
                    break;

                case "D":

                    //$('#txtDias').val(1);
                    //txtDias.prop('disabled', false);
                    //$scope.anormal = true;
                    $('#agenda').val("00:00:00");
                    $('#selMin').val(0);
                    $('#selHoras').val(0);
                    $('#txtDataFim').prop('disabled', false);
                    $('#spDataFim').css("pointer-events", "auto");
                    $('#txtDias').prop('disabled', false);
                    $('#agenda').prop('disabled', true);
                    $('#selMin').prop('disabled', true);
                    $('#selHoras').prop('disabled', true);
                    $('#spAgenda').css("pointer-events", "none");
                    prazo_param = valor;
                    break;

                case "A":

                    //txtDias.prop('disabled', false);
                    //$scope.anormal = true;
                    $('#txtDias').val(0);
                    $('#selMin').val(0);
                    $('#selHoras').val(0);
                    $('#txtDataFim').prop('disabled', false);
                    $('#spDataFim').css("pointer-events", "auto");
                    $('#agenda').prop('disabled', false);
                    $('#txtDias').prop('disabled', true);
                    $('#selMin').prop('disabled', true);
                    $('#selHoras').prop('disabled', true);
                    $('#spAgenda').css("pointer-events", "auto");
                    prazo_param = valor;
                    break;

                case "M":
                    $('#txtDias').val(0);
                    $('#agenda').val("00:00:00");
                    $('#selHoras').val(0);
                    $('#txtDataFim').prop('disabled', false);
                    $('#spDataFim').css("pointer-events", "auto");
                    $('#selMin').prop('disabled', false);
                    $('#txtDias').prop('disabled', true);
                    $('#agenda').prop('disabled', true);
                    $('#spAgenda').css("pointer-events", "none");
                    $('#selHoras').prop('disabled', true);
                    prazo_param = valor;
                    break;

                case "H":
                    $('#txtDias').val(0);
                    $('#agenda').val("00:00:00");
                    $('#selMin').val(0);
                    $('#txtDataFim').prop('disabled', false);
                    $('#spDataFim').css("pointer-events", "auto");
                    $('#selHoras').prop('disabled', false);
                    $('#selMin').prop('disabled', true);
                    $('#txtDias').prop('disabled', true);
                    $('#agenda').prop('disabled', true);
                    $('#spAgenda').css("pointer-events", "none");
                    prazo_param = valor;
                    break;
            }

            $scope.$apply();
        }


        $scope.mudaDiasParamNormal = function () {

            var param = $view.find("#selectParamModal").val();
            var txtDias = $view.find("#txtDias");

            if (param === 'normal' || param === 'Normal') {

                // se NORMAL = SEM DATAFIM E DATAINI depende do reparo
                // DIAS = 0
                $scope.anormal = false;
                txtDias.val(0);

                txtDias.prop('disabled', 'disabled');
                $('#txtDataFim').attr('disabled', true);
                // se NÃO NORMAL = Dias
            } else {
                //txtDias.val(1);
                txtDias.prop('disabled', false);

                $scope.anormal = true;
                $('#txtDataFim').attr('disabled', false);
            }
            $scope.$apply();

        };

        //$("#selectParamModal").change($scope.mudaDiasParamNormal);
		cache.carregaGras(function(){ $scope.preencheGras; });




    }); //FIM do ViewContentLoaded




    $scope.salvar = function () {

        /* $('.modalGrasList').hide();
         $('#lbPrimary').text("");
         $('#myModalLabel').text("EDITAR GRAS");   */

        //var filtro_gras = $view.find("#selectGra").val() || [];
        var filtro_gras = $scope.gras || [];

        var codigos = [];
        console.log($scope.gras);
        //console.log(codigos[0].codgra);

        filtro_gras.forEach(function (gra) {
            codigos.push(gra.id);
        });

        var filtro_parametros = $view.find("#selectParamModal").val() || [];
        var filtro_obs = $('#txtObs').val();
        var filtro_dias = ($('#txtDias').prop('disabled') ? '0' : $('#txtDias').val());
        var filtro_hr = ($('#selHoras').prop('disabled') ? '0' : $('#selHoras').val());
        var filtro_min = ($('#selMin').prop('disabled') ? '0' : $('#selMin').val());
        var filtro_horario = ($('#agenda').val() == "00:00:00" ? null : $('#agenda').val());
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var testedata = testeDataMaisHora(data_ini, data_fim);
        if (testedata !== "") {
            setTimeout(function () {
                //atualizaInfo($scope, testedata);
                //effectNotification();
                //$view.find(".btn-gerar").button('reset');
                alert("Atenção! Data Inicial precisa ser menor que Data Final.")
                $('.btn-atualizarGRAS').trigger('click');

            }, 500);
            return;
        }

        if (filtro_parametros === 'normal') {
            $('#txtDias').val('0');
            $('#txtDias').prop('disabled', 'disabled');
            //}else{
            //    $('#txtDias').val('1');
        }


        if (!$scope.frmGra.$valid) {
            setTimeout(function () {
                $('#lbPrimary').text("Campo(s) obrigatório(s) não preenchido(s).");
                atualizaInfo($scope, "Campo(s) obrigatório(s) não preenchido(s).");
            }, 500);
            return;
        }

        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        // se for normal, não precisa de data fim, os outros casos, verificar se é reparoProgramado
        var stmt2 = "",
            stmt3 = "";

        stmt = db.use + " UPDATE gras_ativos SET dataini = @dataini, datafim = @datafim , parametro = @parametro, dias = @dias ,"
            + "acao = @acao, login = @login, obs = @obs, horas = @horas,prazo = @prazo,minutos = @minutos,horario = @horario ";
        stmt += " WHERE 1=1 ";
        stmt += restringe_consulta('idgra', codigos, true);
        stmt += ";";

        stmt2 = " DELETE FROM gras_ufs_ocorrencias WHERE 1=1";
        stmt2 += restringe_consulta('codgra', filtro_gras.map(function (fg) { return fg.codgra }), true);
        stmt2 += ";";
        if ($scope.regraParametroUF) {

            // criar for para multiplos valores de variavel $('select.filtro-uf-Modal').val()
            var ufsmodal = $('select.filtro-uf-Modal').val() || [];

            console.log(filtro_gras.map(function (fg) { return fg.codgra }))
            for (j = 0; j < filtro_gras.length; j++) {
                for (i = 0; i < ufsmodal.length; i++) {
                    stmt3 += " INSERT INTO gras_ufs_ocorrencias (codgra,dataini,datafim,parametro,uf) VALUES ('" + filtro_gras[j].codgra + "',@dataini,@datafim ,@parametro,'" + ufsmodal[i] + "')";
                    stmt3 += ";";
                };
            };

        } else {
            console.log('nao caiu');
        };
        // LIMITAR AQUI OS UFS

        stmts = stmt + stmt2 + stmt3;
        console.log(stmts)
        db.query(stmts, null, function (err, num_rows) {
            //userLog(stmts, 'Alteracoes de Insert, Delete e Update', 2, err)
            if (err) {
                console.log("ERRO: " + err);
            } else {
                if ($('#myModal') !== null) {
                    $('#myModal').modal('hide');
                }
				console.log("Query: "+stmts);

                setTimeout(function () {
                    limpaProgressBar($scope, "#pag-gra");
                    setTimeout(function () {
                        atualizaInfo($scope, num_rows / 2 + " registro(s) afetado(s), operação realizada com sucesso.");
                        // gilberto 04-06-2014
                        $scope.listar.apply(this);
                        //22/03/2014 Testa lista de gras
                    }, 2000);

                }, 500);

                console.log("O UPDATE retornou " + num_rows + " registros");
                console.log(stmt);
            }

        }, function (request) {

            //request.addParameter('codGra', TYPES.NVarChar, gra.cod);
            request.addParameter('login', TYPES.NVarChar, loginUsuario);
            request.addParameter('dataini', TYPES.NVarChar, $scope.reparoProgramado ? formataDataHora(data_ini) : formataDataHora(new Date()));


            request.addParameter('datafim', TYPES.NVarChar, formataDataHora(data_fim));
            request.addParameter('horas', TYPES.Int, Number(filtro_hr));
            request.addParameter('prazo', TYPES.VarChar, prazo_param);
            request.addParameter('minutos', TYPES.Int, Number(filtro_min));
            request.addParameter('horario', TYPES.VarChar, (filtro_horario));
            request.addParameter('parametro', TYPES.NVarChar, filtro_parametros);
            request.addParameter('dias', TYPES.Int, Number(filtro_dias));
            request.addParameter('acao', TYPES.NVarChar, "A");
            request.addParameter('obs', TYPES.NVarChar, filtro_obs);
        });
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------

        setTimeout(function () {
            $scope.gra.cod = null;
            $scope.gra.problema = null;
            $scope.gra.acao = null;
            $scope.gra.dias = null;
            $scope.gra.obs = null;
        }, 1000);


    }; ///Fim de salvar

    $scope.cancelar = function () {
        //limpaProgressBar($scope, "#pag-gra");
        $('.btn-atualizarGRAS').trigger('click');
    };

    $scope.atualizarGRAS = function () {
        $('.modalGrasList').hide();
        $('#lbPrimary').text("");
        $('#myModalLabel').text("EDITAR GRAS");


        $scope.mudaDiasParamNormal();

        $('#txtDataIni').val(formataDataBR($scope.periodo.inicio));
        $('#txtDataFim').val(formataDataBR($scope.periodo.fim));

        //$('#txtObs').val(gra.obs);
        $('#txtDias').val(0);


    };


    /*$scope.atualizar = function (gra) {

      $('#lbPrimary').text("");
      $('#myModalLabel').text("ATUALIZAR OCORRÊNCIA");

      $('#txtObs').val(gra.obs);
      $('#txtDias').val(gra.dias);


      var $sel_gras = $view.find("#selectGraModal");
      $sel_gras.html("");
      var options = [];

      options.push('<option value="' + gra.codgra + '">' + gra.codgra + '</option>');
      $sel_gras.html(options.join());


      var $sel_param = $view.find("#selectParam");
      $sel_param.html("");
      var options2 = [];
      options2.push('<option value="' + gra.parametro + '">' + gra.parametro + '</option>');
      $sel_param.html(options2.join());

      $sel_param.attr("disabled", "disabled");
      $sel_param.selectpicker("refresh");
      $sel_gras.attr("disabled", "disabled");
      $sel_gras.selectpicker("refresh");

    }; //Fim de Atualizar.
*/
    $scope.listarHistoricoGra = function () {

        $globals.numeroDeRegistros = 0;
        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        $scope.gras = [];

        var filtro_gras = $view.find("#selectGra").val() || [];
        var filtro_parametros = $view.find("select.filtro-parametros").val() || [];
        var filtro_ufs = $view.find("select.filtro-uf").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_codgra = $('.filtro-codGra').val();

        var stmt = db.use + " select HG.dataini, HG.CODGRA,HG.LOGIN, HG.ACAO ,G.UF,G.DDD, HG.PARAMETRO,HG.DIAS,HG.OBS,HG.PRAZO,HG.HORAS,HG.MINUTOS,HG.HORARIO,G.IDGRA ";
        stmt += " from historico_gra HG join gras2 G on HG.IDGRA = G.IDGRA ";
        //var stmt = db.use + " SELECT MAX(dataini) as data ,codgra ,login, acao, parametro ,dias, obs from historico_gra "
        stmt += " WHERE 1=1 ";
        if (filtro_codgra != '') {
            stmt += " AND HG.CODGRA = '" + filtro_codgra + "'";
        }
        stmt += restringe_consulta("HG.CODGRA", filtro_gras, true);
        stmt += restringe_consulta("UF", filtro_ufs, true);
        stmt += restringe_consulta("DDD", filtro_ddds, true);
        stmt += restringe_consulta("PARAMETRO", filtro_parametros, true);
        stmt += " ORDER BY uf, DDD, hg.codgra";

        log(stmt);



        db.query(stmt, function (columns) {
            //userLog(stmt, 'Consulta prazo', 2, "")
            var prazo;
            if (columns[9].value == "D" || columns[9].value == "") {
                prazo = columns[7].value + 'd';
            } else if (columns[9].value == "H") {
                prazo = columns[10].value + ' hs';
            } else if (columns[9].value == "M") {
                prazo = columns[11].value + 'min';
            } else if (columns[9].value == "A") {
                prazo = columns[12].value;
            }

            var gra = {
                codgra: columns[1].value,
                //expirado: moment(columns[0].value).add('days', columns[5].value)._d < new Date(),


                expirado: (columns[6].value !== 'normal' && (columns[0].value.getTime() - new Date().getTime()) < 0),
                dataini: formataDataHoraBR(columns[0].value),
                login: columns[2].value,
                uf: columns[4].value,
                ddd: columns[5].value,
                //acao: columns[3].value,
                parametro: columns[6].value,
                prazo: prazo,
                obs: columns[8].value,
                id: columns[13].value
            };

            $scope.gras.push(gra);

        }, function (err, num_rows) {
            if (err) {
                console.log(err.message);
            }
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            console.log("A consulta retornou " + num_rows + " registros");
            //$scope.$apply(function(){});
            console.log('Executou -> ' + stmt);

            if (num_rows > 0) {
                var $btn = $view.find(".btn-atualizarGRAS");
                $btn.prop("disabled", false);
            } else {
                var $btn = $view.find(".btn-atualizarGRAS");
                $btn.prop("disabled", true);
            }

            $view.on("mouseup", "tr.detalhamento", function () {
                var that = $(this);
                //console.log('passei');
                $('tr.detalhamento.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });

        },
            function (request) {

                //request.addParameter('codGra', TYPES.NVarChar, gra.cod);
            });
        /**/
    }; // Fim de Listar Histórico GRA




    $scope.listarGrasAtivos = function () {


        $globals.numeroDeRegistros = 0;
        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        $scope.gras = [];
        var filtro_gras = $view.find("#selectGra").val() || [];
        var filtro_ufs = $view.find("select.filtro-uf").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_parametros = $view.find("select.filtro-parametros").val() || [];
        var filtro_codgra = $('.filtro-codGra').val();
        //gras = [];

        //var stmt = db.use + " with G AS ( SELECT GRA from gras2 where 1=1 ";
        //	stmt += restringe_consulta("UF", filtro_ufs,true);
        //	stmt += restringe_consulta("DDD", filtro_ddds,true);
        //    stmt += restringe_consulta("GRA", filtro_gras,true);
        //    stmt += ") ";
        //	stmt += " SELECT CODGRA, DATAINI, DATAFIM, PARAMETRO, ACAO, DIAS, OBS from G join gras_ativos GA on G.GRA = GA.CODGRA WHERE 1=1 "
        //	stmt += restringe_consulta("PARAMETRO", filtro_parametros,true);
        //	stmt += " ORDER BY CODGRA ";




        var stmt = db.use + " SELECT G.GRA,G.UF,G.DDD,GA.CODGRA, GA.DATAINI, GA.DATAFIM, GA.PARAMETRO, GA.ACAO, GA.DIAS, GA.OBS,GA.PRAZO,GA.HORAS,GA.MINUTOS,GA.HORARIO,G.IDGRA"
        //Alex 25/07/2017
        stmt += ",(SELECT STUFF((select uf + ',' from Gras_Ufs_Ocorrencias where CODGRA =	GA.CODGRA for xml path('')), 1, 0, ''))AS UFS";
        stmt += " from gras2 G ";
        stmt += " INNER JOIN gras_ativos GA on G.IDGRA = GA.IDGRA";

        stmt += " WHERE 1=1 ";
        if (filtro_codgra != '') {
            stmt += " AND GA.CODGRA = '" + filtro_codgra + "'";
        }
        stmt += restringe_consulta("UF", filtro_ufs, true);
        stmt += restringe_consulta("DDD", filtro_ddds, true);
        stmt += restringe_consulta("GRA", filtro_gras, true);
        stmt += restringe_consulta("PARAMETRO", filtro_parametros, true);
        stmt += " ORDER BY UF,DDD,GA.CODGRA ";


        log(stmt);
        // GILBERTO 05-06-2014, uma conexão exclusiva para cada consulta.



        db.query(stmt, function (columns) {
            var expirou = columns[5].value == null
                ? false
                : ((columns[5].value.getTime() - new Date().getTime()) < 0);

            var datafim = columns[5].value == null
                ? '--'
                : formataDataBR(columns[5].value);

            var prazo;
            if (columns[10].value == "D" || columns[10].value == "") {
                prazo = columns[8].value + 'd';
            } else if (columns[10].value == "H") {
                prazo = columns[11].value + 'hs';
            } else if (columns[10].value == "M") {
                prazo = columns[12].value + 'min';
            } else if (columns[10].value == "A") {
                prazo = columns[13].value;
            }

            var gra = {
                codgra: columns[0].value,
                expirado: expirou,
                uf: columns[1].value,
                ddd: columns[2].value,
                dataini: formataDataBR(columns[4].value),
                datafim: datafim,
                parametro: columns[6].value,
                acao: columns[7].value,
                prazo: prazo,
                ufs: columns['UFS'].value !== null ? columns['UFS'].value.substring(0, columns['UFS'].value.length - 1) : columns['UFS'].value,
                obs: columns[9].value,
                id: columns[14].value
            };

            $scope.gras.push(gra);
            //console.log(gra);

        }, function (err, num_rows) {
            if (err) {
                console.log(err.toString() + num_rows);
            }
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            //$scope.$apply(function(){});
            //console.log('Executou -> ' + stmt);

            var $btn = $view.find(".btn-atualizarGRAS");

            $btn.prop("disabled", num_rows <= 0)
            // if(num_rows > 0){
            // var $btn = $view.find(".btn-atualizarGRAS");
            // $btn.prop("disabled", false);
            // }else{
            // var $btn = $view.find(".btn-atualizarGRAS");
            // $btn.prop("disabled", true);
            // }


            $view.on("mouseup", "tr.detalhamento", function () {
                var that = $(this);
                //console.log('passei');
                $('tr.detalhamento.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });

        },
            function (request) { });
        /**/


    }; // Fim de Listar Histórico GRA






    $scope.consultarGraPorCod = function () {
        var codGra = $scope.codGra;
        codGra = codGra && codGra.toUpperCase();

        console.log("executou consultaGraPorCod ");
        var gra = {};
        //var stmt = " WITH G AS ( SELECT GRA from gras2 where GRA = @codGra ) "
        //stmt += " SELECT TOP 1 CODGRA, DATAINI,DATAFIM, PARAMETRO, DIAS, OBS "
        //stmt += " FROM G join gras_ativos GA on G.GRA = GA.CODGRA ";

        var stmt = " SELECT TOP 1 GA.CODGRA, GA.DATAINI,GA.DATAFIM, GA.PARAMETRO, GA.DIAS, GA.OBS,G.GRA";
        stmt += ",(SELECT STUFF((select uf + ',' from Gras_Ufs_Ocorrencias where CODGRA =	GA.CODGRA for xml path('')), 1, 0, '')) AS UFS";
        stmt += " from gras2 G inner Join gras_ativos GA on G.GRA = GA.CODGRA where GRA = @codGra";

        db.query(stmt, function (columns) {
            //userLog(stmt, 'Consulta GraPorCod', 2, "")
            //db.procedure(db.use + 'consultaGraPorCod', function (columns) {
            var expirou = columns[2].value == null ? false : ((columns[2].value.getTime() - new Date().getTime()) < 0);
            var datafim = columns[2].value == null ? '--' : formataDataBR(columns[2].value);

            gra = {
                codgra: columns[0].value,
                expirado: expirou,
                dataini: formataDataHoraBR(columns[1].value),
                datafim: datafim,
                parametro: columns[3].value,
                dias: columns[4].value,
                ufs: columns['UFS'].value !== null ? columns['UFS'].value.substring(0, columns['UFS'].value.length - 1) : columns['UFS'].value,
                //acao: columns[4].value,
                obs: columns[5].value
            };
        }, function (err, num_rows) {
            if (err) {
                console.log(err.toString() + ", linhas afetadas: " + num_rows);
            }
            retornaStatusQuery(num_rows, $scope);

            //$scope.$apply(function(){});
            //console.log('Executou -> ' + stmt);

            var $btn = $view.find(".btn-atualizarGRAS");

            $btn.prop("disabled", num_rows <= 0)
            // if(num_rows > 0){
            // var $btn = $view.find(".btn-atualizarGRAS");
            // $btn.prop("disabled", false);
            // }else{
            // var $btn = $view.find(".btn-atualizarGRAS");
            // $btn.prop("disabled", true);
            // }
            var $corpoModalResult = $('#corpoModalResult');
            var text = "";
            for (var prop in gra) {
                text += '<p style="margin-left:12px">' + prop + ': ' + gra[prop] + '</p><br/>';
            }

            $corpoModalResult.html(text);

            $('#modalResult').modal();


            $view.on("mouseup", "tr.detalhamento", function () {
                var that = $(this);
                //console.log('passei');
                $('tr.detalhamento.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });

        },
            function (request) {
                request.addParameter('codGra', TYPES.NVarChar, codGra);
            }); //fim do db.query
    }; // fim de consultarGraPorCod



    //$scope.preparaNovoModal = function(){
    //     $('.modalGrasList').show();
    //     $('#lbPrimary').text("");
    //     $('#myModalLabel').text("INSERIR NOVA OCORRÊNCIA");
    //	 $('#selectGraModal').selectpicker('deselectAll').attr("disabled", false);
    //	 $('#selectParam').selectpicker('deselectAll').attr("disabled", false);

    //	 try{
    //	 	$scope.preencheGras();
    //	 }catch(e){
    //	 	console.log(e.message);
    //	 }

    //	 $('#selectGraModal').selectpicker('refresh');
    //	 $('#selectParam').html('<option value="normal">Normal</option><option value="chuva">Chuva</option><option value="energia eletrica">Energia Elétrica</option>').selectpicker('refresh');

    //	$('#txtDias').val('');
    //	$('#txtObs').val('');

    //	$scope.gra.cod = null;
    //	$scope.gra.problema = null;
    //	$scope.gra.acao = null;
    //	$scope.gra.dias = null;
    //	$scope.gra.obs = null;

    //}; //Fim de preparaNovoModal8*/
}

CtrlGra.$inject = ["$scope", "$globals"];
