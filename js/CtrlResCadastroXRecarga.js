/*
 *
 */
function CtrlResCadastroXRecarga($scope, $globals) {



    win.title = "Resumo Cadastro x Recarga - USSD"; //Bernardo 04/02/2016
    $scope.versao = versao;
    $scope.dados = [];
    $scope.csv = [];
    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
    $scope.filtros = {
        aplicacoes: [],
        segmentos: []

    };



    travaBotaoFiltro(0, $scope, "#pag-rescadastroxrecarga", "Resumo Cadastro x Recarga - USSD");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    $scope.ordenacao = ['data_hora', 'chamador'];
    $scope.decrescente = false;


    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };




    $scope.filtro_aplicacoes = [];


    $scope.aba = 2;

    /*cache.produtos_vendas.indice = geraIndice(cache.produtos_vendas);*/

    function geraColunas() {
        var array = [];



        var exibeRecarga = $('#chkRecarga').prop("checked") ? true : false;
        var exibeData = $('#chkDia').prop("checked") ? true : false;
        //var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

        //var templateWithTooltip = '<div class="ngCellText" ng-class="col.colIndex()"><span title="{{row.getProperty(\'erro\')}}" ng-cell-text>{{row.getProperty(col.field)}}</span></div>';

        array.push({ field: "dataCadastro", displayName: "Data Cadastro", width: 200, pinned: false, visible: exibeData },
                   { field: "aplCadastro", displayName: "Aplicação Cad.", width: 120, pinned: false },
                   { field: "entCadastro", displayName: "Entrantes Cad.", width: 120, pinned: false },
                   { field: "sucessoCadastro", displayName: "Sucesso Cad.", width: 120, pinned: false },
                   { field: "aplRecarga", displayName: "Canal Recarga", width: 120, pinned: false, visible: exibeRecarga },
                   { field: "entRecarga", displayName: "Total Rec.", width: 120, pinned: false },
                   { field: "sucessoRecarga", displayName: "Sucesso Rec.", width: 120, pinned: false },
                   { field: "erroRecarga", displayName: "Erro Rec.", width: 120, pinned: false },
                   { field: "valor", displayName: "Valor", width: 100, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' }

                  );

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    $scope.valor = 24;

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-rescadastroxrecarga");
      treeView('#pag-rescadastroxrecarga #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-rescadastroxrecarga #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-rescadastroxrecarga #ed', 65,'ED','dvED');
      treeView('#pag-rescadastroxrecarga #ic', 95,'IC','dvIC');
      treeView('#pag-rescadastroxrecarga #tid', 115,'TID','dvTid');
      treeView('#pag-rescadastroxrecarga #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-rescadastroxrecarga #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-rescadastroxrecarga #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-rescadastroxrecarga #parametros', 225,'Parametros','dvParam');
      treeView('#pag-rescadastroxrecarga #admin', 255,'Administrativo','dvAdmin');
	    treeView('#pag-rescadastroxrecarga #monitoracao', 275, 'Monitoração', 'dvReparo');
      treeView('#pag-rescadastroxrecarga #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/

        $(".aba3").css({ 'position': 'fixed', 'left': '60px', 'top': '40px' });
        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });


        //minuteStep: 5

        //19/03/2014
        componenteDataHora($scope, $view);
        carregaAplicacoes($view, false, true, $scope);
        carregaSegmentosPorAplicacao($scope, $view, true);
        carregaRegioes($view);
        carregaDDDsPorRegiao($scope, $view, true);
        // Popula lista de  ddds a partir das regioes selecionadas
        $view.on("change", "select.filtro-regiao", function () {

            $('#alinkApl').removeAttr('disabled');
            $(".filtro-aplicacao").attr('multiple', 'multiple');
            carregaDDDsPorRegiao($scope, $view);

        });

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () { carregaSegmentosPorAplicacao($scope, $view) });


        //2014-11-27 transição de abas
        var abas = [2, 3, 4];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }


        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope, true) });


        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function () { marcaTodosIndependente($('.filtro-ddd'), 'ddds') });

        // Marca todos as recargas
        $view.on("click", "#alinkRecarga", function () { marcaTodosIndependente($('.filtro-canal'), 'recarga') });

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function () { marcaTodasRegioes($('.filtro-regiao'), $view, $scope) });


        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        /*$view.find(".selectpicker").selectpicker({
          //noneSelectedText: 'Nenhum item selecionado',
          countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-canal").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} recarga',
            showSubtext: true
        });





        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-canal').mouseover(function () {
            $('div.btn-group.filtro-canal').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-canal').mouseout(function () {
            $('div.btn-group.filtro-canal').removeClass('open');
        });



        $view.on("dblclick", "div.ng-scope.ngRow", function () {
            //$scope.formataXML($scope.chamada_atual);
            //$view.find(".btn-log-original").parent().removeClass("active");
            //$view.find(".btn-log-formatado").parent().addClass("active");
            $scope.$apply(function () {
                $scope.ocultar.log_original = true;
                $scope.ocultar.log_formatado = false;
            });
            $view.find("#modal").modal('show');
        });


        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {

            limpaProgressBar($scope, "#pag-cadastroxrecarga");

            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }



            //22/03/2014 Testa se uma aplicação foi selecionada
            //var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            //if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
            //    setTimeout(function () {
            //        atualizaInfo($scope, 'Selecione uma aplicação');
            //        effectNotification();
            //        $view.find(".btn-gerar").button('reset');
            //    }, 500);
            //    return;
            //}
            $scope.colunas = geraColunas();

            /*var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val();
            obtemPromptsDasAplicacoes(filtro_aplicacoes);
            if(!filtro_aplicacoes.indexOf('CORPO') || filtro_aplicacoes !=='CORPO'){
            obtemItensDeControleDasAplicacoes(filtro_aplicacoes,true);
            }*/

            $scope.listaDados.apply(this);
        });



        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-cadastroxrecarga");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        /*$scope.$apply(function(){
        });
        var h = document.getElementById('pag-vendas').scrollHeight;
        $('body').scrollTop(h);*/
        //retornaStatusQuery($scope.chamadas.length, $scope, $scope.numRegistrosExibidos);

    };

    // Lista vendas conforme filtros
    $scope.listaDados = function () {



        //if (!connection) {
        //  db.connect(config, $scope.listaVendas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_recargas = $view.find("select.filtro-canal").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
        if (filtro_recargas.length > 0) { $scope.filtros_usados += " Recargas: " + filtro_recargas; }

        $scope.dados = [];
        $scope.csv = [];


        var tabela;
        $('#24h').prop("checked") ? tabela = "ResumoCadastroxRecarga24h" : tabela = "ResumoCadastroxRecarga48h";
        var tipoData = $('#chkDia').prop("checked") ? ",Convert(Date,DataCadastro) as DataCadastro" : "";
        var tipoRecarga = $('#chkRecarga').prop("checked") ? ",CanalRecarga" : "";

        var stmt = db.use + "SELECT CodAplicacaoCadastro" + tipoData + tipoRecarga +""+
        ",sum(QtdEntrantesCadastro) as QtdEntrantesCadastro,sum(QtdCadastros) as QtdCadastros, " +
        "sum(QtdRecargas) as QtdRecargas,sum(QtdRecargasSucesso) as QtdRecargasSucesso,sum(QtdRecargasErro) as QtdRecargasErro, " +
        "sum(ValorRecargas) as ValorRecargas";
        stmt += " FROM " + db.prefixo + tabela + ""

        + " WHERE 1 = 1"
        + " AND DataCadastro >= '" + formataDataHora(data_ini) + "' AND  DataCadastro <= '" + formataDataHora(data_fim) + "'";

        stmt += restringe_consulta("CodAplicacaoCadastro", filtro_aplicacoes, true);
        if (filtro_recargas.length > 0) {
            if (filtro_recargas == "ussd") {
                stmt += " AND CanalRecarga like '%USSD%'";
            } else if (filtro_recargas == "outros") {
                stmt += " AND CanalRecarga ='OUTROS'";
            }
        }
        stmt += restringe_consulta("DDD", filtro_ddds, true);
        stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);

        stmt += " group by CodAplicacaoCadastro";

        if ($('#chkDia').prop("checked")) { stmt += ",Convert(Date,DataCadastro)"; }
        if ($('#chkRecarga').prop("checked")) { stmt += ",CanalRecarga"; }

        stmt += " order by ";
        if ($('#chkDia').prop("checked")) { stmt += "Convert(Date,DataCadastro),CodAplicacaoCadastro" } else { stmt += "CodAplicacaoCadastro" }
        if ($('#chkRecarga').prop("checked")) { stmt += ",CanalRecarga"; }
        //stmt = stmt + " order by " + tipoData + ",CodAplicacaoCadastro";

        log(stmt);

        function executaQuery(columns) {

            if ($('#chkDia').prop("checked")) {
                var dataAux = new Date(columns["DataCadastro"].value);
                dataAux.setDate(dataAux.getDate() + 1);
            }
            //var dataCadastro = $('#chkDia').prop("checked") ? formataDataBR(dataAux) : formataDataHoraBR(columns["DataCadastro"].value),
            var aplCadastro = columns["CodAplicacaoCadastro"].value,
            entCadastro = +columns["QtdEntrantesCadastro"].value,
            sucessoCadastro = +columns["QtdCadastros"].value,
            aplRecarga = $('#chkRecarga').prop("checked") ? columns["CanalRecarga"].value : "",
            entRecarga = +columns["QtdRecargas"].value,
            sucessoRecarga = +columns["QtdRecargasSucesso"].value,
            erroRecarga = +columns["QtdRecargasErro"].value,
            valor = +columns["ValorRecargas"].value;

            $scope.dados.push({
                dataCadastro: $('#chkDia').prop("checked") ? formataDataBR(dataAux) : "",
                aplCadastro: aplCadastro,
                entCadastro: entCadastro,
                sucessoCadastro: sucessoCadastro,
                aplRecarga: aplRecarga,
                entRecarga: entRecarga,
                sucessoRecarga: sucessoRecarga,
                erroRecarga: erroRecarga,
                valor: valor
            });

            $scope.csv.push([
                $('#chkDia').prop("checked") ? formataDataBR(dataAux) : "",
                aplCadastro,
                entCadastro,
                sucessoCadastro,
                aplRecarga,
                entRecarga,
                sucessoRecarga,
                erroRecarga,
                valor
            ]);
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
            //if ($scope.dados.length % 1000 === 0) {
            //    $scope.$apply();
            //}
            //$scope.$apply();
        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);

            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
        });


    };




    /*// Consulta e formata o XML da chamada
    $scope.formataXML = function (venda) {
      // Se a chamada acabou de ser consultada, não há nada a fazer
      if (venda === $scope.venda_atual) return true;

      // Limpar o log original da chamada consultada anteriormente
      $scope.venda_atual = venda;
      $scope.callLog = "";
      $scope.ocultar.log_original = true;
      $scope.ocultar.log_formatado = false;

      //var trecho  = "AND CodTrecho = '01'";
      var trecho  = "";

      var stmt = "SELECT XMLIVR FROM IVRCDR_" + venda.data_hora.substr(5, 2)
        + " WHERE CodUCIDIVR = '" + venda.UCID + "' "+trecho+"";
      log(stmt);
      db.query(stmt, function (columns) {
        $scope.$apply(function () {
          $scope.logxml = parseXML(columns[0].value, venda);
      });

  });

  return true;


    };*/


    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {


        var $btn_exportar = $(this);
        $btn_exportar.button('loading');


        if ($scope.vendas.length > 50000) {


            //Alex 15-02-2014 - 26/03/2014 TEMPLATE
            var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
                 + " WHERE NomeRelatorio='tVendasMore'";
            log(stmt);
            db.query(stmt, function (columns) {
                userLog(stmt, 'Extrai XLSX', 2, err)
                var dataAtualizacao = columns[0].value,
                    nomeRelatorio = columns[1].value,
                    arquivo = columns[2].value;

                var milis = new Date();
                var baseFile = 'tVendasMore.xlsm';
                var buffer = toBuffer(toArrayBuffer(arquivo));
                fs.writeFileSync(baseFile, buffer, 'binary');
            }, function (err, num_rows) {
                if (fs.existsSync('dados_vendas.txt')) {
                    fs.unlinkSync('dados_vendas.txt');
                }
                var i = 0;
                $scope.vendas.forEach(function (v) {
                    fs.appendFileSync('dados_vendas.txt', v.data_hora_BR + '\t' + v.chamador + '\t' + v.aplicacao + '\t' + v.tipo + '\t' + v.grupo + '\t' + v.produto + '\t' + v.valor + '\t' + v.status + '\t' + v.codErro + '\t' + v.erro + '\t' + v.codOferta + '\t\n');
                    i++;
                    if (i === $scope.vendas.length) {
                        childProcess.exec('tVendasMore.xlsm');
                    }
                });

                setTimeout(function () {
                    $btn_exportar.button('reset');
                }, 500);

            },  function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)              
            });

        } else {

            //Alex 15-02-2014 - 26/03/2014 TEMPLATE
            var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
                 + " WHERE NomeRelatorio='tVendas'";
            log(stmt);
            db.query(stmt, function (columns) {
                userLog(stmt, 'Extrai XLSX', 2, err)
                var dataAtualizacao = columns[0].value,
                    nomeRelatorio = columns[1].value,
                    arquivo = columns[2].value;


                var milis = new Date();
                var baseFile = 'tVendas.xlsx';
                var buffer = toBuffer(toArrayBuffer(arquivo));
                fs.writeFileSync(baseFile, buffer, 'binary');



                //var milis = new Date();
                //var baseFile = 'templates/tVendas.xlsx';


                var file = 'detalhamentoVendas_' + formataDataHoraMilis(milis) + '.xlsx';

                var newData;


                fs.readFile(baseFile, function (err, data) {

                    // Gilberto 28/03/2014 reordenação por data
                    //$scope.vendas.sort(function(primeiro, segundo){
                    //return primeiro.data.getTime()-segundo.data.getTime();
                    //});


                    // var vendas  =  $scope.vendas;

                    //vendas.map(function (array) {
                    //array.erro = array.codErro + array.erro;
                    //return array;
                    //});


                    // Create a template
                    var t = new XlsxTemplate(data);

                    // Perform substitution
                    t.substitute(1, {
                        filtros: $scope.filtros_usados,
                        planDados: $scope.vendas
                    });

                    // Get binary data
                    newData = t.generate();


                    if (!fs.existsSync(file)) {
                        fs.writeFileSync(file, newData, 'binary');
                        childProcess.exec(file);
                    }
                });

                setTimeout(function () {
                    $btn_exportar.button('reset');
                }, 500);



            }, function (err, num_rows) {
                //?
            });

        };

        $scope.celula = function (cod_status) {
            return (['success', 'danger', 'warning'])[cod_status];
        };
    }
}
CtrlResCadastroXRecarga.$inject = ['$scope', '$globals'];
