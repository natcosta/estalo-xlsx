var obj = [];
function CtrlContingenciaRouting($scope, $globals) {

    $scope.versao = versao;
 
    //Alex 06/02/2014 - Para permissões especiais
    $scope.login = loginUsuario;
    $scope.dominio = dominioUsuario;
    
    win.title="Contingência D3"; //Alex 27/02/2014

	//Alex 18/07/2017	
	var files = document.getElementById('contingenciaInput');	
	if (files.addEventListener) files.addEventListener('change', handleFile, false);

    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-contingencia-routing", "Contingência D3");
	//Filtros

    $scope.colunas = [];
    $scope.gridDados = {
      data: "dados",
      rowTemplate: '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="{expirado: row.getProperty(\'expirado\')  == true }" class="ngCell {{col.cellClass}} {{col.colIndex()}}" ng-cell></div>',
      columnDefs: "colunas",
      enableColumnResize: true,
      enablePinning: false,
	  enableSorting: false,
	  enableRowSelection: true
    };
   $scope.linhaIdGeral = "";
   $scope.dados = [];
   var paineis = ['FixoVelox','Pos','Pre','OiTV','OiTotal_Homg','Televendas','Corporativo','Cadastro','Oct','Empresarial', 'OiInternet', 'UraFibra'];

	function geraColunas(){
		var array = [];
		var direcao_b = "baixo";
		var direcao_c = "cima";

		array.push(
			//{ displayName: "", field: "", width: 80, pinned: false ,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="moveUp(row.entity)" class="icon-arrow-up"></a></span><span ng-cell-text style="margin-left:10px"><a href="#modal-log" target="_self" data-toggle="modal" ng-click="moveDown(row.entity)" class="icon-arrow-down"></a></span></div>'} ,
			{ displayName: "Id", field: "id", width: 80, pinned: false, visible: true, sortable: false},
			{ displayName: "Editar", field: "", width: 80, pinned: false ,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="editar(row.entity)" class="icon-edit"></a></span></div>'} ,
			{ displayName: "Excluir", field: "", width: 80, pinned: false ,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="excluir(row.entity)" class="icon-trash"></a></span></div>'}
		);
		
		//ContingenciaFixoVelox
		if ($('.filtro-regras2').val() === "ContingenciaFixoVelox"){
			array.push(
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },
				{ displayName: "URA_MAILING_R2", field: "ura_mailing", width: 160, pinned: false }
			);
		}

		//ContingenciaPos
		if ($('.filtro-regras2').val() === "ContingenciaPos"){
			array.push(
				{ displayName: "URA_PONTODERIVACAO", field: "ura_pontoderivacao", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },
				{ displayName: "URA_VCLIENTEJORNALISTA", field: "ura_vclientejornalista", width: 100, pinned: false },
				{ displayName: "URA_ISFLAGFRAUDE", field: "ura_isflagfraude", width: 100, pinned: false },
				{ displayName: "URA_ISOCT", field: "ura_isoct", width: 100, pinned: false },
				{ displayName: "URA_ISPOSPAGO", field: "ura_ispospago", width: 100, pinned: false },
				{ displayName: "URA_ISCLIENTEOCT4P", field: "ura_isclienteoct4p", width: 100, pinned: false },
				{ displayName: "IS_CONTAEMABERTO_ENTREQTDDIAS", field: "is_contaemaberto_entreqtddias", width: 100, pinned: false },
				{ displayName: "POSSUICONTAEMABERTOENTREXDIAS", field: "possuicontaemabertoentrexdias", width: 100, pinned: false },
				{ displayName: "URA_ISOSCTPROFISSIONALPJ", field: "ura_isosctprofissionalpj", width: 100, pinned: false },
				{ displayName: "URA_ISOCTPROFISSIONALPF", field: "ura_isoctprofissionalpf", width: 100, pinned: false },
				{ displayName: "URA_ISOIVELOX3G", field: "ura_isoivelox3g", width: 100, pinned: false },
				{ displayName: "URA_NOME_PLANO", field: "ura_nome_plano", width: 160, pinned: false },
				{ displayName: "URA_LASTDIALOG", field: "ura_lastdialog", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false }
			);  
		}

		//ContingenciaPre
		if ($('.filtro-regras2').val() === "ContingenciaPre"){
			array.push(
				{ displayName: "URA_PONTODERIVACAO", field: "ura_pontoderivacao", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },				
				{ displayName: "URA_ISFLAGFRAUDE", field: "ura_isflagfraude", width: 100, pinned: false },
				{ displayName: "URA_ISVIP", field: "ura_isvip", width: 100, pinned: false },
				{ displayName: "URA_SEGMENTOVALOR", field: "ura_segmentovalor", width: 100, pinned: false },
				{ displayName: "URA_ISOICONTROLE", field: "ura_isoicontrole", width: 100, pinned: false },
				{ displayName: "URA_ISSCOREANATEL", field: "ura_isscoreanatel", width: 100, pinned: false },
				{ displayName: "URA_WELCOME", field: "ura_welcome", width: 100, pinned: false },
				{ displayName: "URA_ISM4U", field: "ura_ism4u", width: 100, pinned: false },
				{ displayName: "URA_ISDIGITO", field: "ura_isdigito", width: 100, pinned: false },
				{ displayName: "URA_ISDEMANDAATIVA", field: "ura_isdemandaativa", width: 100, pinned: false },
				{ displayName: "URA_ISNID", field: "ura_isnid", width: 100, pinned: false },
				{ displayName: "URA_ISNOVOOICONTROLE", field: "ura_isnovooicontrole", width: 100, pinned: false },				
				{ displayName: "URA_ISPREPAGO", field: "ura_isprepago", width: 100, pinned: false },
				{ displayName: "URA_PROMOCAO", field: "ura_promocao", width: 100, pinned: false },
				{ displayName: "URA_ISOIMOD", field: "ura_isoimod", width: 100, pinned: false }		
			);  
		}

		//ContingenciaOiTV
		if ($('.filtro-regras2').val() === "ContingenciaOiTV"){
			array.push(
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false }
			);
		}

		//ContingenciaOiTotal
		if ($('.filtro-regras2').val() === "ContingenciaOiTotal_Homg"){
			array.push(
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_PLANO", field: "ura_plano", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },				
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false }
			);

		}

		//ContingenciaTelevendas
		if ($('.filtro-regras2').val() === "ContingenciaTelevendas"){
			array.push(
				{ displayName: "URA_MENU_ORIGEM", field: "ura_menu_origem", width: 160, pinned: false },
				{ displayName: "URA_CANAL_DE_ENTRADA", field: "ura_canal_de_entrada", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false }
			);
		}	
		
		//ContingenciaCorporativo
		if ($('.filtro-regras2').val() === "ContingenciaCorporativo"){
			array.push(
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },				
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false },
				{ displayName: "TEL_FIXO", field: "tel_fixo", width: 160, pinned: false },
				{ displayName: "VELOX", field: "velox", width: 160, pinned: false },
				{ displayName: "URA_CARTEIRA", field: "ura_carteira", width: 160, pinned: false },
				{ displayName: "TIPO_CLIENTE", field: "tipo_cliente", width: 160, pinned: false }
			);
		}

		//ContingenciaCadastro
		if ($('.filtro-regras2').val() === "ContingenciaCadastro"){
			array.push(
				{ displayName: "URA_CANAL_DE_ENTRADA", field: "ura_canal_de_entrada", width: 160, pinned: false },
				{ displayName: "URA_TIPO_CHIP", field: "ura_tipo_chip", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO", field: "final_digito", width: 160, pinned: false },	
				{ displayName: "URA_PLANO", field: "ura_plano", width: 160, pinned: false },
				{ displayName: "NOME_PLANO", field: "nome_plano", width: 160, pinned: false },			
				{ displayName: "ATENDIMENTO_BILINGUE", field: "atendimento_bilingue", width: 160, pinned: false },
				{ displayName: "MSIDN", field: "MSIDN", width: 160, pinned: false },
				{ displayName: "CHAMADASREPETIDA_24HORAS", field: "CHAMADASREPETIDA_24HORAS", width: 160, pinned: false }
			);
		}

		//ContingenciaOct
		if ($('.filtro-regras2').val() === "ContingenciaOct"){
			array.push(
				{ displayName: "URA_REPETIDACREP",field: "ura_repetidacrep", width: 160, pinned: false },
				{ displayName: "URA_PILOTOCREP",field: "ura_pilotocrep", width: 160, pinned: false },
				{ displayName: "URA_BLACKLISTCREP",field: "ura_blacklistcrep", width: 160, pinned: false },
				{ displayName: "URA_HORARIOCREP",field: "ura_horariocrep", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_BDABERTODENTRODOPRAZO",field: "URA_BDABERTODENTRODOPRAZO", width: 160, pinned: false },
				{ displayName: "URA_BDABERTOFORADOPRAZO",field: "URA_BDABERTOFORADOPRAZO", width: 160, pinned: false },
				{ displayName: "URA_BDVELOXABERTODENTRODOPRAZO",field: "URA_BDVELOXABERTODENTRODOPRAZO", width: 160, pinned: false },
				{ displayName: "URA_BDVELOXABERTOFORADOPRAZO",field: "URA_BDVELOXABERTOFORADOPRAZO", width: 160, pinned: false },
				{ displayName: "URA_QUARENTENACREP",field: "URA_QUARENTENACREP", width: 160, pinned: false },
				{ displayName: "URA_FRAUDE",field: "URA_FRAUDE", width: 160, pinned: false },
				{ displayName: "URA_TIPOBLOQUEIO",field: "URA_TIPOBLOQUEIO", width: 160, pinned: false },
				{ displayName: "URA_WELCOME",field: "URA_WELCOME", width: 160, pinned: false },
				{ displayName: "URA_CONTA_EM_ABERTO",field: "URA_CONTA_EM_ABERTO", width: 160, pinned: false },
				{ displayName: "URA_SOLICITACAO",field: "URA_SOLICITACAO", width: 160, pinned: false },
				{ displayName: "URA_MAILING",field:"ura_mailing", width: 160, pinned: false },
				{ displayName: "URA_PRODUTO",field:"ura_produto", width: 160, pinned: false },
				{ displayName: "URA_TELEFONE_TRATADO",field: "URA_TELEFONE_TRATADO", width: 160, pinned: false },
				{ displayName: "URA_VIP",field:"URA_VIP", width: 160, pinned: false },
				{ displayName: "URA_RESGATE_ANATEL",field: "URA_RESGATE_ANATEL", width: 160, pinned: false },
				{ displayName: "URA_TRN_ID", field:"URA_TRN_ID", width: 160, pinned: false },
				{ displayName: "URA_USOUAPP12MESES",field: "URA_USOUAPP12MESES", width: 160, pinned: false },
				{ displayName: "URA_USOUAPP20MIN",field:"URA_USOUAPP20MIN", width: 160, pinned: false },
				{ displayName: "URA_EVENTO",field:"URA_EVENTO", width: 160, pinned: false },
				{ displayName: "URA_DDD",field:"ura_ddd", width: 160, pinned: false },
				{ displayName: "FINAL_DIGITO",field:"final_digito", width: 160, pinned: false }
			);
		}		
		
		//ContingenciaEmpresarial
		if ($('.filtro-regras2').val() === "ContingenciaEmpresarial"){
			array.push(
				{ displayName: "URA_CANAL", field: "ura_canal", width: 160, pinned: false },
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },	
				{ displayName: "PARAMETRO_BLOQUEIO_FINANCEIRO", field: "parametro_bloqueio_financeiro", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false },
				{ displayName: "TEL_FIXO", field: "tel_fixo", width: 160, pinned: false },
				{ displayName: "VELOX", field: "velox", width: 160, pinned: false },
				{ displayName: "RENTABILIZACAO_EMPRESARIAL", field: "rentabilizacao_empresarial", width: 160, pinned: false },
				{ displayName: "CONSULTOR_EMPRESARIAL", field: "consultor_empresarial", width: 160, pinned: false }
			);
		}

		//ContingenciaOiInternet
		if ($('.filtro-regras2').val() === "ContingenciaOiInternet"){
			array.push(
				{ displayName: "URA_CANAL", field: "ura_canal", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_SEGMENTO", field: "ura_segmento", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },	
				{ displayName: "URA_PLANO", field: "ura_plano", width: 160, pinned: false },
				{ displayName: "URA_TIPO_BLOQUEIO", field: "ura_tipo_bloqueio", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false },
				{ displayName: "URA_MODEM_ALINHADO", field: "ura_modem_alinhado", width: 160, pinned: false }
			);
		}
		
		//ContingenciaUraFibra
		if ($('.filtro-regras2').val() === "ContingenciaUraFibra"){
			array.push(
				{ displayName: "URA_CANAL", field: "ura_canal", width: 160, pinned: false },
				{ displayName: "URA_PLANO", field: "ura_plano", width: 160, pinned: false },
				{ displayName: "URA_PRODUTO", field: "ura_produto", width: 160, pinned: false },
				{ displayName: "URA_TIPOPLANO", field: "ura_tipoplano", width: 160, pinned: false },	
				{ displayName: "URA_SEGMENTO", field: "ura_segmento", width: 160, pinned: false },
				{ displayName: "URA_PONTODERIVACAO", field: "ura_pontoderivacao", width: 160, pinned: false },
				{ displayName: "URA_ASSUNTO", field: "ura_assunto", width: 160, pinned: false },
				{ displayName: "URA_STATUS_ALONE", field: "ura_status_alone", width: 160, pinned: false },
				{ displayName: "URA_STATUS_OIT_FIXO", field: "ura_status_oit_fixo", width: 160, pinned: false },
				{ displayName: "URA_STATUS_OIT_BL", field: "ura_status_oit_bl", width: 160, pinned: false },
				{ displayName: "URA_STATUS_OIT_TV", field: "ura_status_oit_tv", width: 160, pinned: false },
				{ displayName: "URA_TIPOBLOQUEIO", field: "ura_tipo_bloqueio", width: 160, pinned: false },
				{ displayName: "URA_CIDADE", field: "ura_cidade", width: 160, pinned: false },
				{ displayName: "URA_DDD", field: "ura_ddd", width: 160, pinned: false },
				{ displayName: "URA_BERCARIO", field: "ura_bercario", width: 160, pinned: false },
				{ displayName: "URA_MAILING", field: "ura_mailing", width: 160, pinned: false },
				{ displayName: "URA_ST_CREP", field: "ura_st_crep", width: 160, pinned: false },
				{ displayName: "URA_SAC_CREP", field: "ura_sac_crep", width: 160, pinned: false },
				{ displayName: "URA_TAGRECVOZ", field: "ura_tagrecvoz", width: 160, pinned: false },
				{ displayName: "URA_RECVOZ", field: "ura_recvoz", width: 160, pinned: false }
			);
		}
				
		array.push(
			{ displayName: "extendedResponseServicoDestino", field: "service", width: 160, pinned: false },
			{ displayName: "companyName", field: "company", width: 160, pinned: false },
			{ displayName: "selectedDestAddress", field: "address", width: 160, pinned: false },
			{ displayName: "extendedResponseServicoDestino_2", field: "service_2", width: 160, pinned: false },
			{ displayName: "companyName_2", field: "company_2", width: 160, pinned: false },
			{ displayName: "selectedDestAddress_2", field: "address_2", width: 160, pinned: false },
			{ displayName: "horaIni", field: "horaIni", width: 120, pinned: false },
			{ displayName: "horaFim", field: "horaFim", width: 120, pinned: false }
		);
		
		if ($('.filtro-regras2').val() === "ContingenciaOiTotal_Homg"){
			array.push(
				{ displayName: "URA_CLIENTE_FIBRA", field: "ura_cliente_fibra", width: 160, pinned: false }
			);
		}
	  
      return array;
    }

      $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-contingencia-routing");
	  
	  $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
      });
	  
	  $view.on("click", "#contingenciaInput", function () {
            files = document.getElementById('contingenciaInput');	
		if (files.addEventListener) files.addEventListener('change', handleFile, false);
		document.getElementById('contingenciaInput').value = "";
	  
      });
	  
	  
	  $view.find("select.filtro-regras2").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} regras',
			showSubtext: true
      });

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

	  $('span.btnSearch') && $('span.btnSearch').hide();
	  

	 

     
	$scope.novo = function(){
		var regras = $('.filtro-regras2').val();
		$scope.linhaIdGeral = "";
		$("input[type=text]").val("");
		$('.alert-container').css('display','none');
		switch (regras){
			case 'ContingenciaFixoVelox' : 
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}	
				break;
			case 'ContingenciaPos' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}
				break;
			case 'ContingenciaPre' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}
				break;
			case 'ContingenciaOiTV' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}
				break;
			case 'ContingenciaOiTotal_Homg' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaTelevendas' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}	
				break;
			case 'ContingenciaCorporativo' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaCadastro' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaOct' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaEmpresarial' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaOiInternet' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			case 'ContingenciaUraFibra' :
				$('.grid').css('display','none');
				for (var i = 0; i < paineis.length; i++){
					$('#painel'+paineis[i]+'').css('display',(regras.split('Contingencia')[1] === paineis[i] ? 'block' : 'none'));			
				}		
				break;
			default:
				alert('Selecione uma tabela de contingência para criar uma nova regra');
				return;
		}
		
	}
    
	$scope.voltar = function(linha){
		for (var i = 0; i < paineis.length; i++){
			$('#painel'+paineis[i]+'').css('display','none');			
		}		
		$('.grid').css('display','block');
		$('.alert-container').css('display','block');
	};
		
	
	$scope.editar = function(linha){
		
		$scope.novo();
		$scope.linhaIdGeral = linha.id;
		console.log("Escreve linha"+linha.editar+" "+$('.filtro-regras2').val());
		var regras = $('.filtro-regras2').val();
		
		switch (regras){
			case 'ContingenciaFixoVelox' : 
				$('#IdFixo').val(linha.id);
				$('#Ura_ProdutoFixo').val(linha.ura_produto);
				$('#Ura_AssuntoFixo').val(linha.ura_assunto);
				$('#Ura_DDDFixo').val(linha.ura_ddd);
				$('#Final_DigitoFixo').val(linha.final_digito);
				$('#Ura_Mailing_R2Fixo').val(linha.ura_mailing);
				$('#extendResponseServicoDestinoFixo').val(linha.service);
				$('#companyNameFixo').val(linha.company);
				$('#selectedDestAddressFixo').val(linha.address);
				$('#extendResponseServicoDestinoFixo_2').val(linha.service_2);
				$('#companyNameFixo_2').val(linha.company_2);
				$('#selectedDestAddressFixo_2').val(linha.address_2);
				$('#horaIniFixo').val(linha.horaIni);
				$('#horaFimFixo').val(linha.horaFim);
		
				break;
			case 'ContingenciaPos' :
				$('#IdPos').val(linha.id);
				$('#Ura_PontoderivacaoPos').val(linha.ura_pontoderivacao);
				$('#Ura_DDDPos').val(linha.ura_ddd);
				$('#Final_DigitoPos').val(linha.final_digito);
				$('#Ura_Vclientejornalista').val(linha.ura_vclientejornalista);
				$('#Ura_IsflagfraudePos').val(linha.ura_isflagfraude);
				$('#Ura_Isoct').val(linha.ura_isoct);
				$('#Ura_Ispospago').val(linha.ura_ispospago);
				$('#Ura_Isclienteoct4p').val(linha.ura_isclienteoct4p);
				$('#Is_Contaemaberto_Entreqtddias').val(linha.is_contaemaberto_entreqtddias);
				$('#Possuicontaemabertoentrexdias').val(linha.possuicontaemabertoentrexdias);
				$('#Ura_Isosctprofissionalpj').val(linha.ura_isosctprofissionalpj);
				$('#Ura_Isoctprofissionalpf').val(linha.ura_isoctprofissionalpf);
				$('#Ura_Isoivelox3g').val(linha.ura_isoivelox3g);
				$('#Ura_Nome_Plano').val(linha.ura_nome_plano);
				$('#Ura_Lastdialog').val(linha.ura_lastdialog);
				$('#Ura_MailingPos').val(linha.ura_mailing);
				$('#extendResponseServicoDestinoPos').val(linha.service);
				$('#companyNamePos').val(linha.company);
				$('#selectedDestAddressPos').val(linha.address);
				$('#extendResponseServicoDestinoPos_2').val(linha.service_2);
				$('#companyNamePos_2').val(linha.company_2);
				$('#selectedDestAddressPos_2').val(linha.address_2);
				$('#horaIniPos').val(linha.horaIni);
				$('#horaFimPos').val(linha.horaFim);
				if (linha.horaIni != null) {$('#checkPos').trigger('click') } else {  $('#checkPos').removeAttr('checked')} 
				//$scope.rotear('#checkPos','#cntg-pos')
				
				break;
			case 'ContingenciaPre' :
				$('#IdPre').val(linha.id);
				$('#Ura_PontoderivacaoPre').val(linha.ura_pontoderivacao);
				$('#Ura_DDDPre').val(linha.ura_ddd);
				$('#Final_DigitoPre').val(linha.final_digito);				
				$('#Ura_IsflagfraudePre').val(linha.ura_isflagfraude);
				$('#Ura_Isvip').val(linha.ura_isvip);
				$('#Ura_Segmentovalor').val(linha.ura_segmentovalor);
				$('#Ura_Isoicontrole').val(linha.ura_isoicontrole);
				$('#Ura_Isscoreanatel').val(linha.ura_isscoreanatel);
				$('#Ura_Urawelcome').val(linha.ura_welcome);
				$('#Ura_Ism4u').val(linha.ura_ism4u);
				$('#Ura_Isdigito').val(linha.ura_isdigito);
				$('#Isdemandaativa').val(linha.ura_isdemandaativa);
				$('#Isnid').val(linha.ura_isnid);
				$('#Isnovooicontrole').val(linha.ura_isnovooicontrole);
				$('#Isprepago').val(linha.ura_isprepago);
				$('#Ura_Promocao').val(linha.ura_promocao);
				$('#Ura_Isoimod').val(linha.ura_isoimod);			
				$('#extendResponseServicoDestinoPre').val(linha.service);
				$('#companyNamePre').val(linha.company);
				$('#selectedDestAddressPre').val(linha.address);
				$('#extendResponseServicoDestinoPre_2').val(linha.service_2);
				$('#companyNamePre_2').val(linha.company_2);
				$('#selectedDestAddressPre_2').val(linha.address_2);
				$('#horaIniPre').val(linha.horaIni);
				$('#horaFimPre').val(linha.horaFim);
				
				break;
			case 'ContingenciaOiTV' :
				$('#IdTV').val(linha.id);
				$('#Ura_ProdutoTV').val(linha.ura_produto);
				$('#Ura_AssuntoTV').val(linha.ura_assunto);
				$('#Ura_DDDTV').val(linha.ura_ddd);
				$('#Final_DigitoTV').val(linha.final_digito);
				$('#Ura_MailingTV').val(linha.ura_mailing);
				$('#extendResponseServicoDestinoTV').val(linha.service);
				$('#companyNameTV').val(linha.company);
				$('#selectedDestAddressTV').val(linha.address);
				$('#extendResponseServicoDestinoTV_2').val(linha.service_2);
				$('#companyNameTV_2').val(linha.company_2);
				$('#selectedDestAddressTV_2').val(linha.address_2);
				$('#horaIniTV').val(linha.horaIni);
				$('#horaFimTV').val(linha.horaFim);
		
				break;
			case 'ContingenciaOiTotal_Homg' :
				$('#IdTotal').val(linha.id);
				$('#Ura_ProdutoTotal').val(linha.ura_produto);
				$('#Ura_AssuntoTotal').val(linha.ura_assunto);
				$('#Ura_PlanoTotal').val(linha.ura_plano);
				$('#Ura_DDDTotal').val(linha.ura_ddd);
				$('#Final_DigitoTotal').val(linha.final_digito);
				$('#Ura_MailingTotal').val(linha.ura_mailing);
				$('#extendResponseServicoDestinoTotal').val(linha.service);
				$('#companyNameTotal').val(linha.company);
				$('#selectedDestAddressTotal').val(linha.address);
				$('#extendResponseServicoDestinoTotal_2').val(linha.service_2);
				$('#companyNameTotal_2').val(linha.company_2);
				$('#selectedDestAddressTotal_2').val(linha.address_2);
				$('#horaIniTotal').val(linha.horaIni);
				$('#horaFimTotal').val(linha.horaFim);
				$('#Ura_ClienteFibraTotal').val(linha.ura_cliente_fibra);
				if (linha.horaIni != null) {$('#checkTotal').trigger('click') } else {  $('#checkTotal').removeAttr('checked')} 
				//$scope.rotear('#checkTotal','#cntg-total')
				break;
			case 'ContingenciaTelevendas' :
				$('#IdTelevendas').val(linha.id);
				$('#Ura_Menu_Origem').val(linha.ura_menu_origem);
				$('#Ura_Canal_De_Entrada').val(linha.ura_canal_de_entrada);				
				$('#Ura_DDDTelevendas').val(linha.ura_ddd);
				$('#Final_DigitoTelevendas').val(linha.final_digito);			
				$('#extendResponseServicoDestinoTelevendas').val(linha.service);
				$('#companyNameTelevendas').val(linha.company);
				$('#selectedDestAddressTelevendas').val(linha.address);
				$('#extendResponseServicoDestinoTelevendas_2').val(linha.service_2);
				$('#companyNameTelevendas_2').val(linha.company_2);
				$('#selectedDestAddressTelevendas_2').val(linha.address_2);
				$('#horaIniTelevendas').val(linha.horaIni);
				$('#horaFimTelevendas').val(linha.horaFim);
		
				break;
			case 'ContingenciaCorporativo' :
				$('#IdCorporativo').val(linha.id);
				$('#Ura_ProdutoCorporativo').val(linha.ura_produto);
				$('#Ura_AssuntoCorporativo').val(linha.ura_assunto);
				$('#Ura_DDDCorporativo').val(linha.ura_ddd);				
				$('#Ura_MailingCorporativo').val(linha.ura_mailing);
				$('#Tel_FixoCorporativo').val(linha.tel_fixo);
				$('#VeloxCorporativo').val(linha.velox);
				$('#Ura_CarteiraCorporativo').val(linha.ura_carteira);
				$('#Tipo_ClienteCorporativo').val(linha.tipo_cliente);
				$('#extendResponseServicoDestinoCorporativo').val(linha.service);
				$('#companyNameCorporativo').val(linha.company);
				$('#selectedDestAddressCorporativo').val(linha.address);
				$('#extendResponseServicoDestinoCorporativo_2').val(linha.service_2);
				$('#companyNameCorporativo_2').val(linha.company_2);
				$('#selectedDestAddressCorporativo_2').val(linha.address_2);
				$('#horaIniCorporativo').val(linha.horaIni);
				$('#horaFimCorporativo').val(linha.horaFim);
			
				break;
			case 'ContingenciaCadastro' :
				$('#IdCadastro').val(linha.id);
				$('#Ura_CanaldeentradaCadastro').val(linha.ura_canal_de_entrada);
				$('#Ura_TipochipCadastro').val(linha.ura_tipo_chip);
				$('#Ura_DDDCadastro').val(linha.ura_ddd);				
				$('#FinaldigitoCadastro').val(linha.final_digito);
				$('#Ura_PlanoCadastro').val(linha.ura_plano);
				$('#Ura_NomeplanoCadastro').val(linha.nome_plano);
				$('#AtendimentobilingueCadastro').val(linha.atendimento_bilingue);
				$('#MsidnCadastro').val(linha.MSIDN);
				$('#Chamadasrepetidas24horas').val(linha.CHAMADASREPETIDA_24HORAS);
				$('#extendResponseServicoDestinoCadastro').val(linha.service);
				$('#companyNameCadastro').val(linha.company);
				$('#selectedDestAddressCadastro').val(linha.address);
				$('#extendResponseServicoDestinoCadastro_2').val(linha.service_2);
				$('#companyNameCadastro_2').val(linha.company_2);
				$('#selectedDestAddressCadastro_2').val(linha.address_2);
				$('#horaIniCadastro').val(linha.horaIni);
				$('#horaFimCadastro').val(linha.horaFim);
			
				break;
			case 'ContingenciaOct' :
				$('#IdOct').val(linha.id);
				$('#Ura_Repetidacrep').val(linha.ura_repetidacrep);
				$('#Ura_Pilotocrep').val(linha.ura_pilotocrep);
				$('#Ura_Blacklistcrep').val(linha.ura_blacklistcrep);
				$('#Ura_Horariocrep').val(linha.ura_horariocrep);
				$('#Ura_assuntoOct').val(linha.ura_assunto);
				$('#URA_BDABERTODENTRODOPRAZO').val(linha.URA_BDABERTODENTRODOPRAZO);
				$('#URA_BDABERTOFORADOPRAZO').val(linha.URA_BDABERTOFORADOPRAZO);
				$('#URA_BDVELOXABERTODENTRODOPRAZO').val(linha.URA_BDVELOXABERTODENTRODOPRAZO);
				$('#URA_BDVELOXABERTOFORADOPRAZO').val(linha.URA_BDVELOXABERTOFORADOPRAZO);
				$('#URA_QUARENTENACREP').val(linha.URA_QUARENTENACREP);
				$('#URA_FRAUDE').val(linha.URA_FRAUDE);
				$('#URA_TIPOBLOQUEIO').val(linha.URA_TIPOBLOQUEIO);
				$('#URA_WELCOME').val(linha.URA_WELCOME);
				$('#URA_CONTA_EM_ABERTO').val(linha.URA_CONTA_EM_ABERTO);
				$('#URA_SOLICITACAO').val(linha.URA_SOLICITACAO);
				$('#Ura_mailing').val(linha.ura_mailing);
				$('#Ura_produto').val(linha.ura_produto);
				$('#URA_TELEFONE_TRATADO').val(linha.URA_TELEFONE_TRATADO);
				$('#URA_VIP').val(linha.URA_VIP);
				$('#URA_RESGATE_ANATEL').val(linha.URA_RESGATE_ANATEL);
				$('#URA_TRN_ID').val(linha.URA_TRN_ID);
				$('#URA_USOUAPP12MESES').val(linha.URA_USOUAPP12MESES);
				$('#URA_USOUAPP20MIN').val(linha.URA_USOUAPP20MIN);
				$('#URA_EVENTO').val(linha.URA_EVENTO);
				$('#Ura_dddOct').val(linha.ura_ddd);
				$('#Final_digitoOct').val(linha.final_digito);
				$('#extendResponseServicoDestinoOct').val(linha.service);
				$('#companyNameOct').val(linha.company);
				$('#selectedDestAddressOct').val(linha.address);
				$('#extendResponseServicoDestinoOct_2').val(linha.service_2);
				$('#companyNameOct_2').val(linha.company_2);
				$('#selectedDestAddressOct_2').val(linha.address_2);
				$('#horaIniOct').val(linha.horaIni);
				$('#horaFimOct').val(linha.horaFim);
			
				break;
			case 'ContingenciaEmpresarial' :
				$('#IdEmp').val(linha.id);
				$('#Ura_CanalEmp').val(linha.ura_canal);
				$('#Ura_ProdutoEmp').val(linha.ura_produto);
				$('#Ura_AssuntoEmp').val(linha.ura_assunto);
				$('#Ura_DDDEmp').val(linha.ura_ddd);
				$('#BloqueioEmp').val(linha.parametro_bloqueio_financeiro);
				$('#Ura_MailingEmp').val(linha.ura_mailing);				
				$('#Tel_FixoEmp').val(linha.tel_fixo);
				$('#VeloxEmp').val(linha.velox);
				$('#RentabilizacaoEmp').val(linha.rentabilizacao_empresarial);
				$('#ConsultorEmp').val(linha.consultor_empresarial);
				$('#extendResponseServicoDestinoEmp').val(linha.service);
				$('#companyNameEmp').val(linha.company);
				$('#selectedDestAddressEmp').val(linha.address);
				$('#extendResponseServicoDestinoEmp_2').val(linha.service_2);
				$('#companyNameEmp_2').val(linha.company_2);
				$('#selectedDestAddressEmp_2').val(linha.address_2);
				$('#horaIniEmp').val(linha.horaIni);
				$('#horaFimEmp').val(linha.horaFim);
			
				break;						
			case 'ContingenciaOiInternet' :
				$('#IdOiInternet').val(linha.id);
				$('#Ura_CanalOiInternet').val(linha.ura_canal);
				$('#Ura_AssuntoOiInternet').val(linha.ura_assunto);
				$('#Ura_SegmentoOiInternet').val(linha.ura_segmento);
				$('#Ura_DDDOiInternet').val(linha.ura_ddd);
				$('#Ura_PlanoOiInternet').val(linha.ura_plano);
				$('#Ura_TipoBloqueioOiInternet').val(linha.ura_tipo_bloqueio);
				$('#Ura_MailingOiInternet').val(linha.ura_mailing);
				$('#Ura_ModemAlinhadoOiInternet').val(linha.ura_modem_alinhado);
				$('#extendResponseServicoDestinoOiInternet').val(linha.service);
				$('#companyNameOiInternet').val(linha.company);
				$('#selectedDestAddressOiInternet').val(linha.address);
				$('#extendResponseServicoDestinoOiInternet_2').val(linha.service_2);
				$('#companyNameOiInternet_2').val(linha.company_2);
				$('#selectedDestAddressOiInternet_2').val(linha.address_2);
				$('#horaIniOiInternet').val(linha.horaIni);
				$('#horaFimOiInternet').val(linha.horaFim);
			
				break;
			case 'ContingenciaUraFibra' :
				$('#IdUraFibra').val(linha.id);
				$('#Ura_CanalUraFibra').val(linha.ura_canal);
				$('#Ura_PlanoUraFibra').val(linha.ura_plano);
				$('#Ura_ProdutoUraFibra').val(linha.ura_produto);
				$('#Ura_TipoplanoUraFibra').val(linha.ura_tipoplano);
				$('#Ura_SegmentoUraFibra').val(linha.ura_segmento);
				$('#Ura_PontoderivacaoUraFibra').val(linha.ura_pontoderivacao);
				$('#Ura_AssuntoUraFibra').val(linha.ura_assunto);				
				$('#Ura_StatusAloneUraFibra').val(linha.ura_status_alone);
				$('#Ura_StatusOitFixoUraFibra').val(linha.ura_status_oit_fixo);
				$('#Ura_StatusOitBlUraFibra').val(linha.ura_status_oit_bl);
				$('#Ura_StatusOitTvUraFibra').val(linha.ura_status_oit_tv);
				$('#Ura_TipoBloqueioUraFibra').val(linha.ura_tipo_bloqueio);
				$('#Ura_CidadeUraFibra').val(linha.ura_cidade);
				$('#Ura_DDDUraFibra').val(linha.ura_ddd);
				$('#Ura_BercarioUraFibra').val(linha.ura_bercario);
				$('#Ura_MailingUraFibra').val(linha.ura_mailing);
				$('#Ura_StCrepUraFibra').val(linha.ura_st_crep);
				$('#Ura_SacCrepUraFibra').val(linha.ura_sac_crep);
				$('#Ura_TagrecvozUraFibra').val(linha.ura_tagrecvoz);
				$('#Ura_RecvozUraFibra').val(linha.ura_recvoz);
				$('#extendResponseServicoDestinoUraFibra').val(linha.service);
				$('#companyNameUraFibra').val(linha.company);
				$('#selectedDestAddressUraFibra').val(linha.address);
				$('#extendResponseServicoDestinoUraFibra_2').val(linha.service_2);
				$('#companyNameUraFibra_2').val(linha.company_2);
				$('#selectedDestAddressUraFibra_2').val(linha.address_2);
				$('#horaIniUraFibra').val(linha.horaIni);
				$('#horaFimUraFibra').val(linha.horaFim);
			
				break;
			default:				
				return;
		}
		
		
	}		
	$scope.excluir = function(linha){
		
		var resp = confirm("Deseja mesmo excluir esta regra?");
		if (resp == true) {
			var tabela = $('.filtro-regras2').val();
			sql = "delete from " + tabela + " where id = " + linha.id;			
			console.log(sql);
			db.query(sql, null, function (err, num_rows) {
				console.log("Executando query-> " + sql + " " + num_rows);				
				if (err) {
					console.log("ERRO: " + err);
					alert("ERRO: " + err);
				} else {					
					alert("Regra excluída com sucesso");							
				}				
				$scope.listaDados();
			});
		} else {
			return;
		} 	
	}

	
	$scope.salvar = function(tipo){
		
		console.log(tipo)
		var preenchido = false;
		for (var i=0; i < $('#' + tipo + ' > div > input').length -1 ; i ++ ){
			if ($('#' + tipo + ' > div > input')[i].value != "") preenchido = true;
			if ($('#' +tipo + ' > div > input')[i].value == "" && preenchido){
				alert('Para o preenchimento das informações e empresa por período, é preciso preencher os 5 campos existentes.');
				return;
			}
				
		}
		var sql;
		var sqlLog="";
		var regras = $('.filtro-regras2').val();
		
		switch (regras){
			case 'ContingenciaFixoVelox' : 
				var horaIni = $('#horaIniFixo').val() == "" ? null :  "'" + $('#horaIniFixo').val() + "'";
				var horaFim = $('#horaFimFixo').val() == "" ? null :  "'" + $('#horaFimFixo').val() + "'";
				if ($('#IdFixo').val() !==""){
					
					sql = "update ContingenciaFixoVelox set ura_produto = '" + $('#Ura_ProdutoFixo').val() + "',selectedDestAddress = '" + $('#selectedDestAddressFixo').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDFixo').val() + "',ura_assunto = '" + $('#Ura_AssuntoFixo').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_DigitoFixo').val() + "',ura_mailing_r2 = '" + $('#Ura_Mailing_R2Fixo').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoFixo').val() + "',companyName = '" + $('#companyNameFixo').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ,";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoFixo_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameFixo_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressFixo_2').val() + "' ";
					sql = sql + " where id = " + $('#IdFixo').val();
					
				}else{					
					sql = "if not exists(SELECT * FROM ContingenciaFixoVelox"
                    sql = sql + " where ura_produto = '" + $('#Ura_ProdutoFixo').val() + "'"
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoFixo').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDFixo').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoFixo').val() + "'"
					sql = sql + " and ura_mailing_r2 = '" + $('#Ura_Mailing_R2Fixo').val() + "'"
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoFixo').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameFixo').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressFixo').val() + "')"
					sql = sql + " insert into ContingenciaFixoVelox(ura_produto,ura_assunto,"
					sql = sql + " ura_ddd,final_digito,ura_mailing_r2,extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_ProdutoFixo').val() + "','" + $('#Ura_AssuntoFixo').val() + "','" + $('#Ura_DDDFixo').val() + "','" + $('#Final_DigitoFixo').val() + "',"
					sql = sql + "'" + $('#Ura_Mailing_R2Fixo').val() + "','" + $('#extendResponseServicoDestinoFixo').val() + "','" + $('#companyNameFixo').val() + "',"
					sql = sql + "'" + $('#selectedDestAddressFixo').val() + "',";
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoFixo_2').val() + "',";
					sql = sql + "'" + $('#companyNameFixo_2').val() + "','" + $('#selectedDestAddressFixo_2').val() + "')"
					
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";					
						
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Produto: " + $('#Ura_ProdutoFixo').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoFixo').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDFixo').val() + ";";
					sqlLog = sqlLog + "Final Dígito: " + $('#Final_DigitoFixo').val() + ";";
					sqlLog = sqlLog + "Mailing: " + $('#Ura_Mailing_R2Fixo').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoFixo').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameFixo').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressFixo').val() + "','ContingenciaFixoVelox')";
				}
				break;
			case 'ContingenciaPos' :
				var horaIni = $('#horaIniPos').val() == "" ? null :  "'" + $('#horaIniPos').val() + "'";
				var horaFim = $('#horaFimPos').val() == "" ? null :  "'" + $('#horaFimPos').val() + "'";
				if ($('#IdPos').val() !==""){
					
					sql = "update ContingenciaPos set Ura_Pontoderivacao = '" + $('#Ura_PontoderivacaoPos').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDPos').val() + "',final_digito = '" + $('#Final_DigitoPos').val() + "',Ura_Vclientejornalista = '" + $('#Ura_Vclientejornalista').val() + "',";
					sql = sql + " Ura_Isflagfraude = '" + $('#Ura_IsflagfraudePos').val() + "',Ura_Isoct = '" + $('#Ura_Isoct').val() + "',";
					sql = sql + " Ura_Ispospago = '" + $('#Ura_Ispospago').val() + "',Ura_Isclienteoct4p = '" + $('#Ura_Isclienteoct4p').val() + "',";
					sql = sql + " Is_Contaemaberto_Entreqtddias = '" + $('#Is_Contaemaberto_Entreqtddias').val() + "',Possuicontaemabertoentrexdias = '" + $('#Possuicontaemabertoentrexdias').val() + "',";
					sql = sql + " Ura_Isosctprofissionalpj = '" + $('#Ura_Isosctprofissionalpj').val() + "',Ura_Isoctprofissionalpf = '" + $('#Ura_Isoctprofissionalpf').val() + "',";
					sql = sql + " Ura_Isoivelox3g = '" + $('#Ura_Isoivelox3g').val() + "',Ura_Nome_Plano = '" + $('#Ura_Nome_Plano').val() + "',";
					sql = sql + " Ura_Lastdialog = '" + $('#Ura_Lastdialog').val() + "',Ura_Mailing = '" + $('#Ura_MailingPos').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoPos').val() + "',companyName = '" + $('#companyNamePos').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressPos').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoPos_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNamePos_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressPos_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdPos').val();	
				}else{
					
					//Testa ponto e vírgula
					var uraPontoDerivacao = $('#Ura_PontoderivacaoPos').val().substring($('#Ura_PontoderivacaoPos').val().length-1,$('#Ura_PontoderivacaoPos').val().length) !== ";" ? $('#Ura_PontoderivacaoPos').val() + ";" : $('#Ura_PontoderivacaoPos').val();
					
					sql = "if not exists(SELECT * FROM ContingenciaPos"
                    sql = sql + " where ura_pontoderivacao = '" + uraPontoDerivacao + "'"				
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDPos').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoPos').val() + "'"
					sql = sql + " and Ura_Vclientejornalista ='" + $('#Ura_Vclientejornalista').val() + "'"
					sql = sql + " and Ura_Isflagfraude ='" + $('#Ura_IsflagfraudePos').val() + "'"
					sql = sql + " and Ura_Isoct = '" + $('#Ura_Isoct').val() + "'"
					sql = sql + " and Ura_Ispospago = '" + $('#Ura_Ispospago').val() + "'"
					sql = sql + " and Ura_Isclienteoct4p = '" + $('#Ura_Isclienteoct4p').val() + "'"				
					sql = sql + " and Is_Contaemaberto_Entreqtddias = '" + $('#Is_Contaemaberto_Entreqtddias').val() + "'"
					sql = sql + " and Possuicontaemabertoentrexdias = '" + $('#Possuicontaemabertoentrexdias').val() + "'"
					sql = sql + " and Ura_Isosctprofissionalpj = '" + $('#Ura_Isosctprofissionalpj').val() + "'"
					sql = sql + " and Ura_Isoctprofissionalpf = '" + $('#Ura_Isoctprofissionalpf').val() + "'"
					sql = sql + " and Ura_Isoivelox3g = '" + $('#Ura_Isoivelox3g').val() + "'"
					sql = sql + " and Ura_Nome_Plano = '" + $('#Ura_Nome_Plano').val() + "'"
					sql = sql + " and Ura_Lastdialog = '" + $('#Ura_Lastdialog').val() + "'"
					sql = sql + " and Ura_Mailing ='" + $('#Ura_MailingPos').val() + "'"
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoPos').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNamePos').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressPos').val() + "')"					
					sql = sql + " insert into ContingenciaPos(Ura_Pontoderivacao,Ura_DDD,Final_Digito,"
					sql = sql + " Ura_Vclientejornalista,Ura_Isflagfraude,Ura_Isoct,Ura_Ispospago,Ura_Isclienteoct4p,"
					sql = sql + " Is_Contaemaberto_Entreqtddias,Possuicontaemabertoentrexdias,Ura_Isosctprofissionalpj,"
					sql = sql + " Ura_Isoctprofissionalpf,Ura_Isoivelox3g,Ura_Nome_Plano,Ura_Lastdialog,Ura_Mailing,"
					sql = sql + " extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + uraPontoDerivacao + "','" + $('#Ura_DDDPos').val() + "','" + $('#Final_DigitoPos').val() + "','" + $('#Ura_Vclientejornalista').val() + "','" + $('#Ura_IsflagfraudePos').val() + "',"
					sql = sql + "'" + $('#Ura_Isoct').val() + "','" + $('#Ura_Ispospago').val() + "','" + $('#Ura_Isclienteoct4p').val() + "',"
					sql = sql + "'" + $('#Is_Contaemaberto_Entreqtddias').val() + "','" + $('#Possuicontaemabertoentrexdias').val() + "','" + $('#Ura_Isosctprofissionalpj').val() + "',"
					sql = sql + "'" + $('#Ura_Isoctprofissionalpf').val() + "','" + $('#Ura_Isoivelox3g').val() + "','" + $('#Ura_Nome_Plano').val() + "',"
					sql = sql + "'" + $('#Ura_Lastdialog').val() + "','" + $('#Ura_MailingPos').val() + "','" + $('#extendResponseServicoDestinoPos').val() + "',"
					sql = sql + "'" + $('#companyNamePos').val() + "','" + $('#selectedDestAddressPos').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoPos_2').val() + "',";
					sql = sql + "'" + $('#companyNamePos_2').val() + "','" + $('#selectedDestAddressPos_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";	
					sqlLog=";select getdate()";
				}
				
				break;
			case 'ContingenciaPre' :
				var horaIni = $('#horaIniPre').val() == "" ? null :  "'" + $('#horaIniPre').val() + "'";
				var horaFim = $('#horaFimPre').val() == "" ? null :  "'" + $('#horaFimPre').val() + "'";
				if ($('#IdPre').val() !==""){
					
					sql = "update ContingenciaPre set Ura_Pontoderivacao = '" + $('#Ura_PontoderivacaoPre').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDPre').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_DigitoPre').val() + "',";					
					sql = sql + " Ura_Isflagfraude = '" + $('#Ura_IsflagfraudePre').val() + "',";
					sql = sql + " ura_isvip = '" + $('#Ura_Isvip').val() + "',";
					sql = sql + " ura_segmentovalor = '" + $('#Ura_Segmentovalor').val() + "',";
					sql = sql + " ura_isoicontrole = '" + $('#Ura_Isoicontrole').val() + "',";
					sql = sql + " ura_isscoreanatel = '" + $('#Ura_Isscoreanatel').val() + "',";
					sql = sql + " ura_welcome = '" + $('#Ura_Urawelcome').val() + "',";
					sql = sql + " ura_ism4u = '" + $('#Ura_Ism4u').val() + "',";
					sql = sql + " ura_isdigito = '" + $('#Ura_Isdigito').val() + "',";
					sql = sql + " ura_isdemandaativa = '" + $('#Isdemandaativa').val() + "',";
					sql = sql + " ura_isnid = '" + $('#Isnid').val() + "',";
					sql = sql + " ura_isnovooicontrole = '" + $('#Isnovooicontrole').val() + "',";
					sql = sql + " ura_isprepago = '" + $('#Isprepago').val() + "',";
					sql = sql + " ura_promocao = '" + $('#Ura_Promocao').val() + "',";
					sql = sql + " ura_isoimod = '" + $('#Ura_Isoimod').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoPre').val() + "',";
					sql = sql + " companyName = '" + $('#companyNamePre').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressPre').val() + "',"; 
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoPre_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNamePre_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressPre_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdPre').val();	
				}else{
					
					//Testa ponto e vírgula
					var uraPontoDerivacao = $('#Ura_PontoderivacaoPre').val().substring($('#Ura_PontoderivacaoPre').val().length-1,$('#Ura_PontoderivacaoPre').val().length) !== ";" ? $('#Ura_PontoderivacaoPre').val() + ";" : $('#Ura_PontoderivacaoPre').val();
					
					sql = "if not exists(SELECT * FROM ContingenciaPre"
                    sql = sql + " where ura_pontoderivacao = '" + uraPontoDerivacao + "'"				
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDPre').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoPre').val() + "'"					
					sql = sql + " and Ura_Isflagfraude ='" + $('#Ura_IsflagfraudePre').val() + "'"
					sql = sql + " and ura_isvip = '" + $('#Ura_Isvip').val() + "'"
					sql = sql + " and ura_segmentovalor = '" + $('#Ura_Segmentovalor').val() + "'"
					sql = sql + " and ura_isoicontrole = '" + $('#Ura_Isoicontrole').val() + "'"				
					sql = sql + " and ura_isscoreanatel = '" + $('#Ura_Isscoreanatel').val() + "'"
					sql = sql + " and ura_welcome = '" + $('#Ura_Urawelcome').val() + "'"
					sql = sql + " and ura_ism4u = '" + $('#Ura_Ism4u').val() + "'"
					sql = sql + " and ura_isdigito = '" + $('#Ura_Isdigito').val() + "'"
					sql = sql + " and ura_isdemandaativa = '" + $('#Isdemandaativa').val() + "'"
					sql = sql + " and ura_isnid = '" + $('#Isnid').val() + "'"
					sql = sql + " and ura_isnovooicontrole = '" + $('#Isnovooicontrole').val() + "'"
					sql = sql + " and ura_isprepago ='" + $('#Isprepago').val() + "'"
					sql = sql + " and ura_promocao = '" + $('#Ura_Promocao').val() + "'"
					sql = sql + " and ura_isoimod = '" + $('#Ura_Isoimod').val() + "'"
					sql = sql + " and extendedResponseServicoDestino = '" + $('#extendedResponseServicoDestinoPre').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNamePre').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressPre').val() + "')"					
					sql = sql + " insert into ContingenciaPre(Ura_Pontoderivacao,Ura_DDD,Final_Digito,"
					sql = sql + " Ura_Isflagfraude,ura_isvip,ura_segmentovalor,ura_isoicontrole,"
					sql = sql + " ura_isscoreanatel,ura_welcome,ura_ism4u,"
					sql = sql + " ura_isdigito,ura_isdemandaativa,ura_isnid,ura_isnovooicontrole,ura_isprepago,"
					sql = sql + " ura_promocao,ura_isoimod,"
					sql = sql + " extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + uraPontoDerivacao + "',"
					sql = sql + "'" + $('#Ura_DDDPre').val() + "',"
					sql = sql + "'" + $('#Final_DigitoPre').val() + "',"
					sql = sql + "'" + $('#Ura_IsflagfraudePre').val() + "',"
					sql = sql + "'" + $('#Ura_Isvip').val() + "',"
					sql = sql + "'" + $('#Ura_Segmentovalor').val() + "',"
					sql = sql + "'" + $('#Ura_Isoicontrole').val() + "',"
					sql = sql + "'" + $('#Ura_Isscoreanatel').val() + "',"
					sql = sql + "'" + $('#Ura_Urawelcome').val() + "',"
					sql = sql + "'" + $('#Ura_Ism4u').val() + "',"
					sql = sql + "'" + $('#Ura_Isdigito').val() + "',"
					sql = sql + "'" + $('#Isdemandaativa').val() + "',"
					sql = sql + "'" + $('#Isnid').val() + "',"
					sql = sql + "'" + $('#Isnovooicontrole').val() + "',"
					sql = sql + "'" + $('#Isprepago').val() + "',"
					sql = sql + "'" + $('#Ura_Promocao').val() + "',"
					sql = sql + "'" + $('#Ura_Isoimod').val() + "',"
					sql = sql + "'" + $('#extendResponseServicoDestinoPre').val() + "',"
					sql = sql + "'" + $('#companyNamePre').val() + "',"
					sql = sql + "'" + $('#selectedDestAddressPre').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoPre_2').val() + "',";
					sql = sql + "'" + $('#companyNamePre_2').val() + "','" + $('#selectedDestAddressPre_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";	
					sqlLog=";select getdate()";
				}
				
				break;
			case 'ContingenciaOiTV' :
				var horaIni = $('#horaIniTV').val() == "" ? null :  "'" + $('#horaIniTV').val() + "'";
				var horaFim = $('#horaFimTV').val() == "" ? null :  "'" + $('#horaFimTV').val() + "'";
				if ($('#IdTV').val() !==""){
					
					sql = "update ContingenciaOiTV set ura_produto = '" + $('#Ura_ProdutoTV').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDTV').val() + "',ura_assunto = '" + $('#Ura_AssuntoTV').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_DigitoTV').val() + "',ura_mailing = '" + $('#Ura_MailingTV').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoTV').val() + "',companyName = '" + $('#companyNameTV').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressTV').val() + "' ,";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoTV_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameTV_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressTV_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdTV').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaOiTV"
                    sql = sql + " where ura_produto = '" + $('#Ura_ProdutoTV').val() + "'"
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoTV').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDTV').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoTV').val() + "'"
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingTV').val() + "'"
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoTV').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameTV').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressTV').val() + "')"					
					sql = sql + " insert into ContingenciaOiTV(ura_produto,ura_assunto,"
					sql = sql + " ura_ddd,final_digito,ura_mailing,extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_ProdutoTV').val() + "','" + $('#Ura_AssuntoTV').val() + "','" + $('#Ura_DDDTV').val() + "','" + $('#Final_DigitoTV').val() + "',"
					sql = sql + "'" + $('#Ura_MailingTV').val() + "','" + $('#extendResponseServicoDestinoTV').val() + "','" + $('#companyNameTV').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressTV').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoTV_2').val() + "',";
					sql = sql + "'" + $('#companyNameTV_2').val() + "','" + $('#selectedDestAddressTV_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Produto: " + $('#Ura_ProdutoTV').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoTV').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDTV').val() + ";";
					sqlLog = sqlLog + "Final Dígito: " + $('#Final_DigitoTV').val() + ";";
					sqlLog = sqlLog + "Mailing: " + $('#Ura_MailingTV').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoTV').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameTV').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressTV').val() + "','ContingenciaOiTV')";
				}
				break;
			case 'ContingenciaOiTotal_Homg' :
				var horaIni = $('#horaIniTotal').val() == "" ? null :  "'" + $('#horaIniTotal').val() + "'";
				var horaFim = $('#horaFimTotal').val() == "" ? null :  "'" + $('#horaFimTotal').val() + "'";
				if ($('#IdTotal').val() !==""){
					
					sql = "update ContingenciaOiTotal_Homg set ura_produto = '" + $('#Ura_ProdutoTotal').val() + "',ura_plano = '" + $('#Ura_PlanoTotal').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDTotal').val() + "',ura_assunto = '" + $('#Ura_AssuntoTotal').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_DigitoTotal').val() + "',ura_mailing = '" + $('#Ura_MailingTotal').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoTotal').val() + "',companyName = '" + $('#companyNameTotal').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressTotal').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoTotal_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameTotal_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressTotal_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ,";
					sql = sql + " ura_cliente_fibra = '" + $('#Ura_ClienteFibraTotal').val() + "'";
					sql = sql + " where id = " + $('#IdTotal').val();	
				}else{
					
					//Testa ponto e vírgula
					var uraPlanoTotal = $('#Ura_PlanoTotal').val().substring($('#Ura_PlanoTotal').val().length-1,$('#Ura_PlanoTotal').val().length) !== ";" ? $('#Ura_PlanoTotal').val() + ";" : $('#Ura_PlanoTotal').val();
					
					sql = "if not exists(SELECT * FROM ContingenciaOiTotal_Homg"
                    sql = sql + " where ura_produto = '" + $('#Ura_ProdutoTotal').val() + "'"
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoTotal').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDTotal').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoTotal').val() + "'"
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingTotal').val() + "'"
					sql = sql + " and extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoTotal').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameTV').val() + "'"						
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressTotal').val() + "'"	
					sql = sql + " and ura_plano = '" + uraPlanoTotal + "'"
					sql = sql + " and ura_cliente_fibra = '" + $('#Ura_ClienteFibraTotal').val() + "')"	
					sql = sql + " insert into ContingenciaOiTotal_Homg (ura_produto,ura_assunto,"
					sql = sql + " ura_ddd,final_digito,ura_mailing,extendedResponseServicoDestino,companyName,selectedDestAddress,ura_plano,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2,ura_cliente_fibra)"
					sql = sql + " values ('" + $('#Ura_ProdutoTotal').val() + "','" + $('#Ura_AssuntoTotal').val() + "','" + $('#Ura_DDDTotal').val() + "','" + $('#Final_DigitoTotal').val() + "',"
					sql = sql + "'" + $('#Ura_MailingTotal').val() + "','" + $('#extendResponseServicoDestinoTotal').val() + "','" + $('#companyNameTotal').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressTotal').val() + "','" + uraPlanoTotal + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoTotal_2').val() + "',";
					sql = sql + "'" + $('#companyNameTotal_2').val() + "','" + $('#selectedDestAddressTotal_2').val() + "',"
					sql = sql + "'" + $('#Ura_ClienteFibraTotal').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Produto: " + $('#Ura_ProdutoTotal').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoTotal').val() + ";";
					sqlLog = sqlLog + "Plano: " + $('#Ura_PlanoTotal').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDTotal').val() + ";";
					sqlLog = sqlLog + "Final Dígito: " + $('#Final_DigitoTotal').val() + ";";
					sqlLog = sqlLog + "Mailing: " + $('#Ura_MailingTotal').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoTotal').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameTotal').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressTotal').val() + ";";
					sqlLog = sqlLog + "Fibra: " + $('#Ura_ClienteFibraTotal').val() + "','ContingenciaOiTotal')";
					
				}
								
				break;
			case 'ContingenciaTelevendas' :
				var horaIni = $('#horaIniTelevendas').val() == "" ? null :  "'" + $('#horaIniTelevendas').val() + "'";
				var horaFim = $('#horaFimTelevendas').val() == "" ? null :  "'" + $('#horaFimTelevendas').val() + "'";
				if ($('#IdTelevendas').val() !==""){
					
					sql = "update ContingenciaTelevendas set ura_menu_origem = '" + $('#Ura_Menu_Origem').val() + "',ura_canal_de_entrada = '" + $('#Ura_Canal_De_Entrada').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDTelevendas').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_DigitoTelevendas').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoTelevendas').val() + "',companyName = '" + $('#companyNameTelevendas').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressTelevendas').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoTelevendas_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameTelevendas_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressTelevendas_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdTelevendas').val();	
				}else{
					
					//Testa ponto e vírgula
					var uraPlanoTotal = $('#Ura_PlanoTotal').val().substring($('#Ura_PlanoTotal').val().length-1,$('#Ura_PlanoTotal').val().length) !== ";" ? $('#Ura_PlanoTotal').val() + ";" : $('#Ura_PlanoTotal').val();
					
					sql = "if not exists(SELECT * FROM ContingenciaTelevendas"
                    sql = sql + " where ura_menu_origem = '" + $('#Ura_Menu_Origem').val() + "'"
					sql = sql + " and ura_canal_de_entrada ='" + $('#Ura_Canal_De_Entrada').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDTelevendas').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_DigitoTelevendas').val() + "'"					
					sql = sql + " and extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoTelevendas').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameTelevendas').val() + "'"						
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressTelevendas').val() + "')"					
					sql = sql + " insert into ContingenciaTelevendas(ura_menu_origem,ura_canal_de_entrada,"
					sql = sql + " ura_ddd,final_digito,extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_Menu_Origem').val() + "','" + $('#Ura_Canal_De_Entrada').val() + "','" + $('#Ura_DDDTelevendas').val() + "','" + $('#Final_DigitoTelevendas').val() + "',"
					sql = sql + "'" + $('#extendResponseServicoDestinoTelevendas').val() + "','" + $('#companyNameTelevendas').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressTelevendas').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoTelevendas_2').val() + "',";
					sql = sql + "'" + $('#companyNameTelevendas_2').val() + "','" + $('#selectedDestAddressTelevendas_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Origem: " + $('#Ura_Menu_Origem').val() + ";";
					sqlLog = sqlLog + "Canal: " + $('#Ura_Canal_De_Entrada').val() + ";";					
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDTelevendas').val() + ";";
					sqlLog = sqlLog + "Final Dígito: " + $('#Final_DigitoTelevendas').val() + ";";					
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoTelevendas').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameTelevendas').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressTelevendas').val() + "','ContingenciaTelevendas')";
				}
								
				break;
			case 'ContingenciaCorporativo' :
				var horaIni = $('#horaIniCorporativo').val() == "" ? null :  "'" + $('#horaIniCorporativo').val() + "'";
				var horaFim = $('#horaFimCorporativo').val() == "" ? null :  "'" + $('#horaFimCorporativo').val() + "'";
				if ($('#IdCorporativo').val() !==""){
					
					sql = "update ContingenciaCorporativo set ura_produto = '" + $('#Ura_ProdutoCorporativo').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDCorporativo').val() + "',ura_assunto = '" + $('#Ura_AssuntoCorporativo').val() + "',";
					sql = sql + " tel_fixo = '" + $('#Tel_FixoCorporativo').val() + "',ura_mailing = '" + $('#Ura_MailingCorporativo').val() + "',";
					sql = sql + " velox = '" + $('#VeloxCorporativo').val() + "',ura_carteira = '" + $('#Ura_CarteiraCorporativo').val() + "',";
					sql = sql + " tipo_cliente = '" + $('#Tipo_ClienteCorporativo').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoCorporativo').val() + "',companyName = '" + $('#companyNameCorporativo').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressCorporativo').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoCorporativo_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameCorporativo_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressCorporativo_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdCorporativo').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaCorporativo"
                    sql = sql + " where ura_produto = '" + $('#Ura_ProdutoCorporativo').val() + "'"
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoCorporativo').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDCorporativo').val() + "'"
					sql = sql + " and tel_fixo = '" + $('#Tel_FixoCorporativo').val() + "'"
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingCorporativo').val() + "'"
					sql = sql + " and velox = '" + $('#VeloxCorporativo').val() + "'"
					sql = sql + " and ura_carteira = '" + $('#Ura_CarteiraCorporativo').val() + "'"
					sql = sql + " and tipo_cliente = '" + $('#Tipo_ClienteCorporativo').val() + "'"
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoCorporativo').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameCorporativo').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressCorporativo').val() + "')"					
					sql = sql + " insert into ContingenciaCorporativo(ura_produto,ura_assunto,"
					sql = sql + " ura_ddd,tel_fixo,ura_mailing,velox,ura_carteira,tipo_cliente,extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_ProdutoCorporativo').val() + "','" + $('#Ura_AssuntoCorporativo').val() + "','" + $('#Ura_DDDCorporativo').val() + "','" + $('#Tel_FixoCorporativo').val() + "',"
					sql = sql + "'" + $('#VeloxCorporativo').val() + "','" + $('#Ura_CarteiraCorporativo').val() + "','" + $('#Tipo_ClienteCorporativo').val() + "',"
					sql = sql + "'" + $('#Ura_MailingCorporativo').val() + "','" + $('#extendResponseServicoDestinoCorporativo').val() + "','" + $('#companyNameCorporativo').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressCorporativo').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoCorporativo_2').val() + "',";
					sql = sql + "'" + $('#companyNameCorporativo_2').val() + "','" + $('#selectedDestAddressCorporativo_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Produto: " + $('#Ura_ProdutoCorporativo').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoCorporativo').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDCorporativo').val() + ";";
					sqlLog = sqlLog + "Tel fixo: " + $('#Tel_FixoCorporativo').val() + ";";
					sqlLog = sqlLog + "Mailing: " + $('#Ura_MailingCorporativo').val() + ";";
					sqlLog = sqlLog + "Velox: " + $('#VeloxCorporativo').val() + ";";
					sqlLog = sqlLog + "Ura Carteira: " + $('#Ura_CarteiraCorporativo').val() + ";";
					sqlLog = sqlLog + "Tipo Cliente: " + $('#Tipo_ClienteCorporativo').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoCorporativo').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameCorporativo').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressCorporativo').val() + "','ContingenciaCorporativo')";
				}
				break;
			case 'ContingenciaCadastro' :
				var horaIni = $('#horaIniCadastro').val() == "" ? null :  "'" + $('#horaIniCadastro').val() + "'";
				var horaFim = $('#horaFimCadastro').val() == "" ? null :  "'" + $('#horaFimCadastro').val() + "'";
				if ($('#IdCadastro').val() !==""){
					
					sql = "update ContingenciaCadastro set ura_canal_de_entrada = '" + $('#Ura_CanaldeentradaCadastro').val() + "',";
					sql = sql + " ura_tipo_chip = '" + $('#Ura_TipochipCadastro').val() + "',ura_ddd = '" + $('#Ura_DDDCadastro').val() + "',";
					sql = sql + " final_digito = '" + $('#FinaldigitoCadastro').val() + "',ura_plano = '" + $('#Ura_PlanoCadastro').val() + "',";
					sql = sql + " nome_plano = '" + $('#Ura_NomeplanoCadastro').val() + "',atendimento_bilingue = '" + $('#AtendimentobilingueCadastro').val() + "',";
					sql = sql + " MSIDN = '" + $('#MsidnCadastro').val() + "',CHAMADASREPETIDA_24HORAS = '" + $('#Chamadasrepetidas24horas').val() + "',";					
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoCadastro').val() + "',companyName = '" + $('#companyNameCadastro').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressCadastro').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoCadastro_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameCadastro_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressCadastro_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdCadastro').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaCadastro"
                    sql = sql + " where ura_canal_de_entrada = '" + $('#Ura_CanaldeentradaCadastro').val() + "'"
					sql = sql + " and ura_tipo_chip ='" + $('#Ura_TipochipCadastro').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDCadastro').val() + "'"
					sql = sql + " and final_digito = '" + $('#FinaldigitoCadastro').val() + "'"
					sql = sql + " and ura_plano = '" + $('#Ura_PlanoCadastro').val() + "'"
					sql = sql + " and nome_plano = '" + $('#Ura_NomeplanoCadastro').val() + "'"
					sql = sql + " and atendimento_bilingue = '" + $('#AtendimentobilingueCadastro').val() + "'"
					sql = sql + " and MSIDN = '" + $('#MsidnCadastro').val() + "'"
					sql = sql + " and CHAMADASREPETIDA_24HORAS = '" + $('#Chamadasrepetidas24horas').val() + "'"					
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoCadastro').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameCadastro').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressCadastro').val() + "')"					
					sql = sql + " insert into ContingenciaCadastro(ura_canal_de_entrada,"
					sql = sql + " ura_tipo_chip,"
					sql = sql + " ura_ddd,"
					sql = sql + " final_digito,"
					sql = sql + " ura_plano,"
					sql = sql + " nome_plano,"
					sql = sql + " atendimento_bilingue,"
					sql = sql + " MSIDN,"
					sql = sql + " CHAMADASREPETIDA_24HORAS,"
					sql = sql + " extendedResponseServicoDestino,"
					sql = sql + " companyName,"
					sql = sql + " selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_CanaldeentradaCadastro').val() + "',"
					sql = sql + " '" + $('#Ura_TipochipCadastro').val() + "',"
					sql = sql + " '" + $('#Ura_DDDCadastro').val() + "',"
					sql = sql + " '" + $('#FinaldigitoCadastro').val() + "',"
					sql = sql + " '" + $('#Ura_PlanoCadastro').val() + "',"
					sql = sql + " '" + $('#Ura_NomeplanoCadastro').val() + "',"
					sql = sql + " '" + $('#AtendimentobilingueCadastro').val() + "',"
					sql = sql + " '" + $('#MsidnCadastro').val() + "',"
					sql = sql + " '" + $('#Chamadasrepetidas24horas').val() + "',"
					sql = sql + " '" + $('#extendResponseServicoDestinoCadastro').val() + "',"
					sql = sql + " '" + $('#companyNameCadastro').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressCadastro').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoCadastro_2').val() + "',";
					sql = sql + "'" + $('#companyNameCadastro_2').val() + "','" + $('#selectedDestAddressCadastro_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Canal: " + $('#Ura_CanaldeentradaCadastro').val() + ";";
					sqlLog = sqlLog + "Chip: " + $('#Ura_TipochipCadastro').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDCadastro').val() + ";";
					sqlLog = sqlLog + "Final: " + $('#FinaldigitoCadastro').val() + ";";
					sqlLog = sqlLog + "Plano: " + $('#Ura_PlanoCadastro').val() + ";";
					sqlLog = sqlLog + "Nome: " + $('#Ura_NomeplanoCadastro').val() + ";";
					sqlLog = sqlLog + "Bilingue: " + $('#AtendimentobilingueCadastro').val() + ";";
					sqlLog = sqlLog + "Msidn: " + $('#MsidnCadastro').val() + ";";
					sqlLog = sqlLog + "Repetidas: " + $('#Chamadasrepetidas24horas').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoCadastro').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameCadastro').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressCadastro').val() + "','ContingenciaCadastro')";
				}
				break;
			case 'ContingenciaOct' :
				var horaIni = $('#horaIniOct').val() == "" ? null :  "'" + $('#horaIniOct').val() + "'";
				var horaFim = $('#horaFimOct').val() == "" ? null :  "'" + $('#horaFimOct').val() + "'";
				if ($('#IdOct').val() !==""){
					
					sql = "update ContingenciaOct set ura_repetidacrep = '" + $('#Ura_Repetidacrep').val() + "',";
					sql = sql + " ura_pilotocrep = '" + $('#Ura_Pilotocrep').val() + "',";
					sql = sql + " ura_blacklistcrep = '" + $('#Ura_Blacklistcrep').val() + "',";					
					sql = sql + " ura_horariocrep = '" + $('#Ura_Horariocrep').val() + "',";
					sql = sql + " ura_assunto = '" + $('#Ura_assuntoOct').val() + "',";
					sql = sql + " URA_BDABERTODENTRODOPRAZO = '" + $('#URA_BDABERTODENTRODOPRAZO').val() + "',";
					sql = sql + " URA_BDABERTOFORADOPRAZO = '" + $('#URA_BDABERTOFORADOPRAZO').val() + "',";
					sql = sql + " URA_BDVELOXABERTODENTRODOPRAZO = '" + $('#URA_BDVELOXABERTODENTRODOPRAZO').val() + "',";
					sql = sql + " URA_BDVELOXABERTOFORADOPRAZO = '" + $('#URA_BDVELOXABERTOFORADOPRAZO').val() + "',";
					sql = sql + " URA_QUARENTENACREP = '" + $('#URA_QUARENTENACREP').val() + "',";
					sql = sql + " URA_FRAUDE = '" + $('#URA_FRAUDE').val() + "',";
					sql = sql + " URA_TIPOBLOQUEIO = '" + $('#URA_TIPOBLOQUEIO').val() + "',";
					sql = sql + " URA_WELCOME = '" + $('#URA_WELCOME').val() + "',";
					sql = sql + " URA_CONTA_EM_ABERTO = '" + $('#URA_CONTA_EM_ABERTO').val() + "',";
					sql = sql + " URA_SOLICITACAO = '" + $('#URA_SOLICITACAO').val() + "',";
					sql = sql + " ura_mailing = '" + $('#Ura_mailing').val() + "',";
					sql = sql + " ura_produto = '" + $('#Ura_produto').val() + "',";
					sql = sql + " URA_TELEFONE_TRATADO = '" + $('#URA_TELEFONE_TRATADO').val() + "',";
					sql = sql + " URA_VIP = '" + $('#URA_VIP').val() + "',";
					sql = sql + " URA_RESGATE_ANATEL = '" + $('#URA_RESGATE_ANATEL').val() + "',";
					sql = sql + " URA_TRN_ID = '" + $('#URA_TRN_ID').val() + "',";
					sql = sql + " URA_USOUAPP12MESES = '" + $('#URA_USOUAPP12MESES').val() + "',";
					sql = sql + " URA_USOUAPP20MIN = '" + $('#URA_USOUAPP20MIN').val() + "',";
					sql = sql + " URA_EVENTO = '" + $('#URA_EVENTO').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_dddOct').val() + "',";
					sql = sql + " final_digito = '" + $('#Final_digitoOct').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoOct').val() + "',";
					sql = sql + " companyName = '" + $('#companyNameOct').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressOct').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoOct_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameOct_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressOct_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdOct').val();	
				}else{

					//Testa ponto e vírgula
					var uraProduto = $('#Ura_produto').val().substring($('#Ura_produto').val().length-1,$('#Ura_produto').val().length) !== ";" ? $('#Ura_produto').val() + ";" : $('#Ura_produto').val();
				
					sql = "if not exists(SELECT * FROM ContingenciaOct"
                    sql = sql + " where ura_repetidacrep = '" + $('#Ura_Repetidacrep').val() + "'"				
					sql = sql + " and ura_pilotocrep ='" + $('#Ura_Pilotocrep').val() + "'"
					sql = sql + " and ura_blacklistcrep = '" + $('#Ura_Blacklistcrep').val() + "'"					
					sql = sql + " and ura_horariocrep ='" + $('#Ura_Horariocrep').val() + "'"
					sql = sql + " and ura_assunto = '" + $('#Ura_assuntoOct').val() + "'"
					sql = sql + " and URA_BDABERTODENTRODOPRAZO = '" + $('#URA_BDABERTODENTRODOPRAZO').val() + "'"
					sql = sql + " and URA_BDABERTOFORADOPRAZO = '" + $('#URA_BDABERTOFORADOPRAZO').val() + "'"				
					sql = sql + " and URA_BDVELOXABERTODENTRODOPRAZO = '" + $('#URA_BDVELOXABERTODENTRODOPRAZO').val() + "'"
					sql = sql + " and URA_BDVELOXABERTOFORADOPRAZO = '" + $('#URA_BDVELOXABERTOFORADOPRAZO').val() + "'"
					sql = sql + " and URA_QUARENTENACREP = '" + $('#URA_QUARENTENACREP').val() + "'"
					sql = sql + " and URA_FRAUDE = '" + $('#URA_FRAUDE').val() + "'"
					sql = sql + " and URA_TIPOBLOQUEIO = '" + $('#URA_TIPOBLOQUEIO').val() + "'"
					sql = sql + " and URA_WELCOME = '" + $('#URA_WELCOME').val() + "'"
					sql = sql + " and URA_CONTA_EM_ABERTO = '" + $('#URA_CONTA_EM_ABERTO').val() + "'"
					sql = sql + " and URA_SOLICITACAO ='" + $('#URA_SOLICITACAO').val() + "'"
					sql = sql + " and ura_mailing = '" + $('#Ura_mailing').val() + "'"
					sql = sql + " and ura_produto = '" + uraProduto + "'"
					sql = sql + " and URA_TELEFONE_TRATADO = '" + $('#URA_TELEFONE_TRATADO').val() + "'"
					sql = sql + " and URA_VIP = '" + $('#URA_VIP').val() + "'"
					sql = sql + " and URA_RESGATE_ANATEL = '" + $('#URA_RESGATE_ANATEL').val() + "'"
					sql = sql + " and URA_TRN_ID = '" + $('#URA_TRN_ID').val() + "'"
					sql = sql + " and URA_USOUAPP12MESES = '" + $('#URA_USOUAPP12MESES').val() + "'"
					sql = sql + " and URA_USOUAPP20MIN = '" + $('#URA_USOUAPP20MIN').val() + "'"
					sql = sql + " and URA_EVENTO = '" + $('#URA_EVENTO').val() + "'"
					sql = sql + " and ura_ddd = '" + $('#Ura_dddOct').val() + "'"
					sql = sql + " and final_digito = '" + $('#Final_digitoOct').val() + "'"
					sql = sql + " and extendedResponseServicoDestino = '" + $('#extendedResponseServicoDestinoOct').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameOct').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressOct').val() + "')"					
					sql = sql + " insert into ContingenciaOct(ura_repetidacrep,ura_pilotocrep,ura_blacklistcrep,"
					sql = sql + " ura_horariocrep,ura_assunto,URA_BDABERTODENTRODOPRAZO,URA_BDABERTOFORADOPRAZO,"
					sql = sql + " URA_BDVELOXABERTODENTRODOPRAZO,URA_BDVELOXABERTOFORADOPRAZO,URA_QUARENTENACREP,"
					sql = sql + " URA_FRAUDE,URA_TIPOBLOQUEIO,URA_WELCOME,URA_CONTA_EM_ABERTO,URA_SOLICITACAO,"
					sql = sql + " ura_mailing,ura_produto,"
					sql = sql + " URA_TELEFONE_TRATADO,URA_VIP,URA_RESGATE_ANATEL,URA_TRN_ID,URA_USOUAPP12MESES,"
					sql = sql + " URA_USOUAPP20MIN,URA_EVENTO,ura_ddd,final_digito,"
					sql = sql + " extendedResponseServicoDestino,companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_Repetidacrep').val() + "',"
					sql = sql + "'" + $('#Ura_Pilotocrep').val() + "',"
					sql = sql + "'" + $('#Ura_Blacklistcrep').val() + "',"
					sql = sql + "'" + $('#Ura_Horariocrep').val() + "',"
					sql = sql + "'" + $('#Ura_assuntoOct').val() + "',"
					sql = sql + "'" + $('#URA_BDABERTODENTRODOPRAZO').val() + "',"
					sql = sql + "'" + $('#URA_BDABERTOFORADOPRAZO').val() + "',"
					sql = sql + "'" + $('#URA_BDVELOXABERTODENTRODOPRAZO').val() + "',"
					sql = sql + "'" + $('#URA_BDVELOXABERTOFORADOPRAZO').val() + "',"
					sql = sql + "'" + $('#URA_QUARENTENACREP').val() + "',"
					sql = sql + "'" + $('#URA_FRAUDE').val() + "',"
					sql = sql + "'" + $('#URA_TIPOBLOQUEIO').val() + "',"
					sql = sql + "'" + $('#URA_WELCOME').val() + "',"
					sql = sql + "'" + $('#URA_CONTA_EM_ABERTO').val() + "',"
					sql = sql + "'" + $('#URA_SOLICITACAO').val() + "',"
					sql = sql + "'" + $('#Ura_mailing').val() + "',"
					sql = sql + "'" + uraProduto + "',"
					sql = sql + "'" + $('#URA_TELEFONE_TRATADO').val() + "',"
					sql = sql + "'" + $('#URA_VIP').val() + "',"
					sql = sql + "'" + $('#URA_RESGATE_ANATEL').val() + "',"
					sql = sql + "'" + $('#URA_TRN_ID').val() + "',"
					sql = sql + "'" + $('#URA_USOUAPP12MESES').val() + "',"
					sql = sql + "'" + $('#URA_USOUAPP20MIN').val() + "',"
					sql = sql + "'" + $('#URA_EVENTO').val() + "',"
					sql = sql + "'" + $('#Ura_dddOct').val() + "',"
					sql = sql + "'" + $('#Final_digitoOct').val() + "',"
					sql = sql + "'" + $('#extendResponseServicoDestinoOct').val() + "',"
					sql = sql + "'" + $('#companyNameOct').val() + "',"
					sql = sql + "'" + $('#selectedDestAddressOct').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoOct_2').val() + "',";
					sql = sql + "'" + $('#companyNameOct_2').val() + "','" + $('#selectedDestAddressOct_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";	
					sqlLog=";select getdate()";
				}
				
				break;
				
				

				
			case 'ContingenciaEmpresarial' :
				var horaIni = $('#horaIniEmp').val() == "" ? null :  "'" + $('#horaIniEmp').val() + "'";
				var horaFim = $('#horaFimEmp').val() == "" ? null :  "'" + $('#horaFimEmp').val() + "'";
				if ($('#IdEmp').val() !==""){
					
					sql = "update ContingenciaEmpresarial set ura_canal = '" + $('#Ura_CanalEmp').val() + "',";
					sql = sql + " ura_produto = '" + $('#Ura_ProdutoEmp').val() + "',";					
					sql = sql + " ura_assunto = '" + $('#Ura_AssuntoEmp').val() + "',ura_ddd = '" + $('#Ura_DDDEmp').val() + "',";
					sql = sql + " parametro_bloqueio_financeiro = '" + $('#BloqueioEmp').val() + "',";
					sql = sql + " ura_mailing = '" + $('#Ura_MailingEmp').val() + "',";
					sql = sql + " tel_fixo = '" + $('#Tel_FixoEmp').val() + "',";
					sql = sql + " velox = '" + $('#VeloxEmp').val() + "',";
					sql = sql + " rentabilizacao_empresarial = '" + $('#RentabilizacaoEmp').val() + "',";
					sql = sql + " consultor_empresarial = '" + $('#ConsultorEmp').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoEmp').val() + "',";
					sql = sql + " companyName = '" + $('#companyNameEmp').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressEmp').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoEmp_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameEmp_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressEmp_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdEmp').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaEmpresarial"
					sql = sql + " where ura_canal = '" + $('#Ura_CanalEmp').val() + "'"
                    sql = sql + " and ura_produto = '" + $('#Ura_ProdutoEmp').val() + "'"
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoEmp').val() + "'"
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDEmp').val() + "'"
					sql = sql + " and parametro_bloqueio_financeiro ='" + $('#BloqueioEmp').val() + "'"					
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingEmp').val() + "'"
					sql = sql + " and tel_fixo = '" + $('#Tel_FixoEmp').val() + "'"
					sql = sql + " and velox = '" + $('#VeloxEmp').val() + "'"
					sql = sql + " and rentabilizacao_empresarial = '" + $('#RentabilizacaoEmp').val() + "'"
					sql = sql + " and consultor_empresarial = '" + $('#ConsultorEmp').val() + "'"
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoEmp').val() + "'"
					sql = sql + " and companyName = '" + $('#companyNameEmp').val() + "'"	
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressEmp').val() + "')"					
					sql = sql + " insert into ContingenciaEmpresarial(ura_canal,ura_produto,ura_assunto,"
					sql = sql + " ura_ddd,parametro_bloqueio_financeiro,ura_mailing,"
					sql = sql + " tel_fixo,velox,rentabilizacao_empresarial,consultor_empresarial,"
					sql = sql + " extendedResponseServicoDestino,"
					sql = sql + " companyName,selectedDestAddress,"
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)"
					sql = sql + " values ('" + $('#Ura_CanalEmp').val() + "',"
					sql = sql + " '" + $('#Ura_ProdutoEmp').val() + "',"
					sql = sql + " '" + $('#Ura_AssuntoEmp').val() + "',"
					sql = sql + " '" + $('#Ura_DDDEmp').val() + "',"
					sql = sql + " '" + $('#BloqueioEmp').val() + "',"					
					sql = sql + " '" + $('#Ura_MailingEmp').val() + "',"
					sql = sql + " '" + $('#Tel_FixoEmp').val() + "',"
					sql = sql + " '" + $('#VeloxEmp').val() + "',"
					sql = sql + " '" + $('#RentabilizacaoEmp').val() + "',"
					sql = sql + " '" + $('#ConsultorEmp').val() + "',"
					sql = sql + " '" + $('#extendResponseServicoDestinoEmp').val() + "',"
					sql = sql + " '" + $('#companyNameEmp').val() + "'"
					sql = sql + ",'" + $('#selectedDestAddressEmp').val() + "',"
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoEmp_2').val() + "',";
					sql = sql + "'" + $('#companyNameEmp_2').val() + "','" + $('#selectedDestAddressEmp_2').val() + "')"
					sql = sql + " else"
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Canal: " + $('#Ura_CanalEmp').val() + ";";
					sqlLog = sqlLog + "Produto: " + $('#Ura_ProdutoEmp').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoEmp').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDEmp').val() + ";";
					sqlLog = sqlLog + "Bloqueio: " + $('#BloqueioEmp').val() + ";";					
					sqlLog = sqlLog + "Mailing: " + $('#Ura_MailingEmp').val() + ";";
					sqlLog = sqlLog + "TelFixo: " + $('#Tel_FixoEmp').val() + ";";
					sqlLog = sqlLog + "Velox: " + $('#VeloxEmp').val() + ";";
					sqlLog = sqlLog + "Rentabilizacao: " + $('#RentabilizacaoEmp').val() + ";";
					sqlLog = sqlLog + "Consultor: " + $('#ConsultorEmp').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoEmp').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameEmp').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressEmp').val() + "','ContingenciaEmpresarial')";
					

					
					
				}
				break;

			case 'ContingenciaOiInternet' :
				var horaIni = $('#horaIniOiInternet').val() == "" ? null :  "'" + $('#horaIniOiInternet').val() + "'";
				var horaFim = $('#horaFimOiInternet').val() == "" ? null :  "'" + $('#horaFimOiInternet').val() + "'";
				if ($('#IdOiInternet').val() !==""){
					
					sql = "update ContingenciaOiInternet set "; 
					sql = sql + " ura_canal = '" + $('#Ura_CanalOiInternet').val() + "',";					
					sql = sql + " ura_assunto = '" + $('#Ura_AssuntoOiInternet').val() + "',"; 
					sql = sql + " ura_segmento = '" + $('#Ura_SegmentoOiInternet').val() + "',";
					sql = sql + " ura_ddd = '" + $('#Ura_DDDOiInternet').val() + "',";
					sql = sql + " ura_plano = '" + $('#Ura_PlanoOiInternet').val() + "',";
					sql = sql + " ura_tipo_bloqueio = '" + $('#Ura_TipoBloqueioOiInternet').val() + "',";
					sql = sql + " ura_mailing = '" + $('#Ura_MailingOiInternet').val() + "',";
					sql = sql + " ura_modem_alinhado = '" + $('#Ura_ModemAlinhadoOiInternet').val() + "',";
					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoOiInternet').val() + "',";
					sql = sql + " companyName = '" + $('#companyNameOiInternet').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressOiInternet').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoOiInternet_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameOiInternet_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressOiInternet_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdOiInternet').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaOiInternet";
					sql = sql + " where ura_canal = '" + $('#Ura_CanalOiInternet').val() + "'";
					sql = sql + " and ura_assunto ='" + $('#Ura_AssuntoOiInternet').val() + "'";
					sql = sql + " and ura_segmento ='" + $('#Ura_SegmentoOiInternet').val() + "'";
					sql = sql + " and ura_ddd ='" + $('#Ura_DDDOiInternet').val() + "'";
					sql = sql + " and ura_plano = '" + $('#Ura_PlanoOiInternet').val() + "'";
					sql = sql + " and ura_tipo_bloqueio = '" + $('#Ura_TipoBloqueioOiInternet').val() + "'";
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingOiInternet').val() + "'";
					sql = sql + " and ura_modem_alinhado = '" + $('#Ura_ModemAlinhadoOiInternet').val() + "'";
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoOiInternet').val() + "'";
					sql = sql + " and companyName = '" + $('#companyNameOiInternet').val() + "'"	;
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressOiInternet').val() + "')";
					sql = sql + " insert into ContingenciaOiInternet(ura_canal,ura_assunto,ura_segmento,";
					sql = sql + " ura_ddd,ura_plano,ura_tipo_bloqueio,ura_mailing,ura_modem_alinhado,";
					sql = sql + " extendedResponseServicoDestino,";
					sql = sql + " companyName,selectedDestAddress,";
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)";
					sql = sql + " values ('" + $('#Ura_CanalOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_AssuntoOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_SegmentoOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_DDDOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_PlanoOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_TipoBloqueioOiInternet').val() + "',";
					sql = sql + " '" + $('#Ura_MailingOiInternet').val() + "',";					
					sql = sql + " '" + $('#Ura_ModemAlinhadoOiInternet').val() + "',";
					sql = sql + " '" + $('#extendResponseServicoDestinoOiInternet').val() + "',";
					sql = sql + " '" + $('#companyNameOiInternet').val() + "'";
					sql = sql + ",'" + $('#selectedDestAddressOiInternet').val() + "',";
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoOiInternet_2').val() + "',";
					sql = sql + "'" + $('#companyNameOiInternet_2').val() + "','" + $('#selectedDestAddressOiInternet_2').val() + "')";
					sql = sql + " else";
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					sqlLog = sqlLog + "'Canal: " + $('#Ura_CanalOiInternet').val() + ";";
					sqlLog = sqlLog + "Assunto: " + $('#Ura_AssuntoOiInternet').val() + ";";
					sqlLog = sqlLog + "Segmento: " + $('#Ura_SegmentoOiInternet').val() + ";";
					sqlLog = sqlLog + "DDD: " + $('#Ura_DDDOiInternet').val() + ";";
					sqlLog = sqlLog + "Plano: " + $('#Ura_PlanoOiInternet').val() + ";";					
					sqlLog = sqlLog + "Tipo Bloqueio: " + $('#Ura_TipoBloqueioOiInternet').val() + ";";
					sqlLog = sqlLog + "Mailing: " + $('#Ura_MailingOiInternet').val() + ";";
					sqlLog = sqlLog + "Modem Alinhamento: " + $('#Ura_ModemAlinhadoOiInternet').val() + ";";
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoOiInternet').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameOiInternet').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressOiInternet').val() + "','ContingenciaOiInternet')";
				}
				break;
			
			case 'ContingenciaUraFibra' :
				var horaIni = $('#horaIniUraFibra').val() == "" ? null :  "'" + $('#horaIniUraFibra').val() + "'";
				var horaFim = $('#horaFimUraFibra').val() == "" ? null :  "'" + $('#horaFimUraFibra').val() + "'";
				if ($('#IdUraFibra').val() !==""){
					
					sql = "update ContingenciaUraFibra set "; 
					sql = sql + " ura_canal = '" + $('#Ura_CanalUraFibra').val() + "',";	
					sql = sql + " ura_plano = '" + $('#Ura_PlanoUraFibra').val() + "',";
					sql = sql + " ura_produto = '" + $('#Ura_ProdutoUraFibra').val() + "',";
					sql = sql + " ura_tipoplano = '" + $('#Ura_TipoplanoUraFibra').val() + "',";
					sql = sql + " ura_segmento = '" + $('#Ura_SegmentoUraFibra').val() + "',";
					sql = sql + " ura_pontoderivacao = '" + $('#Ura_PontoderivacaoUraFibra').val() + "',";
					sql = sql + " ura_assunto = '" + $('#Ura_AssuntoUraFibra').val() + "',"; 
					sql = sql + " ura_status_alone = '" + $('#Ura_StatusAloneUraFibra').val() + "',"; 
					sql = sql + " ura_status_oit_fixo = '" + $('#Ura_StatusOitFixoUraFibra').val() + "',"; 
					sql = sql + " ura_status_oit_bl = '" + $('#Ura_StatusOitBlUraFibra').val() + "',"; 
					sql = sql + " ura_status_oit_tv = '" + $('#Ura_StatusOitTvUraFibra').val() + "',"; 
					sql = sql + " ura_tipo_bloqueio = '" + $('#Ura_TipoBloqueioUraFibra').val() + "',"; 
					sql = sql + " ura_cidade = '" + $('#Ura_CidadeUraFibra').val() + "',"; 
					sql = sql + " ura_ddd = '" + $('#Ura_DDDUraFibra').val() + "',"; 
					sql = sql + " ura_bercario = '" + $('#Ura_BercarioUraFibra').val() + "',";
					sql = sql + " ura_mailing = '" + $('#Ura_MailingUraFibra').val() + "',";					
					sql = sql + " ura_st_crep = '" + $('#Ura_StCrepUraFibra').val() + "',"; 
					sql = sql + " ura_sac_crep = '" + $('#Ura_SacCrepUraFibra').val() + "',"; 
					sql = sql + " ura_tagrecvoz = '" + $('#Ura_TagrecvozUraFibra').val() + "',"; 
					sql = sql + " ura_recvoz = '" + $('#Ura_RecvozUraFibra').val() + "',"; 

					sql = sql + " extendedResponseServicoDestino = '" + $('#extendResponseServicoDestinoUraFibra').val() + "',";
					sql = sql + " companyName = '" + $('#companyNameUraFibra').val() + "',";
					sql = sql + " selectedDestAddress = '" + $('#selectedDestAddressUraFibra').val() + "',";
					sql = sql + " extendedResponseServicoDestino_2 = '" + $('#extendResponseServicoDestinoUraFibra_2').val() + "',";
					sql = sql + " companyName_2 = '" + $('#companyNameUraFibra_2').val() + "',";
					sql = sql + " selectedDestAddress_2 = '" + $('#selectedDestAddressUraFibra_2').val() + "' ,";
					sql = sql + " horaIni = " + horaIni + " ,";
					sql = sql + " horaFim = " + horaFim + " ";
					sql = sql + " where id = " + $('#IdUraFibra').val();	
				}else{				
				
					sql = "if not exists(SELECT * FROM ContingenciaUraFibra";
					sql = sql + " where ura_canal = '" + $('#Ura_CanalUraFibra').val() + "'";
					sql = sql + " and ura_plano ='" + $('#Ura_PlanoUraFibra').val() + "'";
					sql = sql + " and ura_produto ='" + $('#Ura_ProdutoUraFibra').val() + "'";
					sql = sql + " and ura_tipoplano ='" + $('#Ura_TipoplanoUraFibra').val() + "'";
					sql = sql + " and ura_segmento = '" + $('#Ura_SegmentoUraFibra').val() + "'";
					sql = sql + " and ura_pontoderivacao = '" + $('#Ura_PontoderivacaoUraFibra').val() + "'";
					sql = sql + " and ura_assunto = '" + $('#Ura_AssuntoUraFibra').val() + "'";
					sql = sql + " and ura_status_alone = '" + $('#Ura_StatusAloneUraFibra').val() + "'";
					sql = sql + " and ura_status_oit_fixo = '" + $('#Ura_StatusOitFixoUraFibra').val() + "'";
					sql = sql + " and ura_status_oit_bl = '" + $('#Ura_StatusOitBlUraFibra').val() + "'";
					sql = sql + " and ura_status_oit_tv = '" + $('#Ura_StatusOitTvUraFibra').val() + "'";
					sql = sql + " and ura_tipo_bloqueio = '" + $('#Ura_TipoBloqueioUraFibra').val() + "'";
					sql = sql + " and ura_cidade = '" + $('#Ura_CidadeUraFibra').val() + "'";
					sql = sql + " and ura_ddd = '" + $('#Ura_DDDUraFibra').val() + "'";
					sql = sql + " and ura_bercario = '" + $('#Ura_BercarioUraFibra').val() + "'";
					sql = sql + " and ura_mailing = '" + $('#Ura_MailingUraFibra').val() + "'";
					sql = sql + " and ura_st_crep = '" + $('#Ura_StCrepUraFibra').val() + "'";
					sql = sql + " and ura_sac_crep = '" + $('#Ura_SacCrepUraFibra').val() + "'";
					sql = sql + " and ura_tagrecvoz = '" + $('#Ura_TagrecvozUraFibra').val() + "'";
					sql = sql + " and ura_recvoz = '" + $('#Ura_RecvozUraFibra').val() + "'";
					
					
					sql = sql + " and EXTENDEDRESPONSESERVICODESTINO = '" + $('#extendResponseServicoDestinoUraFibra').val() + "'";
					sql = sql + " and companyName = '" + $('#companyNameUraFibra').val() + "'"	;
					sql = sql + " and SELECTEDDESTADDRESS = '" + $('#selectedDestAddressUraFibra').val() + "')";
					sql = sql + " insert into ContingenciaUraFibra(ura_canal,ura_plano,ura_produto,";
					sql = sql + " ura_tipoplano,ura_segmento,ura_pontoderivacao,ura_assunto,ura_status_alone,";
					sql = sql + " ura_status_oit_fixo,ura_status_oit_bl,ura_status_oit_tv,ura_tipo_bloqueio,ura_cidade,";
					sql = sql + " ura_ddd,ura_bercario,ura_mailing,ura_st_crep,ura_sac_crep,";
					sql = sql + " ura_tagrecvoz,ura_recvoz,";
					sql = sql + " extendedResponseServicoDestino,";
					sql = sql + " companyName,selectedDestAddress,";
					sql = sql + " horaIni,horaFim,extendedResponseServicoDestino_2,companyName_2,selectedDestAddress_2)";
					sql = sql + " values ('" + $('#Ura_CanalUraFibra').val() + "',";					
					sql = sql + " '" + $('#Ura_PlanoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_ProdutoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_TipoplanoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_SegmentoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_PontoderivacaoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_AssuntoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_StatusAloneUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_StatusOitFixoUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_StatusOitBlUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_StatusOitTvUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_TipoBloqueioUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_CidadeUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_DDDUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_BercarioUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_MailingUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_StCrepUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_SacCrepUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_TagrecvozUraFibra').val() + "',";
					sql = sql + " '" + $('#Ura_RecvozUraFibra').val() + "',";					
					sql = sql + " '" + $('#extendResponseServicoDestinoUraFibra').val() + "',";
					sql = sql + " '" + $('#companyNameUraFibra').val() + "'";
					sql = sql + ",'" + $('#selectedDestAddressUraFibra').val() + "',";
					sql = sql + "" + horaIni + "," + horaFim + ",'" + $('#extendResponseServicoDestinoUraFibra_2').val() + "',";
					sql = sql + "'" + $('#companyNameUraFibra_2').val() + "','" + $('#selectedDestAddressUraFibra_2').val() + "')";
					sql = sql + " else";
					sql = sql + " RAISERROR('Registro já existe!',16,1)";
					
					sqlLog = ";insert into LogContingenciaRouting values ('" + loginUsuario + "',getdate(),'CADASTRO',";
					
					sqlLog = sqlLog + " 'Canal: " + $('#Ura_CanalUraFibra').val() + ";";	
					sqlLog = sqlLog + " Plano: " + $('#Ura_PlanoUraFibra').val() + ";";
					sqlLog = sqlLog + " Produto: " + $('#Ura_ProdutoUraFibra').val() + ";";
					sqlLog = sqlLog + " Tipo Plano: " + $('#Ura_TipoplanoUraFibra').val() + ";";
					sqlLog = sqlLog + " Segmento: " + $('#Ura_SegmentoUraFibra').val() + ";";
					sqlLog = sqlLog + " Ponto Derivacao: " + $('#Ura_PontoderivacaoUraFibra').val() + ";";
					sqlLog = sqlLog + " Assunto: " + $('#Ura_AssuntoUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Status Alone: " + $('#Ura_StatusAloneUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Status Oit Fixo: " + $('#Ura_StatusOitFixoUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Status Oit Bl: " + $('#Ura_StatusOitBlUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Status Oit Tv: " + $('#Ura_StatusOitTvUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Tipo Bloqueio: " + $('#Ura_TipoBloqueioUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Cidade: " + $('#Ura_CidadeUraFibra').val() + ";"; 
					sqlLog = sqlLog + " DDD: " + $('#Ura_DDDUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Bercario: " + $('#Ura_BercarioUraFibra').val() + ";";
					sqlLog = sqlLog + " Mailing: " + $('#Ura_MailingUraFibra').val() + ";";					
					sqlLog = sqlLog + " St Crep: " + $('#Ura_StCrepUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Sac Crep: " + $('#Ura_SacCrepUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Tag Recvoz: " + $('#Ura_TagrecvozUraFibra').val() + ";"; 
					sqlLog = sqlLog + " Recvoz: " + $('#Ura_RecvozUraFibra').val() + ";"; 		
					sqlLog = sqlLog + "Destino: " + $('#extendResponseServicoDestinoUraFibra').val() + ";";
					sqlLog = sqlLog + "Company: " + $('#companyNameUraFibra').val() + ";";
					sqlLog = sqlLog + "Address: " + $('#selectedDestAddressUraFibra').val() + "','ContingenciaUraFibra')";
				}
				break;
			
			default:
				
				return;
		}
		console.log(sql + sqlLog);		

		db.query(sql + sqlLog, null, function (err, num_rows) {
            console.log("Executando query-> " + sql + " " + num_rows);            
            if (err) {
                console.log("ERRO: " + err);
				alert("ERRO: " + err);
            } else {                
				alert("Regra salva com sucesso");	
				$scope.listaDados();
            }  
        });		
	}
	  
    $scope.listar = function() {
        var regras = $('.filtro-regras2').val();
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-contingencia-routing");
            $scope.colunas = geraColunas();
            $scope.listar.apply(this);
        });       
        
        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            $('select.filtro-regras2').val("").selectpicker('refresh');            

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');               
                $scope.$apply();
            }, 500);
        }



        //Alex 23/05/2014
        $scope.abortar = function () {         
          abortar($scope, "#pag-contingencia-routing");
        }
	}
        
	$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');	

  }); //FIM do ViewContentLoaded


        $scope.listaDados = function(){
			
			var $btn_exportar = $view.find(".btn-exportar");
			$btn_exportar.prop("disabled", true);
			
			console.log("Chamou lista Dados");
			$globals.numeroDeRegistros = 0;
            var $btn_gerar = $(this);
            $btn_gerar.button('loading');
			$scope.dados = [];	
            var tabela = $('.filtro-regras2').val();
			for (var i = 0; i < paineis.length; i++){
				$('#painel'+paineis[i]+'').css('display','none');			
			}
			$('.grid').css('display','block');
			$('.alert-container').css('display','block');
			$scope.colunas = geraColunas();
			var stmt = db.use + " select * from " + tabela;
			stmt += " order by id ";			
			
			  log(stmt);
			  

			  function collect() {
				var ret = {};
				var len = arguments.length;
				for (var i=0; i<len; i++) {
				  for (p in arguments[i]) {
					if (arguments[i].hasOwnProperty(p)) {
					  ret[p] = arguments[i][p];
					}
				  }
				}
				return ret;
			  }



			  db.query(stmt, function (columns) {

				var partes = [];
				partes.push({
					id: columns["id"].value
				});

			
				if ($('.filtro-regras2').val() === "ContingenciaFixoVelox"){
		
						partes.push({							
							ura_produto: columns["ura_produto"].value,
							ura_assunto: columns["ura_assunto"].value,
							ura_ddd: columns["ura_ddd"].value,
							final_digito: columns["final_digito"].value,
							ura_mailing: columns["ura_mailing_r2"].value
						});

				}

				try{
				var ptderivacao = columns["ura_pontoderivacao"].value == "*;" ? columns["ura_pontoderivacao"].value.slice(0,-1) : columns["ura_pontoderivacao"].value;
				}catch(ex){

				}
				try{
				var plano = columns["ura_plano"].value == "*;" ? columns["ura_plano"].value.slice(0,-1) : columns["ura_plano"].value;
				}catch(ex){

				}
				if ($('.filtro-regras2').val() === "ContingenciaPos"){
					partes.push({						
						ura_pontoderivacao: ptderivacao,
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value,
						ura_vclientejornalista: columns["ura_vclientejornalista"].value,
						ura_isflagfraude: columns["ura_isflagfraude"].value,
						ura_isoct: columns["ura_isoct"].value,
						ura_ispospago: columns["ura_ispospago"].value,
						ura_isclienteoct4p: columns["ura_isclienteoct4p"].value,
						is_contaemaberto_entreqtddias: columns["is_contaemaberto_entreqtddias"].value,
						possuicontaemabertoentrexdias: columns["possuicontaemabertoentrexdias"].value,
						ura_isosctprofissionalpj: columns["ura_isosctprofissionalpj"].value,
						ura_isoctprofissionalpf: columns["ura_isoctprofissionalpf"].value,
						ura_isoivelox3g: columns["ura_isoivelox3g"].value,
						ura_nome_plano: columns["ura_nome_plano"].value,
						ura_lastdialog: columns["ura_lastdialog"].value,
						ura_mailing: columns["ura_mailing"].value
					});
				  
				
				}

				if ($('.filtro-regras2').val() === "ContingenciaPre"){
					partes.push({						
						ura_pontoderivacao: ptderivacao,
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value,
						ura_isflagfraude: columns["ura_isflagfraude"].value,
						ura_isvip: columns["ura_isvip"].value,
						ura_segmentovalor: columns["ura_segmentovalor"].value,
						ura_isoicontrole: columns["ura_isoicontrole"].value,
						ura_isscoreanatel: columns["ura_isscoreanatel"].value,
						ura_welcome: columns["ura_welcome"].value,
						ura_ism4u: columns["ura_ism4u"].value,
						ura_isdigito: columns["ura_isdigito"].value,
						ura_isdemandaativa: columns["ura_isdemandaativa"].value,
						ura_isnid: columns["ura_isnid"].value,
						ura_isnovooicontrole: columns["ura_isnovooicontrole"].value,
						ura_isprepago: columns["ura_isprepago"].value,
						ura_promocao: columns["ura_promocao"].value,
						ura_isoimod: columns["ura_isoimod"].value
					});
				  
				
				}

				if ($('.filtro-regras2').val() === "ContingenciaOiTV"){

					partes.push({							
						ura_produto: columns["ura_produto"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value,
						ura_mailing: columns["ura_mailing"].value
					});

				}

				if ($('.filtro-regras2').val() === "ContingenciaOiTotal_Homg"){
					partes.push({						
						ura_produto: columns["ura_produto"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_ddd: columns["ura_ddd"].value,
						ura_plano: plano,						
						final_digito: columns["final_digito"].value,
						ura_mailing: columns["ura_mailing"].value
					});						
				}

				if ($('.filtro-regras2').val() === "ContingenciaTelevendas"){
					partes.push({						
						ura_menu_origem: columns["ura_menu_origem"].value,
						ura_canal_de_entrada: columns["ura_canal_de_entrada"].value,								
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value
					});	
				}


				if ($('.filtro-regras2').val() === "ContingenciaCorporativo"){

					partes.push({							
						ura_produto: columns["ura_produto"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_ddd: columns["ura_ddd"].value,
						tel_fixo: columns["tel_fixo"].value,
						ura_mailing: columns["ura_mailing"].value,
						velox: columns["velox"].value,
						ura_carteira: columns["ura_carteira"].value,
						tipo_cliente: columns["tipo_cliente"].value
					});

				}

				if ($('.filtro-regras2').val() === "ContingenciaCadastro"){

					partes.push({							
						ura_canal_de_entrada: columns["ura_canal_de_entrada"].value,
						ura_tipo_chip: columns["ura_tipo_chip"].value,
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value,
						ura_plano: columns["ura_plano"].value,
						nome_plano: columns["nome_plano"].value,
						atendimento_bilingue: columns["atendimento_bilingue"].value,
						MSIDN: columns["MSIDN"].value,
						CHAMADASREPETIDA_24HORAS: columns["CHAMADASREPETIDA_24HORAS"].value
					});

				}

				if ($('.filtro-regras2').val() === "ContingenciaOct"){

					partes.push({							
						ura_repetidacrep: columns["ura_repetidacrep"].value,
						ura_pilotocrep: columns["ura_pilotocrep"].value,
						ura_blacklistcrep: columns["ura_blacklistcrep"].value,
						ura_horariocrep: columns["ura_horariocrep"].value,
						ura_assunto: columns["ura_assunto"].value,
						URA_BDABERTODENTRODOPRAZO: columns["URA_BDABERTODENTRODOPRAZO"].value,
						URA_BDABERTOFORADOPRAZO: columns["URA_BDABERTOFORADOPRAZO"].value,
						URA_BDVELOXABERTODENTRODOPRAZO: columns["URA_BDVELOXABERTODENTRODOPRAZO"].value,
						URA_BDVELOXABERTOFORADOPRAZO: columns["URA_BDVELOXABERTOFORADOPRAZO"].value,
						URA_QUARENTENACREP: columns["URA_QUARENTENACREP"].value,
						URA_FRAUDE: columns["URA_FRAUDE"].value,
						URA_TIPOBLOQUEIO: columns["URA_TIPOBLOQUEIO"].value,
						URA_WELCOME: columns["URA_WELCOME"].value,
						URA_CONTA_EM_ABERTO: columns["URA_CONTA_EM_ABERTO"].value,
						URA_SOLICITACAO: columns["URA_SOLICITACAO"].value,
						ura_mailing: columns["ura_mailing"].value,
						ura_produto: columns["ura_produto"].value,
						URA_TELEFONE_TRATADO: columns["URA_TELEFONE_TRATADO"].value,
						URA_VIP: columns["URA_VIP"].value,
						URA_RESGATE_ANATEL: columns["URA_RESGATE_ANATEL"].value,
						URA_TRN_ID: columns["URA_TRN_ID"].value,
						URA_USOUAPP12MESES: columns["URA_USOUAPP12MESES"].value,
						URA_USOUAPP20MIN: columns["URA_USOUAPP20MIN"].value,
						URA_EVENTO: columns["URA_EVENTO"].value,
						ura_ddd: columns["ura_ddd"].value,
						final_digito: columns["final_digito"].value
					});

				}
				
				if ($('.filtro-regras2').val() === "ContingenciaEmpresarial"){
					
		
					partes.push({	
						ura_canal: columns["ura_canal"].value,					
						ura_produto: columns["ura_produto"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_ddd: columns["ura_ddd"].value,
						parametro_bloqueio_financeiro: columns["parametro_bloqueio_financeiro"].value,						
						ura_mailing: columns["ura_mailing"].value,
						tel_fixo: columns["tel_fixo"].value,
						velox: columns["velox"].value,
						rentabilizacao_empresarial: columns["rentabilizacao_empresarial"].value,
						consultor_empresarial: columns["consultor_empresarial"].value
					});

				}

				if ($('.filtro-regras2').val() === "ContingenciaOiInternet"){
					
		
					partes.push({	
						ura_canal: columns["ura_canal"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_segmento: columns["ura_segmento"].value,
						ura_ddd: columns["ura_ddd"].value,
						ura_plano: columns["ura_plano"].value,
						ura_tipo_bloqueio: columns["ura_tipo_bloqueio"].value,
						ura_mailing: columns["ura_mailing"].value,
						ura_modem_alinhado: columns["ura_modem_alinhado"].value					
					});

				}
				
				if ($('.filtro-regras2').val() === "ContingenciaUraFibra"){
					
		
					partes.push({
					
						ura_canal: columns["ura_canal"].value,
						ura_plano: columns["ura_plano"].value,
						ura_produto: columns["ura_produto"].value,
						ura_tipoplano: columns["ura_tipoplano"].value,
						ura_segmento: columns["ura_segmento"].value,
						ura_pontoderivacao: columns["ura_pontoderivacao"].value,
						ura_assunto: columns["ura_assunto"].value,
						ura_status_alone: columns["ura_status_alone"].value,
						ura_status_oit_fixo: columns["ura_status_oit_fixo"].value,
						ura_status_oit_bl: columns["ura_status_oit_bl"].value,
						ura_status_oit_tv: columns["ura_status_oit_tv"].value,
						ura_tipo_bloqueio: columns["ura_tipo_bloqueio"].value,
						ura_cidade: columns["ura_cidade"].value,
						ura_ddd: columns["ura_ddd"].value,
						ura_bercario: columns["ura_bercario"].value,
						ura_mailing: columns["ura_mailing"].value,
						ura_st_crep: columns["ura_st_crep"].value,
						ura_sac_crep: columns["ura_sac_crep"].value,
						ura_tagrecvoz: columns["ura_tagrecvoz"].value,
						ura_recvoz: columns["ura_recvoz"].value
					
					});

				}


				partes.push({
					service: columns["extendedResponseServicoDestino"].value,
					company: columns["companyName"].value,
					address: columns["selectedDestAddress"].value,
					service_2: columns["extendedResponseServicoDestino_2"].value,
					company_2: columns["companyName_2"].value,
					address_2: columns["selectedDestAddress_2"].value,
					horaIni: columns["horaIni"].value == null ? columns["horaIni"].value : columns["horaIni"].value.substring(0,5),
					horaFim: columns["horaFim"].value == null ? columns["horaFim"].value : columns["horaFim"].value.substring(0,5)
				});
				
				if ($('.filtro-regras2').val() === "ContingenciaOiTotal_Homg"){
					partes.push({						
						ura_cliente_fibra: columns["ura_cliente_fibra"].value
					});						
				}
				
				if ($('.filtro-regras2').val() === "ContingenciaOiTotal_Homg"){
					$scope.dados.push(collect(partes[0], partes[1], partes[2], partes[3]));
				}else{
					$scope.dados.push(collect(partes[0], partes[1], partes[2]));
				}



				
				$scope.$apply();
				

      }, function(err,num_rows){
        if(err){
          console.log(err.message);
        }
		
		/* Eliminar repetidos
		var s = $scope.dados;
		var repetidos = [];		
		for(var i=0; i<s.length;i++){
			for(var j=0; j<s.length;j++){
				if(i!==j){
					var item1 = s[i];
					var item2 = s[j];
					if(_.isEqual(_.omit(item1, ['id']), _.omit(item2, ['id']))){
						if(repetidos.indexOf($scope.dados[i].id)<0) repetidos.push($scope.dados[i].id);
					}
				}
			} 
		}
		console.log(repetidos);
		*/
		
		
                retornaStatusQuery(num_rows, $scope);
                $btn_gerar.button('reset');
				console.log("A consulta retornou " + num_rows + " registros");
				console.log('Executou -> ' + stmt);

                if(num_rows > 0){
                  var $btn = $view.find(".btn-atualizarGRAS");
				  $btn_exportar.prop("disabled", false);
                  $btn.prop("disabled", false);
                }else{
                  var $btn = $view.find(".btn-atualizarGRAS");
                  $btn.prop("disabled", true);
				  $btn_exportar.prop("disabled", true);
                }                

      },
       function(request){
        
      });  
	  
    }; // 
	
	
	 // Exporta planilha XLSX
  $scope.exportaXLSX = function () {
    var $btn_exportar = $(this);

    $btn_exportar.button('loading');

    var linhas  = [];
    linhas.push($scope.colunas.filterMap(function (col,index) {
		if (col.visible || col.visible === undefined) {
			if(col.displayName !== "Editar" && col.displayName !== "Excluir") return col.displayName; 
		} 
	}));
    $scope.dados.forEach(function (dado) {
      linhas.push($scope.colunas.filterMap(function (col,index) {
		  if (col.visible || col.visible === undefined) { return dado[col.field] === "" ? " " : dado[col.field]; } 
	}));
    });
	
	
	
	
	var works = [];
	
	if(linhas.length > 0) works.push({ name: ($('.filtro-regras2').val() || "contingencia"), data: linhas, table: true });


    var planilha = {
    creator: "Estalo",
    lastModifiedBy: loginUsuario || "Estalo",
    worksheets: works,
    autoFilter: false,    
    dataRows: { first: 1 }
  };

    var xlsx = frames["xlsxjs"].window.xlsx;
    planilha = xlsx(planilha, 'binary');
	
    var caminho = ($('.filtro-regras2').val() || "contingencia") +"_"+ formataDataHoraMilis(new Date()) + ".xlsx";
    fs.writeFileSync(caminho, planilha.base64, 'base64');
    childProcess.exec(caminho);

    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  };  
  function handleFile(e) {
	
	
	  var specificCols = "("+$scope.colunas.map(function(col){return col.displayName}).slice(3,$scope.colunas.length).join(',')+")";
	  var specificColsNum = $scope.colunas.length-2;
	  
	  var regra = $('.filtro-regras2').val();
	  var files = e.target.files;
	  var workbook ="";
	  var csv = "";
	  var deletar="";
	  try{
		workbook = XLSX.readFile(files[0].path);
		csv = XLSX.utils.sheet_to_csv(workbook.Sheets[regra]);
		deletar = "Delete from "+regra+";";
	  }catch(ex){
		return;
	  }	 
	  var linhas = csv.split('\n');
	  var inserts = "insert into "+regra+" "+specificCols+" values";
	  for (var i = 1; i < linhas.length; i++){
		  if(linhas[i].match(',,,,,,') === null && linhas[i]!=""){//tratar linha em branco		  
			  inserts +="('"+linhas[i].split(',').slice(1,specificColsNum).join("','")+"')";
			  if(i < linhas.length-2){
				  inserts +=',';			  
			  }
		  }
	  }
	  if(inserts.substring(inserts.length-1,inserts.length) === ','){
		  inserts = inserts.substring(0,inserts.length-1)
	  }else{
		inserts +=';';
	  }

	  
	  db.query(deletar + inserts, null, function (err, num_rows) {
            console.log("Executando query-> " + inserts + " " + num_rows);            
            if (err) {
                console.log("ERRO: " + err);
				alert("ERRO: " + err);
            } else {               
				alert("Regras importadas com sucesso");	
				$scope.listaDados();
            }
      });
  }
  
  function replaceBadInputs(val) {
	  // Replace impossible inputs as they appear
	  val = val.replace(/[^\dh:]/, "");
	  val = val.replace(/^[^0-2]/, "");
	  val = val.replace(/^([2-9])[4-9]/, "$1");
	  val = val.replace(/^\d[:h]/, "");
	  val = val.replace(/^([01][0-9])[^:h]/, "$1");
	  val = val.replace(/^(2[0-3])[^:h]/, "$1");      
	  val = val.replace(/^(\d{2}[:h])[^0-5]/, "$1");
	  val = val.replace(/^(\d{2}h)./, "$1");      
	  val = val.replace(/^(\d{2}:[0-5])[^0-9]/, "$1");
	  val = val.replace(/^(\d{2}:\d[0-9])./, "$1");
	  return val;
	}
	
	

	$('.cntg-hora').keyup(function(){
	  var val = this.value;
	  var lastLength;
	  do {

		lastLength = val.length;
		val = replaceBadInputs(val);
	  } while(val.length > 0 && lastLength !== val.length);
	  this.value = val;
	});


	$('.cntg-hora').blur(function(){
	  var val = this.value;
	  val = (/^(([01][0-9]|2[0-3])h)|(([01][0-9]|2[0-3]):[0-5][0-9])$/.test(val) ? val : "");
	  this.value = val;
	});
  
}

CtrlContingenciaRouting.$inject = ["$scope", "$globals"];