if (nodejs) {

var Connection = require('tedious').Connection,
    Request = require('tedious').Request;
    //TYPES = require('tedious').TYPES

function DB(id, log) {
  this.log = log || function () {};

  this.connection = null;
  this.state = "idle";

  this.use = "/* "+ id + " */ ";
  this.prefixo = "";

  var self = this;

  this.connect = function (config, callback) {
    if (self.connection) return;

    self.log("Iniciando conexão...");
    self.state = "connecting";
    self.connection = new Connection(config);

    self.connection.on('connect', function (err) {
      self.state = "idle";
      if (err) {
        self.log("Não foi possível conectar");
      }
      self.log("Conexão estabelecida");
      callback && callback(err);
    });

    //self.connection.on('debug', function (text) { self.log(text); });
  };

  this.disconnect = function () {
    if (!self.connection) return;
    self.log("Encerrando conexão...");
    self.connection.close();
    self.connection = null;
    self.state = "idle";
    self.log("Conexão encerrada");
  };

  this.isConnected = function () {
    return self.connection !== null;
  };

  this.isIdle = function () {
    return self.state === "idle";
  };

  this.query = function (stmt, on_row, on_done, setParameters) {
    if (!self.connection) return;

    var request = new Request(stmt, function (err, num_rows) {
      self.state = "idle";
      if (err) {
        self.log("Erro: " + err);
      } else {
        self.log(num_rows + " registros recebidos");
      }

      on_done && on_done(err, num_rows);
    });

    setParameters && setParameters(request);

    request.on('row', function (columns) {
      on_row && on_row(columns);
    });

    self.log("Executando query...");
    self.state = "querying";
    try{
        self.connection.execSql(request);
    } catch (e) {
      self.log("Erro " + e.name + ": " + e.message);
    }
  };
}

}