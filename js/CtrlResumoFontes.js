function CtrlResumoFontes($scope, $globals) {

    win.title = "Visão Geral Fontes";
    $scope.versao = versao;

    $scope.rotas = rotas;
    travaBotaoFiltro(0, $scope, "#pag-resumo-fontes", "Visão Geral Fontes");

    $scope.dados = [];
	$scope.xlsx = [];
	
	$scope.colunas = [];
	$scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.ordenacao = 'mes';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];  
	$scope.valor = 'p';
	$scope.dddsSel = false;

    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
		porDia: false
    };

	
	function geraColunas() {
	   var array = [];		
	   if ($('.filtro-aplicacao').val() == "URACAD"){	
			if($scope.valor !== "p") array.push({ field: "datahora", displayName: "Data", width: 90, pinned: true });
			
			if($scope.dddsSel){
				array.push(
				{ field: "ddd", displayName: 'DDD', width: 50, pinned: true }
				);
			}		
			
			array.push(
			    { field: "nomeBase", displayName: "Base", width: 150, pinned: true },
				{ field: "consultas", displayName: "Consultas", width: 100, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
				{ field: "sucesso", displayName: "Sucesso", width: 100, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
				{ field: "retornoOk", displayName: "Retorno Ok", width: 100, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
				{ field: "dadosIncompletos", displayName: "Dados Incompletos", width: 150, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "disponibilidade", displayName: "% Disponibilidade", width: 150, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "aproveitamento", displayName: "% Aproveitamento", width: 150, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "dadosIncompletosPerc", displayName: "% Incompletos", width: 170, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' }
			
			);
	   }
		return array;
	}

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
		
		
       $view = $("#pag-resumo-fontes");

        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoesGrupo($view,false,false,$scope,"Cadastro e Migração");
        //carregaAplicacoesComData($view);
        carregaSites($view);
		carregaBases($view,true);
		
		carregaDDDs($view,true);
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});
		
        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        carregaSegmentosPorAplicacao($scope, $view, true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
          carregaSegmentosPorAplicacao($scope, $view);
          //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
        });        


        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });
		
		$view.on("change", "select.filtro-base", function () {
            Estalo.filtros.filtro_bases = $(this).val();
        });
		
		
		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});
		
		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});
		
		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});		


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-fontes");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            } 
			$view.find("select.filtro-ddd").val() ? $scope.dddsSel = true : $scope.dddsSel = false;
			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this); 

        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-fontes");
        }

        $view.find("div.iframe").html('<iframe name="xlsjs" src="' + base + 'xls.html" frameborder="0" scrolling="no" />');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');

    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;
		$scope.filtros_usados = "";
		
		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("enable", true);
        $btn_exportar_xlsx.prop("disabled", true);
        $btn_exportar_dropdown.prop("enable", true);


        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_bases = $view.find("select.filtro-base").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_bases.length > 0) { $scope.filtros_usados += " Bases: " + filtro_bases; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }

        //$scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
		$scope.xlsx = [];
        var stmt = "";
		var fator = $scope.valor === "h" ? "hour" : $scope.valor === "d" ? "day" : "month"; 
		
		if ($('.filtro-aplicacao').val() == "URACAD"){
			
			if(filtro_ddds.length > 0){	
				stmt += " with cte as ("
				+ " 	select DDD,"+($scope.valor !== 'p' ? "DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DATAHORA," : "")+" NomeBase,"
				+ " 		SUM(case when Tipo = 'CONSULTA' then QtdExecucoes else 0 end) as Consultas,"
				+ "         SUM(case when Tipo = 'SUCESSO' then QtdExecucoes else 0 end) as Sucesso,"
				+ " 		SUM(case when Tipo = 'RETORNOOK' then QtdExecucoes else 0 end) as RetornoOK,"
				+ " 		SUM(case when Tipo = 'DADOSINCOMPLETOS' then QtdExecucoes else 0 end) as DadosIncompletos"
				+ " 	 from ParametrosFontesCadastro p"
				+ " 	join resumoicx c on p.CodAplicacao = c.CodAplicacao and p.CodIC = c.CodIC"
				+ " 	where 1 = 1"
				+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
				+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"	
				+ " 		and p.CodAplicacao = 'URACAD'";
				stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
				stmt += restringe_consulta("CodSite", filtro_sites, true);
				stmt += restringe_consulta("NomeBase", filtro_bases, true);
				stmt += restringe_consulta("DDD", filtro_ddds, true);
				stmt += " 	group by DDD,"+($scope.valor !== 'p' ? "DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0)," : "")+" NomeBase"
				+ " )"
				+ " select DDD,"+($scope.valor !== 'p' ? "DATAHORA," : "")+" NomeBase, Consultas, Sucesso, RetornoOK as [Retorno OK], DadosIncompletos as [Dados Incompletos],"
				+ " 	case when Consultas > 0 then cast(100.0 * Sucesso / Consultas as decimal(10,2)) else 0 end as [Disponibilidade %],"
				+ " 	case when Consultas > 0 then cast(100.0 * RetornoOK / Consultas as decimal(10,2)) else 0 end as [Aproveitamento %],"
				+ " 	case when Consultas > 0 then cast(100.0 * DadosIncompletos / Consultas as decimal(10,2)) else 0 end as [Dados Incompletos %]"
				+ " from cte"
				+ " order by "+($scope.valor !== 'p' ? "DATAHORA," : "")+" NomeBase";
			
			}else{
				stmt += " with cte as ("
				+ " 	select "+($scope.valor !== 'p' ? "DATEADD("+fator+", DATEDIFF("+fator+", 0, datahora), 0) as DATAHORA," : "")+" NomeBase,"
				+ " 		SUM(case when Tipo = 'CONSULTA' then Qtd else 0 end) as Consultas,"
				+ "         SUM(case when Tipo = 'SUCESSO' then Qtd else 0 end) as Sucesso,"
				+ " 		SUM(case when Tipo = 'RETORNOOK' then Qtd else 0 end) as RetornoOK,"
				+ " 		SUM(case when Tipo = 'DADOSINCOMPLETOS' then Qtd else 0 end) as DadosIncompletos"
				+ " 	 from ParametrosFontesCadastro p"
				+ " 	join ConsolidaItemControlex c on p.CodAplicacao = c.Cod_Aplicacao and p.CodIC = c.Cod_ItemDeControle"
				+ " 	where 1 = 1"
				+ " AND DataHora >= '" + formataDataHora(data_ini) + "'"
				+ " AND DataHora <= '" + formataDataHora(data_fim) + "'"	
				+ " 		and p.CodAplicacao = 'URACAD'";
				stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
				stmt += restringe_consulta("Cod_Site", filtro_sites, true);
				stmt += restringe_consulta("NomeBase", filtro_bases, true);
				stmt += " 	group by "+($scope.valor !== 'p' ? "DATEADD("+fator+", DATEDIFF("+fator+", 0, datahora), 0)," : "")+" NomeBase"
				+ " )"
				+ " select "+($scope.valor !== 'p' ? "DATAHORA," : "")+" NomeBase, Consultas, Sucesso, RetornoOK as [Retorno OK], DadosIncompletos as [Dados Incompletos],"
				+ " 	case when Consultas > 0 then cast(100.0 * Sucesso / Consultas as decimal(10,2)) else 0 end as [Disponibilidade %],"
				+ " 	case when Consultas > 0 then cast(100.0 * RetornoOK / Consultas as decimal(10,2)) else 0 end as [Aproveitamento %],"
				+ " 	case when Consultas > 0 then cast(100.0 * DadosIncompletos / Consultas as decimal(10,2)) else 0 end as [Dados Incompletos %]"
				+ " from cte"
				+ " order by "+($scope.valor !== 'p' ? "DATAHORA," : "")+" NomeBase";	
			}
		
		}


        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery(columns) {

			if (($('.filtro-aplicacao').val() == "URACAD")){
				
				var datahora = columns["DATAHORA"] !== undefined ? ($scope.valor === "h" ? formataDataBR(columns["DATAHORA"].value,"hora") : ($scope.valor === "d" ?  formataDataBR(columns["DATAHORA"].value) : formataDataBR(columns["DATAHORA"].value ,"mes" ))) : undefined;
				var nomeBase = columns["NomeBase"].value,
				consultas = columns["Consultas"].value,
				sucesso = columns["Sucesso"].value,
				retornoOk = columns["Retorno OK"].value,
				dadosIncompletos = columns["Dados Incompletos"].value,
				disponibilidade = columns["Disponibilidade %"].value,
				aproveitamento = columns["Aproveitamento %"].value,
				dadosIncompletosPerc = columns["Dados Incompletos %"].value,
				ddd = columns["DDD"] !== undefined ? columns["DDD"].value : undefined;
			 
			  
			  $scope.dados.push({
				datahora: datahora,
				nomeBase: nomeBase,
				consultas: consultas,
				sucesso: sucesso,
				retornoOk: retornoOk,
				dadosIncompletos: dadosIncompletos,
				disponibilidade: disponibilidade,
				aproveitamento: aproveitamento,
				dadosIncompletosPerc: dadosIncompletosPerc,
				ddd: ddd
			  });
			  
			  $scope.xlsx.push([
			    datahora,
                nomeBase,
				consultas,
				sucesso,
				retornoOk,
				dadosIncompletos,
				disponibilidade,
				aproveitamento,
				dadosIncompletosPerc,
				ddd
            ]);
			}
			
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}

        }

        db.query(stmt, executaQuery, function (err, num_rows) {            
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_xlsx.prop("enable", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }            
        });

    };

    // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.xlsx);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'ResumoFontes' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };

}
CtrlResumoFontes.$inject = ['$scope', '$globals'];
