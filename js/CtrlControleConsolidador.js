function CtrlControleConsolidador($scope, $globals) {

    win.title = "Controle Consolidador";
    $scope.versao = versao;
    $scope.rotas = rotas;

    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    var agora = new Date();

    var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    $scope.linhaAtual = {}

    $scope.gridDados = {
        rowSelection: 'single',
        suppressRowClickSelection: false,
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        onRowClicked: criarInstancia,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        columnDefs: [{
                headerName: 'Aplicação',
                field: 'codAplicacao'
            },
            {
                headerName: 'Data Hora',
                field: 'dataHora'
            },
            {
                headerName: 'Chamadas',
                field: 'qtdChamadas'
            }, {
                headerName: 'Consolidadas',
                field: 'qtdChamadas2'
            },
            {
                headerName: 'Diferença',
                field: 'diferenca'
            },
            {
                headerName: 'Diferença Percentual',
                field: 'diferencaPercent'
            }
        ],
        rowData: [],
        localeText: $scope.localeGridText
    };

    $scope.filtros = {
        consolFinalizados: false,
        diferencaDia: false,
        forcarConsolidadorNormal: false,
    }

    $scope.btnCriarInstancia = '';

    $scope.permCriarInstancia = true;
    var p = cache.aclbotoes.map(function (b) {
        return b.idview;
    }).indexOf('pag-controle-consolidador');
    if (p >= 0) {
        var teste = cache.aclbotoes[p].permissoes.split(',');
        for (var i = 0; i < teste.length; i++) {
            if (teste[i] === 'instancia-consol') {
                $scope.permCriarInstancia = true;
            }
        }
    }

    var $view;

    $scope.dados = [];
    $scope.csv = [];
    $scope.colunas = [];
    $scope.difPorAplicacao = [];
    $scope.difAplicacoes = [];

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-controle-consolidador");

        $scope.gridDados.api.hideOverlay();

        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });

        componenteDataHora($scope, $view);
        carregaAplicacoes($view, false, false, $scope);

        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-controle-consolidador");

            $scope.gridDados.api.showLoadingOverlay();
            $scope.colunas = geraColunas();

            var consolFinalizados = [];
            var stmtConsolFinalizados = "SELECT ID FROM ControleExtratores WHERE DataIni = DataFim AND (ID LIKE 'TNLConsolidador%' OR ID LIKE 'TNLConsolAtrasadas%') "
            db.query(stmtConsolFinalizados, function (columns) {
                var numID = columns[0].value.replace(/\D+(\d+)/g, '$1');
                if (parseInt(numID) > 199) {
                    consolFinalizados.push(columns[0].value);
                }
            }, function (err, num_rows) {
                if (err) {
                    console.log('Erro ao buscar consolidadores finalizados. ' + err);
                } else {
                    console.log('Numero de consolidadores encontrados: ' + consolFinalizados.length);
                    if (consolFinalizados.length > 0) {
                        var stmtDelFinalizados = "DELETE FROM ControleExtratores WHERE DataINI = DataFIM AND ID IN (";
                        for (var i = 0; i < consolFinalizados.length; i++) {
                            stmtDelFinalizados = stmtDelFinalizados + "'" + consolFinalizados[i] + "',";
                        }

                        stmtDelFinalizados = stmtDelFinalizados.substr(0, stmtDelFinalizados.length - 1);
                        stmtDelFinalizados = stmtDelFinalizados.concat(')');
                        console.log('Query para DELETAR: ' + stmtDelFinalizados);
                        db.query(stmtDelFinalizados, function () {}, function (err, num_rows) {
                            if (err) {
                                console.log('Erro ao remover consolidadores finalizados');
                            } else {
                                console.log('Numero de consolidadores deletados: ' + num_rows);
                                // Testar se data início é maior que a data fim
                                var data_ini = $scope.periodo.inicio,
                                    data_fim = $scope.periodo.fim;
                                var testedata = testeDataMaisHora(data_ini, data_fim);
                                if (testedata !== "") {
                                    setTimeout(function () {
                                        atualizaInfo($scope, testedata);
                                        effectNotification();
                                        $view.find(".btn-gerar").button('reset');
                                    }, 500);
                                    return;
                                }

                                $scope.dados = [];
                                $scope.difPorAplicacao = [];
                                $scope.listaDiferenca.apply(this);
                            }
                        });
                    } else {
                        // Testar se data início é maior que a data fim
                        var data_ini = $scope.periodo.inicio,
                            data_fim = $scope.periodo.fim;
                        var testedata = testeDataMaisHora(data_ini, data_fim);
                        if (testedata !== "") {
                            setTimeout(function () {
                                atualizaInfo($scope, testedata);
                                effectNotification();
                                $view.find(".btn-gerar").button('reset');
                            }, 500);
                            return;
                        }

                        $scope.dados = [];
                        $scope.difPorAplicacao = [];
                        $scope.listaDiferenca.apply(this);
                    }

                }
            });
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-controle-consolidador");
        }
    });

    $scope.listaDiferenca = function () {

        $scope.gridDados.rowData = [];
        $scope.gridDados.api.setRowData([]);

        var $btn_exportar = $view.find(".btn-exportarCSV");
        $btn_exportar.prop("disabled", true);

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        console.log("FiltroAplicacoes:" + filtro_aplicacoes);

        if ($scope.filtros.diferencaDia) {
            var $btn_gerar = $(this);
            $btn_gerar.button("loading");
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var stmt =
                " with cte as (" +
                " select Cod_Aplicacao as CodAplicacao, dateadd(day, datediff(day, 0, Dat_Referencia), 0) as DataHora," +
                " sum(QtdFinalizadas+QtdDerivadas+QtdAbandonadas+QtdTransfURA) as QtdChamadas" +
                " from ResumoDesempenhoGeralHora" +
                " where Dat_Referencia >= '" + formataHoraZero(formataDataHora($scope.periodo.inicio)) + "' and Dat_Referencia < '" + formataHoraZero(formataDataHora($scope.periodo.fim)) + "'" +
                " and Cod_Segmento = ''";
            // if (filtro_aplicacoes.length > 0) stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
            stmt +=
                " group by Cod_Aplicacao, dateadd(day, datediff(day, 0, Dat_Referencia), 0)" +
                " ), cte2 as (" +
                " select Cod_Aplicacao as CodAplicacao, Dat_Referencia as DataHora," +
                " sum(QtdFinalizadas+QtdDerivadas+QtdAbandonadas+QtdTransfURA) as QtdChamadas" +
                " from ResumoDesempenhoGeral" +
                " where Dat_Referencia >= '" + formataHoraZero(formataDataHora($scope.periodo.inicio)) + "' and Dat_Referencia < '" + formataHoraZero(formataDataHora($scope.periodo.fim)) + "'" +
                " and Cod_Segmento = ''" +
                " group by Cod_Aplicacao, Dat_Referencia" +
                " )" +
                " select cte.CodAplicacao, cte.DataHora," +
                " cte.QtdChamadas, isnull(cte2.QtdChamadas, 0) as QtdChamadas2," +
                " cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) as Diferenca," +
                " str((100.0 * (cte.QtdChamadas - isnull(cte2.QtdChamadas, 0))) / cte.QtdChamadas, 4, 2) + '%' as DiferencaPercent" +
                " from cte" +
                " left outer join cte2 on cte.CodAplicacao = cte2.CodAplicacao and cte.DataHora = cte2.DataHora" +
                " where cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) <> 0" +
                " order by cte.CodAplicacao, cte.DataHora";
            db.query(stmt, function (columns) {
                var codAplicacao = columns[0].value,
                    dataHora = columns[1].value,
                    qtdChamadas = columns[2].value,
                    qtdChamadas2 = columns[3].value,
                    diferenca = columns[4].value,
                    diferencaPercent = columns[5].value

                $scope.dados.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
                });

                $scope.gridDados.rowData.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
                });

                $scope.csv.push([
                    codAplicacao,
                    formataDataHoraBR(dataHora),
                    qtdChamadas,
                    qtdChamadas2,
                    diferenca,
                    diferencaPercent
                ]);

                if ($scope.difPorAplicacao[codAplicacao]) {
                    $scope.difPorAplicacao[codAplicacao] += formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = true;
                } else {
                    $scope.difPorAplicacao[codAplicacao] = formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = false;
                }
            }, function (err, num_rows) {
                if (err) {
                    console.log('Erro ao buscar diferenças: ' + err);
                } else {

                    $scope.gridDados.api.hideOverlay();
                    retornaStatusQuery($scope.gridDados.rowData.length, $scope);

                    $scope.gridDados.api.setRowData($scope.gridDados.rowData);
                    $scope.gridDados.api.refreshCells({force: true});

                    for (var i in $scope.difAplicacoes) {
                        console.log('Iterando para ' + i);
                        if ($scope.difAplicacoes[i]) console.log(i + ' encontrado');
                    }
                    $btn_gerar.button("reset");
                    if ($scope.dados.length > 0) {
                        $btn_exportar.prop("disabled", false);
                    }
                }

            });
            console.log(stmt);

        } else {
            var $btn_gerar = $(this);
            $btn_gerar.button("loading");
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var stmt =
                "WITH cte AS" +
                " ( SELECT CodAplicacao," +
                " dateadd(hour, datediff(hour, 0, DataHora_Inicio), 0) AS DataHora," +
                " count(*) AS QtdChamadas" +
                " FROM IVRCDR" +
                " WHERE DataHora_Inicio >= '" + formataHoraCheia(formataDataHora($scope.periodo.inicio)) + "'" +
                " AND DataHora_Inicio < '" + formataHoraCheia(formataDataHora($scope.periodo.fim)) + "' ";
            if (filtro_aplicacoes.length > 0) {
                stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
            } else {
                stmt += "AND CodAplicacao <> '' ";
            }
            // "AND CodAplicacao <> ''"+
            stmt +=
                " GROUP BY CodAplicacao," +
                " dateadd(hour, datediff(hour, 0, DataHora_Inicio), 0))," +
                " cte2 AS" +
                " ( SELECT CodAplicacao," +
                " DataHora," +
                " sum(QtdChamadas) AS QtdChamadas" +
                " FROM ResumoAtendimentoURA" +
                " WHERE DataHora >= '" + formataHoraCheia(formataDataHora($scope.periodo.inicio)) + "'" +
                " AND DataHora < '" + formataHoraCheia(formataDataHora($scope.periodo.fim)) + "'" +
                " AND TipoServidor = 'A'" +
                " GROUP BY CodAplicacao," +
                " DataHora)" +
                " SELECT cte.CodAplicacao," +
                " cte.DataHora," +
                " cte.QtdChamadas," +
                " isnull(cte2.QtdChamadas, 0) AS QtdChamadas2," +
                " cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) AS Diferenca," +
                " str((100.0 * (cte.QtdChamadas - isnull(cte2.QtdChamadas, 0))) / cte.QtdChamadas, 4, 2) + '%' AS DiferencaPercent" +
                " FROM cte" +
                " LEFT OUTER JOIN cte2 ON cte.CodAplicacao = cte2.CodAplicacao" +
                " AND cte.DataHora = cte2.DataHora" +
                " WHERE cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) <> 0" +
                " ORDER BY cte.CodAplicacao, cte.DataHora"
            // var stmt = "uspVerificaConsolidacao2 \'" + formataDataHora($scope.periodo.inicio) + "','" + formataDataHora($scope.periodo.fim) + "'";
            db.query(stmt, function (columns) {
                var codAplicacao = columns[0].value,
                    dataHora = columns[1].value,
                    qtdChamadas = columns[2].value,
                    qtdChamadas2 = columns[3].value,
                    diferenca = columns[4].value,
                    diferencaPercent = columns[5].value

                $scope.dados.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
                });

                $scope.gridDados.rowData.push({
                    codAplicacao: codAplicacao,
                    dataHora: formataDataHoraBR(dataHora),
                    qtdChamadas: qtdChamadas,
                    qtdChamadas2: qtdChamadas2,
                    diferenca: diferenca,
                    diferencaPercent: diferencaPercent
                });

                $scope.csv.push([
                    codAplicacao,
                    formataDataHoraBR(dataHora),
                    qtdChamadas,
                    qtdChamadas2,
                    diferenca,
                    diferencaPercent
                ]);

                if ($scope.difPorAplicacao[codAplicacao]) {
                    $scope.difPorAplicacao[codAplicacao] += formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = true;
                } else {
                    $scope.difPorAplicacao[codAplicacao] = formataDataHora(dataHora) + ',';
                    $scope.difAplicacoes[codAplicacao] = false;
                }
            }, function (err, num_rows) {

                if (err) {
                    console.log('Erro ao buscar diferenças: ' + err);
                } else {

                    $scope.gridDados.api.hideOverlay();
                    retornaStatusQuery($scope.dados.length, $scope);

                    $scope.gridDados.api.setRowData($scope.gridDados.rowData);
                    $scope.gridDados.api.refreshCells({force: true});

                    $btn_gerar.button("reset");
                    if ($scope.dados.length > 0) {
                        $btn_exportar.prop("disabled", false);
                    }
                }
            });
            console.log(stmt);
        }

    }

    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var colunas = [];
        for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
          colunas.push($scope.gridDados.columnDefs[i].headerName);
        }

        // colunas.pop();

        console.log(colunas);

        var dadosExcel = [colunas].concat($scope.csv);

        var dadosComFiltro = [
            ['Filtros:' + $scope.filtros_usados]
        ].concat(dadosExcel);

        var ws = XLSX.utils.aoa_to_sheet(dadosExcel);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'controleConsolidador_' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

    function geraColunas() {
        var colunas = [];
        for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
            colunas.push({
                displayName: $scope.gridDados.columnDefs[i].headerName,
            });
        }
        return colunas;
    }

    function reverteDataBR(dataBR) {
        return dataBR.replace(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}:\d{2})/, '$3-$2-$1 $4');
    }

    function formataHoraZero(dataHora) {
        return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 00:00:00');
    }

    function formataHoraCheia(dataHora) {
        return dataHora.replace(/(\d{4}-\d{2}-\d{2} \d{2}).+/g, '$1:00:00');
    }

    function criarInstancia() {
        if ($scope.permCriarInstancia) {
            (function(linhas) {
                var linha = JSON.parse(JSON.stringify(linhas[0]));

                confirme('Deseja criar uma instância para a aplicação ' + linha.codAplicacao + '?', 'Criar instância', 'Sim', function (apertouNoSIM) {
                    if ($scope.filtros.diferencaDia) {
                        // Redefine para corrigir bug
                        linha = JSON.parse(JSON.stringify($scope.gridDados.api.getSelectedRows()[0]));

                        if (apertouNoSIM) {
                            var server = [];
                            var stmtServer = "select top 1 hostname from ControleExtratores where id like 'tnlconsol%' and habilitado = 1"
                            db.query(stmtServer, function (c) {
                            server = c[0].value;
                            }, function (err, numRows) {
                                if(err){
                                    console.log(err);
                                } else {
                                    console.log("Servidor "+server);
                            var stmtSelect = "SELECT * FROM ControleExtratores WHERE ID LIKE '%TNLConsoli%' AND Hostname = '"+server+"' AND Habilitado = 1";
                            var arrayNormal = [];

                            db.query(stmtSelect, function (c) {
                                arrayNormal.push(c[1].value);
                            }, function (err, numRows) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    arrayNormal.sort();

                                    if (arrayNormal.length > 0) {
                                        for (var i in arrayNormal) {
                                            arrayNormal[arrayNormal[i]] = true;
                                        }
                                    }
                                    
                                    var stmtChecarDuplicado = "SELECT * FROM ControleExtratores WHERE ID LIKE '%TNLConsoli%' AND HOSTNAME = '"+server+"' AND DATAFIM = DATAADD(HOUR, 1, '" + reverteDataBR(linha.dataHora).replace(/(\d{4}-\d{2}-\d{2} ).+/g, '$1 23:00:00') + "') AND Config LIKE '%" + linha.codAplicacao + "' and Status <> ''"

                                    console.log('CHECANDO COM \n' + stmtChecarDuplicado);
                                    db.query(stmtChecarDuplicado, function () {
                                        console.log('Consultando se a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora + ' já possui instância');
                                    }, function (err, numRows) {
                                        if (numRows > 0) {
                                            alerte('Já existe uma instância para a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora, 'Erro');
                                        } else {
                                            // CONSOLIDADOR NORMAL
                                            var idValido = false;
                                            var idUtilizado = 200;

                                            while (!idValido) {
                                                if (arrayNormal['TNLConsolidador' + idUtilizado.toString()]) {
                                                    if (idUtilizado <= 999) {
                                                        idUtilizado += 1;
                                                    } else {
                                                        alerte('Limite interno de consolidadores atingido. Máximo: 999');

                                                        break;
                                                    }
                                                } else {
                                                    idValido = true;

                                                    console.log('ID válido encontrado: ' + idUtilizado);
                                                }
                                            }
                                        
                                            if (idValido) {
                                               
                                                var stmtInsertNormal = "INSERT INTO ControleExtratores VALUES ('" +server+ "', 'TNLConsolidador" + idUtilizado.toString() + "', 1, '" + reverteDataBR(linha.dataHora).replace(/(\d{4}-\d{2}-\d{2} ).+/g, '$1 23:00:00') + "', DATEADD(HOUR, 1, '" + reverteDataBR(linha.dataHora).replace(/(\d{4}-\d{2}-\d{2} ).+/g, '$1 23:00:00') + "'), 'APLICACAO=" + linha.codAplicacao + "', '', '')";

                                                console.log('INSERINDO COM \n' + stmtInsertNormal);

                                               db.query(stmtInsertNormal, function () {
                                                    console.log('Inserindo instância com o ID TNLConsolidador' + idUtilizado.toString());
                                                }, function (err, numRows) {
                                                    console.log('NumRows: ' + numRows + '\nErr: ' + err);

                                                    alerte('Instancia criada para a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora + ' com o ID TNLConsolidador' + idUtilizado.toString(), 'Sucesso');
                                                });

                                            
                                            }
                                        }
                                    });
                                
                                }
                            });
                        }
                        });
                    }

                    } else {
                        // Redefine para corrigir bug
                        linha = JSON.parse(JSON.stringify($scope.gridDados.api.getSelectedRows()[0]));

                        if (apertouNoSIM) {
                            var server = [];
                            var stmtServer = "select top 1 hostname from ControleExtratores where id like 'tnlconsol%' and habilitado = 1"
                            db.query(stmtServer, function (c) {
                            server = c[0].value;
                            }, function (err, numRows) {
                                if(err){
                                    console.log(err);
                                } else {
                            console.log("Servidor "+server);
                            var stmtSelect = "SELECT * FROM ControleExtratores WHERE (ID LIKE '%TNLConsolA%' OR ID LIKE '%TNLConsoli%') and Hostname = '"+server+"'";
                            var ids = [];
                            var arrayNormal = [];
                            var arrayAtrasado = [];

                            db.query(stmtSelect, function (c) {
                                ids.push(c[1].value);
                            }, function (err, numRows) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    for (var i = 0; i < ids.length; i++) {
                                        var r = /\w+\d{3}/g.exec(ids[i]);

                                        if (r) {
                                            if (r[0].indexOf("Atrasadas") > -1) {
                                                arrayAtrasado.push(r[0]);
                                            } else {
                                                arrayNormal.push(r[0]);
                                            }
                                        }
                                    }

                                    arrayNormal.sort();
                                    arrayAtrasado.sort();

                                    if (arrayNormal.length > 0) {
                                        for (var i in arrayNormal) {
                                            arrayNormal[arrayNormal[i]] = true;
                                        }
                                    }
                                    if (arrayAtrasado.length > 0) {
                                        for (var i in arrayAtrasado) {
                                            arrayAtrasado[arrayAtrasado[i]] = true;
                                        }
                                    }
                                    
                                
                                var stmtChecarDuplicado = "SELECT * FROM ControleExtratores WHERE ID LIKE '%TNLConsoli%' AND HOSTNAME = '"+server+"' AND DATAFIM = DATAADD(HOUR, 1, '" + reverteDataBR(linha.dataHora).replace(/(\d{4}-\d{2}-\d{2} ).+/g, '$1 23:00:00') + "') AND Config LIKE '%" + linha.codAplicacao + "' and Status <> ''"

                                console.log('CHECANDO COM \n' + stmtChecarDuplicado);

                                    db.query(stmtChecarDuplicado, function () {
                                        console.log('Consultando se a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora + ' já possui instância');
                                    }, function (err, numRows) {
                                        if (numRows > 0) {
                                            alerte('Já existe uma instância para a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora, 'Erro');
                                        } else {
                                            if ($scope.filtros.forcarConsolidadorNormal || /^S2S.+$/g.test(linha.codAplicacao) || (linha.consolidadas > linha.chamadas) || linha.consolidadas == 0 || linha.chamadas > 1000) {
                                                // CONSOLIDADOR NORMAL
                                                var idValido = false;
                                                var idUtilizado = 200;

                                                while (!idValido) {
                                                    if (arrayNormal['TNLConsolidador' + idUtilizado.toString()]) {
                                                        if (idUtilizado <= 999) {
                                                            idUtilizado += 1;
                                                        } else {
                                                            alerte('Limite interno de consolidadores atingido. Máximo: 999');

                                                            break;
                                                        }
                                                    } else {
                                                        idValido = true;

                                                        console.log('ID válido encontrado: ' + idUtilizado);
                                                    }
                                                }

                                                if (idValido) {
                                                    
                                                        var stmtInsertNormal = "INSERT INTO ControleExtratores VALUES ( '" + server+ "', 'TNLConsolidador" + idUtilizado.toString() + "', 1, '" + reverteDataBR(linha.dataHora) + "', DATEADD(HOUR, 1, '" + reverteDataBR(linha.dataHora) + "'), 'APLICACAO=" + linha.codAplicacao + "', '', '')";

                                                        console.log('INSERINDO COM \n' + stmtInsertNormal);

                                                        db.query(stmtInsertNormal, function () {
                                                            console.log('Inserindo instância com o ID TNLConsolidador' + idUtilizado.toString());
                                                        }, function (err, numRows) {
                                                            console.log('NumRows: ' + numRows + '\nErr: ' + err);

                                                            alerte('Instancia criada para a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora + ' com o ID TNLConsolidador' + idUtilizado.toString(), 'Sucesso');
                                                        });
                                                }
                                            } else {
                                                // CONSOLIDADOR ATRASADAS
                                                var idValido = false;
                                                var idUtilizado = 200;

                                                while (!idValido) {
                                                    if (arrayAtrasado['TNLConsolAtrasadas' + idUtilizado.toString()]) {
                                                        if (idUtilizado <= 999) {
                                                            idUtilizado += 1;
                                                        } else {
                                                            alerte('Limite interno de consolidadores atingido. Máximo: 999');

                                                            break;
                                                        }
                                                    } else {
                                                        idValido = true;

                                                        console.log('ID válido encontrado: ' + idUtilizado);
                                                    }
                                                }

                                                if (idValido) {
                                                   
                                                        var stmtInsertAtrasadas = "INSERT INTO ControleExtratores VALUES ('" + server+ "', 'TNLConsolAtrasadas" + idUtilizado.toString() + "', 1, '" + reverteDataBR(linha.dataHora) + "', DATEADD(HOUR, 1, '" + reverteDataBR(linha.dataHora) + "'), 'APLICACAO=" + linha.codAplicacao + "', '', '')";

                                                        console.log('INSERINDO COM \n' + stmtInsertAtrasadas);

                                                   db.query(stmtInsertAtrasadas, function () {
                                                        console.log('Inserindo instância com o ID TNLConsolAtrasadas' + idUtilizado.toString());
                                                    }, function (err, numRows) {
                                                        console.log('NumRows: ' + numRows + '\nErr: ' + err);

                                                        alerte('Instancia criada para a aplicação ' + linha.codAplicacao + ' na hora ' + linha.dataHora + ' com o ID TNLConsolAtrasadas' + idUtilizado.toString(), 'Sucesso');
                                                    });
                                                }
                                            }
                                        }
                                    });
                                
                                }
                            });
                        }
                    });
                    }
                }
                
                });
                
            })($scope.gridDados.api.getSelectedRows())
            
        }
    }
}

CtrlControleConsolidador.$inject = ['$scope', '$globals'];
