function CtrlAtendimentoHumano($scope, $globals) {
  win.title = "Atendimento Humano"; //Alex 27/02/2014
  
  $scope.versao = versao;
  $scope.rotas = rotas;
  $scope.limite_registros = 0;
  $scope.incrementoRegistrosExibidos = 100;
  $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

  //Alex 08/02/2014
  travaBotaoFiltro(0, $scope, "#pag-resumo-pesq-serv", "Atendimento Humano");

  //Alex 24/02/2014
  $scope.status_progress_bar = 0;

  $scope.dados = [];
  $scope.csv = [];
  $scope.colunas = [];
  
  $scope.gridDados = {
    data: "dados",
    columnDefs: "colunas",
    enableColumnResize: true,
    enablePinning: true
  };

  $scope.log = [];
  $scope.aplicacoes = []; // FIXME: copy
  $scope.sites = [];
  $scope.segmentos = [];
  $scope.ordenacao = ["data_hora"];
  $scope.decrescente = false;
  $scope.iit = false;
  $scope.parcial = false;
  $scope.agrupaApls = false;
  $scope.servico = true;
  $scope.item = false;
  $scope.consolidado = false;
  $scope.resposta3 = false;

  // Filtros
  $scope.filtros = {
    aplicacoes: [],
    // ALEX - 29/01/2013
    sites: [],
    segmentos: [],
    isHFiltroED: false
  };

  // Filtro: ddd
  $scope.ddds = cache.ddds;

  $scope.porDDD = null;
  $scope.valor = "h";
  $scope.valor2 = "s";

  //02/06/2014 Flag
  $scope.porRegEDDD = $globals.agrupar;

  $scope.aba = 2;

  function geraColunas() {
    var array = [];
	
	if($scope.consolidado){

    array.push(
      /*{
        field: "datReferencia",
        displayName: "Data",
        width: 120,
        pinned: true
      },*/
	  {
        field: "CodEmpresa",
        displayName: "Empresa",
        width: 130,
        pinned: false
      },
      {
        field: "QTDPESQUISA",
        displayName: "Qtd pesquisa",
        width: 130,
        pinned: false
      },
      {
        field: "NPS",
        displayName: "NPS %",
        width: 150,
        pinned: false
      },
	  {
        field: "SATISFACAO",
        displayName: "Satisfação",
        width: 150,
        pinned: false
      },
	  {
        field: "TX_RESOLUCAO",
        displayName: "TX resolução %",
        width: 150,
        pinned: false
      }      
    );
	}else{
		array.push(
      {
        field: "datReferencia",
        displayName: "Data",
        width: 120,
        pinned: true
      },
      {
        field: "OrdemNota",
        displayName: "Ordem nota",
        width: 130,
        pinned: false
      },
      {
        field: "CodEmpresa",
        displayName: "Empresa",
        width: 130,
        pinned: false
      },
      {
        field: "CodProduto",
        displayName: "Produto",
        width: 150,
        pinned: false
      }
	 );
	 
	 var filtro_notas = $view.find("select.filtro-nota").val() || [];
	 
	 if(filtro_notas.length > 0){
			for (var i = 0; i < filtro_notas.length; i++){
				array.push({ field: "n"+filtro_notas[i]+"", displayName: "N|"+filtro_notas[i]+"", width: 70, pinned: false });
			}
		
	 }else{
		 array.push(
		  { field: "n0", displayName: "N|0", width: 70, pinned: false },
		  { field: "n1", displayName: "N|1", width: 70, pinned: false },
		  { field: "n2", displayName: "N|2", width: 70, pinned: false },
		  { field: "n3", displayName: "N|3", width: 70, pinned: false },
		  { field: "n4", displayName: "N|4", width: 70, pinned: false },
		  { field: "n5", displayName: "N|5", width: 70, pinned: false },
		  { field: "n6", displayName: "N|6", width: 70, pinned: false },
		  { field: "n7", displayName: "N|7", width: 70, pinned: false },
		  { field: "n8", displayName: "N|8", width: 70, pinned: false },
		  { field: "n9", displayName: "N|9", width: 70, pinned: false },
		  { field: "n10", displayName: "N|10", width: 70, pinned: false }
		);
	 }
		
	}
    return array;
  }

  // Filtros: data e hora
  var agora = new Date();

  //var dat_consoli = new Date(teste_data);

  //Alex 03/03/2014
  var inicio, fim;
  Estalo.filtros.filtro_data_hora[0] !== undefined
    ? (inicio = Estalo.filtros.filtro_data_hora[0])
    : (inicio = ontemInicio());
  Estalo.filtros.filtro_data_hora[1] !== undefined
    ? (fim = Estalo.filtros.filtro_data_hora[1])
    : (fim = ontemFim());

  $scope.periodo = {
    inicio: inicio,
    fim: fim,
    min: new Date(2013, 11, 1),
    max: agora // FIXME: atualizar ao virar o dia
    /*min: ontem(dat_consoli),
            max: dat_consoli*/
  };

  var $view;

  $scope.$on("$viewContentLoaded", function() {
    $view = $("#pag-resumo-pesq-serv");

    componenteDataMaisHora($scope, $view);
	carregaNotas($view);
	carregaProdutosPesquisa($view);
	carregaEmpresas($view,true);

    $view.on("click", ".btn-exportar", function() {
      $scope.exportaXLSX.apply(this);
    });

    // Limpa filtros Alex 03/03/2014
    $view.on("click", ".btn-limpar-filtros", function() {
      $scope.porRegEDDD = false;
      $scope.limparFiltros.apply(this);
    });

    $scope.limparFiltros = function() {
      iniciaFiltros();
      componenteDataMaisHora($scope, $view, true);

      var partsPath = window.location.pathname.split("/");
      var part = partsPath[partsPath.length - 1];

      var $btn_limpar = $view.find(".btn-limpar-filtros");
      $btn_limpar.prop("disabled", true);
      $btn_limpar.button("loading");

      setTimeout(function() {
        $btn_limpar.button("reset");
        //window.location.href = part + window.location.hash +'/';
      }, 500);
    };

    // Botão agora Alex 03/03/2014
    $view.on("click", ".btn-agora", function() {
      $scope.agora.apply(this);
    });

    $scope.agora = function() {
      iniciaAgora($view, $scope);
    };

    // Botão abortar Alex 23/05/2014
    $view.on("click", ".abortar", function() {
      $scope.abortar.apply(this);
    });

    //Alex 23/05/2014
    $scope.abortar = function() {
      abortar($scope, "#pag-resumo-pesq-serv");
    };

    // Lista chamadas conforme filtros
    $view.on("click", ".btn-gerar", function() {
      limpaProgressBar($scope, "#pag-resumo-pesq-serv");
      $scope.colunas = geraColunas();
      $scope.listaDados.apply(this);
    });
	
	$view.on("change", '#chkConsol', function() {		
		$scope.consolidado = $('#chkConsol').prop('checked');		
	}); 
	
	$view.on("change", '#chkResposta3', function() {		
		$scope.resposta3 = $('#chkResposta3').prop('checked');		
	});

	
	
	  // Marca todos os produtos pesquisa
      $view.on("click", "#alinkProdutoPesquisa", function() {
          marcaTodosIndependente($('.filtro-produto-pesquisa'), 'prodpesq')
      });

      // Marca todos os empresas
      $view.on("click", "#alinkEmp", function() {
          marcaTodosIndependente($('.filtro-empresa'), 'empresa')
      });

      // Marca todos os notas
      $view.on("click", "#alinkNota", function() {
          marcaTodosIndependente($('.filtro-nota'), 'nota')
      });
	

    $view
      .find("div.iframe")
      .html(
        '<iframe name="xlsxjs" src="' +
          base +
          'xlsx.html" frameborder="0" scrolling="no" />'
      );
	  $('input:checkbox[id=chkConsol]').css('margin','-3px 0px 0px 0px');
	  $('input:checkbox[id=chkResposta3]').css('margin','-3px 0px 0px 0px');
	  
  });

  // Lista chamadas conforme filtros
  $scope.listaDados = function() {
	  
	$scope.csv = [];
    $globals.numeroDeRegistros = 0;

    var $btn_exportar = $view.find(".btn-exportar");
    var $btn_exportarCSV = $view.find(".btn-exportarCSV");
    $btn_exportar.prop("disabled", true);
    $btn_exportarCSV.prop("disabled", true);

    var $btn_gerar = $(this);
    $btn_gerar.button("loading");

    var data_ini = $scope.periodo.inicio,
      data_fim = $scope.periodo.fim;

    var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
	var filtro_notas = $view.find("select.filtro-nota").val() || [];
	var filtro_produtos = $view.find("select.filtro-produto-pesquisa").val() || [];

    //filtros usados
    $scope.filtros_usados =
      " Período: " +
      formataDataBR($scope.periodo.inicio) +
      "" +
      " até " +
      formataDataBR($scope.periodo.fim);
    if (filtro_empresas.length > 0) {
      $scope.filtros_usados += " Empresas: " + filtro_empresas;
    }
    if (filtro_notas.length > 0) {
      $scope.filtros_usados += " Notas: " + filtro_notas;
    }
    if (filtro_produtos.length > 0) {
      $scope.filtros_usados += " Produtos: " + filtro_produtos;
    }

    $scope.dados = [];
    var stmt = "";
    var executaQuery = "";
    stmt = db.use;
	
	
	if($scope.consolidado){
		stmt += "with b as ("
		+ " select COUNT(*) AS QTDPESQUISASVALIDASNPS,"
		+ " sum(case when RESPOSTA_3 > 8  then 1 else 0 end) as PROMOTORES,"
		+ " sum(case when RESPOSTA_3 < 7  then 1 else 0 end) as DETRATORES,"
		+ " sum(RESPOSTA_1 * case when RESPOSTA_1 IS NOT NULL  then 1 else 0 end) AS S1,"
		+ " sum(case when RESPOSTA_1 IS NOT NULL then 1 else 0 end)  as S2,"
		+ " sum(case when RESPOSTA_3 IS NOT NULL then 1 else 0 end) AS QTDPESQUISASVALIDAS," 
		+ " isnull(EMPRESA,'') as empresa, CONVERT(DATE,DataHora_Inicio) AS DATAHORA,"
		+ " sum(case when (RESPOSTA_2 = 1) "+($scope.resposta3 ? "AND RESPOSTA_3 IS NOT NULL" : "" )+"  then 1 else 0 end) AS NOTA1"  //
		+ " from pesquisaqualidade"
		+ " WHERE 1=1"
		+ " AND CONVERT(DATE,DataHora_Inicio) >= '" + formataData(data_ini) + "'"
		+ " AND CONVERT(DATE,DataHora_Inicio) <= '" + formataData(data_fim) + "'"
		+  restringe_consulta("EMPRESA", filtro_empresas, true)
		+  restringe_consulta6(["RESPOSTA_1","RESPOSTA_2","RESPOSTA_3"], filtro_notas)
		+  restringe_consulta("PRODUTO", filtro_produtos, true)
		+ " group by CONVERT(DATE,DataHora_Inicio),Empresa"
		+ " )"
		+ " select b.Empresa,"
	    + " sum(b.PROMOTORES) as PROMOTORES,sum(b.DETRATORES) as DETRATORES,"
	    + " convert(float,sum(b.S1))/convert(float,sum(b.S2)) as SATISFACAO,"
	    + " sum(b.NOTA1) as NOTA1,sum(b.QTDPESQUISASVALIDAS) as QTDPESQUISASVALIDAS,"
		+ " sum(b.QTDPESQUISASVALIDASNPS) as QTDPESQUISASVALIDASNPS"
	    + " from b"//a left outer join b "
	    //+ " on b.EMPRESA = a.CodEmpresa AND b.DATAHORA = a.DatReferencia"
	    + " group by b.Empresa "
	    + " order by b.Empresa asc";    
	
	}else{
		
		stmt += "select CONVERT(DATE,DatReferencia) as DatReferencia, OrdemNota, CodEmpresa, CodProduto,";
		
		if(filtro_notas.length > 0){
			for (var i = 0; i < filtro_notas.length; i++){
				stmt += " sum(case when nota = "+filtro_notas[i]+" then qtd else 0 end) as N"+filtro_notas[i]+"";
				if (i < filtro_notas.length-1) stmt += ","; 
			}
		}else{		
			stmt += " sum(case when nota = 0 then qtd else 0 end) as N0,"
			+" sum(case when nota = 1 then qtd else 0 end) as N1,"
			+" sum(case when nota = 2 then qtd else 0 end) as N2,"
			+" sum(case when nota = 3 then qtd else 0 end) as N3,"
			+" sum(case when nota = 4 then qtd else 0 end) as N4,"
			+" sum(case when nota = 5 then qtd else 0 end) as N5,"
			+" sum(case when nota = 6 then qtd else 0 end) as N6,"
			+" sum(case when nota = 7 then qtd else 0 end) as N7,"
			+" sum(case when nota = 8 then qtd else 0 end) as N8,"
			+" sum(case when nota = 9 then qtd else 0 end) as N9,"
			+" sum(case when nota = 10 then qtd else 0 end) as N10";
		}
		
		
		stmt += " from  resumopesquisaonline"
		+" WHERE 1=1"
		+" AND CONVERT(DATE,DatReferencia) >= '" + formataData(data_ini) + "'"
		+" AND CONVERT(DATE,DatReferencia) <= '" + formataData(data_fim) + "'"
		+ restringe_consulta("CodEmpresa", filtro_empresas, true)
		+ restringe_consulta("Nota", filtro_notas, true)
		+ restringe_consulta("CodProduto", filtro_produtos, true)
		+" group by CONVERT(DATE,DatReferencia),OrdemNota,"
		+" CodEmpresa, CodProduto"
		+" order by CONVERT(DATE,DatReferencia), CodEmpresa,CodProduto";
		
	}
   

    executaQuery = executaQuery2;    
    log(stmt);

    function executaQuery2(columns) {
		var s = {};
		var p = {};
		if($scope.consolidado){
			
			var //data_hora = columns["DatReferencia"].value,
				//datReferencia = formataDataBRString(columns["DatReferencia"].value),	
				CodEmpresa =  columns["Empresa"].value,				
				//QTDPESQUISA =  columns["QTDPESQUISA"].value,
				//QTDPESQUISA2 =  columns["QTDPESQUISA2"].value,
				//QTDPESQUISA1 =  columns["QTDPESQUISA1"].value,
				PROMOTORES =  columns["PROMOTORES"].value,
				DETRATORES =  columns["DETRATORES"].value,
				SATISFACAO =  columns["SATISFACAO"].value,
				NOTA1 =  columns["NOTA1"].value,
				QTDPESQUISASVALIDAS = columns["QTDPESQUISASVALIDAS"].value,
				QTDPESQUISASVALIDASNPS = columns["QTDPESQUISASVALIDASNPS"].value;
				
				s =  {
			//data_hora: data_hora,
			//datReferencia: datReferencia,
			CodEmpresa: CodEmpresa,
			QTDPESQUISA: QTDPESQUISASVALIDAS,
			NPS: ((100 * PROMOTORES / QTDPESQUISASVALIDASNPS) - (100 * DETRATORES / QTDPESQUISASVALIDASNPS)).toFixed(2),
			SATISFACAO: SATISFACAO.toFixed(2),
			TX_RESOLUCAO: (100 * NOTA1 / QTDPESQUISASVALIDAS).toFixed(2)
	
		  }
				
				
				$scope.dados.push(s);
			
		}else{
			var data_hora = columns["DatReferencia"].value,
				  datReferencia = formataDataBRString(columns["DatReferencia"].value),
				  OrdemNota =  columns["OrdemNota"].value,
				  CodEmpresa =  columns["CodEmpresa"].value,
				  CodProduto =  columns["CodProduto"].value,              
				  n0 = columns["N0"] !== undefined ? columns["N0"].value : 0,
				  n1 = columns["N1"] !== undefined ? columns["N1"].value : 0,
				  n2 = columns["N2"] !== undefined ? columns["N2"].value : 0,
				  n3 = columns["N3"] !== undefined ? columns["N3"].value : 0,
				  n4 = columns["N4"] !== undefined ? columns["N4"].value : 0,
				  n5 = columns["N5"] !== undefined ? columns["N5"].value : 0,
				  n6 = columns["N6"] !== undefined ? columns["N6"].value : 0,
				  n7 = columns["N7"] !== undefined ? columns["N7"].value : 0,
				  n8 = columns["N8"] !== undefined ? columns["N8"].value : 0,
				  n9 = columns["N9"] !== undefined ? columns["N9"].value : 0,
				  n10 = columns["N10"] !== undefined ? columns["N10"].value : 0,
				  nTotal = n0+n1+n2+n3+n4+n5+n6+n7+n8+n9+n10;
			
		  var textonota = "Atendimento";
		  if(OrdemNota === 2) textonota = "Resolução";
		  if(OrdemNota === 3) textonota = "Recomendação";

		  s = {
			data_hora: data_hora,
			datReferencia: datReferencia,
			OrdemNota: textonota,
			CodEmpresa: CodEmpresa,
			CodProduto: CodProduto,
			n0:n0,			
			n1:n1,			
			n2:n2,			
			n3:n3,			
			n4:n4,			
			n5:n5,			
			n6:n6,			
			n7:n7,			
			n8:n8,			
			n9:n9,			
			n10:n10			
		};
		
		p = {
			data_hora: data_hora,
			datReferencia: "",
			OrdemNota: "%",
			CodEmpresa: "",
			CodProduto: "",			
			n0: (100 * n0 / nTotal).toFixed(2),			
			n1: (100 * n1 / nTotal).toFixed(2),			
			n2: (100 * n2 / nTotal).toFixed(2),			
			n3: (100 * n3 / nTotal).toFixed(2),			
			n4: (100 * n4 / nTotal).toFixed(2),			
			n5: (100 * n5 / nTotal).toFixed(2),			
			n6: (100 * n6 / nTotal).toFixed(2),			
			n7: (100 * n7 / nTotal).toFixed(2),			
			n8: (100 * n8 / nTotal).toFixed(2),			
			n9: (100 * n9 / nTotal).toFixed(2),			
			n10: (100 * n10 / nTotal).toFixed(2)			
		};
		
		$scope.dados.push(s);
		$scope.dados.push(p);
		}
		
		var a = [];	
		var b = [];
		for (var item in $scope.colunas) {				
			a.push(s[$scope.colunas[item].field]);
			if(!isEmpty(p)) b.push(p[$scope.colunas[item].field]);
		}
		
		$scope.csv.push(a);	
		if(!isEmpty(p)) $scope.csv.push(b);	

      

      if ($scope.dados.length % 1000 === 0) {
        $scope.$apply();
      }
    }

    db.query(stmt, executaQuery, function(err, num_rows) {
      console.log("Executando query-> " + stmt + " " + num_rows);
      retornaStatusQuery(num_rows, $scope);
      $btn_gerar.button("reset");
      if (num_rows > 0) {
        $btn_exportar.prop("disabled", false);
        $btn_exportarCSV.prop("disabled", false);
      }
    });

    // GILBERTOOOOOO 17/03/2014
    $view.on("mouseup", "tr.resumo", function() {
      var that = $(this);
      $("tr.resumo.marcado").toggleClass("marcado");
      $scope.$apply(function() {
        that.toggleClass("marcado");
      });
    });
  };

  // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }

      // var colunasFinal = [colunas];

      console.log(colunas);

      var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'AtendHumano_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };
}
CtrlAtendimentoHumano.$inject = ["$scope", "$globals"];
