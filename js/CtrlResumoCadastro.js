function CtrlResumoCadastro($scope, $globals) {

    win.title = "Visão Geral Cadastro/Migração";
    $scope.versao = versao;

    $scope.rotas = rotas;
    travaBotaoFiltro(0, $scope, "#pag-resumo-cadastro", "Visão Geral Cadastro/Migração");

    $scope.dados = [];
	$scope.csv = [];
	
	$scope.colunas = [];
	$scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.ordenacao = 'mes';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];   
	$scope.valor = "d";
	$scope.dddsSel = false;

    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
		porDia: false
    };
	
	function geraColunas() {
	   var array = [];		
	   if ($('.filtro-aplicacao').val() == "URACAD"){	
			array.push(
			    { field: "mes", displayName: ($scope.valor === "m" ? "Mês" : "Dia"), width: 100, pinned: true }
				);
				
				if($scope.dddsSel){
					array.push(
					{ field: "ddd", displayName: 'DDD', width: 50, pinned: true }
					);
				}
	  
			array.push(
				{ field: "entrantes", displayName: "Entrantes", width: 90, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
				{ field: "clientes", displayName: "Clientes", width: 80, pinned: true , cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
				{ field: "derivadas", displayName: "Derivadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "finalizadas", displayName: "Finalizadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "abandonadas", displayName: "Abandonadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "cadastrook", displayName: "Cadastro Ok", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },			
				{ field: "qtdPercDerivadas", displayName: "% Derivadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
				{ field: "qtdPercFinalizadas", displayName: "% Finalizadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
				{ field: "qtdPercAbandonadas", displayName: "% Abandonadas", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },
				{ field: "qtdPercCadastroOk", displayName: "% Cadastro Ok", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' },			
				{ field: "cpfblacklist", displayName: "CPF Blacklist", width: 100, pinned: true},
				{ field: "menordeidade", displayName: "Menor de idade", width: 100, pinned: true}				
			);
	   }else if ($('.filtro-aplicacao').val() == "URAMIGRA"){
		   
		   array.push(
				{ field: "data_hora", displayName: ($scope.valor === "m" ? "Mês" : "Dia"), width: 100, pinned: true }
		   );

			if($scope.dddsSel){
				array.push(
				{ field: "ddd", displayName: 'DDD', width: 50, pinned: true }
				);
			}
				
			array.push(
				{ field: "entrantes", displayName: "Entrantes", width: 100, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "finalizadas", displayName: "Finalizadas", width: 110, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "abandonadas", displayName: "Abandonadas", width: 120, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "pdvInvalido", displayName: "PDV Inválido", width: 130, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "cartaoInvalido", displayName: "Cartão Inválido", width: 130, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "cartaoDigitado", displayName: "Cartão Digitado", width: 130, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },				
				{ field: "consultaSiebel", displayName: "Consulta Siebel", width: 130, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "migracaoSucessoBoleto", displayName: "Migração Boleto", width: 140, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
				{ field: "migracaoSucessoCartao", displayName: "Migração Cartão", width: 140, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' }
			);

		   
	   }
		return array;
	}



    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
		
		
       $view = $("#pag-resumo-cadastro");

        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoesGrupo($view,false,false,$scope,"Cadastro e Migração");
        //carregaAplicacoesComData($view);
        carregaSites($view);
		
		carregaDDDs($view,true);
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});
		
        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        carregaSegmentosPorAplicacao($scope, $view, true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
          carregaSegmentosPorAplicacao($scope, $view);
          //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
        });        


        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });
		
		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});
		
		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});
		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});		


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-cadastro");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes.length > 1 || filtro_aplicacoes[0] === null ) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione somente 1 aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            } 
			$view.find("select.filtro-ddd").val() ? $scope.dddsSel = true : $scope.dddsSel = false;
			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this); 

        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-cadastro");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');

    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;
		$scope.filtros_usados = "";
		
		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);


        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }

        //$scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
		$scope.csv = [];
        var stmt = "";
		var fator = $scope.valor === "h" ? "hour" : $scope.valor === "d" ? "day" : "month";
		var fator2 = fator === "hour" ? "" : "Dia";
		
		if ($('.filtro-aplicacao').val() == "URACAD"){
			
			var tabela1 = "ResumoDesempenhoGeralX";
			var ul = "_";
			var colDDD1 = "";
			var	colDDD2 = "";
			var	colDDD3 = "";
			
			if(filtro_ddds.length > 0){
				tabela1 =  "ResumoAnalitico2";
				ul="";
				colDDD1 = "DDD,";
				colDDD2 = "CodDDD,";
				colDDD3 = ",cte.[DDD]";
			}
			
		
			stmt += " with cte as ("
			+ " 	select "+colDDD1+"DATEADD("+fator+", DATEDIFF("+fator+", 0, Dat"+ul+"Referencia), 0) as [Mês], SUM(QtdChamadas) as [Entrantes],"
			+ " 		SUM(QtdDerivadas) as [Derivadas], SUM(QtdFinalizadas) as [Finalizadas], SUM(QtdAbandonadas) as [Abandonadas]"
			+ " 	from "+tabela1+""
			+ " 	where 1 = 1"
			+ " AND Dat"+ul+"Referencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND Dat"+ul+"Referencia <= '" + formataDataHora(data_fim) + "'"		
			+ " 		and Cod"+ul+"Aplicacao = 'URACAD'";
			stmt += ul === "" ? restringe_consulta("CodSegmento", filtro_segmentos, true) : restringe_consulta2("Cod_Segmento", filtro_segmentos, true);
			stmt += restringe_consulta("Cod"+ul+"Site", filtro_sites, true);
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += " 	group by "+colDDD1+" DATEADD("+fator+", DATEDIFF("+fator+", 0, Dat"+ul+"Referencia), 0)"
			+ " ), cte2 as ("
			+ " 	select "+colDDD2+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as [Mês], SUM(QtdClientes) as [Clientes]"
			+ " 	from TotaisClientesUnicos"+(fator  === "month" ? "Mes" : "")+""
			+ " 	where 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"		
			+ " 		and CodAplicacao = 'URACAD'";
			stmt += restringe_consulta2("CodSegmento", filtro_segmentos, true);
			stmt += restringe_consulta2("CodSite", filtro_sites, true);
			stmt += restringe_consulta("CodDDD", filtro_ddds, true);
			stmt += " 	group by "+colDDD2+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0)"
			+ " ), cte3 as ("
			+ " 	select "+colDDD2+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as [Mês], SUM(QtdVendas) as [Cadastros OK]"
			+ " 	from ResumovendaProduto"
			+ " 	where 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodDDD", filtro_ddds, true);
			stmt += " 		and CodAplicacao = 'URACAD' and CodProduto in (136, 137) and Resultado = 0"
			+ " 	group by "+colDDD2+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0)"
			+ " ), cte4 as ("
			if(filtro_ddds.length > 0){			
				stmt += " 	select "+colDDD1+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as [Mês],"
				+ " 		SUM(case when CodIC = 'IC0575' then QtdExecucoes else 0 end) as [CPF Blacklist],"
				+ " 		SUM(case when CodIC = 'IC0684' then QtdExecucoes else 0 end) as [Menor de Idade]"
				+ " 	from ResumoICX"
				+ " 	where 1 = 1"
				+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
				+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
				stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
				stmt += restringe_consulta("CodSite", filtro_sites, true);	
				stmt += restringe_consulta("DDD", filtro_ddds, true);					
				stmt += " 		and CodAplicacao = 'URACAD'"
				+ " 	group by "+colDDD1+"DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0)";
			}else{
				stmt += " 	select DATEADD("+fator+", DATEDIFF("+fator+", 0, DataHora), 0) as [Mês],"
				+ " 		SUM(case when Cod_ItemDeControle = 'IC0575' then Qtd else 0 end) as [CPF Blacklist],"
				+ " 		SUM(case when Cod_ItemDeControle = 'IC0684' then Qtd else 0 end) as [Menor de Idade]"
				+ " 	from ConsolidaItemControleX"
				+ " 	where 1 = 1"
				+ " AND DataHora >= '" + formataDataHora(data_ini) + "'"
				+ " AND DataHora <= '" + formataDataHora(data_fim) + "'";
				stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
				stmt += restringe_consulta("Cod_Site", filtro_sites, true);		
				stmt += " 		and Cod_Aplicacao = 'URACAD'"
				+ " 	group by DATEADD("+fator+", DATEDIFF("+fator+", 0, DataHora), 0)";
			}
			
			stmt += " )"
			+ " select cte.[Mês], [Entrantes], [Clientes], [Derivadas], [Finalizadas], [Abandonadas],"
			+ " 	ISNULL([Cadastros OK], 0) as [Cadastros OK],"
			+ " 	ISNULL([CPF Blacklist], 0) as [CPF Blacklist],"
			+ " 	ISNULL([Menor de Idade], 0) as [Menor de Idade]"+colDDD3+""
			+ " from cte";
			if(filtro_ddds.length > 0){	
				stmt += " left outer join cte2 on cte.[Mês] = cte2.[Mês] and cte.[DDD] = cte2.CodDDD"
				+ " left outer join cte3 on cte.[Mês] = cte3.[Mês] and cte.[DDD] = cte3.CodDDD"
				+ " left outer join cte4 on cte.[Mês] = cte4.[Mês] and cte.[DDD] = cte4.DDD";
			}else{
				stmt += " left outer join cte2 on cte.[Mês] = cte2.[Mês]"
				+ " left outer join cte3 on cte.[Mês] = cte3.[Mês]"
				+ " left outer join cte4 on cte.[Mês] = cte4.[Mês]";
			}
			stmt += " order by cte.[Mês]"+colDDD3+"";		
		
		} else if ($('.filtro-aplicacao').val() == "URAMIGRA"){		
			var colDDD1 = "";
			var colDDD2 = "";			
			if(filtro_ddds.length > 0){				
				colDDD1 = ",ddd";	
				colDDD2 = ",codDdd";
			}	
			stmt += "with PdvInvalido as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+" "
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO "
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Pdv Inválido' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt +=" GROUP BY DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "
			+" ), "
			+" CartaoInvalidao as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+""
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO "
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Cartão Inválido' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt +=" GROUP BY  DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "
			+" ), "
			+" CartaoDigitadoo as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+""
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO"
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Cartão Digitado' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt +=" GROUP BY  DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "
			+" ),"
			
			+" MigracaoSucessoBoleto as ( "	
			
			+ " select DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+ " SUM(QtdVendas) as qc "+colDDD2+""
			+ " from ResumovendaProduto"
			+ " where 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodDDD", filtro_ddds, true);
			stmt += " and CodAplicacao = 'URAMIGRA' and CodProduto in (224) and Resultado = 2"
			+ " group by DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD2+" "			
			
			/*+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+""
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO" 
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Migração Boleto' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " GROUP BY  DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "*/
			
			+" ), "
			+" MigracaoSucessoCartao as ( "
			
			+ " select DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+ " SUM(QtdVendas) as qc "+colDDD2+""
			+ " from ResumovendaProduto"
			+ " where 1 = 1"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodDDD", filtro_ddds, true);
			stmt += " and CodAplicacao = 'URAMIGRA' and CodProduto in (23) and Resultado = 0"
			+ " group by DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD2+" "
			
			/*+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+""
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO" 
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Migração Cartão' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " GROUP BY  DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "*/
			
			+" ), "
			+" ConsultaSiebel as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) AS qc "+colDDD1+""
			+" FROM ResumoIC"+fator2+" R "
			+" INNER JOIN PARAMETROS_ICS as P on R.CodIC = P.CodIC AND R.CODAPLICACAO = P.CODAPLICACAO "
			+" WHERE P.Relatorio = 'Dia a Dia' and sucesso = 'Sim' and parametro = 'Consulta Siebel' and r.CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " GROUP BY  DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "+colDDD1+" "
			+" ), ";

			if(filtro_ddds.length > 0){	
			
			stmt += " rdg as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, DATREFERENCIA), 0) as DatReferencia,"
			+" SUM(QtdChamadas) as c,   SUM(QtdFinalizadas) as f, "
			+" SUM(QtdDerivadas) as d, "
			+" SUM(QtdAbandonadas) as a,ra.DDD "
			+" FROM ResumoAnalitico2"
			+" as RA LEFT OUTER JOIN UF_DDD as UD on RA.DDD = UD.DDD"
			+" WHERE 1 = 1 and CodAplicacao = 'URAMIGRA'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("RA.DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " GROUP BY DATEADD("+fator+", DATEDIFF("+fator+", 0, DATREFERENCIA), 0),ra.ddd "
			+" ) "
			+" SELECT rdg.DatReferencia,"
			+" sum(rdg.c) as entrantes,"
			+" sum(rdg.a) as abandonadas,"
			+" sum(rdg.f) - ISNULL(sum(MigracaoSucessoBoleto.qc),0) - ISNULL(sum(MigracaoSucessoCartao.qc),0) as finalizadas,"			
			+" ISNULL(sum(PdvInvalido.qc),0) as pdvInvalido,"
			+" ISNULL(sum(CartaoInvalidao.qc),0) as cartaoInvalido,"
			+" ISNULL(sum(CartaoDigitadoo.qc),0) as cartaoDigitado,"
			+" ISNULL(sum(MigracaoSucessoBoleto.qc),0) as migracaoSucessoBoleto,"
			+" ISNULL(sum(MigracaoSucessoCartao.qc),0) as migracaoSucessoCartao,"
			+" ISNULL(sum(ConsultaSiebel.qc),0) as consultaSiebel,"			
			+" rdg.ddd"
			+" FROM rdg "
			+" left outer join PdvInvalido on rdg.DatReferencia = PdvInvalido.DatReferencia "
			+" and rdg.ddd = PdvInvalido.ddd"
			+" left outer join CartaoInvalidao on rdg.DatReferencia = CartaoInvalidao.DatReferencia"		
			+" and rdg.ddd = CartaoInvalidao.ddd"
			+" left outer join CartaoDigitadoo on rdg.DatReferencia = CartaoDigitadoo.DatReferencia"		
			+" and rdg.ddd = CartaoDigitadoo.ddd"
			+" left outer join MigracaoSucessoBoleto on rdg.DatReferencia = MigracaoSucessoBoleto.DatReferencia"
			+" and rdg.ddd = MigracaoSucessoBoleto.CodDDD"
			+" left outer join MigracaoSucessoCartao on rdg.DatReferencia = MigracaoSucessoCartao.DatReferencia"
			+" and rdg.ddd = MigracaoSucessoCartao.CodDDD"
			+" left outer join ConsultaSiebel on rdg.DatReferencia = ConsultaSiebel.DatReferencia"
			+" and rdg.ddd = ConsultaSiebel.ddd"
			+" GROUP BY rdg.DatReferencia,rdg.DDD order by rdg.DatReferencia";
			
			}else{
			
			stmt += " rdg as ( "
			+" SELECT DATEADD("+fator+", DATEDIFF("+fator+", 0, Dat_Referencia), 0) as DatReferencia,"
			+" SUM(QtdChamadas) as c,   SUM(QtdFinalizadas) as f, "
			+" SUM(QtdDerivadas) as d, "
			+" SUM(QtdAbandonadas) as a "
			+" FROM resumodesempenhogeral"
			+" WHERE 1 = 1 and Cod_Aplicacao = 'URAMIGRA'"
			+ " AND Dat_Referencia >= '" + formataDataHora(data_ini) + "'"
			+ " AND Dat_Referencia <= '" + formataDataHora(data_fim) + "'";			
			stmt += restringe_consulta2("Cod_Segmento", filtro_segmentos, true);			
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " GROUP BY DATEADD("+fator+", DATEDIFF("+fator+", 0, Dat_Referencia), 0)"
			+" ) "
			+" SELECT rdg.DatReferencia,"
			+" sum(rdg.f + rdg.d + rdg.a) as entrantes,"
			+" sum(rdg.a) as abandonadas,"
			+" sum(rdg.f) - ISNULL(sum(MigracaoSucessoBoleto.qc),0) - ISNULL(sum(MigracaoSucessoCartao.qc),0) as finalizadas,"
			+" ISNULL(sum(PdvInvalido.qc),0) as pdvInvalido,"
			+" ISNULL(sum(CartaoInvalidao.qc),0) as cartaoInvalido,"
			+" ISNULL(sum(CartaoDigitadoo.qc),0) as cartaoDigitado,"
			+" ISNULL(sum(MigracaoSucessoBoleto.qc),0) as migracaoSucessoBoleto,"
			+" ISNULL(sum(MigracaoSucessoCartao.qc),0) as migracaoSucessoCartao,"
			+" ISNULL(sum(ConsultaSiebel.qc),0) as consultaSiebel"
			+" FROM rdg "
			+" left outer join PdvInvalido on rdg.DatReferencia = PdvInvalido.DatReferencia "
			+" left outer join CartaoInvalidao on rdg.DatReferencia = CartaoInvalidao.DatReferencia"	
			+" left outer join CartaoDigitadoo on rdg.DatReferencia = CartaoDigitadoo.DatReferencia"
			+" left outer join MigracaoSucessoBoleto on rdg.DatReferencia = MigracaoSucessoBoleto.DatReferencia"		
			+" left outer join MigracaoSucessoCartao on rdg.DatReferencia = MigracaoSucessoCartao.DatReferencia"
			+" left outer join ConsultaSiebel on rdg.DatReferencia = ConsultaSiebel.DatReferencia"
			+" group by rdg.DatReferencia order by rdg.DatReferencia";			
			}		
		}


        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery(columns) {

			if (($('.filtro-aplicacao').val() == "URACAD")){
				var mes = columns[0] !== undefined ? ($scope.valor === "h" ? formataDataBR(columns[0].value,"hora") : ($scope.valor === "d" ?  formataDataBR(columns[0].value) : formataDataBR(columns[0].value ,"mes" ))) : undefined;
				
				var entrantes = columns[1].value,
				clientes = columns[2].value,
				derivadas = columns[3].value,
				finalizadas = columns[4].value - columns[6].value,
				abandonadas = columns[5].value,				
				cadastroOk = columns[6].value,
				qtdPercDerivadas = entrantes !== 0 ? (100 * derivadas / entrantes).toFixed(2): 0,
				qtdPercFinalizadas = entrantes !== 0 ? (100 * finalizadas / entrantes).toFixed(2): 0,				
				qtdPercAbandonadas = entrantes !== 0 ? (100 * abandonadas / entrantes).toFixed(2):0,
				qtdPercCadastroOk = entrantes !== 0 ? (100 * cadastroOk / entrantes).toFixed(2):0,
				cpfBlacklist = columns[7].value,
				menorDeIdade = columns[8].value,
				ddd = columns[9] !== undefined ? columns[9].value : undefined;
			 
			  
			  $scope.dados.push({
				mes: mes,
				ddd: ddd,				
				entrantes: entrantes,
				clientes: clientes,
				derivadas: derivadas,
				finalizadas: finalizadas,
				abandonadas: abandonadas,
				cadastrook: cadastroOk,
				qtdPercDerivadas: qtdPercDerivadas,
				qtdPercFinalizadas: qtdPercFinalizadas,
				qtdPercAbandonadas: qtdPercAbandonadas,
				qtdPercCadastroOk: qtdPercCadastroOk,
				cpfblacklist: cpfBlacklist,
				menordeidade: menorDeIdade
				
			  });
			  
				var s = [
					mes,
					ddd,				
					entrantes,
					clientes,
					derivadas,
					finalizadas,
					abandonadas,
					cadastroOk,
					qtdPercDerivadas,
					qtdPercFinalizadas,
					qtdPercAbandonadas,
					qtdPercCadastroOk,
					cpfBlacklist,
					menorDeIdade
				];
				var filtered = s.filter(function (el) {
					return el != undefined;
				});
			  
			  $scope.csv.push(filtered);
				
				
				
			}else if (($('.filtro-aplicacao').val() == "URAMIGRA")){
			  var mes = columns[0] !== undefined ? ($scope.valor === "h" ? formataDataBR(columns[0].value,"hora") : ($scope.valor === "d" ?  formataDataBR(columns[0].value) : formataDataBR(columns[0].value ,"mes" ))) : undefined;              
              var entrantes = columns["entrantes"].value,
			  finalizadas = columns["finalizadas"].value,
			  abandonadas = columns["abandonadas"].value,				
			  pdvInvalido = columns["pdvInvalido"].value,
			  cartaoInvalido = columns["cartaoInvalido"].value,
			  cartaoDigitado = columns["cartaoDigitado"].value,
			  consultaSiebel = columns["consultaSiebel"].value,
			  migracaoSucessoBoleto = columns["migracaoSucessoBoleto"].value,
			  migracaoSucessoCartao = columns["migracaoSucessoCartao"].value,
			  ddd = columns["ddd"] !== undefined ? columns["ddd"].value : undefined;
			 			 
			  
			  $scope.dados.push({
				data_hora: mes,
				ddd:ddd,
				entrantes: entrantes,
				finalizadas: finalizadas,
				abandonadas: abandonadas,
				pdvInvalido: pdvInvalido,
				cartaoInvalido: cartaoInvalido,
				cartaoDigitado: cartaoDigitado,
				consultaSiebel: consultaSiebel,
				migracaoSucessoBoleto: migracaoSucessoBoleto,
				migracaoSucessoCartao: migracaoSucessoCartao
			  });
			  
			  
			  var s = [
				mes,
				ddd,
				entrantes,
				finalizadas,
				abandonadas,
				pdvInvalido,
				cartaoInvalido,
				cartaoDigitado,
				consultaSiebel,
				migracaoSucessoBoleto,
				migracaoSucessoCartao
				];
				
				var filtered = s.filter(function (el) {
					return el != undefined;
				});
			  
			  $scope.csv.push(filtered);

			}
			
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}

        }

        db.query(stmt, executaQuery, function (err, num_rows) {            
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }            
        });

    };

    // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.csv);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'ResumoCadastro' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };

}
CtrlResumoCadastro.$inject = ['$scope', '$globals'];
