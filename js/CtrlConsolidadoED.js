function CtrlConsolidadoED($scope, $globals) {

    win.title="Consolidado por estado de diálogo"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    // Gilberto 02/06/2014

    travaBotaoFiltro(0, $scope, "#pag-consolidado-ed", "Consolidado por estado de diálogo");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;



    $scope.log = [];

    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = []; 
    $scope.iit = false;
    $scope.tlv = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        sites:  [],
        segmentos: [],
        estado: ""
    };
	
    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2012, 10, 1),
        max: agora // FIXME: atualizar ao virar o dia
        };


    // GILBERTO - 29/01/2014 - configuração dos GRIDs

    // GILBERTO 17/02/2014
    /*
    plugin de DoubleClick de linha
    */

    function ngGridDoubleClick() {
        var self = this;
        self.$scope = null;
        self.myGrid = null;

        // o método init é chamado durante a execução da diretiva ng-grid
        self.init = function (scope, grid, services) {

            // a diretiva passa o scope do grid e o objeto do grid
            // o qual queremos salvar para manipulação posterior
            self.$scope = scope;
            self.myGrid = grid;

            // Neste exemplo queremos atribuir eventos do grid.
            self.assignEvents();
        };
        self.assignEvents = function () {
            // Aqui definimos o tratador do evento dblclick para o container do cabeçalho
            self.myGrid.$viewport.on('dblclick', self.onDoubleClick);
        };
        // função duplo clique
        self.onDoubleClick = function (event) {
            self.myGrid.config.dblClickFn(self.$scope.selectedItems[0]);
        };
    }


    $scope.myDblClickHandler = function (rowItem) {
        // TODO: Tratar o dblclick da célula do grid... FEITO 17/02/2014
        if (arguments[0] !== undefined && arguments[0].nomeEstado !== undefined) {
			var $btn_gerar = $("#pag-consolidado-ed").find(".btn-gerar");
		    $btn_gerar.prop("disabled", true);
			limpaProgressBar($scope, "#pag-consolidado-ed");            
			// Inserir no select, o nome do ED selecionado no duplo clique.
            $view.find("div.filtro-edAuto  div.filter-option").html(arguments[0].nomeEstado);
			$scope.filtroEDSelect = arguments[0].codEstado;
			Estalo.filtros.filtro_ed = arguments[0].codEstado;
                 
            
            $scope.listaDados.apply(this);
        }
    };

    $scope.gridEstadosOrigem = [];
    $scope.optEstadosOrigem = {
        data: 'gridEstadosOrigem',
        showGroupPanel: false,
        dblClickFn: $scope.myDblClickHandler,
        multiSelect: false,
        enableColumnResize: true,
        columnDefs: [{ field: 'nomeEstado', displayName: 'Nome' }, { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px"}],
        plugins: [ngGridDoubleClick]
    };

    $scope.gridEstadosDestino = [];
    $scope.optEstadosDestino = { data: 'gridEstadosDestino',
        showGroupPanel: false,
        dblClickFn: $scope.myDblClickHandler,
        multiSelect: false,
        enableColumnResize: true,
        columnDefs: [{ field: 'nomeEstado', displayName: 'Nome' }, { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px"}],
        plugins: [ngGridDoubleClick]
    };

    $scope.gridVDNsEntrada = [];
    $scope.optVDNsEntrada = { data: 'gridVDNsEntrada' ,
        columnDefs: [
             { field: 'VDN', displayName: 'VDN' },
             { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px" }
         ]};

    $scope.gridVDNsSaida = [];
    $scope.optVDNsSaida = { data: 'gridVDNsSaida',
        columnDefs: [
             { field: 'VDN', displayName: 'VDN' },
             { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px" }
         ]
    };

    $scope.gridPrompts = [];
    $scope.optPrompts = {
        data: 'gridPrompts',
        enableColumnResize: true,
        enableHighlighting: true,

        columnDefs: [
             { field: 'descricao', displayName: 'Descrição' },
             { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px" }
         ]
    };

    $scope.gridRespostas = [];
    $scope.optRespostas = {
        data: 'gridRespostas',
        enableColumnResize: true,
        showGroupPanel: false,
        columnDefs: [
                { field: 'Resposta', displayName: 'Resposta' },
                { field: 'qtdRegistros', displayName: 'Quantidade', width: "100px" }
        ]
    };



    $scope.totais = {};

    var $view;
    var preparaComandos = Function;
    var cbChangeAplicacao = Function;
    $scope.aba = 2;



    $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-consolidado-ed");   

       
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});


        //minuteStep: 5

        //19/03/2014
        componenteDataHora($scope, $view);

        carregaAplicacoes($view,false,false,$scope);
        carregaSites($view);
		


        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-segmento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} segmentos'
        });


        $view.find("select.filtro-edAuto").selectpicker();


        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });


      

        //Marcar todos

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        //GILBERTO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        // GILBERTO 03/02/2014


        $view.on("change", "select.filtro-edAuto", function () {
          Estalo.filtros.filtro_ed = $(this).val() || "";
        });

        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE ( APLICAÇÕES )
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE ( APLICAÇÕES )
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE ( SEGMENTOS )
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {

          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){

            if (!this.disabled) {
                $('div.btn-group.filtro-segmento').addClass('open');
            }
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }

        });

        // OCULTAR AO TIRAR O MOUSE ( SEGMENTOS )
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE ( SITES )
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE ( SITES )
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE ( EDs )
        $('div.container > ul > li > div.btn-group.filtro-ed').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ed .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ed').addClass('open');
            $('div.btn-group.filtro-ed>div>ul').css({ 'max-height': '400px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });
        // OCULTAR AO TIRAR O MOUSE ( EDs )
        $('div.container > ul > li > div.btn-group.filtro-ed').mouseout(function () {
            $('div.btn-group.filtro-ed').removeClass('open');
        });

        //TODO Abrir o datepicker ao passar o mouse.

        // Lista dados conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-consolidado-ed");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, '<font color = "#000">' + testedata + '</font>');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

        
            if ($('div.filtro-aplicacao ul li.selected').length === 0 ) {
                setTimeout(function () {
                    atualizaInfo($scope, '<font color = "white">Selecione uma aplicação</font>');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }


        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
          abortar($scope, "#pag-consolidado-ed");
        });



        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

		
		// GILBERTO 1/07/2014
        
        
        preparaComandos = function (filtro_aplicacoes, filtro_sites, filtro_segmentos, filtro_ed) {

            //stmtCountRows = [];
            var arrayStmt = [];


         // 1)  ESTADOS DE ORIGEM
        var stmt = "";
        var tabelas = tabelasParticionadas($scope, 'Caminho_EstadoDialogo', true);
        var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

            var iit1 = "- ISNULL(Qtd_RegistrosIIT, 0)";
            if($scope.iit === true) iit1 = "";
            var tlv1 = "- ISNULL(Qtd_RegistrosTLV, 0)";
            if($scope.tlv === true) tlv1 = "";

            stmt += " SELECT Cod_Estado_De as Nom_Estado_Anterior,"
            + "   isnull(Sum(Qtd_Registros "+iit1+tlv1+"),0) as Qtd_Registros"
            +" from  "+tabelas.nomes[0]+" WHERE Qtd_Registros > 0"
            +" AND Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
            +" AND Dat_Referencia "+datfim+"";

            if (filtro_ed.map === undefined) {
                filtro_ed = [filtro_ed];
            }
            stmt += restringe_consulta("Cod_Estado_Para", filtro_ed, true);
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);

            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''";
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }

            stmt += " Group by Cod_Estado_De";

            //stmt += " order by Qtd_Registros desc";

            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));





        // 2-  ESTADOS DE DESTINO
  var tabelas = tabelasParticionadas($scope, 'Caminho_EstadoDialogo', true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";


          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';


          var iit2 = " - ISNULL(Qtd_RegistrosIIT, 0)";
          if($scope.iit === true) iit2 = "";
          var tlv2 = " - ISNULL(Qtd_RegistrosTLV, 0)";
          if($scope.tlv === true) tlv2 = "";


            stmt += " Select Cod_Estado_Para as Nom_Estado_Destino,"
                + "   isnull(Sum(Qtd_Registros"+iit2+tlv2+"),0) as Qtd_Registros"
                + " FROM " + db.prefixo + tabelas.nomes[0]
                + " WHERE Cod_Estado_de = '" + filtro_ed + "' AND Qtd_Registros > 0 "
                + " AND Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
                + " AND Dat_Referencia "+datfim+""
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);

            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = '' "
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }

            stmt += " GROUP BY Cod_Estado_Para";
            //stmt += " order by Qtd_Registros desc";

              arrayStmt.push(stmt);
              //stmtCountRows.push(stmtContaLinhas(stmt));


            // 3 - VDNs de entrada
  var tabelas = tabelasParticionadas($scope, 'VDNsINPorEstadoDialogo', true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

          var iit3 = " - ISNULL(Qtd_TransfURA, 0)";
          if($scope.iit === true) iit3 = "";
          var tlv3 = " - ISNULL(Qtd_TransfTLV, 0)";
          if($scope.tlv === true) tlv3 = "";

            stmt += " Select VPE.Cod_Site as codSite, VPE.Cod_VDN as codVDN, GV.DescVdn as descVDN,"
                + " isnull(Sum(Qtd_Chamadas "+iit3+tlv3+"),0) as Chamadas"
                + " From "+tabelas.nomes[0]+" as VPE"
                + " Left Outer Join VdnsUra as GV on VPE.Cod_Site = GV.CodSite AND VPE.Cod_VDN = GV.CodVDN "
                + " Where VPE.Cod_Estado_Dialogo = '" + filtro_ed + "' and VPE.Qtd_Chamadas > 0 " /*trocado de Qtd_Registros para Qtd_Chamadas*/
                + " AND VPE.Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
                + " AND VPE.Dat_Referencia "+datfim+""
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);
            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''"
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }
            stmt += "Group by VPE.Cod_Site, VPE.Cod_VDN, GV.DescVdn";
            //stmt += " order by Chamadas desc";

            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));


            // 4 - VDNs de Saída.
  var tabelas = tabelasParticionadas($scope, 'VDNsOUTPorEstadoDialogo', true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

          var iit4 = " - ISNULL(Qtd_TransfURA, 0)";
          if($scope.iit === true) iit4 = "";
          var tlv4 = " - ISNULL(Qtd_TransfTLV, 0)";
          if($scope.tlv === true) tlv4 = "";

            stmt += " SELECT VPE.Cod_Site as codSite, VPE.Cod_VDN as codVDN, GV.DescVdn as descVDN,"
                + " isnull(Sum(Qtd_Chamadas"+iit4+tlv4+"),0) as Chamadas"
                + " From "+tabelas.nomes[0]+" as VPE"
                + " Left Outer Join VdnsUra as GV on VPE.Cod_Site = GV.CodSite and VPE.Cod_VDN = GV.CodVDN "
                + " Where VPE.Cod_Estado_Dialogo = '" + filtro_ed + "' and VPE.Qtd_Chamadas > 0  " /*trocado de Qtd_Registros para Qtd_Chamadas*/
                + " AND VPE.Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
                + " AND VPE.Dat_Referencia "+datfim+""
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);
            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''";
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }
            stmt += "Group by VPE.Cod_Site, VPE.Cod_VDN, GV.DescVdn";
            //stmt += " order by Chamadas desc";

            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));


            //  5 - Prompts tocados.
  var tabelas = tabelasParticionadas($scope, 'Prompt_EstadoDialogo', true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

          var iit5 = " - ISNULL(Qtd_RegistrosIIT, 0)";
          if($scope.iit === true) iit5 = "";
          var tlv5 = " - ISNULL(Qtd_RegistrosTLV, 0)";
          if($scope.tlv === true) tlv5 = "";

            stmt += " Select R.Cod_Tipo_Prompt as p,"
                + " isnull(Sum(Qtd_Registros "+iit5+tlv5+"),0) as Qtd_Registros,"
                + " P.DesDetalhada as dd,"
                + " R.Cod_Aplicacao as ca"
                + " From "+tabelas.nomes[0]+" as R"
                + " Left Outer Join Prompt P on R.Cod_Tipo_Prompt = P.CodPrompt and R.Cod_Aplicacao = P.CodAplicacao"
                + " Where R.Nom_Estado_Dialogo = '" + filtro_ed + "' and R.Qtd_Registros > 0  "
                + " AND R.Cod_Tipo_Prompt <> '' and P.Exibir <> 'N'"
                + " AND R.Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "' "
                + " AND R.Dat_Referencia "+datfim+" "

            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);
            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''"
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }
            stmt += "GROUP BY R.Cod_Aplicacao, R.Cod_tipo_Prompt, P.DesDetalhada";
            //stmt += " ORDER BY Qtd_registros desc";

            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));

            // 6 - Preenche as respostas.
            var nomeTabela = "";
            var isResposta = $scope.filtros.isUltimo;
            var nomeTabela = "";
            if (isResposta) {
                nomeTabela = "UltimoResultado_EstadoDialogo";
            } else {
                nomeTabela = "Resultado_EstadoDialogo";
            }

  var tabelas = tabelasParticionadas($scope, nomeTabela, true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

          tabelas.nomes.length > 1 ? stmt = db.use+'with q as (' : stmt = db.use+'';

            // - GILBERTO - 04/02/2014 - corrigido.

          var iit6 = " - ISNULL(Qtd_RegistrosIIT, 0)";
          if($scope.iit === true) iit6 = "";
          var tlv6 = " - ISNULL(Qtd_RegistrosTLV, 0)";
          if($scope.tlv === true) tlv6 = "";

            stmt += " Select R.Resultado as resul,"
                + " isnull(Sum(Qtd_Registros"+iit6+tlv6+"),0) as Qtd_Registros,"
                + " Nom_Estado_Dialogo as ne"
                + " From " + nomeTabela + " R"
            /*+ " Left Outer Join Prompt P on R.Cod_Tipo_Prompt = P.CodPrompt "*/
                + " Where R.Nom_Estado_Dialogo = '" + filtro_ed + "' and R.Qtd_Registros > 0  "
                + " AND R.Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
                + " AND R.Dat_Referencia "+datfim+""

            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);
            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''"
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true);
            }
            stmt += "Group by Nom_Estado_Dialogo,Resultado";
            //stmt += " order by Qtd_Registros desc";


            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));

            // 7 - Preenche as inputs com Entrantes = Finalizadas + Derivadas + Abandonadas

  var tabelas = tabelasParticionadas($scope, 'Resumo_EncerramentoURAVOZ', true);
  var datfim = tabelas.nomes.length > 1 ? "< '"+tabelas.data+"'" : "<= '" + formataDataHora($scope.periodo.fim) + "'";

            var iit7_1 = " - ISNULL(QtdChamadasIIT, 0)";
            if($scope.iit === true) iit7_1 = "";
            var iit7_2 = " - ISNULL(QtdExecucoesIIT, 0)";
            if($scope.iit === true) iit7_2 = "";
            var tlv7_1 = " - ISNULL(QtdChamadasTLV, 0)";
            if($scope.tlv === true) tlv7_1 = "";
            var tlv7_2 = " - ISNULL(QtdExecucoesTLV, 0)";
            if($scope.tlv === true) tlv7_2 = "";


            stmt = db.use+"SELECT isNull(sum(isNull(QtdChamadas, 0) "+iit7_1+tlv7_1+"),0) as Chamadas, "
            + " isNull(sum(isNull(QtdExecucoes, 0) "+iit7_2+tlv7_2+"),0)   as Execucoes, "
            + " isNull(sum(QtdDerivadas"+tlv7_2+"),0) as Derivadas, isnull(sum(QtdFinalizadas),0) as Finalizadas, "
            + " isnull(sum(QtdAbandonadas),0) as Abandonadas ";
            stmt += " FROM "+tabelas.nomes[0]
            /*+ DescobrirSufixo(Str_DataInicio, Str_DataFim)*/
            stmt += " WHERE Cod_Estado_Dialogo = '" + filtro_ed + "'"
            + " AND Dat_Referencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
            + " AND Dat_Referencia "+datfim+"";
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);
            if (filtro_segmentos.length === 0) {
                stmt += " AND Cod_Segmento = ''"
            } else {
                stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
            }

            arrayStmt.push(stmt);
            //stmtCountRows.push(stmtContaLinhas(stmt));

            return arrayStmt;

        }; // Fim de preparaComandos

        cbChangeAplicacao.apply($view.find("select.filtro-aplicacao"),{});
    }); // Fim de ContentLoaded



    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        var querysRows = 0;
        $globals.numeroDeRegistros = 0;

        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        $scope.gridEstadosOrigem = [];
        $scope.gridEstadosDestino = [];
        $scope.gridVDNsEntrada = [];
        $scope.gridVDNsSaida = [];
        $scope.gridPrompts = [];
        $scope.gridRespostas = [];

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        // GILBERTOOOOOOO 19/03/2014 mudei filtro_aplicacao []
        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

        // GILBERTO 1/07/2014
				
        var filtro_ed = $view.find("select.filtro-edAuto").val() || [];
		if(typeof ($scope.filtroEDSelect) === "string") filtro_ed = [$scope.filtroEDSelect];
        Estalo.filtros.filtro_ed = filtro_ed;


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }



        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros += +columns[0].value;
        }

        //MONTA AS 6 queries, uma para cada Grid
        var comandos = preparaComandos(filtro_aplicacoes, filtro_sites, filtro_segmentos, filtro_ed);

        // TODO: Ajustar a lógica de chamada de múltiplas queries ( Talvez pool de conexões )

        // preenche o grid de Estados de Origem

        var gridEstadosOrigem = 0,
            gridEstadosDestino = 0,
            gridVDNsEntrada = 0,
            gridVDNsSaida = 0,
            gridPrompts = 0,
            gridRespostas = 0;

        function preencheGridEDdeOrigem(columns) {


            var codEstado = columns[0].value,

              nomeEstado = obtemNomeEstado(filtro_aplicacoes,codEstado),
              //nomeEstado = obtemEDPorCodigo(codEstado),
              qtdRegistros = +columns[1].value;

            if (nomeEstado !== undefined && nomeEstado !== "") {
                $scope.gridEstadosOrigem.push({
                    codEstado: codEstado,
                    nomeEstado: nomeEstado,
                    qtdRegistros: qtdRegistros
                });

                if ($scope.gridEstadosOrigem.length > 0) { gridEstadosOrigem = $scope.gridEstadosOrigem.length; };
                querysRows = gridEstadosOrigem;
                //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
                //console.log(querysRows + " " + $globals.numeroDeRegistros);

            }

            $scope.$apply();


        } // fim de preenche grid EDs origem

        //preenche o grid de estados de destino
        function preencheGridEDdeDestino(columns) {

            var codEstado = columns[0].value,
              nomeEstado = obtemNomeEstado(filtro_aplicacoes,codEstado),
              //nomeEstado = obtemEDPorCodigo(codEstado),
              qtdRegistros = +columns[1].value;


            if (nomeEstado !== undefined && nomeEstado !== "") {
                $scope.gridEstadosDestino.push({
                    codEstado: codEstado,
                    nomeEstado: nomeEstado,
                    qtdRegistros: qtdRegistros
                });

                if ($scope.gridEstadosDestino.length > 0) { gridEstadosDestino = $scope.gridEstadosDestino.length; };
                querysRows = gridEstadosOrigem + gridEstadosDestino;
                //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
                //console.log(querysRows + " " + $globals.numeroDeRegistros);

            }

        } // Fim de preenche grid eds de destino.

        // preenche o grid de VDNs de entrada
        function preencheGridVDNsEntrada(columns) {

            var VDN = columns[1].value + ' ' + columns[0].value,
              Grupo = columns[2].value,
              qtdRegistros = +columns[3].value;

            $scope.gridVDNsEntrada.push({
                VDN: VDN,
                Grupo: Grupo,
                qtdRegistros: qtdRegistros
            });


            if ($scope.gridVDNsEntrada.length > 0) { gridVDNsEntrada = $scope.gridVDNsEntrada.length; };
            querysRows = gridEstadosOrigem + gridEstadosDestino + gridVDNsEntrada;
            //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
            //console.log(querysRows + " " + $globals.numeroDeRegistros);


        } // fim de preenche grids vdn entrada

        ///Preenche o grid de VDNs de saída
        function preencheGridVDNsSaida(columns) {

            var VDN = columns[1].value + ' ' + columns[0].value,
              Grupo = columns[2].value,
              qtdRegistros = +columns[3].value;


            $scope.gridVDNsSaida.push({
                VDN: VDN,
                Grupo: Grupo,
                qtdRegistros: qtdRegistros

            });

            if ($scope.gridVDNsSaida.length > 0) { gridVDNsSaida = $scope.gridVDNsSaida.length; };
            querysRows = gridEstadosOrigem + gridEstadosDestino + gridVDNsEntrada + gridVDNsSaida;
            //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
            //console.log(querysRows + " " + $globals.numeroDeRegistros);


        } // fim de preenche grid vdns saida

        // preenche o grid de prompts tocados.
        function preencheGridPrompts(columns) {


            var codTipoPrompt = columns[0].value,
                    qtdRegistros = +columns[1].value,
                    descricao = columns[2].value;

            $scope.gridPrompts.push({
                descricao: descricao,
                //codPrompt : codTipoPrompt,
                qtdRegistros: qtdRegistros

            });

            if ($scope.gridPrompts.length > 0) { gridPrompts = $scope.gridPrompts.length; };
            querysRows = gridEstadosOrigem + gridEstadosDestino + gridVDNsEntrada + gridVDNsSaida + gridPrompts;
            //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
            //console.log(querysRows + " " + $globals.numeroDeRegistros);

            $('div.prompts.left-grid div.ngCellText.col0').toggleClass('hint-right').mouseover(function(){
              $('div.prompts.left-grid div.ngCellText.col0').attr('data-hint', $('div.prompts.left-grid div.ngCellText.col0 span').html());
            });

        } // fim de preenche grid prompts

        // Preenche o grid de respostas.
        function preencheGridRespostas(columns) {


            var Resposta = columns[0].value,
                          qtdRegistros = +columns[1].value;

            $scope.gridRespostas.push({
                Resposta: Resposta,
                qtdRegistros: qtdRegistros
            });
            if ($scope.gridRespostas.length > 0) { gridRespostas = $scope.gridRespostas.length; };
            querysRows = gridEstadosOrigem + gridEstadosDestino + gridVDNsEntrada + gridVDNsSaida + gridPrompts + gridRespostas;
            //atualizaProgressBar($globals.numeroDeRegistros, querysRows, $scope,"#pag-consolidado-ed");
            //console.log(querysRows + " " + $globals.numeroDeRegistros);

            /*if($scope.gridRespostas.length%1000===0){
            $scope.$apply();
            }*/

        }



        // Preenche os input text com os valores de totais
        function preencheTotais(columns) {
            $scope.$apply(function () {

                var chamadas = +columns[0].value,
                    finalizadas = +columns[3].value,
                    abandonadas = +columns[4].value,
                    derivadas = +columns[2].value,
                    execucoes = +columns[1].value;



                $scope.totais.chamadas += chamadas;
                $scope.totais.finalizadas += finalizadas;
                $scope.totais.abandonadas += abandonadas;
                $scope.totais.derivadas += derivadas;
                $scope.totais.execucoes += execucoes;


                querysRows = gridEstadosOrigem + gridEstadosDestino + gridVDNsEntrada + gridVDNsSaida + gridPrompts + gridRespostas + 5;
                atualizaProgressBar(100, 100, $scope, "#pag-consolidado-ed");
                //console.log(querysRows + " " + $globals.numeroDeRegistros);
            });
        }

/*
           ,---.
        ,.'-.   \
       ( ( ,'"""""-.
       `,X          `.
       /` `           `._
      (            ,   ,_\
      |          ,---.,'o `.
      |         / o   \     )
       \ ,.    (      .____,
        \| \    \____,'     \
      '`'\  \        _,____,'
      \  ,--      ,-'     \
        ( C     ,'         \
         `--'  .'           |
           |   |         .O |
         __|    \        ,-'_
        / `L     `._  _,'  ' `.
       /    `--.._  `',.   _\  `
       `-.       /\  | `. ( ,\  \
      _/  `-._  /  \ |--'  (     \
     '  `-.   `'    \/\`.   `.    )
           \  -   -    \ `.  |    |
  */



      Estalo.contaExec = 0;

      //1
        if(Estalo.connectionPool.conexaoEDOrigem === undefined){
          Estalo.connectionPool.conexaoEDOrigem = new DB();
        }

        if(!Estalo.connectionPool.conexaoEDOrigem.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoEDOrigem.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoEDOrigem.query(comandos[0], preencheGridEDdeOrigem , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query ED_Origem -> " + comandos[0] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoEDOrigem.isConnected() && Estalo.connectionPool.conexaoEDOrigem.isIdle()){
                  Estalo.connectionPool.conexaoEDOrigem.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect

        //2
        if(Estalo.connectionPool.conexaoEDdeDestino === undefined){
          Estalo.connectionPool.conexaoEDdeDestino = new DB();
        }

        if(!Estalo.connectionPool.conexaoEDdeDestino.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoEDdeDestino.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoEDdeDestino.query(comandos[1], preencheGridEDdeDestino , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query ED_Destino -> " + comandos[1] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoEDdeDestino.isConnected() && Estalo.connectionPool.conexaoEDdeDestino.isIdle()){
                  Estalo.connectionPool.conexaoEDdeDestino.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect

        // 3
        if(Estalo.connectionPool.conexaoVDNsEntrada === undefined){
          Estalo.connectionPool.conexaoVDNsEntrada = new DB();
        }

        if(!Estalo.connectionPool.conexaoVDNsEntrada.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoVDNsEntrada.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoVDNsEntrada.query(comandos[2], preencheGridVDNsEntrada , function (err, num_rows) {
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query VDNs_Entrada -> " + comandos[2] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoVDNsEntrada.isConnected() && Estalo.connectionPool.conexaoVDNsEntrada.isIdle()){
                  Estalo.connectionPool.conexaoVDNsEntrada.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect
        //4
        if(Estalo.connectionPool.conexaoVDNsSaida === undefined){
          Estalo.connectionPool.conexaoVDNsSaida = new DB();
        }

        if(!Estalo.connectionPool.conexaoVDNsSaida.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoVDNsSaida.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoVDNsSaida.query(comandos[3], preencheGridVDNsSaida , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query VDNs_Saida -> " + comandos[3] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoVDNsSaida.isConnected() && Estalo.connectionPool.conexaoVDNsSaida.isIdle()){
                  Estalo.connectionPool.conexaoVDNsSaida.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect

        //5
        if(Estalo.connectionPool.conexaoPrompts === undefined){
          Estalo.connectionPool.conexaoPrompts = new DB();
        }

        if(!Estalo.connectionPool.conexaoPrompts.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoPrompts.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoPrompts.query(comandos[4], preencheGridPrompts , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query Prompts -> " + comandos[4] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoPrompts.isConnected() && Estalo.connectionPool.conexaoPrompts.isIdle()){
                  Estalo.connectionPool.conexaoPrompts.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect

        //6
        if(Estalo.connectionPool.conexaoRespostas === undefined){
          Estalo.connectionPool.conexaoRespostas = new DB();
        }

        if(!Estalo.connectionPool.conexaoRespostas.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoRespostas.connect(config, function(){
         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoRespostas.query(comandos[5], preencheGridRespostas , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;

              //log("Executou a query Respostas -> " + comandos[5] + " " + num_rows);
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoRespostas.isConnected() && Estalo.connectionPool.conexaoRespostas.isIdle()){
                  Estalo.connectionPool.conexaoRespostas.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect

        //7
        if(Estalo.connectionPool.conexaoTotais === undefined){
          Estalo.connectionPool.conexaoTotais = new DB();
        }

        if(!Estalo.connectionPool.conexaoTotais.isConnected()){
           //                                               | | |
           // Assim que conectar, vai chamar essa callback  V V V    aqui
         Estalo.connectionPool.conexaoTotais.connect(config, function(){

         $scope.totais.chamadas = 0;
         $scope.totais.finalizadas = 0;
         $scope.totais.abandonadas = 0;
         $scope.totais.derivadas = 0;
         $scope.totais.execucoes = 0;

         // necessário armazenar a conexão no array connPool pois a conexão pode sair do escopo quando o controller executa.
           Estalo.connectionPool.conexaoTotais.query(comandos[6], preencheTotais , function (err, num_rows) {
             
             if(err){
                console.log(err);
              }
              Estalo.contaExec++;
              
              log("Executou -> " + comandos.join(';'));
              $scope.$apply(function () { /*Limpar o grid*/ });
              if(Estalo.contaExec === 7){
                Estalo.globalEvents.emit('exec',num_rows);
              }

                if(Estalo.connectionPool.conexaoTotais.isConnected() && Estalo.connectionPool.conexaoTotais.isIdle()){
                  Estalo.connectionPool.conexaoTotais.disconnect();
                }
            });// query
      });// connect
    }// fim de if connect


    // Quando as sete queries terminarem a execução
    Estalo.globalEvents.on('exec',function(num_rows){

      // AO FIM DO PREENCHIMENTO DOS GRIDS...
      retornaStatusQuery(querysRows, $scope);
      $btn_gerar.button('reset');
      $btn_exportar.prop("disabled", false);
      $scope.filtros.filtro_estado = Estalo.filtros.filtro_ed;

      Estalo.contaExec = 0;

    }); // fim do exec

    };      ///FIM DE listaDados()





    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='tConsolidadoED'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = 'tConsolidadoED.xlsx';



    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'consolidadoED_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  chamadas: $scope.totais.chamadas,
                   finalizadas: $scope.totais.finalizadas,
                   abandonadas: $scope.totais.abandonadas,
                   derivadas: $scope.totais.derivadas,
                   execucoes: $scope.totais.execucoes,
                   grid1: $scope.gridEstadosOrigem,
                   grid2: $scope.gridEstadosDestino,
                   grid3: $scope.gridVDNsEntrada,
                   grid4: $scope.gridVDNsSaida,
                   grid5: $scope.gridPrompts,
                   grid6: $scope.gridRespostas
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
            userLog(stmt, "Exporta XLSX ", 5, err)
            //?
          });

    };
}

CtrlConsolidadoED.$inject = ['$scope', '$globals'];
