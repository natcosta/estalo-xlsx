function CtrlUser($scope, $globals) {

    win.title = "Cadastro de Usuário"; //Matheus 28/02/2017
    $scope.versao = versao;

    //$scope.sqlBusy = false;
    $scope.rotas = rotas;
    $scope.Empresas = EmpresasLogin;//Controller commons: var EmpresasLogin = ['Oi', 'Versatil', 'CONTAX', 'BRTCC', 'A5 Solutions', 'Coddera', 'Accenture', 'Everis', 'Nuance'];

    //$scope.usuario = {};
    $scope.userData = []; // Dados do grid de usuários
    $scope.globalPerms = rotasInit || rotas; // Permissões globais extraidas do rotasInit
    $scope.dataGridPerms = []; // Dados do grid de permissões trabalhado em listaPerms
    $scope.dataGridGroups = []; // Dados do grid de grupos de permissões trabalhado em listaGroups
    //$scope.formNames = []; // Array com nome dos formulários de permissões

    // Arrays com informações do usuário aberto
    $scope.userPerms = []; // Permissões do usuário em edição
    $scope.newPerms = [];
    $scope.userGroups = []; // Grupos do usuário em edição
    $scope.solicitacoes = [];
    $scope.requestPerms = []; // Permissões requisitadas
    //Fim da lista de Arrays com informações do usuário aberto

    //requests
    $scope.reqTelefone = "";
    $scope.reqEmail = "";
    $scope.reqLogin = "";
    $scope.reqDominio = "";
    $scope.reqEmpresa = "";
    $scope.reqNome = "";
    $scope.request = false; //Determina se é uma solicitação de cadastro do estalo ou não
    $scope.userTarget = "";
    $scope.domainTarget = "";
    $scope.userOrigem = "";
    $scope.domainOrigem = "";
    //fim dos requests

    //Variáveis de usuário em aberto
    //$scope.editUser = {login: '', dominio: '', email: '', nome:'', empresa:''};

    //Colunas do grid de permissões
    $scope.permissions = [
        { field: "detalhes", displayName: "Relatórios", cellClass: "grid-align", width: '90%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm row{{row.rowIndex+1}}">{{row.getProperty(\"detalhes\")}}</span></div>' },
        { field: "checked", displayName: "Habilitado", cellClass: "grid-align habilitado", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm" ><input type="checkbox" id="{{row.getProperty(\'form\')}}" class="PermissionCheckBox" ng-model="form" ng-change="updatePerms()"></span></div>' }
        // cellClass:"span2", width: 80,
    ];

    $scope.reqPermissions = [
        { field: "detalhes", displayName: "Relatórios", cellClass: "grid-align", width: '90%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm row{{row.rowIndex+1}}">{{row.getProperty(\"detalhes\")}}</span></div>' },
        { field: "checked", displayName: "Habilitado", cellClass: "grid-align habilitado", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm" ><input type="checkbox" id="req{{row.getProperty(\'form\')}}" class="PermissionCheckBox RequestCheckBox" ng-model="form" ng-change=""></span></div>' }
        // cellClass:"span2", width: 80,
    ];

    //Grid de permissões
    $scope.gridPerms = {
        data: "dataGridPerms",
        columnDefs: "permissions",
        enableColumnResize: false,
        enablePinning: false,
        rowHeight: 60
    };

    //Grid de permissões
    $scope.gridReqPerms = {
        data: "dataGridPerms",
        columnDefs: "reqPermissions",
        enableColumnResize: false,
        enablePinning: false,
        rowHeight: 60
    };

    $scope.requests = [
        { field: "Nome", displayName: "Nome", cellClass: "grid-align", pinned: true },// cellClass:"span1", width: 150,
        { field: "LoginUsuario", displayName: "Login", cellClass: "grid-align", pinned: true },// cellClass:"span2", width: 80,
        { field: "Dominio", displayName: "Domínio", cellClass: "grid-align", pinned: true },//  cellClass:"span1", width: 80,
        { field: "Empresa", displayName: "Empresa", cellClass: "grid-align", pinned: true },// cellClass:"span2",width: 80,
        { field: "EmailUsuario", displayName: "Email", cellClass: "grid-align", pinned: false },//  cellClass:"span4", width: 120,
        //{ field: "Cpf", displayName: "CPF", cellClass: "grid-align", pinned: false},//  cellClass:"span4", width: 120,
        { field: "Tools", displayName: "Funções", cellClass: "grid-align", width: 80, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-userRequest" ng-click="reqUser(row.entity)" ng-model="reqUserButton" title="Verificar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];

    $scope.gridRequests = {
        data: "solicitacoes",
        columnDefs: "requests",
        enableColumnResize: false,
        rowHeight: 60
    };

    $scope.groups = [
        { field: "groupName", displayName: "Grupos", cellClass: "grid-align", width: '90%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellGroup">{{row.getProperty(\"groupName\")}}</span></div>' },
        { field: "groupChecked", displayName: "Habilitado", cellClass: "grid-align ", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellGroup" ><input type="checkbox" class="groupCheckBox" id="{{row.getProperty(\'groupCodigo\')}}" ng-click="updateGroups()" ></span></div>' }
        // cellClass:"span2", width: 80,
    ];

    //Grid de permissões
    $scope.gridGroups = {
        data: "dataGridGroups",
        columnDefs: "groups",
        enableColumnResize: false,
        enablePinning: false
    };

    //Colunas do grid de usuários
    $scope.colunas = [
        { field: "Nome", displayName: "Nome", cellClass: "grid-align", pinned: true },// cellClass:"span1", width: 150,
        { field: "LoginUsuario", displayName: "Login", cellClass: "grid-align", pinned: true },// cellClass:"span2", width: 80,
        { field: "Dominio", displayName: "Domínio", cellClass: "grid-align", pinned: true },//  cellClass:"span1", width: 80,
        { field: "Empresa", displayName: "Empresa", cellClass: "grid-align", pinned: true },// cellClass:"span2",width: 80,
        { field: "EmailUsuario", displayName: "Email", cellClass: "grid-align", pinned: false },//  cellClass:"span4", width: 120,
        //{ field: "Cpf", displayName: "CPF", cellClass: "grid-align", pinned: false},//  cellClass:"span4", width: 120,
        { field: "Tools", displayName: "Funções", cellClass: "grid-align", width: 80, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-view" ng-click="editUser(row.entity)" title="Editar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];

    $scope.filtros = {
        filterText: ""
    };

    // Filtro de UsuariosEstatura
    /*$scope.filterUser = function() {
       var filterText = $scope.userFilter; ///'name:' +
       if (filterText !== '') {
         $scope.filtros.filterText = filterText;
       } else {
         $scope.filtros.filterText = '';
       }
     };*/
    // FIm do Filtro

    // Grid de usuários
    $scope.gridDados = {
        data: "userData",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true,
        filterOptions: $scope.filtros
		/*
		showFilter: true*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {



        $view = $("#pag-permissoes");

        $("#pag-permissoes #modal-register").on('hidden', function () {
            $("#pag-permissoes .registerAlert").hide();
            $("#pag-permissoes .registerAlertText").text("");
            $("#pag-permissoes .container button").blur();
            //console.log("Blurred buttons");
        });

        // Inflam o grid dentro do modal
        $('#pag-permissoes a[href="#groupTab"]').on('shown', function () {
            //console.log('Clicou em groupTab');
            $('#groupTab').resize();
        });

        $('#pag-permissoes a[href="#permTab"]').on('shown', function () {
            $('#permTab').resize();
        });

        $('#pag-permissoes .modal-view').on('shown', function () {
            $('#permTab').resize();
        });

        $('#modal-requests').on('shown', function () {
            $('body').resize();
        });

        $('#modal-userRequest').on('shown', function () {
            $('body').resize();
            $("#modal-requests").modal('hide');
        });

        $("#modal-userRequest").on('hidden', function () {
            $("#modal-requests").modal('show');
        });

        // Fim das funções para inflar o grid dentro do modal

        //console.log("Transformou em botão!");

        //$scope.listaUsers();
        $scope.listaSolicitacoes();
        //Embutido no callBack de listaUsers: $scope.listaGroups();
        $scope.listaPerms($scope.globalPerms); //Executa a listagem de permissões baseado em informações do init.json

        //console.log("Carregou view!");

        //preenchendo list com os relatorios

        $view.on("click", "#insertUser", function () {
            $scope.registraUser();
            //console.log("Formulario Valido");
            //$scope.checaDados();
            //console.log("Formulário Inválido");
        });

		/* Função deprecated 05/2017 - Matheus
    $view.on("click", "#saveUser", function() {
				$scope.salvaPerms();
		});
*/
        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            //limpaProgressBar($scope, "#pag-permissoes");
            $scope.pesquisaDados();

        });

        $view.on("click", ".modal-backdrop", function () {
            $(document).focus();
            //console.log("Clicou no backdrop");
        });

        $view.on("click", ".close", function () {
            $(document).focus();
            //console.log("Clicou no fechar modal");
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

		/*$view.on("click", ".icon-search", function () {
			var $this = $(this);
			var rowLogin = $this.data("value");
			// data-value="{{row.getProperty(\'LoginUsuario\')}}" Removido
			//console.log("Row Login: "+rowLogin);
		});*/


        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-permissoes");
        }

        $(".registerTelefone").maskbrphone({
            useDdd: true,
            useDddParenthesis: true,
            dddSeparator: ' ',
            numberSeparator: '-'
        });

    });

    $scope.reqUser = function (row) {

        $scope.reqLogin = row.LoginUsuario;
        $scope.reqDominio = row.Dominio;

        if (row.Empresa != undefined && row.Empresa != "") {
            $scope.reqEmpresa = row.Empresa;
        }

        if (row.EmailUsuario != undefined && row.EmailUsuario != "") {
            $scope.reqEmail = row.EmailUsuario;
        }

        $scope.reqNome = row.Nome;
        $scope.reqTelefone = row.TelefoneUsuario;

        //checkPermissions(row.LoginUsuario, row.Dominio); Substituido por reqPermissions
        function reqPermissions() {
            //console.log("Searching permissions.");
            $(".PermissionCheckBox").prop('checked', false);

            // Desmarca as CheckBox da classe PermissionCheckBox

            $scope.userPerms = []; // Limpou array responsável por guardar as permissões do usuário em edição
            function permInputCheck(columns) {
                var perm = columns[0].value;
                //console.log("Permissoes encontradas:  "+perm+" DENTRO DE PERM INPUT CHECK!!!!");
                if (perm != null && perm != "" && perm != undefined) {
                    perm = perm.split(",");
                    //console.log("Perm APOS SPLIT: "+perm+" Length: "+perm = ""+" String: "+perm.toString());
                    i = 0;

                    while (i <= perm.length) {
                        var permElem = $('#req' + perm[i]);
                        if (permElem.length) {
                            permElem.prop('checked', true);
                            //console.log("PermElement CHECKED:  "+permElem+" Perm I: "+perm[i]);
                        }
                        i++;
                    }

                    $scope.userPerms = perm; // Adiciona a permissão marcada ao UserPerms
                    //$scope.confirmRequest();
                }
            }


            stmt = "select permissoes from solicitacaoUsuarios where loginUsuario = '" + $scope.reqLogin + "' and dominio = '" + $scope.reqDominio + "';";
            //log(stmt);
            db.query(stmt, permInputCheck, function (err, num_rows) {
                //userlog(stmt, "Verificar permissões solicitadas ", 2, err);
                if (err) {
                    //console.log("Erro na consulta "+stmt+" \n Erro: "+err);
                } else {
                    //console.log("Permission Selecionadas: "+$scope.userPerms.toString());
                }
            });

        }

        reqPermissions();
        //$("#modal-userRequest").show();
        //$("body").resize();

    };

    //Função responsável por alterar as informações e abrir o modal de edição de usuário
    $scope.editUser = function (row) {
        //console.log("Clicked Login: "+row.LoginUsuario+" Domain: "+row.Dominio);
        //console.log("Row entity: "+row);

        //Informações salvas para edição posterior
        $scope.openLogin = row.LoginUsuario;
        $scope.openDominio = row.Dominio;

        //Informações em aberto
        $scope.editLogin = row.LoginUsuario;
        $scope.editDominio = row.Dominio;

        if (row.Empresa != undefined && row.Empresa != "") {
            $scope.editEmpresa = row.Empresa;
        }
        //$scope.editEmpresa = row.Empresa;
        //$view.find("#editEmail").val(""+row.EmailUsuario);

        if (row.EmailUsuario != undefined && row.EmailUsuario != "") {
            $scope.editEmail = row.EmailUsuario;
        }

        if (row.TelefoneUsuario != undefined && row.TelefoneUsuario != "") {
            $scope.editTelefone = row.TelefoneUsuario;
        }

        $scope.editNome = row.Nome;
        $view.find(".row1").css('line-height', 'normal');
        checkPermissions(row.LoginUsuario, row.Dominio);
    };
    //Fim da função responsável por alterar as informações e abrir o FORM de edição de usuário

    // Permissão responsável por checar lista de permissões e grupos por login e domínio
    function checkPermissions(login, dominio) {

        $(".PermissionCheckBox").prop('checked', false);
        $(".groupCheckBox").prop('checked', false);

        // Desmarca as CheckBox da classe PermissionCheckBox

        $scope.userPerms = []; // Limpou array responsável por guardar as permissões do usuário em edição
        function permInputCheck(columns) {
            var perm = columns[0].value;
            //console.log("Perm: "+perm+" com trim: "+perm.trim());
            perm = perm.trim();
            //console.log("Perm checada: #"+perm);

            var permElem = $('#' + perm);
            if (permElem.length) {
                permElem.prop('checked', true);
                $scope.userPerms.push(perm); // Adiciona a permissão marcada ao UserPerms
            } else {
                //console.log("Permission inexistente no init: "+perm);
            }
        }

        function groupInputCheck(columns) {
            var group = columns[0].value;
            //console.log("Grupo: "+group+" marcado!");
            $scope.userGroups.push(group); // Adiciona a permissão marcada ao UserPerms
            $('#' + group).prop('checked', true);
        }
        //console.log("Checando permissões para: "+login+" "+dominio);
        stmt = "select distinct formulario from PermissoesUsuariosEstatura where loginUsuario = '" + login + "' and dominio = '" + dominio + "';";
        stmt2 = "select distinct codGrupo from UsuarioGrupoB where loginUsuario = '" + login + "' and dominio = '" + dominio + "';";
        //log(stmt);
        db.query(stmt, permInputCheck, function (err, num_rows) {

            if (err) {
                //console.log("Erro na consulta "+stmt+" \n Erro: "+err);
            } else {
                //console.log("Permission STMT2: "+stmt2);
                //log(stmt2);
                db.query(stmt2, groupInputCheck, function (err, num_rows) {

                    if (err) {
                        //console.log("Erro na consulta "+stmt+" \n Erro: "+err);
                    } else {
                        //console.log("STMT2 realizado com sucesso, rows: "+num_rows);
                    }
                });
            }
        });

    }
    //Fim Permissão responsável por marcar checkbox de grupos e permissões do usuário


    // Função responsável por REGISTRAR usuário no banco de dados (SEM Permissões)
    $scope.registraUser = function () {

        var sLogin = $scope.Login;
        var sNome = $scope.Nome;
        var sDominio = $scope.Dominio;
        var sEmail = $scope.Email;
        var sEmpresa = $scope.Empresa;
        var sTelefone = $scope.Telefone;

        function validForm() {
            $("#pag-permissoes .registerAlertText").text("");
            //var sCpf = $scope.Cpf;

            //console.log("Login registrado: "+sLogin+" Email: "+sEmail);
            if (
                ((sLogin == "" || sLogin == undefined) ||
                    (sNome == "" || sNome == undefined) ||
                    (sDominio == "" || sDominio == undefined) ||
                    //(sEmail==""||sEmail==undefined)||
                    (sEmpresa == "" || sEmpresa == undefined))
            ) {
                var resultstring = "Favor preencha ";

                //console.log("Valor de login: "+sLogin);
                //console.log("Valor de dominio: "+sDominio);
                //console.log("Valor de nome: "+sNome);

                if (sLogin == "" || sLogin == undefined) {
                    resultstring = resultstring + "login, ";
                    sLogin = "";
                }
                if (sNome == "" || sNome == undefined) {
                    resultstring = resultstring + "nome, ";
                    sNome = "";
                }
                if (sDominio == "" || sDominio == undefined) {
                    resultstring = resultstring + "domínio, ";
                    sDominio = "";
                }
				/*if(sEmail==""||sEmail==undefined){
					resultstring = resultstring+"email, ";
					sEmail="";
				}*/
                if (sEmpresa == "" || sEmpresa == undefined) {
                    resultstring = resultstring + "empresa, ";
                    sEmpresa = "";
                }
				/*if($scope.Cpf==""||$scope.Cpf==undefined){
					$scope.Cpf="";
				}*/

                resultstring = resultstring + " e tente novamente.";
                //console.log("Error: "+resultstring);
                $("#pag-permissoes .registerAlertText").text(resultstring);
                $("#pag-permissoes .registerAlert").show();
                return false;
            }
            if (!$scope.UserController.$valid) {
                $("#pag-permissoes .registerAlertText").text("Formulário Inválido. " + $scope.UserController.$valid);
                $("#pag-permissoes .registerAlert").show();
                return false;
            }

            $("#pag-permissoes .registerAlert").hide();
            return true;
        }

        if (validForm()) {
            //if()){
            //var register = registerUser(sLogin, sDominio, sEmpresa, sNome, $scope.Cpf, sEmail);
            // Faz a verificação no login para tentar registrar usuário de acordo com parametros

            checkLoginToRegister(sLogin, sDominio, sEmpresa, sNome, sEmail, sTelefone, $scope)
            setTimeout(function () {
                $("#pag-permissoes #modal-register").modal('hide');
                $("#pag-permissoes #modal-register input").val("");
            }, 500);
        }

    };
    // Fim da função responsável por registrar usuário SEM Permissões

    $scope.denyRequest = function () {
        var Login = $scope.reqLogin;
        var Dominio = $scope.reqDominio;
        var Nome = $scope.reqNome;
        var Empresa = $scope.reqEmpresa;
        var Telefone = $scope.reqTelefone;
        var Email = $scope.reqEmail;

        var deletestmt = "delete from solicitacaoUsuarios where LoginUsuario = '" + $scope.reqLogin + "' AND Dominio = '" + $scope.reqDominio + "'";
        var emailStmt =
            "insert into FilaNotificacaoEmail(Assunto, Para, Cc, Cco, Mensagem) values ('Solicitação de cadastro no Estalo', '<" + $scope.reqEmail + ">',"
            + " '<suporte@versatec.com.br>', '<jorge.esteves@versatec.com.br>', "
            + "'A solicitação de acesso do usuário " + Nome + " ao Estalo foi recusada."
            + " Por favor, entre em contato através do e-mail suporte@versatec.com.br para maiores informações.')";

	
		
		mssqlQueryTedious(deletestmt, function (err, result) {
			if (err) console.log("Erro ao deletar solicitacaoUSuarios: " + err)
			mssqlQueryTedious(emailStmt, function (err, result) {
				if (err) console.log(err)
				setTimeout(
                        alerte("Usuário " + Login.trim() + " com domínio " + Dominio.trim() + " teve registro negado.", "Ação executada."),
                        500);
                    $scope.listaSolicitacoes(); //Atualiza o grid de usuários
                    $("#modal-userRequest").modal('hide');
                    $("#modal-requests").modal('hide');
			})
		})
		
		

    }
    //Função para permitir o cadastro do usuário
    $scope.allowRequest = function () {
        sLogin = $scope.reqLogin;
        sDominio = $scope.reqDominio;
        sEmpresa = $scope.reqEmpresa;
        sNome = $scope.reqNome;
        sEmail = $scope.reqEmail;
        sTelefone = $scope.reqTelefone;
        $scope.request = true; // Confirma que a solicitação foi feita pelo estalo

        $scope.requestPerms = [];
        var reqChecks = $(".RequestCheckBox:checked");
        var i = 0;

        while (i < reqChecks.length) {
            //console.log("Permission Request Checked: "+reqChecks[i].id);
            $scope.requestPerms.push(reqChecks[i].id);
            i++;
        }

        //console.log("Permissões totais: "+$scope.requestPerms.toString());
        checkLoginToRegister(sLogin, sDominio, sEmpresa, sNome, sEmail, sTelefone, $scope)



    };

    // Verifica as permissões do usuário a ser editado (Chamado pelo editUser)
    $scope.listaPerms = function () {
        var perms = $scope.globalPerms;

        function checkNomeByForm(perms, form) {
            var nomes = "";
            //console.log("perms: "+perms+" form: "+form);
            for (x = 0; x < perms.length; x++) {
                //console.log("s "+x+": "+perms[x].nome+" para "+form);
                if (perms[x].form == form) {
                    if (nomes == "") {
                        nomes = perms[x].nome;
                    } else {
                        nomes = perms[x].nome + ", " + nomes;
                    }
                } else {
                    //console.log(perms[x].nome+" FORM +"+perms[x].form);
                }
            }

            if (nomes) {
                return nomes;
            } else {
                return false;
            }
        }

        var formNames = []; // ex: frmChamUraVoz
        var nomes = []; // ex: Detalhamento de chamadas

        for (i = 0; i < perms.length; i++) {
            if (formNames.indexOf(perms[i].form) == -1) {
                nomes.push(checkNomeByForm(perms, perms[i].form));
                formNames.push(perms[i].form);
            } else {
                //console.log(perms[i].form+" na posição "+formNames.indexOf(perms[i].form)+" I: "+i);
            }
        }
        var arrayDeObjetos = [];

        //console.log("Nomes: "+nomes = ""+" Perms: "+formNames = "");

        for (i = 0; i < nomes.length; i++) {
            var objeto = { detalhes: '', form: '' };
            objeto.detalhes = nomes[i];
            objeto.form = formNames[i];
            arrayDeObjetos.push(objeto);
        }
        //console.log("Data Grid Perms Size: "+arrayDeObjetos = "");

        $scope.dataGridPerms = arrayDeObjetos;

    };

    function populateUserGrid(columns) {
        var gridLoginUsuario = columns[0].value,
            gridDominio = columns[1].value,
            gridEmpresa = columns[2].value,
            gridNome = columns[3].value,
            gridEmailUsuario = columns[4].value,
            gridTelefoneUsuario = columns[5].value;
        //gridCpf = columns[5].value,

        dado = {
            LoginUsuario: gridLoginUsuario,
            Dominio: gridDominio,
            Empresa: gridEmpresa,
            Nome: gridNome,
            EmailUsuario: gridEmailUsuario,
            TelefoneUsuario: gridTelefoneUsuario
        };

        $scope.userData.push(dado);
    }

    function populateRequestGrid(columns) {
        var gridLoginUsuario = columns[0].value,
            gridDominio = columns[1].value,
            gridEmpresa = columns[2].value,
            gridNome = columns[3].value,
            gridEmailUsuario = columns[5].value,
            gridTelefoneUsuario = columns[6].value,
            reqPerms = columns[7].value;

        dado = {
            LoginUsuario: gridLoginUsuario,
            Dominio: gridDominio,
            Empresa: gridEmpresa,
            Nome: gridNome,
            EmailUsuario: gridEmailUsuario,
            TelefoneUsuario: gridTelefoneUsuario,
            Permissoes: reqPerms
            //Cpf: gridCpf
        };

        $scope.solicitacoes.push(dado);
    }

    function populateGroupGrid(columns) {
        var
            groupCodigo = columns[0].value,
            groupName = columns[1].value;

        dado = {
            groupName: groupName,
            groupCodigo: groupCodigo
        };

        //console.log("Pushando dados para DataGridGroups "+groupName+" cod: "+groupCodigo);

        $scope.dataGridGroups.push(dado);
    }

    // Função ListaGrupos responsável por popular grid de grupos
    $scope.listaGroups = function () {

        $scope.dataGridGroups = [];
        //console.log("Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n ");
        listaGroups(populateGroupGrid, $scope);

        // Função responsável pelo grid dos grupos (Registro e edição de usuario)
    };

    // Função responsável por popular o grid
    $scope.listaUsers = function () {
        $scope.userData = [];
        listaUsers(0, populateUserGrid, $scope);
    };

    // Função responsável por popular o grid de solicitações
    $scope.listaSolicitacoes = function () {

		/*// var$btn_gerar = $(this);
		//$btn_gerar.button('loading');*/
        $scope.solicitacoes = [];

        //console.log("Carregando solicitações.. ");
        stmt = "Select * from solicitacaoUsuarios ORDER BY DataSolicitacao DESC";
        //log(stmt);
        db.query(stmt, populateRequestGrid, function (err, num_rows) {

            if (err) {
                console.log("Erro ao carregar solicitações");
            } else {
                $scope.listaUsers();
            }
        });
    };

	/*$scope.pesquisaDados = function (){

		// var$btn_gerar = $(this);
		//$btn_gerar.button('loading');

		var filtroLogin = $view.find(".filtro-login").val();
		var filtroDominio = $view.find(".filtro-dominio").val();

				if((filtroLogin==""||filtroLogin==undefined)&&(filtroDominio==""||filtroDominio==undefined)){
					//console.log("Pesquisa vazia realizada em cadastroUsuario");
					//stmt = "Select top 50 LoginUsuario,Dominio,Empresa,Nome,EmailUsuario from UsuariosEstatura ORDER BY DataCadastro DESC";
				}else{
					$scope.userData = [];
					searchUser(filtroLogin, filtroDominio, populateUserGrid, $scope);
				}

		//$btn_gerar.button('reset');
	};
*/

    $scope.updatePerms = function () {
        var userPerms = $scope.userPerms; // Abriu com essas permissões
        var savedPerms = []; // Usuário está com essas permissões em execução
        var nowPerms = [];
        var deletePerms = [];
        var insertPerms = [];


        //var deleteArray = [];
        var Login = $scope.editLogin.trim();
        var Dominio = $scope.editDominio.trim();

        if (Login == undefined || Login == "") {
            return false;
        }

        if (Dominio == undefined || Dominio == "") {
            return false;
        }

        //console.log("User Perms length: "+userPerms = "");

        var permChecks = $("#pag-permissoes .PermissionCheckBox:checked");
        var i = 0;

        while (i < permChecks.length) {
            savedPerms.push(permChecks[i].id);
            i++;
        }

        //console.log("Saved Perms: "+savedPerms = "");
        i = 0;
        while (i < savedPerms.length) {
            //console.log("Perm "+i+": "+savedPerms[i]);
            i++;
        }

        //console.log("B4 ALL \n NowPerms: "+nowPerms.toString()+" \n userPerms: "+userPerms.toString());
        //console.log("Saved Perms Total: "+savedPerms);
        //console.log("User Perms Total: "+userPerms);

        // Função vai comparar os arrays e inserir os novos, deletar os que foram removidos

        i = userPerms.length - 1;
        while (i >= 0) {
            // //console.log("Comparando: "+i+" "+userPerms[i]);
            if (savedPerms.indexOf(userPerms[i]) == -1) {
                //console.log("Ausente no segundo: "+userPerms[i]);
            } else {
                //console.log("Presente nos dois: "+userPerms[i]);
                nowPerms.push(userPerms[i]);
                savedPerms.splice(savedPerms.indexOf(userPerms[i]), 1);
                userPerms.splice(i, 1);
            }

            i--;
        }

        function updatePermissions() {

            var insertStmt = db.use + "INSERT " + db.prefixo + " into PermissoesUsuariosEstatura(LoginUsuario, Dominio, Formulario) VALUES";
            var stmtPerms = "";

            i = 0;
            while (i < savedPerms.length) {
                if (i + 1 == savedPerms.length) {
                    stmtPerms = stmtPerms + "('" + Login + "','" + Dominio + "','" + savedPerms[i] + "')";
                } else {
                    stmtPerms = stmtPerms + "('" + Login + "','" + Dominio + "','" + savedPerms[i] + "'), ";
                }
                i++;
            }

            var deleteStmt = db.use
                + "DELETE " + db.prefixo + " from PermissoesUsuariosEstatura where loginUsuario = '" + Login + "' AND Dominio = '" + Dominio + "' AND (";
            var deletePerms = "";

            i = 0;
            while (i < userPerms.length) {
                if (i + 1 == userPerms.length) {
                    deletePerms = deletePerms + " Formulario = '" + userPerms[i] + "')";
                    //deletePerms = deletePerms + "('"+Login+"','"+Dominio+"','"+userPerms[i]+"')";
                } else {
                    deletePerms = deletePerms + " Formulario = '" + userPerms[i] + "' OR";
                }
                i++;

            }
            insertStmt = insertStmt + stmtPerms;
            deleteStmt = deleteStmt + deletePerms;

            if (deletePerms == "" || deletePerms == undefined) {
                deleteStmt = "";
            }
            if (stmtPerms == "" || stmtPerms == undefined) {
                insertStmt = "";
            }

            if (Login == "" || Login == undefined || Dominio == "" || Dominio == undefined) {
                insertStmt = "";
                deleteStmt = "";
            }
            //console.log("isSqlBusy? Should be!"+$scope.sqlBusy);
            // Permissões totais antes da remoção e da inserção
            nowPerms = unique(nowPerms.concat(userPerms)); //Total de Valores presentes e valores a serem deletados
            $(".PermissionCheckBox").prop("disabled", true);

            //$scope.sqlBusy = true;
            //log(insertStmt);
            db.query(insertStmt, function () { }, function (err, num_rows) {
                //userlog(insertStmt, "Inserção de permissão", 1, err);
                if (err) {
                    //console.log("Erro na sintaxe: "+err);
                    alerte("Falha ao remover permissões de " + Login + ", por favor tente novamente mais tarde.", "Falha em execução.");
                    //console.log("insert não pode ser realizado: "+insertStmt);
                    //$scope.sqlBusy = false;
                    $(".PermissionCheckBox").prop("disabled", false);
                    $('#pag-permissoes .loadingButton').hide();
                    //$("#pag-permissoes #modal-view").modal('hide');

                } else {

                    nowPerms = unique(nowPerms.concat(savedPerms)); //Adiciona os valores inseridos ao array
                    db.query(deleteStmt, function () { }, function (err, num_rows) {
                        if (err) {
                            //console.log("Erro na sintaxe: "+err);
                            alerte("Falha ao remover permissões de " + Login + ", por favor tente novamente mais tarde.", "Falha em execução.");
                            //console.log("Delete não pode ser realizado: "+deleteStmt);
                        } else {

                            //console.log("Now Perms: "+nowPerms);
                            nowPerms = $(nowPerms).not(userPerms).get();
                            alerte("Permissões alteradas com sucesso para o usuário " + Login + ".", "Ação realizada.")
                            setTimeout(function () {
                                $("#bootstrap-alert-box-modal").modal('hide');
                            }, 2000);
                        }
                        //userlog(deleteStmt, "Remoção de permissão", 4, err);

                        //console.log("isSqlBusy? FimDelete "+$scope.sqlBusy);
                        //$scope.sqlBusy = false;
                        //console.log("isSqlBusy? Should Be Deactivated! "+$scope.sqlBusy+" scopo: "+$scope.sqlBusy);
                        $scope.userPerms = nowPerms; // Atualiza o scopo com as permissões que o usuário atualmente possui
                        $(".PermissionCheckBox").prop("disabled", false);
                        $('#pag-permissoes .loadingButton').hide();//button('reset');
                        //$("#pag-permissoes #modal-view").modal('hide');
                    });
                }
            });

            //Fim da função
        }
        function tryAgainInTime() {
            //console.log("Try Again In Time");!$scope.sqlBusy||
            if (db.isIdle()) {
                //$scope.sqlBusy = true;
                $('#pag-permissoes .loadingButton').show();
                //console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
                updatePermissions(); // Realiza as queries
            } else {
                setTimeout(tryAgainInTime, 1000);
            }
        }

        //console.log("isSqlBusy? BEFORE FUNCTION "+$scope.sqlBusy);!$scope.sqlBusy&&
        if (db.isIdle()) {
            //console.log(" IsSQLBUSY?"+$scope.sqlBusy+" dbIdle: "+db.isIdle());      
            $('#pag-permissoes .loadingButton').show();
            //console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
            updatePermissions(); // Realiza as queries baseado nas checkbox da tela de permissões //Matheus 2017.1
        } else {
            setTimeout(tryAgainInTime, 1000)
        }


    }


    $scope.espelhaUser = function () {
        console.log("EspelhaUser.");


        if ($scope.userTarget.length >= 3 && $scope.domainTarget.length >= 2 && $scope.userOrigem.length > 3 && $scope.domainOrigem.length >= 2) {

            confirmLogin($scope.userOrigem, $scope.domainOrigem, confirmLogin($scope.userTarget, $scope.domainTarget, espelhaUser()));

        } else {
            alerte("Preencha os campos do formulário para espelhar um usuário com o outro.", "Ação interrompida");
        }

        function espelhaUser() {
            espelhaStmt = "delete from permissoesUsuariosEstatura where LoginUsuario = '" + $scope.userTarget + "' AND dominio = '" + $scope.domainTarget + "';"
                + " insert into PermissoesUsuariosEstatura select "
                + "'" + $scope.userTarget + "' as loginUsuario, '" + $scope.domainTarget + "' as dominio, Formulario from PermissoesUsuariosEstatura where LoginUsuario = '" + $scope.userOrigem + "' AND dominio = '" + $scope.domainOrigem + "';";
            console.log("EspelhaSTMT? " + espelhaStmt);
            db.query(espelhaStmt, function () { }, function (err, num_rows) {
                console.log("Tentando espelhar...")
                $(".userMirror").val("");
                if (!err) {
                    console.log("Espelhando sem erros.");
                    $("#modal-mirror").modal('hide');
                    setTimeout(function () {
                        alerte("Usuário " + $scope.userTarget + " com domínio " + $scope.domainTarget + " com permissões espelhadas de " + $scope.userOrigem + ".", "Ação realizada.");
                        $scope.userOrigem = "";
                        $scope.userTarget = "";
                        $scope.domainOrigem = "";
                        $scope.domainTarget = "";
                        $("#modal-requests").modal('hide');
                    }, 500);
                } else {
                    console.log("Espelhando com erros.");
                    alerte(err, "Ação interrompida");
                }
            });
        }

    }


    $scope.updateGroups = function () {
        var userGroups = $scope.userGroups; // Abriu com essas Grupos
        var savedGroups = []; // Usuário está com essas Grupos em execução
        var nowGroups = [];
        var deleteGroups = [];


        //var deleteArray = [];
        var Login = $scope.editLogin.trim();
        var Dominio = $scope.editDominio.trim();

        if (Login == undefined || Login == "") {
            return false;
        }

        if (Dominio == undefined || Dominio == "") {
            return false;
        }

        //console.log("User Groups length: "+userGroups = "");

        var groupChecks = $("#pag-permissoes .groupCheckBox:checked");
        var i = 0;

        while (i < groupChecks.length) {
            savedGroups.push(groupChecks[i].id);
            i++;
        }

        //console.log("Saved Groups: "+savedGroups = "");
        i = 0;
        while (i < savedGroups.length) {
            //console.log("Group "+i+": "+savedGroups[i]);
            i++;
        }

        //console.log("B4 ALL \n NowGroups: "+nowGroups.toString()+" \n userGroups: "+userGroups.toString());
        //console.log("Saved Groups Total: "+savedGroups);
        //console.log("User Groups Total: "+userGroups);

        // Função vai comparar os arrays e inserir os novos, deletar os que foram removidos

        i = userGroups.length - 1;
        while (i >= 0) {
            // //console.log("Comparando: "+i+" "+userGroups[i]);
            if (savedGroups.indexOf(userGroups[i]) == -1) {
                //console.log("Ausente no segundo: "+userGroups[i]);
            } else {
                //console.log("Presente nos dois: "+userGroups[i]);
                nowGroups.push(userGroups[i]);
                savedGroups.splice(savedGroups.indexOf(userGroups[i]), 1);
                userGroups.splice(i, 1);
            }

            i--;
        }

        //console.log("Antes da DBQUERY \n NowGroups: "+nowGroups.toString()+" \n userGroups: "+userGroups.toString());
        //console.log("savedGroups filtrado, INSERIR: "+savedGroups);
        //console.log("UserGroups filtrado, DELETAR: "+userGroups);
        //console.log("Is SQL busy? Before updateGroups() "+$scope.sqlBusy);
        function updateGroups() {

            var insertStmt = db.use + "INSERT " + db.prefixo + " into UsuarioGrupoB(LoginUsuario, Dominio, CodGrupo) VALUES";
            var stmtGroups = "";

            i = 0;
            while (i < savedGroups.length) {
                if (i + 1 == savedGroups.length) {
                    stmtGroups = stmtGroups + "('" + Login + "','" + Dominio + "','" + savedGroups[i] + "')";
                } else {
                    stmtGroups = stmtGroups + "('" + Login + "','" + Dominio + "','" + savedGroups[i] + "'), ";
                }
                i++;
            }

            var deleteStmt = db.use
                + "DELETE " + db.prefixo + " from UsuarioGrupoB where loginUsuario = '" + Login + "' AND Dominio = '" + Dominio + "' AND (";
            var deleteGroups = "";

            i = 0;
            while (i < userGroups.length) {
                if (i + 1 == userGroups.length) {
                    deleteGroups = deleteGroups + " CodGrupo = '" + userGroups[i] + "')";
                    //deleteGroups = deleteGroups + "('"+Login+"','"+Dominio+"','"+userGroups[i]+"')";
                } else {
                    deleteGroups = deleteGroups + " CodGrupo = '" + userGroups[i] + "' OR";
                }
                i++;

            }
            insertStmt = insertStmt + stmtGroups;
            deleteStmt = deleteStmt + deleteGroups;

            if (deleteGroups == "" || deleteGroups == undefined) {
                deleteStmt = "";
            }
            if (stmtGroups == "" || stmtGroups == undefined) {
                insertStmt = "";
            }

            if (Login == "" || Login == undefined || Dominio == "" || Dominio == undefined) {
                insertStmt = "";
                deleteStmt = "";
            }
            //console.log("isSqlBusy? Should be!"+$scope.sqlBusy);
            // Grupos totais antes da remoção e da inserção
            nowGroups = unique(nowGroups.concat(userGroups)); //Total de Valores presentes e valores a serem deletados
            $(".groupCheckBox").prop("disabled", true);
            //log(insertStmt);
            db.query(insertStmt, function () { }, function (err, num_rows) {
                if (err) {
                    alerte("Falha ao remover Grupos de " + Login + ", por favor tente novamente mais tarde.", "Falha em execução.");
                    //console.log("insert não pode ser realizado: "+insertStmt+" \n Erro na sintaxe: "+err);
                    //$scope.sqlBusy = false;
                    $(".groupCheckBox").prop("disabled", false);
                    $('#pag-permissoes .loadingButton').hide();
                    //$("#pag-permissoes #modal-view").modal('hide');

                } else {

                    nowGroups = unique(nowGroups.concat(savedGroups)); //Adiciona os valores inseridos ao array

                    //console.log("insert realizado com sucesso: "+insertStmt);
                    //log(deleteStmt);
                    db.query(deleteStmt, function () { }, function (err, num_rows) {
                        if (err) {
                            alerte("Falha ao remover Grupos de " + Login + ", por favor tente novamente mais tarde.", "Falha em execução.");
                            //console.log("Delete não pode ser realizado: "+deleteStmt+" \n Erro na sintaxe: "+err);
                        } else {
                            //console.log("Now Groups: "+nowGroups);
                            nowGroups = $(nowGroups).not(userGroups).get();

                            //console.log("Após a remoção: "+nowGroups);
                            //console.log("Removidos userGroups: "+userGroups.toString());
                            //console.log("Delete realizado com sucesso: "+deleteStmt);
                            alerte("Grupos alteradas com sucesso para o usuário: " + login, "Alterações realizadas.");// de "+Login+"
                        }

                        //console.log("isSqlBusy? FimDelete "+$scope.sqlBusy);
                        //$scope.sqlBusy = false;
                        //console.log("isSqlBusy? Should Be Deactivated! "+$scope.sqlBusy+" scopo: "+$scope.sqlBusy);
                        $scope.userGroups = nowGroups; // Atualiza o scopo com as Grupos que o usuário atualmente possui
                        $(".groupCheckBox").prop("disabled", false);
                        $('#pag-permissoes .loadingButton').hide();//button('reset');
                        //$("#pag-permissoes #modal-view").modal('hide');
                    });
                }
            });

            //Fim da função
        }


        //console.log("isSqlBusy? BEFORE FUNCTION "+$scope.sqlBusy);
		/*if(!$scope.sqlBusy){
			$scope.sqlBusy = true;
			$('#pag-permissoes .loadingButton').show();
			//console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
			updateGroups(); // Realiza as queries
			//Fim da updateGroups
		}*/
    }

    $scope.updateUser = function (row) {

        // Informações guardadas ao abrir edição de usuário
        var oldLogin = $scope.openLogin;
        var oldDomain = $scope.openDominio;

        // Informações possivelmente alteradas
        var newLogin = $scope.editLogin;
        var newDomain = $scope.editDominio;
        var newEmpresa = $scope.editEmpresa || "";
        var newNome = $scope.editNome || "";
        var newEmail = $scope.editEmail || "";
        var newTel = $scope.editTelefone || "";

        if (!newLogin || !newDomain) {
            alerte("Por favor, preencha as informações em branco para que as alterações sejam salvas.", "Falha ao atualizar usuário.");
            return false;
        }


        var updateStmt = db.use + "UPDATE " + db.prefixo + " usuariosEstatura set LoginUsuario = '" + newLogin + "', Dominio = '" + newDomain + "', Empresa = '" + newEmpresa + "', Nome = '" + newNome + "', emailUsuario = '" + newEmail + "', telefoneUsuario = '" + newTel + "' where LoginUsuario = '" + oldLogin + "' AND dominio = '" + oldDomain + "' ";
        var updatePerms = "update permissoesUsuariosEstatura set LoginUsuario = '" + newLogin + "', Dominio = '" + newDomain + "' where LoginUsuario = '" + oldLogin + "' AND dominio = '" + oldDomain + "' ";
        //console.log("Trying to updateUser");

        log(updateStmt);
        db.query(updateStmt, function () { }, function (err, num_rows) {
            if (err) {
                //console.log("Erro ao atualizar usuário: "+err);

                alerte("Falha ao atualizar dados do usuário " + oldLogin + " do domínio " + oldDomain + ".", "Falha em execução.")
                setTimeout(function () {
                    $("#bootstrap-alert-box-modal").modal('hide');
                }, 2000);
            } else {
                db.query(updateStmt, function () { }, function (err, num_rows) {
                    if (err) {
                        alerte("Falha ao atualizar permissões do usuário " + oldLogin + " do domínio " + oldDomain + ", entre em contato com suporte@versatec", "Falha em execução");
                    }

                });
                alerte("Usuário " + oldLogin + " do domínio " + oldDomain + " atualizado com sucesso", "Ação executada.");
                setTimeout(function () {
                    $("#bootstrap-alert-box-modal").modal('hide');
                }, 2000);
                $("#modal-view").modal('hide');
                //$scope.listaUsers();
                $scope.listaSolicitacoes();
            }

        });

    };
}


CtrlUser.$inject = ['$scope', '$globals'];
