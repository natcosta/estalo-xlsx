function CtrlImportacaoAudio($scope, $globals) {

	win.title="Homologação de Prompts"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;
	$scope.prompt = [];
	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;


	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-importacao-audio", "Homologação de Prompts");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;

	$scope.dados = [];


	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	//$scope.ordenacao = 'data_hora';
	//$scope.decrescente = true;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy


	// Filtros
	$scope.filtros = {
		aplicacoes: []
	};



	$scope.aba = 2;
	var distinctEmpresas = [];

    $scope.gridOptions = {
        data: 'myData',
        enablePinning: true,
        columnDefs: [{ field: "codPrompt", displayName: "Prompt", width: 720, pinned: true },
                    //{ field: "codAplicacao", displayName: "Cód. Aplicação", width: 120, pinned: true },
                    { displayName: 'Aplicação', cellTemplate: 
		'<div class="grid-action-cell">'+
		'<select class="aplicacaoAudio"><option value="">Selecione uma Aplicação</option></select></div>' },
                    { displayName: 'Data Início Validade', cellTemplate: 
		'<div class="grid-action-cell">'+
		'<input type="text" class="dataValidade" placeholder="Escreva a data sem barras."></div>' }]
    };
   
    //<input type="checkbox">Aplicar para todos os campos
	//var empresas = cache.empresas.map(function(a){return a.codigo});

	


	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

		$scope.periodo = {
			inicio:inicio,
			fim: fim,
			min: new Date(2013, 11, 1),
			max: agora // FIXME: atualizar ao virar o dia
			/*min: ontem(dat_consoli),
			max: dat_consoli*/
		};

	var $view;

   
	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-importacao-audio");
		  

        carregaAplicacoes($view,false,false,$scope);
        $('.dataValidade').mask('00/00/0000');

	  $view.on("change","input:radio[name=radioB]",function() {

		if($(this).val() === "empresa"){
		  $('#porDDD').attr('disabled', 'disabled');
		  $('#porDDD').prop('checked',false);
		}else{
		  $('#porDDD').attr('disabled', false);
		}

	  });


		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


		// GILBERTO 18/02/2014



		// OCULTAR AO TIRAR O MOUSE estado de dialogo
		

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
		  $('.filtro-codprompt').val("");
		  $scope.limparFiltros.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
	
		$scope.limparFiltros = function () {
			iniciaFiltros();
			$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
			$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
			componenteDataHora ($scope,$view,true);

			var partsPath = window.location.pathname.split("/");
			var part  = partsPath[partsPath.length-1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function(){
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			},500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
        });
        
        

		$scope.agora = function () {
			iniciaAgora($view,$scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
		abortar($scope, "#pag-importacao-audio");
		}

	




		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
	  $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};

	

	

    $scope.selecionarAudios = function(){
		$('#audios').click();
	}

    $scope.carregarAplicacoesAudio = function(){
        var stmt = "select CodAplicacao,NomeAplicacao from MapeamentoAplicacoes where DataFim >= getdate() order by NomeAplicacao";
        executaQuery = executaQuery1;

        function executaQuery1(columns) {
            $('.aplicacaoAudio').append('<option value=' + columns[0].value + '>' + columns[1].value + '</option>');
        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            //retornaStatusQuery(num_rows, $scope);
         
        });
    }
	$('#audios').change(function(ev) {
      
        //$scope.listaDados2();
        //$scope.colunas = geraColunas();
        $scope.listaDados();
     
       
    });

  

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {


      //  $scope.myData = [{ codPrompt: "Moroni", age: 50, codAplicacao: "Oct 28, 1970", dataInicio: "60,000" },
      //  { codPrompt: "Tiancum", age: 43, codAplicacao: "Feb 12, 1985", dataInicio: "70,000" }];

        $scope.myData = [];
		$globals.numeroDeRegistros = 0;



		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		//$scope.myData = [];
	
	     
         var filesPath = $('#audios').val().split(';');
           // var filesName = [];

           // filesPath.forEach( function (item,index) { 
            //    filesName.push(item.split('\\').pop());
           // })
    
            filesPath.forEach( function (item,index) { 
                $scope.myData.push({
                    codPrompt: item,
                    codAplicacao : filtro_aplicacoes[0]
                });
        
            })
        
            $scope.$apply();     
            $('.grid-action-cell')[0].id = "zeroCol"
            $('#zeroCol .aplicacaoAudio').attr('onchange','aplicarTodasApls()');

            $('.grid-action-cell')[1].id = "firstCol"
            $('#firstCol .dataValidade').attr('onblur','aplicarTodasDatas()');

            $('#btnSalvar').remove();
            $('.grid-container').append('<input type="button" id="btnSalvar" value="Salvar arquivos em banco de dados" class="btn-default">');

            var tmr = setInterval(function(){ 
                $('#btnSalvar').css('display','block'); 
                $('#btnSalvar').css('margin','0 auto');
            }, 
            100);

            $scope.carregarAplicacoesAudio();
            
           //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
       if($scope.myData.length%1000===0){
            $scope.$apply();
        }



        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });


        componenteDataMaisHora($scope, $view);
        
    };

	$scope.salvar = function (){
		//$scope.prompt.descricao = $scope.descricaoPrompt;
		var sql = "update Prompt set desDetalhada = '" + $scope.descricaoPrompt + "' where codAplicacao = '" + $scope.prompt.codAplicacao + "' and codprompt = '" + $scope.prompt.codPrompt + "'";
		console.log(sql);
		var sqlLog = "insert into LogPrompts values (getDate(),'" + $scope.prompt.codAplicacao + "','" + $scope.prompt.codPrompt + "',";
		sqlLog += "'" + $scope.hdescricaoPrompt + "','" + $scope.descricaoPrompt + "','" + loginUsuario + "','" + dominioUsuario + "')"; 

		db.query(sql + sqlLog, null, function (err, num_rows) {
            console.log("Executando query-> " + sql + " " + num_rows);            
            if (err) {
                console.log("ERRO: " + err);
				alert("ERRO: " + err);
            } else {                
				alert("Prompt salvo com sucesso");	
				$scope.listaDados();
            }  
        });	
		
		//gravar log
	}






	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

	  var linhas  = [];


	  if($scope.valor === "regra"){



		// Criar cabeçalho
		var cabecalho = $scope.datas.map(function (d) {
			return { value: d.data_BR, bold: 1, hAlign: 'center', autoWidth: true };
		});

		// Inserir primeira coluna: item de controle
		cabecalho.unshift({ value: "Regra", bold: 1, hAlign: 'center', autoWidth: true });

		// Inserir última coluna: total por linha
		cabecalho.push({ value: "Total", bold: 1, hAlign: 'center', autoWidth: true });

		var linhas = $scope.dados.map(function (dado) {	
			var linha = $scope.datas.map(function (d) {
				try {
										
					return { value: dado.totais[d.data].qtd_entrantes || 0 };
				} catch (ex) {
					return 0;
				}
			});

			// Inserir primeira coluna: item de controle
			//linha.unshift({ value: dado.cod_ic + " - " + dado.nome_ic });
			linha.unshift({ value: dado.cod_ic});

			// Inserir última coluna: total por linha
			linha.push({ value: dado.totais.geral.qtd_entrantes });

			return linha;
		});

		// Criar última linha, com os totais por data
		var linha_totais = $scope.datas.map(function (d) {
			try {
				return { value: $scope.totais[d.data].qtd_entrantes || 0 };
			} catch (ex) {
				return 0;
			}
		});

		// Inserir primeira coluna
		linha_totais.unshift({ value: "Total", bold: 1 });

		// Inserir última coluna: total geral
		linha_totais.push({ value: $scope.totais.geral.qtd_entrantes });

		// Inserir primeira linha: cabeçalho
		linhas.unshift(cabecalho);

		// Inserir última linha: totais por data
		linhas.push(linha_totais);



	  }else{

		var header = distinctEmpresas;


		var h =[];
		  
		  
		  h.push({value: "Data" , autoWidth:true});

		for(var i= 0; i<header.length;i++){

		  h.push({value: header[i] , autoWidth:true});
		}
		linhas.push(h);


		$scope.dados.map(function (dado) {
		  var l = [];
			 l.push({value:dado.datReferencia, autoWidth:true});
		  for(var i= 0; i<header.length;i++){
			l.push({value:dado["qtd_"+header[i]], autoWidth:true});
		  }

		  return linhas.push(l);
		});


	  }





		var planilha = {
			creator: "Estalo",
			lastModifiedBy: $scope.username || "Estalo",
			worksheets: [{ name: 'Resumo Chamadas Pré-roteamento', data: linhas, table: true }],
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: { first: 1 }
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');


		var milis = new Date();
		var file = 'resumoEmpresaDestino_' + formataDataHoraMilis(milis) + '.xlsx';


		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, planilha.base64, 'base64');
			childProcess.exec(file);
		}

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);

	  empresas = cache.empresas.map(function(a){return a.codigo});
	  
	  var complemento = "";	
              if($scope.valor === "empresa") complemento +=" EMPRESA";
			  if($scope.valor === "regra") complemento +=" REGRA";	  
			  userLog("", "Exportação "+complemento, 5, "");

    };
    

}

function aplicarTodasDatas(){ 
    var r = confirm("Deseja aplicar " + $(".dataValidade").val() + " para todas as datas?");
    if (r == true) {
        $(".dataValidade").val($(".dataValidade").val()); 
    }
}    

function aplicarTodasApls(){ 
    var r = confirm("Deseja aplicar " + $(".aplicacaoAudio").val() + " para todas as aplicações?");
    if (r == true) {
        $(".aplicacaoAudio").val($(".aplicacaoAudio").val()); 
    }
}  

CtrlImportacaoAudio.$inject = ['$scope', '$globals'];
