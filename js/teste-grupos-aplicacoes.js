var aplicacoesGrupo = [
	{ nomeGrupo: "*880, *3000 e *804", codAplicacao: "3000R1"},
	{ nomeGrupo: "*880, *3000 e *804", codAplicacao: "3000R2"},
	{ nomeGrupo: "*880, *3000 e *804", codAplicacao: "3000R3"},
	{ nomeGrupo: "*880, *3000 e *804", codAplicacao: "NOVA880"},
	{ nomeGrupo: "*880, *3000 e *804", codAplicacao: "URA804"},
	{ nomeGrupo: "Empresarial", codAplicacao: "EMPUNIF"},
	{ nomeGrupo: "Empresarial", codAplicacao: "EMPUNIF2"},
	{ nomeGrupo: "Fixa R1 e R2", codAplicacao: "10314UNIF"},
	{ nomeGrupo: "Fixa R1 e R2", codAplicacao: "10331UNIF"},
	{ nomeGrupo: "Televendas", codAplicacao: "TLVCOPA"},
	{ nomeGrupo: "Televendas", codAplicacao: "TLVEMP"},
	{ nomeGrupo: "Televendas", codAplicacao: "TLVNGR"},
	{ nomeGrupo: "Televendas", codAplicacao: "TLVWEB"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "PUSHNOCREDITREC"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "PUSHNOCREDITSVA"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSD"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSD144"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSD2020"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSD3000"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSD880"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSDPUSH"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSDPUSHACM"},
	{ nomeGrupo: "USSD e PUSH", codAplicacao: "USSDSMS"}
];

/* select * from GrupoAplCadastroValores */

function sugereGrupo(apl) {
  var x = aplicacoesGrupo.filter(function (o) { return o.codAplicacao === apl; });
  if (x.length === 0) return;
  var grupo = x[0].nomeGrupo;
  var aplicacoes = aplicacoesGrupo.filter(function (o) { return o.nomeGrupo === grupo; }).map(function (o) { return o.codAplicacao; });
  return { nomeGrupo: grupo, aplicacoes: aplicacoes };
}

var ret = sugereGrupo('TLVNGR');
if (ret !== undefined) {
  console.log("Seu tapado, não esqueceu nada não?");
  console.log(ret.aplicacoes);
} else {
  console.log("Partiu cadastro então")
}