function CtrlDashboardMailing($scope, $globals) {

	win.title="Dashboard Mailing"; 
	$scope.versao = versao;
	$scope.tamanhoMailings = []
	$scope.tipoBusca = ""
	$scope.resultadoBusca = []

	tabelas_mailing = [
		{nome:"Atend_Convergente_TV", nome_campo:"telefone", tipo_busca:"TEL"},
		{nome:"Cliente_Migrado_Prioridade", nome_campo:"CNPJ", tipo_busca:"CPF"},
		{nome:"Clientes_Migrados", nome_campo:"CNPJ", tipo_busca:"CPF"},
		{nome:"Cobranca_Retencao__Atualiza_Semanal", nome_campo:"Terminal", tipo_busca:"TEL"},
		{nome:"Inadimplentes_Empresarial", nome_campo:"Terminal", tipo_busca:"TEL"},
		{nome:"MailingReclamacaoAnatelEmp", nome_campo:"CPF_CNPJ", tipo_busca:"CPF"},
		{nome:"Propenso_Movel", nome_campo:"ANI", tipo_busca:"TEL"},
		{nome:"PropensoMultiprodutos", nome_campo:"terminal", tipo_busca:"TEL"},
		{nome:"TesteMailingTel", nome_campo:"Terminal", tipo_busca:"TEL"},
		{nome:"TesteMailingCNPJ", nome_campo:"CNPJ", tipo_busca:"CPF"}
	]

	campos_tabela_mailing = [
		"BRT",
		"Rentabilizacao",
		"OiMix",
		"PulaPula",
		"Base3G",
		"OCTR2",
		"FuraFilaVelox",
		"Mailing8",
		"Mailing9",
		"Mailing10",
		"Mailing11",
		"Mailing12",
		"Mailing13",
		"Mailing14",
		"CidadeSitiada"
	]

	function getTamanhoMailings(){
		consultas = []
		for(i=0; i<tabelas_mailing.length; i++){
			stmt = "sp_spaceused "+tabelas_mailing[i].nome
			mssqlQueryTedious(stmt, function (err, result){
				if (err) console.log(err);
				$scope.tamanhoMailings.push({nome:result[0][0].value, tamanho:result[0][1].value});
				try{
					$scope.gridMailings.api.setRowData($scope.tamanhoMailings);
				}catch(ex){
					console.log(ex);
				}
			})		
		}

		for(i=0; i<campos_tabela_mailing.length; i++){
			stmt = "select '"+campos_tabela_mailing[i]+"', count (*) from Mailing where "+campos_tabela_mailing[i]+" = 'S'"
			mssqlQueryTedious(stmt, function(err, result){
				if (err) console.log(err)
				$scope.tamanhoMailings.push({nome:result[0][0].value, tamanho:result[0][1].value});
				try{
					$scope.gridMailings.api.setRowData($scope.tamanhoMailings);
				}catch(ex){
					console.log(ex);
				}				
				console.log(result);
			});
		}
	}

	function buscaRegistro(registro){
		encontrou = false;
		$("#btnProcurar").prop('disabled', true)

		$("#resultado").html("")
		$scope.resultadoBusca = []
		for (i =0; i<tabelas_mailing.length; i++){
			if($scope.tipoBusca != tabelas_mailing[i].tipo_busca){
				continue;
			}

			tabela_nome = tabelas_mailing[i].nome
			campo = tabelas_mailing[i].nome_campo			
			stmt = "select '"+tabela_nome+"', count(*) from "+tabela_nome+" where "+campo+" = '"+registro+"'"
			mssqlQueryTedious(stmt, function (err, result){
				if (err) console.log(err)
				if (result[0][1].value){
					// $scope.resultadoBusca.push(result[0][0].value)
					encontrou = true
					$("#resultado").append("<li>"+result[0][0].value+"</li>")
				}
			})	
		}

		mailing_stmt = "select * from Mailing where terminal = '"+registro+"'"
		mssqlQueryTedious(mailing_stmt, function (err, result){
			if (err) console.log(err)
			if (result.length > 0){
				linha = result[0]
				console.log("linha:")
				console.log(linha)
				for (col=1; col<linha.length; col++){
					if (linha[col].value=='S'){
						encontrou = true
						// $scope.resultadoBusca.push(linha[col].metadata.colName)
						$("#resultado").append("<li>"+linha[col].metadata.colName+"</li>")
					}
				}
			}
		})
		setTimeout(function(){
			$("#btnProcurar").prop('disabled', false)
			if (!encontrou){
				$("#resultado").html("Número não existe em nenhum mailing.")
			}
		}, 6000)
	}

	$scope.gridMailings = {
		rowSelection: 'single',
		enableColResize: true,
		enableSorting: true,
		enableFilter: true,
		onGridReady: function(params) {
			params.api.sizeColumnsToFit();
		},
		overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
		overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
		columnDefs: [
			// { headerName: '', width: 30, checkboxSelection: true, supressSorting: true, supressMenu: true, pinned: true },
			{ headerName: 'Nome', field: 'nome' },
			{ headerName: 'Tamanho', field: 'tamanho' }
		],
		rowData: $scope.tamanhoMailings
	}



	getTamanhoMailings()


	$("#aba1").click(function(){
		$("#buscar-por-cpf").css("display","none")
		$("#relatorio-geral").css("display","")
		$("ul.nav-tabs li:nth-child(2)").removeClass("active")
		$("ul.nav-tabs li:first").addClass("active")
	});
	$("#aba2").click(function(){
		$("#relatorio-geral").css("display","none")
		$("#buscar-por-cpf").css("display","")
		$("ul.nav-tabs li:first").removeClass("active")
		$("ul.nav-tabs li:nth-child(2)").addClass("active")
	});

	$('input[name="tipoBusca"]:radio').change(function () {
        $scope.tipoBusca = $("input[name='tipoBusca']:checked").val();
    });

    $("#btnProcurar").click(function(){
    	console.log($scope)
    	buscaRegistro($("#registroBuscado").val())
    })
}
CtrlDashboardMailing.$inject = ['$scope','$globals'];