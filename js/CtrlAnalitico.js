

function CtrlAnalitico($scope, $globals) {

    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 5000;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    travaBotaoFiltro(0, $scope, "#pag-analitico", "Controle analítico do Estalo");
    $scope.status_progress_bar = 0;
    $scope.dados = [];
    $scope.analysis = [];
    $scope.dataAnalysis = [];
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aba = 2;

          /*{ field: "dominio", displayName: "Domínio",  pinned: false },
          { field: "nome", displayName: "Nome",  pinned: false },
          { field: "query", displayName: "Query", pinned: false },
          { field: "hardware", displayName: "Maquina", pinned: false },*/
	 $scope.colunas = [
          { field: "login", displayName: "Login", width: "10%", pinned: false },
          { field: "acao", displayName: "Ação", width: "30%", pinned: false },
          { field: "crud", displayName: "Operação", width:  "10%", pinned: false},
          { field: "erro", displayName: "Erros", width:  "10%", pinned: false },
          { field: "relatorio", displayName: "Relatório", width: "15%", pinned: false },
          { field: "data", displayName: "Data", width: "15%", pinned: false},
          { field: "Tools", displayName: "Funções",cellClass: "grid-align", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a ng-model="showAnalysisButton" ng-click="grid.appScope.showAnalysis(row.entity)" title="Editar" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
  ];
  
  $scope.colunasAnalysis = [
    { field: "dominio", displayName: "Domínio",  width: "30%", pinned: false },
    { field: "relatorio", displayName: "Relatório", width: "50%", pinned: false },
    { field: "quantidade", displayName: "Quantidade", width: "20%", pinned: false }
  ];

    $scope.gridDados = {        
      data: $scope.analysis,
      columnDefs: $scope.colunas,
      rowHeight:55,
      enableColumnResizing: true
    };
  
  $scope.gridAnalysis = {        
    data: $scope.dataAnalysis,
    columnDefs: $scope.colunasAnalysis,
    rowHeight:55,
    enableColumnResizing: true
  };
    
    // Filtros: data e hora
    var agora = new Date();
    //var dat_consoli = new Date(teste_data);
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = agora;

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-analitico");
      treeView('#pag-analitico #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-analitico #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-analitico #ed', 65,'ED','dvED');
      treeView('#pag-analitico #ic', 95,'IC','dvIC');
      treeView('#pag-analitico #tid', 115,'TID','dvTid');
      treeView('#pag-analitico #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-analitico #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-analitico #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-analitico #parametros', 225,'Parametros','dvParam');
      treeView('#pag-analitico #admin', 255,'Administrativo','dvAdmin');
      treeView('#pag-analitico #monitoracao', 275, 'Monitoração', 'dvReparo');
      treeView('#pag-analitico #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
      treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/

        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        //minuteStep: 5


        //19/03/2014
        componenteDataMaisHora($scope, $view);

        carregaUsuarios($view);
        carregaRelatorios($view);
        carregaDominios($view);


        $view.find("select.filtro-usuario").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} usuários',
            showSubtext: true
        });

        $view.find("select.filtro-relatorio").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} relatórios',
            showSubtext: true
        });

        $view.find("select.filtro-dominio").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} domínios',
            showSubtext: true
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-analitico");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
            $(".analysis-button").prop("disabled", false);
        });

        $scope.abortar = function () {
            abortar($scope, "#pag-analitico");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };

    $scope.listaDados = function () {
        $globals.numeroDeRegistros = 0;
        
        /*var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);*/

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
        
        var filtro_usuarios = $view.find("select.filtro-usuario").val() || [];
        var filtro_relatorios = $view.find("select.filtro-relatorio").val() || [];
        var filtro_dominios = $view.find("select.filtro-dominio").val() || [];
        
        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        
        $scope.dados = [];

        var tabela = "logEstaloUsers";

        stmt = db.use + '';
        
        stmt += " Select datahora, login, u.dominio, query, relatorio, acao, crud, erro, hardware, Nome"
        + " FROM " + db.prefixo + tabela + " l"
        + " left outer join UsuariosEstatura u"
        + " on l.login = u.LoginUsuario"
        + " WHERE 1 = 1"
        + " AND CONVERT(DATE,datahora) >= '" + formataData(data_ini) + "'"
        + " AND CONVERT(DATE,datahora) <= '" + formataData(data_fim) + "'";
        stmt += restringe_consulta("login",filtro_usuarios,true);
        stmt += restringe_consulta("relatorio",filtro_relatorios,true);
        stmt += restringe_consulta("u.dominio",filtro_dominios,true);
        stmt += " ORDER BY datahora";

        
  	function populateAnalysis(columns) {
            var data = formataDataHoraBR(columns["datahora"].value),
                login = columns["login"].value,
                dominio = columns["dominio"].value,
                query = columns["query"].value,
                relatorio = columns["relatorio"].value,
                nome = columns["Nome"].value,
                crud = columns["crud"].value,
                acao = columns["acao"].value,
                erro = columns["erro"].value,
                hard = columns["hardware"].value;
                
            //console.log("Adicionando: "+login+" usuario: "+nome);
            
            dado = {
                data: data,
                login: login,
                dominio: dominio,
                query: query,
                relatorio: relatorio,
                nome: nome,
                crud: crud,
                acao: acao,
                erro: erro,
                hardware: hard
            };
            
            $scope.dados.push(dado);           
            
           //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-analitico");
        }



            db.query(stmt, populateAnalysis, function (err, num_rows) {

                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                //$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                  //console.log("Scope Dados: "+$scope.dados);
                    $scope.analysis = $scope.dados;
                    $scope.gridDados.data = $scope.analysis;
                    //$btn_exportar.prop("disabled", false);
                }
            });
            
    };

    $scope.showAnalysis =  function (row){
      
			$scope.viewResumo = row.acao;
			$scope.viewQuery= row.query;
			$scope.viewLogin = row.login || "";
			$scope.viewError = row.erro || "";
      $scope.viewDominio = row.dominio || "";
			$scope.viewData= row.data;
			$scope.viewRelatorio= row.relatorio;     
			$scope.viewNome= row.nome;
			$scope.viewCrud= row.crud;
      $scope.viewHard = row.hardware;
      computerHardware = $scope.viewHard.split(",");
      //Computer Hardware = Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz,6 GB,1366x671px
      $scope.viewCPU = computerHardware[0];
      $scope.viewRAM = computerHardware[1];
      $scope.viewResolution = computerHardware[2];
      
      
      console.log("Row.Acao: "+row.acao+" Row.Query = "+row.query+" row.login: "+row.login);
      console.log("Row Erro: "+row.erro+" row.Data: "+row.data+" row.nome: "+row.nome+" row Crud: "+row.crud);
      
      console.log("Informações SR: "+$scope.viewResumo+" sQ: "+$scope.viewQuery+" sData: "+$scope.viewData);
      //$view.find(".row1").css('line-height', 'normal');
      $(".modal-view").modal('show');  
    }
    
    $scope.listaAnalysis = function () {
      $(".analysis-button").prop("disabled", true);
        $globals.numeroDeRegistros = 0;
        
        /*var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);*/

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
        
        var filtro_usuarios = $view.find("select.filtro-usuario").val() || [];
        var filtro_relatorios = $view.find("select.filtro-relatorio").val() || [];
        var filtro_dominios = $view.find("select.filtro-dominio").val() || [];
        
        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        
        $scope.dados = [];

        var tabela = "logEstaloUsers";
        stmt = "";
        stmt += "select relatorio, dominio, COUNT(relatorio) as qtd from logEstaloUsers where 1=1 "
        + " AND CONVERT(DATE,datahora) >= '" + formataData(data_ini) + "'"
        + " AND CONVERT(DATE,datahora) <= '" + formataData(data_fim) + "'";
        stmt += restringe_consulta("relatorio",filtro_relatorios,true);
        stmt += restringe_consulta("u.dominio",filtro_dominios,true);
        stmt += " GROUP By dominio, relatorio";

        
  	function populate(columns) {
      //data = formataDataHoraBR(columns["datahora"].value),
                
            var dominio = columns["dominio"].value,
                qtd = columns["qtd"].value || columns[3].value,
                relatorio = columns["relatorio"].value;
                
                //console.log("Informações puxadas: dominio "+dominio+" relatorio "+relatorio+" quantidade "+qtd);
            dado = {
                dominio: dominio,
                relatorio: relatorio,
                quantidade: qtd
            };            
            $scope.dataAnalysis.push(dado);                       
           //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-analitico");
        }
    
     db.query(stmt, populate, function (err, num_rows) {

                $(".analysis-button").prop("disabled", false);
                console.log("Executando query-> " + stmt + " " + num_rows);
                
                //$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                    $scope.gridAnalysis.data = $scope.dataAnalysis;
                    $(".modal-analysis").modal('show');
                    
                    setTimeout(function(){
                      
                      $("body").resize();
                      $(".analysis-button").prop("disabled", false);
                    }, 300);
                }else{
                    notificate("Nenhuma resultado encontrado para análise. Período: " + formataDataBR($scope.periodo.inicio));
                }
            });
    
	}    
    
}
    
CtrlAnalitico.$inject = ['$scope', '$globals'];

