function CtrlResumoEPSTelaUnica($scope, $globals) {

    win.title = "Resumo por EPS"; //Alex 27/02/2014
    $scope.versao = versao;

    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-resumo-eps-telaunica", "Resumo por EPS");

    $scope.dados = [];
    $scope.csv = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
		porHora: false,
        porDia: true,
		porMes: false
    };	
	$scope.operador = false;

    function geraColunas() {
        var array = [];

		if($scope.operador){
			array.push(
				{
					field: "datReferencia",
					displayName: "Data",
					width: 150,
					pinned: true
				}, 
				{
					field: "codempresa",
					displayName: "Empresa",
					width: 150,
					pinned: true
				},
				{
					field: "login",
					displayName: "Login",
					width: 150,
					pinned: true
				},
				{
					field: "QTD",
					displayName: "Qtd",
					width: 150,
					pinned: true
				}
			);
		}else{
			array.push({
					field: "datReferencia",
					displayName: "Data",
					width: 150,
					pinned: true
				}, {
					field: "contax",
					displayName: "Contax",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}, {
					field: "almaviva",
					displayName: "Alma Viva",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}, {
					field: "tel",
					displayName: "Tel",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}, {
					field: "btcc",
					displayName: "BTCC",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}, {
					field: "semempresa",
					displayName: "Sem Empresa",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}, {
					field: "total",
					displayName: "Total",
					width: 130,
					pinned: true,
					cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
				}

			);
		}

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-eps-telaunica");


        //19/03/2014
        componenteDataHora($scope, $view);
        carregaAplicacoes($view, false, false, $scope);
        carregaEmpresas($view);
        carregaSites($view);
        carregaOperacoesECH($view);        

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-operacao", function () {
            var filtro_operacoes = $(this).val() || [];
            Estalo.filtros.filtro_operacoes = filtro_operacoes;
        });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function () {
            marcaTodosIndependente($('.filtro-site'), 'sites')
        });

         //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });
   
        // GILBERTO 18/02/2014

        // Marca todos os operacoes
        $view.on("click", "#alinkOper", function () {
            marcaTodosIndependente($('.filtro-operacao'), 'operacoes')
        });

        // Marca todos os planos
        $view.on("click", "#alinkPlano", function () {
            marcaTodosIndependente($('.filtro-tipoplano'), 'planos')
        });

        // Marca todos os pagamentos
        $view.on("click", "#alinkPagamento", function () {
            marcaTodosIndependente($('.filtro-tipopagamento'), 'pagamentos')
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.porRegEDDD = false;
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-eps-telaunica");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
           
            limpaProgressBar($scope, "#pag-resumo-eps-telaunica");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });
		
		$view.on("change", 'input:radio[name=tela]:checked', function() {	
			$scope.filtros.porHora = false;
			$scope.filtros.porDia = false;
			$scope.filtros.porMes = false;
			if ($("input[name='tela']:checked").val() == '0') $scope.filtros.porHora = true;
			if ($("input[name='tela']:checked").val() == '1') $scope.filtros.porDia = true;
			if ($("input[name='tela']:checked").val() == '2') $scope.filtros.porMes = true;
		});  



        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];   
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
        var filtro_planos = $view.find("select.filtro-tipoplano").val() || [];
        var filtro_pagamentos = $view.find("select.filtro-tipopagamento").val() || [];
        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }
        if (filtro_operacoes.length > 0) {
            $scope.filtros_usados += " Operações: " + filtro_operacoes;
        }
        if (filtro_planos.length > 0) {
            $scope.filtros_usados += " Planos: " + filtro_planos;
        }
        if (filtro_pagamentos.length > 0) {
            $scope.filtros_usados += " Pagamentos: " + filtro_pagamentos;
        }
        
        $scope.dados = [];
        $scope.csv = [];
        var stmt = "";
        var executaQuery = "";
		
		
       		
		if($scope.operador){
			
			if(!(filtro_aplicacoes.length === 1 && 
				 filtro_aplicacoes[0] === 'TELAUNICA' && 
				 filtro_sites.length === 0 &&
				 filtro_pagamentos.length === 0)){
					 alert("Filtro Operador somente para aplicação Tela única Pré Pago, filtros Plano e Operações");
					 retornaStatusQuery(0, $scope, "");
					 $btn_gerar.button('reset');
					 return;
				 }
			
		
			if ($scope.filtros.porMes) {
				var campoData = "month";
			}else if ($scope.filtros.porDia) {
				var campoData = "day";
			} else {
				var campoData = "hour";
			}
			
			stmt = db.use +
			" with a as("+
			" select CodUCID,DataHora,var_userpms,"+
			" CASE"+  
			" WHEN PlanoVoz ='Oi Controle sem Fatura'  THEN 'CSF'"+
			" WHEN PlanoVoz ='Oi Controle com Fatura'  THEN 'CCF'"+
			" WHEN PlanoVoz ='Tarifa Ã?Â?nica'  THEN 'TUN'"+
			" ELSE '' END as PlanoVoz"+
			" from TUPreAnalitico a"+
			" where 1 = 1";
			stmt += restringe_consulta5("PlanoVoz", filtro_planos, true) +
			" AND DataHora >= '" + formataDataHora(data_ini) + "'" +
			" AND DataHora <= '" + formataDataHora(data_fim) + "'" +
			" ),"+
			" b as("+
			" select login,CodEmpresa, MIN(codoperacao) as codoperacao"+
			" from LoginsECH"+
			" where 1 = 1"
			stmt += restringe_consulta("CodOperacao", filtro_operacoes, true) +
			" group by login,CodEmpresa"+
			" )"+
			" select "+
			" DATEADD("+campoData+", DATEDIFF("+campoData+", 0, a.DataHora), 0) as DataHora"+
			" ,b.CodEmpresa,b.Login,count(*) AS QTD"+
			" from a"+
			" inner join b"+
			" on a.var_userpms = b.Login"+			
			" group by DATEADD("+campoData+", DATEDIFF("+campoData+", 0, a.DataHora), 0),"+
			" b.Login,b.CodEmpresa"+
			" order by DATEADD("+campoData+", DATEDIFF("+campoData+", 0, a.DataHora), 0) asc";

		}else{
			
			
		if ($scope.filtros.porMes) {
			var campoData = "DATEADD(month, DATEDIFF(month, 0, DatReferencia), 0)";
		}else if ($scope.filtros.porDia) {
			var campoData = "DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)";
		} else {
			var campoData = "DatReferencia";
		}

        stmt = db.use +
            " with tab as (SELECT " + campoData + " as DatReferencia,SUM(QTDACESSOS) as AlmaViva" +
            " FROM ResumoTelaunica" +
            " where CODEMPRESA = 'ALMAVIVA'"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodPlano", filtro_planos, true)
        stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true) +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " GROUP BY " + campoData + ")," +
            " tab2 as (SELECT " + campoData + " as DatReferencia,SUM(QTDACESSOS) as Tel" +
            " FROM ResumoTelaunica" +
            " where CODEMPRESA = 'TEL'"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)   
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodPlano", filtro_planos, true)
        stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true) +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " GROUP BY " + campoData + ")," +
            " tab3 as (SELECT " + campoData + " as DatReferencia,SUM(QTDACESSOS) as Contax" +
            " FROM ResumoTelaunica" +
            " where CODEMPRESA = 'CONTAX'"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodPlano", filtro_planos, true)
        stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true) +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " GROUP BY " + campoData + ")," +
            " tab4 as (SELECT " + campoData + " as DatReferencia,SUM(QTDACESSOS) as BTCC" +
            " FROM ResumoTelaunica" +
            " where CODEMPRESA = 'BTCC'"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodPlano", filtro_planos, true)
        stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true) +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " GROUP BY " + campoData + ")," +
            " tab5 as (SELECT " + campoData + " as DatReferencia,SUM(QTDACESSOS) as SemEmpresa" +
            " FROM ResumoTelaunica" +
            " where CODEMPRESA = ' '"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodPlano", filtro_planos, true)
        stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true) +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " GROUP BY " + campoData + ")"

            +
            " SELECT tab.DatReferencia,isnull(tab.AlmaViva,0) as AlmaViva," +
            " isnull(tab2.Tel,0) as Tel,isnull(tab3.Contax,0) as Contax,isnull(tab4.BTCC,0) as BTCC,isnull(tab5.SemEmpresa,0) as SemEmpresa" +
            " from tab left outer join tab2 on tab.DatReferencia = tab2.DatReferencia" +
            " left outer join tab3 on tab.DatReferencia = tab3.DatReferencia" +
            " left outer join tab4 on tab.DatReferencia = tab4.DatReferencia" +
            " left outer join tab5 on tab.DatReferencia = tab5.DatReferencia" +
            //" where tab.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            //" AND tab.DatReferencia <= '" + formataDataHora(data_fim) + "'" +
            " order by tab.datreferencia";
		}


        executaQuery = executaQuery2;
        log(stmt);

        function executaQuery2(columns) {
			
		if($scope.operador){		
			
			var data_hora = columns["DataHora"].value,
			datReferencia = $scope.filtros.porDia ? formataDataBR(data_hora) : ($scope.filtros.porMes ? formataDataBR(data_hora,'mes') : formataDataHoraBR(data_hora));
			codempresa = columns["CodEmpresa"].value,
			login = columns["Login"].value,
			QTD = columns["QTD"].value;
			
			var s = {
                data_hora: data_hora,
                datReferencia: datReferencia,
                codempresa: codempresa,
                login: login,
                QTD: QTD
            }
			
		}else{	

            var data_hora = columns["DatReferencia"].value,
                datReferencia = $scope.filtros.porDia ? formataDataBR(data_hora) : ($scope.filtros.porMes ? formataDataBR(data_hora,'mes') : formataDataHoraBR(data_hora));
				almaviva = columns["AlmaViva"].value,
                contax = columns["Contax"].value,
                tel = columns["Tel"].value,
                btcc = columns["BTCC"].value,
                semempresa = columns["SemEmpresa"].value,
                total = almaviva + contax + tel + btcc + semempresa;


            if ($scope.filtros.porDia) {
                datReferencia = datReferencia.replace("00:00:00", "")
            };

            var s = {
                data_hora: data_hora,
                datReferencia: datReferencia,
                almaviva: almaviva,
                contax: contax,
                tel: tel,
                btcc: btcc,
                semempresa: semempresa,
                total: total
            }
			
		}
            $scope.dados.push(s);

            var a = [];
            for (var item in $scope.colunas) {
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);
            
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }
        }

        db.query(stmt, executaQuery, function (err, num_rows) {            
		
		
            console.log("Executando query-> " + stmt + " " + num_rows);
            var datFim = "";
            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
      
        });

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };

    // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.csv);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'ResumoEPS' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };

}

CtrlResumoEPSTelaUnica.$inject = ['$scope', '$globals'];