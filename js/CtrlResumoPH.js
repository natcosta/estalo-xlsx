function CtrlResumoPH($scope, $globals) {
	
	win.title = "Resumo hora a hora"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-resumo-horas", "Botão transpor habilitado para intervalos de " + (+limiteHoras / 24).toFixed(0) + " dias. ");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;

	$scope.dados = [];
	$scope.csv = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;

	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	//$scope.ordenacao = 'data_hora';
	//$scope.decrescente = true;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.tlv = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		// ALEX - 29/01/2013
		sites: [],
		segmentos: [],
		isHFiltroED: false
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;

	$scope.porDDD = null;

	//02/06/2014 Flag
	$scope.porRegEDDD = $globals.agrupar;

	$scope.aba = 2;


	function geraColunas() {
		var array = [];

		array.push({
			field: "datReferencia",
			displayName: "Data e hora",
			width: 150,
			pinned: true
		});

		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];

		if ($('#porRegEDDD').prop('checked') === false && filtro_regioes.length > 0 && original_ddds.length === 0) {
			array.push({
				field: "regiao",
				displayName: "Região",
				width: 100,
				pinned: true
			});
		} else if (original_ddds.length > 0) {
			array.push({
				field: "ddd",
				displayName: "DDD",
				width: 100,
				pinned: true
			});
		}

		array.push({
			field: "qtdEntrantes",
			displayName: "Entrantes",
			width: 100,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdDerivadas",
			displayName: "Derivadas",
			width: 100,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdFinalizadas",
			displayName: "Finalizadas",
			width: 100,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdAbandonadas",
			displayName: "Abandonadas",
			width: 115,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidas",
			displayName: "Retidas",
			width: 85,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});

		// Brunno 19/02/2019
		//if (filtro_regioes.length === 0 && original_ddds.length === 0) {
			array.push({
				field: "tma",
				displayName: "TMA",
				width: 65,
				pinned: true,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
			});
		//}

		array.push({
			field: "qtdPercDerivadas",
			displayName: "% Der",
			width: 65,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdPercFinalizadas",
			displayName: "% Fin",
			width: 65,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdPercAbandonadas",
			displayName: "% Aba",
			width: 65,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "percOcupURA",
			displayName: "% URA",
			width: 65,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="{sucesso: row.getProperty(\'percOcupURACor\') == 1,alerta: row.getProperty(\'percOcupURACor\') == 2,erro: row.getProperty(\'percOcupURACor\') == 3}"><span ng-cell-text>{{row.getProperty(col.field)}}</span></div>'
		});


		return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: ontem(dat_consoli),
		max: dat_consoli*/
	};

	var $view;


	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-resumo-horas");


	
		//minuteStep: 5

		//19/03/2014
		componenteDataHora($scope, $view);

		carregaRegioes($view);
		carregaAplicacoes($view, false, false, $scope);
		carregaSites($view);


		carregaSegmentosPorAplicacao($scope, $view, true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view);
		});

		carregaDDDsPorRegiao($scope, $view, true);
		// Popula lista de  ddds a partir das regioes selecionadas
		$view.on("change", "select.filtro-regiao", function () {
			$('#alinkApl').removeAttr('disabled');
			$(".filtro-aplicacao").attr('multiple', 'multiple');
			$('#HFiltroED').prop('checked', false);
			$scope.filtros.isHFiltroED = false;
			$('#oueED').attr('disabled', 'disabled');
			$('#oueED').prop('checked', false);
			//$('#iit').attr('disabled', false);

			carregaDDDsPorRegiao($scope, $view);
		});

		/*$view.find(".selectpicker").selectpicker({
		//noneSelectedText: 'Nenhum item selecionado',
		countSelectedText: '{0} itens selecionados'
		});*/

		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});

		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-regiao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} regiões',
			showSubtext: true
		});



		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-ddd", function () {
			var filtro_ddds = $(this).val() || [];
			Estalo.filtros.filtro_ddds = filtro_ddds;
		});

		$view.on("change", "select.filtro-regiao", function () {
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});

		//GILBERTOO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});




		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function () {
			marcaTodosIndependente($('.filtro-ddd'), 'ddds')
		});

		//Bernardo 20-02-2014 Marcar todas as regiões
		$view.on("click", "#alinkReg", function () {
			marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
		});

		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function () {
			marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
		});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});




		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.porRegEDDD = false;
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);





			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$scope.agora = function () {
			iniciaAgora($view, $scope);

		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-resumo-horas");
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

			$scope.pivot = false;
			limpaProgressBar($scope, "#pag-resumo-horas");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}


			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});

		//Alex 27/02/2014
		$view.on("click", ".btn-pivot", function () {


			if ($scope.pivot === true) {
				$scope.pivot = false;
				$scope.colunas = geraColunas();
				$scope.listaDados.apply(this);

			} else {
				limpaProgressBar($scope, "#pag-resumo-dia");
				$('.chkMensal').prop('disabled', true);
				$scope.pivot = true;
				$scope.colunas = geraColunas();
				$scope.listaDadosPivot.apply(this);
			}


		});

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};


	var dadosHoras = [],
		dadosEntrantes = [],
		dadosRetidas = [],
		dadosTMAs = [],
		dadosDDDs = [],
		dadosRegioes = [],
		dadosFinalizadas = [],
		dadosDerivadas = [],
		dadosAbandonadas = [],
		dadosPercDerivadas = [],
		dadosPercFinalizadas = [],
		dadosPercAbandonadas = [];




	$scope.listaDadosPivot = function () {

		$scope.colunas = [];
		$scope.dados = [];

		var temp = [];
		dadosHoras.unshift("data");
		dadosEntrantes.unshift("Entrantes");
		temp.push(dadosEntrantes);
		dadosDerivadas.unshift("Derivadas");
		temp.push(dadosDerivadas);
		dadosFinalizadas.unshift("Finalizadas");
		temp.push(dadosFinalizadas);
		dadosAbandonadas.unshift("Abandonadas");
		temp.push(dadosAbandonadas);
		dadosRetidas.unshift("Retidas");
		temp.push(dadosRetidas);

		if (unique(dadosTMAs).toString() !== "") {
			dadosTMAs.unshift("TMAs");
			temp.push(dadosTMAs);
		}

		if (unique(dadosDDDs).toString() !== "") {
			dadosDDDs.unshift("DDD");
			temp.push(dadosDDDs);
		}

		if (unique(dadosRegioes).toString() !== "") {
			dadosRegioes.unshift("Região");
			temp.push(dadosRegioes);
		}

		dadosPercDerivadas.unshift("% Deriv.");
		temp.push(dadosPercDerivadas);
		dadosPercFinalizadas.unshift("% Final.");
		temp.push(dadosPercFinalizadas);
		dadosPercAbandonadas.unshift("% Aband.");
		temp.push(dadosPercAbandonadas);




		for (var i = 0; i < temp.length; i++) {
			var item = {};
			for (var j = 0; j < dadosHoras.length; j++) {

				if (typeof dadosHoras[j] === "object") {

					item[dadosHoras[j].data] = temp[i][j];
				} else {

					item[dadosHoras[j]] = temp[i][j];
				}


			}
			$scope.dados.push(item);
		}

		for (var j = 0; j < dadosHoras.length; j++) {
			if (j !== 0) {
				$scope.colunas.push({
					field: dadosHoras[j].data,
					displayName: dadosHoras[j].dataBR,
					width: 160,
					cellClass: "grid-align",
					pinned: false
				});
			} else {
				$scope.colunas.push({
					field: dadosHoras[j],
					displayName: "Data hora",
					width: 100,
					pinned: true
				});
			}
		}

		retornaStatusQuery($scope.dados.length, $scope);
		//$scope.$apply();

	};

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {


		function testaNGR(valor) {

			var v = cache.apls.filter(function (apl) {
				if (apl.codigo === valor && !apl.ngr) {
					return apl
				}
			}).length > 0;

			return v ? '120' : '300';
		}

		dadosHoras = [],
			dadosEntrantes = [],
			dadosRetidas = [],
			dadosTMAs = [],
			dadosDDDs = [],
			dadosRegioes = [],
			dadosFinalizadas = [],
			dadosDerivadas = [],
			dadosAbandonadas = [],
			dadosPercDerivadas = [],
			dadosPercFinalizadas = [],
			dadosPercAbandonadas = [];


		$globals.numeroDeRegistros = 0;


		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportarCSV = $view.find(".btn-exportarCSV");
		var $btn_exportarDropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportarCSV.prop("disabled", true);
		$btn_exportarDropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		//var filtro_ed = $view.find("select.filtro-ed").val() || [];
		var filtro_ed = $scope.filtroEDSelect || [];
		if (filtro_ed[0] !== undefined) {
			$scope.filtros.isHFiltroED = true;
		} else {
			$scope.filtros.isHFiltroED = false;
		}


		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
			" até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_aplicacoes.length > 0) {
			$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		}
		if (filtro_sites.length > 0) {
			$scope.filtros_usados += " Sites: " + filtro_sites;
		}
		if (filtro_segmentos.length > 0) {
			$scope.filtros_usados += " Segmentos: " + filtro_segmentos;
		}
		if (filtro_ed.length > 0) {
			$scope.filtros_usados += " EDs: " + filtro_ed;
		}

		if (filtro_regioes.length > 0 && filtro_ddds.length === 0) {
			var options = [];
			var v = [];
			filtro_regioes.forEach(function (codigo) {
				v = v.concat(unique2(cache.ddds.map(function (ddd) {
					if (ddd.regiao === codigo) {
						return ddd.codigo
					}
				})) || []);
			});
			unique(v).forEach((function (filtro_ddds) {
				return function (codigo) {
					var ddd = cache.ddds.indice[codigo];
					filtro_ddds.push(codigo);
				};
			})(filtro_ddds));
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
		} else if (filtro_regioes.length > 0 && filtro_ddds.length !== 0) {
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
			$scope.filtros_usados += " DDDs: " + filtro_ddds;
		}


		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";

		$scope.parcial = false;

		var iit = "";
		var iitrdg = "";
		var tlv = "";
		var tlvrdg = "";
		var tempoiit = "";
		var tempoiitrdg = "";
		var tempoiitrau = "";
		var tempotlv = "";
		var tempotlvrdg = "";
		var tempotlvrau = "";

		if ($scope.iit === false) {
			iit = " - ISNULL(QtdTransfURA, 0)";
			tempoiit = " - ISNULL(TempoTransfURA, 0)";
			iitrdg = " - ISNULL(RDG.QtdTransfURA, 0)";
			tempoiitrdg = " - ISNULL(RDG.TempoTransfURA, 0)";
			tempoiitrau = " - ISNULL(RAU.TempoTransfURA, 0)";
		}
		if ($scope.tlv === false) {
			tlv = " - ISNULL(QtdTransfTLV, 0)";
			tempotlv = " - ISNULL(TempoTransfTLV, 0)";
			tlvrdg = " - ISNULL(RDG.QtdTransfTLV, 0)";
			tempotlvrdg = " - ISNULL(RDG.TempoTransfTLV, 0)";
			empotlvrau = " - ISNULL(RAU.TempoTransfTLV, 0)";
		}

		var tempoEncerradas = "TempoAbandonadas+TempoFinalizadas+TempoDerivadas+TempoTransfURA+TempoTransfTLV";
		//var tempoEncerradas = "TempoAbandonadas+TempoFinalizadas+TempoDerivadas+TempoTransfURA";
		//waiting for Gabriel


		if (data_ini <= horaAtual() && data_fim >= horaAtual()) {
			$scope.parcial = true;
		}


		//25/08/2014 Visão por ddd(s) ou por região(ões)
		if (filtro_regioes.length > 0 || filtro_ddds.length > 0) {
			var incluiCampos;
			if ($scope.porRegEDDD) {
				incluiCampos = function () {
					return "";
				};
				$scope.porDDD = "DDDRegiao";
			} else if (original_ddds.length !== 0) {
				incluiCampos = function () {
					return ", RA.ddd";
				};
				$scope.porDDD = "DDD";
			} else {
				incluiCampos = function () {
					return ", regiao";
				};
				$scope.porDDD = "Regiao";
			}




			stmt = db.use +
				"SELECT DATEADD(HOUR, +3, DatReferencia) as horariodeverao, " +
				" DatReferencia" + incluiCampos() +
				" ,SUM(QtdChamadas" + iit + tlv + ") as c" +
				" ,SUM(QtdFinalizadas) as f, SUM(QtdDerivadas" + tlv + ") as d, SUM(QtdAbandonadas) as a," +
				" ISNULL(sum(" + tempoEncerradas + tempoiit + tempotlv + "),0) as tempo " +
				" FROM " + db.prefixo + "resumoAnalitico2 as RA" +
				" left outer join UF_DDD AS UD" +
				" ON RA.DDD = UD.DDD" +
				" WHERE 1 = 1" +
				" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
				" AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("CodSite", filtro_sites, true)
			stmt += restringe_consulta("RA.ddd", filtro_ddds, true)
			stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
			stmt += " GROUP BY DatReferencia" + incluiCampos()
			stmt += " ORDER BY DatReferencia" + incluiCampos();


			//02/06/2014 Visão sem considerar região(ões) e/ou ddd(s)
		} else {

			if ($scope.filtros.isHFiltroED) {

				var iit = "";
				if ($scope.iit === false) iit = "- ISNULL(QtdChamadasIIT, 0)";
				stmt = db.use +
					" Select DATEADD(HOUR, +3, RE.DAT_REFERENCIA) as horariodeverao, " +
					" RE.DAT_REFERENCIA as DatReferencia, " +
					" ISNULL(sum(QtdChamadas" + iit + tlv + "),0) as c, " +
					" ISNULL(sum(QtdFinalizadas),0) as f, " +
					" ISNULL(sum(QtdDerivadas" + tlv + "),0) as d, " +
					" ISNULL(sum(QtdAbandonadas),0) as a, " +
					" ISNULL(sum(TempoEncerradas" + tempoiit + tempotlv + "),0) as tempo " +
					" FROM " + db.prefixo + "Resumo_EncerramentoURAVOZ as RE " +
					" WHERE 1 = 1 " +
					" AND re.Dat_Referencia >= '" + formataDataHora(data_ini) + "'" +
					" AND re.Dat_Referencia <= '" + formataDataHora(data_fim) + "'" +
					restringe_consulta("re.Cod_Aplicacao", filtro_aplicacoes, true) +
					restringe_consulta("re.Cod_Site", filtro_sites, true) +
					restringe_consulta2("re.Cod_Segmento", filtro_segmentos, true)
				stmt += restringe_consulta_like("re.Cod_Estado_Dialogo", filtro_ed, $scope.oueED) +
					" Group by re.Dat_Referencia" +
					" ORDER BY re.Dat_Referencia";
			} else {


				var tabelaPrincipal = "resumodesempenhogeralhora";


				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					tabelaPrincipal = "ResumoAtendimentoURA";
				}


				stmt = db.use
				if (filtro_aplicacoes.length === 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {

					stmt += " with RDG as ( "
				}

				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += "SELECT datahora as DatReferencia,DATEADD(HOUR, +3, datahora) as horariodeverao,"
				} else {
					stmt += "SELECT Dat_Referencia as DatReferencia,DATEADD(HOUR, +3, Dat_Referencia) as horariodeverao,"
				}

				stmt += " ISNULL(sum(RDG.QtdChamadas" + iitrdg + tlvrdg + "),0) as c, " +
					" ISNULL(sum(RDG.QtdFinalizadas),0) as f, " +
					" ISNULL(sum(RDG.QtdDerivadas" + tlv + "),0) as d, " +
					" ISNULL(sum(RDG.QtdAbandonadas),0) as a, " +
					" ISNULL(sum(RDG.TempoEncerradas" + tempoiitrdg + tempotlvrdg + "),0) as tempo "

				stmt += " FROM " + db.prefixo + tabelaPrincipal + " as RDG";
				stmt += " WHERE 1 = 1"

				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += " AND datahora >= '" + formataDataHoraNova(data_ini) + "'" +
						" AND datahora <= '" + formataDataHoraNova(data_fim) + "'"
				} else {
					stmt += " AND Dat_Referencia >= '" + formataDataHora(data_ini) + "'" +
						" AND Dat_Referencia <= '" + formataDataHora(data_fim) + "'"
				}

				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
				} else {
					stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true)
				}

				stmt += restringe_consulta("Cod_Site", filtro_sites, true)
				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += " and tiposervidor = 'A'";
				} else {
					if (filtro_segmentos != "") {
						stmt += restringe_consulta("Cod_Segmento", filtro_segmentos, true)
					} else {
						stmt += " AND Cod_Segmento = ''"
					}
				}


				if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += " GROUP BY datahora";
				} else {
					stmt += " GROUP BY Dat_Referencia";
				}


				if (filtro_aplicacoes.length === 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
					stmt += " ) select DATEADD(HOUR, +3, RDG.DatReferencia) as horariodeverao, RDG.DatReferencia,RDG.c,RDG.f,RDG.d,RDG.a,RDG.tempo," +
						" str(100.0*sum(RAU.TempoEncerradas" + tempoiitrau + tempotlvrau + ")/(" + testaNGR(filtro_aplicacoes[0]) + "*60*60*count(distinct Hostname)),10,2) as percOcupURA" +
						" from RDG LEFT OUTER JOIN ResumoAtendimentoURA as RAU" +
						" ON RDG.DatReferencia = RAU.DataHora"
					stmt += restringe_consulta("RAU.CodAplicacao", filtro_aplicacoes, true) +
						" group by RDG.DatReferencia,RDG.c,RDG.f,RDG.d,RDG.a,RDG.tempo" +
						" order by RDG.DatReferencia";
				} else {
					if ($scope.parcial && filtro_aplicacoes.length >= 1 && filtro_segmentos.length === 0 && filtro_sites.length === 0) {
						stmt += " ORDER BY datahora";
					} else {
						stmt += " ORDER BY Dat_Referencia";
					}
				}
			}
			$scope.porDDD = null;
		}

		executaQuery = executaQuery2;
		log(stmt);

		var stmtCountRows = stmtContaLinhas(stmt);

		// Alex 24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			$globals.numeroDeRegistros = columns[0].value;
		}


		function executaQuery2(columns) {
			var
				data_hora = columns["DatReferencia"].value,
				datReferencia = formataDataHoraBR(data_hora,columns["horariodeverao"].value - data_hora),
				qtdFinalizadas = +columns["f"].value,
				qtdDerivadas = +columns["d"].value,
				qtdAbandonadas = +columns["a"].value,
				qtdEntrantes = +columns["c"].value,
				qtdRetidas = qtdFinalizadas + qtdAbandonadas,
				tma = columns["tempo"] !== undefined ?
				((+columns["tempo"].value) / qtdEntrantes).toFixed(2) :
				undefined,
				ddd = columns["ddd"] !== undefined ? +columns["ddd"].value : undefined,
				regiao = columns["regiao"] !== undefined ? (columns["regiao"].value === null ? regiao = "ND" : columns["regiao"].value) : undefined,
				qtdPercFinalizadas = (100 * qtdFinalizadas / qtdEntrantes).toFixed(2),
				qtdPercDerivadas = (100 * qtdDerivadas / qtdEntrantes).toFixed(2),
				qtdPercAbandonadas = (100 * qtdAbandonadas / qtdEntrantes).toFixed(2),
				//qtdPercRetidas = (100 * qtdRetidas / qtdEntrantes).toFixed(2),
				percOcupURA = columns["percOcupURA"] === undefined ? "NA" : columns["percOcupURA"].value === null ? "ND" : +columns["percOcupURA"].value;

			var percOcupURACor = 0;
			if (typeof percOcupURA === "number") {
				percOcupURA >= 0 && percOcupURA <= 30 ? percOcupURACor = 1 : percOcupURA > 30 && percOcupURA <= 60 ? percOcupURACor = 2 : percOcupURA > 60 ? percOcupURACor = 3 : PercOcupURACor = 0;
			}

			var s = {
				data_hora: data_hora,
				datReferencia: datReferencia,
				qtdFinalizadas: qtdFinalizadas,
				qtdDerivadas: qtdDerivadas,
				qtdAbandonadas: qtdAbandonadas,
				qtdEntrantes: qtdEntrantes,
				qtdRetidas: qtdRetidas,
				tma: tma,
				regiao: regiao,
				ddd: ddd,
				qtdPercFinalizadas: qtdPercFinalizadas,
				qtdPercDerivadas: qtdPercDerivadas,
				qtdPercAbandonadas: qtdPercAbandonadas,
				percOcupURA: percOcupURA > 100 ? percOcupURA = 100 : percOcupURA,
				//percOcupURA: percOcupURA,

				temPercOcupURA: typeof percOcupURA === "number",
				percOcupURACor: percOcupURACor
			}

			$scope.dados.push(s);

			var a = [];
			for (var item in $scope.colunas) {
				a.push(s[$scope.colunas[item].field]);
			}

			$scope.csv.push(a);

			// var linhaCSV = [
			// 	formataDataHora(new Date(data_hora)),
			// 	qtdEntrantes,
			// 	qtdDerivadas,
			// 	qtdFinalizadas,
			// 	qtdAbandonadas,
			// 	qtdRetidas,
			// 	tma,
			// 	qtdPercDerivadas,
			// 	qtdPercFinalizadas,
			// 	qtdPercAbandonadas,
			// 	percOcupURA
			// ];

			// $scope.csv.push(linhaCSV);

			dadosHoras.push({
				dataBR: formataDataHoraBR(data_hora),
				data: formataDataHora2(data_hora)
			});
			dadosDDDs.push(ddd);
			dadosRegioes.push(regiao);
			dadosTMAs.push(tma);
			dadosFinalizadas.push(qtdFinalizadas);
			dadosDerivadas.push(qtdDerivadas);
			dadosAbandonadas.push(qtdAbandonadas);
			dadosEntrantes.push(qtdEntrantes);
			dadosRetidas.push(qtdRetidas);
			dadosPercFinalizadas.push(qtdPercFinalizadas);
			dadosPercDerivadas.push(qtdPercDerivadas);
			dadosPercAbandonadas.push(qtdPercAbandonadas);


			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}


		/*,

		// Limpa grid
		function (err, num_rows) {

		    retornaStatusQuery(num_rows, $scope);
		    $btn_gerar.button('reset');
		    $btn_exportar.prop("disabled", false);
		    $scope.$apply(function () {
		    });
		});*/

		//db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
		//console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);
		db.query(stmt, executaQuery, function (err, num_rows) {

			var complemento = "";

			//ResumoAtendimentoURA
			if (stmt.match('ResumoAtendimentoURA') !== null) complemento += " % ocupação URA";


			if ($scope.porRegEDDD === true) {
				$('#pag-resumo-horas table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1170px',
					'margin-bottom': '100px'
				});
			} else {
				$('#pag-resumo-horas table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1280px',
					'margin-bottom': '100px'
				});
			}
			console.log("Executando query-> " + stmt + " " + num_rows);


			var datFim = "";
			if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 > limiteHoras / 24) {
				datFim = " Botão transpor habilitado para intervalos de " + limiteHoras / 24 + " dias.";
			}

			retornaStatusQuery(num_rows, $scope, datFim);
			$btn_gerar.button('reset');
			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportarCSV.prop("disabled", false);
				$btn_exportarDropdown.prop("disabled", false);
			}

			//if(executaQuery !== executaQuery2){
			$('.btn.btn-pivot').prop("disabled", "false");
			if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 <= limiteHoras / 24 && num_rows > 0) {
				$('.btn.btn-pivot').button('reset');
			}
			//}

			userLog(stmt, "Consulta " + complemento, 2, err);
		});
		//});

		// GILBERTOOOOOO 17/03/2014
		$view.on("mouseup", "tr.resumo", function () {
			var that = $(this);
			$('tr.resumo.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});
	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);
		$btn_exportar.button('loading');

		var template = "";
		if ($scope.pivot === false) {
			$scope.porDDD === "DDD" ? template = 'tResumoPHMaisDDD' :
				$scope.porDDD === "Regiao" ? template = 'tResumoPHMaisReg' :
				$scope.porDDD === "DDDRegiao" ? template = 'tResumoPHMaisDDDMaisReg' :
				template = 'tResumoPH';
		} else {
			$scope.porDDD === "DDD" ? template = 'tResumoPHPMaisDDD' :
				$scope.porDDD === "Regiao" ? template = 'tResumoPHPMaisReg' :
				$scope.porDDD === "DDDRegiao" ? template = 'tResumoPHPMaisDDDMaisReg' :
				template = 'tResumoPHP';
		}


		//Alex 15-02-2014 - 26/03/2014 TEMPLATE
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='" + template + "'";
		log(stmt);
		db.query(stmt, function (columns) {
			var dataAtualizacao = columns[0].value,
				nomeRelatorio = columns[1].value,
				arquivo = columns[2].value;


			var milis = new Date();
			var baseFile = "";
			$scope.pivot === false ? baseFile = 'tResumoPH.xlsx' : baseFile = 'tResumoPHP.xlsx';



			var buffer = toBuffer(toArrayBuffer(arquivo));

			fs.writeFileSync(baseFile, buffer, 'binary');

			var file = "";

			$scope.pivot === false ? file = 'resumoHoraAHora_' + formataDataHoraMilis(milis) + '.xlsx' : file = 'resumoHoraAHoraPivot_' + formataDataHoraMilis(milis) + '.xlsx';

			var newData;


			fs.readFile(baseFile, function (err, data) {
				// Create a template
				var t = new XlsxTemplate(data);
				if ($scope.pivot === false) {
					// Perform substitution
					t.substitute(1, {
						filtros: $scope.filtros_usados,
						planDados: $scope.dados
					});
				} else {
					if ($scope.porDDD === "DDD") {


						// Perform substitution
						t.substitute(1, {

							filtros: $scope.filtros_usados,
							datas: dadosHoras.map(function (dh) {
								return dh.dataBR !== undefined ? dh.dataBR : "Data hora";
							}),
							ddds: dadosDDDs,
							entrantes: dadosEntrantes,
							derivadas: dadosDerivadas,
							finalizadas: dadosFinalizadas,
							abandonadas: dadosAbandonadas,
							retidas: dadosRetidas,
							percDerivadas: dadosPercDerivadas,
							percFinalizadas: dadosPercFinalizadas,
							percAbandonadas: dadosPercAbandonadas
						});

					} else if ($scope.porDDD === "DDDRegiao") {

						// Perform substitution
						t.substitute(1, {

							filtros: $scope.filtros_usados,
							datas: dadosHoras.map(function (dh) {
								return dh.dataBR !== undefined ? dh.dataBR : "Data hora";
							}),
							entrantes: dadosEntrantes,
							derivadas: dadosDerivadas,
							finalizadas: dadosFinalizadas,
							abandonadas: dadosAbandonadas,
							retidas: dadosRetidas,
							percDerivadas: dadosPercDerivadas,
							percFinalizadas: dadosPercFinalizadas,
							percAbandonadas: dadosPercAbandonadas
						});

					} else if ($scope.porDDD === "Regiao") {



						// Perform substitution
						t.substitute(1, {

							filtros: $scope.filtros_usados,
							datas: dadosHoras.map(function (dh) {
								return dh.dataBR !== undefined ? dh.dataBR : "Data hora";
							}),
							regioes: dadosRegioes,
							entrantes: dadosEntrantes,
							derivadas: dadosDerivadas,
							finalizadas: dadosFinalizadas,
							abandonadas: dadosAbandonadas,
							retidas: dadosRetidas,
							percDerivadas: dadosPercDerivadas,
							percFinalizadas: dadosPercFinalizadas,
							percAbandonadas: dadosPercAbandonadas
						});

					} else {
						// Perform substitution
						t.substitute(1, {

							filtros: $scope.filtros_usados,
							datas: dadosHoras.map(function (dh) {
								return dh.dataBR !== undefined ? dh.dataBR : "Data hora";
							}),
							entrantes: dadosEntrantes,
							derivadas: dadosDerivadas,
							finalizadas: dadosFinalizadas,
							abandonadas: dadosAbandonadas,
							retidas: dadosRetidas,
							tma: dadosTMAs,
							percDerivadas: dadosPercDerivadas,
							percFinalizadas: dadosPercFinalizadas,
							percAbandonadas: dadosPercAbandonadas
						});
					}
				}

				// Get binary data
				newData = t.generate();


				if (!fs.existsSync(file)) {
					fs.writeFileSync(file, newData, 'binary');
					childProcess.exec(file);
				}
			});

			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);



		}, function (err, num_rows) {
			var complemento = "";
			if ($scope.pivot) complemento = " PIVOT";
			userLog(stmt, "Exportação" + complemento, 5, err);

			//?
		});

	};


	//{ EDs Input Controle
	var modificaAlertaEDs = function (x) {
		if (x == "Elemento repetido ou número máximo de EDs acumulados") {
			$('.notification .ng-binding').text(x).selectpicker('refresh');
			setTimeout(function () {
				$('.notification .ng-binding').text("").selectpicker('refresh');
			}, 3000);
		} else {
			// $("#filtro-ed").attr("placeholder", x);
		}
	};
	var verificaRepetidoEDs = function (x, item) {
		console.log(x);
		if (x.length == 3) return true;
		for (var z in x) {
			try {
				if (x[z].localeCompare(item) == 0) return true;
			} catch (err) {
				console.log(err);
			}
			try {
				if (x[z + 1].localeCompare(item) == 0) return true;
			} catch (err) {
				console.log(err);
			}
		}
		return false;
	};
	//var $myTextarea = $('#demo');
	$scope.abreAjuda = function (link) {
		abreAjuda(link);
	}


}
CtrlResumoPH.$inject = ['$scope', '$globals'];