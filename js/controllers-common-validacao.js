function validaLogGeral(log) {
	/* 
	contem regras que sao comuns aos logs de todas as aplicacoess
	*/


	regex_data = ";;; (Sun|Mon|Tue|Wed|Thu|Fri|Sat) (Jan|Feb|Mar|Apr|May|Jun|Jul|Ago|Sep|Oct|Nov|Dec) \\d{2} \\d{2}:\\d{2}:\\d{2} \\d{4}"


	regras = [{
			regex: "TIME",
			mensagem: "Faltou a variável TIME"
		},
		{
			regex: "BEGIN_TIME",
			mensagem: "Faltou a variável BEGIN_TIME"
		},
		{
			regex: "APP_ANI",
			mensagem: "Faltou a variável APP_ANI"
		},
		{
			regex: "APP_APPL",
			mensagem: "Faltou a variável APP_APPL"
		},
		{
			regex: "APP_Mark",
			mensagem: "Faltou a variável APP_Mark"
		},
		{
			regex: "APP_IC",
			mensagem: "Faltou a variável APP_IC"
		},
		{
			regex: "http.*wav",
			mensagem: "Faltou o link do arquivo .WAV"
		},
		{
			regex: regex_data,
			mensagem: "Faltou a data no final do log"
		}
	]

	negacoes = [ // <= regras que NAO devem ser encontradas no log
		{regex: "APP_\\s{1,}=", mensagem: "Nome de variavel mal formada: APP_"},
		{regex: "APP_\\S*\\s*=\\s*(null|undefined)", mensagem: "Valores null ou undefined encontrado no log"}
	]

	result = {valido:true,msg:[]}
    for (i=0; i<regras.length; i++){
        passes = (new RegExp(regras[i].regex, 'i')).test(log)
        if (!passes){
            console.log("REGRA QUE FALHOU: "+regras[i].regex)
            result.valido = false
            result.msg.push(regras[i].mensagem)
        }   
    }

    for (i=0; i< negacoes.length; i++){
    	passes = (new RegExp(negacoes[i].regex, 'i')).test(log)
    	if (passes){
            console.log("REGRA QUE FALHOU: "+negacoes[i].regex)
            result.valido = false
            result.msg.push(negacoes[i].mensagem)
        }   
    }

    return result

}

function checkGrid(segmentosForaDoPeriodo) {
	var bru = segmentosForaDoPeriodo;
	//console.log('jujuba' + bru)
	if ($('.grid').is(':visible') && bru.length > 0) {
		setTimeout(function () {
			alertBootstrap();
		}, 5000);

		function alertBootstrap() {
			$(".grid").before("<div class='alertinha'></div>");
			$(".alertinha").append('<div class="alert alert-warning alert-dismissible alertaArray"  role="alert"></div>')
			$('.alertaArray').append($('<p />').text('O(s) segmento(s) foram criados após a data do filtro: ' + bru));
			$(".alertinha").fadeTo(4000, 5000).slideUp(5000, function () {
				$(".alertinha").remove()
			});
		}
	} else {
		setTimeout(function () {
			checkGrid(bru);
		}, 5000); //wait 5s, then try again
	}
};


function apenasUSSD($filtro) {
	var filtro_aplicacoes = $filtro.val() || [];
	if (typeof filtro_aplicacoes === 'string') {
		filtro_aplicacoes = [filtro_aplicacoes];
	}
  if(filtro_aplicacoes.length<1){
    return false;
  }else{ 
    return filtro_aplicacoes.every(function (apl) {
        return apl.match(/^(USSD|PUSH)/);
    });
  }
}


function usoDeMemoria(scop) {
	scop.usoDeMemoria = "";
	scop.baseUrl = (base == "") ? "http://www.versatec.com.br/oi3/" : base;
	setInterval(function () {
		scop.usoDeMemoria = (os.freemem() / 1024 / 1024).toFixed(2) + " MB" + " -> " + (os.totalmem() / 1024 / 1024).toFixed(2) + " MB";
		scop.$apply();
		//console.log($scope.usoDeMemoria);
	}, 1000);
}

function validaCartao(r,bandeira){
	
	var valor = "";
	
	var Visa = new RegExp(/^4[0-9]{12}(?:[0-9]{3})/);
	var Mastercard = new RegExp(/^5[1-5][0-9]{14}/);
	var Amex = new RegExp(/^3[47][0-9]{13}/);
	var DinersClub = new RegExp(/^3(?:0[0-5]|[68][0-9])[0-9]{11}/);
	var Discover = new RegExp(/^6(?:011|5[0-9]{2})[0-9]{12}/);
	var JCB = new RegExp(/^(?:2131|1800|35\d{3})\d{11}/);
	
	var testes = [
		{reg:Visa,texto:"Visa"},
		{reg:Mastercard,texto:"Mastercard"},
		{reg:Amex,texto:"Amex"},
		{reg:DinersClub,texto:"Diners Club"},
		{reg:Discover,texto:"Discover"},
		{reg:JCB,texto:"JCB"},
		];
		
	for (var i=0; i<testes.length; i++){
		if(r.match(testes[i].reg)!==null){
			if(bandeira){
				valor = r.replace(/[0-9]{12}/g, testes[i].texto + ' final ');
			}else{
				valor = r.replace(/[0-9]{12}/g, 'XXXX XXXX XXXX ');
			}			
			break;
		}
	}
	return valor !== "" ? valor : r;
}

function mascaraInformacoes(log){
	//VALIDADE
	var validade = "";
	try{
		validade = log.match(/DtValidadeCartao_SB_02.wav((.*?)\r\n)+/)[0].match('RESULT(.*?) = [0-9][0-9][0-9][0-9]')[0].split(' = ')[1]
	}catch(e){
	}
	if(validade!=="") log = log.replace("RESULT[0]                 = "+validade, "RESULT[0]                 = XXXX");
	
	//cartao
	if(log.match(/(.*?)RESULT(.*?) = [0-9]{16}(.*?)/g)!==null){		
		var mascara = validaCartao(log.match(/(.*?)RESULT(.*?) = [0-9]{16}(.*?)/g)[0].split(' = ')[1]);
		log = log.replace(log.match(/(.*?)RESULT(.*?) = [0-9]{16}(.*?)/g)[0].split(' = ')[1],mascara);
	}
	
	//CVV
	var cvv = "";
	try{
		cvv = log.match(/CadastroCartao_SB_03.wav((.*?)\r\n)+/)[0].match('RESULT(.*?) = [0-9][0-9][0-9]')[0].split(' = ')[1]
	}catch(e){
	}
	if(cvv!=="") log = log.replace("RESULT[0]                 = "+cvv, "RESULT[0]                 = XXX");
		
	return log;

}

function mysql_real_escape_string (str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

