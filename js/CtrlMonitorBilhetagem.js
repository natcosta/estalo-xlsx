function CtrlMonitorBilhetagem($scope, $globals) {
	win.title = "Monitor de Bilhetagem"; //Matheus 2017.1
	$scope.versao = versao;
	$scope.qtdChamadas = [];
	$scope.servidoresAtivos = "";
	$scope.servidoresInativos = "---";
	$scope.taxaImportacao = "";
	$scope.datasUltimosRegsProds = "";	
	$scope.aviso = "";
	$scope.ocupacaoBanco = "";
	$scope.initGraph = "";
	$scope.fimGraph = "";
	
	$scope.horarioAtual = formataDataHoraBR(new Date());/*.toLocaleDateString("pt-BR", {
		day: "2-digit",
		month: "2-digit",
		year: "2-digit",
		hour: "2-digit",
		minute: "2-digit",
		second: "2-digit"
	}).replace("h",":").replace("min",":").replace("s","");*/
	var ultimaAtualizacao = new Date();
	$scope.atualizar = function () {

	$scope.horarioAtual = formataDataHoraBR(new Date());/*.toLocaleDateString("pt-BR", {
		day: "2-digit",
		month: "2-digit",
		year: "2-digit",
		hour: "2-digit",
		minute: "2-digit",
		second: "2-digit"
	}).replace("h",":").replace("min",":").replace("s","");*/
		intervalo = 60;
		horarioAtual = new Date();
		tempoEntreAtualizacoes = (horarioAtual.getTime() - ultimaAtualizacao.getTime()) / 1000;
		if (tempoEntreAtualizacoes > intervalo) {			
			getQtdChamadas()
			getServerStatus()
			atualizaGrafico()
			//getNVPQtdChamadas()
			//getNGRqtdGravacaoDeLogs()
			getNGRDatasUltimosRegsProds()
			getOcupacaoBanco()
			
			ultimaAtualizacao = new Date();
		} else {
			$("#btnAtualizar").prop('disabled', true);
			$("#btnAtualizar").prop('value', 'aguarde');
			setTimeout(function () {
				$("#btnAtualizar").prop('disabled', false);
				$("#btnAtualizar").prop('value', 'atualizar');
			}, intervalo * 1000);
		}
	};
	
	Array.prototype.diff = function(a) {
		return this.filter(function(i) {return a.indexOf(i) < 0;});
	};
	
	
	
	$scope.gridOptions = {
					data: 'myData',
					enablePinning: true,
					columnDefs: [{ field: "Data", width: 120, pinned: true },
								{ field: "Logs", width: 120 },
								{ field: "Fibra", width: 120 },
								{ field: "TV", width: 120 },
								{ field: "Velox", width: 120 },
								{ field: "Fixa", width: 120 }]
				};
				$scope.myData = [{ Data: "20/04", Logs: 50, Fibra: 33, TV: 22, Velox: 3344, Fixa: 332 },
								{ Data: "19/04", Logs: 43, Fibra: 22, TV: 32,Velox: 4344, Fixa: 132 },
								{ Data: "18/04", Logs: 34, Fibra: 43, TV: 43,Velox: 434, Fixa: 532 },
								{ Data: "17/04", Logs: 34, Fibra: 23, TV: 334,Velox: 4324, Fixa: 1432 }];
	

	function getQtdChamadas() {
		$scope.qtdChamadas = []
		queries = [
			"select isnull(min(DataHora_inicio),dateadd(minute,-10,getdate())) as datahora, datepart(weekday,isnull(min(DataHora_inicio),dateadd(minute,-10,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(minute,-10,getdate()) and DATEADD(minute,-9, GETDATE())",
			"select isnull(min(Datahora_Inicio),dateadd(day,-1,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-1,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-1,getdate()) and DATEADD(minute,1, dateadd(day,-1,getdate()))",
			"select isnull(min(Datahora_Inicio),dateadd(day,-2,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-2,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-2,getdate()) and DATEADD(minute,1, dateadd(day,-2,getdate()))",
			"select isnull(min(Datahora_Inicio),dateadd(day,-3,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-3,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-3,getdate()) and DATEADD(minute,1, dateadd(day,-3,getdate()))",
			"select isnull(min(DataHora_inicio),dateadd(day,-4,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-4,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-4,getdate()) and DATEADD(minute,1, dateadd(day,-4,getdate()))",
			"select isnull(min(DataHora_inicio),dateadd(day,-5,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-5,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-5,getdate()) and DATEADD(minute,1, dateadd(day,-5,getdate()))",
			"select isnull(min(DataHora_inicio),dateadd(day,-6,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-6,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-6,getdate()) and DATEADD(minute,1, dateadd(day,-6,getdate()))",
			"select isnull(min(DataHora_inicio),dateadd(day,-7,getdate())) as datahora, datepart(weekday,isnull(min(Datahora_Inicio),dateadd(day,-7,getdate()))) as diasemana, count(*) as qtd from IVRCDR  where CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') and DataHora_Inicio between dateadd(day,-7,getdate()) and DATEADD(minute,1, dateadd(day,-7,getdate()))"
		]
		stmt = queries.join(" union ") + "order by isnull(min(DataHora_inicio),dateadd(minute,-10,getdate())) desc"
		mssqlQueryTedious(stmt, function (err, result) {
			if (err) console.log(err)
			//console.log(result)
			for (i = 0; i < result.length; i++) {
				data = formataDataBR3(result[i].datahora.value);
				dia = formataDataDiaSemana(result[i].diasemana.value);
				qtd = result[i].qtd.value;
				$scope.qtdChamadas.push({
					datahora: data,
					diaSemana: dia,
					qtd: qtd
				})
				$scope.$apply();
			}
		})
	}

	function getNVPQtdChamadas() {
		$scope.qtdNVPChamadas = "";
		$scope.qtdNVPChamadasDiff = "";		 

		var qtdNVPChamadas = [];
		
		queries = [];
		var hc = new Date().getHours() >= 18 || new Date().getHours() <= 8;
		var hc = false;
		
	
		var picoTxt = "";
		
		
		if(hc){
			
			var dias = [1,2];
			var textos = ["Ontem","Anteontem"];
			var rand = dias[Math.floor(Math.random() * dias.length)];
			picoTxt = textos[dias.indexOf(rand)];
			
			queries = [
		
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-10,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-1,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-20,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-11,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-30,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-21,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-40,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-31,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-50,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-41,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
			"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-60,'"+formataDataHoraDoze(antes(null,rand))+"') and datahora_inicio <=DATEADD(minute,-51,'"+formataDataHoraDoze(antes(null,rand))+"') and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')"
			
			];
			
			
			
		}else{

			queries = [
		
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-10,GETDATE()) and datahora_inicio <=DATEADD(minute,-1,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-20,GETDATE()) and datahora_inicio <=DATEADD(minute,-11,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-30,GETDATE()) and datahora_inicio <=DATEADD(minute,-21,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-40,GETDATE()) and datahora_inicio <=DATEADD(minute,-31,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-50,GETDATE()) and datahora_inicio <=DATEADD(minute,-41,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')",
				"select HostnameURA from ivrcdr i where  datahora_inicio >=DATEADD(minute,-60,GETDATE()) and datahora_inicio <=DATEADD(minute,-51,GETDATE()) and codaplicacao in ('CORPO','3000R1','3000R2','3000R3','NOVA880','URA804')"
				
			];
		
		}
		stmt = queries.join(" union all ") + "";
		
		var qtdMaquinasInAtivas = [];
		var qtdMaquinasAtivas = [];		
		var queryAtivas = 'select hostname from ControleExtratores where ID = \'TNLGravaCallLogsNVP\' and Habilitado = 1';
		
		
		mssqlQueryTedious(queryAtivas, function(err, result) {
			if (err) throw err;
			for (i = 0; i < result.length; i++) {
				qtdMaquinasAtivas.push(result[i][0].value);
			}
		});
		

		mssqlQueryTedious(stmt, function (err, result) {
			if (err) console.log(err)
			//console.log(result)
			for (i = 0; i < result.length; i++) {
				hostname = result[i].HostnameURA.value;
				var dado = qtdNVPChamadas[hostname];
				if (dado === undefined) {
					dado = {
						hostname: hostname,
						qtd: 0
					};
					qtdNVPChamadas.push(dado);
					qtdNVPChamadas[hostname] = dado;
				}
				if (hostname !== null) qtdNVPChamadas[hostname].qtd += 1;
				if(qtdMaquinasInAtivas.indexOf(hostname) < 0) qtdMaquinasInAtivas.push(hostname); 
				
			}	
			$scope.qtdNVPChamadas = qtdNVPChamadas.length + " de " + qtdMaquinasAtivas.length +" "+(hc ? " ("+picoTxt+" em hora pico)": " (Horário comercial)");
            $scope.qtdNVPChamadasDiff = (qtdMaquinasAtivas.diff(qtdMaquinasInAtivas).length > 0 ? qtdMaquinasAtivas.diff(qtdMaquinasInAtivas).join(", ") : "");
			$scope.$apply()
		})
	}
	
	
	
	
	function getNGRDatasUltimosRegsProds() {
		$scope.datasUltimosRegsProds = "";
		var datasUltimosRegsProds = [];
		var ultimosRegsProds = "select max(datahora_inicio),reparo  from chamadasreparost_"+pad(new Date().getMonth()+1)+" where reparo in ('FIBRA','FIXO','TV','VELOX') group by reparo";
		
		mssqlQueryTedious(ultimosRegsProds, function(err, result) {
			if (err) throw err;
			for (i = 0; i < result.length; i++) {
				if(result[i][1].value !== "MOVEL") datasUltimosRegsProds.push({ "Data": formataDataBR3(result[i][0].value), "Produto": result[i][1].value});
			}
			$scope.datasUltimosRegsProds = datasUltimosRegsProds;			
			$scope.$apply()
		});
	}
	
	function getNGRqtdGravacaoDeLogs() {
		$scope.qtdGravacaoDeLogs = "";		
		var qtdGravacaoDeLogs = "";
		var gravacaoDeLogs = 'select count(*),(select count(*) from controleextratores where id  = \'TNLGravaCallLogsngr\' and habilitado  = 1) from controleextratores where id  = \'TNLGravaCallLogsngr\' and habilitado  = 1 and status like \'%PENDENTES=0%\'';
		
		
		mssqlQueryTedious(gravacaoDeLogs, function(err, result) {
			if (err) throw err;
			var ativos,ativosPendentes;
			for (i = 0; i < result.length; i++) {
				ativo = result[i][0].value;
				ativosPendentes = result[i][1].value;
			}
			if(ativos === ativosPendentes) qtdGravacaoDeLogs = "Possível próblema na gravação de logs";
			$scope.qtdGravacaoDeLogs = qtdGravacaoDeLogs;
			$scope.$apply();
		});
	}
	
	function getOcupacaoBanco() {
		
		$scope.ocupacaoBanco = "";
	stmt = " with cte as ("+
		" select sum(size)/128.0 as total, sum(convert(int, FILEPROPERTY(name, 'SpaceUsed')))/128.0 as used "+
		" from sys.database_files "+
	") "+
	" select str(used / 1000000, 3, 1) + ' TB' as [Em Uso], str(100 * used/total, 4, 1) + '%' as [Percentual] "+
	" from cte";
	
	mssqlQueryTedious(stmt, function (err, result) {
			if (err) console.log(err);
			$scope.ocupacaoBanco = {valor:"", texto:""}
			$scope.ocupacaoBanco.valor = result[0][1].value.split('%')[0];
			$scope.ocupacaoBanco.texto = result[0][1].value;
			$scope.$apply();
		})
	
	}



	function getServerStatus() {
		stmt = "select Hostname, Status, datediff(second, CONVERT(datetime, SUBSTRING(Status,2,19),120), getdate()) as diferenca from ControleExtratores where ID like '%CallLogsNGR' and Habilitado = 1"
		taxaImportacao = 0
		servidoresAtivos = []
		servidoresInativos = []
		mssqlQueryTedious(stmt, function (err, result) {
			if (err) console.log(err);

			for (i = 0; i < result.length; i++) {

				diferenca = result[i].diferenca.value;
				if (diferenca < 30) {
					servidoresAtivos.push(result[i].Hostname.value);
					stat = result[i].Status.value;
					taxa = stat.split(",")[2].replace(/\D/g, '');
					taxaImportacao += +taxa;
				} else {
					servidoresInativos.push(result[i].Hostname.value);
				}
			}

			$scope.servidoresAtivos = servidoresAtivos.join(", ");
			$scope.taxaImportacao = taxaImportacao + " logs/min";
			$scope.$apply()

			if (servidoresAtivos.length <= 0) {
				$scope.aviso = "FAVOR CONTACTAR A VERSÁTIL";
				$scope.$apply();
			}else{
				$scope.aviso = "";
				$scope.$apply();
			}

			if (servidoresInativos.length > 0) {
				$scope.servidoresInativos = servidoresInativos.join(", ");
				$scope.$apply();
			} else {
				$scope.servidoresInativos = "";
				$scope.$apply();
			}



		})
	}

	function atualizaGrafico() {
		
		/*
		var horas = [-1,-2,-3,-4];
		var textos = ["-1h/Agora","-2h/Agora","-3h/Agora","-4h/Agora"];
    	var rand = horas[Math.floor(Math.random() * horas.length)];
		$scope.initGraph = textos[horas.indexOf(rand)].split('/')[0];
		$scope.fimGraph = textos[horas.indexOf(rand)].split('/')[1];
		*/
		var rand = -6;
		$scope.initGraph = "-6h";
		$scope.fimGraph = "Agora";
			
		/*
		stmt = "with a as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from IVRCDR "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+
		"b as("+		
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120) "+
		") "+
		"select a.data, a.hora,a.dia,a.mes,a.ano,a.minuto,a.qtd AS QTDCHAMADA,isnull(b.qtd,0) AS QTDREPARO "+
		"from a left outer join b "+
		"on a.data = b.data "+
		"order by a.data asc;"
		*/
		
		
		stmt = "with a as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from IVRCDR "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+
		/*"b as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF','NOVAOITV') "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120) "+
		"), "+*/
		"c as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('NOVAOITV') "+
		"and reparo = 'FIBRA' "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+
		"d as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF') "+
		"and reparo = 'FIXO' "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+
		/*"e as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('NGRPRE','POSPAGO') "+
		"and reparo = 'MOVEL' "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+*/
		"f as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('NOVAOITV') "+
		"and reparo = 'TV' "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		"),"+
		"g as("+
		"select datepart(day,DataHora_Inicio) as dia, "+
		"datepart(month,DataHora_Inicio) as mes, datepart(year,DataHora_Inicio) as ano, "+
		"datepart(hour,DataHora_Inicio) as hora, datepart(minute,DataHora_Inicio) as minuto, "+
		"count(*) as qtd, CONVERT(varchar(16),DataHora_Inicio, 120)  as data "+
		"from chamadasreparost "+
		"where DataHora_Inicio between DATEADD(hour, "+rand+", GETDATE()) and getdate() "+
		"and CodAplicacao in ('10331UNIF','10314UNIF') "+
		"and reparo = 'VELOX' "+
		"group by datepart(day,DataHora_Inicio), "+
		"datepart(month,DataHora_Inicio), datepart(year,DataHora_Inicio), "+
		"datepart(hour,DataHora_Inicio), datepart(minute,DataHora_Inicio), "+
		"CONVERT(varchar(16),DataHora_Inicio, 120)"+
		")"+
        "select a.data, a.hora,a.dia,a.mes,a.ano,a.minuto,"+
		"isnull(a.qtd,0) AS QTDCHAMADA,"+
		"isnull(c.qtd,0) + isnull(d.qtd,0) + isnull(f.qtd,0) + isnull(g.qtd,0) AS QTDREPARO, "+
		"isnull(c.qtd,0) AS QTDREPAROFIBRA, "+
		"isnull(d.qtd,0) AS QTDREPAROFIXO, "+
		/*"isnull(e.qtd,0) AS QTDREPAROMOVEL, "+*/
		"isnull(f.qtd,0) AS QTDREPAROTV, "+
		"isnull(g.qtd,0) AS QTDREPAROVELOX "+
		"from a "+
		"left outer join c on a.data = c.data "+		
		"left outer join d on a.data = d.data "+
		/*"left outer join e on a.data = e.data "+*/
		"left outer join f on a.data = f.data "+
		"left outer join g on a.data = g.data "+
		"order by a.data asc;";




		
		
				
		mssqlQueryTedious(stmt, function (err, result) {
			console.log("terminou consulta")
			if (err) console.log(err);
			graphData = [];
			
			if (result.length == 0) graphData.push({
				x: 0,
				y: 0,
				hora: 0,
				min: 0
			})
			for (i = 0; i < result.length; i++) {				
				dia = result[i]['dia'].value;
				mes = result[i]['mes'].value;
				ano = result[i]['ano'].value;
				hora = result[i]['hora'].value;
				hora = ("" + hora).length == 1 ? '0' + hora : hora;
				minuto = result[i]['minuto'].value;
				minuto = ("" + minuto).length == 1 ? '0' + minuto : minuto;

				timestamp = new Date(ano, mes, hora, minuto, 0, 0).getTime() / 100000;

				// graphData.push({x: hora+":"+minuto, y: result[i]['qtd'].value})
			
					graphData.push({
						x: i,
						y: result[i]['QTDCHAMADA'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'CHAMADA'
					});
					
					graphData.push({
						x: i,
						y: result[i]['QTDREPARO'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPARO'
					});
					
					graphData.push({
						x: i,
						y: result[i]['QTDREPAROFIBRA'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPAROFIBRA'
					});
					graphData.push({
						x: i,
						y: result[i]['QTDREPAROFIXO'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPAROFIXO'
					});
					/*graphData.push({
						x: i,
						y: result[i]['QTDREPAROMOVEL'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPAROMOVEL'
					});*/
					graphData.push({
						x: i,
						y: result[i]['QTDREPAROTV'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPAROTV'
					});
					graphData.push({
						x: i,
						y: result[i]['QTDREPAROVELOX'].value,
						dia: dia,
						mes: mes,
						hora: hora,
						min: minuto,
						tipo: 'REPAROVELOX'
					});
					
			}
			limpaHTMLGrafico()
			
			
			var graph = new Rickshaw.Graph({
				element: document.querySelector("#chart"),
				width: 750,
				height: 380,
				renderer: 'line',
				series: [{
						data: graphData.filter(function(a){ if(a.tipo === "CHAMADA") return a; }),
						color: 'steelblue',
						name: 'Chamadas'
					} , {
							 data: graphData.filter(function(a){ if(a.tipo === "REPARO") return a; }),
							 color: 'lightblue',
							 name: 'Reparo'
					 }
					 , {
							 data: graphData.filter(function(a){ if(a.tipo === "REPAROFIBRA") return a; }),
							 color: 'Pink',
							 name: 'Fibra'
					 }
					 , {
							 data: graphData.filter(function(a){ if(a.tipo === "REPAROFIXO") return a; }),
							 color: 'Red',
							 name: 'Fixo'
					 }
					 , /*{
							 data: graphData.filter(function(a){ if(a.tipo === "REPAROMOVEL") return a; }),
							 color: 'Pink',
							 name: 'Movel'
					 }
					 ,*/ {
							 data: graphData.filter(function(a){ if(a.tipo === "REPAROTV") return a; }),
							 color: 'Green',
							 name: 'Tv'
					 }
					 , {
							 data: graphData.filter(function(a){ if(a.tipo === "REPAROVELOX") return a; }),
							 color: 'Orange',
							 name: 'Velox'
					 }
				]
			});
			graph.render();
			
			var preview;
			
			try{

			preview = new Rickshaw.Graph.RangeSlider({
				graph: graph,
				element: document.getElementById('preview'),
			});
			}catch(ex){
			}
				
			var hoverDetail = new Rickshaw.Graph.HoverDetail({
				graph: graph,
				formatter: function (series, x, y) {
					//var date = '<span class="date">' + graphData[x]['QTDCHAMADA'] + '</span>';
					var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
					if(series.name === "Chamadas"){
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "CHAMADA") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "CHAMADA") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "CHAMADA") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "CHAMADA") return a; })[x].min;
					}else if(series.name === "Fibra"){
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPAROFIBRA") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPAROFIBRA") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPAROFIBRA") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPAROFIBRA") return a; })[x].min;
					}else if(series.name === "Fixo"){
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPAROFIXO") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPAROFIXO") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPAROFIXO") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPAROFIXO") return a; })[x].min;
					//}else if(series.name === "Movel"){
					//	var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPAROMOVEL") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPAROMOVEL") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPAROMOVEL") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPAROMOVEL") return a; })[x].min;
					}else if(series.name === "Tv"){
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPAROTV") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPAROTV") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPAROTV") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPAROTV") return a; })[x].min;
					}else if(series.name === "Velox"){
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPAROVELOX") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPAROVELOX") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPAROVELOX") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPAROVELOX") return a; })[x].min;
					}else{
						var content = series.name + ": " + parseInt(y) + '<br>' + graphData.filter(function(a){ if(a.tipo === "REPARO") return a; })[x].dia+'/'+graphData.filter(function(a){ if(a.tipo === "REPARO") return a; })[x].mes+' '+graphData.filter(function(a){ if(a.tipo === "REPARO") return a; })[x].hora+':'+graphData.filter(function(a){ if(a.tipo === "REPARO") return a; })[x].min;
					}
					return content;
				}
			});
			

			var legend = new Rickshaw.Graph.Legend({
				graph: graph,
				element: document.getElementById('legend'),
				formatter: function (series, x, y) {
					// var date = '<span class="date">' + graphData[x]['hora'] + '</span>';
					var date = '<span class="date">' +graphData[x].dia+'/'+graphData[x].mes+' '+graphData[x].hora+':'+graphData[x].min + '</span>';
					var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
					var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
					return content;
				}
			});

			var yAxis = new Rickshaw.Graph.Axis.Y({
				graph: graph
			});

			yAxis.render();

			var ticksTreatment = 'glow';
			

			var xAxis = new Rickshaw.Graph.Axis.X({
				graph: graph,
				tickFormat: function (x){ 
					
					
							data = graph.series[0].data[x];						
							return data.hora+":"+data.min;
						
					
					
				}
			});

			xAxis.render();

			var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({
				graph: graph,
				legend: legend
			});

		})
	}

	function limpaHTMLGrafico() {
		grafico = '<div id="chart"></div><div id="timeline"></div>' +
			'<div id="legend_container">' +
			'<div id="smoother" title="Smoothing"></div>' +
			'<div id="legend"></div>' +
			'<div id="timeline"></div>' +
			'</div>' +
			'<div id="slider"></div>' +
			'</div><div id="preview"></div>';
		$("#chart_container").html(grafico)
	}


	$scope.$on('$viewContentLoaded', function () {
		getQtdChamadas()
		getServerStatus()
		atualizaGrafico()
		//getNVPQtdChamadas()
		//getNGRqtdGravacaoDeLogs()
		getNGRDatasUltimosRegsProds()
		getOcupacaoBanco()
		var $view = $("#pag-MonBilhetagem");
		
		setInterval(function(){ $('.filtro-uf').css('margin-right','6px')
		$('.filtro-ddd').css('margin-right','6px'); }, 1000);
		$view.on("click", ".btn-gerar", function () {
			if ($('.btn-gerar').html() == "Carregar"){
				$('.grid2').css('display','block');
				$('.btn-gerar').html("Ocultar");
			}else{
				$('.grid2').css('display','none');
				$('.btn-gerar').html("Carregar");
			}
						
            
        });
		$scope.ddds = cache.ddds;
		$scope.ufs = cache.ufs;
		carregaUFs($view);
		carregaDDDsPorUF($scope,$view,true);
		$view.on("change", "select.filtro-uf", function(){ carregaDDDsPorUF($scope,$view)});
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-uf").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} UFs',
			showSubtext: true
		});
		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

		// Marca todos os ufs
		$view.on("click", "#alinkUf", function(){ marcaTodosIndependente($('.filtro-uf'),'ufs')});
		
		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
		  if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
			$('div.btn-group.filtro-ddd').addClass('open');
			$('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px', 'margin-left':'-200px' });
		  }
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
			$('div.btn-group.filtro-ddd').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-uf').mouseover(function () {
			$('div.btn-group.filtro-uf').addClass('open');
			$('div.btn-group.filtro-uf>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-uf').mouseout(function () {
			$('div.btn-group.filtro-uf').removeClass('open');
		});
		
		
	});
}



CtrlMonitorBilhetagem.$inject = ['$scope', '$globals'];