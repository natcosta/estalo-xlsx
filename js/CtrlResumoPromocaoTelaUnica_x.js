function CtrlResumoPromocaoTelaUnica ($scope, $globals) {

    win.title="Resumo por Promoções"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-promocao-telaunica", "Resumo por Promoções");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
	$scope.vendas = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;
	$scope.total_geral;
	$scope.total_der;
	$scope.total_transf;
	$scope.total_naoatend;
    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        porDia: false
    };

  
	
    var empresas = cache.empresas.map(function(a){return a.codigo});

    $scope.aba = 2;



    $scope.valor = "datReferencia";



    function geraColunas(){
      var array = [];


     array.push(
			{ field: "cod_ic", displayName: "Código", width: 80, pinned: true, visible: false },
			{ field: "promo", displayName: "Promoção", width: 200, pinned: true }
        
		);

      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


	  $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-resumo-promocao-telaunica");
     

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/


       $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});


        $(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
        $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
        $('.navbar-inner').css('height','70px');
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

        //minuteStep: 5

      //19/03/2014
		componenteDataHora ($scope,$view);
        carregaAplicacoes($view,false,false,$scope);
   		carregaEmpresas($view);
		carregaSites($view);
        carregaOperacoesECH($view);


        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


     


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });
		
		$view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });       

        $view.find("select.filtro-operacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Operações',
            showSubtext: true
        });
 

		 $view.find("select.filtro-empresa").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Empresas'
        });
		
		 $view.find("select.filtro-tipopagamento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Pagamento'
        });
		
		 $view.find("select.filtro-tipoplano").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Plano'
        });
     

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });
		
		
        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-operacao", function () {
            var filtro_operacoes = $(this).val() || [];
            Estalo.filtros.filtro_operacoes = filtro_operacoes;
        });

      //2014-11-27 transição de abas
      var abas = [2,3,4];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


      // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});
       

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

      
        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

		 // Marca todos os empresas
        $view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});
        // GILBERTO 18/02/2014

        // Marca todos os operacoes
        $view.on("click", "#alinkOper", function(){ marcaTodosIndependente($('.filtro-operacao'),'operacoes')});
		
		 // Marca todos os planos
        $view.on("click", "#alinkPlano", function(){ marcaTodosIndependente($('.filtro-tipoplano'),'planos')});
		
		 // Marca todos os pagamentos
        $view.on("click", "#alinkPagamento", function(){ marcaTodosIndependente($('.filtro-tipopagamento'),'pagamentos')});

      // EXIBIR AO PASSAR O MOUSE
       


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });
		
		
		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

     
        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
            $('div.btn-group.filtro-empresa').addClass('open');

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
            $('div.btn-group.filtro-empresa').removeClass('open');

        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseover(function () {
            $('div.btn-group.filtro-operacao').addClass('open');
            $('div.btn-group.filtro-operacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseout(function () {
            $('div.btn-group.filtro-operacao').removeClass('open');
        });
		
		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-tipoplano').mouseover(function () {
            $('div.btn-group.filtro-tipoplano').addClass('open');
            $('div.btn-group.filtro-tipoplano>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-tipoplano').mouseout(function () {
            $('div.btn-group.filtro-tipoplano').removeClass('open');
        });
		
			// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-tipopagamento').mouseover(function () {
            $('div.btn-group.filtro-tipopagamento').addClass('open');
            $('div.btn-group.filtro-tipopagamento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-tipopagamento').mouseout(function () {
            $('div.btn-group.filtro-tipopagamento').removeClass('open');
        });
       


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-resumo-promocao-telaunica");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-resumo-promocao-telaunica");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });



        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };








    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
		var filtro_planos = $view.find("select.filtro-tipoplano").val() || [];
		var filtro_pagamentos = $view.find("select.filtro-tipopagamento").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }
        if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
		if (filtro_planos.length > 0) { $scope.filtro_planos += " Planos: " + filtro_planos; }
		if (filtro_pagamentos.length > 0) { $scope.filtro_pagamentos += " Pagamentos: " + filtro_pagamentos; }
		
		$scope.vendas = [];
        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };

    
	
		 var campoData = "DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)"; 
	
		
		  stmt = db.use
		  + " select CodPromocao as Cod_Item," + campoData + " as DatReferencia,"
		  + " sum(qtd) as Qtd"
		  + " from ResumoTelaUnicaPromo"
		  + " WHERE 1 = 1"
		  + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		  + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
		  stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		  stmt += restringe_consulta("CodSite", filtro_sites, true)
		  stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
		  stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
          stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
		  stmt += restringe_consulta("CodPlano", filtro_planos, true)
		  stmt += restringe_consulta("CodTipoPagto", filtro_pagamentos, true)
		  if ($scope.filtros.comSMS) stmt += " AND envioSMS = '1'";
		  stmt += " GROUP BY " + campoData + ",CodPromocao";
		
        executaQuery = executaQuery2; 
        log(stmt);


        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery2(columns) {
	
				
		  
			if ($.isNumeric(columns[0].value) == false) {
				  var cod_ic = columns[0].value,
				  data = columns[1].value === null ? null : "_" + formataData2(columns[1].value),
				  data_BR = columns[1].value === null ? null : formataDataBR(columns[1].value),
				  qtd_entrantes = +columns[2].value;
				  //descricao = columns[3].value

					if (data != null) {
						var d = $scope.datas[data];
						if (d === undefined) {
							d = { data: data, data_BR: data_BR };
							$scope.datas.push(d);
							$scope.datas[data] = d;
							$scope.totais[data] = { qtd_entrantes: 0 };
						}
					}

					var dado = $scope.dados[cod_ic];
					if (dado === undefined) {
						dado = {
							promo: cod_ic,
							totais: { geral: { qtd_entrantes: 0 } }
						};
						$scope.dados.push(dado);
						$scope.dados[cod_ic] = dado;
					}

					if (data != null) {
						dado.totais[data] = { qtd_entrantes: qtd_entrantes };
						dado.totais.geral.qtd_entrantes += qtd_entrantes;
						$scope.totais[data].qtd_entrantes += qtd_entrantes;
						$scope.totais.geral.qtd_entrantes += qtd_entrantes;
					}

					
			}	


			  //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
        }

				$scope.total_geral = 0;
				$scope.total_transf = 0;
				$scope.total_der = 0;
				$scope.total_naoatend = 0;
				
              db.query(stmt, executaQuery, function (err, num_rows) {
                  console.log("Executando query-> " + stmt + " " + num_rows);
                  userLog(stmt, 'Carrega dados', 2, err)
					retornaStatusQuery(num_rows, $scope);
					$btn_gerar.button('reset');
					if (num_rows > 0) {
						$btn_exportar.prop("disabled", false);
					}
					
					if($scope.detalhe){
					/* 	$scope.dados.push({
								data_hora: "",
								datReferencia: "",
								transferida: $scope.total_transf,
								derivada: $scope.total_der,
								naoencontradas: $scope.total_naoatend,
								total: $scope.total_geral,
								pontoDerivacao: "",
								grupo1: "",
								grupo2: "TOTAL",
								regra: "",
								percTransf: (100 * $scope.total_transf / $scope.total_geral).toFixed(2)
						}); */
					}else
					{
						// Preencher com zero datas inexistentes em cada IC
						$scope.dados.forEach(function (dado) {
							$scope.datas.forEach(function (d) {
								if (dado.totais[d.data] === undefined) {
									dado.totais[d.data] = { qtd_entrantes: 0 };
								}
							})
						});

						$scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
						$scope.datas.forEach(function (d) {
							$scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: 100 });
						});

						$scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: 100 });
						$scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });
					}
					

					$scope.$apply();
				});
         
				// GILBERTOOOOOO 17/03/2014
				$view.on("mouseup", "tr.resumo", function () {
					var that = $(this);
					$('tr.resumo.marcado').toggleClass('marcado');
					$scope.$apply(function () {
						that.toggleClass('marcado');
					});
				});
    };




	   
	  // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
		
		if (!$scope.detalhe){
			// Criar cabeçalho
			var cabecalho = $scope.datas.map(function (d) {
				return { value: d.data_BR, bold: 1, hAlign: 'center', autoWidth: true };
			});

			// Inserir primeira coluna: item de controle
			cabecalho.unshift({ value: "Promo", bold: 1, hAlign: 'center', autoWidth: true });

			// Inserir última coluna: total por linha
			cabecalho.push({ value: "Total", bold: 1, hAlign: 'center', autoWidth: true });

			var linhas = $scope.dados.map(function (dado) {
				var linha = $scope.datas.map(function (d) {
					try {
						return { value: dado.totais[d.data].qtd_entrantes || 0 };
					} catch (ex) {
						return 0;
					}
				});

				// Inserir primeira coluna: item de controle
				linha.unshift({ value: dado.promo });

				// Inserir última coluna: total por linha
				linha.push({ value: dado.totais.geral.qtd_entrantes });

				return linha;
			});

			// Criar última linha, com os totais por data
			var linha_totais = $scope.datas.map(function (d) {
				try {
					return { value: $scope.totais[d.data].qtd_entrantes || 0 };
				} catch (ex) {
					return 0;
				}
			});

			// Inserir primeira coluna
			linha_totais.unshift({ value: "Total", bold: 1 });

			// Inserir última coluna: total geral
			linha_totais.push({ value: $scope.totais.geral.qtd_entrantes });

			// Inserir primeira linha: cabeçalho
			linhas.unshift(cabecalho);

			// Inserir última linha: totais por data
			//linhas.push(linha_totais);
		}
        else{
			var linhas = [];
			 linhas.push([{value:'Dia', autoWidth:true},{value:'Qtd Total', autoWidth:true}, {value:'Qtd OK', autoWidth:true},{value:'Qtd Erro', autoWidth:true},{value:'% OK', autoWidth:true}]);
			  $scope.dados.map(function (dado) {
				return linhas.push([{value:dado.dia, autoWidth:true}, {value:dado.total, autoWidth:true},{value:dado.ok, autoWidth:true},{value:dado.erro, autoWidth:true}, {value:dado.perc_ok, autoWidth:true}]);
			  });
		}

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'Resumo por Promoções', data: linhas, table: true }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 1 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoPromocoes_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}

CtrlResumoPromocaoTelaUnica .$inject = ['$scope', '$globals'];
