/*
 * CtrlDerivacoesRIC
 */
function CtrlDerivacoesRIC($scope, $globals) {
	win.title = "Resumo de repetidas por IC"; //Alex 27/02/2014
	$scope.versao = versao;


	//Alex 08/02/2014
	travaBotaoFiltro(0, $scope, "#pag-derivacoes-repetidas", "Resumo de repetidas por IC");

	$scope.dados = [];
	$scope.xlsx = [];
	$scope.colunas = [];

	var columnDefs = [
		{
			headerName: '',		
			children: [
				{ field: "data_hora_BR", headerName: "Data", width: 170, pinned:true }, 
				{ field: "aplicacao", headerName: "Aplicação", width: 130, filter: 'agTextColumnFilter', pinned:true },
				{ field: "codIC", headerName: "IC", width: 90, filter: 'agTextColumnFilter', pinned:true }
			]
		
		},
		{
			headerName: 'Clientes',		
			children: [		
				{ field: "qtdClientes", headerName: "Total", width: 100 }
			]
		
		},
		{
			headerName: 'Chamadas',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdChamadas', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdChamadasFinal', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdChamadasFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdChamadasDeriv', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'percQtdChamadasDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdChamadasAband', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdChamadasAband', width: 100, filter: 'agNumberColumnFilter'}
			]
		},
		{
			headerName: 'Repetidas',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdRepetidas', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdRepetidasFinal', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdRepetidasFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdRepetidasDeriv', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'percQtdRepetidasDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdRepetidasAband', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdRepetidasAband', width: 100, filter: 'agNumberColumnFilter'}
			]
		},	
		
		{
			headerName: 'Repetidas após finalização',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdDerivadas', width: 210, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdDerivadasFinal', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdDerivadasFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdDerivadasDeriv', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'percQtdDerivadasDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdDerivadasAband', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdDerivadasAband', width: 100, filter: 'agNumberColumnFilter'}
			]
		},		
		{
			headerName: 'Repetidas após derivação',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdRepAposDeriv', width: 210, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdRepAposDerivFinal', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdRepAposDerivFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdRepAposDerivDeriv', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'qtdRepAposDerivDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdRepAposDerivAband', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdRepAposDerivAband', width: 100, filter: 'agNumberColumnFilter'}
			]
		},
		{
			headerName: 'Repetidas após abandono',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdRederivadas', width: 210, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdRederivadasFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdRederivadasFinal', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdRederivadasDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'qtdRederivadasDeriv', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdRederivadasAband', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdRederivadasAband', width: 100, filter: 'agNumberColumnFilter'}
			]
		},{
			headerName: 'Repetidas pelo mesmo ic',
			children: [
				{headerName: 'Total', columnGroupShow: 'closed', field: 'qtdRepetidasIC', width: 210, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdRepetidasFinalIC', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Final.', columnGroupShow: 'open', field: 'percQtdRepetidasFinalIC', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdRepetidasDerivIC', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Deriv.', columnGroupShow: 'open', field: 'percQtdRepetidasDerivIC', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdRepetidasAbandIC', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: '% Aband.', columnGroupShow: 'open', field: 'percQtdRepetidasAbandIC', width: 100, filter: 'agNumberColumnFilter'}
			]
		}

	];

	columnDefs.forEach(function (col) {
		col.children.forEach(function (child) {
			$scope.colunas.push({
				displayName: col.headerName + " " + child.headerName,
			});
		})
	});

	$scope.gridOptions = {
		defaultColDef: {
			sortable: true,
			resizable: true,
			filter: true
		},
		debug: true,
		columnDefs: [],
		rowData: []
	};

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		sites: [],
		segmentos: [],
		chamador: ""
	};

	$scope.tabela = "";
	$scope.valor = "24h";
	$scope.datahora = "data"	
	var sufixo = "";
	var sufixoTabAnt = "";

	// Filtros: data e hora
	var agora = new Date();

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: mesAnterior(dat_consoli),
		max: dat_consoli*/
	};

	var $view;

	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-derivacoes-repetidas-ic");

		componenteDataMaisHora($scope, $view);
		carregaAplicacoes($view, false, false, $scope);		

		// Lista chamadas conforme filtros
		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("enable", true);
        $btn_exportar_xlsx.prop("disabled", true);
		$btn_exportar_dropdown.prop("enable", true);
		
		$view.on("click", ".btn-gerar", function () {
			limpaProgressBar($scope, "#pag-derivacoes-repetidas-ic");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}			
			$scope.listaDados.apply(this);
		});

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);


			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$view.on('change', '.filtro-segmento', function () {
			filtro_segmentos = $view.find("select.filtro-segmento").val()
		})

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-derivacoes-repetidas");
		}

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {


		$scope.tabela = "";
		sufixo = "";
		sufixoTabAnt = $scope.tabela + sufixo;
		sufixo = $scope.tabela + sufixo + $scope.valor;


		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("enable", true);
        $btn_exportar_xlsx.prop("disabled", true);
        $btn_exportar_dropdown.prop("enable", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		ic = $scope.filtroEscolhido.ic ||  [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
			" até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_aplicacoes.length > 0) {
			$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		}
		if (ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
            $scope.filtros_usados += " Items de controle: " + ic;
        }
	
		$scope.dados = [];
		$scope.xlsx = [];
		
		var tabs = ["1h", "2h", "24h"];
		//var tabAnt = tabs[tabs.indexOf($scope.valor)-1] !== undefined ? tabs[tabs.indexOf($scope.valor)-1] : undefined;
		var tabAnt = undefined;
		
		var stmt = db.use;
			stmt += "with cte as (Select ri.DatReferencia,RI.CodAplicacao, ri.codic,SUM(RI.QtdChamadas) as cha, SUM(RI.QtdFinalizadas) as chaf, SUM(RI.QtdDerivadas) as chad, SUM(RI.QtdAbandonadas) as chaa from ResumoICDia as RI "
			stmt += "where 1 = 1 AND RI.DatReferencia >= '" + formataDataHora(data_ini) + "' AND RI.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "' "
			stmt += restringe_consulta("RI.CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("RI.CodIC", ic, true)
			stmt += "GROUP BY RI.CodAplicacao,RI.CodIC,ri.DatReferencia),"
			stmt += "cti as( Select TR.Codaplicacao, TR.[CodIC2] as CodIC, TR.[DatReferencia], "
			stmt += "SUM(TR.QtdRepetidas"+(!tabAnt ? "":" - TRA.QtdRepetidas")+") as repic, "
			stmt += "SUM(TR.QtdRepFinal"+(!tabAnt ? "":" - TRA.QtdRepFinal")+") as repfic, " 
			stmt += "SUM(TR.QtdRepDeriv"+(!tabAnt ? "":" - TRA.QtdRepDeriv")+") as repdic, "
			stmt += "SUM(TR.QtdRepAband"+(!tabAnt ? "":" - TRA.QtdRepAband")+") as repaic "
			stmt += "from " + db.prefixo + "TotaisRepetidasICxIC" + sufixo+ " as TR "
			if(tabAnt){
				stmt += " INNER JOIN TotaisRepetidasICxIC"+sufixoTabAnt + tabAnt+" as TRA" +
				" ON TR.DatReferencia = TRA.DatReferencia" +
				" AND TR.CodIC2 = TRA.CodIC2" +
				" AND TR.CodIC1 = TRA.CodIC1" +
				" AND TR.CodAplicacao = TRA.CodAplicacao ";			
			}
			stmt += "where 1 = 1"
			stmt += restringe_consulta("TR.CodAplicacao", filtro_aplicacoes, true)
			stmt += restringe_consulta("TR.CodIC2", ic, true)	
			stmt += " GROUP BY TR.CodAplicacao,TR.CodIC2,TR.DatReferencia)"	
			stmt += "Select TR.DatReferencia as datr, TR.CodAplicacao as capli, TR.CodIC as cic,cha,chaf,chad,chaa,repic,repfic,repdic,repaic, "
			stmt += "TR.QtdClientes as qc,"
			stmt += "SUM(TR.QtdRepetidas"+(!tabAnt ? "":" - TRA.QtdRepetidas")+") as rep,"
			stmt += "SUM(TR.QtdRepFinal"+(!tabAnt ? "":" - TRA.QtdRepFinal")+") as repf," 
			stmt += "SUM(TR.QtdRepDeriv"+(!tabAnt ? "":" - TRA.QtdRepDeriv")+") as repd,"
			stmt += "SUM(TR.QtdRepAband"+(!tabAnt ? "":" - TRA.QtdRepAband")+") as repa,"
			stmt += "SUM(TR.QtdRepAposFinal"+(!tabAnt ? "":" - TRA.QtdRepAposFinal")+") as reaf,"
			stmt += "SUM(TR.QtdFinalAposFinal"+(!tabAnt ? "":" - TRA.QtdFinalAposFinal")+") as reaff," 
			stmt += "SUM(TR.QtdDerivAposFinal"+(!tabAnt ? "":" - TRA.QtdDerivAposFinal")+") as reafd," 
			stmt += "SUM(TR.QtdAbandAposFinal"+(!tabAnt ? "":" - TRA.QtdAbandAposFinal")+") as reafa,"
			stmt += "SUM(TR.QtdRepAposDeriv"+(!tabAnt ? "":" - TRA.QtdRepAposDeriv")+") as rea," 
			stmt += "SUM(TR.QtdFinalAposDeriv"+(!tabAnt ? "":" - TRA.QtdFinalAposDeriv")+") as readf,"
			stmt += "SUM(TR.QtdDerivAposDeriv"+(!tabAnt ? "":" - TRA.QtdDerivAposDeriv")+") as readd,"
			stmt += "SUM(TR.QtdAbandAposDeriv"+(!tabAnt ? "":" - TRA.QtdAbandAposDeriv")+") as reada,"
			stmt += "SUM(TR.QtdRepAposAband"+(!tabAnt ? "":" - TRA.QtdRepAposAband")+") as reab,"
			stmt += "SUM(TR.QtdFinalAposAband"+(!tabAnt ? "":" - TRA.QtdFinalAposAband")+") as reabf,"
			stmt += "SUM(TR.QtdDerivAposAband"+(!tabAnt ? "":" - TRA.QtdDerivAposAband")+") as reabd,"
			stmt += "SUM(TR.QtdAbandAposAband"+(!tabAnt ? "":" - TRA.QtdAbandAposAband")+") as reaba "	+			
				" FROM " + db.prefixo + "TotaisRepetidasIC" + sufixo + " as TR";
			if(tabAnt){
				stmt += " INNER JOIN TotaisRepetidasIC"+sufixoTabAnt + tabAnt+" as TRA" +
				" ON TR.DatReferencia = TRA.DatReferencia" +
				" AND TR.CodIC = TRA.CodIC" +
				" AND TR.CodAplicacao = TRA.CodAplicacao";			
			}				
			stmt += " inner join cte as RI" + 
			" on TR.DatReferencia = RI.DatReferencia" +
			" and TR.Codaplicacao = RI.CodAplicacao" +
			" and TR.CodIC = RI.CodIC" +
			" inner join cti as RIC" + 
			" on TR.DatReferencia = RIC.DatReferencia" +
			" and TR.Codaplicacao = RIC.CodAplicacao" +
			" and TR.CodIC = RIC.CodIC" +
			" WHERE 1 = 1" +
			" AND TR.DatReferencia >= '" + formataDataHora(data_ini) + "'" +
			" AND TR.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";
			stmt += restringe_consulta("TR.CodAplicacao", filtro_aplicacoes, true);
			stmt += restringe_consulta("TR.CodIC", ic, true);
			if(tabAnt){
				stmt += restringe_consulta("TRA.CodAplicacao", filtro_aplicacoes, true);
				stmt += restringe_consulta("TR.CodIC", ic, true);
			}		
			stmt += " GROUP BY TR.DatReferencia,TR.CodAplicacao,TR.CodIC,cha,chaf,chad,chaa,repic,repfic,repdic,repaic"
			stmt += ",TR.QtdClientes";
			stmt += " order by TR.DatReferencia";

		log(stmt);
		
		$scope.gridOptions.api.setColumnDefs([]);
		$scope.gridOptions.api.setRowData([]);


		function executaQuery(columns) {
			//db.query(stmt, function (columns) {



			var data_hora = columns[0].value,
				data_hora_BR = typeof data_hora === 'string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
				aplicacao = obtemNomeAplicacao(columns[1].value),
				codIC=  columns[2].value,

				qtdClientes = columns["qc"] === undefined ? "NA" : columns["qc"].value === null ? "ND" : +columns["qc"].value,




				qtdChamadas = +columns["cha"].value,
				qtdChamadasFinal = +columns["chaf"].value,
				percQtdChamadasFinal = qtdChamadasFinal === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasFinal / qtdChamadas).toFixed(2)),
				qtdChamadasDeriv = +columns["chad"].value,
				percQtdChamadasDeriv = qtdChamadasDeriv === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasDeriv / qtdChamadas).toFixed(2)),
				qtdChamadasAband = +columns["chaa"].value,
				percQtdChamadasAband = qtdChamadasAband === 0 || qtdChamadas === 0 ? 0 : parseFloat((100 * qtdChamadasAband / qtdChamadas).toFixed(2)),


				qtdRepetidas = +columns["rep"].value,
				qtdRepetidasFinal = +columns["repf"].value,
				percQtdRepetidasFinal = qtdRepetidasFinal === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasFinal / qtdRepetidas).toFixed(2)),
				qtdRepetidasDeriv = +columns["repd"].value,
				percQtdRepetidasDeriv = qtdRepetidasFinal === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasDeriv / qtdRepetidas).toFixed(2)),
				qtdRepetidasAband = +columns["repa"].value,
				percQtdRepetidasAband = qtdRepetidasAband === 0 || qtdRepetidas === 0 ? 0 : parseFloat((100 * qtdRepetidasAband / qtdRepetidas).toFixed(2)),

				qtdDerivadas = +columns["reaf"].value,
				qtdDerivadasFinal = +columns["reaff"].value,
				percQtdDerivadasFinal = qtdDerivadasFinal === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasFinal / qtdDerivadas).toFixed(2)),
				qtdDerivadasDeriv = +columns["reafd"].value,
				percQtdDerivadasDeriv = qtdDerivadasDeriv === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasDeriv / qtdDerivadas).toFixed(2)),
				qtdDerivadasAband = +columns["reafa"].value,
				percQtdDerivadasAband = qtdDerivadasAband === 0 || qtdDerivadas === 0 ? 0 : parseFloat((100 * qtdDerivadasAband / qtdDerivadas).toFixed(2)),

				qtdRepAposDeriv = +columns["rea"].value,
				qtdRepAposDerivFinal = +columns["readf"].value,
				percQtdRepAposDerivFinal = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivFinal / qtdRepAposDeriv).toFixed(2)),
				qtdRepAposDerivDeriv = +columns["readd"].value,
				percQtdRepAposDerivDeriv = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivDeriv / qtdRepAposDeriv).toFixed(2)),
				qtdRepAposDerivAband = +columns["reada"].value,
				percQtdRepAposDerivAband = qtdRepAposDerivFinal === 0 || qtdRepAposDeriv === 0 ? 0 : parseFloat((100 * qtdRepAposDerivAband / qtdRepAposDeriv).toFixed(2)),

				qtdRederivadas = +columns["reab"].value,
				qtdRederivadasFinal = +columns["reabf"].value,
				percQtdRederivadasFinal = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasFinal / qtdRederivadas).toFixed(2)),
				qtdRederivadasDeriv = +columns["reabd"].value,
				percQtdRederivadasDeriv = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasDeriv / qtdRederivadas).toFixed(2)),
				qtdRederivadasAband = +columns["reaba"].value,
				percQtdRederivadasAband = qtdRederivadasFinal === 0 || qtdRederivadas === 0 ? 0 : parseFloat((100 * qtdRederivadasAband / qtdRederivadas).toFixed(2)),

				qtdRepetidasIC = +columns["repic"].value,
				qtdRepetidasFinalIC = +columns["repfic"].value,
				percQtdRepetidasFinalIC = qtdRepetidasFinalIC === 0 || qtdRepetidasIC === 0 ? 0 : parseFloat((100 * qtdRepetidasFinalIC / qtdRepetidasIC).toFixed(2)),
				qtdRepetidasDerivIC = +columns["repdic"].value,
				percQtdRepetidasDerivIC = qtdRepetidasFinal === 0 || qtdRepetidasIC === 0 ? 0 : parseFloat((100 * qtdRepetidasDerivIC / qtdRepetidasIC).toFixed(2)),
				qtdRepetidasAbandIC = +columns["repaic"].value,
				percQtdRepetidasAbandIC = qtdRepetidasAbandIC === 0 || qtdRepetidasIC === 0 ? 0 : parseFloat((100 * qtdRepetidasAbandIC / qtdRepetidasIC).toFixed(2));



			$scope.dados.push({
				data_hora: data_hora,
				data_hora_BR: $scope.datahora === "hora"?data_hora_BR:data_hora_BR.substring(0, 10),
				aplicacao: aplicacao,
				codIC:codIC,
				qtdClientes: qtdClientes,

				qtdChamadas: qtdChamadas,
				qtdChamadasFinal: qtdChamadasFinal,
				percQtdChamadasFinal: percQtdChamadasFinal,
				qtdChamadasDeriv: qtdChamadasDeriv,
				percQtdChamadasDeriv: percQtdChamadasDeriv,
				qtdChamadasAband: qtdChamadasAband,
				percQtdChamadasAband: percQtdChamadasAband,

				qtdRepetidas: qtdRepetidas,
				qtdRepetidasFinal: qtdRepetidasFinal,
				percQtdRepetidasFinal: percQtdRepetidasFinal,
				qtdRepetidasDeriv: qtdRepetidasDeriv,
				percQtdRepetidasDeriv: percQtdRepetidasDeriv,
				qtdRepetidasAband: qtdRepetidasAband,
				percQtdRepetidasAband: percQtdRepetidasAband,

				qtdDerivadas: qtdDerivadas,
				qtdDerivadasFinal: qtdDerivadasFinal,
				percQtdDerivadasFinal: percQtdDerivadasFinal,
				qtdDerivadasDeriv: qtdDerivadasDeriv,
				percQtdDerivadasDeriv: percQtdDerivadasDeriv,
				qtdDerivadasAband: qtdDerivadasAband,
				percQtdDerivadasAband: percQtdDerivadasAband,

				qtdRepAposDeriv: qtdRepAposDeriv,
				qtdRepAposDerivFinal: qtdRepAposDerivFinal,
				percQtdRepAposDerivFinal: percQtdRepAposDerivFinal,
				qtdRepAposDerivDeriv: qtdRepAposDerivDeriv,
				percQtdRepAposDerivDeriv: percQtdRepAposDerivDeriv,
				qtdRepAposDerivAband: qtdRepAposDerivAband,
				percQtdRepAposDerivAband: percQtdRepAposDerivAband,

				qtdRederivadas: qtdRederivadas,
				qtdRederivadasFinal: qtdRederivadasFinal,
				percQtdRederivadasFinal: percQtdRederivadasFinal,
				qtdRederivadasDeriv: qtdRederivadasDeriv,
				percQtdRederivadasDeriv: percQtdRederivadasDeriv,
				qtdRederivadasAband: qtdRederivadasAband,
				percQtdRederivadasAband: percQtdRederivadasAband,

				qtdRepetidasIC: qtdRepetidasIC,
				qtdRepetidasFinalIC: qtdRepetidasFinalIC,
				percQtdRepetidasFinalIC: percQtdRepetidasFinalIC,
				qtdRepetidasDerivIC: qtdRepetidasDerivIC,
				percQtdRepetidasDerivIC: percQtdRepetidasDerivIC,
				qtdRepetidasAbandIC: qtdRepetidasAbandIC,
				percQtdRepetidasAbandIC: percQtdRepetidasAbandIC,

				temTotaisClientes: typeof qtdClientes === "number"

			});

			$scope.xlsx.push([
				data_hora_BR.substring(0, 10),
				aplicacao,
				codIC,
				qtdClientes,

				qtdChamadas,
				qtdChamadasFinal,
				percQtdChamadasFinal,
				qtdChamadasDeriv,
				percQtdChamadasDeriv,
				qtdChamadasAband,
				percQtdChamadasAband,

				qtdRepetidas,
				qtdRepetidasFinal,
				percQtdRepetidasFinal,
				qtdRepetidasDeriv,
				percQtdRepetidasDeriv,
				qtdRepetidasAband,
				percQtdRepetidasAband,

				qtdDerivadas,
				qtdDerivadasFinal,
				percQtdDerivadasFinal,
				qtdDerivadasDeriv,
				percQtdDerivadasDeriv,
				qtdDerivadasAband,
				percQtdDerivadasAband,

				qtdRepAposDeriv,
				qtdRepAposDerivFinal,
				percQtdRepAposDerivFinal,
				qtdRepAposDerivDeriv,
				percQtdRepAposDerivDeriv,
				qtdRepAposDerivAband,
				percQtdRepAposDerivAband,

				qtdRederivadas,
				qtdRederivadasFinal,
				percQtdRederivadasFinal,
				qtdRederivadasDeriv,
				percQtdRederivadasDeriv,
				qtdRederivadasAband,
				percQtdRederivadasAband,

				qtdRepetidasIC,
				qtdRepetidasFinalIC,
				percQtdRepetidasFinalIC,
				qtdRepetidasDerivIC,
				percQtdRepetidasDerivIC,
				qtdRepetidasAbandIC,
				percQtdRepetidasAbandIC,

			]);
			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}


		db.query(stmt, executaQuery, function (err, num_rows) {
			console.log("Executando query-> " + stmt + " " + num_rows);
			retornaStatusQuery(num_rows, $scope);
			$btn_gerar.button('reset');

			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$scope.gridOptions.columnDefs = columnDefs;				
				$scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
				$scope.gridOptions.api.setRowData($scope.dados);				
			}
		});
	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		var $btn_exportar = $(this);
		$btn_exportar.button('loading');
		//Alex 15-02-2014 - 26/03/2014 TEMPLATE
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='tDerivacoesRIC'";
		log(stmt);
		db.query(stmt, function (columns) {
			var dataAtualizacao = columns[0].value,
				nomeRelatorio = columns[1].value,
				arquivo = columns[2].value;


			var milis = new Date();
			var baseFile = 'tDerivacoesRIC.xlsx';



			var buffer = toBuffer(toArrayBuffer(arquivo));

			fs.writeFileSync(baseFile, buffer, 'binary');

			var file = 'derivacoesRIC_' + formataDataHoraMilis(milis) + '.xlsx';

			var newData;


			fs.readFile(baseFile, function (err, data) {
				// Create a template
				var t = new XlsxTemplate(data);

				// Perform substitution
				t.substitute(1, {
					filtros: $scope.filtros_usados,
					planDados: trataPorcentagem($scope.dados, true)
				});

				// Get binary data
				newData = t.generate();


				if (!fs.existsSync(file)) {
					fs.writeFileSync(file, newData, 'binary');
					childProcess.exec(file);
					trataPorcentagem($scope.dados, false);
				}
			});

			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);



		}, function (err, num_rows) {
			//userLog(stmt, 'Exportar XLSX', 2, err)
			//?
		});

	};
}
CtrlDerivacoesRIC.$inject = ['$scope', '$globals'];