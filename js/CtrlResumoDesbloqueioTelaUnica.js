function CtrlResumoDesbloqueioTelaUnica($scope, $globals) {

    win.title = "Resumo Desbloqueio Tela Única"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-desbloqueio-telaunica", "Resumo desbloqueio Tela Única");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.csv = [];
    $scope.vendas = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;
    $scope.total_geral;
    $scope.total_der;
    $scope.total_transf;
    $scope.total_naoatend;
    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        porDia: false
    };



    //var empresas = cache.empresas.map(function (a) { return a.codigo });

    $scope.aba = 2;



    $scope.valor = "datReferencia";



    function geraColunas() {
        var array = [];
        array.push({
                field: "data",
                displayName: "Data",
                width: "20%",
                pinned: true
            },
            //{ field: "totalChamadas", displayName: "Total de Chamadas", width: "15%", pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
            {
                field: "atendidas",
                displayName: "Total de Atendidas",
                width: "10%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "bloqueados",
                displayName: "Total de Bloqueados",
                width: "10%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "desbloqueados",
                displayName: "Total de Desbloqueados",
                width: "10%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "elegiveis",
                displayName: "Total de Elegíveis",
                width: "10%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "naoElegiveis",
                displayName: "Total de Não Elegíveis",
                width: "10%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "elegiveisDesbloqueados",
                displayName: "Total de Elegíveis Desbloqueados",
                width: "15%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }, {
                field: "elegiveisBloqueados",
                displayName: "Total de Elegíveis Bloqueados",
                width: "15%",
                pinned: true,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
            }
        );
        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: ontem(dat_consoli),
        max: dat_consoli*/
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-desbloqueio-telaunica");

        $(".aba2").css({
            'position': 'fixed',
            'left': '47px',
            'top': '42px'
        });


        $(".aba4").css({
            'position': 'fixed',
            'left': '750px',
            'top': '40px'
        });
        $(".aba5").css({
            'position': 'fixed',
            'left': '55px',
            'right': 'auto',
            'margin-top': '35px',
            'z-index': '1'
        });
        $('.navbar-inner').css('height', '70px');
        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });

        //minuteStep: 5

        //19/03/2014
        componenteDataHora($scope, $view);
        carregaAplicacoes($view, false, false, $scope);



        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });



        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.porRegEDDD = false;
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-desbloqueio-telaunica");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

            $scope.pivot = false;
            limpaProgressBar($scope, "#pag-resumo-desbloqueio-telaunica");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });



        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };


    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }

        //$scope.vendas = [];
        $scope.dados = [];
        $scope.csv = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = {
            geral: {
                qtd_entrantes: 0
            }
        };


        if ($scope.filtros.porDia) {
            var campoData = "DATEADD(day, DATEDIFF(day, 0, DatReferencia), 0)";
        } else {
            var campoData = "DatReferencia";
        }
        stmt = db.use +
            " select " + campoData + " as DatReferencia,"
            //+ "ISNULL(QtdAcessos,0) as QtdAcessos,"
            +
            "ISNULL(QtdChamadas,0) as QtdChamadas," +
            "ISNULL(QtdBloqueados,0) as QtdBloqueados," +
            "ISNULL(QtdDesbloqueados,0) as QtdDesbloqueados," +
            "ISNULL(QtdElegiveis,0) as QtdElegiveis," +
            "ISNULL(QtdNaoElegiveis,0) as QtdNaoElegiveis," +
            "ISNULL(QtdElegiveisDesbloqueados,0) as QtdElegiveisDesbloqueados," +
            "ISNULL(QtdElegiveisNaoDesbloqueados,0) as QtdElegiveisNaoDesbloqueados" +
            " from ResumoTelaUnicaDesbloqueio" +
            " WHERE 1 = 1" +
            " AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
            " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += " ORDER BY " + campoData + "";


        executaQuery = executaQuery2;
        log(stmt);


        function formataData2(data) {
            var y = data.getFullYear(),
                m = data.getMonth() + 1,
                d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery2(columns) {



            var data_hora = columns["DatReferencia"].value,
                datReferencia = typeof columns[0].value === 'string' ? formataDataHoraBR(data_hora) : formataDataHoraBR(data_hora);
            atendidas = columns[1].value;
            bloqueados = columns[2].value;
            desbloqueados = columns[3].value;
            elegiveis = columns[4].value;
            naoElegiveis = columns[5].value;
            elegiveisDesbloqueados = columns[6].value;
            elegiveisBloqueados = columns[7].value;
            //totalChamadas = columns[8].value;


            if ($scope.filtros.porDia) {
                datReferencia = datReferencia.replace("00:00:00", "")
            };

            var s = {
                data: datReferencia,
                atendidas: atendidas,
                bloqueados: bloqueados,
                desbloqueados: desbloqueados,
                elegiveis: elegiveis,
                naoElegiveis: naoElegiveis,
                elegiveisDesbloqueados: elegiveisDesbloqueados,
                elegiveisBloqueados: elegiveisBloqueados
                //totalChamadas: totalChamadas
            }

            $scope.dados.push(s);

            var a = [];
            for (var item in $scope.colunas) {
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);

            // $scope.csv.push([
            //     datReferencia,
            //     atendidas,
            //     bloqueados,
            //     desbloqueados,
            //     elegiveis,
            //     naoElegiveis,
            //     elegiveisDesbloqueados,
            //     elegiveisBloqueados
            // ]);

            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }
        }

        $scope.total_geral = 0;
        $scope.total_transf = 0;
        $scope.total_der = 0;
        $scope.total_naoatend = 0;

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err);


            if ($scope.porRegEDDD === true) {
                $('#pag-resumo-desbloqueio-telaunica table').css({
                    'margin-left': 'auto',
                    'margin-right': 'auto',
                    'margin-top': '100px',
                    'max-width': '1170px',
                    'margin-bottom': '100px'
                });
            } else {
                $('#pag-resumo-desbloqueio-telaunica table').css({
                    'margin-left': 'auto',
                    'margin-right': 'auto',
                    'margin-top': '100px',
                    'max-width': '1280px',
                    'margin-bottom': '100px'
                });
            }
            console.log("Executando query-> " + stmt + " " + num_rows);


            var datFim = "";


            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }

            /* 	$scope.dados.push({
                    data_hora: data_hora,
                    datReferencia: datReferencia,
                    acessos: acessos,
                    falhas: falhas,
                    percacessos: percacessos,
                    percfalhas: percfalhas,
                    tma: tma
                }); */

            //if(executaQuery !== executaQuery2){
            $('.btn.btn-pivot').prop("disabled", "false");
            if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 <= limiteHoras / 24 && num_rows > 0) {
                $('.btn.btn-pivot').button('reset');
            }

            $scope.datas.sort(function (a, b) {
                return a.data < b.data ? -1 : 1;
            });



        });

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };





    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);

        $btn_exportar.button('loading');

        var linhas = [];


        linhas.push([{
                value: 'Data',
                autoWidth: true
            },
            {
                value: 'Total de Atendidas',
                autoWidth: true
            },
            {
                value: 'Total de Bloqueados',
                autoWidth: true
            },
            {
                value: 'Total de Desbloqueados',
                autoWidth: true
            },
            {
                value: 'Total de Elegíveis',
                autoWidth: true
            },
            {
                value: 'Total de Não Elegíveis',
                autoWidth: true
            },
            {
                value: 'Total de Elegíveis Desbloqueados',
                autoWidth: true
            },
            {
                value: 'Total de Elegíveis Bloqueados',
                autoWidth: true
            }
        ]);


        $scope.dados.map(function (dado) {
            return linhas.push([{
                    value: dado.data,
                    autoWidth: true
                },
                {
                    value: dado.atendidas,
                    autoWidth: true
                },
                {
                    value: dado.bloqueados,
                    autoWidth: true
                },
                {
                    value: dado.desbloqueados,
                    autoWidth: true
                },
                {
                    value: dado.elegiveis,
                    autoWidth: true
                },
                {
                    value: dado.naoElegiveis,
                    autoWidth: true
                },
                {
                    value: dado.elegiveisDesbloqueados,
                    autoWidth: true
                },
                {
                    value: dado.elegiveisBloqueados,
                    autoWidth: true
                }
            ]);
        });


        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: 'Resumo Desbloqueio Tela Única',
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumodesbloqueioTelaUnica' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}

CtrlResumoDesbloqueioTelaUnica.$inject = ['$scope', '$globals'];