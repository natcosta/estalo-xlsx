function CtrlResumoRaco($scope, $globals) {

    win.title = "Resumo de cancelamento";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-resumo-raco", "Resumo de cancelamento");

    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.colunas = [
        // { field: "cod_ic", displayName: "", width: 80, pinned: true },
        { field: "nome_ic", displayName: "Descrição", width: 150, pinned: true }
    ];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.datas = [];
    $scope.totais = {};
    $scope.ordenacao = 'nome_ic';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];
    $scope.iit = false;

    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: []
    };

    /*for (var cod_aplicacao in cache.aplicacoes) {
      var apl = cache.aplicacoes[cod_aplicacao];
      if (apl.itens_controle) {
        apl.itens_controle.indice = geraIndice(apl.itens_controle);
      }
    }*/

    $scope.aba = 2;

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {

      $view = $("#pag-resumo-raco");
      // treeViewMaster($view);
		  treeView('#pag-resumo-raco #chamadas', 15,'Chamadas','dvchamadas');
		  treeView('#pag-resumo-raco #repetidas', 35,'Repetidas','dvRepetidas');
		  treeView('#pag-resumo-raco #ed', 65,'ED','dvED');
		  treeView('#pag-resumo-raco #ic', 95,'IC','dvIC');
		  treeView('#pag-resumo-raco #tid', 115,'TID','dvTid');
		  treeView('#pag-resumo-raco #vendas', 145,'Vendas','dvVendas');
		  treeView('#pag-resumo-raco #falhas', 165,'Falhas','dvFalhas');
		  treeView('#pag-resumo-raco #extratores', 195,'Extratores','dvExtratores');
		  treeView('#pag-resumo-raco #parametros', 225,'Parametros','dvParam');
		  treeView('#pag-resumo-raco #admin', 255,'Administrativo','dvAdmin');
		  treeView('#pag-resumo-raco #monitoracao', 275, 'Monitoração', 'dvReparo');
          treeView('#pag-resumo-raco #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      /*$('.nav.aba3').css('display','none');*/

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});


        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view,false,false,$scope);
        //carregaAplicacoesComData($view);
        carregaSites($view);

        carregaRegioes($view);

        carregaSegmentosPorAplicacao($scope, $view, true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
          carregaSegmentosPorAplicacao($scope, $view);
          //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
        });


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        //$view.find("select.filtro-op").selectpicker({
        //    selectedTextFormat: 'count',
        //    countSelectedText: '{0} operações'
        //});

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });






      //2014-11-27 transição de abas
      var abas = [2,3];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }

        //Marcar todos
        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($('.filtro-site'), 'sites') });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope) });

        // Marca todos os regiões
        $view.on("click", "#alinkReg", function(){
          $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});


        });



        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        //$view.on("change", "select.filtro-op", function () {
        //    Estalo.filtros.filtro_operacoes = $(this).val();
        //});


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        //$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseover(function () {
        //    $('div.btn-group.filtro-op').addClass('open');
        //    $('div.btn-group.filtro-op>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
        //});

        // OCULTAR AO TIRAR O MOUSE
        //$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseout(function () {
        //    $('div.btn-group.filtro-op').removeClass('open');
        //});


      //25/05/2015 Alex - Informação sobre consolidação
      delay($(".filtro-aplicacao >.dropdown-menu li a"), function() {
        cache.apls.forEach(function(apl){
          if(apl.nome === aplFocus){
            console.log(apl.codigo);
            var stmt = db.use+" SELECT 'Aplicação "+apl.codigo+" com registros até',MAX(DatReferencia),'COM DDD' FROM ResumoIC WHERE CodAplicacao = '"+apl.codigo+"' AND DatReferencia > GETDATE() - 2 UNION ALL SELECT 'e ',MAX(DataHora),'SEM DDD' FROM ConsolidaItemControle WHERE Cod_Aplicacao = '"+apl.codigo+"' AND DataHora > GETDATE() - 2";
            executaThreadBasico('extratorDiaADia',stringParaExtrator(stmt,[]), function(dado){console.log(dado)});
          }
        });
      });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-raco");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }


            var todas = true;
            /*for (var i = 0; i <= filtro_aplicacoes.length -1; i++) {
                if (cache.aplicacoes[filtro_aplicacoes[i]].hasOwnProperty('itens_controle')) {
                    if (cache.aplicacoes[filtro_aplicacoes[i]].itens_controle.length <= 100) {
                        delete cache.aplicacoes[filtro_aplicacoes[i]].itens_controle;
                        //todas = true;
                    }
                }
            }*/


           obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val(),true);

           var seguir = [];
		   
		   

            function teste(){
				setTimeout(function(){

              var itens = typeof $('select.filtro-aplicacao').val() ==='string' ? [$('select.filtro-aplicacao').val()] : $('select.filtro-aplicacao').val();

                  for(var i=0; i < itens.length; i++){
                    if(cache.aplicacoes[itens[i]].hasOwnProperty('itens_controle')){
                      if(seguir.indexOf(itens[i])< 0) seguir.push(itens[i]);
                    }
                  }

              if(seguir.length >= itens.length){
                clearTimeout(teste);
				console.log("seguir daqui");
                $scope.listaDados.apply(this);
              }else{
				  teste();
			  }
            },500);
			}
			teste();
          //exibeDataUltAtualAplic();

        });

        function exibeDataUltAtualAplic() {

            //Data da última atualização da aplicação
            stmt = db.use + "SELECT top 1 dataAtualizacao FROM ConsolidaItemControle "
            + "WHERE 1 = 1 "
            //stmt += restringe_consulta("ic.Cod_Aplicacao", filtro_aplicacoes, true);

        }

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-raco");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');

        /*$(window).scroll(function () {
            $view.find(".fixo-topo-esquerda").css({
                'padding-top': $(this).scrollTop() + 100,
                'padding-left': $(this).scrollLeft() + 30
            });
            $view.find(".fixo-esquerda").css({
                'padding-left': $(this).scrollLeft() + 30
            });
            $view.find(".fixo-topo").css({
                'padding-top': $(this).scrollTop() + 100
            });
        });*/
    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');
        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        //var filtro_operacoes = $view.find("select.filtro-op").val() || [];
        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_regioes.length > 0) { $scope.filtros_usados += " Regiões: " + filtro_regioes; }
        //if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
        $scope.colunas = $scope.colunas.slice(0, 1);
        $scope.dados = [];
        var stmt = "";
        $scope.ICs = [];
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };
        var tabelas = tabelasParticionadas($scope, 'ConsolidaItemControleD', false);
        var datfim = tabelas.nomes.length > 1 ? "'"+tabelas.data+"'" : "'" +  formataDataHora(precisaoHoraFim(data_fim)) + "'";
      var iit = "";

      //{QUERY PRINCIPAL
    
        // stmt = db.use + "with cte as ("
        // + " SELECT Cod_Aplicacao, Cod_Item"
        // + " FROM " + db.prefixo + "ItemDeControle"
        // + " WHERE 1 = 1"
        // stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
        // stmt += " ), cte2 as ("
        // + " SELECT distinct Cod_Aplicacao, Cod_ItemDeControle as Cod_Item"
      // + " FROM " + db.prefixo + "ConsolidaItemControleD"
        // + " WHERE 1 = 1"
        // stmt += " AND Cod_ItemDeControle <> '' AND DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
        // + " AND "+ datfim +""
        // stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
        // if (filtro_segmentos.length > 0) {
          // stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
        // }
        // stmt += restringe_consulta("Cod_Site", filtro_sites, true);
        // stmt += " )"
        // + " select cte.Cod_Item, NULL, 0"
        // + " from cte"
        // + " left outer join cte2 on cte.Cod_Aplicacao = cte2.Cod_Aplicacao and cte.Cod_Item = cte2.Cod_Item"
        // + " where cte2.Cod_Item is null"
        // + " UNION ALL"
        // + " SELECT Cod_ItemDeControle as Cod_Item, DATEADD(day, DATEDIFF(day, 0, DataHora), 0),"
        // + " SUM(Qtd "+iit+") AS Qtd"
        // + " FROM " + db.prefixo + "ConsolidaItemControleD"
        // stmt += " WHERE Cod_ItemDeControle <> '' AND DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
        // + " AND "+ datfim +""
        // stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
        // if (filtro_segmentos.length > 0) {
          // stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
        // }
        // stmt += restringe_consulta("Cod_Site", filtro_sites, true);
        // stmt += " GROUP BY Cod_ItemDeControle,"
        // + " DATEADD(day, DATEDIFF(day, 0, DataHora), 0)"
        // + " order by Cod_Item";
            /*stmt = db.use + "SELECT Cod_ItemDeControle as Cod_Item,"
              + " DATEADD(day, DATEDIFF(day, 0, ci.DataHora), 0), SUM(ci.Qtd "+iit+") AS Qtd"
              + " FROM " + db.prefixo + "ConsolidaItemControle ci"
              + " WHERE 1 = 1"
              + " AND ci.DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
              + " AND "+ datfim +""

            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            if (filtro_segmentos.length > 0) {
                stmt += restringe_consulta("Cod_Produto", filtro_segmentos, true);
            }
            stmt += restringe_consulta("Cod_Site", filtro_sites, true);

            stmt += " GROUP BY Cod_ItemDeControle, DATEADD(day, DATEDIFF(day, 0, ci.DataHora), 0)"

            stmt += " UNION ALL"
            stmt += " SELECT cod_item, null, 0"
            stmt += " FROM " + db.prefixo + "ItemDeControle WHERE Cod_Item NOT IN (SELECT Cod_ItemDeControle FROM ConsolidaItemControle"
            stmt += " WHERE DataHora BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
              + " AND "+ datfim +""
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += ")"
            stmt += restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true);
            stmt += " ORDER BY Cod_Item";*/
      //} FIM QUERY PRINCIPAL
      
      
          if($scope.iit === false) iit =  "- ISNULL(QtdTransfURA, 0)";
      stmt = db.use
      + " SELECT DatReferencia, r.CodAplicacao, Descricao, sum(QtdChamadas " +iit+ ") as Qtd"
      + " from  ResumoGenerico r "
      + " join CriteriosConsolGenerica c on r.CodAplicacao = c.CodAplicacao and r.CodCombinacao = c.CodCombinacao "
      + " WHERE 1 = 1 "
      +" AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) +  "'"
      stmt += restringe_consulta("r.CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      + " AND CodProjeto = 'ResumoCancelamento' "
      +" GROUP BY DatReferencia, r.CodAplicacao,Descricao"
      + " order by DatReferencia, r.CodAplicacao, Descricao ;"
      
      
      

      //{
// select DatReferencia, r.CodAplicacao, Descricao, sum(QtdChamadas-QtdTransfURA) as Qtd
// from ResumoGenerico r
// join CriteriosConsolGenerica c on r.CodAplicacao = c.CodAplicacao and r.CodCombinacao = c.CodCombinacao
// where DatReferencia >= '2017-02-01' and DatReferencia < '2017-02-02'
    // and CodProjeto = 'ResumoCancelamento'
// group by DatReferencia, r.CodAplicacao, Descricao
// order by DatReferencia, r.CodAplicacao, Descricao
      //}
      
      
        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery(columns) {


            // if ($.isNumeric(columns[0].value) == false) {
                // var cod_ic = columns[0].value,
              // data = columns[1].value === null ? null : "_" + formataData2(columns[1].value),
              // data_BR = columns[1].value === null ? null : formataDataBR(columns[1].value),
              // qtd_entrantes = +columns[2].value
              
              
               //  if ($.isNumeric(columns[0].value) == false) {
           //     var cod_ic = columns[0].value,
           var cod_ic = columns[2].value,
              data = columns[0].value === null ? null : "_" + formataData2(columns[0].value),
              data_BR = columns[0].value === null ? null : formataDataBR(columns[0].value),
              qtd_entrantes = +columns[3].value

                if (data != null) {
                    var d = $scope.datas[data];
                    if (d === undefined) {
                        d = { data: data, data_BR: data_BR };
                        $scope.datas.push(d);
                        $scope.datas[data] = d;
                        $scope.totais[data] = { qtd_entrantes: 0 };
                    }
                }

                var dado = $scope.dados[cod_ic];
                if (dado === undefined) {
                    dado = {
                       // cod_ic: cod_ic,
                        nome_ic: obtemDescricaoItemControleDasAplicacoes(filtro_aplicacoes, cod_ic),
                        totais: { geral: { qtd_entrantes: 0 } }
                    };
                    $scope.dados.push(dado);
                    $scope.dados[cod_ic] = dado;
                }

                if (data != null) {
                    dado.totais[data] = { qtd_entrantes: qtd_entrantes };
                    dado.totais.geral.qtd_entrantes += qtd_entrantes;
                    $scope.totais[data].qtd_entrantes += qtd_entrantes;
                    $scope.totais.geral.qtd_entrantes += qtd_entrantes;
                }

                if ($scope.dados.length % 1000 === 0) {
                    $scope.$apply();
                }
            //}
        }
        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
            }
            // Preencher com zero datas inexistentes em cada IC
            $scope.dados.forEach(function (dado) {
                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = { qtd_entrantes: 0 };
                    }
                })
            });
            $scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
            $scope.datas.forEach(function (d) {
                $scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: 100,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'  });
            });
            $scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: 100, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'  });
            $scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });
            $scope.$apply();
       
        });

        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

        $btn_gerar.button('reset');
        $btn_exportar.prop("disabled", false);
    };

    //{ Exportar Table Excel                   
 // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
        // Criar cabeçalho
        var cabecalho = $scope.datas.map(function (d) {
            return { value: d.data_BR, bold: 1 ,  autoWidth: true};
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({ value: "Descrição", bold: 1 });

        // Inserir última coluna: total por linha
        cabecalho.push({ value: "Total", bold: 1 });

        var linhas = $scope.dados.map(function (dado) {
          var contTemp = 0;
            var linha = $scope.datas.map(function (d) {
                try {
                  contTemp += dado.totais[d.data].qtd_entrantes || 0;
                    return { value: dado.totais[d.data].qtd_entrantes || 0,  autoWidth: true};
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            // linha.unshift({ value: dado.cod_ic + " - " + dado.nome_ic });
                     linha.unshift({ value:  dado.nome_ic });
                     linha.push({value : contTemp.toString(), autoWidth: true});

            // Inserir última coluna: total por linha
            // linha.push({ value: dado.totais.geral.qtd_entrantes });

            return linha;
        });

        // Criar última linha, com os totais por data
        var linha_totais = $scope.datas.map(function (d) {
            try {
                return { value: $scope.totais[d.data].qtd_entrantes || 0};
            } catch (ex) {
                return 0;
            }
        });

        // Inserir primeira coluna
        linha_totais.unshift({ value: "Total", bold: 1 });

        // Inserir última coluna: total geral
        linha_totais.push({ value: $scope.totais.geral.qtd_entrantes});

        // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);
          
        
        linhas.pop();
          
        // Inserir última linha: totais por data
        linhas.push(linha_totais);
        
        console.log(linhas);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'Resumo cancelamento', data: linhas, table: true }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 1 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoRaco_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };
    //}

}
CtrlResumoIC.$inject = ['$scope', '$globals'];
