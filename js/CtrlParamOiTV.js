function CtrlParamOiTV($scope, $globals) {

    win.title = "Parâmetros Oi TV"; //Matheus 28/02/2017
    $scope.versao = versao;

    $scope.sqlBusy = false;
	
	//$scope.registerDDD = $(".filtro-ddd").value();
	$scope.registerUF = [];
	$scope.produto = "";
	$scope.frase1 = "";
	
	
	// $scope.UFs = cache.ufs;
	// $scope.DDDs = cache.ddds;

    $scope.rotas = rotas;
    //$scope.usuario = {};
    $scope.paramsAtivos = []; // Dados do grid de usuários
    $scope.parametros = [];

    //Colunas do grid de alertas
    $scope.colunas = [
		{ displayName: "DataInicio", field: "dataini", width: 160, pinned: false },
        { displayName: "DataFim", field: "datafim", width: 160, pinned: false },
        { displayName: "Horario", field: "horario", width: 100, pinned: false },
        { displayName: "UF", field: "estado", width: 100, pinned: false },
        { displayName: "DDD", field: "ddd", width: 100, pinned: false },
        { displayName: "Mensagem", field: "mensagem", width: 190, pinned: false },
        //{ displayName: "Ação", field: "acao", width: 100, pinned: false },
        { displayName: "Produto", field: "produto", width: 100, pinned: false },
        // { displayName: "Obs", field: "obs", width: 590, pinned: false },
        { field: "Tools", displayName: "Funções", cellClass: "grid-align", width: 80, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a ng-click="deleteMsg(row.entity)" title="Deletar" class="icon-remove" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];

    $scope.filtros = {
        filterText: "",
		porDDD: false
    };

    // Grid de usuários
    $scope.gridDados = {
        data: "paramsAtivos",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true,
        filterOptions: $scope.filtros
    };
	
	
   var agora = new Date();
    var inicio, fim;
    if (Estalo.filtros.filtro_data_hora[0] !== undefined) {
        inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]);

    } else {
        inicio = hoje(); Estalo.filtros.filtro_data_hora[0];
    }
    if (Estalo.filtros.filtro_data_hora[1] !== undefined) {
        fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]);
    } else {
        fim = amanha();
    }

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
          max: dat_consoli*/
    };

    $scope.agenda = new Date(2015, 12, 2, 0, 0, 0);

    Estalo.filtros.filtro_data_hora[0] = $scope.periodo.inicio;
    Estalo.filtros.filtro_data_hora[1] = $scope.periodo.fim;

	
    $view = $("#pag-params-oitv");
	$scope.agenda = new Date(2015, 12, 2, 0, 0, 0);
	carregaUFs($view,true);
	// carregaDDDsPorUF($scope, $view, true);
	
	carregaDDDsPorRegiao($scope, $view, true);
	
	// componenteDataMaisHora($scope, $view);
	componenteDataHora($scope, $view);
    componenteHora($scope, $view);
	
	
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ufs = $view.find("select.filtro-uf").val() || [];      
		var filtro_dias = ($('#txtDias').prop('disabled') ? '0' : $('#txtDias').val());
        var filtro_hr = ($('#selHoras').prop('disabled') ? '0' : $('#selHoras').val());
        var filtro_min = ($('#selMin').prop('disabled') ? '0' : $('#selMin').val());
        var filtro_horario = ($('#agenda').val() == "00:00:00" ? null : $('#agenda').val());
		

	
    $scope.$on('$viewContentLoaded', function () {
		
		$(".filtro-uf").prop('multiple', false);
		$(".filtro-ddd").prop('multiple', false);
		
        $("#pag-params-oitv #modal-register").on('hidden', function () {
            $("#pag-params-oitv .registerAlert").hide();
            $("#pag-params-oitv .registerAlertText").text("");
            $("#pag-params-oitv .container button").blur();
            //console.log("Blurred buttons");
        });		
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-uf").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} UFs',
			showSubtext: true
		});	
		
		$(".checkAll2").css("display", "none");
		//Gambiarrel Azevedo 2018-06-21 RXRXRX O DEUS DA GAMBIRRA ANonymous
		
		 // Popula lista de  ddds a partir das UFs selecionadas
        // $view.on("change", "select.filtro-uf", function () { carregaDDDsPorUF($scope, $view) });
		$view.on("change", "select.filtro-ddd", function() { 
			console.log("Mudou FiltroUF: "+$("select.filtro-ddd").val());
			if($("select.filtro-ddd").val()!=null){
				$(".btn-register").removeAttr("disabled")
			}else{
				$(".btn-register").prop('disabled', true);
			}		
		});
		
		$view.on("change", "select.filtro-uf", function() {
			console.log("Mudou FiltroUF: "+$("select.filtro-uf").val());
			if($("select.filtro-uf").val()!=null){
				$(".btn-register").removeAttr("disabled")
			}else{
				$(".btn-register").prop('disabled', true);				
			}		
		});	

		$view.on("change", ".porddd", function() { 
			$(".btn-register").prop('disabled', true);	
		});
		
        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });		
		$(".filtro-ddd").removeAttr("disabled") 
		
        $scope.listaparamsAtivos();

        var options2 = [];
		
		// EXIBIR AO PASSAR O MOUSE
		$('div.btn-group.filtro-uf').mouseover(function () {
			$('div.btn-group.filtro-uf').addClass('open');
			$('div.btn-group.filtro-uf>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '400px'
			});
		});
		// OCULTAR AO TIRAR O MOUSE
		$('div.btn-group.filtro-uf').mouseout(function () {
			$('div.btn-group.filtro-uf').removeClass('open');
		});

        $view.on("click", ".modal-backdrop", function () {
            $(document).focus();
        });

        $view.on("click", ".close", function () {
            $(document).focus();
        });

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        $scope.abortar = function () {
            abortar($scope, "#pag-params-oitv");
        }		
		
		$scope.atualizaProduto = function () {
			console.log("Produto: "+$scope.produto);
			console.log("Mensagem: ");
			console.log($scope.mensagem);
			$scope.produto = $scope.mensagem.tipo;
			$scope.frase1 = $scope.mensagem.frase2+" - "+$scope.mensagem.frase1;
		};
		
		$scope.registraMensagem = function() {
            // limpaProgressBar($scope, "#pag-params-oitv");
            // Testar se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function() {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    // $view.find(".btn-success").button('reset');
                }, 500);
                return;
            }
			var ufddd = "";
			
			console.log("Data inicio: "+data_ini);
			console.log("Data fim: "+data_fim);
			console.log("Mensagem: "+$scope.mensagem.cod);
			console.log("Produto: "+$scope.produto);
			console.log("Horario: "+$("#agenda").val());
			if($(".filtro-uf").val()){
				console.log("UF?"+$(".filtro-uf").val());
				ufddd = "UF";
			}
			if($(".filtro-ddd").val()){
				console.log("DDD?"+$(".filtro-ddd").val());
				ufddd = "DDD";
			}
			
			if(!($(".filtro-ddd").val()||$(".filtro-uf").val())){
				alert("É necessário selecionar DDD ou UF para registrar a mensagem.", "Falha ao registrar mensagem.");
				return;
			}
			
			var horario = $('#agenda').val()==null?'':$('#agenda').val()
			var DDD = ufddd=='DDD'?$('.filtro-ddd').val():'';
			var UF = ufddd=='UF'? $('.filtro-uf').val():'';
			
			var stmt = db.use+ "INSERT into gras_ativos_uratv VALUES "
			+"('"+formataDataHora(data_ini)+"','"+formataDataHora(data_fim)+"', '"+DDD+"' , '"+UF+"','"+$scope.produto+"','"+$scope.mensagem.cod+"', '"+0+"', '"+0+"', '"+0+"', '"+horario+"')";

			console.log("STMT: "+stmt);

			db.query(stmt, function(){}, function(err, num_rows){
					//userLog(stmt, "Cadastro de Alerta", 1, err);
				if(err){
					console.log("Erro na sintaxe: "+err);
					//alerte("Erro ao registrar mensagem: "+err, "Falha no registro");
				}else{
					$(".modal-register").modal("hide");
					$('#agenda').val("");					
					$scope.listaparamsAtivos();
				}		
			});			
		};
	});

    $scope.novoParam = function () {		
        $(".modal-register").modal('show');
    }


    function carregaParams() {
        // Função responsável por selecionar quantidade de usuários para preencher inicialmente o grid vinculado a array

        stmt = "select * from ParametrosGraUraTV";

        function populateParams(columns) {

            //id	codigo	nome	prazo	Frase1	Frase2

            var id = columns[0].value,
                cod = columns[1].value,
                nome = columns[2].value,
                prazo = columns[3].value,
                frase1 = columns[4].value,
                frase2 = columns[5].value;
				tipo = columns[6].value;

            dado = {
                id: id,
                cod: cod,
                nome: nome,
                prazo: prazo,
                frase1: frase1,
                frase2: frase2,
				tipo: tipo
            };
            $scope.parametros.push(dado);
        }

        db.query(stmt, populateParams, function (err, num_rows) {

            if (err) {
                console.log("Erro na sintaxe: " + err);
            } else {
                console.log($scope.params);
            }

            //retornaStatusQuery(num_rows, $scope);
            console.log("" + num_rows);
        });
    }

    $scope.listaparamsAtivos = function () {
        $scope.params = [];
		$scope.paramsAtivos = [];
		$scope.paramsCache = [];

            $scope.paramsAtivos = [];
            // Função responsável por selecionar quantidade de usuários para preencher inicialmente o grid vinculado a array
            stmt = "select * from gras_ativos_uratv";
            function populateparamsAtivos(columns) {

                //id	codigo	nome	prazo	Frase1	Frase2
                //CODGRA	DATAINI	DATAFIM	LOGIN	PARAMETRO	ACAO	DIAS	OBS	PRAZO	MINUTOS	HORAS	HORARIO	IDGRA	ESTADO

                var dataini = columns[0].value,
                    datafim = columns[1].value,
                    DDD = columns[2].value,
                    estado = columns[3].value,
                    produto = columns[4].value,
                    mensagem = columns[5].value,
                    dias = columns[6].value,
                    horas = columns[7].value,
                    minutos = columns[8].value,
                    horario = columns[9].value

                dado = {
                    dataini: formataDataHoraBR(dataini),
                    datafim: formataDataHoraBR(datafim),
					data_ini: dataini, // sem formatar
					data_fim: datafim, // sem formatar
                    ddd: DDD,
                    estado: estado,
                    produto: produto,
                    mensagem: mensagem,
                    dias: dias,
                    horas: horas,
                    minutos: minutos,
                    horario: horario
                };
                $scope.paramsAtivos.push(dado);

            }            
        
		
			db.query(stmt, populateparamsAtivos, function (err, num_rows) {

                if (err) {
                    console.log("Erro na sintaxe: " + err);                    
                } else {
					retornaStatusQuery(num_rows, $scope);
                    console.log($scope.paramsAtivos);
                    if($scope.parametros.length === 0) carregaParams();
                }
				
				$(".grid").resize();
                console.log("" + num_rows);
            });

    };

    $scope.ativaParam = function () {

        var sAlert = $scope.Alerta;
        var sIntervalo = $scope.Intervalo;
        var sQuery = $scope.Query;
        var sEmail = $scope.Email;
        var sCopy = $scope.Copy || "";
		
		$(".modal-register").modal("show");

    };
	
	var estadoouddd, datainicial, datafinal, mensagem;
	
	function deleteMsgz(esoudd,ini,fim, msg){
		var ddduf = isNaN(esoudd)?"UF":"DDD";
			var stmtDelete = "Delete FROM gras_ativos_uratv WHERE dataini = '"+ini+"' and datafim = '"+fim+"' and mensagem = '"+msg+"' ";
			
			duf = ddduf=="UF"?"ESTADO":"DDD";
		
			stmtDelete = stmtDelete + "AND "+duf+" = '"+esoudd+"'";
			
			console.log("DeleteSTMT: "+stmtDelete);
		
		db.query(stmtDelete, function(){}, function(err, num_rows){
			if(err){
				console.log("Erro na sintaxe: "+err);
				alert("Erro ao apagar mensagem da UraTV: "+err, "Falha na exclusão");
			}else{
				//alert("Mensagem '"+mensagem+"' com data inicial "+ini+" e data final "+fim+" removida.", "Ação executada.");
				$scope.listaparamsAtivos();
			  }

			});       
	};

    $scope.deleteMsg = function (row) {
        // $(".modal-view").modal('show');
		estadoouddd = (row.estado==false?row.ddd:row.estado);
		console.log("linha: ");
		console.log(row);
		
		datainicial = formataDataHora(row.data_ini);
		datafinal = formataDataHora(row.data_fim);
		mensagem = row.mensagem;
		
		confirme("Apagar mensagem de "+estadoouddd+" com o código "+mensagem+", entre as datas  "+datainicial+" e "+datafinal+"?",
			"Excluir", "Sim", function(realize){
				if(realize){
				  deleteMsgz(estadoouddd,datainicial,datafinal,mensagem);
				}else{
				  console.log("Desistiu de apagar.");
				}        
		});
    };
}

CtrlParamOiTV.$inject = ['$scope', '$globals'];
