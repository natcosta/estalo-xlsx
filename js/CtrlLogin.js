function CtrlLogin($scope, $globals) {
    win.title = "Estalo " + versao;

    $scope.valor3 = tipoVersao;
    $scope.login = loginUsuario;
    $scope.dominio = dominioUsuario;

    $scope.versao = versao;
    $scope.rotas = rotas;

    $scope.filtros = {
        arq_cache: bkpCache
      };

    var $view;

    link = cache.releaseEstaloLink;
    $scope.htmlRelease = arrumaHtml(cache.releaseEstaloHtml);
    versaoMod = versao.substring(9, 17).replace(new RegExp('/', 'g'), '_');

    // $globals.logado = false;
    if ($globals.logado === false) {
        win.title = "Estalo " + versao;
        if (fs.existsSync(tempDir3() + "log_extrator.txt")) fs.unlinkSync(tempDir3() + "log_extrator.txt");
    } else {
        win.title = "Detalhamento de chamadas";
        cache.datsUltMesDetCham = new Date(cache.datsUltMesDetCham);
        $scope.datsUltMesDetCham = pad(cache.datsUltMesDetCham.getMonth() + 1) + "/" + cache.datsUltMesDetCham.getFullYear();
        $scope.datsUltMesDetChamFull = formataDataHoraBR(cache.datsUltMesDetCham);
    }

    if (versao === "OLD VERSION") {
        travaBotaoFiltro(0, $scope, "#pag-login", "Esta versão do Estalo está desatualizada");
    } else {
        travaBotaoFiltro(0, $scope, "#pag-login", "Consulta realizada de 5 em 5 minutos.");
    }

    splash($scope, "#pag-login", base + "img/logo-versatil.png", $globals.logado);

    $scope.$on('$viewContentLoaded', function () {	
		
		
		cache.carregaAplicacaoIC();		
        //Utilizado para verificar se há conexão
        if (true) testaConexao2($(".bar"));
        $view = $("#pag-login");

        $view.on("click", ".btn-logar", function () {
            if ($scope.valor3 !== tipoVersao) {
                var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
                    " WHERE NomeRelatorio = 'CONFIGINIT" + $scope.valor3 + "'";
                executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
                    console.log(dado);
                });
            }

            $scope.montaRotas.apply(this);

            // Mostra Modal
            var fs = require("fs");

            if (fs.existsSync(tempDir3() + 'versao.txt')) {
                var dataVersao = fs.readFileSync(tempDir3() + 'versao.txt');
            } else {
                //console.log("Versão não encontrada... Criando arquivo com versão atual");
                fs.writeFileSync(tempDir3() + 'versao.txt', pegaDataVersao);
            }

            var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
            //Fim Mostra Modal
        });

        $view.on("click", ".btn-recarregar", function () {
            location.reload(true);
        });
    });

    // Autenticação
    $scope.montaRotas = function () {
        var forms = [];

        var stmt = db.use + "SELECT DISTINCT ltrim(rtrim(Formulario)) FROM PermissoesUsuariosEstatura where LoginUsuario = '" + $scope.login + "' AND UPPER(Dominio) = '" + $scope.dominio + "'";
        log(stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			
			var bkpRotas = rotas;
			bkpRotas.indice = geraIndice2(bkpRotas, false, 'form');
			
			rotas = [];
			
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var form = (result[i][0].value).trim();					
					if (frmInc.indexOf(form) >= 0){						
						for (var j = 1; j < 20; j++){
							if(bkpRotas.indice[form+j]){
								rotas.push(bkpRotas.indice[form+j]);
								console.log(form);
							}else{
								break;
							}
						}
					}else if(bkpRotas.indice[form] !== undefined){
						rotas.push(bkpRotas.indice[form]);
					}
				}				
				var text = fs.readFileSync(tempDir() + "infoCache", "utf-8");
                text = JSON.parse(text);
                text.cache = $scope.filtros.arq_cache;
                fs.writeFileSync(tempDir() + "infoCache", JSON.stringify(text));

                // reordenação por ordem
                rotas.sort(function (primeiro, segundo) {
                    // Ordenar somente por ordem
                    return primeiro.ordem - segundo.ordem;
                });

                var partsPath = window.location.pathname.split("/");
                var part = partsPath[partsPath.length - 1];   
                window.location.href = part + '#/chamadas/';
				$('.login form label').text("");
				
			}else if (result.length === 0){
				$(".alertaVersao").show();
                $(".alertaVersao").html("<br/>Usuário não autorizado.<br/>Solicite acesso para suporte@versatec.com.br.")
                $("body").css("background-color", "#FFF");
                window.location = '#/solicitaUsuario'; //Aguardando Liberação dos Emails Matheus 06/2017
                $('.btn-logar').attr('disabled', 'disabled');				
			}
						
			  		
		  });
    };

}

CtrlLogin.$inject = ['$scope', '$globals'];