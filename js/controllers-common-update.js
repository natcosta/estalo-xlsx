//deprecated ?
function baixarNovaVersao(texto) {
	var mensagem = texto;
	$('div.notification .alert').html(mensagem);
	$('#statusCache').html(mensagem);
	//Booleano para evitar looping
	if (aplAtualizado) {
		setTimeout(function () {
			try {
				gui.Shell.openExternal(link);
			} catch (ex) {
				console.log(ex.message);
			}
		}, 5000);
	}
	aplAtualizado = false;
}


function trocaExecutavelEstaloEXE() {
	//TROCA ESTALO.EXE
	var estaloNovo = new Date(cache.releaseEstaloEXE);
	var estaloAtual = new Date(fs.statSync(tempDir3() + 'Estalo.exe').mtime);

	console.log("Data ESTALO.EXE novo -> " + estaloNovo);
	console.log("Data ESTALO.EXE atual -> " + estaloAtual);

	if (estaloAtual < estaloNovo) {

		setTimeout(function () {
			if (fs.existsSync(tempDir3() + "Estalo.exe") && fs.existsSync(tempDir3() + "Estalo_temp")) {
				try {
					fs.unlinkSync(tempDir3() + 'Estalo.exe');
					if (fs.existsSync(tempDir3() + "Estalo_temp")) {
						try {
							fs.renameSync(tempDir3() + 'Estalo_temp', tempDir3() + 'Estalo.exe');
						} catch (ex) {
							console.log(ex);
						}
						console.log("Nova versão trocada");
					}
				} catch (ex) {
					console.log(ex);
				}
			}

		}, 5000);

	} //TROCA ESTALO.EXE
}

function trocaExecutavelToolEXE() {
	//TROCA TOOL.EXE

	if (!fs.existsSync(tempDir3() + "tool.exe.full")) {
		var toolNovo = new Date(unique2(cache.toolInfos.map(function (i) {
			if (i.nome === 'TOOL' + tipoVersao + '.EXE') return i.datahora;
		})));
		var toolAtual = new Date(fs.statSync(tempDir3() + 'tool.exe').mtime);

		var pjsonTeste = require('./package.json');
		console.log("Data TOOL.EXE novo -> " + toolNovo);
		console.log("Data TOOL.EXE atual -> " + toolAtual);

		if (toolAtual < toolNovo) {
			if (pjsonTeste.window.title !== "estalo_developer") {
				console.log("Troca de TOOL" + tipoVersao + ".EXE");
				var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
					" WHERE NomeRelatorio='TOOL" + tipoVersao + ".EXE'";
				executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
					console.log(dado);
				});
			}
		}
	} //TROCA TOOL.EXE
}

