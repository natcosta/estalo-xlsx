var limiteDias = 320; //deprecated
var limiteHoras = 960; //deprecated

//{Módulos de data
function componenteDataMaisHora(scop, view, refresh) {
	if (refresh === true) {
		scop.periodo.inicio = ontem();
		scop.periodo.fim = new Date(precisaoHoraFim(ontem()));
	}
	var $Dat_Referencia = view.find(".datahora-inicio");
	$Dat_Referencia.datetimepicker({
		language: 'pt-BR',
		pickSeconds: false,
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min),
		value: formataDataBR(scop.periodo.inicio)
	});

	$Dat_Referencia.data('datetimepicker').setLocalDate(scop.periodo.inicio);

	//Método executado quando se altera a data
	$Dat_Referencia.on('changeDate', function (ev) {
		var datahora = new Date(ev.localDate);
		scop.$apply((function (datadia) {
			return function () {
				scop.periodo.inicio = datahora;
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(datahora));
		//02/03/2014 escondendo o calendario e mostrando o outro
		$(this).data("datetimepicker").hide();
		Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
	});

	var $datahora_fim = view.find(".datahora-fim");
	
	if($datahora_fim.length<=0){
		var $datahora_fim = view.find(".datahora-inicio");
	}
	
	$datahora_fim.datetimepicker({
		language: 'pt-BR',
		pickSeconds: false,
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min)
	});

	$datahora_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);

	//Método executado quando se altera a data
	$datahora_fim.on('changeDate', function (ev) {
		var datahora = new Date(ev.localDate);
		scop.$apply((function (datahora) {
			return function () {
				scop.periodo.fim = datahora;
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(datahora));
		//02/03/2014 escondendo o calendario
		$(this).data("datetimepicker").hide();
		Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
	});
}

//ES-4562
function componenteDataMesSO(scop, view, refresh) {
	if (refresh === true) {
		scop.periodo.inicio = ontem();
		scop.periodo.fim = new Date(precisaoHoraFim(ontem()));
	}
	var $Dat_Referencia = view.find(".datahora-inicio");
	$Dat_Referencia.datetimepicker({
		language: 'pt-BR',
		pickSeconds: false,
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min),
		value: formataDataBR(scop.periodo.inicio),
		format: "MM/yyyy",
		viewMode: "months",
		minViewMode: "months"
	});

	$Dat_Referencia.data('datetimepicker').setLocalDate(scop.periodo.inicio);

	//Método executado quando se altera a data
	$Dat_Referencia.on('changeDate', function (ev) {
		var datahora = new Date(ev.localDate);
		scop.$apply((function (datadia) {
			return function () {
				scop.periodo.inicio = datahora;
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(datahora));
		//02/03/2014 escondendo o calendario e mostrando o outro
		$(this).data("datetimepicker").hide();
		Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
	});

	var $datahora_fim = view.find(".datahora-fim");
	
	if($datahora_fim.length<=0){
		var $datahora_fim = view.find(".datahora-inicio");
	}
	
	$datahora_fim.datetimepicker({
		language: 'pt-BR',
		pickSeconds: false,
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min)
	});

	$datahora_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);

	//Método executado quando se altera a data
	$datahora_fim.on('changeDate', function (ev) {
		var datahora = new Date(ev.localDate);
		scop.$apply((function (datahora) {
			return function () {
				scop.periodo.fim = datahora;
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(datahora));
		//02/03/2014 escondendo o calendario
		$(this).data("datetimepicker").hide();
		Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
	});
}

function componenteHora(scop, view, refresh) {
	try {


		//Alex 21/02/2014 datetime picker hora separada
		var $hora_inicio = view.find(".hora-inicio");
		$hora_inicio.datetimepicker({
			language: 'pt-BR',
			pickDate: false,
			pick24HourFormat: true,
			startDate: new Date(),
			value: formataHora(scop.agenda)
		});

		$hora_inicio.data('datetimepicker').setLocalDate(scop.agenda);


		$hora_inicio.on('changeDate', function (ev) {
			//ajustarDSTWin(true,scop);
			var hora = new Date(ev.localDate);
			//if(ev.localDate === null) hora = new Date(BrDataToDefault(view.find('.data-inicio input').val().trim())+" 00:00:00");
			scop.$apply((function (hora) {
				return function () {
					scop.agenda = new Date(BrDataToDefault(formataHora(hora)));
					//relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view,true,true,scop) :
					//carregaAplicacoes(view,true,false,scop);
					//if(view.find('.hora-inicio input').val() === "")  view.find('.hora-inicio input').val("00:00:00");
				};
			})(hora));
			//Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
			//ajustarDSTWin(false,scop);
		});

		//Estalo.filtros.filtro_data_hora[2] = scop.periodo.inicio;

	} catch (err) {
		console.log(err);
	}
}

function componenteDataMes(scop, view, refresh) {
	try {
		if (refresh === true) {
			scop.periodo.inicio = ontem();
			scop.periodo.fim = new Date(precisaoHoraFim(ontem()));
		} else if (refresh === "chamada") {
			scop.periodo.inicio = hoje();
			scop.periodo.fim = new Date();
		} else if (refresh === "venda") {
			scop.periodo.inicio = hoje();
			scop.periodo.fim = horaAnterior();
		}

		//Alex 21/02/2014 datetime picker data separada
		var $data_inicio = view.find(".data-inicio");
		$data_inicio.datetimepicker({
			language: 'pt-BR',
			pickTime: false,
			startDate: formataDataBR(scop.periodo.min),
			value: formataDataBR(scop.periodo.inicio),
			format: "MM/yyyy",
			viewMode: "months",
			minViewMode: "months"
		});

		$data_inicio.data('datetimepicker').setLocalDate(scop.periodo.inicio);



		//Alex 21/02/2014 datetime picker hora separada
		var $hora_inicio = view.find(".hora-inicio");
		if ($hora_inicio.length > 0) {
			$hora_inicio.datetimepicker({
				language: 'pt-BR',
				pickDate: false,
				pick24HourFormat: true,
				startDate: formataHora(scop.periodo.min),
				value: formataHora(scop.periodo.inicio)
			});

			$hora_inicio.data('datetimepicker').setLocalDate(scop.periodo.inicio);
		}

		$data_inicio.on('changeDate', function (ev) {
			//ajustarDSTWin(true,scop);

			var dat = new Date(ev.localDate);

			scop.$apply((function (dat) {
				return function () {
					var $hora_inicio = view.find('.hora-inicio');
					var horario = $hora_inicio.length > 0 ? " " + $hora_inicio.find('input').val() : "";
					scop.periodo.inicio = new Date(formataData(dat) + horario);
					if(view.attr('id') !== "pag-consolidado-ed"){
						relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
					}
				};

			})(dat));

			// Gilberto 02/03/2014 escondendo o calendario e mostrando o outro
			$(this).data("datetimepicker").hide();
			//view.find(".data-fim").data("datetimepicker").show();
			Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
			Estalo.filtros.filtro_data_hora[1] = view.find(".data-fim").data("datetimepicker").startDate;
			//ajustarDSTWin(false,scop);
		});

		$hora_inicio.on('changeDate', function (ev) {
			//ajustarDSTWin(true,scop);
			var hora = new Date(ev.localDate);
			if (ev.localDate === null) hora = new Date(BrDataToDefault(view.find('.data-inicio input').val().trim()) + " 00:00:00");
			scop.$apply((function (hora) {
				return function () {
					scop.periodo.inicio = new Date(BrDataToDefault(view.find('.data-inicio input').val().trim()) + " " + formataHora(hora));
					if(view.attr('id') !== "pag-consolidado-ed"){
						relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
					}
					if (view.find('.hora-inicio input').val() === "") view.find('.hora-inicio input').val("00:00:00");
				};
			})(hora));
			Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
			//ajustarDSTWin(false,scop);
		});

		//Estalo.filtros.filtro_data_hora[2] = scop.periodo.inicio;

		//Alex 21/02/2014 datetime picker data separada
		var $data_fim = view.find(".data-fim");
		$data_fim.datetimepicker({
			language: 'pt-BR',
			pickTime: false,
			startDate: formataDataBR(scop.periodo.min),
			value: formataDataBR(scop.periodo.fim),
			format: "MM/yyyy",
			viewMode: "months",
			minViewMode: "months"
		});

		view.find(".data-fim").data("datetimepicker").startDate = Estalo.filtros.filtro_data_hora[2];

		$data_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);
		//Alex 21/02/2014 datetime picker hora separada
		var $hora_fim = view.find(".hora-fim");
		if ($hora_fim.length > 0) {
			$hora_fim.datetimepicker({
				language: 'pt-BR',
				pickDate: false,
				pick24HourFormat: true,
				startDate: formataHora(scop.periodo.min),
				value: formataHora(scop.periodo.fim)
			});

			$hora_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);
		}

		$data_fim.on('changeDate', function (ev) {
			//ajustarDSTWin(true,scop);
			var dat = new Date(ev.localDate);
			scop.$apply((function (dat) {
				return function () {
					var $hora_fim = view.find('.hora-fim');
					var horario = $hora_fim.length > 0 ? " " + $hora_fim.find('input').val() : "";
					scop.periodo.fim = new Date(formataData(dat) + horario);
					if(view.attr('id') !== "pag-consolidado-ed"){
						relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
					}
				};
			})(dat));

			// Gilberto 02/03/2014 escondendo o calendario
			$(this).data("datetimepicker").hide();
			Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
			//ajustarDSTWin(false,scop);
		});

		$hora_fim.on('changeDate', function (ev) {
			//ajustarDSTWin(true,scop);
			var hora = new Date(ev.localDate);
			scop.$apply((function (hora) {
				return function () {
					scop.periodo.fim = new Date(BrDataToDefault(view.find('.data-fim input').val().trim()) + " " + formataHora(hora));
					if(view.attr('id') !== "pag-consolidado-ed"){
						relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
					}
				};
			})(hora));
			Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
			//ajustarDSTWin(false,scop);
		});

	} catch (err) {
		console.log(err);
	}
}

function componenteDataHora(scop, view, refresh) {

	if (refresh === true) {
		scop.periodo.inicio = ontem();
		scop.periodo.fim = new Date(precisaoHoraFim(ontem()));
	} else if (refresh === "chamada") {
		scop.periodo.inicio = hoje();
		scop.periodo.fim = new Date();
	} else if (refresh === "venda") {
		scop.periodo.inicio = hoje();
		scop.periodo.fim = horaAnterior();
	}

	//Alex 21/02/2014 datetime picker data separada
	var $data_inicio = view.find(".data-inicio");
	$data_inicio.datetimepicker({
		language: 'pt-BR',
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min),
		value: formataDataBR(scop.periodo.inicio)
	});

	$data_inicio.data('datetimepicker').setLocalDate(scop.periodo.inicio);



	//Alex 21/02/2014 datetime picker hora separada
	var $hora_inicio = view.find(".hora-inicio");
	if ($hora_inicio.length > 0) {
		$hora_inicio.datetimepicker({
			language: 'pt-BR',
			pickDate: false,
			pick24HourFormat: true,
			startDate: formataHora(scop.periodo.min),
			value: formataHora(scop.periodo.inicio)
		});

		$hora_inicio.data('datetimepicker').setLocalDate(scop.periodo.inicio);
	}

	$data_inicio.on('changeDate', function (ev) {
		//ajustarDSTWin(true,scop);
		var dat = new Date(ev.localDate);
		carregaSegmentosPorAplicacao(scop, view, false, $(this).context.textContent);
		scop.$apply((function (dat) {
			return function () {
				var $hora_inicio = view.find('.hora-inicio');
				var horario = $hora_inicio.length > 0 ? " " + $hora_inicio.find('input').val() : "";
				scop.periodo.inicio = new Date(formataData(dat) + horario);
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}

			};

		})(dat));

		// Gilberto 02/03/2014 escondendo o calendario e mostrando o outro
		$(this).data("datetimepicker").hide();
		//view.find(".data-fim").data("datetimepicker").show();
		Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
		Estalo.filtros.filtro_data_hora[1] = view.find(".data-fim").data("datetimepicker").startDate;
		//ajustarDSTWin(false,scop);
	});

	$hora_inicio.on('changeDate', function (ev) {
		//ajustarDSTWin(true,scop);
		var hora = new Date(ev.localDate);
		if (ev.localDate === null) hora = new Date(BrDataToDefault(view.find('.data-inicio input').val().trim()) + " 00:00:00");
		scop.$apply((function (hora) {
			return function () {
				scop.periodo.inicio = new Date(BrDataToDefault(view.find('.data-inicio input').val().trim()) + " " + formataHora(hora));
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
				if (view.find('.hora-inicio input').val() === "") view.find('.hora-inicio input').val("00:00:00");
			};
		})(hora));
		Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
		//ajustarDSTWin(false,scop);
	});

	//Estalo.filtros.filtro_data_hora[2] = scop.periodo.inicio;

	//Alex 21/02/2014 datetime picker data separada
	var $data_fim = view.find(".data-fim");
	$data_fim.datetimepicker({
		language: 'pt-BR',
		pickTime: false,
		startDate: formataDataBR(scop.periodo.min),
		value: formataDataBR(scop.periodo.fim)
	});

	view.find(".data-fim").data("datetimepicker").startDate = Estalo.filtros.filtro_data_hora[2];

	$data_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);
	//Alex 21/02/2014 datetime picker hora separada
	var $hora_fim = view.find(".hora-fim");
	if ($hora_fim.length > 0) {
		$hora_fim.datetimepicker({
			language: 'pt-BR',
			pickDate: false,
			pick24HourFormat: true,
			startDate: formataHora(scop.periodo.min),
			value: formataHora(scop.periodo.fim)
		});

		$hora_fim.data('datetimepicker').setLocalDate(scop.periodo.fim);
	}

	$data_fim.on('changeDate', function (ev) {
		//ajustarDSTWin(true,scop);
		var dat = new Date(ev.localDate);
		scop.$apply((function (dat) {
			return function () {
				var $hora_fim = view.find('.hora-fim');
				var horario = $hora_fim.length > 0 ? " " + $hora_fim.find('input').val() : "";
				scop.periodo.fim = new Date(formataData(dat) + horario);
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(dat));

		// Gilberto 02/03/2014 escondendo o calendario
		$(this).data("datetimepicker").hide();
		Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
		//ajustarDSTWin(false,scop);
	});

	$hora_fim.on('changeDate', function (ev) {
		//ajustarDSTWin(true,scop);
		var hora = new Date(ev.localDate);
		scop.$apply((function (hora) {
			return function () {
				scop.periodo.fim = new Date(BrDataToDefault(view.find('.data-fim input').val().trim()) + " " + formataHora(hora));
				if(view.attr('id') !== "pag-consolidado-ed"){
					relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
				}
			};
		})(hora));
		Estalo.filtros.filtro_data_hora[1] = scop.periodo.fim;
		//ajustarDSTWin(false,scop);
	});



}
//}

//{29/01/2014 - 15/03/2014 - 22/03/2014 testa limite de tempo
function testaLimiteTempo(data_ini, data_fim, limite, parametro) {
	var horas = moment(data_fim).diff(moment(data_ini), parametro);
	var novadata = data_fim;
	if (horas > limite) {
		novadata = moment(data_ini).add(parametro, limite - 1)._d;
	}
	return novadata;
}
//}

//{22/03/2014 Testa data incluíndo hora
function testeDataMaisHora(dataini, datafim) {
	var mensagem = "";
	var testedata = moment(datafim).diff(moment(dataini), "days");
	if (testedata < 0) {
		mensagem = "Data início MAIOR ou IGUAL que a data fim";
	} else if (testedata === 0) {
		testedata = moment(datafim).diff(moment(dataini), "seconds");
		if (testedata <= 0) {
			mensagem = "Data início MAIOR ou IGUAL que a data fim";
		}
	}
	return mensagem;
}
//}

function testeDataMaisHoraLimite(dataini, datafim, limite) {
	var mensagem = "";
	var testedata = moment(datafim).diff(moment(dataini), "days");
	if (testedata > limite) {
		mensagem = "Limite máximo  excedido em " + (testedata - (limite)) + " dia(s).";
	}
	return mensagem;
}

//{24/03/2014 Testa data
function testeDataHora(dataini, datafim, msg) {
	var mensagem = "";
	var testedata = moment(datafim).diff(moment(dataini), "days");
	if (testedata < 0) {
		mensagem = msg;
	}
	return mensagem;
}
//}

function tabelaHorasStr(data_ini, data_fim) {

	var fim = testaLimiteTempo(data_ini, data_fim, limiteHoras, "hours");
	var intervalHora = data_ini,
		tabelas = "";
	tabelas += '[' + formataDataHora(data_ini) + '],';

	do {
		intervalHora = horaSeguinte(intervalHora);
		tabelas += '[' + formataDataHora(intervalHora) + '],';
	}
	while (intervalHora < fim);
	tabelas = tabelas.substring(0, tabelas.length - 1);
	return tabelas;
}

function tabelaHorasArr(data_ini, data_fim) {

	var fim = testaLimiteTempo(data_ini, data_fim, limiteHoras, "hours");
	var intervalHora = data_ini,
		tabelas = [];
	data_ini_mod = formataDataHoraBR(data_ini);
	item = {
		value: data_ini_mod,
		bold: 1,
		hAlign: 'center',
		autoWidth: true
	};
	tabelas.push(item);

	do {
		intervalHora = horaSeguinte(intervalHora);
		intervalHoraMod = formataDataHoraBR(intervalHora);
		item = {
			value: intervalHoraMod,
			bold: 1,
			hAlign: 'center',
			autoWidth: true
		};
		tabelas.push(item);
	}
	while (intervalHora < fim);

	return tabelas;
}

//{ pivot sql deprecated ?
function tabelaHorasArrView(data_ini, data_fim) {

	var fim = testaLimiteTempo(data_ini, data_fim, limiteHoras, "hours");
	var intervalHora = data_ini,
		tabelas = [];
	data_ini_mod = formataDataHoraBR(data_ini);
	tabelas.push(data_ini_mod);

	do {
		intervalHora = horaSeguinte(intervalHora);
		intervalHoraMod = formataDataHoraBR(intervalHora);
		tabelas.push(intervalHoraMod);
	}
	while (intervalHora < fim);

	return tabelas;
}

function tabelaDiasStr(data_ini, data_fim) {

	var fim = testaLimiteTempo(data_ini, data_fim, limiteDias, "days");
	var intervalDia = data_ini,
		tabelas = "";
	tabelas += '[' + formataData(data_ini) + '],';

	while (formataData(intervalDia) < formataData(fim)) {
		intervalDia = amanha(intervalDia);
		tabelas += '[' + formataData(intervalDia) + '],';
	}

	var arrQtd = tabelas.split(",");

	tabelas = tabelas.substring(0, tabelas.length - 1);
	return tabelas;
}

function tabelaDiasArr(data_ini, data_fim) {

	var fim = testaLimiteTempo(data_ini, data_fim, limiteDias, "days");
	var intervalDia = data_ini,
		tabelas = [];
	data_ini_mod = formataDataBR(data_ini);
	item = {
		value: data_ini_mod,
		bold: 1,
		hAlign: 'center',
		autoWidth: true
	};
	tabelas.push(item);

	while (formataData(intervalDia) < formataData(fim)) {
		intervalDia = amanha(intervalDia);
		intervalDiaMod = formataDataBR(intervalDia);
		item = {
			value: intervalDiaMod,
			bold: 1,
			hAlign: 'center',
			autoWidth: true
		};
		tabelas.push(item);
	}

	tabelas = tabelas.slice(0, limiteDias);
	return tabelas;
}

function tabelaDiasArrView(data_ini, data_fim) {
	var fim = testaLimiteTempo(data_ini, data_fim, limiteDias, "days");
	var intervalDia = data_ini,
		tabelas = [];
	data_ini_mod = formataDataBR(data_ini);
	tabelas.push(data_ini_mod);

	while (formataData(intervalDia) < formataData(fim)) {
		intervalDia = amanha(intervalDia);
		intervalDiaMod = formataDataBR(intervalDia);
		tabelas.push(intervalDiaMod);
	}


	tabelas = tabelas.slice(0, limiteDias);
	return tabelas;
}
//}

//{Array de consultas
function arrayDiasQuery(query, data_ini, data_fim) {

	//var fim = testaLimiteTempo(data_ini,data_fim,limiteHoras,"hours");
	var intervalDia = data_ini,
		tabelas = [];

	var newQuery = query.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoHoraFim(data_ini)));

	item = [newQuery];
	tabelas.push(item);

	if (data_fim - data_ini === 0) {
		tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoHoraFim(data_ini)), 'g'), formataDataHora(data_ini));
		item = [tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoHoraFim(data_ini)), 'g'), formataDataHora(data_ini))];
		tabelas.push(item);

	} else {

		do {
			//intervalHora = moment(intervalHora).add('hours',1)._d;
			intervalDia = amanha(intervalDia);
			newQuery = query.replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal(intervalDia)));
			newQuery = newQuery.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoHoraFim(intervalDia)));
			item = [newQuery];
			tabelas.push(item);
		}
		while (intervalDia < data_fim);

		newQuery = tabelas[tabelas.length - 1].toString().replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal(intervalDia)));
		newQuery = newQuery.replace(new RegExp(formataDataHora(precisaoHoraFim(intervalDia)), 'g'), formataDataHora(data_fim));
		item = [newQuery];
		tabelas.pop();
		tabelas.push(item);
	}
	return tabelas;
}

function arrayHorasQuery(query, data_ini, data_fim) {

	//var fim = testaLimiteTempo(data_ini,data_fim,limiteHoras,"hours");
	var intervalHora = data_ini,
		tabelas = [];

	var newQuery = query.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoMinutoFim(data_ini)));

	item = [newQuery];
	tabelas.push(item);

	if (data_fim - data_ini === 0) {
		tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoMinutoFim(data_ini)), 'g'), formataDataHora(data_ini));
		item = [tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoMinutoFim(data_ini)), 'g'), formataDataHora(data_ini))];
		tabelas.push(item);

	} else {

		do {
			//intervalHora = moment(intervalHora).add('hours',1)._d;
			intervalHora = horaSeguinte(intervalHora);
			newQuery = query.replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal(intervalHora)));
			newQuery = newQuery.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoMinutoFim(intervalHora)));
			item = [newQuery];
			tabelas.push(item);
		}
		while (intervalHora < data_fim);

		newQuery = tabelas[tabelas.length - 1].toString().replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal(intervalHora)));
		newQuery = newQuery.replace(new RegExp(formataDataHora(precisaoMinutoFim(intervalHora)), 'g'), formataDataHora(data_fim));
		item = [newQuery];
		tabelas.pop();
		tabelas.push(item);
	}
	return tabelas;
}

function arrayMinutosQuery(query, data_ini, data_fim) {

	//var fim = testaLimiteTempo(data_ini,data_fim,limiteHoras,"hours");
	var intervalMinuto = data_ini,
		tabelas = [];

	var newQuery = query.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoSegundoFim(data_ini)));

	item = [newQuery];
	tabelas.push(item);

	if (data_fim - data_ini === 0) {
		tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoSegundoFim(data_ini)), 'g'), formataDataHora(data_ini));
		item = [tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoSegundoFim(data_ini)), 'g'), formataDataHora(data_ini))];
		tabelas.push(item);

	} else {

		do {
			//intervalHora = moment(intervalHora).add('hours',1)._d;
			intervalMinuto = minutoSeguinte(intervalMinuto);
			newQuery = query.replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
			newQuery = newQuery.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoSegundoFim(intervalMinuto)));
			item = [newQuery];
			tabelas.push(item);
		}
		while (intervalMinuto < data_fim);

		newQuery = tabelas[tabelas.length - 1].toString().replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
		newQuery = newQuery.replace(new RegExp(formataDataHora(precisaoSegundoFim(intervalMinuto)), 'g'), formataDataHora(data_fim));
		item = [newQuery];
		tabelas.pop();
		tabelas.push(item);
	}
	return tabelas;
}

function arrayCincoMinutosQuery(query, data_ini, data_fim) {

	//var fim = testaLimiteTempo(data_ini,data_fim,limiteHoras,"hours");
	var intervalMinuto = data_ini,
		tabelas = [];

	var newQuery = query.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoCincoMinSegundoFim(data_ini)));

	item = [newQuery];
	tabelas.push(item);

	if (data_fim - data_ini === 0) {
		tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoCincoMinSegundoFim(data_ini)), 'g'), formataDataHora(data_ini));
		item = [tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoCincoMinSegundoFim(data_ini)), 'g'), formataDataHora(data_ini))];
		tabelas.push(item);

	} else {

		do {
			//intervalHora = moment(intervalHora).add('hours',1)._d;
			intervalMinuto = cincoMinutoSeguinte(intervalMinuto);
			newQuery = query.replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
			newQuery = newQuery.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoCincoMinSegundoFim(intervalMinuto)));
			item = [newQuery];
			tabelas.push(item);
		}
		while (intervalMinuto < data_fim);

		newQuery = tabelas[tabelas.length - 1].toString().replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
		newQuery = newQuery.replace(new RegExp(formataDataHora(precisaoCincoMinSegundoFim(intervalMinuto)), 'g'), formataDataHora(data_fim));
		item = [newQuery];
		tabelas.pop();
		tabelas.push(item);
	}
	return tabelas;
}

function arrayDezMinutosQuery(query, data_ini, data_fim) {

	//var fim = testaLimiteTempo(data_ini,data_fim,limiteHoras,"hours");
	var intervalMinuto = data_ini,
		tabelas = [];

	var newQuery = query.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoDezMinSegundoFim(data_ini)));

	item = [newQuery];
	tabelas.push(item);

	if (data_fim - data_ini === 0) {
		tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoDezMinSegundoFim(data_ini)), 'g'), formataDataHora(data_ini));
		item = [tabelas[0].toString().replace(new RegExp(formataDataHora(precisaoDezMinSegundoFim(data_ini)), 'g'), formataDataHora(data_ini))];
		tabelas.push(item);

	} else {

		do {
			//intervalHora = moment(intervalHora).add('hours',1)._d;
			intervalMinuto = dezMinutoSeguinte(intervalMinuto);
			newQuery = query.replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
			newQuery = newQuery.replace(new RegExp(formataDataHora(data_fim), 'g'), formataDataHora(precisaoDezMinSegundoFim(intervalMinuto)));
			item = [newQuery];
			tabelas.push(item);
		}
		while (intervalMinuto < data_fim);

		newQuery = tabelas[tabelas.length - 1].toString().replace(new RegExp(formataDataHora(data_ini), 'g'), formataDataHora(dataConsoliReal2(intervalMinuto)));
		newQuery = newQuery.replace(new RegExp(formataDataHora(precisaoDezMinSegundoFim(intervalMinuto)), 'g'), formataDataHora(data_fim));
		item = [newQuery];
		tabelas.pop();
		tabelas.push(item);
	}
	return tabelas;
}

function arrayNumAnisQuery(stmt, anis, p, data, inicio, fim) {

	var numero;
	var parteQuery = "";

	if (p === 'Telefone Tratado') numero = 'ClienteTratado';
	if (p === 'Telefone Chamador') numero = 'NumANI';

	if (data) {
		parteQuery = "AND IVR.DataHora_Inicio >= '" + formataDataHora(inicio) + "' AND IVR.DataHora_Inicio <='" + formataDataHora(fim) + "'";
	}

	var stmts = [];
	for (i = 0; i < anis.length; i++) {
		var s = stmt.replace(/AND IVR.DataHora_Inicio >= \'.+\' /g, "and " + numero + " = '" + anis[i] + "' " + (parteQuery !== "" ? parteQuery : "") + " ");

		stmts.push(s);

	}

	return stmts;
}
//}


function tabelasMensais(data_ini, data_fim) {
	var ano = data_ini.getFullYear(),
		mes = data_ini.getMonth() + 1,
		ano_fim = data_fim.getFullYear(),
		mes_fim = data_fim.getMonth() + 1,
		tabelas = [];
	while (ano < ano_fim || (ano === ano_fim && mes <= mes_fim)) {
		tabelas.push({
			ano: ano,
			mes: mes
		});
		if (++mes > 12) {
			ano++;
			mes = 1;
		}
	}
	return tabelas;
}

//{10/05/2014 stringDataPathToDate
function stringDataPathToDate(valor) {
	valor = valor.split("_");
	var hora = valor[2].substring(0, valor[2].length - 3).replace(/-/g, ':');
	var data = valor[1] + " " + hora;
	data = new Date(data);
	return data;
}
//}

function stringToDate(string){
	//espera uma data no formato: 01/12/2017 18:07:57
	dia = string.substring(0,2)
	mes = string.substring(3,5)
	ano = string.substring(6,10)
	hora = string.substring(11,13)
	minuto = string.substring(14,16)
	segundo = string.substring(17,19)
	milliseconds = 0
	d = ""+ano+"-"+mes+"-"+dia+"T"+hora+":"+minuto+":"+segundo
	console.log(d)
	return new Date(d)
	// return new Date(ano, mes, dia, hora, minuto, segundo, milliseconds);
}

//{Funções para tratar datas
function _02d(x) {
	return (x < 10 ? "0" : "") + x;
}

function ehAnoBissexto(ano) {
	return ano % 4 === 0 && ano % 100 !== 0 || ano % 400 === 0;
}

function getDaysInMonth(ano, mes) {
	return [31, (ehAnoBissexto(ano) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][mes];
}

function formataDataHora3(data) {
	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return y + "-" + _02d(m) + "-" + _02d(d) + " " + _02d(h);
}

function formataDataBR(data, granularidade) {


	granularidade = granularidade || "dia";
	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate();
	if (granularidade === "dia") {
		return _02d(d) + "/" + _02d(m) + "/" + y;
	} else if (granularidade === "mes") {
		return _02d(m) + "/" + y;
	} else if (granularidade === "ano") {
		return "" + y;
	}
}

function formataDataBR2(data) {


	var y = data.getFullYear().toString().substring(2, 4),
		m = data.getMonth() + 1,
		d = data.getDate();

	return _02d(d) + "/" + _02d(m) + "/" + y;

}

function formataDataBR3(data) {


	var m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes();
		
	return _02d(d) + "/" + _02d(m) + " " + _02d(h)+ ":" + _02d(min);

}


function formataDataBRString(valor) {
	var dia = valor.substring(5, 7),
		mes = valor.substring(8, 10),
		ano = valor.substring(0, 4);

	return mes + "/" + dia + "/" + ano;
}

function formataDataHoraBRString(valor) {
	var dia = valor.substring(5, 7),
		mes = valor.substring(8, 10),
		ano = valor.substring(0, 4),
		hora = valor.substring(11, 19);
	return mes + "/" + dia + "/" + ano + " " + hora;
}

function formataDataHoraSemSegsBRString(valor) {
	var dia = valor.substring(5, 7),
		mes = valor.substring(8, 10),
		ano = valor.substring(0, 4),
		hora = valor.substring(11, 16);
	return mes + "/" + dia + "/" + ano + " " + hora;
}

function formataData(data) {


	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate();
	return y + "-" + _02d(m) + "-" + _02d(d);
}

function formataData2(data) {


	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate();
	return y + _02d(m) + _02d(d);
}

function formataData2String(valor) {
	var mes = valor.substring(5, 7),
		dia = valor.substring(8, 10),
		ano = valor.substring(0, 4);
	return ano + mes + dia;
}

function formataDataHora(data) {
	
	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return y + "-" + _02d(m) + "-" + _02d(d) + " " + _02d(h) + ":" + _02d(min) + ":" + _02d(s);
}

function formataDataHoraNova(data,nova) {
		if(nova === 7200000){
			var dat = data.toLocaleDateString();
			var tim = data.toLocaleTimeString();
			dat = dat.split('/');
			tim = tim.split(':');
			
			var	 y = dat[2],
			m = dat[1],
			d = dat[0],
			h = tim[0],
			min = tim[1],
			s = tim[2];
			

		   
			return y + "-" + _02d(m) + "-" + _02d(d) + " " + h + ":" + min + ":" + s;
			
		}else if(nova === 10800000 && new Date('2019-01-02 00:00:00') || nova === 14400000){// && data >= '2019-12-31 00:00:00' || nova === 10800000 && data < '2019-01-02 00:00:00'){
			
			var dat = data.toLocaleDateString();
			var tim = data.toLocaleTimeString();
			
			if(data.toLocaleString() === '1/1/2019 00:00:00'){
				dat = '1/1/2019';
				tim = '01:00:00';
			}else if(data.toLocaleString() === '1/1/2019 01:00:00'){
				dat = '1/1/2019';
				tim = '02:00:00';
			}else if(data.toLocaleString() === '1/1/2019 02:00:00'){
			dat = '1/1/2019';
			tim = '03:00:00';
			}else if(data.toLocaleString() === '1/1/2019 03:00:00'){
			dat = '1/1/2019';
			tim = '04:00:00';
			}else if(data.toLocaleString() === '1/1/2019 04:00:00'){
			dat = '1/1/2019';
			tim = '05:00:00';
			}else if(data.toLocaleString() === '1/1/2019 05:00:00'){
			dat = '1/1/2019';
			tim = '06:00:00';
			}else if(data.toLocaleString() === '1/1/2019 06:00:00'){
			dat = '1/1/2019';
			tim = '07:00:00';
			}else if(data.toLocaleString() === '1/1/2019 07:00:00'){
			dat = '1/1/2019';
			tim = '08:00:00';
			}else if(data.toLocaleString() === '1/1/2019 08:00:00'){
			dat = '1/1/2019';
			tim = '09:00:00';
			}else if(data.toLocaleString() === '1/1/2019 09:00:00'){
			dat = '1/1/2019';
			tim = '10:00:00';
			}else if(data.toLocaleString() === '1/1/2019 10:00:00'){
			dat = '1/1/2019';
			tim = '11:00:00';
			}else if(data.toLocaleString() === '1/1/2019 11:00:00'){
			dat = '1/1/2019';
			tim = '12:00:00';
			}else if(data.toLocaleString() === '1/1/2019 12:00:00'){
			dat = '1/1/2019';
			tim = '13:00:00';
			}else if(data.toLocaleString() === '1/1/2019 13:00:00'){
			dat = '1/1/2019';
			tim = '14:00:00';
			}else if(data.toLocaleString() === '1/1/2019 14:00:00'){
			dat = '1/1/2019';
			tim = '15:00:00';
			}else if(data.toLocaleString() === '1/1/2019 15:00:00'){
			dat = '1/1/2019';
			tim = '16:00:00';
			}else if(data.toLocaleString() === '1/1/2019 16:00:00'){
			dat = '1/1/2019';
			tim = '17:00:00';
			}else if(data.toLocaleString() === '1/1/2019 17:00:00'){
			dat = '1/1/2019';
			tim = '18:00:00';
			}else if(data.toLocaleString() === '1/1/2019 18:00:00'){
			dat = '1/1/2019';
			tim = '19:00:00';
			}else if(data.toLocaleString() === '1/1/2019 19:00:00'){
			dat = '1/1/2019';
			tim = '20:00:00';
			}else if(data.toLocaleString() === '1/1/2019 20:00:00'){
			dat = '1/1/2019';
			tim = '21:00:00';
			}else if(data.toLocaleString() === '1/1/2019 21:00:00'){
			dat = '1/1/2019';
			tim = '22:00:00';
			}else if(data.toLocaleString() === '1/1/2019 22:00:00'){
			dat = '1/1/2019';
			tim = '23:00:00';			
			}
				
			dat = dat.split('/');
			tim = tim.split(':');
			
			var	 y = dat[2],
			m = dat[1],
			d = dat[0],
			h = tim[0],
			min = tim[1],
			s = tim[2];
				
			return y + "-" + _02d(m) + "-" + _02d(d) + " " + h + ":" + min + ":" + s;
			
		}else{			
		
		
			var y = data.getFullYear(),
			m = data.getMonth() + 1,
			d = data.getDate(),
			h = data.getHours(),
			min = data.getMinutes(),
			s = data.getSeconds();			
			
			return y + "-" + _02d(m) + "-" + _02d(d) + " " + _02d(h) + ":" + _02d(min) + ":" + _02d(s);
		}
}

function formataDataHoraDoze(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return y + "-" + _02d(m) + "-" + _02d(d) + " 13:" + _02d(min) + ":" + _02d(s);
}

function formataDataHoraSemSegs(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes();
	return _02d(d) + "/" + _02d(m) + "/" + y + " " + _02d(h) + ":" + _02d(min);
}

function formataDataHora2(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return y + _02d(m) + _02d(d) + _02d(h) + _02d(min) + _02d(s);
}

function formataHora(data) {

	var h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return _02d(h) + ":" + _02d(min) + ":" + _02d(s);
}

function BrDataToDefault2(valor) {

	var dia = valor.substring(0, 2),
		mes = valor.substring(3, 5),
		ano = valor.substring(6, 10),
		resto = valor.substring(11, valor.length);
	return ano + "-" + mes + "-" + dia +' '+resto;
}

function BrDataToDefault(valor) {

	var dia = valor.substring(0, 2),
		mes = valor.substring(3, 5),
		ano = valor.substring(6, 10);
	return mes + "-" + dia + "-" + ano;
}

function formataDataHoraMilis(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds(),
		mil = data.getMilliseconds();
	return y + "-" + _02d(m) + "-" + _02d(d) + "_" + _02d(h) + "-" + _02d(min) + "-" + _02d(s) + "-" + _02d(mil);
}

function formataDataHoraBR_old(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return _02d(d) + "/" + _02d(m) + "/" + y + " " + _02d(h) + ":" + _02d(min) + ":" + _02d(s);
}

function formataDataHoraBR(data,nova) {

	
		
		if(nova === 7200000 && data >= new Date('2019-01-01 00:00:00') && data < new Date('2019-01-02 00:00:00')){
			var dat = data.toLocaleDateString();
			var tim = data.toLocaleTimeString();
			dat = dat.split('/');
			tim = tim.split(':');
			
			var	 y = dat[2],
			m = dat[1],
			d = dat[0],
			h = tim[0],
			min = tim[1],
			s = tim[2];
				
			return _02d(d) + "/" + _02d(m) + "/" + y + " " + h + ":" + min + ":" + s;
			
		}else if(nova === 10800000 && data >= new Date('2019-01-01 00:00:00') && data < new Date('2019-01-02 00:00:00') 
			|| nova === 14400000 && data >= new Date('2019-01-01 00:00:00') && data < new Date('2019-01-02 00:00:00')
			){// && data >= '2019-12-31 00:00:00' || nova === 10800000 && data < '2019-01-02 00:00:00'){
			
			var dat = data.toLocaleDateString();
			var tim = data.toLocaleTimeString();
			
			if(data.toLocaleString() === '1/1/2019 00:00:00'){
				dat = '1/1/2019';
				tim = '01:00:00';
			}else if(data.toLocaleString() === '1/1/2019 01:00:00'){
				dat = '1/1/2019';
				tim = '02:00:00';
			}else if(data.toLocaleString() === '1/1/2019 02:00:00'){
			dat = '1/1/2019';
			tim = '03:00:00';
			}else if(data.toLocaleString() === '1/1/2019 03:00:00'){
			dat = '1/1/2019';
			tim = '04:00:00';
			}else if(data.toLocaleString() === '1/1/2019 04:00:00'){
			dat = '1/1/2019';
			tim = '05:00:00';
			}else if(data.toLocaleString() === '1/1/2019 05:00:00'){
			dat = '1/1/2019';
			tim = '06:00:00';
			}else if(data.toLocaleString() === '1/1/2019 06:00:00'){
			dat = '1/1/2019';
			tim = '07:00:00';
			}else if(data.toLocaleString() === '1/1/2019 07:00:00'){
			dat = '1/1/2019';
			tim = '08:00:00';
			}else if(data.toLocaleString() === '1/1/2019 08:00:00'){
			dat = '1/1/2019';
			tim = '09:00:00';
			}else if(data.toLocaleString() === '1/1/2019 09:00:00'){
			dat = '1/1/2019';
			tim = '10:00:00';
			}else if(data.toLocaleString() === '1/1/2019 10:00:00'){
			dat = '1/1/2019';
			tim = '11:00:00';
			}else if(data.toLocaleString() === '1/1/2019 11:00:00'){
			dat = '1/1/2019';
			tim = '12:00:00';
			}else if(data.toLocaleString() === '1/1/2019 12:00:00'){
			dat = '1/1/2019';
			tim = '13:00:00';
			}else if(data.toLocaleString() === '1/1/2019 13:00:00'){
			dat = '1/1/2019';
			tim = '14:00:00';
			}else if(data.toLocaleString() === '1/1/2019 14:00:00'){
			dat = '1/1/2019';
			tim = '15:00:00';
			}else if(data.toLocaleString() === '1/1/2019 15:00:00'){
			dat = '1/1/2019';
			tim = '16:00:00';
			}else if(data.toLocaleString() === '1/1/2019 16:00:00'){
			dat = '1/1/2019';
			tim = '17:00:00';
			}else if(data.toLocaleString() === '1/1/2019 17:00:00'){
			dat = '1/1/2019';
			tim = '18:00:00';
			}else if(data.toLocaleString() === '1/1/2019 18:00:00'){
			dat = '1/1/2019';
			tim = '19:00:00';
			}else if(data.toLocaleString() === '1/1/2019 19:00:00'){
			dat = '1/1/2019';
			tim = '20:00:00';
			}else if(data.toLocaleString() === '1/1/2019 20:00:00'){
			dat = '1/1/2019';
			tim = '21:00:00';
			}else if(data.toLocaleString() === '1/1/2019 21:00:00'){
			dat = '1/1/2019';
			tim = '22:00:00';
			}else if(data.toLocaleString() === '1/1/2019 22:00:00'){
			dat = '1/1/2019';
			tim = '23:00:00';			
			}
				
			dat = dat.split('/');
			tim = tim.split(':');
			
			var	 y = dat[2],
			m = dat[1],
			d = dat[0],
			h = tim[0],
			min = tim[1],
			s = tim[2];
				
			return _02d(d) + "/" + _02d(m) + "/" + y + " " + h + ":" + min + ":" + s;
			
		}else{
			

			
			var y = data.getFullYear(),
			m = data.getMonth() + 1,
			d = data.getDate(),
			h = data.getHours(),
			min = data.getMinutes(),
			s = data.getSeconds();			
			
			return _02d(d) + "/" + _02d(m) + "/" + y + " " + _02d(h) + ":" + _02d(min) + ":" + _02d(s);
		}
		
		
	
}

function formataDataHoraBR2(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return _02d(d) + "/" + _02d(m) + "/" + y + " na hora " + _02d(h);
}

function formataDataHoraBR3(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return _02d(d) + "/" + _02d(m) + " " + _02d(h) + "h";
}

function formataDataHoraBRUrl(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate(),
		h = data.getHours(),
		min = data.getMinutes(),
		s = data.getSeconds();
	return _02d(d) + "_" + _02d(m) + "_" + y + "_" + _02d(h) + "_" + _02d(min) + "_" + _02d(s);
}

function formataDataBRUrl(data) {

	var y = data.getFullYear(),
		m = data.getMonth() + 1,
		d = data.getDate();;
	return _02d(d) + "_" + _02d(m) + "_" + y;
}

function formataDataMesString(data) {
	var mes = data.substr(5, 2)
	var obj = {
		'01': 'janeiro',
		'02': 'fevereiro',
		'03': 'março',
		'04': 'abril',
		'05': 'maio',
		'06': 'junho',
		'07': 'julho',
		'08': 'agosto',
		'09': 'setembro',
		'10': 'outubro',
		'11': 'novembro',
		'12': 'dezembro'
	}
	return obj[mes];
}

function formataDataDiaSemana(numero){
		if (numero == "1" ) return "Domingo"
		if (numero == "2" ) return "Segunda-feira"
		if (numero == "3" ) return "Terça-feira"
		if (numero == "4" ) return "Quarta-feira"
		if (numero == "5" ) return "Quinta-feira"
		if (numero == "6" ) return "Sexta-feira"
		if (numero == "7" ) return "Sábado"
	}

function mesAtual(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), 1);
}

function mesAnterior(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth() - 1, 1);
}

function mesAnterior2(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
}

function mesAnterior3(date, numero, h, m, s) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth() - numero, date.getDate(), h, m, s);
}

function mesSeguinte(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth() + 1, 1);
}

function hoje(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

function hojeFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
}

function getDataServidor(){
	mssqlQueryTedious("select getdate() as datahora", function(err, result){
		if (err) console.log(err);
		return new Date(result[0].datahora.value);
	})
}

function antiOntem(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 2);
}

function ontem(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
}

function antes(date,num) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - (num));
}

function ontemInicio(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1, 0, 0, 0);
}

function ontemFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1, 23, 59, 59);
}
function antesDeOntemInicio(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 2, 0, 0, 0);
}

function antesDeOntemFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 2, 23, 59, 59);
}

function precisaoHoraIni(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 00, 00, 00);
}

function precisaoMesInicio(n) {
	return amanha(new Date(n.getFullYear(), n.getMonth(), 0));
}

function precisaoMesFim(date) {
	return precisaoHoraFim(new Date(date.getFullYear(), date.getMonth() + 1, 0));
}

function precisaoHoraFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
}

function precisaoHoraFimZero(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 00, 00);
}

function primeiroDiaDoMes(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth());
}

function ultimoDiaDoMes(data, zero) {
	var y = data.getFullYear(),
		m = data.getMonth() + 1;
	var dd = new Date(y, m, 0);

	if (zero !== undefined) {
		return precisaoHoraFimZero(dd);
	} else {
		return precisaoHoraFim(dd);
	}
}

function precisaoMinutoFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 59, 59);
}

function precisaoSegundoFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 59);
}

function precisaoCincoMinSegundoFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + 4, 59);
}
function precisaoDezMinSegundoFim(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + 9, 59);
}

function dataConsoliReal2(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0);
}

function dataConsoliReal(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 00, 0);
}

function ontemFimAlternativo(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1, 23, 00, 0);
}

function amanha(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
}

function horaAtual(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours());
}

function horaAnterior(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours() - 1);
}

function horaSeguinte(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours() + 1);
}

function minutoAtual(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
}

function minutoAnterior(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() - 1);
}

function minutosAnteriores(date, valor) {
	date || (date = new Date());
	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() - (valor));
}

function minutoAnteriorPrecisaoFim(date) {
	date || (date = new Date());
	var ajuste = parseInt(pad(parseInt(new Date().getMinutes().toString())).substring(1));
	if (ajuste < 5) {
		//console.log("é menor "+ajuste);
		ajuste = ajuste + 1;
	} else if (ajuste > 5) {
		//console.log("é maior "+ajuste);
		ajuste = (ajuste - 5) + 1;
	} else {
		//console.log("é igual");
		ajuste = 6;
	}
	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() - (ajuste), 59);
}

function minutoSeguinte(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + 1);
}

function cincoMinutoSeguinte(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + 5);
}

function dezMinutoSeguinte(date) {
	date || (date = new Date());

	return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + 10);
}
//}

//{02/04/2014 pad
function pad(n) {
	return ("0" + n).slice(-2);
}
//}

