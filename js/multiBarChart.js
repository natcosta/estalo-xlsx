function multiBarChart(data, dimensions, svgElem, totalWidth, totalHeight, options) {
  options || (options = {});

  var margin = {top: 30, right: -30, bottom: 150, left: 70},
      xTickFormat = options.xTickFormat || function (d) { return d3.time.format("%H")(d) + "h"; },
      yTickFormat = options.yTickFormat || function (d) { return d > 0 && d % 1000 === 0 ? (d / 1000) + " mil" : (d !== 0 ? d + "" : ""); },
      maxDataPoint = options.maxDataPoint,
      gapBetweenGroups = options.gapBetweenGroups || 10,
      maxBars = 10;

  var startDate = data[0][0].date;
  var endDate = data[0][data[0].length - 1].date;

  // figure out the maximum data point
  if (maxDataPoint === undefined) {
    maxDataPoint = 0;
    data.forEach(function (d0) {
      d0.slice(0, maxBars).forEach(function (d) {
        dimensions.forEach(function (dim) {
          var v = d[dim.field];
          if (v > maxDataPoint) {
            maxDataPoint = v;
          }
        });
      });
    });
  }

  var zippedData = data.map(function (d, i) {
    var z = [];
    d.slice(0, maxBars).forEach(function (d) {
      dimensions.forEach(function (dim) {
        var a = { date: d.date, value: d[dim.field], title: dim.field, color: dim.color };
        z.push(a);
      });
    });
    return z;
  });

  function draw(totalWidth, totalHeight) {
    svgElem.selectAll("g").remove();

    if (totalWidth === 0) {
      totalWidth = svgElem[0][0].parentNode.getBoundingClientRect().width;
    }

    var width = totalWidth - margin.left - margin.right,
        height = totalHeight - margin.top - margin.bottom;

    var svg = svgElem
        .attr("width", totalWidth)
        .attr("height", totalHeight);

    // Obter o título de cada gráfico
    var titles = data.map(function (d) { return d[0].title; });

    var chartHeight = height * (1 / data.length);

    var charts = [];
    data.forEach(function (d, i) {
      charts.push(new Chart({
        data: d.slice(0, maxBars),
        zippedData: zippedData[i],
        id: i,
        name: titles[i],
        width: width,
        height: height * (1 / data.length),
        svg: svg,
        margin: margin,
        showBottomAxis: (i === titles.length - 1),
        showEmptyBottomAxis: (i !== titles.length - 1)
      }));
    });
  }

  function Chart(options) {
    this.data = options.data;
    this.zippedData = options.zippedData;
    this.width = options.width;
    this.height = options.height;
    this.svg = options.svg;
    this.id = options.id;
    this.name = options.name;
    this.margin = options.margin;
    this.showBottomAxis = options.showBottomAxis;
    this.showEmptyBottomAxis = options.showEmptyBottomAxis;

    var localName = this.name;

    var that = this;

    /* XScale is time based */
    /*this.xScale = d3.time.scale()
        .range([0, this.width])
        .domain(d3.extent(this.data.map(function (d) { return d.date; })));*/
    this.xScale = d3.scale.ordinal()
      .domain(this.data.map(function (d) { return d.date; }))
      .rangeBands([0, this.width - 60], .1);

    /* YScale is linear based on the maxData Point we found earlier */
    this.yScale = d3.scale.linear()
        .range([this.height, 0])
        .domain([0, maxDataPoint]);

    var xS = this.xScale;
    var yS = this.yScale;

    var x1 = d3.scale.ordinal()
      .domain(dimensions.map(function (dim) { return dim.field; }))
      .rangeBands([0, xS.rangeBand()]);

    // assign it a class so we can assign a fill color and position it on the page
    this.chartContainer = this.svg.append("g")
        .attr("class", this.name.toLowerCase())
        .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + (this.height * this.id) + (10 * this.id)) + ")");

/*
    var barWidth = (this.width - (this.data.length - 1) * gapBetweenGroups) / (this.data.length * dimensions.length);

    // Criar as barras
    var bars = this.chartContainer.selectAll("g")
        .data(this.zippedData)
        .enter().append("g")
        .attr("transform", function (d, i) {
          return "translate(" + (i * barWidth + gapBetweenGroups * (0.5 + Math.floor(i / dimensions.length))) + "," + yS(d.value) + ")";
        });

    // Desenhar os retângulos
    bars.append("rect")
        //.style("fill", function (d, i) { return color(i % dimensions.length); })
        .style("fill", function (d) { return d.color || null; })
        .attr("class", function (d) { return "bar " + d.title; })
        .attr("width", barWidth - 1)
        .attr("width", function(d) { return x(d.date) + x1(p.key); })
        .attr("height", function (d) { return that.height - yS(d.value); });
*/

    // Criar as barras
    var bars = this.chartContainer.selectAll("rect")
        .data(this.zippedData)
        .enter().append("rect")
        .attr("x", function (d) { return xS(d.date) + x1(d.title); })
        .attr("y", function (d) { return yS(d.value); })
        .attr("width", function(d) { return x1.rangeBand(); })
        .attr("height", function (d) { return that.height - yS(d.value); })
        .attr("class", function (d) { return "bar " + d.title; })
        .style("fill", function (d) { return d.color || null; });

    this.xAxisTop = d3.svg.axis().scale(this.xScale).orient("top")
        .tickFormat(function (d, i) { return that.width < 410 && i % 2 == 0 ? "" : xTickFormat(d); });
    this.xAxisBottom = d3.svg.axis().scale(this.xScale).orient("bottom")
        .tickFormat(function (d, i) { return that.width < 410 && i % 2 == 0 ? "" : xTickFormat(d); });

    // Criar um eixo superior apenas para o primeiro gráfico
    if (this.id == 0) {
      this.chartContainer.append("g")
          .attr("class", "x axis top")
          .attr("transform", "translate(0,0)")
          .call(this.xAxisTop);
    }

    // Criar um eixo inferior apenas para o último gráfico
    if (this.showBottomAxis) {
      this.chartContainer.append("g")
          .attr("class", "x axis bottom")
          .attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxisBottom);
    } else if (this.showEmptyBottomAxis) {
      this.xAxisBottom.ticks(0)
          .tickFormat(function (d) { return ""; });
      this.chartContainer.append("g")
          .attr("class", "x axis bottom")
          .attr("transform", "translate(0," + this.height + ")")
          .call(this.xAxisBottom);
    }

    this.yAxis = d3.svg.axis().scale(this.yScale).orient("left").ticks(4).tickFormat(yTickFormat);

    this.chartContainer.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(-15,0)")
        .call(this.yAxis);

    this.chartContainer.append("text")
        .attr("class","title")
        .attr("transform", "translate(15,40)")
        .text(this.name);
  }

  draw(totalWidth, totalHeight);

  return draw;
}