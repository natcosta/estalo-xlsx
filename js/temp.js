function DB(log) {

  this.log = log || function () {};

  this.connection = null;
  this.state = "idle";


  var pjsonTeste0 = require('./package.json');
  var v = pjsonTeste0.window.title;
//{ Pega info PC Usuários
  var fs = require('fs');
var os = require('os');
var pcInfo="CPU : "+ os.cpus()[0].model+", Núcleos : " + os.cpus().length + " Memória RAM: " + Math.round(os.totalmem()/Math.pow(1024,3))+"GB, Resolução : "+window.innerWidth+"x"+window.innerHeight+"px";
  
//}
  this.use = "";//"/* Estalo - "+v+" - "+versao+" "+pcInfo+"  */";
  this.prefixo = "";

  var self = this;

  this.connect = function (config, callback) {
    if (self.connection) return;

    if (config.server === "server01.versatec.com.br") {
      self.use = "USE Estalo; ";
      self.prefixo = "dbo.";
    }
    else if (config.server === "versa345001-pc") {
      self.use = "USE Estalo; ";
      self.prefixo = "dbo.";
    }


    self.log("Iniciando conexão...");
    self.state = "connecting";
    self.connection = new Connection(config);

    self.connection.on('connect', function (err) {
      self.state = "idle";
      if (err) {
        self.log("Não foi possível conectar");
      }
      self.log("Conexão estabelecida");
      callback && callback(err);
    });

    //self.connection.on('debug', function (text) { self.log(text); });
  };

  this.disconnect = function () {
    if (!self.connection) return;
    self.log("Encerrando conexão...");
    self.connection.close();
    self.connection = null;
    self.state = "idle";
    self.log("Conexão encerrada");
  };

  this.isConnected = function () {
    return self.connection !== null;
  };

  this.isIdle = function () {
    return self.state === "idle";
  };

  this.query = function (stmt, on_row, on_done, setParameters) {

    if (!self.connection) return;
    var request = new Request(stmt, function (err, num_rows) {

      self.state = "idle";
      if (err) {
        self.log("Erro: " + err);
      } else {
        self.log(num_rows + " registros recebidos");

      }

      on_done && on_done(err, num_rows);
    });

    setParameters && setParameters(request);

    request.on('row', function (columns) {
      on_row && on_row(columns);
    });

    self.log("Executando query...");
    self.state = "querying";

    // Gilberto 15/04/2014
    try{
        self.connection.execSql(request);
    }catch(e){
      console.log(e.name + ": " + e.message);
  }
  };

  this.procedure = function (procName, on_row, on_done, setParameters) {

    if (!self.connection) return;
    var request = new Request(procName, function (err, num_rows) {

      self.state = "idle";
      if (err) {
        self.log("Erro: " + err);
      } else {
        self.log(num_rows + " registros recebidos");
      }

      on_done && on_done(err, num_rows);
    });

    setParameters && setParameters(request);

    request.on('row', function (columns) {
      on_row && on_row(columns);
    });

    self.log("Executando procedure...");
    self.state = "querying";

    // Gilberto 15/04/2014
    try{
        self.connection.callProcedure(request);

    }catch(e){
      console.log(e.name + ": " + e.message);
  }
  };

}


/*
 * CtrlChamadas
 */
var changeFalhas = false;

function CtrlChamadas($scope, $globals, $compile, $timeout,$window) {

  $scope.langICs = {
    selectAll: "Selecionar todos",
    selectNone: "Remover seleção",
    reset: "Desfazer",
    search: "Pesquisar IC..",
    nothingSelected: "Nenhum IC selecionado" //default-label is deprecated and replaced with this.
  }

  $scope.valor3 = tipoVersao;
  $scope.login = loginUsuario;
  $scope.dominio = dominioUsuario;

  link = cache.releaseEstaloLink;
  $scope.htmlRelease = arrumaHtml(cache.releaseEstaloHtml);
  versaoMod = versao.substring(9, 17).replace(new RegExp('/', 'g'), '_');

  if ($globals.logado === false) {
    win.title = "Estalo " + versao;
    if (fs.existsSync(tempDir3() + "log_extrator.txt")) fs.unlinkSync(tempDir3() + "log_extrator.txt");
  } else {
    win.title = "Detalhamento de chamadas";
    cache.datsUltMesDetCham = new Date(cache.datsUltMesDetCham);
    $scope.datsUltMesDetCham = pad(cache.datsUltMesDetCham.getMonth() + 1) + "/" + cache.datsUltMesDetCham.getFullYear();
    $scope.datsUltMesDetChamFull = formataDataHoraBR(cache.datsUltMesDetCham);

    trocaExecutavelEstaloEXE();
    trocaExecutavelToolEXE();
  }
  
  /*
  try {
    childProcess.exec('start /MIN cmd /c pscp.exe -pw "Est@l0 Oi" estalo@web567.webfaction.com:/home/versatiltec/webapps/htdocs/downloads/bases/ftp.txt ' + tempDir3() + '', function (error) {
      if (error) console.log(error)
    });
  } catch (ex) {
    console.log(ex);
  }
  */

  $scope.versao = versao;
  $scope.rotas = rotas;

  $scope.limite_registros = 10000;
  var tempoLimite = 500;

  $scope.estadoEnx = false;
  $scope.itemEnx = false;
  $scope.itemEnxAll = false;

  if (versao === "OLD VERSION") {
    travaBotaoFiltro(0, $scope, "#pag-chamadas", "Esta versão do Estalo está desatualizada");
  } else {
    travaBotaoFiltro(0, $scope, "#pag-chamadas", "Consulta realizada de 5 em 5 minutos.");
  }

  splash($scope, "#pag-chamadas", base + "img/logo-versatil.png", $globals.logado);

  $scope.chamadas = [];
  $scope.ordenacao = ['data_hora', 'chamador'];
  $scope.decrescente = false;
  $scope.ocultar = {
    tratado: false,
    log_formatado: false,
    log_original: true
  };

  $scope.colunas = [];
  $scope.gridDados = {
    data: "chamadas",
    rowTemplate: '<div ng-dblclick="formataXML(row.entity)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
    columnDefs: "colunas",
    enableColumnResize: true,
    enablePinning: true
  };

  $scope.chamada_atual = undefined;
  $scope.callLog = "";
  $scope.logxml = [];

  $scope.log = [];

  $scope.aplicacoes = [];
  $scope.segmentos = [];
  $scope.falhas = [];
  $scope.valor = "Telefone tratado";

  // Filtros
  $scope.filtros = {
    aplicacoes: [],
    segmentos: [],
    falhas: [],
    chamador: "",
    isUltimoED: false,
    UCID: "",
    arq_cache: bkpCache,
    nps: false
  };

  $scope.logValidado = {};
  $scope.extratorArquivoPorData = false;
  $scope.msgAlerta = false;
  $scope.callFlow = false;
  $scope.extrator = false;
  $scope.descExtracao = "";
  $scope.descExtracao2 = "";
  $scope.aba = 2;

  function geraColunas() {

    var array = [];
    array.push({
      field: "data_hora_BR",
      displayName: "Data",
      width: 150,
      pinned: true
    }, {
      field: "chamador",
      displayName: "Chamador",
      width: 100,
      pinned: false
    });
    $scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));
    //console.log("Ocultar Tratado? "+$scope.ocultar.tratado+" Valores do Filtro: ")
    //console.log($view.find("select.filtro-aplicacao"));
    if (!$scope.ocultar.tratado) {
      array.push({
        field: "tratado",
        displayName: "Tratado",
        width: 100,
        pinned: false
      });
    }

    array.push({
      field: "aplicacao",
      displayName: "Aplicação",
      width: 130,
      pinned: false
    }, {
      field: "segmento",
      displayName: "Segmento",
      width: 100,
      pinned: false
    }, {
      field: "encerramento",
      displayName: "Encerramento",
      width: 150,
      pinned: false
    }, {
      field: "estado_final",
      displayName: "Último estado",
      width: 400,
      pinned: false
    });

    if ($scope.filtros.nps === true) {
      array.push({
        field: "npss",
        displayName: "Satisf.",
        width: 80,
        pinned: false
      }, {
        field: "npsr",
        displayName: "Recom.",
        width: 80,
        pinned: false
      });
    }

    array.push({
      field: "ucid",
      displayName: "Log",
      width: 65,
      cellClass: "grid-align",
      pinned: false,
      cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>'
    });

    return array;
  }

  // Filtros: data e hora
  var agora = new Date();

  var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
  var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

  $scope.periodo = {
    inicio: inicio,
    fim: fim,
    min: new Date(2013, 11, 1),
    max: agora // FIXME: atualizar ao virar o dia
  };

  var $view;
  var numanis = [];


  var invalidsNumanis = [];

  $scope.$on('$viewContentLoaded', function () {
    //Utilizado para verificar se há conexão
    if (true) testaConexao2($(".bar"));

    $('.log-original').css('overflow', 'hidden');
    $('#modalAlerta').on('hidden', function () {
      $("#modalAlerta").css({
        'margin-top': '0px',
        'width': '0px',
        'z-index': '0'
      });

      if ($scope.msgAlerta) {
        var fs = require("fs");
        var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
        fs.writeFileSync(tempDir3() + 'versao.txt', pegaDataVersao);
      }

      $('div.notification').fadeIn(500);
    });
    $('#pag-chamadas .grid').css("margin-top", "100px")
    $view = $("#pag-chamadas");

    $(".dropdown-menu").mousemove(function (event) {
      if ($("#modalAlerta").attr("aria-hidden") == "false") {
        $('#listaM').remove();
      } else {
        $("#modalAlerta").remove();
      }

    });


    //NOVO ESTILO
    $(".aba4").css({
      'position': 'fixed',
      'left': '590px',
      'top': '40px'
    });
    $(".aba5").css({
      'position': 'fixed',
      'left': '340px',
      'top': '40px'
    });
    $('#contextVal').css('width', '50px');
    $(".aba6").css({
      'position': 'fixed',
      'left': '6px',
      'top': '40px'
    });
    $(".ultimomes").css({
      'position': 'fixed',
      'left': '770px',
      'top': '50px'
    });
    $(".botoes").css({
      'position': 'fixed',
      'left': 'auto',
      'right': '25px',
      'margin-top': '35px'
    });

    //minuteStep: 5

    componenteDataHora($scope, $view);

    // Carregar filtros
    carregaAplicacoes($view, false, false, $scope);
    carregaSites($view);
    carregaFalhas($view);
    carregaJsons($view);
    carregaSegmentosPorAplicacao($scope, $view, true);

    // change de filtros

    // Popula lista de segmentos a partir das aplicações selecionadas
    $view.on("change", "select.filtro-aplicacao", function () {
      // $scope.carregarICsEdsArquivo($('select.filtro-aplicacao').val());
      carregaSegmentosPorAplicacao($scope, $view);
    });

    $view.on("change", "select.filtro-segmento", function () {
      Estalo.filtros.filtro_segmentos = $(this).val() || [];
    });

    $view.on("change", "select.filtro-ic", function () {
      Estalo.filtros.filtro_ic = $(this).val() || [];
    });

    $view.on("change", "select.filtro-ed", function () {
      Estalo.filtros.filtro_ed = $(this).val() || [];
    });

    $view.on("change", "select.filtro-jsons", function () {
      Estalo.filtros.varjson = $(this).val() || [];
    });

    $view.on("change", "select.filtro-site", function () {
      var filtro_sites = $(this).val() || [];
      Estalo.filtros.filtro_sites = filtro_sites;
    });

    $("#import2").click(function () {
      $('#modalConfirma').css("z-index", "1045");
    });

    $("#confirma").click(function () {
      $("#browser").trigger("click");
    });

    $("#confirma").click(function () {
      $('#modalConfirma').css("z-index", "-999");
    });

    $('#browser').change(function () {
      numanis = fs.readFileSync($("#browser").val()).toString().split('\r\n');
      var n1 = [];
      var n2 = [];

      if (numanis.length > 0) {
        for (var i = 0; i < numanis.length; i++) {
          if (n1.indexOf(numanis[i].trim()) < 0 && numanis[i].trim() !== "" && numanis[i].trim().length >= 10 && numanis[i].trim().length <= 11) {
            n1.push(numanis[i].trim());
          } else if (numanis[i].trim() !== "") {
            n2.push(numanis[i].trim());
          }
        }
        if (numanis.length > 0) {
          numanis = n1;
          invalidsNumanis = n2;
          $('#import2').css('background', '#A9D0F5').selectpicker('refresh');
        } else {
          numanis = [];
          invalidsNumanis = [];
          $("#browser").val("");
          alert("Arquivo fora de especificação. O padrão é DDD + 8 ou 9 dígitos para cada linha.");
        }
      }
    });

    $view.on("change", "select.filtro-falhas", function () {
      changeFalhas = true;
      var filtro_falhas = $(this).val() || [];
      Estalo.filtros.filtro_falhas = filtro_falhas;
    });

    $("#pag-chamadas").on("click", ".filtro-falhas div.dropdown-menu.open ul li", function () {
      if (changeFalhas === false) {
        Estalo.filtros.filtro_falhas = [];
        $('select.filtro-falhas').val('').selectpicker('refresh');
      }
      changeFalhas = false;
    });

    $("#pag-chamadas").on("click", ".dropdown-menu.opcao", function () {
      if ($('.filtro-chamador').attr('placeholder') === "CPF/CNPJ" || $('.filtro-chamador').attr('placeholder') === "Contrato") {
        $('#callFlow').attr('disabled', true);
        $('#callFlow').prop('checked', false);
        $scope.callFlow = false;
      } else {
        $('#callFlow').attr('disabled', false);
      }
    });


    //{ Marcar todos

    // Marca todos os sites
    $view.on("click", "#alinkSite", function () {
      marcaTodosIndependente($('.filtro-site'), 'sites');
    });

    // Marca todos os segmentos
    $view.on("click", "#alinkSeg", function () {
      marcaTodosIndependente($('.filtro-segmento'), 'segmentos');
    });

    // Marcar todas aplicações
    $view.on("click", "#alinkApl", function () {
      marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope);
      $(".filtro-aplicacao").trigger('change');
    });

    //}

    // Default view filtros

    $view.find("select.filtro-aplicacao").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} aplicações',
      showSubtext: true
    });

    $view.find("select.filtro-site").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} Sites/POPs',
      showSubtext: true
    });
    $view.find("select.filtro-segmento").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} segmentos'
    });

    $view.find("select.filtro-ed").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} EDs'
    });

    $view.find("select.filtro-ic").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} ICs'
    });

    $view.find("select.filtro-falhas").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} Falhas'
    });

    $view.find("select.filtro-jsons").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} Variáveis'
    });


    // Informação sobre consolidação
    delay($(".filtro-aplicacao >.dropdown-menu li a"), function () {
      cache.apls.forEach(function (apl) {
        if (apl.nome === aplFocus) {
          var stmt = db.use + " SELECT 'Aplicação " + apl.codigo + " atualizada até',MAX(DataHora) FROM HistoricoConsolidacao WHERE CodAplicacao = '" + apl.codigo + "'";
          executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
            console.log(dado);
          });
        }
      });
    });

    $view.on("click", ".btn-logar", function () {
      if ($scope.valor3 !== tipoVersao) {
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
          " WHERE NomeRelatorio = 'CONFIGINIT" + $scope.valor3 + "'";
        executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
          console.log(dado);
        });
      }

      $scope.montaRotas.apply(this);

      // Mostra Modal
      var fs = require("fs");

      if (fs.existsSync(tempDir3() + 'versao.txt')) {
        var dataVersao = fs.readFileSync(tempDir3() + 'versao.txt');
      } else {
        //console.log("Versão não encontrada... Criando arquivo com versão atual");
        fs.writeFileSync(tempDir3() + 'versao.txt', pegaDataVersao);
      }

      var pegaDataVersao = versao.match(/\d\d\/\d\d\/\d\d/g);
      //Fim Mostra Modal
    });

    $view.on("click", ".btn-recarregar", function () {
      location.reload(true);
    });

    // Lista chamadas conforme filtros
    $view.on("click", ".btn-gerar", function () {
      limpaProgressBar($scope, "#pag-chamadas");
      // Testar se data início é maior que a data fim
      var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;
      var testedata = testeDataMaisHora(data_ini, data_fim);
      if (testedata !== "") {
        setTimeout(function () {
          atualizaInfo($scope, testedata);
          effectNotification();
          $view.find(".btn-gerar").button('reset');
        }, 500);
        return;
      }

      if ($scope.extrator) {
        $scope.extracao.apply(this);
        return;
      }

      var filtro_ics = $scope.filtroICSelect || [];
      var filtro_eds = $scope.filtroEDSelect || [];
      var filtro_jsons = $view.find("select.filtro-jsons").val() || [];

      if (filtro_jsons.length > 1 && $('#contextVal').val() === "") {
        pv = [];
        for (var i = 1; i < filtro_jsons.length; i++) {
          pv.push(';');
        }
        $('#contextVal').val(pv.join(""));
      }

      var contexto = $('#contextVal').val().split(';');
      if (contexto[0] === "" && contexto.length === 1 && filtro_jsons.length === 0) contexto = [];

      if (filtro_jsons.length !== contexto.length) {
        setTimeout(function () {
          atualizaInfo($scope, '<font color = "White">O número de variáveis de contexto deve ser o mesmo que os valores pretendidos separados por ponto e vírgula</font>');
          effectNotification();
          $view.find(".btn-gerar").button('reset');
          $view.find(".btn-exportar").prop("disabled", "disabled");
        }, 500);
        return;
      }
	  
	  $scope.colunas = geraColunas();
      $scope.listaChamadas.apply(this);


    });

   
    $view.on("dblclick", "div.ng-scope.ngRow", function () {

      $scope.$apply(function () {
        $scope.ocultar.log_original = true;
        $scope.ocultar.log_prerouting = true;
        $scope.ocultar.log_preroutingret = true;
        $scope.ocultar.log_formatado = false;
      });
      $view.find("#modal-log").modal('show');
    });

    $view.on("click", "tr.chamada", function () {
      var that = $(this);
      $('tr.chamada.marcado').toggleClass('marcado');
      $scope.$apply(function () {
        that.toggleClass('marcado');
      });
    });


    $view.on("click", ".btn-log-prerouting", function (ev) {
      killThreads();
      ev.preventDefault();

      $scope.consultaCallLog($scope.chamada_atual);

      $scope.$apply(function () {
        $scope.ocultar.log_formatado = true;
        $scope.ocultar.log_prerouting = false;
        $scope.ocultar.log_preroutingret = true;
        $scope.ocultar.log_original = true;
      });
    });

    $view.on("click", ".btn-log-preroutingret", function (ev) {
      killThreads();
      ev.preventDefault();

      $scope.consultaCallLog($scope.chamada_atual);

      $scope.$apply(function () {
        $scope.ocultar.log_formatado = true;
        $scope.ocultar.log_prerouting = true;
        $scope.ocultar.log_preroutingret = false;
        $scope.ocultar.log_original = true;
      });
    });

    $view.on("click", ".btn-log-original", function (ev) {
      ev.preventDefault();

      $scope.consultaCallLog($scope.chamada_atual);

      $scope.$apply(function () {
        $scope.ocultar.log_formatado = true;
        $scope.ocultar.log_prerouting = true;
        $scope.ocultar.log_preroutingret = true;
        $scope.ocultar.log_original = false;
      });
    });

    $view.on("click", ".btn-log-formatado", function (ev) {
      killThreads();
      ev.preventDefault();

      $scope.formataXML($scope.chamada_atual);

      $scope.$apply(function () {
        $scope.ocultar.log_original = true;
        $scope.ocultar.log_prerouting = true;
        $scope.ocultar.log_preroutingret = true;
        $scope.ocultar.log_formatado = false;
      });
    });

    $view.on("click", ".btn-log-formatadoEdEnx", function (ev) {
      killThreads();
      $scope.formataXML($scope.chamada_atual, true);
      $scope.$apply();
    });

    $view.on("click", ".btn-log-formatadoIcEnx", function (ev) {
      killThreads();
      $('.btn-log-formatadoAllIcEnx').attr('checked', false);
      $scope.itemEnxAll = false;
      $scope.formataXML($scope.chamada_atual, true);
      $scope.$apply();
    });

    $view.on("click", ".btn-log-formatadoAllIcEnx", function (ev) {
      killThreads();
      $('.btn-log-formatadoIcEnx').attr('checked', false);
      $scope.itemEnx = false;
      $scope.formataXML($scope.chamada_atual, true);
      $scope.$apply();
    });

    $view.on("click", ".btn-exportar", function () {
      $scope.exportaXLSX.apply(this);
    });

   

    // Botão agora
    $view.on("click", ".btn-agora", function () {
      $scope.agora.apply(this);
    });

    $scope.agora = function () {
      iniciaAgora($view, $scope);
    };

    // Botão abortar
    $view.on("click", ".abortar", function () {
      killThreads();
      //{PROJETO TABS
      var tempReso = $(".row-fluid").outerHeight() + 10;
      //$('#pag-chamadas .grid').css("margin-top",tempReso);
      //}
      $scope.abortar.apply(this);
    });

    $scope.abortar = function () {
      abortar($scope, "#pag-chamadas");
    };

    $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  });



  // Lista chamadas conforme filtros
  $scope.listaChamadas = function () {

    var placeholder = $scope.filtros.chamador;
    $globals.numeroDeRegistros = 0;
    $scope.ordenacao = ['data_hora', 'chamador'];
    var $btn_exportar = $view.find(".btn-exportar");
    /*$btn_exportar.prop("disabled", true);*/

    var $btn_gerar = $(this);
    $btn_gerar.button('loading');

    var filtro_chamadores = $scope.filtros.chamador;
    if (filtro_chamadores === "x") filtro_chamadores = "";
    if (filtro_chamadores.match(/^\d*x\d*$/)) {
      filtro_chamadores = filtro_chamadores.replace("x", "%");
    }

    filtro_chamadores = filtro_chamadores !== "" ? [filtro_chamadores] : [];

    var data_ini = $scope.periodo.inicio,
      data_fim = $scope.periodo.fim;

    var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

    if (placeholder === "" && !$scope.callFlow && filtro_aplicacoes.length === 0 && numanis.length === 0 && invalidsNumanis.length === 0) {
      atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
      $btn_gerar.button('reset');
      return;
    }

    var filtro_sites = $view.find("select.filtro-site").val() || [];
    var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
    var filtro_jsons = $view.find("select.filtro-jsons").val() || [];
    var contex = $('#contextVal').val().split(';');

    if (filtro_jsons.length === 0 && contex[0] === "") contex = [];

    var filtro_valJsons = contex || [];
    var filtro_falhas = $view.find("select.filtro-falhas").val() || [];
    //var texto = "", ed = $(".filtro-ed").val() || [], ic = $(".filtro-ic").val() || [];
    var texto = "",
      ed = $scope.filtroEDSelect || [],
      ic = $scope.filtroICSelect || [];
    $scope.filtros.isUltimoED === true ? texto = "Último estado" : texto = "Estados de diálogo";

    var edNome = $scope.filtroEDSelect ? $scope.filtroEDSelect.map(function (e) {
      return '<se na="' + obtemNomeEstado($('.filtro-aplicacao').val(), e) + '"';
    }) : [];

    $scope.ocultar.tratado = apenasUSSD($view.find("select.filtro-aplicacao"));
    console.log("Ocultar Tratado? "+$scope.ocultar.tratado+" Valores do Filtro: ")
    console.log($view.find("select.filtro-aplicacao"));
    if ($scope.ocultar.tratado) {
      $('#pag-chamadas table').css({
        'margin-top': '40px',
        'margin-bottom': '100px',
        'max-width': '1190px',
        'margin-left': 'auto',
        'margin-right': 'auto'
      });
      $scope.$apply();
    } else {
      $('#pag-chamadas table').css({
        'margin-top': '40px',
        'margin-bottom': '100px',
        'max-width': '1310px'
      });
      $scope.$apply();
    }

    // filtros usados

    $scope.filtros_usados = "";

    if (placeholder === "") {
      $scope.filtros_usados += " Período: " + formataDataHoraBR($scope.periodo.inicio) +
        " até " + formataDataHoraBR($scope.periodo.fim);
    }

    if (numanis.length > 0 && $('#data').prop("checked") == false) {
      $scope.filtros_usados = "";
    }

    if (filtro_aplicacoes.length > 0) {
      $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
    }
    if (filtro_sites.length > 0) {
      $scope.filtros_usados += " Sites: " + filtro_sites;
    }
    if (filtro_falhas != "Falhas") {
      $scope.filtros_usados += " Falha: " + filtro_falhas;
    }
    if (filtro_segmentos.length > 0) {
      $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
    }
    if (filtro_jsons.length > 0) {
      $scope.filtros_usados += " Variáveis: " + filtro_jsons;
    }
    if (filtro_valJsons.length > 0 && filtro_valJsons[0] !== "") {
      $scope.filtros_usados += " Valores: " + filtro_valJsons;
    }

    if (filtro_chamadores != "") {
      $scope.filtros_usados += ($('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? " Chamador: " :
        $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' ? " Telefone tratado: " : $('.filtro-chamador').attr('placeholder') === 'Contrato' ? " Contrato: " :
        $('.filtro-chamador').attr('placeholder') === 'Protocolo' ? " Protocolo: " : " CPF/CNPJ: ") + filtro_chamadores;
    }
    if (ed.length > 0 && !$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
      $scope.filtros_usados += " " + texto + ": " + ed;
    }
    if (ic.length > 0 && !$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
      $scope.filtros_usados += " Items de controle: " + ic;
    }

    $scope.chamadas = [];

    var xml = "";
    var nps = "";
    var tabela = "IVRCDR";

    //if ($scope.filtros.isHFiltroIC === true && ic.length > 0) {
    if (ic.length > 0) {
      var likes = ic.map(function (codIC) {
        return "XMLIVR LIKE '%<ty>pc</ty><va>" + codIC + "</va>%'";
      });
      xml = ", CASE WHEN " + likes.join(" OR ") + " THEN 0 ELSE 1 END AS XMLIVR";
    }

    if ($scope.filtros.nps === true) {
      nps = ', respostaPesq';
    }

    var valor = placeholder !== "" ? 10000 : $scope.limite_registros;
    var stmt = db.use +
      " SELECT" + testaLimite(valor) +
      "   IVR.CodUCIDIVR, IVR.DataHora_Inicio, NumANI, ClienteTratado, IVR.CodAplicacao," +
      "   QtdSegsDuracao, SegmentoChamada, IndicSucessoGeral, Sequencia_EstadoDialogo " + xml +
      ", IndicDelig, transferid, ctxid, CodSite" + nps +
      (filtro_falhas !== "Falhas" ? ', ErroAp, ErroCtg, SemPmt, SemDig, ErroVendas' : '') +
      (filtro_jsons.length > 0 ? ', PreRoteamentoRet' : '') +
      " ,reconhecimentoVoz " +
      " FROM " + db.prefixo + tabela + " as IVR";
    if (placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ') {
      stmt += " JOIN CPF_CHAMADA AS CC" +
        "   ON CC.CodUCIDIVR = IVR.CodUCIDIVR AND CC.CodAplicacao = IVR.CodAplicacao";
    }
    if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
      stmt += " JOIN CONTRATO_CHAMADA AS CC" +
        "   ON CC.CodUCIDIVR = IVR.CodUCIDIVR AND CC.CodAplicacao = IVR.CodAplicacao";
    }
    stmt += " WHERE 1 = 1";

    if (placeholder === "") {
      stmt += "   AND IVR.DataHora_Inicio >= '" + formataDataHora($scope.periodo.inicio) + "'" +
        "   AND IVR.DataHora_Inicio <= '" + formataDataHora($scope.periodo.fim) + "'";
    }

    if (filtro_aplicacoes.map === undefined) {
      filtro_aplicacoes = [filtro_aplicacoes];
    }
    stmt += restringe_consulta("IVR.CodAplicacao", filtro_aplicacoes, true);


    // Testar se filtra por chamador ou cliente tratado
    if (filtro_chamadores.length > 0) {
      var campoTelefone = "";
      var nonoDigito = false;

      if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
        campoTelefone = "NumAni";
        nonoDigito = true;
      } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
        campoTelefone = "ClienteTratado";
        nonoDigito = true;
      } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'CPF/CNPJ') {
        campoTelefone = "CC.CPF";
      } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Contrato') {
        campoTelefone = "CC.CONTRATO";
      } else if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Protocolo') {
        campoTelefone = "Protocolo";
      }

      if (filtro_chamadores[0].indexOf("%") !== -1) {
        stmt += restringe_consulta_like(campoTelefone, filtro_chamadores);
      } else {
        // Tratar nono dígito
        if (nonoDigito && (filtro_chamadores[0].length === 11 || filtro_chamadores[0].length === 10)) {
          if (filtro_chamadores[0].length === 10) {
            var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2), filtro_chamadores[0].substring(0, 2) + 9);
          }
          if (filtro_chamadores[0].length === 11) {
            var newNum = filtro_chamadores[0].replace(filtro_chamadores[0].substring(0, 2) + 9, filtro_chamadores[0].substring(0, 2));
          }
          filtro_chamadores.push(newNum);
        }
        stmt += restringe_consulta(campoTelefone, filtro_chamadores, true);
      }
    }

    stmt += " order by dataHora_Inicio, numANI";

    stmtOriginal = stmt;
    dataHoraPesquisa = formataDataHora(new Date());

    if (placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' || placeholder && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado') {
      var tipoTel = $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? "NumAni" : "ClienteTratado";
      var telCallFLow = false;
      for (var i = 0; i < filtro_chamadores.length; i++) {
        if (obtemTelCallFlow(filtro_chamadores[i]) == true) {
          telCallFLow = true;
        }
      }

      var stmtPre = db.use;
      if (telCallFLow) {
        stmtOriginal = stmtOriginal.replace('IVRCDR', 'chamadascallflow') + " ";
      } else {
        stmtOriginal = stmtOriginal + ";INSERT INTO contingenciaEstalo VALUES ('" + dataHoraPesquisa + "','" + loginUsuario + "','" + dominioUsuario + "','" + quoteReplace("select * from ivrcdr where " + tipoTel + " in ('" + filtro_chamadores.join('\'\,\'') + "')") + "','" + win.title + "',null)";
      }
    }

    var stmts = [];

    if (numanis.length > 0 && placeholder === "") {
      var tipoTel = $('#2h').prop("checked") ? "Telefone Chamador" : "Telefone Tratado";
      stmts = arrayNumAnisQuery(stmt, numanis, tipoTel, $scope.extratorArquivoPorData, data_ini, data_fim);
      $('#import2').css('background', '#f5f5f5').selectpicker('refresh');

      for (var i = 0; i < invalidsNumanis.length; i++) {
        $scope.chamadas.push({
          UCID: "",
          data_hora: "",
          data_hora_BR: "",
          data: "",
          chamador: $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' ? invalidsNumanis[i] : "",
          tratado: $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador' ? invalidsNumanis[i] : "",
          cod_aplicacao: "",
          aplicacao: "",
          duracao: "",
          segmento: "",
          encerramento: "",
          estados: "",
          estado_final: "",
          transferid: "",
          ctxid: "",
          reconhecimentoVoz: ""
        });
      }

      numanis = [];
      invalidsNumanis = [];
      $("#browser").val("");
    } else {
      if (placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' || placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
        stmt = "select getdate()/*" + stmtOriginal + "*/";
        stmts = [stmtOriginal, stmt, "select getdate()/*" + stmtOriginal + "*/"];
      } else {
        if (ed.indexOf("0") < 0) {
          if (placeholder === "") {
            //Por 5 minutos
            stmts = arrayCincoMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
          } else {
            stmts.push(stmt);
          }
        } else {
          if (placeholder === "") {
            //Por minuto
            stmts = arrayMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
          } else {
            stmts.push(stmt);
          }
        }
      }
    }

    var executaQuery = "";
    //var aplsPrompts = [];

    if (placeholder !== "") {
      executaQuery = executaQuery3;
      log(stmt);
      disparo = new Date();
    } else {
      if ($scope.filtros.isUltimoED === true) {
        executaQuery = executaQuery2;
        log("Último ED->" + stmt);
        disparo = new Date();
      } else {
        executaQuery = executaQuery1;
        log(stmt);
        disparo = new Date();
      }
    }

    var controle = 0;
    var total = 0;

    var contador = 0;
    var tempoIni = 0;
    var tempoFim = 0;
    var tempoDecorrido = 0;
    var ucids = [];
    var aplsDistincts = [];
    var disparo;
    var tempoPassadoPreCont;
    var chamadasTemp = [];

    // Padrão
    function executaQuery1(columns) {



      contador++;

      var UCID = columns[0].value,
        data_hora = formataDataHora(columns[1].value),
        data_hora_BR = formataDataHoraBR(columns[1].value),
        data = columns[1].value;
      //chamador = columns[2].value === 'Anonymous' ? columns[2].value : +columns[2].value;

      var chamador = columns[2].value,
        tratado = columns[3].value,
        cod_aplicacao = columns[4].value,
        aplicacao = obtemNomeAplicacao(columns[4].value),
        duracao = columns[5].value,
        cod_segmento = columns[6].value,
        segmento = cod_segmento,
        encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
        estados = columns[8].value.split(","),
        estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
        transferid = columns["transferid"].value,
        reconhecimentoVoz = columns["reconhecimentoVoz"].value,
        ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
        codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
        preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
        xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",
        //xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join("") : "")  : "",
        npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
        npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

      if (filtro_falhas != "Falhas") {
        var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
          erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
          erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
          errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
          errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
      }

      if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
        segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
      }


      chamadasTemp.push({
        UCID: UCID,
        data_hora: data_hora,
        data_hora_BR: data_hora_BR,
        data: data,
        chamador: chamador,
        tratado: tratado,
        cod_aplicacao: cod_aplicacao,
        aplicacao: aplicacao,
        duracao: duracao,
        segmento: segmento,
        cod_segmento: cod_segmento,
        encerramento: encerramento,
        reconhecimentoVoz: reconhecimentoVoz,
        estados: estados,
        estado_final: estado_final,
        transferid: transferid,
        ctxid: ctxid,
        codsite: codsite,
        preroteamentoret: preroteamentoret,
        xmlivr: xmlivr,
        npss: npss,
        npsr: npsr,
        erroap: erroap,
        erroctg: erroctg,
        erropmt: erropmt,
        errodig: errodig,
        errovendas: errovendas
      });
    }

    // Último estado
    function executaQuery2(columns) {


      var UCID = columns[0].value,
        data_hora = formataDataHora(columns[1].value),
        data_hora_BR = formataDataHoraBR(columns[1].value),
        data = columns[1].value;
      //chamador = columns[2].value === 'Anonymous' ? columns[2].value : +columns[2].value;
      var chamador = columns[2].value,
        tratado = columns[3].value,
        cod_aplicacao = columns[4].value,
        aplicacao = obtemNomeAplicacao(columns[4].value),
        duracao = columns[5].value,
        cod_segmento = columns[6].value,
        segmento = cod_segmento,
        encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
        estados = columns[8].value.split(","),
        estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
        cod_estado_final = "",
        transferid = columns["transferid"].value,
        reconhecimentoVoz = columns["reconhecimentoVoz"].value,
        ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
        codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
        preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
        xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",
        //xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join(""):"")  : "",
        npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
        npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

      if (filtro_falhas != "Falhas") {
        var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
          erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
          erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
          errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
          errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
      }

      if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
        segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
      }
      // if (cod_aplicacao === "USSD" || cod_aplicacao === "USSD880" || cod_aplicacao === "USSD3000" || cod_aplicacao === "USSD144") segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);

      for (var i = estados.length - 1; i >= 0; i--) {
        var codigo = estados[i],
          estado = obtemEstado(cod_aplicacao, codigo);
        if (estado && estado.exibir) {
          estado_final = estado.nome;
          cod_estado_final = estado.codigo;
          break;
        }
      }

      var filtro_eds = $scope.filtroEDSelect;

      var items = [];
      items = jQuery.grep([cod_estado_final], function (item) {
        return jQuery.inArray(item, filtro_eds) < 0;
      });

      if (items.length === 0) {
        chamadasTemp.push({
          UCID: UCID,
          data_hora: data_hora,
          data_hora_BR: data_hora_BR,
          data: data,
          chamador: chamador,
          tratado: tratado,
          cod_aplicacao: cod_aplicacao,
          aplicacao: aplicacao,
          duracao: duracao,
          segmento: segmento,
          cod_segmento: cod_segmento,
          encerramento: encerramento,
          reconhecimentoVoz: reconhecimentoVoz,
          estados: estados,
          estado_final: estado_final,
          transferid: transferid,
          ctxid: ctxid,
          codsite: codsite,
          preroteamentoret: preroteamentoret,
          xmlivr: xmlivr,
          npss: npss,
          npsr: npsr,
          erroap: erroap,
          erroctg: erroctg,
          erropmt: erropmt,
          errodig: errodig,
          errovendas: errovendas
        });
      }
    }

    // Consulta por telefone, cpf ou protocolo
    function executaQuery3(columns) {



      if (columns["CodUCIDIVR"] !== undefined) {
        var UCID = columns[0].value,
          data_hora = formataDataHora(columns[1].value),
          data_hora_BR = formataDataHoraBR(columns[1].value),
          data = columns[1].value;
        //if(columns[2].value=== 'Anonymous'){ chamador = columns[2].value; }else{ chamador = +columns[2].value; };

        var chamador = columns[2].value,
          tratado = columns[3].value,
          cod_aplicacao = columns[4].value,
          aplicacao = obtemNomeAplicacao(columns[4].value),
          duracao = columns[5].value,
          cod_segmento = columns[6].value,
          segmento = cod_segmento,
          encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
          estados = columns[8].value.split(","),
          estado_final = "", // estados.match(/([^,]+$)/) || [ "" ])[0]
          transferid = columns["transferid"].value,
          reconhecimentoVoz = columns["reconhecimentoVoz"].value,
          ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "",
          codsite = columns["CodSite"] !== undefined ? columns["CodSite"].value : "",
          preroteamentoret = columns["PreRoteamentoRet"] !== undefined ? columns["PreRoteamentoRet"].value : "",
          xmlivr = columns["XMLIVR"] !== undefined ? columns["XMLIVR"].value : "",
          //xmlivr = columns["XMLIVR"] !== undefined ? (columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig) !== null ? columns["XMLIVR"].value.match(/\<ty\>pc\<\/ty\>\<va\>[a-z]{1,6}[_]{0,1}[a-z]{0,6}[0-9]{1,6}\<\/va\>/ig).join(""):"")  : "",
          npss = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[0] : "",
          npsr = columns["respostaPesq"] !== undefined ? columns["respostaPesq"].value.split(';')[1] : "";

        if (filtro_falhas != "Falhas") {
          var erroap = columns["ErroAp"] !== undefined ? columns["ErroAp"].value : "",
            erroctg = columns["ErroCtg"] !== undefined ? columns["ErroCtg"].value : "",
            erropmt = columns["SemPmt"] !== undefined ? columns["SemPmt"].value : "",
            errodig = columns["SemDig"] !== undefined ? columns["SemDig"].value : "",
            errovendas = columns["ErroVendas"] !== undefined ? columns["ErroVendas"].value : "";
        }

        if (cod_aplicacao.match(/^(USSD|PUSH)/)) {
          segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);
        }
        // if (cod_aplicacao === "USSD" || cod_aplicacao === "USSD880" || cod_aplicacao === "USSD3000" || cod_aplicacao === "USSD144") segmento = obtemNomeGrupoSegmentos(cache.segs.indice[cod_segmento].grupo);



        if (ucids.indexOf(UCID) < 0) {
          if (placeholder !== "") {
            for (var i = estados.length - 1; i >= 0; i--) {
              var codigo = estados[i],
                estado = obtemEstado(cod_aplicacao, codigo);
              if (estado && estado.exibir) {
                estado_final = estado.nome;
                break;
              }
            }

            $scope.chamadas.push({
              UCID: UCID,
              data_hora: data_hora,
              data_hora_BR: data_hora_BR,
              data: data,
              chamador: chamador,
              tratado: tratado,
              cod_aplicacao: cod_aplicacao,
              aplicacao: aplicacao,
              duracao: duracao,
              segmento: obtemNomeSegmento(segmento),
              encerramento: encerramento,
              reconhecimentoVoz: reconhecimentoVoz,
              estados: estados,
              estado_final: estado_final,
              transferid: transferid,
              ctxid: ctxid,
              codsite: codsite,
              preroteamentoret: preroteamentoret,
              xmlivr: xmlivr,
              npss: npss,
              npsr: npsr,
              erroap: erroap,
              erroctg: erroctg,
              erropmt: erropmt,
              errodig: errodig,
              errovendas: errovendas
            });

            $scope.$apply(function () {
              $scope.ordenacao = ['data_hora', 'chamador'];

              if (placeholder !== "" || stmts.lenght==1) {
                var arrSort = $scope.chamadas;
                arrSort.sort(function (a, b) {
                  return (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0);
                });
                $scope.chamadas = arrSort;
              }
            });
          } else {
            chamadasTemp.push({
              UCID: UCID,
              data_hora: data_hora,
              data_hora_BR: data_hora_BR,
              data: data,
              chamador: chamador,
              tratado: tratado,
              cod_aplicacao: cod_aplicacao,
              aplicacao: aplicacao,
              duracao: duracao,
              segmento: segmento,
              cod_segmento: cod_segmento,
              encerramento: encerramento,
              reconhecimentoVoz: reconhecimentoVoz,
              estados: estados,
              estado_final: estado_final,
              transferid: transferid,
              ctxid: ctxid,
              codsite: codsite,
              preroteamentoret: preroteamentoret,
              xmlivr: xmlivr,
              npss: npss,
              npsr: npsr,
              erroap: erroap,
              erroctg: erroctg,
              erropmt: erropmt,
              errodig: errodig,
              errovendas: errovendas
            });
          }

          ucids.push(UCID);
        }
      } else {
        if (columns['status'] !== undefined) {
          contador++;
          console.log("TIMER-> " + contador + " " + aplStatusMail);

          if (contador > tempoLimite) {
            controle++;
          }

          if (columns['status'].value !== null && columns['status'].value !== 2) {
            columns['status'].value === 1 ? console.log('uar 1') : console.log('uar 0');
            controle++;
          }
        }
      }
    }



    if (placeholder === "") {
      //Data final do penúltimo array com base na hora final do filtro, último array é uma flag
      //Por hora
      if (ed.indexOf("0") < 0) {
          if(stmts.length<=1) {
            notificate("Utilize o campo texto para buscar um único telefone.", $scope);     
            $(".filtro-chamador").slideUp(300).slideDown(400); 
            killThreads();
            var tempReso = $(".row-fluid").outerHeight() + 10;
      //$('#pag-chamadas .grid').css("margin-top",tempReso);
      //}
            $scope.abortar.apply(this);
            return false;
          }else{
            if (stmts[stmts.length - 2].toString().match(/IVR.DataHora_Inicio <= \'.+\' /g) !== null) {
              stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(/IVR.DataHora_Inicio <= \'.+\' /g, "IVR.DataHora_Inicio <= \'" + formataDataHora(new Date(data_fim)) + "\' ")];
            } else if (stmts[stmts.length - 2].toString().match(/IVR.DataHora_Inicio <= \'.+\'' /g) !== null) {
              stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(/IVR.DataHora_Inicio <= \'.+\'' /g, "IVR.DataHora_Inicio <= \'" + formataDataHora(new Date(data_fim)) + "\' ")];
            }
          }

      } else {
        //Data final do penúltimo array com base na hora final do filtro, último array é uma flag
        //Por minuto
        stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(formataDataHora(precisaoSegundoFim(data_fim)), formataDataHora(data_fim))];
      }
    }

    function executaTestesDeInsercao() {

      //{PROJETO TABS
      var tempReso = $(".row-fluid").outerHeight() + 10;
      //$('#pag-chamadas .grid').css("margin-top",tempReso);
      //}


      for (var c = 0; c < chamadasTemp.length; c++) {
        var inserir = true;

        if ($scope.filtroICSelect.length > 0 && $scope.filtroEDSelect.length == 0) {
          inserir = (chamadasTemp[c].xmlivr === 0);
        } else if ($scope.filtroEDSelect.length > 0  && $scope.filtroICSelect.length == 0) {
          for (var i = 0; i < $scope.filtroEDSelect.length; i++) {
            inserir = (chamadasTemp[c].estados.indexOf(ed[i]) >= 0);
          }
        } else if ($scope.filtroEDSelect.length > 0 && $scope.filtroICSelect.length > 0) {
          for (var i = 0; i < $scope.filtroEDSelect.length; i++) {
            inserir = (chamadasTemp[c].estados.indexOf(ed[i]) >= 0);
          }
          if (inserir) {
            inserir = (chamadasTemp[c].xmlivr === 0);
          }
        }

        // Site
        if (filtro_sites.length > 0 && inserir) {
          inserir = (filtro_sites.indexOf(chamadasTemp[c].codsite) >= 0);
        }

        // Segmento
        if (filtro_segmentos.length > 0 && inserir) {
          inserir = (filtro_segmentos.indexOf(chamadasTemp[c].cod_segmento) >= 0);
        }

        if (filtro_falhas !== "Falhas" && inserir) {
          inserir = (chamadasTemp[c].erroap === 1 && filtro_falhas === "ErroAp" || chamadasTemp[c].erroctg === 1 && filtro_falhas === "ErroCtg" || chamadasTemp[c].erropmt === 1 && filtro_falhas === "SemPmt" || chamadasTemp[c].errodig === 1 && filtro_falhas === "SemDig" || chamadasTemp[c].errovendas === 1 && filtro_falhas === "ErroVendas");
        }

        if (filtro_jsons.length > 0 && inserir) {
          var strs = $('#contextVal').val().split(';');
          for (var i = 0; i < filtro_jsons.length; i++) {
            var s = (strs[i] !== "" && strs[i] !== undefined) ? '"' + strs[i] + '"' : "";
            inserir = ((chamadasTemp[c].preroteamentoret.toUpperCase().replace("|", "PIPE")).match((filtro_jsons[i] + ':' + s).toUpperCase().replace("|", "PIPE")) !== null);
          }
        }

        if ($scope.filtros.nps === true && inserir) {
          inserir = (chamadasTemp[c].npss !== "" || chamadasTemp[c].npsr !== "");
        }

        if (inserir) {
          if ($scope.filtros.isUltimoED === false) {
            for (var i = chamadasTemp[c].estados.length - 1; i >= 0; i--) {
              var codigo = chamadasTemp[c].estados[i],
                estado = obtemEstado(chamadasTemp[c].cod_aplicacao, codigo);
              if (estado && estado.exibir) {
                chamadasTemp[c].estado_final = estado.nome;
                break;
              }
            }
          }

          $scope.chamadas.push({
            UCID: chamadasTemp[c].UCID,
            data_hora: chamadasTemp[c].data_hora,
            data_hora_BR: chamadasTemp[c].data_hora_BR,
            data: chamadasTemp[c].data,
            chamador: chamadasTemp[c].chamador,
            tratado: chamadasTemp[c].tratado,
            cod_aplicacao: chamadasTemp[c].cod_aplicacao,
            aplicacao: chamadasTemp[c].aplicacao,
            duracao: chamadasTemp[c].duracao,
            segmento: obtemNomeSegmento(chamadasTemp[c].segmento),
            encerramento: chamadasTemp[c].encerramento,
            reconhecimentoVoz: chamadasTemp[c].reconhecimentoVoz,
            estados: chamadasTemp[c].estados,
            estado_final: chamadasTemp[c].estado_final,
            transferid: chamadasTemp[c].transferid,
            ctxid: chamadasTemp[c].ctxid,
            npss: chamadasTemp[c].npss,
            npsr: chamadasTemp[c].npsr
          });
        }


      }
    }


    //padrão
    function proxima(s) {


      tempoIni = tempoFim;
      tempoFim = contador;
      tempoDecorrido = tempoFim - tempoIni;

      chamadasTemp = [];

      db.query(s, executaQuery, function (err, num_rows) {

        console.log("Executando query-> " + s + " " + num_rows);

        num_rows !== undefined ? total += num_rows : total += 0;

        var telCallFLow = false;
        for (var i = 0; i < filtro_chamadores.length; i++) {
          if (obtemTelCallFlow(filtro_chamadores[i]) === true) {
            telCallFLow = true;
          }
        }

        if (telCallFLow == false && placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Tratado' || telCallFLow == false && placeholder !== "" && $('.filtro-chamador').attr('placeholder') === 'Telefone Chamador') {
          proxima2("SELECT top 1 login, dominio, query, status from contingenciaestalo where login = '" + loginUsuario + "' and dominio = '" + dominioUsuario + "' and datahora = '" + dataHoraPesquisa + "'");
          var tempoPassado = new Date() - disparo;
          log("Secs(PreCont): " + militosec(tempoPassado) + " " + stmt);
          tempoPassadoPreCont = militosec(tempoPassado);
        } else if (placeholder !== "") {
          retornaStatusQuery($scope.chamadas.length, $scope);
          var tempoPassado = new Date() - disparo;
          log("Secs: " + militosec(tempoPassado) + " " + stmt);
          $btn_gerar.button('reset');
          if ($scope.chamadas.length > 0) {
            $btn_exportar.prop("disabled", false);
          } else {
            $btn_exportar.prop("disabled", "disabled");
          }
        } else {
          $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, stmts.length) + " Pesquisados: " + total + (tempoDecorrido > 15000 ? " A consulta está demorando mais do que o esperado. " : "")).selectpicker('refresh');

          controle++;

          $scope.$apply();

          // Executa dbquery enquanto array de querys menor que length-1
          if (controle < stmts.length - 1) {
            if (placeholder === "") {
              executaTestesDeInsercao();
            }

            proxima(stmts[controle].toString());
          }

          //Evento fim
          if (controle === stmts.length - 1) {
            if (placeholder === "") {
              executaTestesDeInsercao();
            }

            if (err) {
              retornaStatusQuery(undefined, $scope);
              var tempoPassado = new Date() - disparo;
              log("Secs: " + militosec(tempoPassado) + " " + stmt);

            } else {
              retornaStatusQuery($scope.chamadas.length, $scope);
              var tempoPassado = new Date() - disparo;
              log("Secs: " + militosec(tempoPassado) + " " + stmt);
            }
            console.log(err);
            $btn_gerar.button('reset');
            if ($scope.chamadas.length > 0) {
              $btn_exportar.prop("disabled", false);
              $('#grid').css('display', 'block');
            } else {
              $btn_exportar.prop("disabled", "disabled");
              $('#grid').css('display', 'none');
            }
          }
        }
      });
    }


    //base de contingência
    function proxima2(s) {
      db.query(s, executaQuery, function (err, num_rows) {
        console.log("Executando query-> " + s + " " + num_rows);

        num_rows !== undefined ? total += num_rows : total += 0;


        $('.notification .ng-binding').text("Aguardando resposta... (Dados de contigência de outro servidor para meses anteriores)").selectpicker('refresh');


        $scope.$apply();

        // Executa dbquery enquanto array de querys menor que length-1
        if (controle < stmts.length - 1) {
          if (placeholder === "") {
            executaTestesDeInsercao();
          }
          proxima2("/*uar 1*/SELECT top 1 login, dominio, query, status from contingenciaestalo where login = '" + loginUsuario + "' and dominio = '" + dominioUsuario + "' and datahora = '" + dataHoraPesquisa + "'");
        }

        // Evento fim
        if (controle === stmts.length - 1) {
          if (placeholder === "") {
            executaTestesDeInsercao();
          }

          if (err) {
            retornaStatusQuery(undefined, $scope);
            var tempoPassado = new Date() - disparo;
            log("Secs: " + militosec(tempoPassado) + " " + stmt);
          } else {
            if (s.substring(0, 9) !== "/*uar 1*/") {
              retornaStatusQuery($scope.chamadas.length, $scope);
              var tempoPassado = new Date() - disparo;
              log("Secs(Cont): " + (militosec(tempoPassado) - tempoPassadoPreCont) + " " + (stmt !== undefined ? stmt : stmtOriginal));
            }

            console.log("thurai-> " + contador);

            if (contador > tempoLimite && aplStatusMail) {

              var stmt = db.use + " insert into emails values(GETDATE(),'" + loginUsuario + "','" + dominioUsuario + "','Consulta ao " + $('.filtro-chamador').attr('placeholder') + " " + $('.filtro-chamador').val() + " está demorando mais do que o esperado.','')";
              setTimeout(function () {
                executaThreadBasico('extratorDiaADia2', stringParaExtrator(stmt, []), function (dado) {
              });
                $('.notification .ng-binding').text($('.notification .ng-binding').text() + " Não foi possível trazer dados do banco de contingência.").selectpicker('refresh');
              }, 5000);
              aplStatusMail = false;
            }

            if (stmts[controle].toString().substring(0, 16) === "select getdate()") {
              controle = 0;
              total = 0;
              stmts = [stmtOriginal.substring(0, stmtOriginal.indexOf('INSERT'))];
              proxima2(stmtOriginal.substring(0, stmtOriginal.indexOf('INSERT')));
              return;
            }
          }

          console.log(err);
          $btn_gerar.button('reset');
          if ($scope.chamadas.length > 0) {
            $btn_exportar.prop("disabled", false);
            $('#grid').css('display', 'block');
          } else {
            $btn_exportar.prop("disabled", "disabled");
            $('#grid').css('display', 'none');
          }
        }
      });
    }

    // Disparo inicial
    proxima(stmts[0].toString());

  };

  // Consulta o log da chamada
  $scope.consultaCallLog = function (chamada) {
    // Se a chamada acabou de ser consultada (incluindo o log original), não há nada a fazer
    if (chamada === $scope.chamada_atual && $scope.callLog !== "" && $scope.logxml !== "") return true;

    var tabela = "CallLog_";
    if (unique2(cache.apls.map(function (a) {
        if (a.nvp) return a.codigo;
      })).indexOf(chamada.cod_aplicacao) >= 0) {
      tabela = "CallLogNVP_";
    }

    if (chamada.cod_aplicacao === "MENUOI") {
      tabela = "CallLogM4U_";
    }

    if (chamada.cod_aplicacao.match('TELAUNICA')) {
      tabela = "CallLogTelaUnica_";
    }
	
    /*if (chamada.cod_aplicacao.match('S2S')) {
      tabela = "CallLogS2S_";
    }*/
    
	if (chamada.cod_aplicacao.match('S2S')) {
      tabela = "CruzamentoS2S";
    }
    
    $scope.callLog = '';
    $scope.$apply();
    
    var verificaLogs = "sp_spaceused " + tabela;
    if(tabela != "CruzamentoS2S"){
       verificaLogs+= chamada.data_hora.substr(5, 2);
    }
    mssqlQueryTedious(verificaLogs, function (err, retorno) {
      if (err) console.log(err)
      // console.log('b')
      // console.log(retorno)
      // console.log(retorno[0][1])
      if(retorno[0][1].value == 0) {
        var mesString = formataDataMesString(chamada.data_hora);
        $scope.callLog = 'Não há dados de log original de ' + mesString + ' no banco de dados.'
        $scope.$apply();
        // $('pre.callLogValue').html('Não há dados de <b>Log Original</b> de ' + mesString + ' no banco de dados.');
      } else {
        if(tabela === "CruzamentoS2S" || tabela === "CallLogTelaUnica_") {
                var stmt = "SELECT TOP 1 "+(tabela === "CruzamentoS2S" ? "conteudoFinal" : "ConteudoOriginal")+" AS CONTEUDOORIGINAL " 
                + " FROM "+(tabela === "CallLogTelaUnica_" ? "CallLogTelaUnica_"+chamada.data_hora.substr(5, 2) : tabela)+""
                + " WHERE "
                + " CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "' "
                + (tabela === "CruzamentoS2S" ? "OR CodUCIDOriginal = '"+chamada.UCID+"'" : "OR CodUCID = '"+ chamada.ctxid +"'");
                
                console.log("Chamada: ");
                console.log(chamada);
                /*OR"
                + " "+(tabela === "CruzamentoS2S" ? "CodUCIDOriginal" : "CodUCID")
                + " = '" + chamada.UCID + "'";           */           
        }else{
                var stmt = "SELECT TOP 1 Comprimido AS ARQUIVO "
                + "FROM "+ tabela + ""+ chamada.data_hora.substr(5, 2)
                + " WHERE CodUCID = '" + chamada.UCID + "'"
                + "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'" 
                + " ORDER BY LEN(CodUCID)";
        }
        /*{Versão antiga do STMT}
        var stmt = "SELECT TOP 1 "+(tabela === "CallLogS2S_" ? "conteudoOriginal" : "Comprimido")+" AS "+(tabela === "CallLogS2S_" ? "CONTEUDOORIGINAL" : "ARQUIVO")+" FROM " + tabela + chamada.data_hora.substr(5, 2)
                + " WHERE CodUCID = '" + chamada.UCID + "'"
                + "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'" 
                + " ORDER BY LEN(CodUCID)";
          log(stmt);
          */
          
          console.log(stmt+" Final");

        $scope.chamadaCallLogData = chamada.data;
        $scope.tabelaCallLog = tabela;
        
         $scope.downloadLog = "";
        
        if(tabela === "CruzamentoS2S" || tabela === "CallLogTelaUnica_") {
          
          console.log("Statement: "+stmt);
          
          function salvaLog(columns){
            $scope.downloadLog = columns[0].value;
            //retorna apenas um valor            
          }
          
          db.query(stmt, salvaLog, function (err, num_rows) {  
            if(err) {console.log(err)}            
          });
          
          
        }else{
          executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function(dado) {});
        }

        var teste4 = false;

        var teste3 = setInterval(function() {
          
          
          if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip') || $scope.downloadLog) {
            console.log("teste3 funciionando.");
			  
            clearInterval(teste3);
			  
            if(tabela === "CallLogS2S_"){
				
				$scope.$apply(function() {
					
					$scope.callLog = fs.readFileSync(tempDir3() + chamada.UCID + '_log.zip').toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					//console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip')) {
					  fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');
					  teste4 = false;
					}
				});
				
				
				
			}else if(tabela === "CruzamentoS2S" || tabela === "CallLogTelaUnica_"){
        
				$scope.$apply(function() {
        $scope.callLog = $scope.downloadLog;
        //.toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					//console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if ($scope.downloadLog) {
					  clearInterval(teste3);
					 /* fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');*/
					  teste4 = false;
					}
				});
				
        
      }else{
			  
				
				// Descomprimir o log original
				zlib.unzip(new Buffer(fs.readFileSync(tempDir3() + chamada.UCID + '_log.zip')), function(err, buffer) {
				  $scope.$apply(function() {
					if (err) {
					  clearInterval(teste3);
					  log(err);
					  return;
					}
					//console.log(buffer.toString());
					$scope.callLog = buffer.toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip')) {
					  fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');
					  teste4 = false;
					}
				  });
				});
			
			}
			
			
          }
        }, 500);
      }
    }, function (err, num_rows){});

    function tempoLimiteCarregarLogOriginal() {
      setTimeout(function() {
        if (!fs.existsSync(tempDir3() + chamada.UCID + '_log.zip') && teste4 ) {
          clearInterval(teste3);
          $('#myModalLabel').text("Log demorando para carregar ou indisponível");

        }
      }, 20000);
    }
    return true;
  };

  $scope.infos = [];

  // Consulta e formata o XML da chamada
  $scope.formataXML = function (chamada, check) {
    $('#myModalLabel').text("Log da chamada");


    delete cache.aplicacoes[chamada.cod_aplicacao].prompts;
    delete cache.aplicacoes[chamada.cod_aplicacao].itens_controleOnDemand;


    // Se a chamada acabou de ser consultada, não há nada a fazer
    if (check === undefined) {
      if (chamada === $scope.chamada_atual && logCarregado) return true;
    }

    // Limpar o log original da chamada consultada anteriormente
    $scope.chamada_atual = chamada;
    $scope.callLog = "";
    $scope.ocultar.log_original = true;
    $scope.ocultar.log_prerouting = true;
    $scope.ocultar.log_preroutingret = true;
    $scope.ocultar.log_formatado = false;
    $scope.logxml = "";

    // Buscar o log em XML
    var stmt = "SELECT XMLIVR,PREROTEAMENTO,PREROTEAMENTORET,RECONHECIMENTOVOZ FROM IVRCDR WHERE CodUCIDIVR = '" + chamada.UCID + "'";
    log(stmt);

    var xml = "",
      routing = "",
      routingRet = "",
      recVoz = "";


    executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
      // console.log(dado)
    }, undefined, '' + chamada.UCID + '_temp.txt');
    var teste = setInterval(function () {
      if (fs.existsSync(tempDir3() + chamada.UCID + '_temp.txt')) {
        clearInterval(teste);
        var dados_temp = fs.readFileSync(tempDir3() + chamada.UCID + '_temp.txt', 'UTF-8');
        dados_temp = dados_temp.split("\t");


        xml = dados_temp[0];
        routing = dados_temp[1];
        routingRet = dados_temp[2];
        parseXMLPrompts(dados_temp[0], chamada);
        //parseXMLICs(dados_temp[0], chamada);
        recVoz = dados_temp[3];

      }
    }, 500);


    var teste2 = setInterval(function () {


      /*if (cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') &&
      	cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controleOnDemand') &&
      	cache.aplicacoes[chamada.cod_aplicacao].itens_controleOnDemand['indice'] !== undefined){*/
      if (cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') &&
        cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controle') &&
        cache.aplicacoes[chamada.cod_aplicacao].itens_controle['indice'] !== undefined) {
        clearInterval(teste2);

        $('div.modal-header h3').text("Log da chamada");
        $scope.$apply(function () {

          // Formatar o log em XML
          if ($scope.estadoEnx === true) {
            $scope.logxml = parseXML(RemoverEDsExcedentes(xml), chamada, $scope.itemEnx, $scope.itemEnxAll);
          } else {

            $scope.logxml = parseXML(xml, chamada, $scope.itemEnx, $scope.itemEnxAll);
          }

          $scope.prerouting = obterRouting(routing);
          $scope.preroutingret = obterRouting(routingRet);

          if (fs.existsSync(tempDir3() + chamada.UCID + '_temp.txt')) {
            fs.unlinkSync(tempDir3() + chamada.UCID + '_temp.txt');
          }

        });
      } else {
        $('div.modal-header h3').text("Carregando...");
      }
    }, 500);



    return true;
  };

  function obterRouting(valor) {
    if (valor === "") {
      return "Não há dados";
    }

    valor = valor.replace("}", "").replace("{", "");
    var obj = valor.split(",");

    for (var i = 0; i < obj.length; i++) {
      var cod = obj[i].substring(0, 2);
      var nome = obtemNomeVarJSon(cod);
      obj[i] = obj[i].replace(cod, nome).toUpperCase();
    }

    var resultado = obj.sort().toString();
    while (resultado.indexOf(",") > 0) {
      resultado = resultado.replace(",", "\n");
    }

    return resultado;
  }

  function obterRecVoz(valor) {
    if (valor === "" || valor == null) {
      return "";
    }
    if (valor.indexOf(",") == -1) return valor;
    valor = valor.replace("}", "").replace("{", "");
    //valor= valor.replace("\"", "");

    valor = valor.replace("el\":", "Elocução:".bold());
    valor = valor.replace("re\":", "Resposta:".bold());
    valor = valor.replace("rc\":", "Reconhecimento:".bold());
    valor = valor.replace("du\":", "Duração:".bold());
    valor = valor.replace("sc\":", "Score:".bold());
    var obj = valor.split(",");
    var num = obj[0].replace("co\":", "");
    num = parseFloat(num.replace("\"", ""));
    obj[0] = obj[0].replace("co\":", "Confiança:".bold());
    var perc = num * 100;
    obj[0] = obj[0].replace(num, perc) + '%';
    var resultado = obj[1].toString() + "\n" + obj[2].toString() + "\n" + obj[0].toString() + '"';
    var i;
    for (i = 3; i < obj.length; i++) {
      resultado = resultado + "\n" + obj[i].toString();
    }

    //alert(resultado)
    return resultado;
  }

  // Exporta planilha XLSX
  $scope.exportaXLSX = function () {
    var $btn_exportar = $(this);
    $btn_exportar.button('loading');

    if ($scope.chamadas.length > 50000) {
      if (fs.existsSync(tempDir3() + 'tChamadasMore.xlsm')) {
        fs.unlinkSync(tempDir3() + 'tChamadasMore.xlsm');
      }

      // TEMPLATE
      var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo" +
        " FROM " + db.prefixo + "ArquivoRelatorios" +
        " WHERE NomeRelatorio='tChamadasMore'";
      log(stmt);
      db.query(stmt, function (columns) {
        var dataAtualizacao = columns[0].value,
          nomeRelatorio = columns[1].value,
          arquivo = columns[2].value;

        var milis = new Date();
        var baseFile = 'tChamadasMore.xlsm';
        var buffer = toBuffer(toArrayBuffer(arquivo));
        fs.writeFileSync(tempDir3() + baseFile, buffer, 'binary');
      }, function (err, num_rows) {

        userLog(stmt, "Consulta de Chamadas", 2, err);
        if (fs.existsSync(tempDir3() + 'dados_chamadas.txt')) {
          fs.unlinkSync(tempDir3() + 'dados_chamadas.txt');
        }
        var i = 0;
        $scope.chamadas.forEach(function (v) {
          fs.appendFileSync(tempDir3() + 'dados_chamadas.txt', 
		  v.data_hora_BR + '\t' 
		  + v.chamador + '\t' 
		  + v.tratado + '\t' 
		  + v.aplicacao + '\t' 
		  + v.duracao + '\t' 
		  + v.segmento + '\t' 
		  + v.encerramento + '\t' 
		  + v.estado_final + '\t' 
		  + (v.transferid !== "" ? v.transferid : "NA") + '\t' 
		  + (v.ctxid !== "" ? v.ctxid : "NA") + '\t' 
		  + (v.npss !== "" ? v.npss : "NA") + '\t' 
		  + (v.npsr !== "" ? v.npsr : "NA") + '\t' + '\t\n');

          i++;
          if (i === $scope.chamadas.length) {
            childProcess.exec(tempDir3() + 'tChamadasMore.xlsm');
          }
        });

        setTimeout(function () {
          $btn_exportar.button('reset');
        }, 500);
      });
    } else {
      // TEMPLATE
      var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo" +
        " FROM " + db.prefixo + "ArquivoRelatorios" +
        " WHERE NomeRelatorio='tChamadas'";
      log(stmt);
      db.query(stmt, function (columns) {
        var dataAtualizacao = columns[0].value,
          nomeRelatorio = columns[1].value,
          arquivo = columns[2].value;

        var milis = new Date();
        var baseFile = 'tChamadas.xlsx';
        var buffer = toBuffer(toArrayBuffer(arquivo));
        fs.writeFileSync(baseFile, buffer, 'binary');

        // teste
        //var milis = new Date();
        //var baseFile = 'templates/tChamadas.xlsx';

        var file = 'detalhamentoChamadas_' + formataDataHoraMilis(milis) + '.xlsx';

        var newData;

        fs.readFile(baseFile, function (err, data) {
          // Create a template
          var t = new XlsxTemplate(data);

          // Perform substitution
          t.substitute(1, {
            filtros: $scope.filtros_usados,
            planDados: $scope.chamadas
          });

          // Get binary data
          newData = t.generate();

          if (!fs.existsSync(file)) {
            fs.writeFileSync(file, newData, 'binary');
            childProcess.exec(file);
          }
        });

        setTimeout(function () {
          $btn_exportar.button('reset');
        }, 500);
      }, function (err, num_rows) {
        userLog(stmt, "Consulta de ArquivoRelatorios ", 2, err)
        //?
      });
    }
  };

  // Autenticação
  $scope.montaRotas = function () {
    var forms = [];

    var stmt = db.use + "SELECT DISTINCT Formulario FROM PermissoesUsuariosEstatura where LoginUsuario = '" + $scope.login + "' AND UPPER(Dominio) = '" + $scope.dominio + "'";
    log(stmt);
    db.query(stmt, function (columns) {
      var form = columns[0].value;
      forms.push(form);
    }, function (err, num_rows) {
      if (num_rows > 0) {
        //userLog(stmt, "Login Autorizado",  2, err);
        var text = fs.readFileSync(tempDir() + "infoCache", "utf-8");
        text = JSON.parse(text);
        text.cache = $scope.filtros.arq_cache;
        fs.writeFileSync(tempDir() + "infoCache", JSON.stringify(text));

        var bkpRotas = rotas;
        rotas = [];
        /*//chamadas
        rotas.push(bkpRotas[0]);*/
        $globals.logado = true;
        for (var i = 0; i < forms.length; i++) {
          bkpRotas.forEach(function (rota) {

            if (rota.form === forms[i]) {
              var indice = bkpRotas.indexOf(rota);
              //rota.templateUrl = rota.templateUrl.replace(".html","2.html");
              rotas.push(bkpRotas[indice]);
            }

            /* var htmlTemp = unique2(rotasInit.map(function(r){if(r.url.indexOf(rota.url)>=0){ return r.templateUrl }else{ return undefined} })).toString();
            if (htmlTemp !== undefined) {
              rota.templateUrl = base + htmlTemp;
            }*/
          });
        }

        // reordenação por ordem
        rotas.sort(function (primeiro, segundo) {
          // Ordenar somente por ordem
          return primeiro.ordem - segundo.ordem;
        });

        // cookie
        //fs.writeFileSync("cookie", $scope.login+";"+$scope.dominio);

        var partsPath = window.location.pathname.split("/");
        var part = partsPath[partsPath.length - 1];
        window.location.href = part + window.location.hash + '/';
      }

      $('.login form label').text("");
      /*setTimeout(function () {
		if (err) {
		  retornaMsgErro(err);
		}
	  });*/

      if (num_rows === 0) {
        //retornaMsgErro("Sem permissão, <br/> entre em contato com suporte@versatec.com.br", true);
        //$(".msg").html("<br/> Usuário não autorizado,<br/>Solicite acesso para suporte@versatec.com.br.")
        $(".alertaVersao").show();
        $(".alertaVersao").html("<br/>Usuário não autorizado.<br/>Solicite acesso para suporte@versatec.com.br.")
        $("body").css("background-color", "#FFF");
        window.location = '#/solicitaUsuario'; //Aguardando Liberação dos Emails Matheus 06/2017
        $('.btn-logar').attr('disabled', 'disabled');
        userLog(stmt, "Login Recusado", 2, err);
        //$(".modal-register").show();
      }

    });
    //return true;
  };



  //Plugin jQuery
  jQuery.extend({
    highlight: function (node, re, nodeName, className) {
      if (node.nodeType === 3) {
        var match = node.data.match(re);
        if (match) {
          var highlight = document.createElement(nodeName || 'span');
          highlight.className = className || 'highlight';
          var wordNode = node.splitText(match.index);
          wordNode.splitText(match[0].length);
          var wordClone = wordNode.cloneNode(true);
          highlight.appendChild(wordClone);
          wordNode.parentNode.replaceChild(highlight, wordNode);
          return 1; //skip added node in parent
        }
      } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
        !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
        !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
        for (var i = 0; i < node.childNodes.length; i++) {
          i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
        }
      }
      return 0;
    }
  });

  jQuery.fn.unhighlight = function (options) {
    var settings = {
      className: 'highlight',
      element: 'span'
    };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function () {
      var parent = this.parentNode;
      parent.replaceChild(this.firstChild, this);
      parent.normalize();
    }).end();
  };

  jQuery.fn.highlight = function (words, options) {
    var settings = {
      className: 'highlight',
      element: 'span',
      caseSensitive: false,
      wordsOnly: false
    };
    jQuery.extend(settings, options);

    if (words.constructor === String) {
      words = [words];
    }
    words = jQuery.grep(words, function (word, i) {
      return word != '';
    });
    words = jQuery.map(words, function (word, i) {
      return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) {
      return this;
    };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
      pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function () {
      jQuery.highlight(this, re, settings.element, settings.className);
    });
  };

  //Fim do Plugin

  /* Função de busca */
  function searchAndHighlight(searchTerm, selector, resultElement) {
    if (searchTerm && !(searchTerm === " ")) {
      var selector = selector;


      if (selector == "" || !selector) {
        if ($("div.log-formatado").css('display') == 'block') {
          selector = "div.log-formatado";
        }
        if ($("div.log-original").css('display') == 'block') {
          selector = "div.log-original";
        }
        if ($("div.log-prerouting").css('display') == 'block') {
          selector = "div.log-prerouting";
        }
        if ($("div.log-preroutingret").css('display') == 'block') {
          selector = "div.log-preroutingret";
        }
      }
      //searchTerm = searchTerm.trim();

      $(selector).highlight(searchTerm, {
        className: 'match'
      });
      var matches = $('.match');
      //console.log("Matches: "+matches.length+" searchTerm "+searchTerm);
      // Funções dependentes de resultados a seguir
      if (matches != null && matches.length > 0) {

        try {
          resultElement.text(matches.length + ' encontrados'); // Conta a quantidade de elementos encontrados
        } catch (er) {
          console.log("Erro na função de pesquisa " + er);
        }

        // Marca o primeiro elemento MATCH como highlighted ao efetuar uma pesquisa
        $('.match:first').addClass('highlighted');

        var i = 0;

        // Função responsável por "scrollar" e marcar o próximo elemento match como highlighted
        $('.searchtool_next').on('click', function () {

          i++;

          if (i >= $('.match').length) i = 0;

          //Remove o atual e marca o próximo elemento match
          $('.match').removeClass('highlighted');
          $('.match').eq(i).addClass('highlighted');

          // Cálculo final até a posição do elemento marcado highlighted
          var container = $('.modal-body'),
            scrollTo = $('.match').eq(i);
          var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

          container.scrollTop(
            wheretogo - 125
          );
          $('.highlighted').focus();

          // Fim do cálculo e scroll
        });

        // Função para "Scrollar" e "Marcar" o anterior
        $('.searchtool_prev').on('click', function () {

          i--;
          if (i < 0) i = $('.match').length - 1;
          //Remove o atual e marca o elemento anterior
          $('.match').removeClass('highlighted');
          $('.match').eq(i).addClass('highlighted');
          //Cálculo final da posição para o scroll
          var container = $('.modal-body'),
            scrollTo = $('.match').eq(i);
          var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

          container.scrollTop(
            wheretogo - 125
          );

          $('.highlighted').focus();

        });


        if ($('.highlighted:first').length) { //if match found, scroll to where the first one appears
          $(window).scrollTop($('.highlighted:first').position().top);
        }
        return true;
      }
    }
    return false;
  }

  // Função responsável por checar atualizações no INPUT text
  $('.searchtool_text').each(function () {

    // ResultElement é o elemento onde a quantidade de resultados é exibida
    var resultElement = $('.search_results');
    var searchElement = $(this);

    search();
    // Save current value of element
    $(this).data('oldVal', $(this));

    // Look for changes in the value
    $(this).bind("propertychange keyup input paste", function (event) {

      // If value has changed...
      if ($(this).data('oldVal') != $(this).val()) {
        // Updated stored value
        $(this).data('oldVal', $(this).val());

        $("div .log-original").unhighlight({
          className: 'match'
        });
        $("div .log-formatado").unhighlight({
          className: 'match'
        });
        $("div .log-prerouting").unhighlight({
          className: 'match'
        });
        $("div .log-preroutingret").unhighlight({
          className: 'match'
        });
        if (!searchAndHighlight($(this).val(), "", resultElement)) {
          resultElement.html('Sem resultados.');
        }
      }
    });
  }).delay(300);

  function clearSearchTool(searchElement, resultElement) {

    // Função responsável por resetar a função de pesquisa
    //console.log("Limpou a ferramenta de pesquisa de logs");

    if (resultElement == "" || resultElement == null) {
      resultElement = $('.search_results');
    }
    $(resultElement).html('');
    $(searchElement).val('');
    $(searchElement).attr("placeholder", "Buscar...");
    $("div .log-original").unhighlight({
      className: 'match'
    });
    $("div .log-formatado").unhighlight({
      className: 'match'
    });
    $("div .log-prerouting").unhighlight({
      className: 'match'
    });
    $("div .log-preroutingret").unhighlight({
      className: 'match'
    });
  };

  $(".changeSearchLog").click(function () {
    var resultElement = $('.search_results');
    clearSearchTool("", resultElement);
    //console.log("Chamou a função para limpar");
  });

  $("#modal-log").on('hidden', function () {
    var resultElement = $('.search_results');
    var searchElement = $('.searchtool_text');
    clearSearchTool(searchElement, resultElement);
    //console.log("Chamou a função para limpar");
  });

  /*Fim da função de busca*/

  //{ EDs Input Controle
  var modificaAlertaEDs = function (x) {
    if (x == "Elemento repetido ou número máximo de EDs acumulados") {
      $('.notification .ng-binding').text(x).selectpicker('refresh');
      setTimeout(function () {
        $('.notification .ng-binding').text("").selectpicker('refresh');
      }, 3000);
    } else {
      $("#filtro-ed").attr("placeholder", x);
    }
  };
  var verificaRepetidoEDs = function (x, item) {
    console.log(x);
    if (x.length == 3) return true;
    for (var z in x) {
      try {
        if (x[z].localeCompare(item) == 0) return true;
      } catch (err) {
        console.log(err);
      }
      try {
        if (x[z + 1].localeCompare(item) == 0) return true;
      } catch (err) {
        console.log(err);
      }
    }
    return false;
  };
  //var $myTextarea = $('#demo');
  //$myTextarea.css("background-color","grey").css("color","white");
  var arr2 = [];
  $('#filtro-ed').typeahead({
    source: $scope.options5opt,
    minLength: 3,
    items: 20,
    updater: function (item) {
      var temp;
      if (arr2.length < 1) {
        temp = item.split(" ");
        arr2.push(temp[0]);
        $scope.filtroEDSelect = arr2;
        console.log($scope.filtroEDSelect);
        modificaAlertaEDs(arr2[0]);
        return '';
      } else {
        temp = item.split(" ");
        if (verificaRepetidoEDs(arr2, temp[0])) {
          modificaAlertaEDs("Elemento repetido ou número máximo de EDs acumulados");
          setTimeout(function () {
            if (arr2.length == 3) {
              modificaAlertaEDs(arr2[0] + ", " + arr2[1] + ", " + arr2[2]);
            }
            if (arr2.length == 2) {
              modificaAlertaEDs(arr2[0] + ", " + arr2[1]);
            }
            if (arr2.length == 1) {
              modificaAlertaEDs(arr2[0]);
            }
          }, 3000);
        } else {
          temp = item.split(" ");
          arr2.push(temp[0]);
          //$myTextarea.css("color","white");
          if (arr2.length == 3) {
            modificaAlertaEDs(arr2[0] + ", " + arr2[1] + ", " + arr2[2]);
          }
          if (arr2.length == 2) {
            modificaAlertaEDs(arr2[0] + ", " + arr2[1]);
          }
          if (arr2.length == 1) {
            modificaAlertaEDs(arr2[0]);
          }
          $scope.filtroEDSelect = arr2;
          console.log($scope.filtroEDSelect);
        }
        //verificaRepetido($myTextarea,item)? $('.notification .ng-binding').text("Elemento repetido ou máximo de EDs alcançados...").selectpicker('refresh'); : $myTextarea.append(',',item);
        return '';
      }
      $scope.filtroEDSelect = arr2;
      console.log($scope.filtroEDSelect);
    }
  });
  //} Fim EDs Input Controle

  //{ ICs Input Controle
  var modificaAlertaICs = function (x) {
    if (x == "Elemento repetido ou número máximo de ICs acumulados") {
      $('.notification .ng-binding').text(x).selectpicker('refresh');
      setTimeout(function () {
        $('.notification .ng-binding').text("").selectpicker('refresh');
      }, 3000);
    } else {
      $("#filtro-ic").attr("placeholder", x);
    }

  };
  var verificaRepetidoICs = function (x, item) {
    console.log(x);
    if (x.length == 3) return true;
    for (var z in x) {
      try {
        if (x[z].localeCompare(item) == 0) return true;
      } catch (err) {
        console.log(err);
      }
      try {
        if (x[z + 1].localeCompare(item) == 0) return true;
      } catch (err) {
        console.log(err);
      }
    }
    return false;
  };
  
  var vetorFiltroIc = [];
 

  function search(selector) {
    var resultElement = $('.search_results');
    var searchElement = $(".searchtool_text");
    clearSearchTool("", resultElement);

    //console.log("Search!!!!!!");

    if (selector == "" || !selector) {
      if ($("div.log-formatado").css('display') == 'block') {
        selector = "div.log-formatado";
        //console.log("Div log formatado ativa");
      }
      if ($("div.log-original").css('display') == 'block') {
        selector = "div.log-original";
        //console.log("Div log original ativa");
      }
      if ($("div.log-prerouting").css('display') == 'block') {
        selector = "div.log-prerouting";
        //console.log("Div log prerouting ativa");
      }
      if ($("div.log-preroutingret").css('display') == 'block') {
        selector = "div.log-preroutingret";
        //console.log("Div log preroutingret ativa");
      }
    }

    if (searchElement.val() != "" && searchElement.val() != undefined && searchElement.val() != null) {
      //console.log("Função SEARCH realizando pesquisa por: "+searchElement.val()+" em "+selector);
      try {
        /*if (!searchAndHighlight(searchElement, selector, resultElement)) {
        	console.log("Não Encontrados");
        }*/


        $(selector).highlight(searchElement.val(), {
          className: 'match'
        });
        $('.match:first').addClass('highlighted');
        var matches = $(".match");
        resultElement.text(matches.length + ' encontrados');

      } catch (err) {
        console.log("Error: " + err);
      }
    }
  }



  /*setInterval(function(){
  	search();
  }, 1000);*/
  //} Fim ICs Input Controle

  // { Função timeoutsearch
  /// Paramêtro DIV é o elemento jQuery a ser inserido (ex: .log-formatado)
  function waitForSearch(div) {
    if ($(div).length && $(div).is(":visible")) {
      setTimeout(function () {
        //console.log("Preparando Log Original...");
        if ($('div.modal-header h3').text() == "Carregando...") {
          waitForSearch(div);
        } else {
          search(div);
        }
      }, 1000);
    } else {
      //console.log("Div requisitada: "+div+" não existe ou está invisível.");
    }
  }
  //} fim da timeoutsearch

  $scope.$watch('callLog', function (newValue, oldValue) {
    if (newValue) {
      //console.log("NewValue: "+newValue.length);
    }
    waitForSearch(".log-original");
    //console.log("Realizou a pesquisa devido ao CallLog :"+$(".searchtool_text").val());
  });



  $scope.$watch('logxml', function (newValue, oldValue) {
    if (newValue) {
      //console.log("NewValue: "+newValue.length);
    }

    //console.log("Realizou a pesquisa devido ao CallLog :"+$(".searchtool_text").val());
    waitForSearch(".log-formatado");
  });


  $scope.$watch('ocultar.log_formatado', function (newValue, oldValue) {
    //console.log("Ocultar LOG FORMATADO?: "+$scope.ocultar.log_formatado);
    //console.log("Old Value: "+oldValue+" new value: "+newValue);
    if (newValue == false) {
      search(".log-formatado");
      //console.log("Realizou a pesquisa em Log Formatado!");
    }
  });

  $scope.$watch('ocultar.log_original', function (newValue, oldValue) {
    //console.log("Ocultar LOG ORIGINAL?: "+$scope.ocultar.log_original);
    //console.log("Old Value: "+oldValue+" new value: "+newValue);
    if (newValue == false) {
      search(".log-original");
      //console.log("Realizou a pesquisa em LOG ORIGINAL por :"+$(".searchtool_text").val());
    }
  });

  $scope.$watch('ocultar.log_prerouting', function (newValue, oldValue) {
    //console.log("Ocultar LOG prerouting?: "+$scope.ocultar.log_prerouting);
    //console.log("Old Value: "+oldValue+" new value: "+newValue);
    waitForSearch(".log-prerouting");
  });


  $scope.$watch('ocultar.log_preroutingret', function (newValue, oldValue) {
    //console.log("Ocultar LOG preroutingret?: "+$scope.ocultar.log_preroutingret);
    //console.log("Old Value: "+oldValue+" new value: "+newValue);
    waitForSearch(".log-preroutingret");
  });




  $scope.abreAjuda = function (link) {
    abreAjuda(link);
  }

  $scope.addItem = function (data) {
    console.log("AddITEM codigo: " + data.codigo + " Desc: " + data.desc);
    iccode = data.codigo;
    if ($scope.outputICS.indexOf(iccode) == -1) {
      console.log("Botao adicionado para: " + iccode);

      icbutton = '<button type="button" ' +
        'class="helperButton ng-binding ng-scope ' + iccode + '" ' +
        'ng-click="removeItem(\'' + iccode + '\');" ' +
        ' >×&nbsp;&nbsp; ' + iccode + '</button>';
      icelem = $(icbutton);
      targetElement = $(".helperButton").parent();

      angular.element(targetElement).injector().invoke(function ($compile) {
        var escope = angular.element(targetElement).scope();
        targetElement.append($compile(icelem)($scope));
        // Finally, refresh the watch expressions in the new element
        $scope.$apply();
      });

      function logArrayElements(element, index, array) {
        console.log("a[" + index + "] = Codigo: " + element.codigo + " Desc: " + element.desc);
      }
      $scope.outputICS.forEach(logArrayElements);

    } else {
      $("." + iccode + "").remove();
      console.log("Tentou remover: ." + iccode);
      //$scope.outputICS.remove(data.codigo);
    }
    console.log("Output ICS: " + $scope.outputICS);
    console.log("Output ICS Length: " + $scope.outputICS.length);
  }

  $scope.removeItem = function (codigo) {
    //function removeItem(codigo){
    console.log("Entrou na função de remover!!!!!1111");
    if ($scope.outputICS.indexOf(codigo) == -1 && $("." + codigo).length <= 0) {
      function logArrayElements(element, index, array) {
        console.log("Index: " + index + " IC Codigo: " + element.codigo + " Desc: " + element.desc);
      }
      $scope.outputICS.forEach(logArrayElements);

    } else {
      console.log("Código IC a ser removido: " + codigo);
      //$("."+codigo+"").remove();
      function removerIC(value) {
        console.log("Comparando Valor: " + value.codigo + " Com: " + codigo);
        return value.codigo == codigo;
      }

      $scope.outputICS = $scope.outputICS.filter(removerIC);
      //
      //$scope.select("IC", "externalClickListener", codigo);
      $timeout(function () {
        angular.element($(".select" + codigo).parent().parent().parent().parent()).triggerHandler('click');
      }, 0);
    }
  }
  $scope.windowInnerWidth = window.innerWidth;
  window.addEventListener('resize', function(){
    $scope.windowInnerWidth = window.innerWidth;
    $scope.$apply();

  }, true);
    
  
}
CtrlChamadas.$inject = ['$scope', '$globals', '$compile', '$timeout'];if (nodejs) {
	var fs = require('fs'),
		childProcess = require('child_process'); //14/02/2014 Abrir arquivo externo
	var gui = require('nw.gui');
	var http = require('http');
	var https = require('https');
	var zlib = require('zlib');
	var node_require = require;
	var TYPES = require('tedious').TYPES; //15/04/2014
	var cacheEmitter = new(require('events').EventEmitter)(); //19/04/2014
	var Connection = require('tedious').Connection,
		Request = require('tedious').Request,
		zlib = require('zlib'),
		path = require('path'),
		XlsxTemplate = require('xlsx-template'), //15/02/2014 Adicionar dados a um arquivo excel externo
		os = require('os'); //04/04/2014
	var defaultPage = "#pag-chamadas"; //24/02/2014 Tela de splash
	var thrs = [];
	var cacheCount = 0;
	var logCarregado = true; //02/11/2017 Controle adicional para chamada atual consultada
	var cacheEstimado = 50; //24/02/2014 Auxilia progress bar
	var backupIC = ""; //02/11/2017 Consultar tabela de contingência para IC
	if (typeof versao == "undefined") var versao = "";
	var link = "";
	var versaoNodeWebKit = process.versions['node-webkit'];
	var extratorStatus = "";
	var intervaloExtratorStatus;
	var intervaloExtratorStatusAux;
	var linkArq = "";
	var linkArqContent = "";
	var intervaloIndices;
	var conexaoWeb = true;
	var aplAtualizado = true;
	var aplStatusMail = true;
	var backupCacheApls = {};
	var backupCache = {};
	var disparoInicial = "";
	var tipoVersao = base.match("homo") !== null ? "H" : "P";
	var EmpresasLogin = ['Oi', 'Versatil', 'CONTAX', 'BRTCC', 'A5 Solutions', 'Coddera', 'Accenture', 'Everis', 'Nuance'];
	var loginUsuario = process.env.username;
	var dominioUsuario = process.env.userdomain;
	var __dirname = tempDir3();
} //Fim nodejs

var db = new DB();

function highlight(text) {
	inputText = document.getElementById("uar");
	var innerHTML = inputText.innerHTML;
	var index = innerHTML.indexOf(text);
	if (index >= 0) {
		innerHTML = innerHTML.substring(0, index) + "<span class='highlight'>" + innerHTML.substring(index, index + text.length) + "</span>" + innerHTML.substring(index + text.length);
		inputText.innerHTML = innerHTML;
	}
}

function arrumaHtml(txt) {
	txt = txt.replace(new RegExp('&lt;', 'g'), '<');
	txt = txt.replace(new RegExp('&gt;', 'g'), '>');
	txt = txt.replace(new RegExp('&amp;', 'g'), '&');
	txt = txt.replace(new RegExp('&#39;', 'g'), '\'');
	txt = txt.replace(new RegExp('&quot;', 'g'), '"');
	return txt;
}

function militosec(v) {
	return Math.round(v / 1000)
} //12/09/2016

//{02/11/2017 Funções para restringe consultas (sql query)
function restringe_consulta3(campo, valores, quotes) {
	if (valores.length === 0) return "";
	if (quotes) valores = valores.map(function (valor) {
		return "'R" + valor + "'";
	});
	return " AND " + campo + " IN (" + valores.join(",") + ")";
}

function restringe_consulta2(campo, valores, quotes) {
	if (valores.length === 0) return " AND " + campo + " = ''";
	if (quotes) valores = valores.map(function (valor) {
		return "'" + valor + "'";
	});
	return " AND " + campo + " IN (" + valores.join(",") + ")";
}

function restringe_consulta(campo, valores, quotes) {
	if (Estalo.filtros.filtro_regioes.indexOf("ND") >= 0 && campo.substring(3, campo.length).toUpperCase() === "DDD" || Estalo.filtros.filtro_regioes.indexOf("ND") >= 0 && campo.toUpperCase() === "DDD") {
		valores = valores.concat("??")
	}
	if (typeof (valores) === 'string') {
		valores = [valores]
	}
	if (valores.length === 0) return "";
	if (quotes) valores = valores.map(function (valor) {
		return "'" + valor + "'";
	});
	return " AND " + campo + " IN (" + valores.join(",") + ")";
}

function restringe_consultaOper(campo, campo2, valores, quotes) {
	if (typeof (valores) === 'string') {
		valores = [valores]
	}
	if (valores.length === 0) return "";
	if (quotes) valores = valores.map(function (valor) {
		return "'" + valor + "'";
	});
	return " AND (" + campo + " IN (" + valores.join(",") + ") OR " + campo2 + " IN (" + valores.join(",") + "))";
}

function restringe_consulta_like(campo, valores, tipo, ic, valJsons) {
	if (valores.length === 0) return "";
	var likes = "",
		t = "OR",
		pre = "",
		suf = "";
	if (tipo === true) t = "AND";
	if (ic === true) var pre = "<ty>pc</ty><va>",
		suf = "</va>";
	if (ic === "rot") var pre = "",
		suf = ":";

	for (var v = 0; v < valores.length; v++) {
		if (valJsons !== undefined) {
			if (valJsons[v] !== "") {
				valJsons[v] = "\"" + valJsons[v] + "\"";
			}
		}

		if (v === 0) {
			likes += " AND (" + campo + " LIKE '%" + pre + valores[v] + suf + (valJsons !== undefined ? valJsons[v] : "") + "%'";
		} else {
			likes += " " + t + " " + campo + " LIKE '%" + pre + valores[v] + suf + (valJsons !== undefined ? valJsons[v] : "") + "%'";
		}
	}
	likes += ")";
	if (ic === "rot") likes += " and indicdelig in('TRN') ";
	return likes;
}

function restringe_consulta_like2(campo, valores) {
	if (valores.length === 0) return "";
	var likes = "",
		t = "OR";

	for (var v = 0; v < valores.length; v++) {
		if (v === 0) {
			likes += " AND (" + campo + " LIKE '%" + valores[v] + "%'";
		} else {
			likes += " " + t + " " + campo + " LIKE '%" + valores[v] + "%'";
		}
	}
	likes += ")";
	return likes;
}

function restringe_consulta4(campo, valores, tipo) {
	if (valores.length === 0) return "";
	var likes = "",
		t = "OR";
	if (tipo === true) t = "AND";

	for (var v = 0; v < valores.length; v++) {
		if (v === 0) {
			likes += " AND (" + campo + " = '" + valores[v] + "'";
		} else {
			likes += " " + t + " " + campo + " = '" + valores[v] + "'";
		}
	}
	likes += ")";
	return likes;
}

function restringe_consulta_ddd(campo, valores, quotes) {
	var arr = [];
	for (i = 0; i < valores.length; i++) {
		if (valores[i] === "R1") {
			arr1 = ['2', '3', '7', '8', '9']; //região 1
			arr = arr.concat(arr1);
		}
		if (valores[i] === "R2") {
			arr2 = ['4', '5', '6']; //região 2
			arr = arr.concat(arr2);
		}
		if (valores[i] === "R3") {
			arr3 = ['1']; //região 3
			arr = arr.concat(arr3);
		}
	}
	var restricoes = " AND substring(" + campo + ",1,1) in (" + arr.join(",") + ")";
	if (arr.length === 0) {
		restricoes = "";
	};
	return restricoes;
}
//}

//19/03/2014 //27/08/2014
var relsVendas = ['pag-resumo-vendas', 'pag-vendas', 'pag-resumo-por-promo', 'pag-cadastroxrecarga', 'pag-rescadastroxrecarga'];

//{25/05/2015 Informação sobre consolidação
var aplFocus = null;
var delay = function (elem, callback) {
	var timeout = null;
	elem.mouseover(function () {
		// Define o tempo limite para ser um temporizador que invocará retorno após 1s
		timeout = setTimeout(callback, 1000);
		aplFocus = $(this).text();
		if (aplFocus.substring(aplFocus.length - 3) === "NGR") aplFocus = aplFocus.substring(0, aplFocus.length - 3);
	});
	elem.mouseout(function () {
		// Limpa todos os temporizadores definidos para o tempo limite
		clearTimeout(timeout);
		aplFocus = null;
	});
};
//}

//{blacklist e Whitelist
function blacklistWhitelist(apls, idview) {

	//WHITELIST
	var w = cache.aclapls.map(function (acl) {
		if (acl.tipo === 'W') return acl.idview
	}).indexOf(idview);
	var wAll = cache.aclapls.map(function (acl) {
		if (acl.tipo === 'W') return acl.idview
	}).indexOf('*');
	if (w >= 0) {
		var teste = cache.aclapls[w].aplicacoes.split(',');
		apls = cache.apls.filter(function (apl) {
			if (teste.indexOf(apl.codigo) >= 0) {
				return apl
			}
		});
	} else if (wAll >= 0) {
		var teste = cache.aclapls[wAll].aplicacoes.split(',');
		apls = cache.apls.filter(function (apl) {
			if (teste.indexOf(apl.codigo) >= 0) {
				return apl
			}
		});
	}
	//BLACKLIST
	var b = cache.aclapls.map(function (acl) {
		if (acl.tipo === 'B') return acl.idview
	}).indexOf(idview);
	var bAll = -1;
	if (w < 0) bAll = cache.aclapls.map(function (acl) {
		if (acl.tipo === 'B') return acl.idview
	}).indexOf('*');
	if (b >= 0) {
		var teste = cache.aclapls[b].aplicacoes.split(',');
		apls = cache.apls.filter(function (apl) {
			if (teste.indexOf(apl.codigo) < 0) {
				return apl
			}
		});
	} else if (bAll >= 0) {
		var teste = cache.aclapls[bAll].aplicacoes.split(',');
		apls = cache.apls.filter(function (apl) {
			if (teste.indexOf(apl.codigo) < 0) {
				return apl
			}
		});
	}
	return apls;
}
//}

//22/02/2014
function testaLimite(valor) {
	var result;
	if (valor > 0) {
		result = " TOP " + valor + "";
	} else {
		result = " ";
	}
	return result;
}

//22/02/2014
function effectNotification(pagina) {
	/*$('body').css({'background-color':'#fc9a9a'});
	setTimeout(function(){
	$('body').css({'background-color':'white'});
	},1000);*/
}

//{11/07/2014 Simular acesso de usuário
function teste() {
	$('.txtDominio').prop('disabled', false);
	$('.txtLogin').prop('disabled', false);
}
//}

// Menu TreeView deprecated ?
function treeView(view, valor, pagina, dv, menu) {
	var arrSubMenu = [{
			id: '#chamadas',
			valor: 15,
			pagina: 'Chamadas',
			dv: 'dvchamadas'
		},
		{
			id: '#repetidas',
			valor: 35,
			pagina: 'Repetidas',
			dv: 'dvRepetidas'
		},
		{
			id: '#ed',
			valor: 65,
			pagina: 'ED',
			dv: 'dvED'
		},
		{
			id: '#ic',
			valor: 95,
			pagina: 'IC',
			dv: 'dvIC'
		},
		{
			id: '#tid',
			valor: 115,
			pagina: 'TID',
			dv: 'dvTid'
		},
		{
			id: '#vendas',
			valor: 145,
			pagina: 'Vendas',
			dv: 'dvVendas'
		},
		{
			id: '#falhas',
			valor: 165,
			pagina: 'Falhas',
			dv: 'dvFalhas'
		},
		{
			id: '#extratores',
			valor: 195,
			pagina: 'Extratores',
			dv: 'dvExtratores'
		},
		{
			id: '#parametros',
			valor: 225,
			pagina: 'Parametros',
			dv: 'dvParam'
		},
		{
			id: '#admin',
			valor: 255,
			pagina: 'Administrativo',
			dv: 'dvAdmin'
		},
		{
			id: '#monitoracao',
			valor: 275,
			pagina: 'Monitoração',
			dv: 'dvReparo'
		},
		{
			id: '#cradleToGrave',
			valor: 295,
			pagina: 'Cradle to Grave',
			dv: 'dvcontGlobalTransf'
		},
		{
			id: '#uracadastro',
			valor: 325,
			pagina: 'Cadastro e Migração',
			dv: 'dvCadastro'
		}
	];

	if (valor == undefined) {
		arrSubMenu.map(function (valor) {
			var listaM = '#listaM';
			if (menu === undefined) {
				menu = '#menu';
			} else {
				listaM = menu + ' .listaM';
			}
			$(view + " " + valor.id).mousemove(function () {
				$(listaM).remove();
				var res = retornaPaginas(valor.pagina);
				if (res.length > 0 || valor.dv === 'dvAdmin') {
					if (listaM === '#listaM') {
						$(menu).append('<ul id="listaM" class="dropdown-menu" style="width: 310px;margin-left:150px;margin-top:' + valor.valor + 'px"></ul>');
					} else {
						$(menu).append('<ul class="listaM dropdown-menu" style="width: 310px;margin-left:150px;margin-top:' + valor.valor + 'px"></ul>');
					}
				}
				for (i = 0; i < res.length; i++) {
					if (res[i].nome.toUpperCase() !== "AJUDA") {
						$(listaM).append('<li class="itensM" <div class="' + valor.dv + '" ><a class="aMenu" style="text-decoration:none" href="#' + res[i].url + '"</a>' + res[i].nome + '</div></li>');
					}
				}
			});
		});
	} else {
		var listaM = '#listaM';
		if (menu === undefined) {
			menu = '#menu';
		} else {
			listaM = menu + ' .listaM';
		}
		$(view).mousemove(function () {
			$(listaM).remove();
			var res = retornaPaginas(pagina);
			if (res.length > 0 || dv === 'dvAdmin') {
				if (listaM === '#listaM') {
					$(menu).append('<ul id="listaM" class="dropdown-menu" style="width: 310px;margin-left:150px;margin-top:' + valor + 'px"></ul>');
				} else {
					$(menu).append('<ul class="listaM dropdown-menu" style="width: 310px;margin-left:150px;margin-top:' + valor + 'px"></ul>');
				}
			}
			for (i = 0; i < res.length; i++) {
				if (res[i].nome.toUpperCase() !== "AJUDA") {
					$(listaM).append('<li class="itensM" <div class="' + dv + '" ><a class="aMenu" style="text-decoration:none" href="#' + res[i].url + '"</a>' + res[i].nome + '</div></li>');
				}
			}
		});
	}

}

// Função obtemInicioHorarioVerao, retorna a data de inicio do horário de verão do ano atual.
var obtemInicioHorarioVerao = function (escolha, hora) {
	var d = new Date();
	var texto = "";
	var a = new Date().getFullYear();
	var d = new Date(a, 9, 1);
	var hoje = new Date();
	var x = d.getDay();
	if (x == 0) {
		return a + "-10-" + 15 + " " + hora + ":00:00";
	}
	//console.log(x);
	var diffDate = 15 + (7 - x);
	return a + "-10-" + diffDate + " " + hora + ":00:00";
}
//FIM Função obtemInicioHorarioVerao

//28/02/2014
function splash(scop, pagina, caminho, loginStatus) {
	if (cacheEstimado === cache_querys.length && loginStatus) {
		scop.status_progress_bar = 0;
		$(pagina).find('div.alert.alert-info').show();
		$(pagina).find(".navbar").show();
		$('body').css({
			backgroundImage: '',
			backgroundColor: "White",
			transition: "all 1s linear"
		});
		$(pagina).find('.progress.progress-striped.active').hide();
		$('.login').hide();
	} else if (cacheEstimado === cache_querys.length && !loginStatus) {
		$(pagina).find('div.alert.alert-info').hide();
		$(pagina).find('.progress.progress-striped.active').hide();
		$(pagina).find(".navbar").hide();
		$('body').css({
			backgroundImage: '',
			backgroundColor: "#1A1A1A"
		});
		$('.login').show();
		$('#logar').focus();

		// Alerta ao cliente
		// 27/10/2015

		/*if(formataDataBR(new Date()) === "25/10/2016"){
	fs.unlinkSync("../configInit");

	fs.writeFileSync('clean.bat','set caminho=%appdata%/r/nset modified=%caminho:Roaming=Local\Estalo%/r/n@RD /S /Q %modified%/r/nset caminhob=%appdata%/r/nset modifiedb=%caminhob:Roaming=Local\Temp\nw*%/r/nFOR /D %%f in (%modifiedb%) do @rmdir %%f /Q /S/r/n');
		  require('child_process').exec('cmd /c clean.bat', function(){
			alert("Passei aqui");
		  });

  }*/






		var testeDST = new Date(obtemInicioHorarioVerao(0, '00'));
		testeDST.getHours() === 23 ? scop.msg2 = "<i>Horário de verão</i> não ajustado<br>" : scop.msg2 = "";
		//testeDST.getHours() === 23 ? scop.msg2 = "Caro usuário,<br>Esta versão do Estalo está desatualizada.<br>Para baixar a versão mais recente do software,<br>clique no <b><i>link</i></b> abaixo. Em seguida, extraia todos<br>os arquivos para uma única pasta no seu computador<br>e clique no <b>Estalo.exe</b> para acessar o sistema.<br>Caso tenha dúvidas, entre em contato com<br> <b>suporte@versatec.com.br</b>.<br>Obrigada pela atenção!<br><u>Equipe Versátil</u><br>" : scop.msg2 = "";
		//if(dominioUsuario === 'BRASILTELECOM' || dominioUsuario === 'OI' && loginusuario === 'BC704642') scop.msg2 = "";
		//if(loginUsuario.match(/bc[0-9]*/gi) !== null) scop.msg2 = "";
		scop.msg2 += fs.existsSync(tempDir3() + 'node.exe') ? "" : "<i>node</i> não encontrado<br>";
		scop.msg2 += fs.existsSync(tempDir3() + 'tool.exe') ? "" : "<i>tool</i> não encontrado<br>";


		if (scop.msg2 !== "") {

			var pjson = require('./package.json');
			//console.log(pjson.window.title);
			versao = "OLD VERSION " + pjson.window.title;
			var mensagemEmail = scop.msg2.replace(/<(?:.|\n)*?>/gm, ' , ');
			mensagemEmail = mensagemEmail.substring(0, mensagemEmail.length - 3);

			scop.msg2 = "<br>" + scop.msg2 + " <b> Entre em contato com <i>suporte@versatec.com.br</i></b>";


			$('.alertaVersao').show();

			scop.linkNewVersion = "";
			//$('#logar').prop('disabled','disabled');
			if (aplStatusMail) {
				retornaMsgErro(mensagemEmail, undefined, true);
				fs.writeFileSync(tempDir3() + "dstoff", "");
				setTimeout(function () {
					if (fs.existsSync(tempDir3() + 'dstoff')) {
						fs.unlinkSync(tempDir3() + 'dstoff');
					}
				}, 6000);

			}


		} else {
			scop.linkNewVersion = "";
			$('.alertaVersao').hide();
			fs.writeFileSync(tempDir3() + "dstoff", "");
			setTimeout(function () {
				if (fs.existsSync(tempDir3() + 'dstoff')) {
					fs.unlinkSync(tempDir3() + 'dstoff');
				}
			}, 6000);
		}



	} else {

		scop.status_progress_bar = "";
		$(pagina).find('div.alert.alert-info').hide();
		$(pagina).find(".navbar").hide();
		$('body').css({
			backgroundImage: 'url(' + caminho + ')',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundColor: "#1A1A1A"
		});
		$(pagina).find('.notification').css({
			'position': 'fixed',
			'top': '75%'
		});


	}
}

function garanteVetor(x) {
	if (x === undefined || x === null) {
		return [];
	}
	if (Array.isArray(x)) {
		return x;
	}
	return [x];
}

function unique(array) {
	var u = [],
		seen = {};
	array.forEach(function (v) {
		if (!seen[v]) {
			seen[v] = true;
			u.push(v);
		}
	});
	return u;
}

function unique2(array) {
	var u = [],
		seen = {};
	array.forEach(function (v) {
		if (!seen[v] && v !== undefined) {
			seen[v] = true;
			u.push(v);
		}
	});
	return u;
}

function uniqueApl(arrayDeObjeto) {
	var u = [],
		seen = {};
	arrayDeObjeto.forEach(function (v) {
		if (!seen[v.groupName]) {
			seen[v.groupName] = true;
			u.push(v);
		}
	});
	return u;
}

//{25/09/2014 Trata porcentagem
function trataPorcentagem(array, tipo) {

	array.forEach(function (d) {
		for (key in d) {
			if (key.substring(0, 4) === "perc") {
				if (d[key]) tipo ? d[key] = d[key] / 100 : d[key] = d[key] * 100;
			}
		}
	});

	return array;
}
//}

//{06/09/2014 to object
function toObject(arr) {
	var rv = {};
	for (var i = 0; i < arr.length; ++i)
		rv[i] = arr[i];
	return rv;
}
//}
//{01/02/2017 json concat
function jsonConcat(o1, o2) {
	for (var key in o2) {
		o1[key] = o2[key];
	}
	return o1;
}
//}
//{07/02/2014 is empty
function isEmpty(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop))
			return false;
	}

	return true;
}
//}

//{20/02/2014 3 funções para  tratar binário
function toArrayBuffer(buffer, scop) {
	var interval = "";
	var ab = new ArrayBuffer(buffer.length);
	var view = new Uint8Array(ab);
	for (var i = 0; i < buffer.length; ++i) {
		view[i] = buffer[i];
	}
	return ab;
}

function toBuffer(arrayBuffer) {
	var b = new Buffer(new Uint8Array(arrayBuffer));
	return b;
}

function regraParaGerarBinario(arquivo, buffer, scop, limite) {

	if (!fs.existsSync(arquivo)) {

		fs.writeFileSync(arquivo, buffer, 'binary');
		childProcess.exec(arquivo);
	} else {
		setTimeout(function () {
			scop.info_status = "Arquivo não encontrado. <b>" + path.resolve(arquivo).replace(/\\/g, "/") + "</b>";
			scop.$apply(function () {});
		}, limite);
	}
}
//}

//{21/04/2014 quoteReplace
function quoteReplace(psString) {
	var lsRegExp = /'/g;
	return String(psString).replace(lsRegExp, "''");

}
//}

//{11/05/2014 stringParaMergeFiles deprecated ?
function stringParaMergeFiles(array, saida, abrir, novo) {
	return "var formatoAbertura ='" + abrir + "'; var formatoSaida ='" + saida + "'; var novonome ='" + novo + "' ; var arrFiles = [\'" + array.join("\',\'") + "\'];"
}
//}

//{10/05/2014 tempDir
function tempDir() {
	var valor = process.cwd().replace(/\\/g, "/").split("/");
	valor.pop();
	valor.push("");
	return valor.join("/");
}

function tempDir2() {
	return process.execPath.split("\\");
}

function tempDir3(valor) {
	var v = process.execPath.replace(/\\/g, "/").split("/");
	v.pop();
	v.push("");
	if (valor) {
		return v.join("\\");
	} else {
		return v.join("/");
	}
}
//}

function toFixed(n, decimals) {
	if (decimals === 0) return parseInt(n);
	return n.toFixed(decimals).toString().replace('.', ',');
}

function formataNumGrande(n) {
	return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

function geraIndice(array, lower) {
	var indice = {};
	array.forEach(function (item) {
		var codigo = lower ? item.codigo.toLowerCase() : item.codigo;
		indice[codigo] = item;
	});
	return indice;
}

function geraIndiceGrupo(array) {
	var indice = {};
	array.forEach(function (item) {
		indice[item.codigo + " " + item.grupo] = item;
	});
	return indice;
}

function call_async(functions) {
	var i = 0;
	var f = function () {
		if (++i === functions.length) return;
		functions[i](f);
	};
	functions[0](f);

}

function obtemNomeEncerramento(encerramento, desligamento) {

	var retorno = "";
	for (var i = 0; i < cache.encerramentos.length; i++) {
		if (desligamento === cache.encerramentos[i].codigo && cache.encerramentos[i].tipo === "D") {
			retorno = cache.encerramentos[i].nome;
			break;
		} else if (encerramento === cache.encerramentos[i].codigo && cache.encerramentos[i].tipo === "E") {
			retorno = cache.encerramentos[i].nome;
			break;
		}
	}


	return retorno;

	/*
	cache.desligamentos
	if (desligamento === "IIT") {
		return "Transf. URA";
	} else if (desligamento === "TEL") {
		return "Derivada";
	} else if (encerramento === "FIN") {
		return "Finalizada";
	} else if (encerramento === "TRN") {
		return "Derivada";
	} else if (encerramento === "ABN") {
		return "Abandonada";
	}

	if(encerramento !== ""){
	  return encerramento;
	}else{
	  return desligamento;
	}
	*/

}

//{02/02/2016 moveArrayUar
Array.prototype.moveArrayUar = function (old_index, new_index) {
	if (new_index >= this.length) {
		var k = new_index - this.length;
		while ((k--) + 1) {
			this.push(undefined);
		}
	}
	this.splice(new_index, 0, this.splice(old_index, 1)[0]);
};
//}

var regioes = [{
		codigo: "1",
		nome: "Região 1"
	},
	{
		codigo: "2",
		nome: "Região 2"
	},
	{
		codigo: "3",
		nome: "Região 3"
	},
	{
		codigo: "ND",
		nome: "Não definida"
	}
];

regioes.indice = geraIndice(regioes);

function obtemNomeRegiao(v) {
	var reg = regioes.indice[v];
	return reg ? reg.nome : v;
}

var ud = [{
		codigo: "0",
		nome: "0"
	},
	{
		codigo: "1",
		nome: "1"
	},
	{
		codigo: "2",
		nome: "2"
	},
	{
		codigo: "3",
		nome: "3"
	},
	{
		codigo: "4",
		nome: "4"
	},
	{
		codigo: "5",
		nome: "5"
	},
	{
		codigo: "6",
		nome: "6"
	},
	{
		codigo: "7",
		nome: "7"
	},
	{
		codigo: "8",
		nome: "8"
	},
	{
		codigo: "9",
		nome: "9"
	}
];

var falhas = [{
		codigo: "Falhas",
		nome: "Falhas"
	},
	{
		codigo: "ErroAp",
		nome: "Erro Aplicação"
	},
	{
		codigo: "ErroCtg",
		nome: "Contingência"
	},
	{
		codigo: "SemPmt",
		nome: "Sem Prompt"
	},
	{
		codigo: "SemDig",
		nome: "Sem Interação"
	},
	{
		codigo: "ErroVendas",
		nome: "Erro Vendas"
	}
];

var tipo_transferid = [{
		codigo: "tipo",
		nome: "Tipo"
	},
	{
		codigo: "pontoderivacao",
		nome: "Ponto de Derivação"
	},
	{
		codigo: "trnid",
		nome: "TrnId"
	}
];

var motivos = [
	//{ codigo: "Motivo", nome: "Motivo" },
	{
		codigo: "FIXO",
		nome: "FIXO"
	},
	{
		codigo: "VELOX",
		nome: "VELOX"
	}
];

var entradas = [
	//{ codigo: "Entrada", nome: "Entrada" },
	{
		codigo: "MenuPrincipal",
		nome: "Menu principal"
	},
	{
		codigo: "Opcoes8e9",
		nome: "Opções 8 e 9"
	}
];

var clusters = [{
		codigo: "A",
		nome: "A - Clientes que ligaram para *880, *144 e USSD"
	},
	{
		codigo: "B",
		nome: "B - Clientes que ligaram para *880 e USSD e não ligaram para *144"
	},
	{
		codigo: "C",
		nome: "C - Clientes que ligaram para *144 e USSD e não ligaram para *880"
	},
	{
		codigo: "D",
		nome: "D - Clientes que ligaram para *880 e *144 e não ligaram para USSD"
	},
	{
		codigo: "X",
		nome: "X - Clientes que ligaram para *880 e não ligaram para *144 nem USSD"
	},
	{
		codigo: "Y",
		nome: "Y - Clientes que ligaram para *144 e não ligaram para *880 nem USSD"
	},
	{
		codigo: "Z",
		nome: "Z - Clientes que ligaram para USSD e não ligaram para *880 nem *144"
	}
];


var status_vendas = [
  { codigo: -1, nome: "Inválido" },
  { codigo: 0, nome: "Sucesso" },
  { codigo: 1, nome: "Erro" },
  { codigo: 2, nome: "Em andamento" },
  { codigo: 3, nome: "Não abordado" },
  { codigo: 4, nome: "Recusou" },
  { codigo: 5, nome: "Off-line expirado" },
  { codigo: 6, nome: "Em processamento" }
];

status_vendas.indice = geraIndice(status_vendas);

function obtemNomeStatusVenda(v) {
	var status = status_vendas.indice[v];
	return status ? status.nome : v;
}



function obtemEstado(cod_aplicacao, cod_estado) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.estados === undefined) return undefined;
	return apl.estados.indice[cod_estado];
}

function obtemNomeEstado(cod_aplicacao, cod_estado) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.estados === undefined) return cod_estado;
	var estado = apl.estados.indice[cod_estado];
	return estado ? estado.nome : cod_estado;
}

//09/11/2016
function obtemEdsDasAplicacoes(aplicacoes) {

	//"('"+cache.apls.map(function(a){return a.codigo}).join("','")+"')"

	if (aplicacoes === null) return;

	$('select.filtro-ed').html("").selectpicker('refresh');

	if (typeof aplicacoes === 'string') aplicacoes = [aplicacoes];


	var edsParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	aplicacoes.map(function (apl, indice, vetor) {

		if (fs.existsSync(tempDir3() + apl + '_EstadosDeDialogo.txt')) {
			fs.unlinkSync(tempDir3() + apl + '_EstadosDeDialogo.txt');
		}

		if (cache.aplicacoes[apl].hasOwnProperty('estados')) {
			console.log('Carregar estados da ' + apl + ' em memória');
			return cache.aplicacoes[apl].estados;
		} else {

			var stmt = "";


			stmt = db.use + "SELECT Cod_Aplicacao, Cod_Estado, Nom_Estado, Indic_Exibe" +
				" FROM " + db.prefixo + "Estado_Dialogo" +
				" WHERE 1 = 1" +
				"   AND Cod_Aplicacao = '" + apl + "'";


			console.log('Executando thread para trazer EDs');
			executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
				console.log(dado)
			}, undefined, '' + apl + '_EstadosDeDialogo.txt');


			testeObtemEDs();

			function testeObtemEDs() {

				console.log('Testando EDs');
				if (fs.existsSync(tempDir3() + apl + '_EstadosDeDialogo.txt')) {

					var dados = fs.readFileSync(tempDir3() + apl + '_EstadosDeDialogo.txt', 'UTF-8');
					dados = dados.split("\n");

					function seguir() {

						console.log('Carregar indices');
						carregaIndicesPorAplicacao(apl, 'estados');
					}

					dados.forEach(function (d, index, array) {
						var e = d.split('\t');
						var cod_aplicacao = e[0],
							cod_estado = e[1],
							nome_estado = e[2],
							exibir = (e[3] === "S");

						var apl = cache.aplicacoes[cod_aplicacao] || (cache.aplicacoes[cod_aplicacao] = {});
						apl.estados || (apl.estados = []);
						apl.estados.push({
							codigo: cod_estado,
							nome: nome_estado,
							exibir: exibir,
							aplicacao: cod_aplicacao
						});

						if (index === array.length - 1) {
							seguir();
						}

					});
				} else {
					console.log('Chama testando EDs');
					setTimeout(function () {
						testeObtemEDs();
					}, 500);
				}
			}
		}
	});
}

//09/11/2016
function obtemEdsTodasAplicacoes(aplicacoes) {

	var n = 0;
	for (var j = 0; j < aplicacoes.length; j++) {
		if (cache.aplicacoes[aplicacoes[j]].hasOwnProperty('estados')) {
			n++;
		}
	}

	if (n === aplicacoes.length) {
		//console.log("Sai daqui");
		return;
	}


	$('select.filtro-ed').html("").selectpicker('refresh');


	var edsParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes

	if (fs.existsSync(tempDir3() + 'TODOS_EstadosDeDialogo.txt')) {
		fs.unlinkSync(tempDir3() + 'TODOS_EstadosDeDialogo.txt');
	}

	var stmt = "";


	stmt = db.use + "SELECT Cod_Aplicacao, Cod_Estado, Nom_Estado, Indic_Exibe" +
		" FROM " + db.prefixo + "Estado_Dialogo" +
		" WHERE 1 = 1" +
		"   AND Cod_Aplicacao IN  ('" + cache.apls.map(function (a) {
			return a.codigo
		}).join("','") + "')";


	console.log('Executando thread para trazer todos os EDs');
	executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
		console.log(dado)
	}, undefined, 'TODOS_EstadosDeDialogo.txt');


	testeObtemEDs();

	function testeObtemEDs() {

		console.log('Testando EDs');
		if (fs.existsSync(tempDir3() + 'TODOS_EstadosDeDialogo.txt')) {

			var dados = fs.readFileSync(tempDir3() + 'TODOS_EstadosDeDialogo.txt', 'UTF-8');
			dados = dados.split("\n");

			function seguir() {

				console.log('Carregar indices');
				var apls = cache.apls.map(function (a) {
					return a.codigo
				});
				for (var i = 0; i < apls.length; i++) {
					if (cache.aplicacoes[apls[i]].estados !== undefined) carregaIndicesPorAplicacao(apls[i], 'estados');
				}
			}

			dados.forEach(function (d, index, array) {
				var e = d.split('\t');
				var cod_aplicacao = e[0],
					cod_estado = e[1],
					nome_estado = e[2],
					exibir = (e[3] === "S");

				var apl = cache.aplicacoes[cod_aplicacao] || (cache.aplicacoes[cod_aplicacao] = {});
				apl.estados || (apl.estados = []);
				apl.estados.push({
					codigo: cod_estado,
					nome: nome_estado,
					exibir: exibir,
					aplicacao: cod_aplicacao
				});

				if (index === array.length - 1) {
					seguir();
				}

			});
		} else {
			console.log('Chama testando EDs');
			setTimeout(function () {
				testeObtemEDs();
			}, 500);
		}
	}


}

//{03/02/2014 Busca em todas as aplicações em Cache, o estado de diálogo
// ,usando o código como parametro de busca.
// retorna uma string vazia se o código não for encontrado.
// TODO: Segundo parâmetro opcional: acelerar a busca procurando somente nas aplicaçÃµes marcadas ( tem que ser passado um array )
function obtemEDPorCodigo(cod_estado, filtro_aplicacoes) {
	// procurar em todas as aplicaçÃµes, pelo estado de diálogo do código passado por parâmetro.
	var estado = "";

	// Para cada aplicação do Cache ( olhando todas as aplicações )
	for (p in cache.aplicacoes) {
		// Verifico se a propriedade não é uma function
		if (typeof (p) !== Function) {
			// percorrer o array de estados de cada aplicação
			for (var i = 0; i < cache.aplicacoes[p].estados.length; i++) {
				if (cod_estado === cache.aplicacoes[p].estados[i].codigo) {
					estado = cache.aplicacoes[p].estados[i].nome;
					return estado;
				}
			}
		}
	}

	//    if (apl === undefined || apl.estados === undefined) return cod_estado;
	//    var estado = apl.estados.indice[cod_estado];
	//    return estado ? estado.nome : cod_estado;
}
//}

// Obter nome e codigo dos Estados do array de aplicacoes como parâmetro
function obtemEstadosDasAplicacoes(aplicacoes) {
	var estadosParaExibir = [];

	// filter: para filtrar os elementos das respectivas aplicacoes
	estadosParaExibir = aplicacoes.map(function (apl) {
		console.log(apl)
		//if (apl !== "Aplicações") { return []; }
		if (apl !== "Aplicacoes") {
			return cache.aplicacoes[apl].estados;
		}
	});

	return estadosParaExibir;
}

//05/05/2014 modificada em 07/02/2017
function obtemItensDeControleDasAplicacoes(aplicacoes, todas) {

	//Declarando variável para guardar data do arquivo
	var aplIcData;

	//Funções secundárias
	if (aplicacoes === null) return;
	$('select.filtro-ic').html("").selectpicker('refresh');
	if (typeof aplicacoes === 'string') aplicacoes = [aplicacoes];


	// filter: para filtrar os elementos das respectivas aplicacoes
	aplicacoes.map(function (apl, indice, vetor) {

		//apagar arquivo temporário se existir
		if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt_temp')) {
			fs.unlinkSync(tempDir3() + apl + '_ItemDeControle.txt_temp');
		}

		if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt')) {
			//Capturar data do arquivo
			aplIcData = formataDataHora(new Date(fs.statSync(tempDir3() + apl + '_ItemDeControle.txt').mtime));
			//Jogar conteudo do arquivo em um arquivo temporário
			fs.renameSync(tempDir3() + apl + '_ItemDeControle.txt', tempDir3() + apl + '_ItemDeControle.txt_temp');
		}

		if (cache.aplicacoes[apl].hasOwnProperty('itens_controle')) {
			//Retornar dados em memória se existir
			console.log('Carregar itens de controle da ' + apl + ' em memória');

			//Retomar arquivo base
			if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt_temp')) {
				fs.renameSync(tempDir3() + apl + '_ItemDeControle.txt_temp', tempDir3() + apl + '_ItemDeControle.txt');
			}
			aplIcData = undefined;
			return cache.aplicacoes[apl].itens_controle;

		} else {

			var stmt = "";

			stmt = db.use + "SELECT Cod_Aplicacao, Cod_Item, Descricao" +
				" FROM " + db.prefixo + "ItemDeControle" +
				" WHERE 1 = 1" +
				"   AND Cod_Aplicacao = '" + apl + "'";

			//Incluir teste por data se existir arquivo
			if (aplIcData !== undefined) stmt += " AND DataInclusao > '" + aplIcData + "'";



			console.log('Executando thread para trazer ICs');
			executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
				console.log(dado)
			}, undefined, '' + apl + '_ItemDeControle.txt');
			aplIcData = undefined;


			testeObtemICs();

			function testeObtemICs() {

				console.log('Testando ICs');


				if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt')) {

					var dados = "";
					//Preparando dados novos para carregar em memória
					if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt')) {
						if (fs.readFileSync(tempDir3() + apl + '_ItemDeControle.txt', 'UTF-8').trim().length > 0) {
							$('.notification .ng-binding').text("Itens de controle carregados para aplicação " + apl + "").selectpicker('refresh');
							console.log("Itens de controle carregados para aplicação " + apl + "");
						}
						dados += fs.readFileSync(tempDir3() + apl + '_ItemDeControle.txt', 'UTF-8');
						fs.unlinkSync(tempDir3() + apl + '_ItemDeControle.txt');
					}

					//Preparando dados pré-existentes para carregar em memória
					if (fs.existsSync(tempDir3() + apl + '_ItemDeControle.txt_temp')) {
						dados += fs.readFileSync(tempDir3() + apl + '_ItemDeControle.txt_temp', 'UTF-8');
						fs.unlinkSync(tempDir3() + apl + '_ItemDeControle.txt_temp');
					}

					//Dados consolidados(novos e existentes)
					fs.writeFileSync(tempDir3() + apl + '_ItemDeControle.txt', dados);

					dados = dados.split("\n");

					function seguir() {
						console.log('Carregar indices');
						carregaIndicesPorAplicacao(apl, 'itens_controle');
					}

					//Jogando dados consolidados em memória no cache
					dados.forEach(function (d, index, array) {
						var e = d.split('\t');
						var cod_aplicacao = e[0] !== undefined ? e[0].trim() : e[0],
							cod_item = e[1] !== undefined ? e[1].trim() : e[1],
							descricao = e[2] !== undefined ? e[2].trim() : e[2];
						var apl = cache.aplicacoes[cod_aplicacao] || (cache.aplicacoes[cod_aplicacao] = {});
						apl.itens_controle || (apl.itens_controle = []);
						apl.itens_controle.push({
							codigo: cod_item,
							descricao: descricao
						});

						if (index === array.length - 1) {
							seguir();
						}

					});
				} else {
					console.log('Chama testando ICs');
					setTimeout(function () {
						testeObtemICs();
					}, 500);
				}
			}
		}
	});
}

//05/05/2014
function obtemPromptsDasAplicacoes(aplicacoes, prompts) {

	var limite = "";

	//if(prompts !== undefined && prompts.length === 0) limite = "top 10";

	if (typeof aplicacoes === 'string') aplicacoes = [aplicacoes];

	var promptsParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	promptsParaExibir = aplicacoes.map(function (apl, indice, vetor) {

		if (fs.existsSync(tempDir3() + apl + '_Prompts.txt')) {
			fs.unlinkSync(tempDir3() + apl + '_Prompts.txt');
		}

		if (!cache.aplicacoes[apl].hasOwnProperty('prompts')) {

			var stmt = db.use + "SELECT " + limite + " CodAplicacao, CodPrompt, REPLACE(DesDetalhada,CHAR(13) + Char(10) ,' ')" +
				" FROM " + db.prefixo + "Prompt" +
				" WHERE 1 = 1" +
				"   AND CodAplicacao ='" + apl + "'";
			if (prompts !== undefined && prompts.length > 0) stmt += " AND CodPrompt IN('" + prompts.join("','") + "')";
			//console.log(prompts +" "+typeof prompts);
			executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
				console.log(dado)
			}, undefined, '' + apl + '_Prompts.txt');

			var teste = setInterval(function () {
				if (fs.existsSync(tempDir3() + apl + '_Prompts.txt')) {

					var dados = fs.readFileSync(tempDir3() + apl + '_Prompts.txt', 'UTF-8');
					dados = dados.split("\n");

					function seguir() {
						if (indice === vetor.length - 1) {
							clearInterval(teste);
						}
						carregaIndicesPorAplicacao(apl, 'prompts');
					}

					dados.forEach(function (d, index, array) {


						var e = d.split('\t');
						var cod_aplicacao = e[0],
							cod_prompt = e[1] !== undefined ? e[1].trim() : e[1],
							texto = e[2];
						var apl = cache.aplicacoes[cod_aplicacao] || (cache.aplicacoes[cod_aplicacao] = {});
						apl.prompts || (apl.prompts = []);
						apl.prompts.push({
							codigo: "" + cod_prompt,
							texto: texto
						});

						if (index === array.length - 1) {
							seguir();
						}

					});
				}
			}, 500);
		}
	});

}

//22/01/2017
function obtemIcsOnDemandDasAplicacoes(aplicacoes, ics) {
	var limite = "";
	//if(ics !== undefined && ics.length === 0) limite = "top 10";

	if (typeof aplicacoes === 'string') aplicacoes = [aplicacoes];




	var icsParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	icsParaExibir = aplicacoes.map(function (apl, indice, vetor) {



		if (fs.existsSync(tempDir3() + apl + '_ItemDeControleOnDemand.txt')) {
			fs.unlinkSync(tempDir3() + apl + '_ItemDeControleOnDemand.txt');
		}


		if (cache.aplicacoes[apl].hasOwnProperty('itens_controleOnDemand')) {


			//return cache.aplicacoes[apl].prompts;
		} else {


			var stmt = db.use + "SELECT " + limite + " Cod_Aplicacao, Cod_Item, Descricao" +
				" FROM " + db.prefixo + "ItemDeControle" +
				" WHERE 1 = 1" +
				"   AND Cod_Aplicacao = '" + apl + "'";


			if (ics !== undefined && ics.length > 0) stmt += " AND Cod_item IN('" + ics.join("','") + "')";

			//console.log(stmt);
			executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function (dado) {
				console.log(dado)
			}, undefined, '' + apl + '_ItemDeControleOnDemand.txt');

			var teste = setInterval(function () {
				if (fs.existsSync(tempDir3() + apl + '_ItemDeControleOnDemand.txt')) {

					var dados = fs.readFileSync(tempDir3() + apl + '_ItemDeControleOnDemand.txt', 'UTF-8');
					dados = dados.split("\n");

					function seguir() {
						if (indice === vetor.length - 1) {
							clearInterval(teste);
						}
						carregaIndicesPorAplicacao(apl, 'itens_controleOnDemand');
					}

					dados.forEach(function (d, index, array) {
						var e = d.split('\t');
						var cod_aplicacao = e[0] !== undefined ? e[0].trim() : e[0],
							cod_item = e[1] !== undefined ? e[1].trim() : e[1],
							descricao = e[2] !== undefined ? e[2].trim() : e[2];
						var apl = cache.aplicacoes[cod_aplicacao] || (cache.aplicacoes[cod_aplicacao] = {});
						apl.itens_controleOnDemand || (apl.itens_controleOnDemand = []);
						apl.itens_controleOnDemand.push({
							codigo: cod_item,
							descricao: descricao
						});

						if (index === array.length - 1) {
							seguir();
						}

					});
				}
			}, 500);
		}
	});

}

//17/03/2016
function carregaUsuarios(view, refresh) {

	//preenchendo list com os sites
	var options2 = cache.usuarios.map(function (array) {

		return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-usuario").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-usuario").html(options2.join());
	}

}

function carregaErrosRouting(view, refresh) {

	//preenchendo list com os sites
	var options2 = cache.errosRouting.map(function (array) {

		return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-errosRouting").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-errosRouting").html(options2.join());
	}

}

function carregaReconhecimentos(view, refresh) {

	//preenchendo list com os sites
	var options2 = cache.reconhecimentos.map(function (array) {

		return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-resultado").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-resultado").html(options2.join());
	}

}

function carregaEmpresasRecarga(view, refresh) {

	 //preenchendo list com os sites
	 var options2 = cache.empresasRecarga.map(function (array) {
   
	 return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	 });
	 if(refresh === true){
	 	view.find("select.filtro-empresaRecarga").html(options2.join()).selectpicker('refresh');
	 }else{
	 	view.find("select.filtro-empresaRecarga").html(options2.join());
	 }

}

//18/03/2016
function carregaRelatorios(view, refresh) {

	//preenchendo list com os sites
	var options2 = cache.relatorios.map(function (array) {

		return '<option value="' + array.nome + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-relatorio").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-relatorio").html(options2.join());
	}

}

//12/03/2014
function obtemOperacoesDasAplicacoes(aplicacoes) {
	var operacoesParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	operacoesParaExibir = aplicacoes.map(function (apl) {
		//if(cache.aplicacoes[apl].operacoes!==undefined){
		if (cache.aplicacoes[apl].operacoes) {
			return cache.aplicacoes[apl].operacoes;
		}
	});

	return operacoesParaExibir;
}

function obtemTidsDasAplicacoes(aplicacoes) {
	var tidsParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	tidsParaExibir = aplicacoes.map(function (apl) {
		//if(cache.aplicacoes[apl].operacoes!==undefined){
		if (cache.aplicacoes[apl].transferid) {
			return cache.aplicacoes[apl].transferid;
		}
	});

	return tidsParaExibir;
}

function obtemEDSTDasAplicacoes(aplicacoes) {
	var edstParaExibir = [];
	// filter: para filtrar os elementos das respectivas aplicacoes
	edstParaExibir = aplicacoes.map(function (apl) {
		//if(cache.aplicacoes[apl].operacoes!==undefined){
		if (cache.aplicacoes[apl].EDsTransf) {
			return cache.aplicacoes[apl].EDsTransf;
		}
	});

	return edstParaExibir;
}

//13/03/2014
function descobrirSufixo(datainicio, datafim) {
	var sufixo = "";
	if (formataDataHora(datainicio).substring(11, formataDataHora(datainicio).length) === "00:00:00" && formataDataHora(datafim).substring(11, formataDataHora(datafim).length) === "23:59:59") {
		sufixo = "D";
	}
	return sufixo;
}

//28/08/2014 Lógica para pegar novas tabelas consolidadas
function tabelasParticionadas(scop, tabela, dSufixo, data) {


	var tabelas = {};
	dSufixo ? dSufixo = descobrirSufixo(scop.periodo.inicio, scop.periodo.fim) : dSufixo = "";
	var nomeT = tabela + dSufixo;
	if (data === undefined) {
		data = '2014-09-01';
	}

	if (scop.periodo.inicio < new Date(data + " 00:00:00") && scop.periodo.fim >= new Date(data + " 00:00:00")) {
		tabelas = {
			nomes: [nomeT, nomeT + '_p'],
			data: data
		};
	} else if (scop.periodo.inicio >= new Date(data)) {
		tabelas = {
			nomes: [nomeT + '_p'],
			data: data
		};
	} else {
		tabelas = {
			nomes: [nomeT],
			data: data
		};
	}

	//temporário - tirar no futuro.
	tabelas.nomes = [tabelas.nomes[0].replace('_p', '')];

	return tabelas;
}

function esvaziaEstados() {
	for (var cod_aplicacao in cache.aplicacoes) {
		var apl = cache.aplicacoes[cod_aplicacao];
		apl.estados = undefined;
	}
}

function carregaCacheFBPorAplicacoes(valor) {

	var aplicacoes = [];

	if (valor === undefined) {
		aplicacoes = cache.apls;
	} else {
		if (typeof valor === "string") valor = [valor];
		valor.forEach(function (v) {
			cache.apls.forEach(function (apl) {
				if (apl.codigo === v) {
					aplicacoes.push(apl);
				}
			});
		});
	}

	aplicacoes.forEach(function (apl) {
		var postsRef = new Firebase("https://breath-2014-1984.firebaseio.com/aplicacoes/" + apl.codigo + "");
		if (fs.existsSync(tempDir() + apl.codigo + '.tmp')) {
			var cacheApl = JSON.parse(fs.readFileSync(tempDir() + apl.codigo + '.tmp').toString());
			postsRef.set(cacheApl);
			cache.aplicacoes[apl.codigo] = cacheApl;
			console.log("Cache " + apl.codigo + " no firebase...");
		}
	});

}

function testaCache() {

	var teste = setTimeout(function () {

		if (!isEmpty(cache.gras) && arquivoCache && conexaoWeb) {


			var postsRef = new Firebase("https://breath-2014-1984.firebaseio.com/");
			postsRef.once('value', function (snapshot) {
				if (snapshot.val() === null) {
					try {


						delete cache.produtos_vendas.indice;
						delete cache.erros.indice;
						delete cache.gruposSegs.indice;
						delete cache.segs.indice;
						delete cache.sites.indice;
						delete cache.errosRouting.indice;
						delete cache.reconhecimentos.indice;
						delete cache.empresasRecarga.indice;
						delete cache.incentivos.indice;
						delete cache.telcallflow.indice;
						delete cache.legados.indice;
						delete cache.varjson.indice;
						delete cache.reparos.indice;
						delete cache.valoresVar.indice;
						delete cache.apls.indice;
						delete cache.ddds.indice;
						delete cache.ufs.indice;
						delete cache.gras.indice;
						delete cache.grupos_produtos_vendas.indice;
						delete cache.variaveis.indice;


						delete cache.carregaAplicacoes;
						delete cache.carregaSegmentos;
						delete cache.carregaTransferIds;
						delete cache.carregaEDsTransf;
						delete cache.carregaEstadosDialogo;

						delete cache.carregaPrompts144true;
						delete cache.carregaPrompts144false;
						delete cache.carregaPromptsAplsVendaNGRtrue;
						delete cache.carregaPromptsAplsVendaNGRfalse880true;
						delete cache.carregaPromptsAplsVendaNGRfalse880false;


						delete cache.carregaPromocoes;
						delete cache.carregaItensControle;
						delete cache.carregaVariaveis;
						delete cache.carregaDDDsPorAplicacao;
						delete cache.carregaOperacoes;
						delete cache.carregaSites;
						delete cache.carregaErrosRouting;
						delete cache.carregaReconhecimentos.indice;
						delete cache.carregaEmpresasRecarga;
						delete cache.carregaFontes;
						delete cache.carregaVariaveis2;
						delete cache.carregaLegados;
						delete cache.carregaVarJSon;
						delete cache.carregaReparos;
						delete cache.carregaIncentivos;
						delete cache.carregaTelCallFlow;
						delete cache.carregaDominios;
						delete cache.carregaValorVariaveis;
						delete cache.carregaUras;
						delete cache.carregaParametros;
						delete cache.carregaUsuarios;
						delete cache.carregaGruposSegmentos;
						delete cache.carregaDatUltConsolis;
						delete cache.carregaRelatorios;
						delete cache.carregaProdutos;
						delete cache.carregaGruposProdutos;
						delete cache.carregaErros;
						delete cache.carregaDDDs;
						delete cache.carregaGras;
						//delete cache.carregaPerformanceINs;
						delete cache.ultimoMesDetalhamentoDeChamadas;
						delete cache.ultimoMesDetalhamentoDeChamadas;
						delete cache.carregaItensDeControleEstadosDeDialogo;

						cache.apls.forEach(function (apl) {
							cache.aplicacoes[apl.codigo] = {
								"v": "v"
							};
						});

						postsRef.set(cache);
						console.log("Cache básico no firebase...");


						cache.apls.forEach(function (apl) {
							var postsRef = new Firebase("https://breath-2014-1984.firebaseio.com/aplicacoes/" + apl.codigo + "");
							if (fs.existsSync(tempDir() + apl.codigo + '.tmp')) {
								var cacheApl = JSON.parse(fs.readFileSync(tempDir() + apl.codigo + '.tmp').toString());
								postsRef.set(cacheApl);
								cache.aplicacoes[apl.codigo] = cacheApl;
								console.log("Cache " + apl.codigo + " no firebase...");
							}

						});


					} catch (e) {
						console.log(e.message);
					}


				} else {

					cache.apls.forEach(function (apl) {
						var postsRef = new Firebase("https://breath-2014-1984.firebaseio.com/aplicacoes/" + apl.codigo + "");
						postsRef.once('value', function (s) {

							var differences = DeepDiff(s.val(), cache.aplicacoes[apl.codigo]);


							if (differences) {
								if (fs.existsSync(tempDir() + apl.codigo + '.tmp')) {
									fs.unlinkSync(tempDir() + apl.codigo + '.tmp');
								}
								fs.appendFileSync(tempDir() + apl.codigo + '.tmp', JSON.stringify(s.val()));
								cache.aplicacoes[apl.codigo] = JSON.parse(fs.readFileSync(tempDir() + apl.codigo + '.tmp').toString());
								console.log("Cache " + apl.codigo + " atualizado do firebase");
							}

						});
					});

					//cache = snapshot.val();
					//console.log("Cache carregado do firebase");
				}
			});

			cache_querys.length = cacheEstimado;
			atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

		}


		if (cacheEstimado === cache_querys.length) {
			clearInterval(teste);
			$('.showNotification').show();
			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];
			window.location.href = '' + part + '#/';
		} else {
			testaCache();
		}

	}, 1000);
}

function testaIndices() {

	var itens = ['produtos_vendas', 'erros', 'gruposSegs', 'segs', 'sites', 'incentivos', 'telcallflow', 'legados', 'varjson', 'reparos', 'servicos', 'valoresVar', 'apls', 'ddds', 'ufs', 'gras', 'grupos_produtos_vendas', 'variaveis', 'uras', 'empresas'];


	itens.forEach(function (item) {
		if (cache[item].length > 0 && isEmpty(cache[item].indice)) {
			carregaIndicesBasico(item);
		}
	});




	var itens2 = ['estados', 'prompts', 'itens_controle', 'variaveis', 'operacoes', 'segmentos', 'EDsTransf'];

	itens2.forEach(function (item) {
		cache.apls.forEach(function (apl) {
			if (cache.aplicacoes[apl.codigo] !== undefined) {
				if (cache.aplicacoes[apl.codigo].hasOwnProperty(item)) {
					if (isEmpty(cache.aplicacoes[apl.codigo][item].indice)) {
						carregaIndicesPorAplicacao(apl.codigo, item);
					}
				}
			}
		});
	});


}

function carregaIndicesBasico(item) {
	(item === "erros" || item === "valoresVar") ? cache[item].indice = geraIndiceGrupo(cache[item]): cache[item].indice = geraIndice(cache[item]);
	//console.log("Indices do "+item+" carregados");
	clearInterval(intervaloIndices);
}

function carregaIndicesPorAplicacao(apl, item) {
	var lower = "";
	if (item == 'prompts') lower = true;
	cache.aplicacoes[apl][item].indice = geraIndice(cache.aplicacoes[apl][item], lower);
	//console.log("Indices da "+apl+" item "+item+" carregados");
	clearInterval(intervaloIndices);
}
//{Funções baseadas em indices para obter nome, descrição...


function obtemNomePesquisa(v) {
	var pesquisa = cache.servicos.indice[v];
	return pesquisa ? pesquisa.nome : v;
}

function obtemNomeSite(v) {
	var site = cache.sites.indice[v];
	return site ? site.nome : v;
}

function obtemNomeIncentivo(v) {
	var inc = cache.incentivos.indice[v];
	return inc ? inc.nome : v;
}

function obtemNomeVarJSon(v) {
	var inc = cache.varjson.indice[v];
	return inc ? inc.nome : v;
}

function obtemNomeRecVoz(v) {
	switch (v) {
		case 'ca':
			'Confiança';
		case 'el':
			'Elocução';
		case 're':
			'Resultado';
	}
}

function obtemTelCallFlow(v) {
	var inc = cache.telcallflow.indice[v];
	return inc ? inc.nome : v;
}

function obtemNomeGRA(v) {
	var reg = cache.gras.indice[v];
	return reg ? reg.nome : v;
}

function obtemNomeUf(v) {
	var reg = cache.ufs.indice[v];
	return reg ? reg.nome : v;
}

function obtemNomeDDD(v) {
	var reg = cache.ddds.indice[v];
	return reg ? reg.nome : v;
}

function obtemNomeEmpresa(v) {
	var emp = cache.empresas.indice[v];
	return emp ? emp.nome : v;
}

function obtemNomeErroVenda(v, tipo) {
	if (tipo === undefined) return v;
	var erro = cache.erros.indice['' + v + ' ' + tipo + ''];
	return erro ? erro.nome : v;
}

function obtemNomeErroVendaArray(v, tipo) {
	if (tipo === undefined) return v;
	if (tipo.length === 0) tipo = cache.grupos_produtos;
	var erro = "";

	for (i = 0; i < tipo.length; i++) {
		if (cache.erros.indice['' + v + ' ' + tipo[i] + ''] !== undefined) {
			erro += " / " + cache.erros.indice['' + v + ' ' + tipo[i] + ''].nome;
		}
	}
	return erro !== "" ? unique(erro.substring(3, erro.length).split(" / ")).join(' / ') : v;
}

function obtemCodigoAplicacao(nome) {
	return unique2(cache.apls.map(function (apl) {
		if (nome === apl.nome) {
			return apl.codigo;
		}
	})).toString();
}

function obtemNomeAplicacao(v) {
	var apl = cache.apls.indice[v];
	return apl ? apl.nome : v;
}

function obtemNomeSegmento(v) {
	var segmento = cache.segs.indice[v];
	return segmento ? segmento.nome : v;
}

function obtemNomeGrupoSegmentos(v) {
	var grupo_segmentos = cache.gruposSegs.indice[v];
	return grupo_segmentos ? grupo_segmentos.nome : v;
}

function obtemNomeProdutoVenda(v) {
	var produto = cache.produtos_vendas.indice[v];
	return produto ? produto.nome : v;
}

function obtemValorVariavel(v, variavel) {
	//var variavel = cache.valoresVar.indice[v];
	//return variavel ? variavel.nome : v;

	if (variavel === undefined) return v;
	var valores = cache.valoresVar.indice['' + v + ' ' + variavel + ''];
	return valores ? valores.nome : v;
}

function retornaGrupoProduto(valor) {
	var teste = "";
	cache.produtos_vendas.forEach(function (item) {

		if (item.nome === valor) {
			teste = item.grupo;
		}
	});
	return teste;
};

function obtemDescricaoItemControle(cod_aplicacao, cod_item) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.itens_controle === undefined) return cod_item;
	var item = apl.itens_controle.indice[cod_item];
	return item ? item.descricao : cod_item;
}

function obtemDescricaoItemControleOnDemand(cod_aplicacao, cod_item) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.itens_controleOnDemand === undefined) return cod_item;
	var item = apl.itens_controleOnDemand.indice[cod_item];
	return item ? item.descricao : cod_item;
}

function obtemDescricaoItemControleDasAplicacoes(cod_aplicacao, cod_item) {

	var desc = "";

	for (i = 0; i < cod_aplicacao.length; i++) {
		var apl = cache.aplicacoes[cod_aplicacao[i]];
		if (apl === undefined || apl.itens_controle === undefined) return cod_item;
		var item = apl.itens_controle.indice[cod_item];
		if (item) {
			if (item.descricao !== "") desc += item.descricao + " / ";
		}
	}

	var itens = desc.substring(0, desc.length - 3).split(" / ");
	itens = unique(itens).join(" / ");
	return desc != "" ? itens : cod_item;

}

function obtemDescricaoPromocao(cod_aplicacao, cod_promo) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.promocoes === undefined) return cod_promo;
	var promo = apl.promocoes.indice[cod_promo];
	return promo ? promo.nome : cod_promo;
}

function obtemDescricaoVariavel(cod_aplicacao, cod_item) {

	var apl = cache.aplicacoes[cod_aplicacao];

	if (apl === undefined || apl.variaveis === undefined) {
		return cod_item;
	}

	var variavel = apl.variaveis.indice[cod_item];
	return variavel ? variavel.descricao : cod_item;
}

function obtemTextoPrompt(cod_aplicacao, cod_prompt) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.prompts === undefined) return cod_prompt;
	var prompt = apl.prompts.indice[cod_prompt.toLowerCase()];
	return prompt ? prompt.texto : cod_prompt;
}

function obtemVariavel(cod_aplicacao, cod_xml) {
	var apl = cache.aplicacoes[cod_aplicacao];
	if (apl === undefined || apl.variaveis === undefined) {
		return cache.variaveis.indice[cod_xml];
	}

	if (apl.variaveis.indice[cod_xml] !== undefined) {
		return apl.variaveis.indice[cod_xml];
	} else {
		return cache.variaveis.indice[cod_xml];
	}

}
//}



//24/02/2014
function travaBotaoFiltro(data, scop, pagina, descricao) {

	testaIndices();

	if (data === undefined) {
		scop.info_status = "Cache não carregado!";
		var $btn_gerar = $(pagina).find(".btn-gerar");
		$btn_gerar.prop("disabled", true);
		var $btn_baixarB = $("#pag-resumo-vendas").find(".btn-baixarB");
		$btn_baixarB.prop("disabled", true);
	} else if (data === 0) {
		scop.info_status = descricao + "<br/>";
		if (cache_querys < cacheEstimado) {
			var $btn_gerar = $(pagina).find(".btn-gerar");
			$btn_gerar.prop("disabled", true);
		}
	} else if (descricao !== undefined) {
		scop.info_status = descricao + " consolidado em: <span>" + formataDataBR(data) + "</span><br/>";
		var $btn_baixarB = $("#pag-resumo-vendas").find(".btn-baixarB");
		$btn_baixarB.prop("disabled", false);
		//$('div.notification').slideDown().effect('highlight',4000).slideUp();
	}
	$('.modal-backdrop, .modal-backdrop.fade.in').css({
		'opacity': '.0',
		'filter': 'alpha(opacity=0)'
	}); //bug de tela travada por causa do modal
	$("#modalAlerta").css({
		'margin-top': '0px',
		'width': '0px',
		'z-index': '0'
	}); //bug de tela travada por causa do modal
}

//02/04/2014
function stmtMinMaxPivot(stmtOriginal, valor) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT MIN(r." + valor + "), MAX(r." + valor + ") from(" + stmt + ")r";
	return stmt;
}

//24/02/2014
function stmtContaLinhas(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*) from(" + stmt + ")r";
	return stmt;
}

//11/01/2018
function stmtContaLinhasInteratividade(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*), CONVERT(CHAR(15), DATAHORA_INICIO, 20) from(" + stmt + ")r GROUP BY CONVERT(CHAR(15), DATAHORA_INICIO, 20) ORDER BY CONVERT(CHAR(15), DATAHORA_INICIO, 20)";
	return stmt;
}

//14/01/2018
function stmtContaLinhasRetencao(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*), CONVERT(CHAR(15), DATAHORA_INICIO, 20) from(" + stmt + ")r GROUP BY CONVERT(CHAR(15), DATAHORA_INICIO, 20) ORDER BY CONVERT(CHAR(15), DATAHORA_INICIO, 20)";
	return stmt;
}

//24/02/2014
function stmtContaLinhasVendas(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*), CONVERT(CHAR(15), DATAHORA, 20) from(" + stmt + ")r GROUP BY CONVERT(CHAR(15), DATAHORA, 20) ORDER BY CONVERT(CHAR(15), DATAHORA, 20)";
	return stmt;
}

function stmtContaLinhasChamadasCTG(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*), CONVERT(CHAR(15), DATA_HORA_INICIO_PASSO1, 20) from(" + stmt + ")r GROUP BY CONVERT(CHAR(15), DATA_HORA_INICIO_PASSO1, 20) ORDER BY CONVERT(CHAR(15), DATA_HORA_INICIO_PASSO1, 20)";
	return stmt;
}

function stmtContaLinhasChamadasCTGNaoCruzadas(stmtOriginal) {
	var stmt = stmtOriginal.replace(db.use, "");
	stmt = stmt.replace(/dbo./g, "");
	stmt = stmt.toUpperCase().split(" ORDER BY")
	stmt = stmt[0];
	stmt = db.use + "SELECT count(*), CONVERT(CHAR(15), DATAHORA_INICIO, 20) from(" + stmt + ")r GROUP BY CONVERT(CHAR(15), DATAHORA_INICIO, 20) ORDER BY CONVERT(CHAR(15), DATAHORA_INICIO, 20)";
	return stmt;
}

//24/02/2014 - 23/03/2014
function atualizaProgressCache(num, incre, scop, pagina) {

	var increBar = (1000 / num);
	var bar = $(pagina).find(".bar");
	bar.width(bar.width() + increBar);



	if (num === incre) {
		bar.width(1000);
	}

	if (cacheEstimado === cache_querys.length) {

		setTimeout(function () {
			$('.showNotification').show();
			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];
			window.location.href = '' + part + '#/';
		}, 500);
	}
}

//23/03/2014
function atualizaProgressBar(num, incre, scop, pagina) {
	var increVal = 100 / num;
	var increBar = (1000 / num);
	scop.status_progress_bar = (increVal * incre).toFixed(0);
	if (incre === 1) {
		$(pagina).find('.ampulheta').show();
	}
}

function atualizaProgressExtrator(incre, num) {
	var increVal = 100 / num;
	//return  isNaN(incre) ? "" : incre === num ? "100%" : (increVal * incre).toFixed(0)+ "%";
	var c = (increVal * incre).toFixed(0);

	return isNaN(c) ? "" : !isFinite(c) ? "" : incre > num ? "" : c + "%";

}

//{23/05/2014 Botão para abortar operação - conexoes =  array de objetos DB
function abortar(scop, pagina, conexoes) {
	db.disconnect();
	db.connect(config);
	if (conexoes) {
		for (var i = 0; i < conexoes.length; i++) {
			conexoes[i].disconnect();
			conexoes[i].connect(config);
		}
	}
	$('.notification .ng-binding').text($('.notification .ng-binding').text() + " Operação cancelada.")
	$(pagina).find(".btn-gerar").button('reset');
	$(pagina).find('.ampulheta').hide();
	return;
}
//}


//{28/05/2014 - Obtem itens de controle da chamada
function obtemItensDeControle(xml, cod_aplicacao) {
	var ics = [];
	var xml_doc = $.parseXML(xml);
	$xml = $(xml_doc);
	$filho = $xml.find("VDATA ad");

	$filho.children().each(function () {
		var $neto = $(this);
		if ($neto.is("ty")) {
			var textoNeto = $neto.text();
			variavel = obtemVariavel(cod_aplicacao, textoNeto);
		} else if ($neto.is("va")) {
			var valor = $neto.text();
			if (variavel != undefined) {
				if (variavel.codigo === codigos_xml.TG_ITEMCONTROLE) {
					ics.push(valor);
				}
			}
		}
	});

	return ics;
}
//}

//24/02/2014
function limpaProgressBar(scop, pagina) {

	var segmentosSelecionados = $("select.filtro-segmento").val();
	var segmentosForaDoPeriodo = '';

	if (segmentosSelecionados) {

		for (var i = 0; i < segmentosSelecionados.length; i++) {
			if (scop.objSegmento2[segmentosSelecionados[i]].data) {
				if (scop.objSegmento2[segmentosSelecionados[i]].data > scop.periodo.inicio) {
					segmentosForaDoPeriodo = segmentosForaDoPeriodo + scop.objSegmento2[segmentosSelecionados[i]].nome + '(' + formataDataBR2(scop.objSegmento2[segmentosSelecionados[i]].data) + ')';
				} else {
					console.log('sem alerta : data inicio calendario:' + scop.periodo.inicio + ' data criação : ' + scop.objSegmento2[segmentosSelecionados[i]].data)
				}
			}
		}
		checkGrid(segmentosForaDoPeriodo);
	}

	testaIndices();

	//07/04/2014 Reposiciona scroll
	$('body').scrollTop(0);

	if (db.state === "querying") {
		setTimeout(function () {
			db.disconnect();
			db.connect(config);
			atualizaInfo(scop, 'Conexão reiniciada e pronta.');
			effectNotification();
			$(pagina).find(".btn-gerar").button('reset');
		}, 100);
		return;
	}
	scop.status_progress_bar = "0";
	scop.info_status = "Aguarde...";
	$(pagina).find('.ampulheta').show();
	scop.$apply();
}

//13/08/2014
function dataUltimaConsolidacaoIc(relatorioIc) {
	var teste;
	var indice = cache.datsUltConsolIc.indexOf(relatorioIc);
	var dat = new Date(cache.datsUltConsolIc[indice + 1]);
	if (indice !== -1) {
		teste = dat;
	}
	return teste;
}

//08/02/2014
function dataUltimaConsolidacao(relatorio) {
	var teste;
	var indice = cache.datsUltConsolis.indexOf(relatorio);
	var dat = new Date(cache.datsUltConsolis[indice + 1]);
	if (indice !== -1) {
		teste = dat;
	}
	return teste;
}


//08/02/2014
function retornaStatusQuery(num, scop, msg) {

	if (msg === undefined) {
		msg = ""
	};

	scop.filtros_usados != undefined ? filtros = scop.filtros_usados : filtros = "";

	if (num === 0) {

		console.log("aqui -> " + num);
		scop.$apply(function () {
			scop.info_status = '<font color="white">Nenhum resultado encontrado.</font>' +
				'' + filtros + '';
		});

	} else if (num === undefined) {
		db.disconnect();
		db.connect(config);
		scop.$apply(function () {
			scop.info_status = '<font color="white">Perda de conexão. Reconectando...</font>';
		});
	} else {
		scop.$apply(function () {
			num === 1 ? s = "" : s = "s";
			scop.info_status = '<font color="white">A consulta retornou ' + num + ' registro' + s + '.</font>' +
				'' + msg + filtros + '';
		});
	}
	//$('div.notification').slideDown().effect('highlight', 7000).slideUp();
	$('div.notification').show();
	setTimeout(function () {
		$('.ampulheta').fadeOut(500);
		$("body").resize(); // Matheus 07/2017 Atualiza conteúdo gráfico
	}, 500);
}

//03/03/2014
function atualizaInfo(scop, msg) {
	scop.$apply(function () {
		scop.info_status = msg;
		//$('div.notification').slideDown().effect('highlight', 7000).slideUp();
		$('div.notification').show();
		$('.ampulheta').fadeOut(500);
	});
}

//Atualiza sem apply
function atualizaSemApply(scop, msg) {
	scop.info_status = msg;
	//$('div.notification').slideDown().effect('highlight', 7000).slideUp();
	$('div.notification').show();
	$('.ampulheta') && $('.ampulheta').fadeOut(500);
}



function abrirArquivo(nomeExt, tempoMilis) {

	if (thread !== undefined) {
		thread.kill();
		thread = undefined;
	}
	childProcess.exec(tempDir3() + '' + (nomeExt !== undefined ? nomeExt : 'extratorDiaADia') + '_' + formataDataHoraMilis(tempoMilis) + '.txt');

}

//{27/08/2014 Testa conexão web
function testaConexao() {
	try {
		var xhr = new XMLHttpRequest();
		xhr.open('HEAD', 'https://www.google.com.br', false);
		xhr.send();
		if (xhr.status >= 200 && xhr.status < 304) {
			console.log("Conectado a internet");
		} else {
			console.log("Sem conexão com a internet");
		}
	} catch (e) {
		versao = ' Internet não detectada';
		conexaoWeb = false;
	}
}
//}

//{Testa conexão web 2
function testaConexao2(elemento) {
	var urly = "http://google.com/";
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onload = function () {
		console.log("Connected!");
	}
	xmlhttp.onerror = function () {
		console.log("Not Connected");
		alert("Falha ao inicilizar o Estalo, verifique a sua conexão com internet.");
		elemento.text("Falha ao inicilizar o Estalo, verifique a sua conexão com internet.");
		return false;
	}
	xmlhttp.open("GET", urly, true);
	xmlhttp.send();
}
//}

//26/08/2014 mensagem de erro
function retornaMsgErro(err, msg, logar) {
	if (logar === undefined) $('#logar').prop('disabled', 'disabled');

	if (err.substring(0, 13) === 'Invalid state') {
		$('#statusCache').html("&nbsp;&nbsp;Erro de conexão com banco de dados");
		$('.login form label').text("Erro de conexão");
	} else {
		$('#statusCache').html("&nbsp;&nbsp;Erro detectado");
	}

	if (msg) {
		$('.login form label').text(err);
	}


	if (fs.existsSync(tempDir() + 'anexo.txt')) {
		fs.unlinkSync(tempDir() + 'anexo.txt');
	}
	fs.appendFileSync(tempDir() + 'anexo.txt', '<!DOCTYPE html><html data-ng-app="Estalo" lang="pt-br"><head><meta charset="utf-8" name="viewport" content="initial-scale=1.0, user-scalable=no"  /><title>Estalo</title><link rel="stylesheet" href="http://www.versatec.com.br/oi2/css/estilo1.css" /><link rel="stylesheet" href="http://www.versatec.com.br/oi2/css/estilo2.css" /></head><body><body>' + $('body').html() + '</body></html>');

	var stmt = db.use + " insert into emails values(GETDATE(),'" + loginUsuario + "','" + dominioUsuario + "','" + err + "',@txt)";
	executaThreadBasico('extratorDiaADia2', stringParaExtrator(stmt, []), function (dado) {
		console.log(dado)
	});
	aplStatusMail = false;
}


var partName = formataData(new Date()).replace(new RegExp('-', 'g'), '_');

var gui = require('nw.gui'); //or global.window.nwDispatcher.requireNwGui() (see https://github.com/rogerwang/node-webkit/issues/707)

// Get the current window
var win = gui.Window.get();

var tempLocation = location.hash;
if (tempLocation.substring(2, 7) == "ajuda") {} else {
	db.isConnected() ? carregaCache() : db.connect(config, carregaCache);
}

//10/10/2016
function retornaPaginas(tipo) {
	var pages = [];
	for (i = 0; i < rotas.length; i++) {
		if (rotas[i].grupo == tipo) {
			pages.push({
				nome: rotas[i].nome,
				url: rotas[i].url
			});
		}
	}
	return pages;
}

// Função que abre AJUDA
var abreAjuda = function (link) {
	var guiAjuda = require('nw.gui');
	var winAjuda = guiAjuda.Window.get();
	var new_winAjuda = guiAjuda.Window.open('ajuda?link=' + link, {
		toolbar: false,
		name: 'ajuda',
		main: 'ajuda.html',
		position: 'center',
		width: 1024,
		height: 600
	});
};
// FIM função Ajuda

function eventoPlaceHolder() {
	$('.filtro-chamador').attr("placeholder", "Protocolo")
}

function obtemUCIDAntigo(s, apl) {
	if (apl === "MENUOI") return s;
	if (!s.match(/^16C/)) return s;
	var URAs = {
		K2: "APPCTX27RJOMK",
		J6: "APPCTX28RJOMK",
		E2: "URACTX01BHZPA",
		E3: "URACTX01BSBPA",
		E4: "URACTX01CGRPA",
		E5: "URACTX01CTAPA",
		E6: "URACTX01FORPA",
		E7: "URACTX01POAPA",
		F5: "URACTX01RECPA",
		A0: "URACTX01RJOED",
		E8: "URACTX01RJOPA",
		E9: "URACTX01RJOPB",
		F0: "URACTX01SALPA",
		F1: "URACTX01SPOPA",
		A1: "URACTX02BHZPA",
		A2: "URACTX02BSBPA",
		A3: "URACTX02CGRPA",
		A4: "URACTX02CTAPA",
		A5: "URACTX02FORPA",
		A6: "URACTX02POAPA",
		F7: "URACTX02RJOED",
		A7: "URACTX02RJOPA",
		A8: "URACTX02RJOPB",
		A9: "URACTX02SALPA",
		F2: "URACTX02SPOPA",
		B0: "URACTX03BHZPA",
		B1: "URACTX03BSBPA",
		B2: "URACTX03CGRPA",
		B3: "URACTX03CTAPA",
		B4: "URACTX03FORPA",
		B5: "URACTX03POAPA",
		F8: "URACTX03RJOED",
		F9: "URACTX03RJONT",
		B6: "URACTX03RJOPA",
		B7: "URACTX03RJOPB",
		B8: "URACTX03SALPA",
		F3: "URACTX03SPOPA",
		K1: "URACTX04BHZPA",
		B9: "URACTX04CTAPA",
		C0: "URACTX04FORPA",
		F6: "URACTX04RECPA",
		G0: "URACTX04RJOED",
		G1: "URACTX04RJONT",
		C1: "URACTX04RJOPA",
		C2: "URACTX04RJOPB",
		C3: "URACTX04SALPA",
		F4: "URACTX04SPOPA",
		C4: "URACTX05BHZPA",
		C5: "URACTX05FORPA",
		G2: "URACTX05RJOED",
		G3: "URACTX05RJONT",
		C6: "URACTX05RJOPA",
		C7: "URACTX05RJOPB",
		C8: "URACTX06BSBPA",
		C9: "URACTX06CGRPA",
		D0: "URACTX06POAPA",
		G4: "URACTX06RJOED",
		G5: "URACTX06RJONT",
		D1: "URACTX07BSBPA",
		D2: "URACTX07CGRPA",
		G6: "URACTX07RJOED",
		G7: "URACTX07RJONT",
		D3: "URACTX08CTAPA",
		D4: "URACTX08POAPA",
		G8: "URACTX08RJOED",
		G9: "URACTX08RJONT",
		D5: "URACTX09BSBPA",
		D6: "URACTX09CGRPA",
		H0: "URACTX09RJOED",
		H1: "URACTX09RJONT",
		D7: "URACTX09RJOPB",
		D8: "URACTX10POAPA",
		H2: "URACTX10RJOED",
		H3: "URACTX10RJONT",
		D9: "URACTX10RJOPA",
		E0: "URACTX11CTAPA",
		H4: "URACTX11RJOED",
		H5: "URACTX11RJONT",
		E1: "URACTX12CTAPA",
		H6: "URACTX12RJONT",
		H7: "URACTX13RJONT",
		H8: "URACTX14RJONT",
		H9: "URACTX15RJONT",
		I0: "URACTX16RJONT",
		I1: "URACTX17RJONT",
		I2: "URACTX18RJONT",
		I3: "URACTX19RJONT",
		I4: "URACTX20RJONT",
		I5: "URAOIM02SPOAL",
		I6: "URAOIM03SPOAL",
		I7: "URAOIM05SPOAL",
		I8: "URAOIM06SPOAL",
		I9: "URAOIM07SPOAL",
		J0: "URAOIM08SPOAL",
		J1: "URAOIM09SPOAL",
		J2: "URAOIM10SPOAL",
		J3: "URAOIM11SPOAL",
		J4: "URAOIM11SPOLP",
		J5: "URAOIM12SPOLP",
		J7: "URAOIM13SPOAL",
		J8: "URAOIM14SPOAL",
		J9: "URAOIM14SPOLP",
		K0: "URAOIM15SPOLP"
	};
	var hostname = URAs[s.substring(9, 11)];
	if (hostname === undefined) return s;
	var _ = function (c) {
		var d = {
			A: 10,
			B: 11,
			C: 12,
			D: 13,
			E: 14,
			F: 15,
			G: 16,
			H: 17,
			I: 18,
			J: 19,
			K: 20,
			L: 21,
			M: 22,
			N: 23,
			O: 24,
			P: 25,
			Q: 26,
			R: 27,
			S: 28,
			T: 29,
			U: 30,
			V: 31,
			W: 32,
			X: 33,
			Y: 34,
			Z: 35
		};
		return _02d(c.match(/^\d$/) ? +c : d[c]);
	};
	// NVP
	if (apl === "NOVA880" || apl === "3000R1" || apl === "3000R3" || apl === "URA804" || apl === "CORPO") {
		return "U" + hostname.substring(11, 13) + _(s.substring(2, 3)) + _(s.substring(3, 4)) + _(s.substring(4, 5)) + s.substring(5, 9) + hostname.substring(6, 8) + s.substring(11);
	}
	// NGR
	return _(s.substring(2, 3)) + _(s.substring(3, 4)) + _(s.substring(4, 5)) + s.substring(5, 9) + hostname.substring(6, 9) + s.substring(11);
}

//{ Exporta XLSX
function exportaXLSX(titulo, linhas, nomeExportacao, nomeUsuario) {
	linhas = linhas.map(function (colunas) {
		return colunas.map(function (v) {
			return {
				value: v,
				autoWidth: true
			};
		});
	});

	var planilha = {
		creator: "Estalo",
		lastModifiedBy: nomeUsuario || "Estalo",
		worksheets: [{
			name: titulo,
			data: linhas,
			table: true
		}],
		autoFilter: false,
		// Não incluir a linha do título no filtro automático
		dataRows: {
			first: 1
		}
	};

	var xlsx = frames["xlsxjs"].window.xlsx;
	planilha = xlsx(planilha, 'binary');

	var caminho = nomeExportacao + "_" + formataDataHoraMilis(new Date()) + ".xlsx";
	fs.writeFileSync(caminho, planilha.base64, 'base64');
	childProcess.exec(caminho);
}
//}
var Estalo = angular.module("Estalo", ['ngGrid', 'ngSanitize','ngRoute', 'ui.grid', 'ui.grid.pinning','ui.grid.resizeColumns','ui.grid.exporter','ui.grid.selection','ui.grid.autoResize','ui.select','ui.grid.grouping','ui.grid.moveColumns']);

// Testa se é numero
(function () {
  var _$locale = {
  "NUMBER_FORMATS": {
	"CURRENCY_SYM": "R$",
	"DECIMAL_SEP": ",",
	"GROUP_SEP": ".",
	"PATTERNS": [
	  {
		"gSize": 3,
		"lgSize": 3,
		"maxFrac": 3,
		"minFrac": 0,
		"minInt": 1,
		"negPre": "-",
		"negSuf": "",
		"posPre": "",
		"posSuf": ""
	  },
	  {
		"gSize": 3,
		"lgSize": 3,
		"maxFrac": 2,
		"minFrac": 2,
		"minInt": 1,
		"negPre": "\u00a4-",
		"negSuf": "",
		"posPre": "\u00a4",
		"posSuf": ""
	  }
	]
  }
};

var DECIMAL_SEP = '.';
function formatNumber(number, pattern, groupSep, decimalSep, fractionSize) {
  if (!isFinite(number) || typeof number === 'Object') return '';

  var isNegative = number < 0;
  number = Math.abs(number);
  var numStr = number + '',
	  formatedText = '',
	  parts = [];

  var hasExponent = false;
  if (numStr.indexOf('e') !== -1) {
	var match = numStr.match(/([\d\.]+)e(-?)(\d+)/);
	if (match && match[2] == '-' && match[3] > fractionSize + 1) {
	  numStr = '0';
	  number = 0;
	} else {
	  formatedText = numStr;
	  hasExponent = true;
	}
  }

  if (!hasExponent) {
	var fractionLen = (numStr.split(DECIMAL_SEP)[1] || '').length;

	// determine fractionSize if it is not specified
	if (fractionSize === undefined) {
	  fractionSize = Math.min(Math.max(pattern.minFrac, fractionLen), pattern.maxFrac);
	}

	// safely round numbers in JS without hitting imprecisions of floating-point arithmetics
	// inspired by:
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
	number = +(Math.round(+(number.toString() + 'e' + fractionSize)).toString() + 'e' + -fractionSize);

	if (number === 0) {
	  isNegative = false;
	}

	var fraction = ('' + number).split(DECIMAL_SEP);
	var whole = fraction[0];
	fraction = fraction[1] || '';

	var i, pos = 0,
		lgroup = pattern.lgSize,
		group = pattern.gSize;

	if (whole.length >= (lgroup + group)) {
	  pos = whole.length - lgroup;
	  for (i = 0; i < pos; i++) {
		if ((pos - i)%group === 0 && i !== 0) {
		  formatedText += groupSep;
		}
		formatedText += whole.charAt(i);
	  }
	}

	for (i = pos; i < whole.length; i++) {
	  if ((whole.length - i)%lgroup === 0 && i !== 0) {
		formatedText += groupSep;
	  }
	  formatedText += whole.charAt(i);
	}

	// format fraction part.
	while(fraction.length < fractionSize) {
	  fraction += '0';
	}

	if (fractionSize && fractionSize !== "0") formatedText += decimalSep + fraction.substr(0, fractionSize);
  } else {

	if (fractionSize > 0 && number > -1 && number < 1) {
	  formatedText = number.toFixed(fractionSize);
	}
  }

  parts.push(isNegative ? pattern.negPre : pattern.posPre);
  parts.push(formatedText);
  parts.push(isNegative ? pattern.negSuf : pattern.posSuf);
  return parts.join('');
}

  Estalo.filter('numeroOuNao', function () {
	var formats = _$locale.NUMBER_FORMATS;
	return function (x, casasDec) {
	  // se não for número, retorna inalterado
	  return (typeof x !== "number") ? x :
		formatNumber(x, formats.PATTERNS[0], formats.GROUP_SEP, formats.DECIMAL_SEP, casasDec);
	};
  });

})();

(function () {
Estalo.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://srv*.assets.example.com/**',
     'http://www.versatec.com.br/*',
    'https://www.versatec.com.br/*',
     'http://www.versatec.com.br/**',
    'https://www.versatec.com.br/**',
        '*'
  ]);
  });
})();

var rotas = [
  { url: "/chamadas", controller: CtrlChamadas, templateUrl: base + "chamadas.html", nome: "Detalhamento de Chamadas", form: "frmPesqChamURAVoz", ordem: 1, grupo: "Chamadas" },
  { url: "/gra", controller: CtrlGra, templateUrl: base + "gra-v6.html", nome: "Contingências GRA", form: "frmGra", ordem: 2, grupo: "Parâmetros" },
  { url: "/resumo880144", controller: CtrlResumo880x144, templateUrl: base + "resumo-880-144.html", nome: "Resumo 880 (3000) x 144", form: "FrmResumo880_144", ordem: 3, grupo: "Chamadas" },
  { url: "/resumoph", controller: CtrlResumoPH, templateUrl: base + "resumo-ph.html", nome: "Resumo Hora a Hora", form: "frmVisaoHoraGeral", ordem: 4, grupo: "Chamadas" },
  { url: "/resumodd", controller: CtrlResumoDD, templateUrl: base + "resumo-dd.html", nome: "Resumo Dia a Dia", form: "FrmDiaaDiaGeral", ordem: 6, grupo: "Chamadas" },
  { url: "/resumoed", controller: CtrlED, templateUrl: base + "resumo-ed.html", nome: "Resumo por Estado de Diálogo", form: "FrmVisaoEstados", ordem: 8, grupo: "Estados de diálogo" },
  { url: "/consolidadoed", controller: CtrlConsolidadoED, templateUrl: base + "consolidado-ed.html", nome: "Consolidado por Estado de Diálogo", form: "frmConsTotRotinaDetalhado", ordem: 9, grupo: "Estados de diálogo" },
  { url: "/transferid", controller: CtrlTransferID, templateUrl: base + "transferID.html", nome: "Resumo por TransferID e Ponto de Derivação", form: "frmTransferId", ordem: 10, grupo: "Derivação" },
  { url: "/derivacaovdn", controller: CtrlDerivacaoVDN, templateUrl: base + "derivacao-vdn.html", nome: "Derivações por VDN", form: "frmConsolTransfURA", ordem: 11, grupo: "Derivação" },
  { url: "/resumoVS", controller: CtrlResumoVS, templateUrl: base + "resumo-vs.html", nome: "Resumo de Vendas", form: "frmResumoVendas", ordem: 15, grupo: "Vendas" },
  { url: "/vendas", controller: CtrlVendas, templateUrl: base + "vendas.html", nome: "Detalhamento de Vendas", form: "frmDetalhaVendas", ordem: 16, grupo: "Vendas" },
  { url: "/performancedddud", controller: CtrlPerformanceDDDUD, templateUrl: base + "performance-dddud.html", nome: "Resumo por DDD e Último Digito", form: "frmResumoAnalitico", ordem: 17, grupo: "Chamadas" },
  { url: "/resumoic", controller: CtrlResumoIC, templateUrl: base + "resumo-ic.html", nome: "Resumo por Item de Controle", form: "frmConsultaItemControle", ordem: 18, grupo: "Itens de controle" },
  { url: "/extratores", controller: CtrlExtratores, templateUrl: base + "extratores.html", nome: "Extrator Geral", form: "frmPesqChamURAVoz", ordem: 19, grupo: "Extratores" },
  { url: "/resumoporpromo", controller: CtrlResumoPorPromo, templateUrl: base + "resumo-por-promo.html", nome: "Resumo por Promoção Vendas", form: "frmResumoPromocaoVendas", ordem: 20, grupo: "Vendas" },
  { url: "/derivacoesrepetidas", controller: CtrlDerivacoesR, templateUrl: base + "derivacoes-repetidas.html", nome: "Resumo de Repetidas", form: "frmRepetidas", ordem: 21, grupo: "Repetidas" },
  { url: "/derivacoesrepetidastelevendas", controller: CtrlDerivacoesRTL, templateUrl: base + "derivacoes-repetidas-televendas.html", nome: "Resumo de Repetidas de Televendas", form: "frmRepetidasTLV", ordem: 22, grupo: "Repetidas" },
  { url: "/derivacoesrepetidasfinaltel", controller: CtrlDerivacoesRUD, templateUrl: base + "derivacoes-repetidas-finaltel.html", nome: "Resumo de Repetidas por Último Dígito", form: "frmRepetidasUD", ordem: 23, grupo: "Repetidas" },
  { url: "/gestaoeds", controller: CtrlGestaoED, templateUrl: base + "gestao-eds.html", nome: "Gestão de Estados de Diálogo", form: "frmGestaoED", ordem: 24, grupo: "Parâmetros" },
  { url: "/resumofalhas", controller: CtrlResumoFalhas, templateUrl: base + "resumo-falhas.html", nome: "Resumo de Falhas", form: "frmResumoFalhas", ordem: 25, grupo: "Falhas e Legados" },
  { url: "/resumoMenuOi-USSD", controller: CtrlMenuOiUssd, templateUrl: base + "resumoMenuOi-USSD.html", nome: "Resumo MenuOi x USSD", form: "frmResumoMenuOi-USSD", ordem: 27, grupo: "Vendas" },
  { url: "/cadastroXRecarga", controller: CtrlCadastroXRecarga, templateUrl: base + "cadastroXRecarga.html", nome: "Detalhamento Cadastro x Recarga", form: "frmCadastroRecarga", ordem: 28, grupo: "Vendas" },
  { url: "/rescadastroXRecarga", controller: CtrlResCadastroXRecarga, templateUrl: base + "resCadastroXRecarga.html", nome: "Resumo Cadastro x Recarga", form: "frmCadastroRecarga", ordem: 29, grupo: "Vendas" },
  { url: "/resumoIncentivo", controller: CtrlResIncentivo, templateUrl: base + "resumoIncentivo.html", nome: "Resumo por Incentivo", form: "frmIncentivo", ordem: 30, grupo: "Vendas" },
  { url: "/resumoCluster", controller: CtrlCliUniPorCluster, templateUrl: base + "resumoCluster.html", nome: "Clientes Únicos por Cluster", form: "frmCliUniPorCluster", ordem: 31, grupo: "Chamadas" },
  { url: "/estaloAnalitico", controller: CtrlEstaloAnalise, templateUrl: base + "estaloAnalise.html", nome: "Ferramenta Analítica do Estalo", form: "formDev", ordem: 26, grupo: "Administrativo" },
  { url: "/reparo", controller: CtrlReparo, templateUrl: base + "reparo.html", nome: "Gestão de Reparos", form: "frmReparo", ordem: 33, grupo: "Parâmetros" },
  { url: "/falhasCallLogs", controller: CtrlResumoFalhasCallLogs, templateUrl: base + "resumoFalhasCallLogs.html", nome: "Resumo de Falhas por Calllogs", form: "frmFalhasCallLogs", ordem: 34, grupo: "Falhas e Legados" },
  { url: "/resumoPesqServ", controller: CtrlResumoPesqServ, templateUrl: base + "resumo-por-pesquisa-servico.html", nome: "Resumo Pesquisa de Satisfação por Serviço", form: "frmResumoPesqServ", ordem: 34, grupo: "Chamadas" },
  { url: "/resumoVendaFalhaSucesso", controller: CtrlResumoVendasFalhaSucesso, templateUrl: base + "resumo-venda-falha-sucesso.html", nome: "Resumo de Vendas (falha / sucesso)", form: "frmResumoVendaFalhaSucesso", ordem: 35, grupo: "Vendas" },
  { url: "/resumoEmpresaDestino", controller: CtrlResumoEmpresaDestino, templateUrl: base + "resumo-por-empresa-destino.html", nome: "Resumo Pré-Roteamento", form: "frmResumoEmpresaDestino", ordem: 35, grupo: "Chamadas" },
  { url: "/ajuda", controller: CtrlAjuda, templateUrl: base + "ajuda.html", nome: "Ajuda", form: "frmPesqChamURAVoz", ordem: 36, grupo: "Suporte"},
  { url: "/monitorReparo", controller: CtrlMonitorReparo, templateUrl: base + "monitorReparo.html", nome: "Reparo", form: "formDev", ordem: 37, grupo: "Monitoração" },
  { url: "/monitorBilhetagem", controller: CtrlMonitorBilhetagem, templateUrl: base + "monitorBilhetagem.html", nome: "Monitor de bilhetagem", form: "frmMonitorBilhetagem", ordem: 52, grupo: "Monitoração" },
  //{CTG 1
  { url: "/contGlobalTransf", controller: CtrlContGlobalTransf, templateUrl: base + "contGlobalTransf.html", nome: "Contagem Global de Transferências", form: "ContGlobalTransf", ordem: 38, grupo: "Cradle to grave" },
  { url: "/resumoContagemPorProdXGrupoAtend", controller: CtrlResumoContProdXGrupAtend, templateUrl: base + "resumo-contagem-por-prod-x-grupo-atend.html", nome: "Resumo Contagem Produto x Grupo Atend.", form: "ResumoContagemPorProdXGrupoAtend", ordem: 39, grupo: "Cradle to grave" },
  { url: "/resumoEmpresa", controller: CtrlResumoEmpresa, templateUrl: base + "resumo-empresa.html", nome: "Resumo por Empresa", form: "ResumoEmpresa", ordem: 40, grupo: "Cradle to grave" },
  { url: "/resumoPdXGrupo", controller: CtrlResumoPontoDerGrupo, templateUrl: base + "resumo-pontoder-grupo.html", nome: "Resumo Ponto de Der. x Grupo de Atend.", form: "ResumoPdXGrupo", ordem: 41, grupo: "Cradle to grave" },
  { url: "/chamadasctg", controller: CtrlChamadasCTG, templateUrl: base + "chamadas-ctg.html", nome: "Detalhamento de Chamadas", form: "Chamadasctg", ordem: 42, grupo: "Cradle to grave"},
  { url: "/chamadasctgnaocruzadas", controller: CtrlChamadasCTGNaoCruzadas, templateUrl: base + "chamadas-ctg-naocruzadas.html", nome: "Detalhamento de Chamadas Não Cruzadas", form: "Chamadasctg", ordem: 42, grupo: "Cradle to grave"},
  { url: "/resumoURAxECH", controller: CtrlResumoUraxECH, templateUrl: base + "resumoURAxECH.html", nome: "Resumo URA x ECH", form: "ResumoURAxECH", ordem: 43, grupo: "Cradle to grave" },
  { url: "/resumoTransferenciasOperacao", controller: CtrlResumoTransferenciasOperacao, templateUrl: base + "resumo-transf-operacao.html", nome: "Resumo de Transferências por Operação", form: "ResumoTransferenciasOperacao", ordem: 45, grupo: "Cradle to grave" },
  { url: "/resumoTransferenciasED", controller: CtrlResumoTransferenciasED, templateUrl: base + "resumo-transf-ed.html", nome: "Resumo de Transferências por ED", form: "ResumoTransferenciasED", ordem: 46, grupo: "Cradle to grave" },
  { url: "/resumoTransferenciasTransferID", controller: CtrlResumoTransferenciasTransferID, templateUrl: base + "resumo-transf-transferid.html", nome: "Resumo de Transferências por TransferID", form: "ResumoTransferenciasTransferID", ordem: 47, grupo: "Cradle to grave" },
  //}CTG 1
  //{CTG 2
  { url: "/contGlobalTransf2", controller: CtrlContGlobalTransf2, templateUrl: base + "contGlobalTransf.html", nome: "Contagem Global de Transferências", form: "ContGlobalTransf2", ordem: 38, grupo: "Cradle to grave 2" },
  { url: "/resumoContagemPorProdXGrupoAtend2", controller: CtrlResumoContProdXGrupAtend2, templateUrl: base + "resumo-contagem-por-prod-x-grupo-atend.html", nome: "Resumo Contagem Produto x Grupo Atend.", form: "ResumoContagemPorProdXGrupoAtend2", ordem: 39, grupo: "Cradle to grave 2" },
  { url: "/resumoEmpresa2", controller: CtrlResumoEmpresa2, templateUrl: base + "resumo-empresa.html", nome: "Resumo por Empresa", form: "ResumoEmpresa2", ordem: 40, grupo: "Cradle to grave 2" },
  { url: "/resumoPdXGrupo2", controller: CtrlResumoPontoDerGrupo2, templateUrl: base + "resumo-pontoder-grupo.html", nome: "Resumo Ponto de Der. x Grupo de Atend.", form: "ResumoPdXGrupo2", ordem: 41, grupo: "Cradle to grave 2" },
  { url: "/chamadasctg2", controller: CtrlChamadasCTG2, templateUrl: base + "chamadas-ctg.html", nome: "Detalhamento de Chamadas", form: "Chamadasctg", ordem: 42, grupo: "Cradle to grave 2"},
  { url: "/chamadasctgnaocruzadas2", controller: CtrlChamadasCTGNaoCruzadas2, templateUrl: base + "chamadas-ctg-naocruzadas.html", nome: "Detalhamento de Chamadas Não Cruzadas", form: "Chamadasctg2", ordem: 42, grupo: "Cradle to grave 2"},
  { url: "/resumoURAxECH2", controller: CtrlResumoUraxECH2, templateUrl: base + "resumoURAxECH.html", nome: "Resumo URA x ECH", form: "ResumoURAxECH2", ordem: 43, grupo: "Cradle to grave 2" },
  { url: "/resumoTransferenciasOperacao2", controller: CtrlResumoTransferenciasOperacao2, templateUrl: base + "resumo-transf-operacao.html", nome: "Resumo de Transferências por Operação", form: "ResumoTransferenciasOperacao2", ordem: 45, grupo: "Cradle to grave 2" },
  { url: "/resumoTransferenciasED2", controller: CtrlResumoTransferenciasED2, templateUrl: base + "resumo-transf-ed.html", nome: "Resumo de Transferências por ED", form: "ResumoTransferenciasED2", ordem: 46, grupo: "Cradle to grave 2" },
  { url: "/resumoTransferenciasTransferID2", controller: CtrlResumoTransferenciasTransferID2, templateUrl: base + "resumo-transf-transferid.html", nome: "Resumo de Transferências por TransferID", form: "ResumoTransferenciasTransferID2", ordem: 47, grupo: "Cradle to grave 2" },
  { url: "/resumoECH", controller: CtrlResumoECH, templateUrl: base + "resumo-ech.html", nome: "Resumo ECH", form: "frmResumoECH", ordem: 47, grupo: "Cradle to grave 2" },
  //}CTG 2
  { url: "/resumoRaco", controller: CtrlResumoRaco, templateUrl: base + "resumo-raco.html", nome: "Resumo de Cancelamento", form: "frmRaco", ordem: 48, grupo: "Itens de controle" },
  { url: "/resumoAutomacoes", controller: CtrlResumoAutomacoes, templateUrl: base + "resumo-automacoes.html", nome: "Resumo de Automações", form: "frmConsultaItemControle", ordem: 51, grupo: "Itens de controle" },
  { url: "/cadastroUsuario", controller: CtrlUser, templateUrl: base + "cadastroUsuario.html", nome: "Cadastro de Usuário", form: "frmCadastroUser", ordem: 52, grupo: "Administrativo" },
  { url: "/resumoErrosPreRouting", controller: CtrlResumoErrosPreRouting, templateUrl: base + "resumoErrosPreRouting.html", nome: "Resumo de Erros de Pré Roteamento", form: "frmRoteamento", ordem: 51, grupo: "Chamadas" },
  { url: "/cadastroPrompt", controller: CtrlPrompt, templateUrl: base + "cadastroPrompt.html", nome: "Cadastro de Prompt", form: "formCadastro", ordem: 53, grupo: "Administrativo" },
  { url: "/cadastroIC", controller: CtrlIC, templateUrl: base + "cadastroIC.html", nome: "Cadastro de IC", form: "formCadastro", ordem: 54, grupo: "Administrativo" },
  { url: "/solicitaUsuario", controller: CtrlUserRequest, templateUrl: base + "solicitaUsuario.html", nome: "Solicitação de Cadastro", form: "formCadastroUser", ordem: 55, grupo: "Administrativo" },
  { url: "/resumoTabs", controller: CtrlResumoTabs, templateUrl: base + "resumo-tabs.html", nome: "Resumo de Tabs", form: "formDev", ordem: 49, grupo: "Administrativo" },
  { url: "/detalhamentoNaoAbordados", controller: CtrlNaoAbordados, templateUrl: base + "detalhamentoNaoAbordados.html", nome: "Detalhamento Clientes não Abordados", form: "frmCliNaoAbord", ordem: 50, grupo: "Vendas" },
  { url: "/parametroGra", controller: CtrlParametroGra, templateUrl: base + "parametroGra.html", nome: "Cadastro de Parâmetros GRA", form: "frmParametroGra", ordem: 50, grupo: "Administrativo" },
  { url: "/CtrlAlerts", controller: CtrlAlerts, templateUrl: base + "CtrlAlerts.html", nome: "Controle de Alertas", form: "formDev", ordem: 56, grupo: "Administrativo" },
  { url: "/controleAnalitico", controller: CtrlAnalitico, templateUrl: base + "controleAnalitico.html", nome: "Controle Analítico do Estalo", form: "formDev", ordem: 56, grupo: "Administrativo" },
  { url: "/criaGra", controller: CtrlCriaGra, templateUrl: base + "criaGra.html", nome: "Cadastro de GRAs", form: "frmCriaGra", ordem: 50, grupo: "Administrativo" }  ,
  { url: "/resumoDDTelaUnica", controller: CtrlResumoDDTelaUnica, templateUrl: base + "resumoDDTelaUnica.html", nome: "Resumo Dia a Dia", form: "frmConsultaItemControle", ordem: 51, grupo: "Tela Única" } ,
  { url: "/resumoDesbloqueioTelaUnica", controller: CtrlResumoDesbloqueioTelaUnica, templateUrl: base + "resumoDesbloqueioTelaUnica.html", nome: "Resumo Desbloqueio Tela Única", form: "frmConsultaItemControle", ordem: 51, grupo: "Tela Única" } ,
  { url: "/InteratividadeTelaUnica", controller: CtrlInteratividadeTelaUnica, templateUrl: base + "InteratividadeTelaUnica.html", nome: "Detalhamento de Interatividade", form: "frmInteratividade", ordem: 53, grupo: "Tela Única" } ,
  { url: "/resumoEPSTelaUnica", controller: CtrlResumoEPSTelaUnica, templateUrl: base + "resumoEPSTelaUnica.html", nome: "Resumo por EPS", form: "frmConsultaItemControle", ordem: 52, grupo: "Tela Única" } ,
  { url: "/resumoPromocaoTelaUnica", controller: CtrlResumoPromocaoTelaUnica, templateUrl: base + "resumoPromocaoTelaUnica.html", nome: "Resumo por Promoções", form: "frmConsultaItemControle", ordem: 52, grupo: "Tela Única" } ,
  { url: "/produtosURA", controller: CtrlProdutosUra, templateUrl: base + "ProdutosURA.html", nome: "Cadastro de Produtos Vendidos", form: "frmPesqChamURAVoz", ordem: 50, grupo: "Administrativo" } ,
  { url: "/resumoFontes", controller: CtrlResumoFontes, templateUrl: base + "resumoFontes.html", nome: "Resumo de Fontes", form: "frmPesqChamURAVoz", ordem: 48, grupo: "Cadastro e Migração" },
  { url: "/resumoDerivacoes", controller: CtrlResumoDerivacoes, templateUrl: base + "resumoDerivacoes.html", nome: "Resumo de Derivações", form: "frmPesqChamURAVoz", ordem: 49, grupo: "Cadastro e Migração" },
  { url: "/resumoCadastro", controller: CtrlResumoCadastro, templateUrl: base + "resumoCadastro.html", nome: "Resumo Cadastro/Migração", form: "frmPesqChamURAVoz", ordem: 50, grupo: "Cadastro e Migração" } ,
  { url: "/infoFaltanteLog", controller: CtrlInfFaltLog, templateUrl: base + "info-faltante-log.html", nome: "Informações faltantes nos logs", form: "frmInfoFaltLog", ordem: 51, grupo: "Chamadas" },
  { url: "/retencaoLiquida", controller: CtrlRetencaoLiquida, templateUrl: base + "retencao-liquida.html", nome: "Resumo de repetidas do reset", form: "frmResumoRetencao", ordem: 52, grupo: "Repetidas" },
  { url: "/chamadasRetencaoLiquida", controller: CtrlChamadasRetencaoLiquida, templateUrl: base + "chamadas-retencao-liquida.html", nome: "Detalhamento de Chamadas", form: "frmDetalhadoRetencao", ordem: 53, grupo: "Repetidas" }
    ];


var gruposRelatorios = [
  { codigo: "chamadas", titulo: "Chamadas", div: 'dvchamadas', pos: 15 },
  { codigo: "repetidas", titulo: "Repetidas", div: 'dvRepetidas', pos: 35 },
  { codigo: "ed", titulo: "Estados de Diálogo", div: 'dvED', pos: 65 },
  { codigo: "ic", titulo: "Item de Controle", div: 'dvIC', pos: 95 },
  { codigo: "tid", titulo: "Transfer Id e VDN", div: 'dvTid', pos:115 },
  { codigo: "vendas", titulo: "Vendas", div: 'dvVendas', pos: 145 },
  { codigo: "falhas", titulo: "Falhas e Legados", div: 'dvFalhas', pos: 165 },
  { codigo: "extratores", titulo: "Extratores", div: 'dvExtratores', pos: 195 },
  { codigo: "parametros", titulo: "Parâmetros", div: 'dvParam', pos: 225 },
  { codigo: "admin", titulo: "Administrativo", div: 'dvAdmin', pos: 255 },
  { codigo: "monitoracao", titulo: "Monitoração", div: 'dvReparo', pos: 275 },
  { codigo: "cradleToGrave", titulo: "Cradle to Grave", div: 'dvcontGlobalTransf', pos: 295 },
  { codigo: "cradleToGrave 2", titulo: "Cradle to Grave 2", div: 'dvcontGlobalTransf', pos: 295 },
  { codigo: "uracadastro", titulo: "Cadastro e Migração", div: 'dvCadastro', pos: 325 }
];

var urlsRotas = [];
var templatesRotas =[];
try {
  urlsRotas = rotasInit.map(function(rt){return rt.url});
  templatesRotas = rotasInit.map(function(rt){return rt.templateUrl});
} catch (ex) {
  console.log(ex);
}

// Rotas
// URL -> {template, controller}
function configRoutes($routeProvider) {

	 // $locationProvider.html5Mode({
		   // enabled: true,
  // requireBase: false

	 // });
  rotas.forEach(function (rota) {
	//console.log(templatesRotas[urlsRotas.indexOf(rota.url)]);
	$routeProvider.when(rota.url, {
	  controller: rota.controller,
	  templateUrl: urlsRotas.indexOf(rota.url) >= 0 ? (base + templatesRotas[urlsRotas.indexOf(rota.url)]) : rota.templateUrl
	});
  });
  $routeProvider.otherwise({
	redirectTo: "/chamadas" // "/"
  });
}

Estalo.config(['$routeProvider','$locationProvider', configRoutes]);

Estalo.factory('$globals', function () {
  return { numeroDeRegistros: 0, //Número de registros de uma consulta usado para atualizar barra de progresso
	pivot: false,
	agrupar: false, // Passando valor de checkbox para outro relatório
	intervalo: "",
	condicional: true,
	logado: false,
	viewAlerta: 0
  };
});

Estalo.connectionPool = {}; // preencher com objetos DB
Estalo.globalEvents = new (require('events').EventEmitter)();

var bkpCache;

if (fs.existsSync(tempDir()+"infoCache")) {
  try {
  var texto =  fs.readFileSync(tempDir()+"infoCache", "utf-8");
  texto = JSON.parse(texto);
  bkpCache = texto.cache;
  } catch (ex) {
	console.log(ex.message);
	fs.unlinkSync(tempDir()+"infoCache");
  }
} else {
  var texto = {"cache": false, "relatorios": {"planilhaDeVendas":{"erros":{"01":0,"02":0,"03":0,"04":0,"05":0,"06":0,"07":0,"08":0,"09":0,"10":0,"11":0,"12":0},"minMax":{"01":{"min":"","max":""},"02":{"min":"","max":""},"03":{"min":"","max":""},"04":{"min":"","max":""},"05":{"min":"","max":""},"06":{"min":"","max":""},"07":{"min":"","max":""},"08":{"min":"","max":""},"09":{"min":"","max":""},"10":{"min":"","max":""},"11":{"min":"","max":""},"12":{"min":"","max":""}}}}};
  fs.writeFileSync(tempDir()+"infoCache", JSON.stringify(texto));
  bkpCache = false;
};

var arquivoCache = bkpCache;

if (!bkpCache && fs.existsSync(tempDir()+"cache.tmp")){
  fs.unlinkSync(tempDir()+"cache.tmp");
}

function iniciaFiltros() {
	Estalo.filtros = {};
	Estalo.filtros.filtro_data_hora = [];
	Estalo.filtros.filtro_final = [];
	Estalo.filtros.filtro_aplicacoes_vendas = [];
	Estalo.filtros.filtro_aplicacoes = [];
	Estalo.filtros.filtro_sites = [];
	Estalo.filtros.filtro_segmentos = [];
	Estalo.filtros.filtro_ddds = [];
	Estalo.filtros.filtro_grupos_produtos = [];
	Estalo.filtros.filtro_produtos = [];
	Estalo.filtros.filtro_erros = [];
	Estalo.filtros.filtro_resultados = [];
	Estalo.filtros.filtro_operacoes = [];
	Estalo.filtros.filtro_regioes = [];
	Estalo.filtros.filtro_ufs = [];
	Estalo.filtros.filtro_ed = [];
	Estalo.filtros.filtro_ic = [];
	Estalo.filtros.filtro_ud = [];
	Estalo.filtros.filtro_falhas = [];
	Estalo.filtros.filtro_uras = [];
	Estalo.filtros.filtro_servicos = [];
	Estalo.filtros.filtro_empresas = [];
	Estalo.filtros.filtro_reconhecimentos = [];
	Estalo.filtros.filtro_skills = [];
	Estalo.filtros.filtro_regras = [];
	Estalo.filtros.varjson = [];
	Estalo.filtros.filtro_transferids = [];
	Estalo.filtros.filtro_edstransf = [];
	Estalo.filtros.filtro_fontes = [];

	$('select.filtro-aplicacao').val('').selectpicker('refresh');
	$('select.filtro-site').val('').selectpicker('refresh');
	$('select.filtro-falhas').val('').selectpicker('refresh');
	$('select.filtro-uras').val('').selectpicker('refresh');
	$('select.filtro-segmento').val('').selectpicker('refresh');
	$('select.filtro-empresa').val('').selectpicker('refresh');
  $('select.filtro-empresa').val('').selectpicker('refresh');
	$('select.filtro-errosRouting').val('').selectpicker('refresh');
	$('select.filtro-resultado').val('').selectpicker('refresh');
	$('select.filtro-segmento').prop('disabled',true).selectpicker('refresh');
	$('#alinkSeg').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-ddd').val('').selectpicker('refresh')
	$('select.filtro-ddd').prop('disabled',true).selectpicker('refresh');
	$('#alinkDDD').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-grupo-produto').val('').selectpicker('refresh');
	$('select.filtro-grupo-produto').prop('disabled',true).selectpicker('refresh');
	$('#alinkGru').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-produto').val('').selectpicker('refresh');
	$('select.filtro-produto').prop('disabled',true).selectpicker('refresh');
	$('#alinkProd').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-erro').val('').selectpicker('refresh');
	$('select.filtro-status').val('').selectpicker('refresh');
	$('select.filtro-empresaRecarga').val('').selectpicker('refresh');
	$('select.filtro-empresaRecarga').val('').selectpicker('refresh');
	$('select.filtro-op').val('').selectpicker('refresh');
	$('select.filtro-op').prop('disabled',true).selectpicker('refresh');
	$('#alinkOp').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-regiao').val('').selectpicker('refresh');
	$('select.filtro-uf').val('').selectpicker('refresh');
	$('select.filtro-operacao').val('').selectpicker('refresh');

	$('select.filtro-transferid').val('').selectpicker('refresh');
	$('select.filtro-transferid').prop('disabled',true).selectpicker('refresh');
	$('#alinkTid').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-edstransf').val('').selectpicker('refresh');
	$('select.filtro-edstransf').prop('disabled',true).selectpicker('refresh');
	$('#alinkEDTr').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-ed').val('').selectpicker('refresh');
	$('select.filtro-ed').prop('disabled',true).selectpicker('refresh');
	$('#HFiltroED').attr('checked',false);

	$('select.filtro-ic').val('').selectpicker('refresh');
	$('select.filtro-ic').prop('disabled',true).selectpicker('refresh');
	$('#HFiltroIC').attr('checked',false);
	$('#porRegEDDD').attr('checked',false);
	$('.chkMensal').attr('checked',false);

	$('select.filtro-ud').val('').selectpicker('refresh');

	$('select.filtro-final').val('').selectpicker('refresh');
}

iniciaFiltros();

function iniciaAgora(pag, scop) {
  var datahora = Estalo.filtros.filtro_data_hora[1];
  if (datahora === undefined) {
	datahora = formataDataHora(scop.periodo.fim);
  } else {
	datahora = formataDataHora(Estalo.filtros.filtro_data_hora[1]);
  }
  var data = datahora.substring(0, 10);
  //ajustarDSTWin(true,scop);
  var newHora = new Date();
  var newHora = pad(newHora.getHours())+":"+pad(newHora.getMinutes())+":"+pad(newHora.getSeconds());
  var dat = data +" "+newHora;
  Estalo.filtros.filtro_data_hora[1] = new Date(dat);
  pag.find('.hora-fim input').val(newHora);
  scop.periodo.fim = Estalo.filtros.filtro_data_hora[1];
  scop.$apply();
}
