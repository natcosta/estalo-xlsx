function CtrlResumoTransferenciaPorTag($scope, $globals) {
  win.title="% Transferência por TAG";
  $scope.versao = versao;
  $scope.rotas = rotas;
  $scope.limite_registros = 0;
  $scope.incrementoRegistrosExibidos = 100;
  $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

  travaBotaoFiltro(0, $scope, "#pag-resumo-transferencia-por-tag", "% Transferência por TAG");

  //Alex 24/02/2014
  $scope.status_progress_bar = 0;
  $scope.aplicacoes = []; // FIXME: copy

  $scope.filtros = {
    aplicacoes: [],
    segmentos: [],
    porDia: false,
  };

  $scope.gridOptions = {
    enableColResize: true,
    enableSorting: true,
    enableFilter: true,
    suppressLoadingOverlay: true,
    suppressNoRowsOverlay: true,
    // overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    // overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">A consulta não retornou resultados</span>',
    localeText: {
      contains: 'Contém',
      notContains: 'Não contém',
      equals: 'Igual',
      notEquals: 'Diferente',
      startsWith: 'Começa com',
      endsWith: 'Termina com',

    },
    columnDefs: [
      { headerName: 'Dia', field: 'dia', cellStyle: {textAlign:'center'}, lockPosition: true },
      { headerName: 'TAG', field: 'tag', cellStyle: {textAlign:'center'}, lockPosition: true },
      { headerName: 'Total de Atendidas', field: 'atendidas', cellStyle: {textAlign:'center'}, lockPosition: true },
      { headerName: 'Total de Transferidas por operação 2', field: 'transferidas', cellStyle: {textAlign:'center'}, lockPosition: true },
      { headerName: '% de Transferência', field: 'percentual', cellStyle: {textAlign:'center'}, lockPosition: true }
    ],
    rowData: []
  };

	//Alex 02/08/2017
	$scope.intervalo = false;
	var p = cache.aclbotoes.map(function(b){ return b.idview; }).indexOf('pag-resumo-transferencia-por-tag');
	if(p >= 0){
		var teste = cache.aclbotoes[p].permissoes.split(',');
		for (var i = 0; i < teste.length; i++){
			if(teste[i] === 'intervalo'){
				$scope.intervalo = true;
			}
		}
	}

  // Filtros: data e hora
  var agora = new Date();

  var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
  var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

  $scope.periodo = {
    inicio: inicio,
    fim: fim,
    min: new Date(2013, 11, 1),
    max: agora // FIXME: atualizar ao virar o dia
  };

  var $view;

  $scope.$on('$viewContentLoaded', function () {
    $view = $("#pag-resumo-transferencia-por-tag");
    // $(".aba2").css({'position':'fixed','left':'120px','top':'42px'});
    $('.navbar-inner').css('height','70px');
    $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
    //minuteStep: 5
    
    componenteDataHora($scope, $view);

    // Carregar filtros
    carregaAplicacoes($view, false, false, $scope);
    carregaSites($view);
    carregaSegmentosPorAplicacao($scope, $view, true);
    carregaOperacoesECH($view);
    carregaDDDsPorRegiao($scope, $view, true);

    // Marca todos os operacoes
    $view.on("click", "#alinkOper", function(){ marcaTodosIndependente($('.filtro-operacao'),'operacoes')});

    // Marca todos os edstransf
    $view.on("click", "#alinkTid", function(){ marcaTodosIndependente($('.filtro-transferid'),'transferid')});

    // Marca todos os edstransf
    $view.on("click", "#alinkEDT", function(){ marcaTodosIndependente($('.filtro-edstransf'),'edstransf')});

    // Marca todos os sites
    $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

    // Marca todos os segmentos
    $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

    // Marca todos os empresas
    $view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});

    //Marcar todas aplicações
    $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});

    $view.find("select.filtro-aplicacao").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} aplicações',
      showSubtext: true
    });

    $view.find("select.filtro-site").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} Sites/POPs',
      showSubtext: true
    });
    $view.find("select.filtro-segmento").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} segmentos'
    });

    $view.find("select.filtro-ed").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} EDs'
    });

    $view.find("select.filtro-ic").selectpicker({
      selectedTextFormat: 'count',
      countSelectedText: '{0} ICs'
    });
  
    // change de filtros
    // Popula lista de segmentos a partir das aplicações selecionadas
    $view.on("change", "select.filtro-aplicacao", function () {
      // $scope.carregarICsEdsArquivo($('select.filtro-aplicacao').val());
      carregaSegmentosPorAplicacao($scope, $view);
    });

    $view.on("change", "select.filtro-segmento", function () {
      Estalo.filtros.filtro_segmentos = $(this).val() || [];
    });

    // $view.on("change", "select.filtro-ic", function () {
    //   Estalo.filtros.filtro_ic = $(this).val() || [];
    // });

    // $view.on("change", "select.filtro-ed", function () {
    //   Estalo.filtros.filtro_ed = $(this).val() || [];
    // });


    $view.on("change", "select.filtro-site", function () {
      var filtro_sites = $(this).val() || [];
      Estalo.filtros.filtro_sites = filtro_sites;
    });

    // var filtro_ics = $scope.filtroICSelect || [];
    // var filtro_eds = $scope.filtroEDSelect || [];

    $view.on("click", ".btn-exportar", function () {
      $scope.exportaXLSX.apply(this);
    });

    // Limpa filtros Alex 03/03/2014
    $view.on("click", ".btn-limpar-filtros", function () {
      // $scope.porRegEDDD = false;
      $scope.limparFiltros.apply(this);
    });

    $scope.limparFiltros = function () {
      iniciaFiltros();
      componenteDataMaisHora ($scope,$view,true);

      var partsPath = window.location.pathname.split("/");
      var part  = partsPath[partsPath.length-1];

      var $btn_limpar = $view.find(".btn-limpar-filtros");
      $btn_limpar.prop("disabled", true);
      $btn_limpar.button('loading');

      setTimeout(function(){
        $btn_limpar.button('reset');
        //window.location.href = part + window.location.hash +'/';
      },500);
    }

    // Botão agora Alex 03/03/2014
    $view.on("click", ".btn-agora", function () {
      $scope.agora.apply(this);
    });

    $scope.agora = function () {
      iniciaAgora($view,$scope);
    }

    // Botão abortar Alex 23/05/2014
    $view.on("click", ".abortar", function () {
      $scope.abortar.apply(this);
    });

    //Alex 23/05/2014
    $scope.abortar = function () {
      abortar($scope, "#pag-resumo-transferencia-por-tag");
    }

    // $scope.gridOptions.api.hideOverlay();

    // Lista chamadas conforme filtros
    $view.on("click", ".btn-gerar", function () {
      // $scope.gridOptions.api.showLoadingOverlay()
      limpaProgressBar($scope, "#pag-resumo-transferencia-por-tag");
      //22/03/2014 Testa se data início é maior que a data fim
      var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
      var testedata = testeDataMaisHora(data_ini, data_fim);
      if (testedata !== "") {
        setTimeout(function () {
          atualizaInfo($scope, testedata);
          effectNotification();
          $view.find(".btn-gerar").button('reset');
        }, 500);
        return;
      }
      $scope.listaDados.apply(this);
      // $scope.gridOptions.api.hideOverlay();
    });

    $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  });

  // Exibe mais registros
  $scope.exibeMaisRegistros = function () {
    $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
  };

  // Lista chamadas conforme filtros
  $scope.listaDados = function () {
    $globals.numeroDeRegistros = 0;
    var $btn_exportar = $view.find(".btn-exportar");
    // var $btn_exportar_csv = $view.find(".btn-exportarCSV");
    // var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
    $btn_exportar.prop("disabled", true);
    // $btn_exportar_csv.prop("disabled", true);
    // $btn_exportar_dropdown.prop("disabled", true);

    var $btn_gerar = $(this);
    $btn_gerar.button('loading');

    var data_ini = $scope.periodo.inicio;
    var data_fim = $scope.periodo.fim;

    $scope.gridOptions.rowData = [];
    $scope.gridOptions.api.setRowData([]);

    $scope.executeQuery();
    $btn_exportar.prop("disabled", false);
    // $btn_exportar_csv.prop("disabled", false);
    // $btn_exportar_dropdown.prop("disabled", false);
  };

  $scope.executeQuery = function() {
	  
	var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
	var filtro_sites = $view.find("select.filtro-site").val() || [];
	var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
  var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
  var filtro_ddd = $view.find("select.filtro-ddd").val() || [];
    var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
	    
    // var filtro_ic = $scope.filtroEscolhido.ic || [];
    // var filtro_ed = $scope.filtroEscolhido.ed || [];

    var query = "";

    if ($scope.filtros.porDia) {
      query = "SELECT DatReferencia,"
        + " ISNULL(ResultadoRecVoz, '') AS TAG,"
        + " SUM(QtdChamadas) AS 'ATENDIDAS',"
        + " SUM(QtdTransferidas) AS 'TRANSFERIDAS',"
        + " 100 * SUM(QtdTransferidas) / SUM(QtdChamadas) AS 'PERCENTUAL'"
        + " FROM ResumoECHReconhecimentoVoz2Dia"
        + " WHERE 1=1"
        + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        + restringe_consulta("CodSite", filtro_sites, true)
        + restringe_consulta("CodSegmento", filtro_segmentos, true)
        + restringe_consulta("DDD", filtro_ddd, true)
        // + restringe_consulta("CodEmpresa", filtro_empresas, true)
        + " AND DatReferencia >= '" + formataDataHora($scope.periodo.inicio).substring(0,10) + "'"
        + " AND DatReferencia <= '" + formataDataHora($scope.periodo.fim).substring(0,10) + "'"
        + " GROUP BY DatReferencia, ResultadoRecVoz"
        + " ORDER BY DatReferencia"
    } else {
      query = "SELECT DatReferencia,"
        + " ISNULL(ResultadoRecVoz, '') AS TAG,"
        + " SUM(QtdChamadas) AS 'ATENDIDAS',"
        + " SUM(QtdTransferidas) AS 'TRANSFERIDAS',"
        + " 100 * SUM(QtdTransferidas) / SUM(QtdChamadas) AS 'PERCENTUAL'"
        + " FROM ResumoECHReconhecimentoVoz2"
        + " WHERE 1=1"
        + restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        + restringe_consulta("CodSite", filtro_sites, true)
        + restringe_consulta("CodSegmento", filtro_segmentos, true)
        + restringe_consulta("DDD", filtro_ddd, true)
        // + restringe_consulta("CodEmpresa", filtro_empresas, true)
        + " AND DatReferencia >= '" + formataDataHora($scope.periodo.inicio) + "'"
        + " AND DatReferencia <= '" + formataDataHora($scope.periodo.fim) + "'"
        + " GROUP BY DatReferencia, ResultadoRecVoz"
        + " ORDER BY DatReferencia";
    }
    
    $scope.gridOptions.api.setRowData([]);
    mssqlQueryTedious(query, function(err, res) {
      if (err) {
        console.log('erro query', err);
        alerte("Erro ao efetuar a consulta", "Erro");
      }

      // if (res.length) $scope.gridOptions.api.showNoRowsOverlay();
      
      res.forEach(function(linha) {
        if($scope.filtros.porDia) {
          var ano = linha.DatReferencia.value.getUTCFullYear();
          var mes = linha.DatReferencia.value.getUTCMonth() + 1;
          var dia = linha.DatReferencia.value.getUTCDate();
          var dataGrid = dia + "/" + mes + "/" + ano;
        } else {
          var dataGrid = formataDataHoraBR(linha.DatReferencia.value);
        }

        $scope.gridOptions.rowData.push({
          dia: dataGrid,
          tag: linha.TAG.value,
          atendidas: linha.ATENDIDAS.value,
          transferidas: linha.TRANSFERIDAS.value,
          percentual: linha.PERCENTUAL.value
        })
      })
      $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);



      retornaStatusQuery(res.length, $scope);
      $('.btn-gerar').button('reset')
    })
  }

  // Exportar planilha XLSX
  $scope.exportaXLSX = function () {

    var $btn_exportar = $(this);
    $btn_exportar.button('loading');
    var linhas  = [];
    var header = [];

    for ( var i = 0 ; i < $scope.gridOptions.columnDefs.length ; i++ ) {
      header.push({
        value: $scope.gridOptions.columnDefs[i].headerName, hAlign: 'center', autoWidth: true
      })
    }
    linhas.push(header);

    $scope.gridOptions.api.getModel().rowsToDisplay.forEach(function(el) {
      var linha = [];
      linha.push({ 
        value: el.data.dia, 
        hAlign: 'center' 
      }, { 
        value: el.data.tag, 
        hAlign: 'center' 
      }, { 
        value: el.data.atendidas, 
        hAlign: 'center' 
      }, { 
        value: el.data.transferidas, 
        hAlign: 'center' 
      }, { 
        value: el.data.percentual, 
        hAlign: 'center' 
      });
      linhas.push(linha);
    })

    var planilha = {
      creator: "Estalo",
      lastModifiedBy: $scope.username || "Estalo",
      worksheets: [{ name: 'Ura Ativa', data: linhas, table: true }],
      autoFilter: false,
      // Não incluir a linha do título no filtro automático
      dataRows: { first: 1 }
    };

    var xlsx = frames["xlsxjs"].window.xlsx;
    planilha = xlsx(planilha, 'binary');

    var milis = new Date();
    var file = 'UraAtiva_' + formataDataHoraMilis(milis) + '.xlsx';

    if (!fs.existsSync(file)) {
      fs.writeFileSync(file, planilha.base64, 'base64');
      childProcess.exec(file);
    }


    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  };
}

CtrlResumoTransferenciaPorTag.$inject = ['$scope', '$globals'];
