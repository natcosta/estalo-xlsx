function CtrlCliUniPorCluster($scope, $globals) {

    win.title="Clientes únicos por cluster"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoDD"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-cluster", "Mandala");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    $scope.dados = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };


    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    $scope.diaFoco = "";
    $scope.diaFoco2 = "";

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        isHFiltroED: false,
        isHFiltroIC: false
    };

    // Filtro: ddd
    $scope.ddds = cache.ddds;


    $scope.porDDD = null;

    //02/06/2014 Flag
    $scope.porRegEDDD = $globals.agrupar;

    $scope.aba = 2;


    function geraColunas(){
      var array = [];

      array.push(
        { field: "datReferencia", displayName: "Data", width: 100, pinned: true },
        { field: "cluster", displayName: "Cluster", width: 475, pinned: true },
        { field: "qtdEntrantes", displayName: "Entrantes", width: 100, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "qtdDerivadas", displayName: "Derivadas", width: 100, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
        { field: "qtdClientesEntrantes", displayName: "Clientes únicos entrantes", width: 215, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>' },
        { field: "qtdClientesDerivados", displayName: "Clientes únicos derivados", width: 215, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>' }
      );


      return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);


    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]) : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]) : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: mesAnterior(dat_consoli),
            max: dat_consoli*/
        };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-cluster");
		  treeView('#pag-cluster #chamadas', 15,'Chamadas','dvchamadas');
		  treeView('#pag-cluster #repetidas', 35,'Repetidas','dvRepetidas');
		  treeView('#pag-cluster #ed', 65,'ED','dvED');
		  treeView('#pag-cluster #ic', 95,'IC','dvIC');
		  treeView('#pag-cluster #tid', 115,'TID','dvTid');
		  treeView('#pag-cluster #vendas', 145,'Vendas','dvVendas');
		  treeView('#pag-cluster #falhas', 165,'Falhas','dvFalhas');
		  treeView('#pag-cluster #extratores', 195,'Extratores','dvExtratores');
		  treeView('#pag-cluster #parametros', 225,'Parametros','dvParam');
		  treeView('#pag-cluster #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-cluster #monitoracao', 275, 'Monitoração', 'dvReparo');
          treeView('#pag-cluster #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      /*
      $('.nav.aba3').css('display','none');
      $('.nav.aba4').css('display','none');
      */

      $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
      $(".aba6").css({'position':'fixed','left':'390px','right':'auto','margin-top':'35px','z-index':'1'});
      $('.navbar-inner').css('height','70px');
      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});


      //19/03/2014
      componenteDataMaisHora ($scope,$view);

      carregaRegioes($view);

      carregaSegmentos($view,true);
      carregaClusters($view,true);


      carregaDDDsPorRegiao($scope,$view,true);
      // Popula lista de  ddds a partir das regioes selecionadas
      $view.on("change", "select.filtro-regiao", function(){

        $('#alinkApl').removeAttr('disabled');
        $(".filtro-aplicacao").attr('multiple', 'multiple');
        $('#HFiltroED').prop('checked',false);
        //$('#HFiltroIC').prop('checked',false);
        $scope.filtros.isHFiltroED = false;
        //$scope.filtros.isHFiltroIC = false;
        $('#oueED').attr('disabled', 'disabled');
        $('#oueED').prop('checked',false);
        //$('#oueIC').attr('disabled', 'disabled');
        //$('#oueIC').prop('checked',false);
        //$('#iit').attr('disabled', false);

        var $sel_eds = $view.find("select.filtro-ed");
        $sel_eds.
        val([])
        .selectpicker('refresh');
        $sel_eds
        //.html("")
        .prop("disabled", "disabled")
        .selectpicker('refresh');

        /*var $sel_ics = $view.find("select.filtro-ic");
        $sel_ics.
        val([])
        .selectpicker('refresh');
        $sel_ics
        //.html("")
        .prop("disabled", "disabled")
        .selectpicker('refresh');*/

        carregaDDDsPorRegiao($scope,$view);

      });



        /*$view.find(".selectpicker").selectpicker({
        //noneSelectedText: 'Nenhum item selecionado',
        countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-cluster").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} clusters',
            showSubtext: true
        });


        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });




        $view.on("change", "select.filtro-ddd", function () {
            var filtro_ddds = $(this).val() || [];
            Estalo.filtros.filtro_ddds = filtro_ddds;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        //GILBERTO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

      //2014-11-27 transição de abas
      var abas = [2,3,4,5];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function(){marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

        // Marca todos os clusters
        $view.on("click", "#alinkCluster", function(){ marcaTodosIndependente($('.filtro-cluster'),'clusters')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-cluster').mouseover(function () {
            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-cluster').addClass('open');
            $('div.btn-group.filtro-cluster>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '500px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-cluster').mouseout(function () {
            $('div.btn-group.filtro-cluster').removeClass('open');
        });



        // // EXIBIR AO PASSAR O MOUSE
        // $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
        //   //11/07/2014 não mostrar filtros desabilitados
        //   if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
        //     $('div.btn-group.filtro-segmento').addClass('open');
        //     $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        //   }
        // });

        // // OCULTAR AO TIRAR O MOUSE
        // $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
        //     $('div.btn-group.filtro-segmento').removeClass('open');
        // });

        // // EXIBIR AO PASSAR O MOUSE
        // $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
        //   //11/07/2014 não mostrar filtros desabilitados
        //   if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
        //     $('div.btn-group.filtro-ddd').addClass('open');
        //     $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        //   }
        // });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          $('.chkMensal').prop('disabled',false);

          limpaProgressBar($scope, "#pag-cluster");
          //22/03/2014 Testa se data início é maior que a data fim
          var testedata = testeDataMaisHora($scope.periodo.inicio,$scope.periodo.fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }

            /*if(Estalo.filtros.filtro_regioes.length >= 1 && Estalo.filtros.filtro_regioes.indexOf("ND") < 0){
                $globals.agrupar = $scope.porRegEDDD;
            }else{
                $globals.agrupar = false;
                $scope.porRegEDDD = false;
            }*/

            if($scope.chkMensal === true && $scope.periodo.fim > hojeFim()){
              setTimeout(function(){
                atualizaInfo($scope,"Operação inválida para opção MENSAL, tente selecionar um período diferente");
                effectNotification();
                $view.find(".btn-gerar").button('reset');
              },500);
              return;
            }


            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.chkMensal = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {

            iniciaFiltros();
            componenteDataMaisHora ($scope,$view,true);


            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
              $('select.filtro-segmento').prop('disabled',false).selectpicker('refresh');
              $('#alinkSeg').attr('disabled',false).selectpicker('refresh');
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-cluster");
        }

        //Alex 27/02/2014
        $view.on("click", ".btn-pivot", function () {



          if($scope.pivot === true){
            $scope.pivot = false;
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);

          }else{
            limpaProgressBar($scope, "#pag-cluster");
            $('.chkMensal').prop('disabled',true);
            $scope.pivot = true;
             $scope.colunas = geraColunas();
            $scope.listaDadosPivot.apply(this);
          }



        });






        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };



    var dadosDias = [],
        dadosEntrantes = [],
        dadosRetidas = [],
        dadosTMAs = [],
        dadosDDDs = [],
        dadosRegioes = [],
        dadosFinalizadas = [],
        dadosDerivadas = [],
        dadosAbandonadas = [],
        dadosPercDerivadas = [],
        dadosPercFinalizadas = [],
        dadosPercAbandonadas = [],
        dadosClientesEntrantes = [],
        dadosClientesDerivados = [],
        dadosPercClientesEntrantes = [],
        dadosPercClientesDerivados = [],
        temTotaisClientes = [];



  $scope.listaDadosPivot = function () {

              $scope.colunas = [];
              $scope.dados = [];

              var temp = [];

              dadosDias.unshift("data");
              dadosEntrantes.unshift("Entrantes");
              temp.push(dadosEntrantes);
              dadosDerivadas.unshift("Derivadas");
              temp.push(dadosDerivadas);
              dadosFinalizadas.unshift("Finalizadas");
              temp.push(dadosFinalizadas);
              dadosAbandonadas.unshift("Abandonadas");
              temp.push(dadosAbandonadas);
              dadosRetidas.unshift("Retidas");
              temp.push(dadosRetidas);

              if(unique(dadosTMAs).toString() !== ""){
                 dadosTMAs.unshift("TMAs");
                temp.push(dadosTMAs);
              }

              if(unique(dadosDDDs).toString() !== ""){
                dadosDDDs.unshift("DDD");
                temp.push(dadosDDDs);
              }

              if(unique(dadosRegioes).toString() !== ""){
                dadosRegioes.unshift("Região");
                temp.push(dadosRegioes);
              }

              dadosPercDerivadas.unshift("% Deriv.");
              temp.push(dadosPercDerivadas);
              dadosPercFinalizadas.unshift("% Final.");
              temp.push(dadosPercFinalizadas);
              dadosPercAbandonadas.unshift("% Aband.");
              temp.push(dadosPercAbandonadas);
              dadosClientesEntrantes.unshift("CE");
              temp.push(dadosClientesEntrantes);
              dadosClientesDerivados.unshift("CD");
              temp.push(dadosClientesDerivados);
              dadosPercClientesEntrantes.unshift("% CE");
              temp.push(dadosPercClientesEntrantes);
              dadosPercClientesDerivados.unshift("% CD");
              temp.push(dadosPercClientesDerivados);



              for(var i = 0; i< temp.length; i++){
                var item = {};
                for(var j = 0; j< dadosDias.length;j++){

                  if(typeof dadosDias[j] === "object"){

                    item[dadosDias[j].data] = temp[i][j];
                  }else{

                    item[dadosDias[j]] = temp[i][j];
                  }


                }
                $scope.dados.push(item);
              }

                for(var j = 0; j< dadosDias.length;j++){
                  if(j!==0){
                    $scope.colunas.push({field: dadosDias[j].data, displayName: dadosDias[j].dataBR, width: 127, pinned: false, cellClass: "grid-align" });
                  }else{
                    $scope.colunas.push({field: dadosDias[j], displayName: "Data", width: 120, pinned: true });
                  }
                }

        retornaStatusQuery($scope.dados.length, $scope);
        //$scope.$apply();

      };






  function retornaTamanho(arr,letra){
    var w = parseInt(unique2(arr.map(function (i){if(i.substring(0,1) ===letra)return i.substring(2,i.length)})));
    return !isNaN(w)? w : '';
  }


  function geraGrafico(d){

    $('#venn').html("");
    $('#legenda').css({'position': 'relative','margin-top':'-270px'});
    $("#venn").css({"padding":'70px','height':'200px'});
    $('#grid').css('margin-top','-20px');

    itens = [];
    d.forEach(function(v){
      if(v.datReferencia === (formataDataBR($scope.periodo.fim)).substring(3,10) || v.datReferencia === (formataDataBR($scope.periodo.fim))){
        itens.push( v.letra+" "+v.qtdClientesEntrantes);
      }
    });



    var teste = itens.filter(function(i){ var  m = ["X","Y","Z"];if(m.indexOf(i.substr(0,1))>=0){ return i  }});
    if(teste.length === 0) return;

    $scope.diaFoco = "GRÁFICO CLIENTES ÚNICOS ENTRANTES "+($scope.chkMensal ? "MENSAL" : "DIA" );

    var sets = [];

    if(!isNaN(retornaTamanho(itens,"X")))sets.push({sets:["X"], size: retornaTamanho(itens,"X")});
    if(!isNaN(retornaTamanho(itens,"Y")))sets.push({sets:["Y"], size: retornaTamanho(itens,"Y")});
    if(!isNaN(retornaTamanho(itens,"Z")))sets.push({sets:["Z"], size: retornaTamanho(itens,"Z")});
    if(!isNaN(retornaTamanho(itens,"D")))sets.push({sets: ["X", "Y"], size: retornaTamanho(itens,"D"), label: "D"});
    if(!isNaN(retornaTamanho(itens,"B")))sets.push({sets: ["X", "Z"], size: retornaTamanho(itens,"B"), label: "B"});
    if(!isNaN(retornaTamanho(itens,"C")))sets.push({sets: ["Y", "Z"], size: retornaTamanho(itens,"C"), label: "C"});
    if(!isNaN(retornaTamanho(itens,"A")))sets.push({sets: ["X", "Y", "Z"], size: retornaTamanho(itens,"A"), label: "A"});

    //sets = toObject(sets);



var chart = venn.VennDiagram()
    chart.wrap(true)
    .width(320)
    .height(320);

var div = d3.select("#venn").datum(sets).call(chart);
div.selectAll("text").style("fill", "white");
div.selectAll(".venn-circle path").style("fill-opacity", .6);


function annotateSizes() {
    d3.select(this).select("text")
        .append("tspan")
        .text(function(d) { return formataNumGrande(d.size); })
        .attr("x", function() { return d3.select(this.parentNode).attr("x"); })
        .attr("dy", "1.5em")
        .style("fill", "Black")
        .style("font-size", "15px");

}
div.selectAll("g").transition("venn").each("end", annotateSizes);

  }


  function geraGrafico2(d){

    $('#venn2').html("");
    $('#legenda2').css({'position': 'relative','margin-top':'-270px'});
    $("#venn2").css({"padding":'70px','height':'200px'});
    $('#grid').css('margin-top','-20px');

    itens = [];
    d.forEach(function(v){
      if(v.datReferencia === (formataDataBR($scope.periodo.fim)).substring(3,10) || v.datReferencia === (formataDataBR($scope.periodo.fim))){
        itens.push( v.letra+" "+v.qtdClientesDerivados);
      }
    });



    var teste = itens.filter(function(i){ var  m = ["X","Y","Z"];if(m.indexOf(i.substr(0,1))>=0){ return i  }});
    if(teste.length === 0) return;

    $scope.diaFoco2 = "GRÁFICO CLIENTES ÚNICOS DERIVADOS "+($scope.chkMensal ? "MENSAL" : "DIA" );

    var sets = [];

    if(!isNaN(retornaTamanho(itens,"X")))sets.push({sets:["X"], size: retornaTamanho(itens,"X")});
    if(!isNaN(retornaTamanho(itens,"Y")))sets.push({sets:["Y"], size: retornaTamanho(itens,"Y")});
    if(!isNaN(retornaTamanho(itens,"Z")))sets.push({sets:["Z"], size: retornaTamanho(itens,"Z")});
    if(!isNaN(retornaTamanho(itens,"D")))sets.push({sets: ["X", "Y"], size: retornaTamanho(itens,"D"), label: "D"});
    if(!isNaN(retornaTamanho(itens,"B")))sets.push({sets: ["X", "Z"], size: retornaTamanho(itens,"B"), label: "B"});
    if(!isNaN(retornaTamanho(itens,"C")))sets.push({sets: ["Y", "Z"], size: retornaTamanho(itens,"C"), label: "C"});
    if(!isNaN(retornaTamanho(itens,"A")))sets.push({sets: ["X", "Y", "Z"], size: retornaTamanho(itens,"A"), label: "A"});

    //sets = toObject(sets);



var chart = venn.VennDiagram()
    chart.wrap(true)
    .width(320)
    .height(320);

var div = d3.select("#venn2").datum(sets).call(chart);
div.selectAll("text").style("fill", "white");
div.selectAll(".venn-circle path").style("fill-opacity", .6);


function annotateSizes() {
    d3.select(this).select("text")
        .append("tspan")
        .text(function(d) { return formataNumGrande(d.size); })
        .attr("x", function() { return d3.select(this.parentNode).attr("x"); })
        .attr("dy", "1.5em")
        .style("fill", "Black")
        .style("font-size", "15px");

}
div.selectAll("g").transition("venn").each("end", annotateSizes);

  }

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        dadosDias = [],
        dadosEntrantes = [],
        dadosRetidas = [],
        dadosTMAs = [],
        dadosDDDs = [],
        dadosRegioes = [],
        dadosFinalizadas = [],
        dadosDerivadas = [],
        dadosAbandonadas = [],
        dadosPercDerivadas = [],
        dadosPercFinalizadas = [],
        dadosPercAbandonadas = [],
        dadosClientesEntrantes = [],
        dadosClientesDerivados = [],
        dadosPercClientesEntrantes = [],
        dadosPercClientesDerivados = [],
        temTotaisClientes = [];

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_clusters = $view.find("select.filtro-cluster").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
    + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_clusters.length > 0) { $scope.filtros_usados += " Clusters: " + filtro_clusters; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }


        if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
          var options = [];
          var v = [];
          filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
          unique(v).forEach((function (filtro_ddds) {
            return function (codigo) {
              var ddd = cache.ddds.indice[codigo];
              filtro_ddds.push(codigo);
            };
          })(filtro_ddds));
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
        }else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
          $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }

        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        var tabelaMensal ="";



      if($('.chkMensal').prop('checked') === true){
        tabelaMensal = "Mes2";
      }





      stmt = db.use
              + "   SELECT datreferencia as DatReferencia,  cluster as clu, SUM(qtdchamadas) as c, SUM(qtdderivadas) as d,"
      + "   SUM(qtdclientes) as qc, SUM(qtdclientesderivados) as qcd"
      + "   FROM "+db.prefixo+"TotaisClientesUnicosCluster"+tabelaMensal+" as TCU "
      + "   WHERE 1 = 1"
      + "   AND CONVERT(DATE,TCU.DatReferencia) >= '" + formataData(data_ini) + "'"
      + "   AND CONVERT(DATE,TCU.DatReferencia) <= '" + formataData(data_fim) + "'";

      stmt += restringe_consulta("TCU.cluster", filtro_clusters, true);
      stmt += restringe_consulta("TCU.codddd", filtro_ddds, true);
      stmt += restringe_consulta2("TCU.CodSegmento",filtro_segmentos,true);
      stmt +=  "   GROUP BY DatReferencia,Cluster";
      stmt +=  "   ORDER BY DatReferencia,Cluster";




        executaQuery = executaQuery2;

        log(stmt);




        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas (columns) {
          $globals.numeroDeRegistros = columns[0].value;
        }

        // 02/06/2014 Visão por ddd(s) e regiões(s)
        function executaQuery2(columns) {


 var data_hora = columns["DatReferencia"].value,
     datReferencia = typeof columns[0].value==='string' ? formataDataBRString(data_hora) : formataDataBR(data_hora),
          cluster = unique2(clusters.map(function(c){ if(c.codigo === columns["clu"].value) return c.nome })).toString(),
              qtdDerivadas = +columns["d"].value,
              qtdEntrantes = +columns["c"].value,
              qtdClientesEntrantes = +columns["qc"].value,
              qtdClientesDerivados = +columns["qcd"].value;


          $scope.dados.push({
            data_hora: data_hora,
            datReferencia: datReferencia,
            cluster: cluster,
            letra: cluster.substr(0,1),
            qtdDerivadas: qtdDerivadas,
            qtdEntrantes: qtdEntrantes,
            qtdClientesEntrantes: qtdClientesEntrantes,
            qtdClientesDerivados: qtdClientesDerivados
          });




        }





            db.query(stmt,executaQuery, function (err, num_rows) {
				
				
				var complemento = "";
				

                  if($scope.porRegEDDD===true){
                    $('#pag-cluster table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1230px','margin-bottom':'100px'});
                  }else{
                    $('#pag-cluster table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1310px','margin-bottom':'100px'});
                  }

                    console.log("Executando query-> "+stmt+" "+num_rows);

                  var datFim = "";
                  if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1 > limiteDias){
                    datFim = " Botão transpor habilitado para intervalos de "+limiteDias+" dias.";
                  }



                  // GILBERTO 09/07/2014 -- agrupar por MÊS
                  // ________________________________________________________
                  // ________________________________________________________
                  // crio uma nova propriedade em cada objeto do array dados, chamado comp, para ser usado como critério de agrupamento
                  // neste caso específico, pego o mês e ano da string (caractere de 3 a 10) de data.

                if($('.chkMensal').prop('checked') === true){
                  if(filtro_regioes.length === 0 || $('#porRegEDDD').prop('checked') === true){

                    $scope.dados.forEach(function(d){
                      d.comp = d.datReferencia.substr(3,10)+" "+d.cluster;
                    });


                    var objetoAgrupado = _.groupBy($scope.dados,'comp');

                    $scope.dadosMes = [];
					
					complemento += " mensal";

                    for(var m in objetoAgrupado){
						
						if (Object.keys(objetoAgrupado)[0] === m) complemento += " "+m;
					if (Object.keys(objetoAgrupado).length > 1 && Object.keys(objetoAgrupado)[Object.keys(objetoAgrupado).length-1] === m) complemento += " "+m;




                      var mes = objetoAgrupado[m].reduce(function(a,b){

                        var testeAdicional = formataDataHora(new Date(objetoAgrupado[m][0].data_hora)) === formataDataHora(primeiroDiaDoMes(new Date(objetoAgrupado[m][0].data_hora)));

                        return {

                          data_hora: a.data_hora,
                              datReferencia: b.comp,
                              cluster: a.cluster,
                              letra: a.cluster.substr(0,1),
                              //qtdDerivadas: a.qtdDerivadas + b.qtdDerivadas,
                              //qtdEntrantes: a.qtdEntrantes + b.qtdEntrantes,
                              qtdDerivadas: b.qtdDerivadas,
                              qtdEntrantes: b.qtdEntrantes,
                              qtdClientesEntrantes: typeof objetoAgrupado[m][objetoAgrupado[m].length-1].qtdClientesEntrantes === "number" && testeAdicional ? objetoAgrupado[m][objetoAgrupado[m].length-1].qtdClientesEntrantes :"NA",
                              qtdClientesDerivados: typeof objetoAgrupado[m][objetoAgrupado[m].length-1].qtdClientesDerivados === "number" && testeAdicional ? objetoAgrupado[m][objetoAgrupado[m].length-1].qtdClientesDerivados :"NA",
                              };

                        }
                      );

                      mes.datReferencia =  mes.datReferencia.substring(0,9).split('/').length > 2 ? mes.datReferencia.substring(3,10) : mes.datReferencia.substring(0,7);

                      $scope.dadosMes.push(mes);
                    }

                    //if por mes

                    if($scope.chkMensal){
                      $scope.dados = $scope.dadosMes;
                      num_rows = $scope.dados.length;
                    }
                    // ________________________________________________________
                    // ________________________________________________________
                  }
                }

                    retornaStatusQuery(num_rows, $scope, datFim);

                    $btn_gerar.button('reset');

                    if(num_rows>0){
                      $btn_exportar.prop("disabled", false);

                      if($scope.chkMensal || formataData($scope.periodo.inicio) === formataData($scope.periodo.fim)){
                        geraGrafico($scope.dados);
                        //geraGrafico2($scope.dados);
                      }else{
                        $('#venn').html("");
                        $('#legenda').css({'position': 'relative','margin-top':'-535px'});
                        $("#venn").css({"padding":'70px','height':'200px'});
                        //$('#venn2').html("");
                        //$('#legenda2').css({'position': 'relative','margin-top':'-535px'});
                        //$("#venn2").css({"padding":'70px','height':'200px'});
                        $('#grid').css('margin-top','-20px');
                      }
                    }

                  //if(executaQuery !== executaQuery2){
                    $('.btn.btn-pivot').prop("disabled","false");
                    if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1 <= limiteDias && num_rows > 0){
                      $('.btn.btn-pivot').button('reset');
                    }
                  //}
				  
				  userLog(stmt, "Consulta "+complemento, 2, err);
				  
                });
            //});

            // GILBERTOOOOOO 17/03/2014
            $view.on("mouseup", "tr.resumo", function () {
                var that = $(this);
                $('tr.resumo.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });

    };





  // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var template = "";
      template = 'tResumoCUPC';




   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='"+template+"'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = "tResumoCUPC.xlsx";





    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

          var file = 'resumoCUPC_'+formataDataHoraMilis(milis)+'.xlsx';


   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados
                });


    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
			  var complemento = "";
			  if($scope.pivot) complemento = " PIVOT";
			  userLog(stmt, "Exportação "+complemento, 5, err);
            //?
          });

    };


}
CtrlCliUniPorCluster.$inject = ['$scope','$globals'];
