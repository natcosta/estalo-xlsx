function escapeSqlChars(input){
   input = input.replace('\'','\'\'');
   input = input.replace(';','');
   return input;
}

function DB(log) {
  this.log = log || function () {};

  this.connection = null;
  this.state = "idle";


  var pjsonTeste0 = require('./package.json');
  var v = pjsonTeste0.window.title;
  //{ Pega info PC Usuários
	var fs = require('fs');
	var os = require('os');
	var pcInfo="CPU : "+ os.cpus()[0].model+", Núcleos : " + os.cpus().length + " Memória RAM: " + Math.round(os.totalmem()/Math.pow(1024,3))+"GB, Resolução : "+window.innerWidth+"x"+window.innerHeight+"px";  
  //}
  this.use = "/*"+config.server+"*/";//"/* Estalo - "+v+" - "+versao+" "+pcInfo+"  */";
  this.prefixo = "";

  var self = this;  
  for (var i=0; i < regraNAT.length; i++){
	  if(dominioUsuario === regraNAT[i].dominio && loginUsuario === regraNAT[i].login) config.server = config.serverNAT;
  }
  
    for (var i=0; i < regraVERSA.length; i++){
	  if(dominioUsuario === regraVERSA[i].dominio && loginUsuario === regraVERSA[i].login){
		  config.server = config.serverVERSA;
		  config.options.port = config.options.portVERSA;
	  }
  }

  this.connect = function (config, callback) { 
	  if (self.connection) return;
	  self.log("Iniciando conexão...");
	  self.state = "connecting";
	  self.connection = new Connection(config);
	  self.connection.on('connect', function (err) {
		  self.state = "idle";
		  if (err) {
			  self.log("Não foi possível conectar");
		  }
		  self.log("Conexão estabelecida");
		  callback && callback(err);
	  });
  };

  this.disconnect = function () {
    if (!self.connection) return;
    self.log("Encerrando conexão...");
    self.connection.close();
    self.connection = null;
    self.state = "idle";
    self.log("Conexão encerrada");
  };

  this.isConnected = function () {
    return self.connection !== null;
  };

  this.isIdle = function () {
    return self.state === "idle";
  };

  this.query = function (stmt, on_row, on_done, setParameters) {
    if (!self.connection) return;
    var request = new Request(stmt, function (err, num_rows) {
      self.state = "idle";
      if (err) {
        self.log("Erro: " + err);
      } else {
        self.log(num_rows + " registros recebidos");

      }

      on_done && on_done(err, num_rows);
    });

    setParameters && setParameters(request);

    request.on('row', function (columns) {
      on_row && on_row(columns);
    });

    self.log("Executando query...");
    self.state = "querying";

    // Gilberto 15/04/2014
    try{
        self.connection.execSql(request);
    }catch(e){
      console.log(e.name + ": " + e.message);
  }
  };
}
