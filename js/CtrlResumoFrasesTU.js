function CtrlResumoFrasesTU($scope, $globals) {
	
    win.title = "Frases TU"; //Paulo 05/07/2019
    $scope.versao = versao;

    travaBotaoFiltro(0, $scope, "#pag-resumo-frases-tu", "Frases TU");

    $scope.dados = [];
    $scope.colunas = [];
    $scope.csv = [];
    $scope.dadosTotais = [];
    $scope.colunasTotais = [];
    $scope.csvTotais = [];     
    $scope.dadosDetalhados = [];
    $scope.csvDetalhado = [];
    $scope.colunasDetalhadas = [];
    
       
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    
    $scope.gridDadosTotais = {
        data: "dadosTotais",
        columnDefs: "colunasTotais",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.gridDadosDetalhados = {
        data: "dadosDetalhados",
        columnDefs: "colunasDetalhadas",
        enableColumnResize: true,
        enablePinning: true
    };
    

    $scope.modalAssuntoAtual = "";
    $scope.carregandoDetalhados = false;   
    $scope.numRegistrosDetalhados; 

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
   // $scope.segmentos = [];
    $scope.ordenacao = ['assunto'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {       
        produtos: [],
        empresas: [],
    };	
	//$scope.operador = false;

    function geraColunas() {
        var array = [];

        array.push(
            {
                field: "assunto",
                displayName: "Assunto",
                width: 150
            }, 
            {
                field: "naoMarcouNaoAderente", 
                displayName: "Não Marcou",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="naoMarcouNaoAderente" ng-bind="row.entity.naoMarcouNaoAderente"></a></span></div>'              
            },
            {
                field: "percNaoMarcouNaoAderente",
                displayName: "% Não Marcou",
                width: 150
            },
            {
                field: "totalAderenteNao",
                displayName: "Marcou N",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderenteNao" ng-bind="row.entity.totalAderenteNao"></a></span></div>'
            },
            {
                field: "totalAderenteSim",
                displayName: "Marcou S",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderenteSim" ng-bind="row.entity.totalAderenteSim"></a></span></div>'
            },
            {
                field: "totalAderente",
                displayName: "Marcou",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderente" ng-bind="row.entity.totalAderente"></a></span></div>'
            },
            {
                field: "percMarcouNao",
                displayName: "% Marcou Não",
                width: 150
            },
            {
                field: "percMarcouSim",
                displayName: "% Marcou Sim",
                width: 150
            },
            {
                field: "percGeralAderente",
                displayName: "% Marcou",
                width: 150
            },
            {
                field: "totalMensagemFechadaSim",
                displayName: "Fechou Balão",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalMensagemFechadaSim" ng-bind="row.entity.totalMensagemFechadaSim"></a></span></div>'
            },
            {
                field: "percMensagemFechadaSim",
                displayName: "% Fechou Balão",
                width: 150
            }
        );

        return array;
    }

    function geraColunasDadosTotais() {
        var array = [];

        array.push(
            {
                field: "assunto",
                displayName: "Assunto",
                width: 150
            }, 
            {
                field: "naoMarcouNaoAderente", 
                displayName: "Total Não Marcou",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="naoMarcouNaoAderente" ng-bind="row.entity.naoMarcouNaoAderente"></a></span></div>'              
            },
            {
                field: "percNaoMarcouNaoAderente",
                displayName: "Total % Não Marcou",
                width: 150
            },
            {
                field: "totalAderenteNao",
                displayName: "Total Marcou N",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderenteNao" ng-bind="row.entity.totalAderenteNao"></a></span></div>'
            },
            {
                field: "totalAderenteSim",
                displayName: "Total Marcou S",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderenteSim" ng-bind="row.entity.totalAderenteSim"></a></span></div>'
            },
            {
                field: "totalAderente",
                displayName: "Total Marcou",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalAderente" ng-bind="row.entity.totalAderente"></a></span></div>'
            },
            {
                field: "percMarcouNao",
                displayName: "Total % Marcou Não",
                width: 150
            },
            {
                field: "percMarcouSim",
                displayName: "Total % Marcou Sim",
                width: 150
            },
            {
                field: "percGeralAderente",
                displayName: "Total % Marcou",
                width: 150
            },
            {
                field: "totalMensagemFechadaSim",
                displayName: "Total Fechou Balão",
                width: 150,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-detalhamento" class="btn-detalhar" ng-click="mostraGridAssuntoDetalhado($event)" target="_self" data-toggle="modal" data-tipo-marcou="totalMensagemFechadaSim" ng-bind="row.entity.totalMensagemFechadaSim"></a></span></div>'
            },
            {
                field: "percMensagemFechadaSim",
                displayName: "Total % Fechou Balão",
                width: 150
            }
        );

        return array;
    }

    function geraColunasAssuntoDetalhado() {
        var array = [];

        array.push(
            {
                field: "Item",
                displayName: "Item",
                width: 50
            },
            {
                field: "CodUCID",
                displayName: "CodUCID",
                width: 150
            }, 
            {
                field: "CtXId",
                displayName: "CtXId",
                width: 150                            
            },
            {
                field: "DataHora",
                displayName: "DataHora",
                width: 150
            },
            {
                field: "NumCPFCNPJ",
                displayName: "NumCPFCNPJ",
                width: 150
            },
            {
                field: "NumANI",
                displayName: "NumANI",
                width: 150
            },
            {
                field: "TelTratado",
                displayName: "TelTratado",
                width: 150
            },
            {
                field: "Produto",
                displayName: "Produto",
                width: 150
            },
            {
                field: "CheckVoiceBot",
                displayName: "CheckVoiceBot",
                width: 150
            },
            {
                field: "Assunto",
                displayName: "Assunto",
                width: 150
            },
            {
                field: "EPS",
                displayName: "EPS",
                width: 150
            },
            {
                field: "EPSIP",
                displayName: "EPSIP",
                width: 150
            },
            {
                field: "USUARIOCPF",
                displayName: "USUARIOCPF",
                width: 150
            }
        );

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora
    };

    var $view;

    // Executa quando o conteúdo da tela for carregado.
    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-frases-tu");

        //19/03/2014
        componenteDataHora($scope, $view);        
        carregaEmpresas($view);
        carregaProdutosTU($view,true);
		
        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSXComum.apply(this);
        });
        
        $view.click(function () {
            $(".btn-exportarCSV").unbind();
        });

        $view.on("click", ".btn-exportarCSV", function() {
            $scope.exportarCSVComum.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {    
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {           
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-frases-tu");
        }
		
		$view.on("mouseover",".grid",function(){$('div.notification').fadeIn(500)});

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {    

            limpaProgressBar($scope, "#pag-resumo-frases-tu");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.dados = [];
            $scope.csv = [];
            $scope.dadosTotais = [];
            $scope.csvTotais = [];
            $scope.colunas = geraColunas();            
            $scope.listaDados.apply(this);                  
            
        });
        
        $scope.mostraGridTotais = function () {
            $scope.colunasTotais = geraColunasDadosTotais(); 
            $scope.listaDadosTotais.apply(this);
        }
        
        $scope.mostraGridAssuntoDetalhado = function(event) {
            target = event.target; 

            var $gridContainerAtual = target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;

            var gridHasClassAttribute = $gridContainerAtual.getAttribute("class").indexOf("dados-totais");
           
            // Função que retorna um assunto caso o evento selecionado tenha sido no primeiro grid ou todos os assuntos caso o evento selecionado tenha sido no segundo grid.
            assuntosList = (function(dadosTotais) {
                assuntos = [];
                if(!dadosTotais) {
                    var $rowSelecionado = target.parentElement.parentElement.parentElement.parentElement.parentElement;
                    var $rowsElements = target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children;                   
                    var rowsElementsList = [];
                    var rowElementIndex;

                    for(rowElement in $rowsElements) {
                        rowsElementsList.push($rowsElements[rowElement]);
                    }                                         

                    rowElementIndex = rowsElementsList.indexOf($rowSelecionado);
                    assuntos.push($scope.listaTextAssuntos[rowElementIndex]);

                    return assuntos;
                }                
                return $scope.listaTextAssuntos;
               
            })(gridHasClassAttribute != -1);            
                       
            $scope.colunasDetalhadas = geraColunasAssuntoDetalhado();            
            $scope.listaDadosDetalhados.apply(this, [assuntosList, target.getAttribute("data-tipo-marcou")]); 
        } 
    });
    
    $scope.executaQueryGeneric = function(csvList, dadosList, colunasList, turnOffLoader) {
        return function (columns) {
            var assunto = columns["assunto"].value ? columns["assunto"].value : undefined;
            var naoMarcouNaoAderente = columns["naoMarcouNaoAderente"].value ? columns["naoMarcouNaoAderente"].value : 0;
            var percNaoMarcouNaoAderente = columns["percNaoMarcouNaoAderente"].value ? columns["percNaoMarcouNaoAderente"].value : 0;
            var totalAderenteNao = columns["totalAderenteNao"].value ? columns["totalAderenteNao"].value : 0;
            var totalAderenteSim = columns["totalAderenteSim"].value ? columns["totalAderenteSim"].value : 0;
            var totalAderente = columns["totalAderente"].value ? columns["totalAderente"].value : 0;
            var percMarcouNao = columns["percMarcouNao"].value ? columns["percMarcouNao"].value : 0;
            var percMarcouSim = columns["percMarcouSim"].value ? columns["percMarcouSim"].value : 0;
            var percGeralAderente = columns["percGeralAderente"].value ? columns["percGeralAderente"].value : 0;
            var totalMensagemFechadaSim = columns["totalMensagemFechadaSim"].value ? columns["totalMensagemFechadaSim"].value : 0;
            var percMensagemFechadaSim = columns["percMensagemFechadaSim"].value ? columns["percMensagemFechadaSim"].value : 0;

			var s = {                
                assunto : assunto,
                naoMarcouNaoAderente :  naoMarcouNaoAderente,
                percNaoMarcouNaoAderente : percNaoMarcouNaoAderente.toFixed(2),
                totalAderenteNao : totalAderenteNao,
                totalAderenteSim : totalAderenteSim,
                totalAderente : totalAderente,
                percMarcouNao : percMarcouNao.toFixed(2),
                percMarcouSim : percMarcouSim.toFixed(2),
                percGeralAderente : percGeralAderente.toFixed(2),
                totalMensagemFechadaSim : totalMensagemFechadaSim,
                percMensagemFechadaSim : percMensagemFechadaSim.toFixed(2)                          
            }
                                       
            $scope[dadosList].push(s);            
            var a = [];
            for (var item in $scope[colunasList]) {               
                a.push(s[$scope[colunasList][item].field]);
            }
            $scope[csvList].push(a);

            if(turnOffLoader)
                turnOffLoader();
        }
    }

    // Listar dados principais
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_produtotu = $view.find("select.filtro-produtotu").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }

        if (filtro_produtotu.length > 0) {
            $scope.filtros_usados += " Produtos TU: " + filtro_produtotu;
        }
       
        var stmt = "";        
       
        $scope.filtro_empresas_join = (filtro_empresas.length > 0) ? "''"+filtro_empresas.join("'',''")+"''" : '';
        $scope.filtro_produtostu_join = (filtro_produtotu.length > 0 ) ? "''"+filtro_produtotu.join("'',''")+"''" : '';
        
        stmt = db2.use +
        "Exec uspTUOiTotalFrasesTU" +
        " @dataIni = '" + formataDataHora(data_ini) +
        "', @dataFim = '" + formataDataHora(data_fim) + "'" +
        ($scope.filtro_empresas_join ? ", @eps = '" + $scope.filtro_empresas_join + "'" : '')+
        ($scope.filtro_produtostu_join ? ", @produto = '" + $scope.filtro_produtostu_join + "'" : '');
               
        log(stmt);
        
        var executaQuery = $scope.executaQueryGeneric.apply(this,['csv', 'dados', 'colunas']);
        
        $scope.finished = false;
        db2.query(stmt, executaQuery, function (err, num_rows) {            
            num_rows = $scope.dados.length;
            console.log("Executando query-> " + stmt + " " + num_rows);
            var datFim = "";
            
            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            } 
            $scope.finished = true;     
        });

        var interval = setInterval(checkQuery, 500);

        function populaListaAssuntos() {
            var $listaElementosAssuntos = document.querySelectorAll('div.dados > .grid-dados > .ngViewport > .ngCanvas > .ngRow');            
            var elementoAssunto;
            $scope.listaElementAssuntos =  [];
            for(elementoIndex in $listaElementosAssuntos) {                   
                elementoAssunto = $listaElementosAssuntos[elementoIndex];
                if(typeof elementoAssunto == 'object') 
                    $scope.listaElementAssuntos.push(elementoAssunto);
            }

            $scope.listaTextAssuntos = $scope.listaElementAssuntos.map(function(x) {
                return x.children[0].innerText.trim();
            });
        }

        function checkQuery() {
            if($scope.finished == true){
                
                populaListaAssuntos();
                $scope.mostraGridTotais.apply(this);
                stopInterval();
            }               
        }

        function stopInterval() {
            clearInterval(interval);
        }
    };

    // Listar totais dos dados principais
    $scope.listaDadosTotais = function () {
        $scope.carregandoDadosTotais = true;
        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_produtotu = $view.find("select.filtro-produtotu").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }

        if (filtro_produtotu.length > 0) {
            $scope.filtros_usados += " Produtos TU: " + filtro_produtotu;
        }
    
      
        var stmt = "";       
       
        $scope.filtro_empresas_join = (filtro_empresas.length > 0) ? "''"+filtro_empresas.join("'',''")+"''" : '';
        $scope.filtro_produtostu_join = (filtro_produtotu.length > 0 ) ? "''"+filtro_produtotu.join("'',''")+"''" : '';
        
        stmt = db2.use +
        "Exec uspTUOiTotalFrasesTU" +
        " @dataIni = '" + formataDataHora(data_ini) +
        "', @dataFim = '" + formataDataHora(data_fim) + "'" +
        ($scope.filtro_empresas_join ? ", @eps = '" + $scope.filtro_empresas_join + "'" : '')+
        ($scope.filtro_produtostu_join ? ", @produto = '" + $scope.filtro_produtostu_join + "'" : '')+
        ", @sumario = 1 ";
        
        log(stmt); 
        var turnOffLoader = function () {
            $scope.carregandoDadosTotais = false;
        }
        var executaQuery = $scope.executaQueryGeneric.apply(this,['csvTotais', 'dadosTotais', 'colunasTotais', turnOffLoader]);
        
        db2.query(stmt, executaQuery, function (err, num_rows) {            
            num_rows = $scope.dadosTotais.length;
            console.log("Executando query-> " + stmt + " " + num_rows);           
            $scope.$apply();            
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
            if(err) {
                console.log("Ocorreu um erro: ", err);
            }                 
        });             
    };
    
    // Listar dados detalhados por assunto
    $scope.listaDadosDetalhados = function (assuntosList, marcou) { 
        $scope.carregandoDetalhados = true;
        $scope.numRegistrosDetalhados = null;
        var incrementItem = 0;          
        var $btn_exportar_detalhado = $view.find(".btn-exportar-excel-detalhado");
        var $btn_exportar_csvDetalhado = $view.find(".btn-exportar-csv-detalhado");
        $btn_exportar_detalhado.prop("disabled", true);
        $btn_exportar_csvDetalhado.prop("disabled", true);
        

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
       
        $scope.modalAssuntoAtual = assuntosList.length > 1 ? assuntosList.join(",") : assuntosList[0]; 
        $scope.dadosDetalhados = [];
        $scope.csvDetalhado = [];
           
        var stmt = "";      
                    
        stmt = db.use +
        "Exec uspTUOiTotalFrasesTU" + 
        " @dataIni = '" + formataDataHora(data_ini) +
        "', @dataFim = '" + formataDataHora(data_fim) + "'" +
        ($scope.filtro_empresas_join ? ", @eps = '" + $scope.filtro_empresas_join + "'" : '')+
        ($scope.filtro_produtostu_join ? ", @produto = '" + $scope.filtro_produtostu_join + "'" : '')+
        ", @resumoouanalitico = 0, @assunto = '"+ 
        ((assuntosList.length > 1 ) ? "''"+assuntosList.join("'',''")+"''" : "''" + assuntosList[0] + "''")+ 
        (marcou == "totalMensagemFechadaSim" ? "', @mensagemfechada = '''S'''" :
        (function(marcou) {            
            switch(marcou) {
                case marcou = "naoMarcouNaoAderente":
                    return "', @checkvoicebot = ''''''";
                case marcou = "totalAderenteNao":
                    return "', @checkvoicebot = '''N'''"; 
                case marcou = "totalAderenteSim":
                    return "', @checkvoicebot = '''S'''";
                case marcou = "totalAderente":
                    return "', @checkvoicebot = '''S'',''N'''";                     
            }           
        })(marcou)); 

        log(stmt);
           
        function executaQuery(columns) {           
            var CodUCID = columns["CodUCID"].value ? columns["CodUCID"].value : undefined,            
                CtXId = columns["CtXId"].value ? columns["CtXId"].value : undefined,
                DataHora = columns["DataHora"].value ? columns["DataHora"].value : undefined,
                NumCPFCNPJ = columns["NumCPFCNPJ"].value ? columns["NumCPFCNPJ"].value : undefined,
                NumANI = columns["NumANI"].value ? columns["NumANI"].value : undefined,
                TelTratado = columns["TelTratado"].value ? columns["TelTratado"].value : undefined,
                Produto = columns["Produto"].value ? columns["Produto"].value : undefined,
                CheckVoiceBot = columns["CheckVoiceBot"].value ? columns["CheckVoiceBot"].value : undefined,
                Assunto = columns["Assunto"].value ? columns["Assunto"].value : undefined,
                EPS = columns["EPS"].value ? columns["EPS"].value : undefined,
                EPSIP = columns["EPSIP"].value ? columns["EPSIP"].value : undefined,
                USUARIOCPF = columns["USUARIOCPF"].value ? columns["USUARIOCPF"].value : undefined; 

                
			var s = { 
                Item : (incrementItem += 1),               
                CodUCID : CodUCID,               
                CtXId : CtXId,
                DataHora : formataDataHoraBR(DataHora),
                NumCPFCNPJ : NumCPFCNPJ,
                NumANI : NumANI,
                TelTratado : TelTratado,
                Produto : Produto,
                CheckVoiceBot : CheckVoiceBot,
                Assunto : Assunto,
                EPS : EPS,
                EPSIP : EPSIP,
                USUARIOCPF : USUARIOCPF
            }
                                       
            $scope.dadosDetalhados.push(s);            
            var a = [];
            for (var item in $scope.colunasDetalhadas) {               
                a.push(s[$scope.colunasDetalhadas[item].field]);
            }
            $scope.csvDetalhado.push(a);            
            $scope.carregandoDetalhados = false; 
        }
        
        // Uso de db para não ocupar o db2 com 2 queries caso ainda esteja sendo utilizado pelo grid principal.
        db.query(stmt, executaQuery, function (err, num_rows) {            
            num_rows = $scope.dadosDetalhados.length;
            $scope.numRegistrosDetalhados = num_rows;
            console.log("Executando query-> " + stmt + " " + num_rows);
            $scope.$apply();
            //retornaStatusQuery(num_rows, $scope, datFim);
                
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar_detalhado.prop("disabled", false);
                $btn_exportar_csvDetalhado.prop("disabled", false);                
            }        
        });     
        if($scope.dadosDetalhados.length == 0){
            setTimeout(function() { $scope.carregandoDetalhados = false }, 2000);
        }    
            
    };

    // Exportar planilha XLSX para grid comum
    $scope.exportaXLSXComum = function() {
        
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var colunas = [];
        var colunasTotais = [];
        for (i = 0; i < $scope.colunas.length; i++) {
            colunas.push($scope.colunas[i].displayName);          
        }

        for (i = 0; i < $scope.colunasTotais.length; i++) {
            colunasTotais.push($scope.colunasTotais[i].displayName);          
        }
        
        
        var dadosPrincipalExcel = [colunas].concat($scope.csv);
        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosPrincipalExcel);

        var dadosTotaisExcel = [colunasTotais].concat($scope.csvTotais);
        var dadosTotaisComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosTotaisExcel);

        var ws1 = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var ws1_name = "Grid Principal";
        var ws2 = XLSX.utils.aoa_to_sheet(dadosTotaisComFiltro);
        var ws2_name = "Grid Totais";
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'ResumoFrasesTU' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws1, ws1_name);
        XLSX.utils.book_append_sheet(wb, ws2, ws2_name);
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);
                setTimeout(function() {
            $btn_exportar.button('reset');
        }, 500);

    };

    // Exportar planilha CSV para grid comum
    $scope.exportarCSVComum = function() {       
        if ($scope.csv.length > 0) {
            var separator = ";"
            var colunas = ""
            var colunasTotais = "";
            var file_name = tempDir3() + "export_" + Date.now()
            for (i = 0; i < $scope.colunas.length; i++) {
                colunas = colunas + $scope.colunas[i].displayName + separator;
            }
            colunas = colunas + "\n"
            for (i = 0; i < $scope.csv.length; i++) {
                colunas = colunas + $scope.csv[i].join(separator) + "\n";
            }

            for (i = 0; i < $scope.colunasTotais.length; i++) {
                colunasTotais = colunasTotais + $scope.colunasTotais[i].displayName + separator;
            }
            colunasTotais = colunasTotais + "\n";

            for (i = 0; i < $scope.csvTotais.length; i++) {
                colunasTotais = colunasTotais + $scope.csvTotais[i].join(separator) + "\n";
            }

            colunas = colunas + "\n" + colunasTotais;
            
            exportaTXT(colunas, file_name, 'csv', true);
            // $scope.csv = [];
            // $scope.csvTotais = [];
        } else {
            $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
        }
        $(".dropdown-exportar").toggle();
        
    }

    // Exportar planilha XLSX para grid detalhado
    $scope.exportaXLSXDetalhado = function(event) {
        event.target.disabled = true;               
        var colunas = [];
        for (i = 0; i < $scope.colunasDetalhadas.length; i++) {
            colunas.push($scope.colunasDetalhadas[i].displayName);
        }
        
        var dadosExcel = [colunas].concat($scope.csvDetalhado);
        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);  
        var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'ResumoFrasesTUDetalhadoPorAssunto' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);

        setTimeout(function() {
            event.target.disabled = false;
        }, 500);

    };

     // Exportar planilha CSV para grid detalhado
    $scope.exportaCSVDetalhado = function (event) {
            event.target.disabled = true;
            if ($scope.csvDetalhado.length > 0) {
                var separator = ";";
                var colunas = "";
                var file_name = tempDir3() + "export_" + Date.now();
                for (i = 0; i < $scope.colunasDetalhadas.length; i++) {
                    colunas = colunas + $scope.colunasDetalhadas[i].displayName + separator;
                }
                colunas = colunas + "\n"
                for (i = 0; i < $scope.csvDetalhado.length; i++) {
                    colunas = colunas + $scope.csvDetalhado[i].join(separator) + "\n";
                }               
                exportaTXT(colunas, file_name, 'csv', true);
                $scope.csvDetalhado = [];
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
            setTimeout(function() {
                event.target.disabled = false;
            }, 500);                        
    }
       
}

CtrlResumoFrasesTU.$inject = ['$scope', '$globals'];