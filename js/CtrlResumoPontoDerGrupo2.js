function CtrlResumoPontoDerGrupo2($scope, $globals) {

	win.title = "Resumo Ponto Derivação x Grupo Atendimento"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-resumo-pontoder-grupo", "Resumo Ponto Derivação x Grupo Atendimento");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;

	$scope.dados = [];
	$scope.csv = [];
	$scope.vendas = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;
	$scope.total_geral;
	$scope.total_der;
	$scope.total_transf;
	$scope.total_naoatend;
	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	//$scope.ordenacao = 'data_hora';
	//$scope.decrescente = true;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		// ALEX - 29/01/2013
		sites: [],
		segmentos: [],
		isHFiltroED: false,
		porDia: false,
		porDDD: false
	};




	$scope.aba = 2;



	$scope.valor = "pontoDerivacao";



	function geraColunas() {
		var array = [];


		array.push({
			field: "datReferencia",
			displayName: "Data",
			width: 180,
			pinned: true
		}, {
			field: "pontoDerivacao",
			displayName: "Ponto Derivação",
			width: 250,
			pinned: true
		}, {
			field: "regra",
			displayName: "Regra",
			width: 130,
			pinned: true
		}, {
			field: "grupo1",
			displayName: "Operação 1",
			width: 130,
			pinned: true
		}, {
			field: "grupo2",
			displayName: "Operação 2",
			width: 130,
			pinned: true
		}, {
			field: "derivada",
			displayName: "Qtd Operação 1",
			width: 120,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "transferida",
			displayName: " Qtd Operação 2",
			width: 120,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "naoencontradas",
			displayName: "Ñ atendidas",
			width: 120,
			pinned: true,
			visible: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "total",
			displayName: "Total",
			width: 120,
			pinned: true,
			visible: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "percTransf",
			displayName: "% Operação 2",
			width: 120,
			pinned: true,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});

		return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: ontem(dat_consoli),
		max: dat_consoli*/
	};

	var $view;


	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-resumo-pontoder-grupo");


		//19/03/2014
		componenteDataHora($scope, $view);
		carregaAplicacoes($view, false, false, $scope);
		carregaSites($view);
		carregaOperacoesECH($view,true);



		carregaSegmentosPorAplicacao($scope, $view, true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view)
		});





		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-operacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Operações',
			showSubtext: true
		});

		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});



		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-ddd", function () {
			var filtro_ddds = $(this).val() || [];
			Estalo.filtros.filtro_ddds = filtro_ddds;
		});

		$view.on("change", "select.filtro-operacao", function () {
			var filtro_operacoes = $(this).val() || [];
			Estalo.filtros.filtro_operacoes = filtro_operacoes;
		});

		$view.on("change", "select.filtro-regiao", function () {
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});

		//GILBERTOO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});



		//2014-11-27 transição de abas
		var abas = [2, 3, 4];

		$view.on("click", "#alinkAnt", function () {

			if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
			if ($scope.aba > abas[0]) {
				$scope.aba--;
				mudancaDeAba();
			}
		});

		$view.on("click", "#alinkPro", function () {

			if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

			if ($scope.aba < abas[abas.length - 1]) {
				$scope.aba++;
				mudancaDeAba();
			}

		});

		function mudancaDeAba() {
			abas.forEach(function (a) {
				if ($scope.aba === a) {
					$('.nav.aba' + a + '').fadeIn(500);
				} else {
					$('.nav.aba' + a + '').css('display', 'none');
				}
			});
		}



		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function () {
			marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
		});

		// Marca todos os operacoes
		$view.on("click", "#alinkOper", function () {
			marcaTodosIndependente($('.filtro-operacao'), 'operacoes')
		});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});

		// Marca todos os transferId
		$view.on("click", "#alinkTid", function () {
			marcaTodosIndependente($('.filtro-transferid'), 'transferid')
		});


		// GILBERTO 18/02/2014



		// EXIBIR AO PASSAR O MOUSE



		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

			$("div.data-inicio.input-append.date").data("datetimepicker").hide();
			$("div.data-fim.input-append.date").data("datetimepicker").hide();
			$('div.btn-group.filtro-aplicacao').addClass('open');
			$('div.btn-group.filtro-aplicacao>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '350px'
			});
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
			$('div.btn-group.filtro-aplicacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
			$('div.btn-group.filtro-site').addClass('open');
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
			$('div.btn-group.filtro-site').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseover(function () {
			$('div.btn-group.filtro-operacao').addClass('open');
			$('div.btn-group.filtro-operacao>div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '350px'
			});
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseout(function () {
			$('div.btn-group.filtro-operacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
			//11/07/2014 não mostrar filtros desabilitados
			if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
				$('div.btn-group.filtro-segmento').addClass('open');
				$('div.btn-group.filtro-segmento>div>ul').css({
					'max-height': '500px',
					'overflow-y': 'auto',
					'min-height': '1px',
					'max-width': '350px'
				});
			}
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
			$('div.btn-group.filtro-segmento').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-transferid').mouseover(function () {
			//11/07/2014 não mostrar filtros desabilitados
			if (!$('div.btn-group.filtro-transferid .btn').hasClass('disabled')) {
				$('div.btn-group.filtro-transferid').addClass('open');
				$('div.dropdown-menu.open').css({
					'margin-left': '-230px'
				});
				$('div.btn-group.filtro-transferid>div>ul').css({
					'max-height': '500px',
					'overflow-y': 'auto',
					'min-height': '1px',
					'max-width': '350px'
				});
			}
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-transferid').mouseout(function () {
			$('div.btn-group.filtro-transferid').removeClass('open');
			$('div.dropdown-menu.open').css({
				'margin-left': '0px'
			});
		});




		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.porRegEDDD = false;
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);

			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-resumo-pontoder-grupo");
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

			$scope.pivot = false;
			limpaProgressBar($scope, "#pag-resumo-pontoder-grupo");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}



			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});



		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};








	// Lista chamadas conforme filtros
	$scope.listaDados = function () {



		$globals.numeroDeRegistros = 0;



		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true)

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
		var filtro_transferid = $view.find("select.filtro-transferid").val() || [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
			" até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_aplicacoes.length > 0) {
			$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		}
		if (filtro_sites.length > 0) {
			$scope.filtros_usados += " Sites: " + filtro_sites;
		}
		if (filtro_segmentos.length > 0) {
			$scope.filtros_usados += " Segmentos: " + filtro_segmentos;
		}
		if (filtro_operacoes.length > 0) {
			$scope.filtros_usados += " Operações: " + filtro_operacoes;
		}
		if (filtro_transferid.length > 0) {
			$scope.filtros_transferid += " TransferId: " + filtro_transferid;
		}

		$scope.vendas = [];
		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";
		$scope.datas = [];
		$scope.totais = {
			geral: {
				qtd_entrantes: 0
			}
		};

		if ($scope.filtros.porDia) {
			var tabela = "ResumoECHTransferIdDia2";

		} else {
			var tabela = "ResumoECHTransferId2";

		}

		stmt = db.use +
			" SELECT DatReferencia,TransferId,CodOperacao1,CodOperacao2,CodRegra, " +
			" sum(qtdDerivadas) as qtdDerivadas,sum(qtdTransferidas) as qtdTransferidas, " +
			" sum(qtdNaoEncontradas) as qtdNaoEncontradas,sum(qtdChamadas) as qtdChamadas " +
			" FROM " + tabela + "" +
			" WHERE 1 = 1" +
			" AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
			" AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt += restringe_consulta("CodSite", filtro_sites, true)
		stmt += restringe_consulta("TransferId", filtro_transferid, true)
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
		stmt += restringe_consultaOper("CodOperacao1", "CodOperacao2", filtro_operacoes, true) +
			" GROUP BY DatReferencia,TransferId,CodRegra,CodOperacao1,CodOperacao2"; +
		" ORDER BY DatReferencia";



		executaQuery = executaQuery2;
		log(stmt);


		function formataData2(data) {
			var y = data.getFullYear(),
				m = data.getMonth() + 1,
				d = data.getDate();
			return y + _02d(m) + _02d(d);
		}

		function executaQuery2(columns) {

			var data_hora = columns["DatReferencia"].value,
				datReferencia = typeof columns[0].value === 'string' ? formataDataHoraBR(data_hora) : formataDataHoraBR(data_hora),
				transferId = columns["TransferId"].value,
				grupo1 = columns["CodOperacao1"].value,
				grupo2 = columns["CodOperacao2"].value,
				qtdTransferidas = +columns["qtdTransferidas"].value,
				qtdDerivadas = +columns["qtdDerivadas"].value,
				qtdNaoEncontradas = +columns["qtdNaoEncontradas"].value,
				qtdChamadas = +columns["qtdChamadas"].value,
				regra = columns["CodRegra"].value,
				qtdPercTransf = (100 * qtdTransferidas / qtdDerivadas).toFixed(2)
			if (grupo1 == "" && grupo2 == "") qtdPercTransf = 0;
			if ($scope.filtros.porDia) {
				datReferencia = datReferencia.replace("00:00:00", "")
			};

			$scope.total_geral = $scope.total_geral + qtdChamadas;
			$scope.total_transf = $scope.total_transf + qtdTransferidas;
			$scope.total_der = $scope.total_der + qtdDerivadas;
			$scope.total_naoatend = $scope.total_naoatend + qtdNaoEncontradas;

			$scope.dados.push({
				data_hora: data_hora,
				datReferencia: datReferencia,
				transferida: qtdTransferidas,
				derivada: qtdDerivadas,
				naoencontradas: qtdNaoEncontradas,
				total: qtdChamadas,
				pontoDerivacao: transferId,
				grupo1: grupo1,
				grupo2: grupo2,
				regra: regra,
				percTransf: qtdPercTransf
			});

			$scope.csv.push([
				datReferencia,
				transferId,
				regra,
				grupo1,
				grupo2,
				qtdDerivadas,
				qtdTransferidas,
				qtdPercTransf
			]);


			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}

		$scope.total_geral = 0;
		$scope.total_transf = 0;
		$scope.total_der = 0;
		$scope.total_naoatend = 0;

		db.query(stmt, executaQuery, function (err, num_rows) {
			userLog(stmt, 'Carrega dados', 2, err)

			if ($scope.porRegEDDD === true) {
				$('#pag-resumo-pontoder-grupo table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1170px',
					'margin-bottom': '100px'
				});
			} else {
				$('#pag-resumo-pontoder-grupo table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1280px',
					'margin-bottom': '100px'
				});
			}
			console.log("Executando query-> " + stmt + " " + num_rows);


			var datFim = "";


			retornaStatusQuery(num_rows, $scope, datFim);
			$btn_gerar.button('reset');
			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportar_csv.prop("disabled", false);
				$btn_exportar_dropdown.prop("disabled", false);
			}

			$scope.dados.push({
				data_hora: "",
				datReferencia: "",
				transferida: $scope.total_transf,
				derivada: $scope.total_der,
				naoencontradas: $scope.total_naoatend,
				total: $scope.total_geral,
				pontoDerivacao: "",
				grupo1: "",
				grupo2: "TOTAL",
				regra: "",
				percTransf: (100 * $scope.total_transf / $scope.total_der).toFixed(2)
			});

			//if(executaQuery !== executaQuery2){
			$('.btn.btn-pivot').prop("disabled", "false");
			if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 <= limiteHoras / 24 && num_rows > 0) {
				$('.btn.btn-pivot').button('reset');
			}

			$scope.datas.sort(function (a, b) {
				return a.data < b.data ? -1 : 1;
			});



		});

		// GILBERTOOOOOO 17/03/2014
		$view.on("mouseup", "tr.resumo", function () {
			var that = $(this);
			$('tr.resumo.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});
	};





	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {
		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

		var linhas = [];


		linhas.push([{
			value: 'Data',
			autoWidth: true
		}, {
			value: 'Ponto de Derivação',
			autoWidth: true
		}, {
			value: 'Regra',
			autoWidth: true
		}, {
			value: 'Operação 1',
			autoWidth: true
		}, {
			value: 'Operação 2',
			autoWidth: true
		}, {
			value: 'Qtd Operação 1',
			autoWidth: true
		}, {
			value: 'Qtd Operação 2',
			autoWidth: true
		}, {
			value: '% transferidas',
			autoWidth: true
		}]);
		$scope.dados.map(function (dado) {
			return linhas.push([{
				value: dado.datReferencia,
				autoWidth: true
			}, {
				value: dado.pontoDerivacao,
				autoWidth: true
			}, {
				value: dado.regra,
				autoWidth: true
			}, {
				value: dado.grupo1,
				autoWidth: true
			}, {
				value: dado.grupo2,
				autoWidth: true
			}, {
				value: dado.derivada,
				autoWidth: true
			}, {
				value: dado.transferida,
				autoWidth: true
			}, {
				value: dado.percTransf,
				autoWidth: true
			}]);
		});


		var planilha = {
			creator: "Estalo",
			lastModifiedBy: $scope.username || "Estalo",
			worksheets: [{
				name: ' Ponto Deriv. x Grupo A.',
				data: linhas,
				table: true
			}],
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: {
				first: 1
			}
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');


		var milis = new Date();
		var file = 'resumoPDxGrupo_' + formataDataHoraMilis(milis) + '.xlsx';


		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, planilha.base64, 'base64');
			childProcess.exec(file);
		}

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);
	};

}

CtrlResumoPontoDerGrupo2.$inject = ['$scope', '$globals'];