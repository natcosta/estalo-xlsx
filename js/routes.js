agGrid.initialiseAgGridWithAngular1(angular);
var Estalo = angular.module("Estalo", ['ngGrid', 'ngSanitize','ngRoute', 'ui.grid', 'ui.grid.pinning','ui.grid.resizeColumns','ui.grid.exporter','ui.grid.selection','ui.grid.autoResize','ui.select','ui.grid.grouping','ui.grid.moveColumns','agGrid']);


// Testa se é numero
(function () {
  var _$locale = {
  "NUMBER_FORMATS": {
	"CURRENCY_SYM": "R$",
	"DECIMAL_SEP": ",",
	"GROUP_SEP": ".",
	"PATTERNS": [
	  {
		"gSize": 3,
		"lgSize": 3,
		"maxFrac": 3,
		"minFrac": 0,
		"minInt": 1,
		"negPre": "-",
		"negSuf": "",
		"posPre": "",
		"posSuf": ""
	  },
	  {
		"gSize": 3,
		"lgSize": 3,
		"maxFrac": 2,
		"minFrac": 2,
		"minInt": 1,
		"negPre": "\u00a4-",
		"negSuf": "",
		"posPre": "\u00a4",
		"posSuf": ""
	  }
	]
  }
};

var DECIMAL_SEP = '.';
function formatNumber(number, pattern, groupSep, decimalSep, fractionSize) {
  if (!isFinite(number) || typeof number === 'Object') return '';

  var isNegative = number < 0;
  number = Math.abs(number);
  var numStr = number + '',
	  formatedText = '',
	  parts = [];

  var hasExponent = false;
  if (numStr.indexOf('e') !== -1) {
	var match = numStr.match(/([\d\.]+)e(-?)(\d+)/);
	if (match && match[2] == '-' && match[3] > fractionSize + 1) {
	  numStr = '0';
	  number = 0;
	} else {
	  formatedText = numStr;
	  hasExponent = true;
	}
  }

  if (!hasExponent) {
	var fractionLen = (numStr.split(DECIMAL_SEP)[1] || '').length;

	// determine fractionSize if it is not specified
	if (fractionSize === undefined) {
	  fractionSize = Math.min(Math.max(pattern.minFrac, fractionLen), pattern.maxFrac);
	}

	// safely round numbers in JS without hitting imprecisions of floating-point arithmetics
	// inspired by:
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
	number = +(Math.round(+(number.toString() + 'e' + fractionSize)).toString() + 'e' + -fractionSize);

	if (number === 0) {
	  isNegative = false;
	}

	var fraction = ('' + number).split(DECIMAL_SEP);
	var whole = fraction[0];
	fraction = fraction[1] || '';

	var i, pos = 0,
		lgroup = pattern.lgSize,
		group = pattern.gSize;

	if (whole.length >= (lgroup + group)) {
	  pos = whole.length - lgroup;
	  for (i = 0; i < pos; i++) {
		if ((pos - i)%group === 0 && i !== 0) {
		  formatedText += groupSep;
		}
		formatedText += whole.charAt(i);
	  }
	}

	for (i = pos; i < whole.length; i++) {
	  if ((whole.length - i)%lgroup === 0 && i !== 0) {
		formatedText += groupSep;
	  }
	  formatedText += whole.charAt(i);
	}

	// format fraction part.
	while(fraction.length < fractionSize) {
	  fraction += '0';
	}

	if (fractionSize && fractionSize !== "0") formatedText += decimalSep + fraction.substr(0, fractionSize);
  } else {

	if (fractionSize > 0 && number > -1 && number < 1) {
	  formatedText = number.toFixed(fractionSize);
	}
  }

  parts.push(isNegative ? pattern.negPre : pattern.posPre);
  parts.push(formatedText);
  parts.push(isNegative ? pattern.negSuf : pattern.posSuf);
  return parts.join('');
}

  Estalo.filter('numeroOuNao', function () {
	var formats = _$locale.NUMBER_FORMATS;
	return function (x, casasDec) {
	  // se não for número, retorna inalterado
	  return (typeof x !== "number") ? x :
		formatNumber(x, formats.PATTERNS[0], formats.GROUP_SEP, formats.DECIMAL_SEP, casasDec);
	};
  });

})();

(function () {
Estalo.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://srv*.assets.example.com/**',
     'http://www.versatec.com.br/*',
    'https://www.versatec.com.br/*',
     'http://www.versatec.com.br/**',
    'https://www.versatec.com.br/**',
	'http://versatil.website/*',
    'https://versatil.website/*',
     'http://versatil.website/**',
    'https://versatil.website/**',
        '*'
  ]);
  });
})();

var rotas = [

  //Chamadas
  { url: "/chamadas", controller: CtrlChamadas, templateUrl: base + "chamadas.html", nome: "Detalhamento de Chamadas", form: "frmPesqChamURAVoz", ordem: 1, grupo: "Chamadas" },
  { url: "/resumo880144", controller: CtrlResumo880x144, templateUrl: base + "resumo-880-144.html", nome: "Resumo 880 (3000) x 144", form: "FrmResumo880_144", ordem: 3, grupo: "Chamadas" },
  { url: "/resumoph", controller: CtrlResumoPH, templateUrl: base + "resumo-ph.html", nome: "Resumo Hora a Hora", form: "frmVisaoHoraGeral", ordem: 4, grupo: "Chamadas" },
  { url: "/resumodd", controller: CtrlResumoDD, templateUrl: base + "resumo-dd.html", nome: "Resumo Dia a Dia", form: "FrmDiaaDiaGeral", ordem: 6, grupo: "Chamadas" },
  { url: "/performancedddud", controller: CtrlPerformanceDDDUD, templateUrl: base + "performance-dddud.html", nome: "Resumo por DDD e Último Digito", form: "frmResumoAnalitico", ordem: 17, grupo: "Chamadas" },
  { url: "/resumoEmpresaDestino", controller: CtrlResumoEmpresaDestino, templateUrl: base + "resumo-por-empresa-destino.html", nome: "Resumo Pré-Roteamento", form: "frmResumoEmpresaDestino", ordem: 35, grupo: "Chamadas" },
  { url: "/resumoErrosPreRouting", controller: CtrlResumoErrosPreRouting, templateUrl: base + "resumoErrosPreRouting.html", nome: "Resumo de Erros de Pré Roteamento", form: "frmRoteamento", ordem: 51, grupo: "Chamadas" },
  { url: "/infoFaltanteLog", controller: CtrlInfFaltLog, templateUrl: base + "info-faltante-log.html", nome: "Informações faltantes nos logs", form: "frmInfoFaltLog", ordem: 51, grupo: "Chamadas" },
  { url: "/login", controller: CtrlLogin, templateUrl: base + "login.html", nome: "Login", form: "frmPesqChamURAVoz", ordem: 36, grupo: "Chamadas", ocultar: true},
  { url: "/contingenciaRouting", controller: CtrlContingenciaRouting, templateUrl: base + "contingenciaRouting.html", nome: "Contingência routing", form: "frmContRouting", ordem: 56, grupo: "Chamadas"},
  { url: "/resumoTag", controller: CtrlResumoTag, templateUrl: base + "resumo-tag.html", nome: "Resumo por Tag", form: "frmResumoTag", ordem: 57, grupo: "Chamadas" },  
  { url: "/resumoRetencaoLiquida", controller: CtrlResumoRetencaoLiquida, templateUrl: base + "resumo-retencao-liquida.html", nome: "Resumo Retenção Líquida", form: "frmResumoRetencaoLiquida", ordem: 110, grupo: "Chamadas"},   
  //Parâmetros
  { url: "/gra", controller: CtrlGra, templateUrl: base + "gra.html", nome: "Contingências GRA", form: "frmGra", ordem: 2, grupo: "Parâmetros" },  
  { url: "/CtrlParamOiTV", controller: CtrlParamOiTV, templateUrl: base + "ParamOiTV.html", nome: "Paramêtros Oi TV", form: "frmGraTV", ordem: 58, grupo: "Parâmetros" },
  { url: "/gestaoeds", controller: CtrlGestaoED, templateUrl: base + "gestao-eds.html", nome: "Gestão de Estados de Diálogo", form: "frmGestaoED", ordem: 24, grupo: "Parâmetros" },
  //Estados de diálogo
  { url: "/resumoed", controller: CtrlED, templateUrl: base + "resumo-ed.html", nome: "Resumo por Estado de Diálogo", form: "FrmVisaoEstados", ordem: 8, grupo: "Estados de diálogo" },
  { url: "/consolidadoed", controller: CtrlConsolidadoED, templateUrl: base + "consolidado-ed.html", nome: "Consolidado por Estado de Diálogo", form: "frmConsTotRotinaDetalhado", ordem: 9, grupo: "Estados de diálogo" },
  //Derivação 
  { url: "/transferid", controller: CtrlTransferID, templateUrl: base + "transferID.html", nome: "Resumo por TransferID e Ponto de Derivação", form: "frmTransferId", ordem: 10, grupo: "Derivação" },
  { url: "/derivacaoTLV", controller: CtrlDerivacaoTLV, templateUrl: base + "devivacao-tlv.html", nome: "Derivações de chamadas TLV", form: "frmDerivacaoTLV", ordem: 52, grupo: "Derivação" },
  //Vendas
  { url: "/resumoVS", controller: CtrlResumoVS, templateUrl: base + "resumo-vs.html", nome: "Resumo de Vendas", form: "frmResumoVendas", ordem: 15, grupo: "Vendas" },
  { url: "/vendas", controller: CtrlVendas, templateUrl: base + "vendas.html", nome: "Detalhamento de Vendas", form: "frmDetalhaVendas", ordem: 16, grupo: "Vendas" },
  { url: "/resumoporpromo", controller: CtrlResumoPorPromo, templateUrl: base + "resumo-por-promo.html", nome: "Resumo por Promoção Vendas", form: "frmResumoPromocaoVendas", ordem: 20, grupo: "Vendas" }, 
  { url: "/resumoIncentivo", controller: CtrlResIncentivo, templateUrl: base + "resumoIncentivo.html", nome: "Resumo por Incentivo", form: "frmIncentivo", ordem: 30, grupo: "Vendas" },
  { url: "/resumoVendaFalhaSucesso", controller: CtrlResumoVendasFalhaSucesso, templateUrl: base + "resumo-venda-falha-sucesso.html", nome: "Resumo de Vendas (falha / sucesso)", form: "frmResumoVendaFalhaSucesso", ordem: 35, grupo: "Vendas" },
  { url: "/cadastroXRecarga", controller: CtrlCadastroXRecarga, templateUrl: base + "cadastroXRecarga.html", nome: "Detalhamento Cadastro x Recarga", form: "frmCadastroRecarga", ordem: 28, grupo: "Vendas" },
  { url: "/rescadastroXRecarga", controller: CtrlResCadastroXRecarga, templateUrl: base + "resCadastroXRecarga.html", nome: "Resumo Cadastro x Recarga", form: "frmCadastroRecarga", ordem: 29, grupo: "Vendas" },
  { url: "/resumoRecargaM4U", controller: CtrlResumoRecargaM4U, templateUrl: base + "resumoRecargaM4U.html", nome: "Resumo de Recargas M4U", form: "frmResumoRecargaM4U", ordem: 51, grupo: "Vendas" },
  //Itens de controle
  { url: "/resumoic", controller: CtrlResumoIC, templateUrl: base + "resumo-ic.html", nome: "Resumo por Item de Controle", form: "frmConsultaItemControle", ordem: 18, grupo: "Itens de controle" },
  { url: "/resumoRaco", controller: CtrlResumoRaco, templateUrl: base + "resumo-raco.html", nome: "Resumo de Cancelamento", form: "frmRaco", ordem: 48, grupo: "Itens de controle" },
  { url: "/resumoAutomacoes", controller: CtrlResumoAutomacoes, templateUrl: base + "resumo-automacoes.html", nome: "Resumo de Automações", form: "frmConsultaItemControle", ordem: 51, grupo: "Itens de controle" },
  //Repetidas
  { url: "/derivacoesrepetidas", controller: CtrlDerivacoesR, templateUrl: base + "derivacoes-repetidas.html", nome: "Resumo de Repetidas", form: "frmRepetidas", ordem: 21, grupo: "Repetidas" },  
  { url: "/derivacoesrepetidasIC", controller: CtrlDerivacoesRIC, templateUrl: base + "derivacoes-repetidas-ic.html", nome: "Resumo de Repetidas por IC", form: "frmRepetidasIC", ordem: 24, grupo: "Repetidas" },
  { url: "/derivacoesrepetidasTag", controller: CtrlDerivacoesRTag, templateUrl: base + "derivacoes-repetidas-tag.html", nome: "Resumo de Repetidas por Tag", form: "frmRepetidasTag", ordem: 25, grupo: "Repetidas" },
  //{ url: "/histogramarepetidas", controller: CtrlHistogramaRepetidas, templateUrl: base + "histograma-repetidas-periodo.html", nome: "Histograma de Repetidas", form: "frmHistogramaRepetidas", ordem: 58, grupo: "Repetidas" },
  { url: "/histogramaClientes", controller: CtrlHistogramaClientes, templateUrl: base + "histograma-clientes.html", nome: "Histograma de Repetidas", form: "frmHistogramaClientes", ordem: 59, grupo: "Repetidas" },
  { url: "/retencaoLiquida", controller: CtrlRetencaoLiquida, templateUrl: base + "retencao-liquida.html", nome: "Resumo de repetidas do reset", form: "frmResumoRetencao", ordem: 52, grupo: "Repetidas" },
  { url: "/controleMetas", controller: CtrlMetaURA, templateUrl: base + "controle-metas.html", nome: "Controle de Metas", form: "frmControleMetas", ordem: 60, grupo: "Repetidas" },
  //Falhas e Legados
  { url: "/resumofalhas", controller: CtrlResumoFalhas, templateUrl: base + "resumo-falhas.html", nome: "Resumo de Falhas", form: "frmResumoFalhas", ordem: 25, grupo: "Falhas e Legados" },
  { url: "/falhasCallLogs", controller: CtrlResumoFalhasCallLogs, templateUrl: base + "resumoFalhasCallLogs.html", nome: "Resumo de Falhas por Calllogs", form: "frmFalhasCallLogs", ordem: 34, grupo: "Falhas e Legados" },
  //Extratores
  { url: "/extratores", controller: CtrlExtratores, templateUrl: base + "extratores.html", nome: "Extrator Geral", form: "frmPesqChamURAVoz", ordem: 19, grupo: "Extratores" },
  //Administrativo
  { url: '/cadastroUsuarios', controller: CtrlCadastroUsuarios, templateUrl: base + "cadastro-usuarios.html", nome: "Cadastro de Usuários", form: 'frmCadastroUsuarios', ordem: 53, grupo: "Administrativo" },
  { url: "/solicitaUsuario", controller: CtrlUserRequest, templateUrl: base + "solicitaUsuario.html", nome: "Solicitação de Cadastro", form: "formCadastroUser", ordem: 55, grupo: "Administrativo", ocultar: true },
  { url: "/importacaoAudio", controller: CtrlImportacaoAudio, templateUrl: base + "importacaoAudio.html", nome: "Importação de Áudios", form: "frmMonitorPromptsIcs", ordem: 102, grupo: "Administrativo" },
  //Suporte
  { url: "/ajuda", controller: CtrlAjuda, templateUrl: base + "ajuda.html", nome: "Ajuda", form: "frmPesqChamURAVoz", ordem: 36, grupo: "Suporte"},
  //Monitoração
  { url: "/monitorBilhetagem", controller: CtrlMonitorBilhetagem, templateUrl: base + "monitorBilhetagem.html", nome: "Monitor de bilhetagem", form: "frmMonitorBilhetagem", ordem: 52, grupo: "Monitoração" },
  { url: "/dashboardMailing", controller: CtrlDashboardMailing, templateUrl: base + "dashboardMailing.html", nome: "Dashboard Mailing", form: "frmDashboardMailing", ordem: 57, grupo: "Monitoração" },
  { url: "/controleConsolidador", controller: CtrlControleConsolidador, templateUrl: base + "controleConsolidador.html", nome: "Controle consolidador", form: "frmControleConsolidador", ordem: 99, grupo: "Monitoração"},
  { url: "/monitorExportacaoMailing", controller: CtrlMonitorExportacaoMailing, templateUrl: base + "monitorExportacaoMailing.html", nome: "Status D -1", form: "frmPesqChamURAVoz", ordem: 99, grupo: "Monitoração"},
  { url: "/monitorTeradata", controller: CtrlMonitorTeradata, templateUrl: base + "monitorTeradata.html", nome: "Monitor Teradata", form: "frmPesqChamURAVoz", ordem: 99, grupo: "Monitoração"},
  { url: "/acessousuarios", controller: CtrlAcessoUsuarios, templateUrl: base + "acesso-usuarios.html", nome: "Monitor Usuários", form: "frmMonitor", ordem: 99, grupo: "Monitoração"},
  { url: "/logsEmAtraso", controller: CtrlLogsEmAtraso, templateUrl: base + "logsEmAtraso.html", nome: "Logs em Atraso", form: "frmPesqChamURAVoz", ordem: 1, grupo: "Monitoração" },
  { url: "/monitorBilhetagemST", controller: CtrlMonitorBilhetagemST, templateUrl: base + "monitorBilhetagemST.html", nome: "Monitor ST Online", form: "frmMonitorPromptsIcs", ordem: 99, grupo: "Monitoração"},
  { url: "/monitorPromptIc", controller: CtrlMonitorPromptIc, templateUrl: "monitor-promptIc.html", nome: "Prompts e Ic's sem conteúdo", form: "frmMonitorPromptsIcs", ordem: 99, grupo: "Monitoração"},
  { url: "/monitorTesteProcedure", controller: CtrlMonitorTesteProcedure, templateUrl: base + "monitorTesteProcedure.html", nome: "Teste de Procedure", form: "frmTesteProcedure", ordem: 107, grupo: "Monitoração"},
  { url: "/homologacaoPrompt", controller: CtrlHomologacaoPrompt, templateUrl: base + "homologacaoPrompt.html", nome: "Homologação de Prompts", form: "frmMonitorPromptsIcs", ordem: 102, grupo: "Administrativo" },
  //Cradle to grave 2
  { url: "/contGlobalTransf2", controller: CtrlContGlobalTransf2, templateUrl: base + "contGlobalTransf.html", nome: "Contagem Global de Transferências", form: "ContGlobalTransf2", ordem: 38, grupo: "Cradle to grave 2" },
  { url: "/resumoContagemPorProdXGrupoAtend2", controller: CtrlResumoContProdXGrupAtend2, templateUrl: base + "resumo-contagem-por-prod-x-grupo-atend.html", nome: "Resumo Contagem Produto x Grupo Atend.", form: "ResumoContagemPorProdXGrupoAtend2", ordem: 39, grupo: "Cradle to grave 2" },
  { url: "/resumoEmpresa2", controller: CtrlResumoEmpresa2, templateUrl: base + "resumo-empresa.html", nome: "Resumo por Empresa", form: "ResumoEmpresa2", ordem: 40, grupo: "Cradle to grave 2" },
  { url: "/resumoPdXGrupo2", controller: CtrlResumoPontoDerGrupo2, templateUrl: base + "resumo-pontoder-grupo.html", nome: "Resumo Ponto de Der. x Grupo de Atend.", form: "ResumoPdXGrupo2", ordem: 41, grupo: "Cradle to grave 2" },
  { url: "/chamadasctg2", controller: CtrlChamadasCTG2, templateUrl: base + "chamadas-ctg.html", nome: "Detalhamento de Chamadas", form: "Chamadasctg2", ordem: 42, grupo: "Cradle to grave 2"},
  { url: "/resumoURAxECH2", controller: CtrlResumoUraxECH2, templateUrl: base + "resumoURAxECH.html", nome: "Resumo URA x ECH", form: "ResumoURAxECH2", ordem: 43, grupo: "Cradle to grave 2" },
  { url: "/resumoTransferenciaPorTag", controller: CtrlResumoTransferenciaPorTag, templateUrl: base + "resumo-transferencia-por-tag.html", nome: "Resumo de Transferências por Tag", form: "frmResumoTransferenciaTag", ordem: 48, grupo: "Cradle to grave 2" },
  //Tela única  
  { url: "/resumoDDTelaUnica", controller: CtrlResumoDDTelaUnica, templateUrl: base + "resumoDDTelaUnica.html", nome: "Resumo Dia a Dia", form: "frmConsultaItemControle", ordem: 51, grupo: "Tela Única" } ,
  { url: "/resumoDesbloqueioTelaUnica", controller: CtrlResumoDesbloqueioTelaUnica, templateUrl: base + "resumoDesbloqueioTelaUnica.html", nome: "Resumo Desbloqueio", form: "frmConsultaItemControle", ordem: 51, grupo: "Tela Única" } ,
  { url: "/resumoContestacaoTelaUnica", controller: CtrlResumoContestacaoTelaUnica, templateUrl: base + "resumoContestacaoTelaUnica.html", nome: "Resumo Contestação", form: "frmConsultaItemControle", ordem: 51, grupo: "Tela Única" } ,
  { url: "/resumoEPSTelaUnica", controller: CtrlResumoEPSTelaUnica, templateUrl: base + "resumoEPSTelaUnica.html", nome: "Resumo por EPS", form: "frmConsultaItemControle", ordem: 52, grupo: "Tela Única" } ,
  { url: "/resumoTUOTFunilSegVia", controller: CtrlResumoTUOTFunilSegVia, templateUrl: base + "resumo-tuot-funil-segvia.html", nome: "Funil de Segunda Via (Oi Total)", form: "frmTuOtFunilSegVia", ordem: 105, grupo: "Tela Única" },
  { url: "/resumoTUOTServicosVariaveis", controller: CtrlResumoTUOTServicosVariaveis, templateUrl: base + "resumo-tuot-servicos-variaveis.html", nome: "Detalhamento dos serviços por variáveis (Oi Total)", form: "frmTuOtServicoVariaveis", ordem: 103, grupo: "Tela Única" },
  { url: "/CtrlResumoTUOTQtdEntrantes", controller: CtrlResumoTUOTQtdEntrantes, templateUrl: base + "resumo-tuot-qtd-entrantes.html", nome: "Quantidade de chamadas atendidas TU que poparam na operação (Oi Total)", form: "frmTuOtQtdEntrantes", ordem: 104, grupo: "Tela Única" },
  { url: "/resumoTUOTFunilDesbloqueio", controller: CtrlResumoTUOTFunilDesbloqueio, templateUrl: base + "resumo-tuot-funil-desbloqueio.html", nome: "Funil de Desbloqueio (Oi Total)", form: "frmTuOtFunilDesbloqueio", ordem: 106, grupo: "Tela Única" },
  { url: "/resumoFrasesTU", controller: CtrlResumoFrasesTU, templateUrl: base + "resumo-frases-tu.html", nome: "Frases TU", form: "frmFrasesTu", ordem: 108, grupo: "Tela Única" },
  { url: "/automacaoOiTotal", controller: CtrlAutomacaoOiTotal, templateUrl: base + "automacao-oi-total.html", nome: "Automação Oi Total", form: "frmAutomacaoOiTotal", ordem: 110, grupo: "Tela Única" },
  //Cadastro e Migração  
  { url: "/resumoFontes", controller: CtrlResumoFontes, templateUrl: base + "resumoFontes.html", nome: "Resumo de Fontes", form: "frmPesqChamURAVoz", ordem: 48, grupo: "Cadastro e Migração" },
  { url: "/resumoDerivacoes", controller: CtrlResumoDerivacoes, templateUrl: base + "resumoDerivacoes.html", nome: "Resumo de Derivações", form: "frmPesqChamURAVoz", ordem: 49, grupo: "Cadastro e Migração" },
  { url: "/resumoCadastro", controller: CtrlResumoCadastro, templateUrl: base + "resumoCadastro.html", nome: "Resumo Cadastro/Migração", form: "frmPesqChamURAVoz", ordem: 50, grupo: "Cadastro e Migração" },	
  { url: "/higienizacaoCadastro", controller: CtrlHigienizacaoCadastro, templateUrl: base + "higienizacaoCadastro.html", nome: "Higienização cadastro", form: "frmHigieneCad", ordem: 36, grupo: "Cadastro e Migração"},
  //UraAtiva
  { url: "/uraAtiva", controller: CtrlUraAtiva, templateUrl: base + "ura-ativa.html", nome: "Ura Ativa", form: "frmUraAtiva", ordem: "54", grupo: "Ura Ativa" },
  //Pesquisa de Satisfação
  { url: "/resumoPesqServ", controller: CtrlResumoPesqServ, templateUrl: base + "resumo-por-pesquisa-servico.html", nome: "Atendimento Eletrônico", form: "frmResumoPesqServ", ordem: 100, grupo: "Pesquisa de Satisfação" },
  { url: "/atendimentoHumano", controller: CtrlAtendimentoHumano, templateUrl: base + "atendimento-humano.html", nome: "Atendimento Humano", form: "frmAtendimentoHumano", ordem: 101, grupo: "Pesquisa de Satisfação" },
  { url: "/ofensoresRepetidas", controller: CtrlOfensoresRepetidas, templateUrl: base + "ofensores-repetidas.html", nome: "Ofensores de Repetidas", form: "frmOfensoresRepetidas", ordem: 102, grupo: "Repetidas" }
	];

var gruposRelatorios = [
  { codigo: "chamadas", titulo: "Chamadas", div: 'dvchamadas', pos: 15 },
  { codigo: "repetidas", titulo: "Repetidas", div: 'dvRepetidas', pos: 35 },
  { codigo: "ed", titulo: "Estados de Diálogo", div: 'dvED', pos: 65 },
  { codigo: "ic", titulo: "Item de Controle", div: 'dvIC', pos: 95 },
  { codigo: "tid", titulo: "Transfer Id e VDN", div: 'dvTid', pos:115 },
  { codigo: "vendas", titulo: "Vendas", div: 'dvVendas', pos: 145 },
  { codigo: "falhas", titulo: "Falhas e Legados", div: 'dvFalhas', pos: 165 },
  { codigo: "extratores", titulo: "Extratores", div: 'dvExtratores', pos: 195 },
  { codigo: "parametros", titulo: "Parâmetros", div: 'dvParam', pos: 225 },
  { codigo: "admin", titulo: "Administrativo", div: 'dvAdmin', pos: 255 },
  { codigo: "monitoracao", titulo: "Monitoração", div: 'dvReparo', pos: 275 },
  { codigo: "cradleToGrave", titulo: "Cradle to Grave", div: 'dvcontGlobalTransf', pos: 295 },
  { codigo: "cradleToGrave 2", titulo: "Cradle to Grave 2", div: 'dvcontGlobalTransf', pos: 295 },
  { codigo: "uracadastro", titulo: "Cadastro e Migração", div: 'dvCadastro', pos: 325 },
  { codigo: "uraAtiva", titulo: "Ura Ativa", div: "dvUraAtiva", pos: 345 }
];

var urlsRotas = [];
var templatesRotas =[];
try {
  urlsRotas = rotasInit.map(function(rt){return rt.url});
  templatesRotas = rotasInit.map(function(rt){return rt.templateUrl});
} catch (ex) {
  console.log(ex);
}

// Rotas
// URL -> {template, controller}
function configRoutes($routeProvider) {

  rotas.forEach(function (rota) {
	//console.log(templatesRotas[urlsRotas.indexOf(rota.url)]);
	$routeProvider.when(rota.url, {
	  controller: rota.controller,
	  templateUrl: urlsRotas.indexOf(rota.url) >= 0 ? (base + templatesRotas[urlsRotas.indexOf(rota.url)]) : rota.templateUrl
	});
  });
  $routeProvider.otherwise({
	redirectTo: "/login" 
  });
}

Estalo.config(['$routeProvider','$locationProvider', configRoutes]);

Estalo.factory('$globals', function () {
  return { numeroDeRegistros: 0, //Número de registros de uma consulta usado para atualizar barra de progresso
	pivot: false,
	agrupar: false, // Passando valor de checkbox para outro relatório
	intervalo: "",
	condicional: true,
	logado: false,
	viewAlerta: 0
  };
});

Estalo.connectionPool = {}; // preencher com objetos DB
Estalo.globalEvents = new (require('events').EventEmitter)();

var bkpCache;

if (fs.existsSync(tempDir()+"infoCache")) {
  try {
  var texto =  fs.readFileSync(tempDir()+"infoCache", "utf-8");
  texto = JSON.parse(texto);
  bkpCache = texto.cache;
  } catch (ex) {
	console.log(ex.message);
	fs.unlinkSync(tempDir()+"infoCache");
  }
} else {
  var texto = {"cache": false, "relatorios": {"planilhaDeVendas":{"erros":{"01":0,"02":0,"03":0,"04":0,"05":0,"06":0,"07":0,"08":0,"09":0,"10":0,"11":0,"12":0},"minMax":{"01":{"min":"","max":""},"02":{"min":"","max":""},"03":{"min":"","max":""},"04":{"min":"","max":""},"05":{"min":"","max":""},"06":{"min":"","max":""},"07":{"min":"","max":""},"08":{"min":"","max":""},"09":{"min":"","max":""},"10":{"min":"","max":""},"11":{"min":"","max":""},"12":{"min":"","max":""}}}}};
  fs.writeFileSync(tempDir()+"infoCache", JSON.stringify(texto));
  bkpCache = false;
};

var arquivoCache = bkpCache;

if (!bkpCache && fs.existsSync(tempDir()+"cache.tmp")){
  fs.unlinkSync(tempDir()+"cache.tmp");
}

function iniciaFiltros() {
	Estalo.filtros = {};
	Estalo.filtros.filtro_data_hora = [];
	Estalo.filtros.filtro_final = [];
	Estalo.filtros.filtro_aplicacoes_vendas = [];
	Estalo.filtros.filtro_aplicacoes = [];
	Estalo.filtros.filtro_sites = [];
	Estalo.filtros.filtro_segmentos = [];
	Estalo.filtros.filtro_produto_prepago = [];
	Estalo.filtros.filtro_ddds = [];
	Estalo.filtros.filtro_grupos_produtos = [];
	Estalo.filtros.filtro_produtos = [];
	Estalo.filtros.filtro_erros = [];
	Estalo.filtros.filtro_resultados = [];
	Estalo.filtros.filtro_nao_abordado = [];
	Estalo.filtros.filtro_operacoes = [];
	Estalo.filtros.filtro_regioes = [];
	Estalo.filtros.filtro_ufs = [];
	Estalo.filtros.filtro_ed = [];
	Estalo.filtros.filtro_ic = [];
	Estalo.filtros.filtro_ud = [];
	Estalo.filtros.filtro_falhas = [];
	Estalo.filtros.filtro_uras = [];
	Estalo.filtros.filtro_servicos = [];
	Estalo.filtros.filtro_bases = [];
	Estalo.filtros.filtro_empresas = [];
	Estalo.filtros.filtro_reconhecimentos = [];
	Estalo.filtros.filtro_skills = [];
	Estalo.filtros.filtro_regras = [];
	Estalo.filtros.varjson = [];
	Estalo.filtros.filtro_transferids = [];
	Estalo.filtros.filtro_edstransf = [];
	Estalo.filtros.filtro_fontes = [];
	Estalo.filtros.filtro_assuntos = [];
	Estalo.filtros.filtro_produtotu = [];

	$('select.filtro-aplicacao').val('').selectpicker('refresh');
	$('select.filtro-site').val('').selectpicker('refresh');
	$('select.filtro-falhas').val('').selectpicker('refresh');
	$('select.filtro-uras').val('').selectpicker('refresh');
	$('select.filtro-segmento').val('').selectpicker('refresh');
	$('select.filtro-assunto').val('').selectpicker('refresh');
	$('select.filtro-empresa').val('').selectpicker('refresh');
    $('select.filtro-empresa').val('').selectpicker('refresh');
	$('select.filtro-errosRouting').val('').selectpicker('refresh');
	$('select.filtro-resultado').val('').selectpicker('refresh');
	$('select.filtro-segmento').prop('disabled',true).selectpicker('refresh');
	$('#alinkSeg').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-produto-prepago').val('').selectpicker('refresh')
	$('select.filtro-produto-prepago').prop('disabled',true).selectpicker('refresh');
	$('#alinkPrepago').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-ddd').val('').selectpicker('refresh');
	$('select.filtro-ddd').prop('disabled',true).selectpicker('refresh');
	$('#alinkDDD').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-grupo-produto').val('').selectpicker('refresh');
	$('select.filtro-grupo-produto').prop('disabled',true).selectpicker('refresh');
	$('#alinkGru').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-produto').val('').selectpicker('refresh');
	$('select.filtro-produto').prop('disabled',true).selectpicker('refresh');
	$('#alinkProd').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-erro').val('').selectpicker('refresh');
	$('select.filtro-status').val('').selectpicker('refresh');
	$('select.filtro-empresaRecarga').val('').selectpicker('refresh');
	$('select.filtro-empresaRecarga').val('').selectpicker('refresh');
	$('select.filtro-op').val('').selectpicker('refresh');
	$('select.filtro-op').prop('disabled',true).selectpicker('refresh');
	$('#alinkOp').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-regiao').val('').selectpicker('refresh');
	$('select.filtro-uf').val('').selectpicker('refresh');
	$('select.filtro-operacao').val('').selectpicker('refresh');

	$('select.filtro-transferid').val('').selectpicker('refresh');
	$('select.filtro-transferid').prop('disabled',true).selectpicker('refresh');
	$('#alinkTid').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-edstransf').val('').selectpicker('refresh');
	$('select.filtro-edstransf').prop('disabled',true).selectpicker('refresh');
	$('#alinkEDTr').attr('disabled','disabled').selectpicker('refresh');

	$('select.filtro-ed').val('').selectpicker('refresh');
	$('select.filtro-ed').prop('disabled',true).selectpicker('refresh');
	$('#HFiltroED').attr('checked',false);

	$('select.filtro-ic').val('').selectpicker('refresh');
	$('select.filtro-ic').prop('disabled',true).selectpicker('refresh');
	$('#HFiltroIC').attr('checked',false);
	$('#porRegEDDD').attr('checked',false);
	$('.chkMensal').attr('checked',false);

	$('select.filtro-ud').val('').selectpicker('refresh');
	$('select.filtro-final').val('').selectpicker('refresh');
	$('select.filtro-produtotu').val('').selectpicker('refresh');
}

iniciaFiltros();

function iniciaAgora(pag, scop) {
  var datahora = Estalo.filtros.filtro_data_hora[1];
  if (datahora === undefined) {
	datahora = formataDataHora(scop.periodo.fim);
  } else {
	datahora = formataDataHora(Estalo.filtros.filtro_data_hora[1]);
  }
  var data = datahora.substring(0, 10);  
  var newHora = new Date();
  var newHora = pad(newHora.getHours())+":"+pad(newHora.getMinutes())+":"+pad(newHora.getSeconds());
  var dat = data +" "+newHora;
  Estalo.filtros.filtro_data_hora[1] = new Date(dat);
  pag.find('.hora-fim input').val(newHora);
  scop.periodo.fim = Estalo.filtros.filtro_data_hora[1];
  scop.$apply();
}
