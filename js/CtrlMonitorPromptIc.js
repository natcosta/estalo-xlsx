/*
 *
 */
function CtrlMonitorPromptIc($scope, $globals) {

    win.title = "Monitoração de Prompts e Ic's sem conteúdo"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;
	$scope.dias = [];
	$scope.numDias = 0;
    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 5000;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoED"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-monitor-promptIc", "Monitoração de Prompts e Ic's sem conteúdo");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.csv = [];

    $scope.decrescente = true;


    $scope.log = [];
	$scope.grid = [];


    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };


	$('.menu-checkbox > input').css('margin','4px');

    $scope.aba = 2;
	
	//function geraValores(Dias){ for(var i=0 ; i< 10; i++){ d.push({field: 'Dias[i].qtd', displayName: 'Dias[i].i', width: 150, pinned: false}) }

    function geraColunas(dias) {
        var array = [];
		var d = [];
		
		
		//var d =[]; for(var i=0 ; i< 10; i++){ d.push({i: $scope.dias[i],qtd: columns[i + 1].value}) } return d;}
		
        array.push(
          { field: "CodAplicacao", displayName: "Aplicação", width: 150, pinned: false}		  
        );
		
		for(var i=0 ; i<= $scope.numDias; i++){ array.push({field: "Dias[" + i + "].qtd", 
			displayName:$scope.dias[i].substring(8,10) + "/" + $scope.dias[i].substring(5,7) + "/" + $scope.dias[i].substring(0,4), width: 150, pinned: false}) 
		}
		
		 array.push(
          { field: "Total", displayName: "Total", width: 150, pinned: false}		  
        );
		
		$scope.grid = array;			
        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-monitor-promptIc");
      treeView('#pag-monitor-promptIc #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-monitor-promptIc #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-monitor-promptIc #ed', 65,'ED','dvED');
      treeView('#pag-monitor-promptIc #ic', 95,'IC','dvIC');
      treeView('#pag-monitor-promptIc #tid', 115,'TID','dvTid');
      treeView('#pag-monitor-promptIc #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-monitor-promptIc #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-monitor-promptIc #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-monitor-promptIc #parametros', 225,'Parametros','dvParam');
      treeView('#pag-monitor-promptIc #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-monitor-promptIc #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-monitor-promptIc #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/

        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        //minuteStep: 5

        //Alex 21/02/2014 datetime picker data separada

        //19/03/2014
        componenteDataMaisHora($scope, $view);






        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-monitor-promptIc");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
			
			obterDias();
            $scope.colunas = geraColunas($scope.dias);
			
            $scope.listaDados.apply(this);
        });
		
		function obterDias() {
			
			var diff = new Date($scope.periodo.fim.getTime() - $scope.periodo.inicio.getTime());
			$scope.numDias = diff.getUTCDate() - 1;	
				
			$scope.dias = [];
			for (var i=0; i<= $scope.numDias ;i++){
				var d = new Date($scope.periodo.fim);
				d.setDate(d.getDate()-i);
				var month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
				if (month.length < 2) month = '0' + month;
				if (day.length < 2) day = '0' + day;
				$scope.dias.push([year, month, day].join('-'));
			}
						
			
		}

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });
		
		$view.on("click", ".btn-exportarCSV", function() {
            // var params = {
            //     allColumns: true,
            //     fileName: 'export_' + Date.now(),
            //     columnSeparator: ';'
            // }

            // $scope.gridDados.api.exportDataAsCsv(params);
			var x = $scope.grid;

            if ($scope.grid.length > 0) {
                separator = ";"
                colunas = "";
				var nome = $('#chkPrompts').prop('checked') ? "Prompts_" : "Ics_";
			    file_name = tempDir3() + 'monitoracao_' + nome + Date.now();
                for (i = 0; i < $scope.grid.length; i++) {
                    colunas = colunas + $scope.grid[i].displayName + separator
                }
                colunas = colunas + "\n"

                for (var i = 0; i < $scope.dados.length; i++) {
					colunas = colunas + $scope.dados[i].CodAplicacao + separator;
                     for (var j = 0; j < $scope.dados[i].Dias.length; j++) {
                        colunas = colunas + $scope.dados[i].Dias[j].qtd + separator;
                    }
					colunas = colunas + $scope.dados[i].Total + separator;
                    colunas = colunas + "\n";
                }
                // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                // }
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'csv', true);
                $scope.csv = [];
				$('.notification .ng-binding').text('CSV gerado com sucesso!').selectpicker('refresh');
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
        });


        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-monitor-promptIc");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        $globals.numeroDeRegistros = 0;
        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);


        $scope.dados = [];
        $scope.csv = [];

        var tabela = $('#chkPrompts').prop('checked') ? "FalhaDescricaoPrompt" : "FalhaDescricaoIc";
		var datas ="";
		for (var i=0; i < $scope.dias.length; i++){
			datas += "[" + $scope.dias[i] + "],"
		}
		datas = datas.slice(0, -1);
		
        stmt = db.use + '';
        stmt += " Select * "
        + " FROM (select codAplicacao, datahora, quantidade from " + db.prefixo + "" + tabela + ") src"
        + " pivot(  sum(quantidade)  for datahora in (" + datas + ") "
		+ ") piv";
      

        log(stmt);
        $scope.filtros_usados += $scope.parcial ? " <b>Registro PARCIAL</b>" : "";

		function obterTotais(columns) { var d =[]; for(var i=0 ; i<= $scope.numDias; i++){ d.push({i: $scope.dias[i],qtd: columns[i + 1].value == null ? 0 : columns[i + 1].value}) } return d;}
		function obterTotalGeral(columns) { 
			var total = 0; 
			for(var i=0 ; i<= $scope.numDias; i++){ 
				total+= columns[i + 1].value == null ? 0 : columns[i + 1].value; 
			} 
			return total;
		}
		
		function compare(a,b) {
		  if (a.Total > b.Total)
			return -1;
		  if (a.Total < b.Total)
			return 1;
		  return 0;
		}

		

        function executaQuery(columns) {


			
            $scope.dados.push({
                CodAplicacao: columns["codAplicacao"].value,
				Dias: obterTotais(columns),
				Total: obterTotalGeral(columns)
            });
		
			$scope.dados.sort(compare);	
			//$scope.dados.sort((a,b) => (a.Total < b.Total) ? 1 : ((b.Total < a.Total) ? -1 : 0));

            //$scope.csv.push([
                //data,
                //qtdUssd,
                //qtdMenuOi,
                //qtdCadastros,
              //  qtdRecargas
            //])
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
            //if ($scope.dados.length % 1000 === 0) {
            //    $scope.$apply();
            //}
        }



            db.query(stmt, executaQuery, function (err, num_rows) {
                userLog(stmt, 'Cruzando dados', 2, err)
                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                //$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                    $btn_exportar.prop("disabled", false);
                    $btn_exportar_csv.prop("disabled", false);
                    $btn_exportar_dropdown.prop("disabled", false);
                }
            });

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.estado_de", function () {
            var that = $(this);
            $('tr.estado_de.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };




  // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho = $scope.dias.map(function (d) {
            return {
                value: d.substring(8,10) + "/" + d.substring(5,7) + "/" + d.substring(0,4),
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            };
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({
            value: "CodAplicacao",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });
		
		 // Inserir última coluna: total por linha
        cabecalho.push({
            value: "Total",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });
		
		var linhas = $scope.dados.map(function (dado) {
            var linha = dado.Dias.map(function (d) {
                try {
                    return {
                        value: d.qtd || 0
                    };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            linha.unshift({
                value: dado.CodAplicacao
            });
			
			

            // Inserir última coluna: total por linha
            linha.push({
                value: dado.Total
            });

            return linha;
        });

      
       // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);
		
		 var titulo= [];
		 titulo.push({
            value: "Filtro por Prompt",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });
		// Inserir primeira linha: cabeçalho
		//linhas.unshift(titulo);

      
        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: $('#chkPrompts').prop('checked') ? "Prompts sem conteúdo" : "Ics sem conteúdo",
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
		var nome = $('#chkPrompts').prop('checked') ? "Prompts_" : "Ics_";
        var file = 'monitoracao_' + nome + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };
}
CtrlMonitorPromptIc.$inject = ['$scope', '$globals'];
