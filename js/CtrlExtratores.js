function CtrlExtratores($scope, $globals) {
	win.title = "Extrator genérico";
	cache.datsUltMesDetCham = new Date(cache.datsUltMesDetCham);
	$scope.datsUltMesDetCham = pad(cache.datsUltMesDetCham.getMonth() + 1) + "/" + cache.datsUltMesDetCham.getFullYear();
	$scope.datsUltMesDetChamFull = formataDataHoraBR(cache.datsUltMesDetCham);
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 5000;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

	travaBotaoFiltro(0, $scope, "#pag-extratores", "Extrator");

	$scope.status_progress_bar = 0;
	$scope.dados = [];
	$scope.log = [];
	$scope.aplicacoes = [];
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.operacoes = [];
	$scope.ordenacao = ['DatReferencia','DDD'];
	$scope.decrescente = false;
	$scope.porRegiao = null;
	$scope.extrator = true;
	$scope.status_extrator;
	$scope.linkArq = linkArq;
	$scope.linkArqContent = linkArqContent;
	$scope.tipoConsul = "5m";
	$scope.tipoExt = "1";
	$scope.preview = true;
	$scope.extratorArquivoPorData = false;
	$scope.valor = "Telefone Tratado";
	$scope.iit = false;
	$scope.confianca = false;
	$scope.contas = false;
	$scope.dadospessoais = false;
	$scope.enviosms = false;
	$scope.envioboleto = false;
	$scope.enviocontestacao = false;
	
	var cols = [
    { codigo: "0", nome: 'Chamador', descricao: 'I.NumANI' },
    { codigo: "1", nome: 'Telefone tratado', descricao: 'I.clienteTratado as TelefoneTratado' },
    { codigo: "2", nome: 'Telefone digitado', descricao: 'I.TelTratado as Digitado' },
    { codigo: "3", nome: 'Duração', descricao: 'I.QtdSegsDuracao' },
    { codigo: "4", nome: 'VDN saída', descricao: 'I.CodVDNTransf' },
    { codigo: "5", nome: 'TransferID', descricao: 'I.TransferID' },
    { codigo: "6", nome: 'ResultUltEstadoRec', descricao: 'I.ResultUltEstadoRec' },
    { codigo: "7", nome: 'IndicDelig', descricao: 'I.IndicDelig as Indic' },
    { codigo: "8", nome: 'IndicSucessoGeral', descricao: 'I.IndicSucessoGeral as IndicSucess' },
    //{ codigo: "9", nome: 'Nome estado', descricao: 'E.Nom_Estado' },
    { codigo: "10", nome: 'VDN entrada', descricao: 'I.NumDNIS' },
    { codigo: "11", nome: 'Aplicação', descricao: 'I.CodAplicacao' },
    { codigo: "12", nome: 'DDD', descricao: 'substring(I.ClienteTratado,1,2) as DDD' },
    { codigo: "13", nome: 'Último dígito', descricao: 'substring(I.ClienteTratado,len(I.ClienteTratado),1) as Ultimo' },
    { codigo: "14", nome: 'Segmento', descricao: 'I.SegmentoChamada' },
    { codigo: "15", nome: 'Site', descricao: 'I.CodSite' },
    { codigo: "16", nome: 'Contrato', descricao: 'CC.Contrato' },
    { codigo: "17", nome: 'CPF/CNPJ', descricao: 'I.NumCPFCNPJ AS CPF_CNPJ' },
    { codigo: "18", nome: 'CTXID', descricao: 'ISNULL(I.CTXID, \'\')' },
    //{ codigo: "50", nome: 'PROTOCOLO1', descricao: 'CASE WHEN ISNUMERIC(REPLACE(substring(I.xmlivr,PATINDEX(\'%InformaProtocolo_03%\',xmlivr)+20, PATINDEX(\'%_f\',xmlivr)+19),\',\',\'\')) = 1 THEN REPLACE(substring(I.xmlivr,PATINDEX(\'%InformaProtocolo_03%\',xmlivr)+20, PATINDEX(\'%_f\',xmlivr)+19),\',\',\'\') ELSE NULL END'},
    { codigo: "19", nome: 'PROTOCOLO', descricao: 'I.Protocolo'},
    { codigo: "20", nome: 'Penúltimo ED relevante', descricao: 'I.SEQUENCIA_ESTADODIALOGO AS PENULTIMO_ESTADODIALOGO' },
    { codigo: "21", nome: 'Último ED relevante', descricao: 'I.SEQUENCIA_ESTADODIALOGO' },
    { codigo: "22", nome: 'UCID', descricao: 'I.coducidivr' },
    { codigo: "23", nome: 'Ponto de derivação', descricao: 'I.PontoDerivacao' },
    { codigo: "24", nome: 'Trn ID', descricao: 'I.TrnId' },
    { codigo: "25", nome: 'Chamada repetida', descricao: '(I.PreRoteamento + I.Contexto) AS CHAMADA_REPETIDA' },
    { codigo: "26", nome: 'Data (Evento GRA)', descricao: 'I.evento AS DATA_EVENTO' },
    { codigo: "27", nome: 'Localidade (Evento GRA)', descricao: 'I.evento AS LOCALIDADE_EVENTO' },
    { codigo: "28", nome: 'Macroárea (Evento GRA)', descricao: 'I.evento AS MACROAREA_EVENTO' },
    { codigo: "29", nome: 'Sistema origem (Evento GRA)', descricao: 'I.evento AS SISTEMAORIGEM_EVENTO' },
    { codigo: "30", nome: 'GRA (Evento GRA)', descricao: 'I.evento AS GRA_EVENTO' },
    { codigo: "31", nome: 'Código central (Evento GRA)', descricao: 'I.evento AS CODIGOCENTRAL_EVENTO' },
    { codigo: "32", nome: 'Tipo tecnologia (Evento GRA)', descricao: 'I.evento AS TIPOTECNOLOGIA_EVENTO' },
    { codigo: "33", nome: 'Sigla estação (Evento GRA)', descricao: 'I.evento AS SIGLAESTACAO_EVENTO' },
    { codigo: "34", nome: 'Conta online', descricao: 'I.RespostaContaOnline AS CONTAONLINE' },
    { codigo: "35", nome: 'Confiança (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS CONFIANCA' },
    { codigo: "36", nome: 'Elocução (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS ELOCUCAO' },
    { codigo: "37", nome: 'Resultado (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS RESULTADO' },
    { codigo: "38", nome: 'Reconhecimento (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS RECONHECIMENTO' },
    { codigo: "39", nome: 'Duração (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS DURACAO' },
    { codigo: "40", nome: 'Score (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS SCORE' },
    { codigo: "41", nome: 'Sessão (Reconhecimento de voz)', descricao: 'I.ReconhecimentoVoz AS SESSAO' },
    { codigo: "42", nome: 'Seglabel', descricao: 'I.PreRoteamentoRet AS SEGLABEL' },
    { codigo: "43", nome: 'Operador (Tela Única)', descricao: 'I.XMLIVR AS OPERADOR' },
    { codigo: "44", nome: 'Plano (Tela Única)', descricao: 'I.XMLIVR AS PLANO' },
    { codigo: "45", nome: 'Modalidade (Tela Única)', descricao: 'I.XMLIVR AS MODALIDADE' },
    { codigo: "46", nome: 'Promoção (Tela Única)', descricao: 'I.XMLIVR AS PROMOCAO' },	
    { codigo: "54", nome: 'Envio de SMS (Tela Única)', descricao: 'I.XMLIVR AS SMS' },
    { codigo: "55", nome: 'Desbloqueio Solicitado (Tela Única)', descricao: 'I.INTERATIVIDADE AS DESBLOQUEIO'},
    { codigo: "56", nome: 'Elegível Desbloqueio (Tela Única)', descricao: 'I.INTERATIVIDADE AS ELEGIVEL_DESBLOQUEIO'},
    { codigo: "47", nome: 'Envio de boleto (Tela Única)', descricao: 'I.XMLIVR AS BOLETO' },		
    { codigo: "48", nome: 'Assunto', descricao: 'I.PreRoteamento AS ASSUNTO' },	
    { codigo: "49", nome: 'Empresa', descricao: 'I.PreRoteamentoRet AS EMPRESA' },
    { codigo: "50", nome: 'Dados Pessoais Nome', descricao: 'I.DadosPessoais AS DADOS_NOME' },
    { codigo: "51", nome: 'Dados Pessoais CPF', descricao: 'I.DadosPessoais AS DADOS_CPF' },
    { codigo: "52", nome: 'Dados Pessoais EMAIL', descricao: 'I.DadosPessoais AS DADOS_EMAIL' },
	{ codigo: "53", nome: 'Dados Pessoais Retorno', descricao: 'I.DadosPessoais AS DADOS_RETORNO' },
	{ codigo: "57", nome: 'Não Ouviu', descricao: 'I.XMLIVR AS ICM15054POSPAGO' },
	{ codigo: "58", nome: 'Não Aceitou', descricao: 'I.XMLIVR AS ICOR01003POSPAGO' },
	{ codigo: "59", nome: 'Erro CPF', descricao: 'I.XMLIVR AS ICOR01008POSPAGO' },
	{ codigo: "60", nome: 'Contestacao Procedente (Tela Única)', descricao: 'I.XMLIVR AS ContestacaoProcedente' },
	{ codigo: "61", nome: 'Contestacao Improcedente (Tela Única)', descricao: 'I.XMLIVR AS ContestacaoImprocedente' },
	{ codigo: "62", nome: 'Servidor de mídia', descricao: 'I.HostnameURA' },
	{ codigo: "63", nome: 'Pergunta Dirigida NLU', descricao: 'C.Comprimido AS PERGUNTADIRIGIDANLU' }
	

    //{ codigo: "49", nome: 'BRI', descricao: 'C.Comprimido AS BRI' }
    //{ codigo: "47", nome: 'Assunto', descricao: 'C.Comprimido AS ASSUNTO' },
    //{ codigo: "47", nome: 'Assunto', descricao: 'I.PreRoteamentoRet AS ASSUNTO' }
    //{ codigo: "42", nome: 'Plano Oi Total', descricao: 'C.Comprimido AS PLANO' }
	];
	
	var contas = [
	{ codigo: "0", nome: 'Aceitou' },
	{ codigo: "1", nome: 'Recusou' },
	{ codigo: "2", nome: 'Não respondeu' }
	];



	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		segmentos: [],
		sites: [],
		operacoes: [],
		relOrExt: "ext",
		IndicVendas: false,
		isHFiltroED: false,
		isHFiltroIC: false
	};

	$scope.oueED = false;
	$scope.oueIC = false;

	// Filtro: ddd/ufs/ud
	$scope.ddds = cache.ddds;
	$scope.ufs = cache.ufs;
	$scope.ud = ud;
	
	$scope.aba = 2;

	// Filtros: data e hora
	var agora = new Date();

	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
	};

	var $view;
	var numanis = [];
	var invalidsNumanis = [];

	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-extratores");

		$(".aba2").css({'left':'50px','top':'-15px'});
		$(".aba3").css({'left':'0px','top':'20px'});
		$(".aba4").css({'left':'53px','top':'-33px'});
		$('.transferid').css({'color':'white'});
		
		$(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'115px'});
		$($(".botoes li")[0]).css('margin','5px');
		
		componenteDataHora($scope, $view);
		
		carregaRegioes($view);		
		carregaUFs($view);
		carregaDigitos($view,true);
		carregaAplicacoes($view,false,false,$scope);
		carregaSites($view);
		
		//Popula lista de colunas padrão
		var options = [];
		cols.forEach(function (item) {
			options.push('<option value="' + item.codigo + '">' + item.nome + '</option>');
		});
		$view.find("select.filtro-cols").html(options.join());
		
		var options2 = [];
		contas.forEach(function (item) {
			options2.push('<option value="' + item.codigo + '">' + item.nome + '</option>');
		});
		$view.find("select.filtro-contaonline").html(options2.join());
		
		$("#confirma").click(function() {
		  $("#browser").trigger("click");
		});

		$('#browser').change(function() {
			numanis = fs.readFileSync($("#browser").val()).toString().split('\r\n');
			var n1 = [], n2 = [];
			
			if(numanis.length > 0){
				for (var i = 0; i < numanis.length; i++){
					if(n1.indexOf(numanis[i].trim()) < 0 && numanis[i].trim() !== "" && numanis[i].trim().length >= 10 && numanis[i].trim().length <= 11){
						n1.push(numanis[i].trim());
					}else if (numanis[i].trim() !== ""){
						n2.push(numanis[i].trim());
					}
				}
				if(numanis.length > 0){
					numanis = n1;
					invalidsNumanis = n2;
					$('#import2').css('background', '#A9D0F5').selectpicker('refresh');
					
				}else{
					numanis = [];
					invalidsNumanis = [];
					$("#browser").val("");
					alert("Arquivo fora de especificação. O padrão é DDD + 8 ou 9 dígitos para cada linha.");
				}
			}
		});
		
		
		
	carregaDDDsPorUF($scope,$view,true);
	// Popula lista de  ddds a partir das UFs selecionadas
	$view.on("change", "select.filtro-uf", function(){ carregaDDDsPorUF($scope,$view)});

	carregaDDDsPorRegiao($scope,$view,true);
	// Popula lista de  ddds a partir das regiões selecionadas
	$view.on("change", "select.filtro-regiao", function(){ carregaDDDsPorRegiao($scope,$view)});

	carregaSegmentosPorAplicacao($scope,$view,true);
	// Popula lista de segmentos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view) });
	
	$view.on("change", "select.filtro-cols", function () {
		if($(this).val() !== null){
			if($(this).val().indexOf("35")>=0){ $scope.confianca = true;}else{$scope.confianca = false;}
			if($(this).val().indexOf("34")>=0){ $scope.contas = true;}else{$scope.contas = false;}
			if($(this).val().indexOf("47")>=0){ $scope.envioboleto = true;}else{$scope.envioboleto = false;}
			if($(this).val().indexOf("60")>=0 || $(this).val().indexOf("61")>=0){ $scope.enviocontestacao = true;}else{$scope.enviocontestacao = false;}
			//
			if($(this).val().indexOf("54")>=0){ $scope.enviosms = true;}else{$scope.enviosms = false;}
			
			if($(this).val().indexOf("50")>=0 || 
			$(this).val().indexOf("51")>=0 || 
			$(this).val().indexOf("52")>=0 || 
			$(this).val().indexOf("53")>=0){ 
				$scope.dadospessoais = true;
			}else{
				$scope.dadospessoais = false;
			}
			
			$scope.$apply();
		}else{
			$scope.confianca = false;
			$scope.contas = false;
			$scope.dadospessoais = false;
			$scope.envioboleto = false;
			$scope.enviocontestacao = false;
			$scope.enviosms = false;
			$scope.$apply();
		}
	});
	
	$view.find("select.filtro-ddd").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} DDDs',
		showSubtext: true
	});
	
	$view.find("select.filtro-uf").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} UFs',
		showSubtext: true
	});
	
	$view.find("select.filtro-ud").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} último dígito',
		showSubtext: true
	});
	
	$view.find("select.filtro-cols").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} colunas',
		showSubtext: true
	});
		
	$view.find("select.filtro-contaonline").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} Respostas',
		showSubtext: true
	});

	$view.find("select.filtro-aplicacao").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} aplicações',
		showSubtext: true
	});

	$view.find("select.filtro-site").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} Sites/POPs',
		showSubtext: true
	});

	$view.find("select.filtro-op").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} operações'
	});

	$view.find("select.filtro-segmento").selectpicker({
	selectedTextFormat: 'count',
	countSelectedText: '{0} segmentos'
	});




	$view.find("select.filtro-regiao").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} regiões',
		showSubtext: true
	});

	$view.on("change", "select.filtro-site", function () {
		var filtro_sites = $(this).val() || [];
		Estalo.filtros.filtro_sites = filtro_sites;
	});
	
	//2014-11-27 transição de abas
	var abas = [2,3,4,5,6,7,8,9,10];
	
	$view.on("click", "#alinkAnt", function () {
		if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
			if($scope.aba > abas[0]){
				$scope.aba--;
				mudancaDeAba();
			}
	});
	
	$view.on("click", "#alinkPro", function () {
		if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;
			if($scope.aba < abas[abas.length-1]){
				$scope.aba++;
				mudancaDeAba();
			}
	});
	
	function mudancaDeAba(){
		abas.forEach(function(a){
			if($scope.aba === a){
				$('.nav.aba'+a+'').fadeIn(500);
			}else{
				$('.nav.aba'+a+'').css('display','none');
			}
		});
	}

	// Marca todos os ddds
	$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

	// Marca todos os ufs
	$view.on("click", "#alinkUf", function(){ marcaTodosIndependente($('.filtro-uf'),'ufs')});

	// Marca todos os uds
	$view.on("click", "#alinkUltDig", function(){ marcaTodosIndependente($('.filtro-ud'),'uds')});

	// Marca todos as cols
	$view.on("click", "#alinkCols", function(){ marcaTodosIndependente($('.filtro-cols'),'cols')});
	
	// Marca todos as contas
	$view.on("click", "#alinkCline", function(){ marcaTodosIndependente($('.filtro-contaonline'),'contas')});

	// Marca todos os sites
	$view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

	// Marca todos os segmentos
	$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

	// Marca todos os operações
	$view.on("click", "#alinkOp", function(){ marcaTodosIndependente($('.filtro-op'),'ops')});

	//Bernardo 20-02-2014 Marcar todas as regiões
	$view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

	//Bernardo 20-02-2014 Marcar todas as ufs
	$view.on("click", "#alinkUf", function(){ marcaTodasUFs($('.filtro-uf'),$view,$scope)});

	//Marcar todas aplicações
	$view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});
	
	
	//Matar processo
	$view.on("click", ".btn-cancelar", function(){
		if(thread !== undefined){
		  console.log("Cancelando thread...");
		  $scope.status_extrator = "";
		  $scope.$apply();
		  //clearInterval(intervaloExtratorStatus);

		  if(fs.existsSync(tempDir3()+"log_extrator.txt")){
			fs.unlinkSync(tempDir3()+"log_extrator.txt");
		  }
			//clearInterval(intervaloExtratorStatusAux);
			$('.notification .ng-binding').text("Operação cancelada.").selectpicker('refresh');
			thread.kill();
		}
	});

	$view.on("change", "select.filtro-regiao", function () {
		var filtro_regioes = $(this).val() || [];
		Estalo.filtros.filtro_regioes = filtro_regioes;
	});

	$view.on("change", "select.filtro-segmento", function () {
		Estalo.filtros.filtro_segmentos = $(this).val();
	});

	$view.on("change", "select.filtro-op", function () {
		Estalo.filtros.filtro_operacoes = $(this).val();
	});

	


	$view.on("change", "select.filtro-ud", function () {
		Estalo.filtros.filtro_ud = $(this).val();
	});

	$view.on("click", "#um", function () {
	  $("select.filtro-cols").prop("disabled",false).selectpicker("refresh");
	  $('#alinkCols').removeAttr('disabled');
	});

	$view.on("click", "#quatro", function () {
	  $("select.filtro-cols").prop("disabled",true).selectpicker("refresh");
	  $('#alinkCols').attr('disabled','disabled');
	});
	
	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
		$("div.data-inicio.input-append.date").data("datetimepicker").hide();
		$("div.data-fim.input-append.date").data("datetimepicker").hide();
		$('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
		$('div.btn-group.filtro-aplicacao').addClass('open');
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
		$('div.btn-group.filtro-aplicacao').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
		$('div.btn-group.filtro-site').addClass('open');
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
		$('div.btn-group.filtro-site').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
	  if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
		$('div.btn-group.filtro-segmento').addClass('open');
		$('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
		$('div.btn-group.filtro-segmento').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
	  if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
		$('div.btn-group.filtro-ddd').addClass('open');
		$('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px', 'margin-left':'-200px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
		$('div.btn-group.filtro-ddd').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-uf').mouseover(function () {
		$('div.btn-group.filtro-uf').addClass('open');
		$('div.btn-group.filtro-uf>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-uf').mouseout(function () {
		$('div.btn-group.filtro-uf').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-ud').mouseover(function () {
		$('div.btn-group.filtro-ud').addClass('open');
		$('div.btn-group.filtro-ud>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-ud').mouseout(function () {
		$('div.btn-group.filtro-ud').removeClass('open');
	});

	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-cols').mouseover(function () {
	  if(!$('div.btn-group.filtro-cols .btn').hasClass('disabled')){
		$('div.btn-group.filtro-cols').addClass('open');
		$('div.btn-group.filtro-cols>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px', 'margin-left':'-200px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-cols').mouseout(function () {
		$('div.btn-group.filtro-cols').removeClass('open');
	});		
	
	// OCULTAR AO TIRAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-contaonline').mouseout(function () {
		$('div.btn-group.filtro-contaonline').removeClass('open');
	});
	
	// EXIBIR AO PASSAR O MOUSE
	$('div > ul > li >>div> div.btn-group.filtro-contaonline').mouseover(function () {
	  if(!$('div.btn-group.filtro-contaonline .btn').hasClass('disabled')){
		$('div.btn-group.filtro-contaonline').addClass('open');
		$('div.btn-group.filtro-contaonline>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
	  }
	});		

	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseover(function () {
	  if(!$('div.btn-group.filtro-op .btn').hasClass('disabled')){
		$('div.btn-group.filtro-op').addClass('open');
		$('div.btn-group.filtro-op>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
	  }
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseout(function () {
		$('div.btn-group.filtro-op').removeClass('open');
	});


	
	
	// EXIBIR AO PASSAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
		$('div.btn-group.filtro-regiao').addClass('open');
	});

	// OCULTAR AO TIRAR O MOUSE
	$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
		$('div.btn-group.filtro-regiao').removeClass('open');
	});

	var limite = 999;
	$scope.valorCol = limite;

	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};

	// Lista chamadas conforme filtros
	$view.on("click", ".btn-gerar", function () {
		limpaProgressBar($scope, "#pag-extratores");
		//Testa se data início é maior que a data fim
		var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
		var testedata = testeDataMaisHora(data_ini, data_fim);
		if (testedata !== "") {
			setTimeout(function () {
				atualizaInfo($scope,testedata);
				effectNotification();
				$view.find(".btn-gerar").button('reset');
			}, 500);
			return;
		}
		
		//Testa se uma aplicação foi selecionada
		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
		  setTimeout(function () {
			atualizaInfo($scope, 'Selecione uma aplicação');
			effectNotification();
			$view.find(".btn-gerar").button('reset');
		  }, 500);
		  return;

		}
		
		if(typeof filtro_aplicacoes === "string") filtro_aplicacoes = [filtro_aplicacoes];
		var filtro_cols = $view.find("select.filtro-cols").val() || [];
		if (filtro_cols.length < 1){
			setTimeout(function () {
				atualizaInfo($scope, 'Selecione no mínimo 1 coluna.');
				effectNotification();
				$view.find(".btn-gerar").button('reset');
			}, 500);
			return;
		}
		
		//Testa se um ddd ou uf foi selecionado quando região foi marcada
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ufs = $view.find("select.filtro-uf").val() || [];
		
		obtemEdsDasAplicacoes($('select.filtro-aplicacao').val());
		
		var seguir = [];
		
		function teste(){
			setTimeout(function(){
				var itens = typeof $('select.filtro-aplicacao').val() ==='string' ? [$('select.filtro-aplicacao').val()] : $('select.filtro-aplicacao').val();
				for(var i=0; i < itens.length; i++){
					if(cache.aplicacoes[itens[i]].hasOwnProperty('estados')){
						if(seguir.indexOf(itens[i])< 0) seguir.push(itens[i]);
					}
				}
				
				if(seguir.length >= itens.length){
					clearTimeout(teste);
					console.log("seguir daqui");
					$scope.listaDados.apply(this);
				}else{
					teste();
				}
			},500);
		}
		teste();
	});
	
	$view.on("click", ".btn-exportar", function () {
		$scope.exportaXLSX.apply(this);
	});

	//Limpa filtros
	$view.on("click", ".btn-limpar-filtros", function () {
		$scope.limparFiltros.apply(this);
	});
	
	$scope.limparFiltros = function () {
		iniciaFiltros();
		componenteDataHora ($scope,$view,true);		

		
		var partsPath = window.location.pathname.split("/");
		var part = partsPath[partsPath.length - 1];
		
		var $btn_limpar = $view.find(".btn-limpar-filtros");
		$btn_limpar.prop("disabled", true);
		$btn_limpar.button('loading');
		
		setTimeout(function () {
			$btn_limpar.button('reset');
			}, 500);
	}

	//Botão agora
	$view.on("click", ".btn-agora", function () {
		$scope.agora.apply(this);
	});

	//02/04/2014
	$scope.agora = function () {
		iniciaAgora($view,$scope);
	}

	// Botão abortar 23/05/2014
	$view.on("click", ".abortar", function () {
		$scope.abortar.apply(this);
	});

	//23/05/2014
	$scope.abortar = function () {
		abortar($scope, "#pag-extratores");
	}

	if($scope.preview){
		//Ações comuns a todos os tipos de extração
		intervaloExtratorStatus = setInterval(function (){			
			if(fs.existsSync(tempDir3()+"log_extrator.txt")){
				var lines = fs.readFileSync(tempDir3()+"log_extrator.txt").toString().split('/n');
				var lastLines = "";
				
				for (var i = 0; i < lines.length; i++){
					if(lines[i] !== undefined) lastLines = lastLines.concat(lines[i]+'\r\n');
				}
				$scope.linkArqContent = lastLines;
				linkArqContent = $scope.linkArqContent;
				
				if($('#rolAuto').prop('checked') === true){
					var textarea = document.getElementById('uar');
					if(textarea!==null) textarea.scrollTop = textarea.scrollHeight;
				}
			}
			$scope.$apply();
		}, 1000);
	}
	$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});

	//Lista chamadas conforme filtros
	$scope.listaDados = function () {
		$scope.linkArq = "";
		$scope.linkArqContent = "";
		linkArq = "";
		linkArqContent = "";
		
		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
		$btn_exportar.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

		var getDate = formataDataHoraBR(data_ini);
		var mes = getDate.substr(3, 2);

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ufs = $view.find("select.filtro-uf").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];		
		var filtro_ud = $view.find("select.filtro-ud").val() || [];
		var filtro_operacoes = $view.find("select.filtro-op").val() || [];
		
		
		var filtro_ed =  $scope.filtroEDSelect || [];
		
		var filtro_contas = $view.find("select.filtro-contaonline").val() || [];
		var filtro_dadospessoais = $('#dadosP').prop('checked');
		var filtro_sms = $('#dadosSMS').prop('checked');
		var filtro_boleto = $('#dadosBOLETO').prop('checked');
		var filtro_contestacao = $('#dadosCONTESTACAO').prop('checked');
		var filtro_rangePercent = !$('li #output1').hasClass('ng-hide') ? $('#output1').val() : "";
		
		
	   
		var filtro_ic =  $scope.filtroICSelect || [];
		var filtro_cols = $view.find("select.filtro-cols").val() || [];		
		var filtro_transferid = $view.find('#transferid').val() || "";

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
		+ " até " + formataDataHoraBR($scope.periodo.fim);
		if (numanis.length > 0 && $('#data').prop("checked") == false){$scope.filtros_usados = "";}
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
		if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
		if (filtro_ufs.length > 0) { $scope.filtros_usados += " UFs: " + filtro_ufs; }
		if (filtro_ud.length > 0) { $scope.filtros_usados += " UDs: " + filtro_ud; }
		if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
		if (filtro_contas.length > 0) { $scope.filtros_usados += " Respostas: " + filtro_contas; }
		if (filtro_rangePercent !== "") { $scope.filtros_usados += " Confiança: " + filtro_rangePercent+"%"; }
		if (filtro_cols.length > 0 && !$("select.filtro-cols").prop('disabled')) { $scope.filtros_usados += " Colunas: " + unique2(cols.map(function(c){ if(filtro_cols.indexOf(c.codigo)>=0){return c.nome}})); }
        
		if (filtro_regioes.length > 0 && filtro_ddds.length === 0){
			var options = [];
			var v = [];
			filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
			unique(v).forEach((function (filtro_ddds) {
				return function (codigo) {
					var ddd = cache.ddds.indice[codigo];
					filtro_ddds.push(codigo);
				};
			})(filtro_ddds));
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
		}else if (filtro_regioes.length > 0 && filtro_ddds.length !== 0){
			$scope.filtros_usados += " Regiões: " + filtro_regioes;
			$scope.filtros_usados += " DDDs: " + filtro_ddds;
		}
		
		if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
		if (filtro_ic.length > 0) { $scope.filtros_usados += " ICs: " + filtro_ic; }
		if (filtro_transferid !== "") { $scope.filtros_usados += " TransferID: " + filtro_transferid; }
		
		
		if (filtro_dadospessoais) { $scope.filtros_usados += " Exibe DP"; }
		if (filtro_boleto) { $scope.filtros_usados += " Exibe BOLETO"; }
		if (filtro_contestacao) { $scope.filtros_usados += " Exibe CONTESTACAO"; }
		if (filtro_sms) { $scope.filtros_usados += " Exibe SMS"; }
		
		$scope.dados = [];
		var executaQuery = "";

		if(thread !== undefined){
		  thread.kill();
		}
		
		if($scope.tipoExt === "1"){
			var colunas = unique2(cols.map(function(c){ if(filtro_cols.indexOf(c.codigo)>=0){return c.descricao; } }));
			//query extrator
			var stmt = db.use + " /*Extrator_Estalo*/ SELECT I.DataHora_Inicio" + (colunas.length > 0 ? (",").concat(colunas.toString()) : "")+""
			+ " FROM IVRCDR as I"			
			
			if(filtro_cols.indexOf("9") >= 0){
				stmt += " left outer join Estado_Dialogo E on E.Cod_Estado = I.CodUltEstadoRec"
			}
			if(filtro_cols.indexOf("16") >= 0){
				stmt += " left outer join CONTRATO_CHAMADA AS CC ON"
				+" CC.CodUCIDIVR = I.CodUCIDIVR AND CC.CodAplicacao = I.CodAplicacao"
			}
			/*if (filtro_cols.indexOf("17") >= 0) {
				stmt += " left outer join CPF_CHAMADA AS CP ON"
				+ " CP.CodUCIDIVR = I.CodUCIDIVR AND CP.CodAplicacao = I.CodAplicacao"
			}*/			
			//if (filtro_cols.indexOf("47") >= 0 || filtro_cols.indexOf("49") >= 0) {
			if (filtro_cols.indexOf("63") >= 0) {
				stmt += " left outer join CALLLOG AS C ON"
				+ " C.CodUCID = I.CodUCIDIVR"
			}
			
			stmt += " WHERE 1 = 1"
			+ " AND I.DataHora_Inicio >= '" + formataDataHora(data_ini) + "'"
			+ " AND I.DataHora_Inicio <= '" + formataDataHora(data_fim) + "'";
			
			if (filtro_aplicacoes.map === undefined) {
				filtro_aplicacoes = [filtro_aplicacoes];
			}
			
			stmt += restringe_consulta("I.CodAplicacao", filtro_aplicacoes, true)
			
			if(filtro_cols.indexOf("9") >= 0){
				stmt += restringe_consulta("E.Cod_Aplicacao", filtro_aplicacoes, true)
			}
			
			stmt += restringe_consulta_like("I.Sequencia_EstadoDialogo", filtro_ed, $scope.oueED);
			stmt += restringe_consulta_like("XMLIVR", filtro_ic,$scope.oueIC,true);
			
			if (filtro_transferid !== "") {
				stmt += " and ( I.TransferID = '"+filtro_transferid+"' or I.TrnId = '"+filtro_transferid+"' or  I.PontoDerivacao = '"+filtro_transferid+"') ";
			}
			
			stmt += restringe_consulta("substring(I.ClienteTratado,1,2)", filtro_ddds, true)
			stmt += restringe_consulta("substring(I.ClienteTratado,len(I.ClienteTratado),1)", filtro_ud, true)
			stmt += restringe_consulta("I.SegmentoChamada", filtro_segmentos, true)
			stmt += restringe_consulta("I.CodSite", filtro_sites, true)
			stmt += restringe_consulta("I.CodPerfil", filtro_operacoes, true)
			//if(!$scope.iit) stmt += " and indicdelig <> 'IIT' ";
			stmt += " Order By I.DataHora_Inicio";
			
			var header = unique2(cols.map(function(c){ if(filtro_cols.indexOf(c.codigo)>=0){return c.nome; } }));
			header.unshift('Data hora');
			
			$scope.filtros.relOrExt === "ext" ? log("Extrator -> "+stmt) : log(stmt);
			
			stmt_mod = stmt;
			stmt_mod_ = stmt_mod.replace(/\"/g, "\\\"");
			datainicio = $scope.periodo.inicio;
			datafim = $scope.periodo.fim;
			if(fs.existsSync(tempDir3()+"log_extrator.txt")){
				fs.unlinkSync(tempDir3()+"log_extrator.txt");
			}
			
			/*if(moment(new Date(geraDataString(0,"23"))).isBetween(data_ini, data_fim) || moment(new Date(geraDataString(0,"00"))).isBetween(data_ini, data_fim) || moment(new Date(geraDataString(0,"01"))).isBetween(data_ini, data_fim)){*/
			//fs.writeFileSync('timezonechange.bat','tzutil /s "E. South America Standard Time_dstoff"');
			//require('child_process').exec('cmd /c timezonechange.bat', function(){
				
				executaThread2(datainicio, datafim, filtro_aplicacoes.toString(), header, stmt_mod_, $scope.tipoConsul, $scope,undefined,undefined,numanis,$scope.extratorArquivoPorData,$scope.valor);
				setTimeout(function(){
					//fs.writeFileSync('timezonechange.bat','tzutil /s "E. South America Standard Time"');
					//require('child_process').exec('cmd /c timezonechange.bat', function(){
						//console.log("DST executado com sucesso");

            if (numanis.length > 0){
              $('#import2').css('background', '#f5f5f5').selectpicker('refresh');
              numanis = [];
              invalidsNumanis = [];
              $("#browser").val("");
            }

					//});
				},2000);
			//});
			

			for(var i = 0; i < filtro_aplicacoes.length; i++){
				if(fs.existsSync(tempDir3()+''+filtro_aplicacoes[i].toString()+'.tmp')){
					fs.unlinkSync(tempDir3()+''+filtro_aplicacoes[i].toString()+'.tmp');
				}
				fs.appendFileSync(tempDir3()+''+filtro_aplicacoes[i].toString()+'.tmp',JSON.stringify(cache.aplicacoes[filtro_aplicacoes[i].toString()].estados));
			}
		}
		
		$scope.filtros_usados != undefined ? filtros = $scope.filtros_usados : filtros = "";
		$scope.info_status = filtros;
		
		$('div.notification').show();
		setTimeout(function(){
			$('.ampulheta').fadeOut(500);
		},500);
		
		$btn_gerar.button('reset');
		//$scope.extrator = false;
		$scope.limite_registros = 20000;
		$scope.$apply();
		
		$view.on("mouseup", "tr.resumo", function () {
			var that = $(this);
			console.log('passei');
			$('tr.resumo.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});
	};
	
	
	$scope.abrirLinkPreview = function (){
		//console.log("Tentando abrir preview");
		//console.log($scope.linkArq);
		gui.Shell.openItem($scope.linkArq);
	}
}	
CtrlExtratores.$inject = ['$scope', '$globals'];
