function CtrlResumoRetencaoLiquida($scope, $globals) {

	win.title = "Resumo Retenção Líquida"; //Paulo Raoni 24/07/2019
	$scope.versao = versao;

	$scope.rotas = rotas;

	travaBotaoFiltro(0, $scope, "#pag-resumo-retencao-liquida", "Resumo Retenção Líquida");

	$scope.dados = [];
	$scope.csv = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;

	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};


	$scope.log = [];
	$scope.aplicacoes = [];
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.tlv = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		segmentos: [],
		sites: [],
		isHFiltroED: false,
		isHFiltroIC: false
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;


	$scope.porDDD = null;

	//02/06/2014 Flag
	$scope.porRegEDDD = $globals.agrupar;
	$scope.aba = 2;


	function geraColunas() {
		var array = [];

		array.push({
			field: "datReferencia",
			displayName: "Data",
			width: 100,
			pinned: true
		});
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

		if (filtro_segmentos.length > 0) {
			array.push({
				field: "segmentos",
				displayName: "Segmento",
				width: 95,
				pinned: false,
			});
		}

		array.push({
			field: "qtdEntrantes",
			displayName: "Entrantes",
			width: 100,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidas",
			displayName: "Retidas",
			width: 85,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdPercRetidas",
			displayName: "% Retidas",
			width: 85,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdRetidasLiq15m",
			displayName: "Retidas Líquidas 15m",
			width: 160,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidasLiq30m",
			displayName: "Retidas Líquidas 30m",
			width: 160,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidasLiq45m",
			displayName: "Retidas Líquidas 45m",
			width: 160,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidasLiq1h",
			displayName: "Retidas Líquidas 1H",
			width: 150,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		},
			{
				field: "qtdRetidasLiq2h",
				displayName: "Retidas Líquidas 2H",
				width: 150,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			},
			{
				field: "qtdRetidasLiq24h",
				displayName: "Retidas Líquidas 24H",
				width: 160,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			}, 
			{
				field: "qtdPercRetidasLiq15m",
				displayName: "% Retidas Líquidas 15M",
				width: 175,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
			},
			{
				field: "qtdPercRetidasLiq30m",
				displayName: "% Retidas Líquidas 30M",
				width: 175,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
			},
			{
				field: "qtdPercRetidasLiq45m",
				displayName: "% Retidas Líquidas 45M",
				width: 175,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
			},{
			field: "qtdPercRetidasLiq1h",
			displayName: "% Retidas Líquidas 1H",
			width: 165,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		}, {
			field: "qtdPercRetidasLiq2h",
			displayName: "% Retidas Líquidas 2H",
			width: 165,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		}, {
			field: "qtdPercRetidasLiq24h",
			displayName: "% Retidas Líquidas 24H",
			width: 175,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		});

		return array;
	}


	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);


	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]) : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]) : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: mesAnterior(dat_consoli),
		max: dat_consoli*/
	};


	var $view;

	$scope.$on('$viewContentLoaded', function () {

		$view = $("#pag-resumo-retencao-liquida");


		$(".aba5").css({
			'position': 'fixed',
			'left': '55px',
			'right': 'auto',
			'margin-top': '35px',
			'z-index': '1'
		});
		$(".aba6").css({
			'position': 'fixed',
			'left': '390px',
			'right': 'auto',
			'margin-top': '35px',
			'z-index': '1'
		});
		$('.navbar-inner').css('height', '70px');
		$(".botoes").css({
			'position': 'fixed',
			'left': 'auto',
			'right': '25px',
			'margin-top': '35px'
		});


		carregaAplicacoes($view, false, false, $scope);
		componenteDataMaisHora($scope, $view);

		carregaSegmentosPorAplicacao($scope, $view, true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view)
		});

		//habilita o checkbox agrupar para segmento
		$view.on("change", "select.filtro-segmento", function () {
			$('#porRegEDDD').removeAttr('disabled');
			$('#porRegEDDD').prop('checked', false);
		})



		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		//GILBERTO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });


		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});


		$view.on("click", ".btn-gerar", function () {

			limpaProgressBar($scope, "#pag-resumo-retencao-liquida");
			//22/03/2014 Testa se data início é maior que a data fim
			var testedata = testeDataMaisHora($scope.periodo.inicio, $scope.periodo.fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}
			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});


		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.porRegEDDD = false;
			$scope.chkMensal = false;
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {

			iniciaFiltros();
			componenteDataMaisHora($scope, $view, true);
			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash + '/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-resumo-retencao-liquida");
		}

		//Alex 27/02/2014

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	// Lista chamadas conforme filtros
	$scope.listaDados = function () {

			dadosDias = [],
			dadosSegmento = [],
			dadosEntrantes = [],
			dadosRetidas = [],
			dadosFinalizadas = [],
			dadosDerivadas = [],
			dadosAbandonadas = [],
			dadosPercDerivadas = [],
			dadosPercFinalizadas = [],
			dadosPercAbandonadas = [],
			dadosqtdRetidasLiq1h = [];
			dadosqtdRetidasLiq2h = [];
			dadosqtdRetidasLiq24h = [];
			dadosPercRetidasLiq1h = [];
			dadosPercRetidasLiq2h = [];
			dadosPercRetidasLiq24h = [];
			dadosqtdRetidasLiq15m = [];
			dadosqtdRetidasLiq30m = [];
			dadosqtdRetidasLiq45m = [];



		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		//var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
			+ " até " + formataDataBR($scope.periodo.fim);
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		//if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }

		$scope.dados = [];
		var stmt = "";
		if (typeof (filtro_aplicacoes) === "string") filtro_aplicacoes = [filtro_aplicacoes];


		//Brunno
		// function segmentovarivel(vseg) {
		// 	if (filtro_segmentos.length > 0) {
		// 		return seg = " , " + vseg;
		// 	} else {
		// 		return seg = "";
		// 	}
		// }

		// Nenhuma aplicação selecionada (= todas as aplicações)
		// Exibir total de clientes únicos independente de aplicação (CodAplicacao = '')



		// segmentovarivel("CodSegmento")

		stmt = db.use +
		"with tcu as ( select CONVERT(DATE,_15min.DatReferencia) as DatReferencia, "+
		"Sum(_15min.QtdRetidasLiq) as rl15, "+
		"Sum(isnull(_30min.QtdRetidasLiq, 0)) as rl30, "+
		"Sum(isnull(_45min.QtdRetidasLiq, 0)) as rl45,  "+
		"Sum(isnull(_1h.QtdRetidasLiq, 0)) as rl1, "+
		"Sum(isnull(_2h.QtdRetidasLiq, 0)) as rl2,  "+
		"Sum(isnull(_24h.QtdRetidasLiq, 0)) as rl24 "+
		"from TotaisRetidasLiq15min _15min  "+
		"left outer join TotaisRetidasLiq30min _30min on _15min.DatReferencia = _30min.DatReferencia and _15min.CodAplicacao = _30min.CodAplicacao "+
		"left outer join TotaisRetidasLiq45min _45min on _15min.DatReferencia = _45min.DatReferencia and _15min.CodAplicacao = _45min.CodAplicacao "+
		"left outer join TotaisRetidasLiq1h _1h on _15min.DatReferencia = _1h.DatReferencia and _15min.CodAplicacao = _1h.CodAplicacao "+
		"left outer join TotaisRetidasLiq2h _2h on _15min.DatReferencia = _2h.DatReferencia and _15min.CodAplicacao = _2h.CodAplicacao "+
		"left outer join TotaisRetidasLiq24h _24h on _15min.DatReferencia = _24h.DatReferencia and _15min.CodAplicacao = _24h.CodAplicacao "+
		"where _15min.DatReferencia >= '" + formataData(data_ini) + "'" +" and _15min.DatReferencia <= '" + formataData(data_fim) + "'"+
		restringe_consulta("_15min.CodAplicacao", filtro_aplicacoes, true) +
		"GROUP BY _15min.DatReferencia )"	


		stmt = (stmt == "") ? db.use + " with" : stmt + ",";


		stmt += 
		" rdg as (" +
			" SELECT CONVERT(DATE,DAT_REFERENCIA) as DatReferencia, ";

		// if (filtro_segmentos.length > 0) {
		// 	stmt += "Cod_Segmento as CodSegmento, ";
		// 	seg1 = " ,Cod_Segmento";
		// 	seg_group = " ,rdg.CodSegmento"
		// } else {
		// 	stmt += "";
		// 	seg_group = "";
		// 	seg1 = "";
		// }

		stmt += "   SUM(QtdChamadas) as c," +
			"   SUM(QtdFinalizadas) as f, SUM(QtdDerivadas) as d, SUM(QtdAbandonadas) as a," +
			"   SUM(CAST(TempoEncerradas AS BIGINT)) as tempo" +
			" FROM " + db.prefixo + "resumodesempenhogeral" +
			" WHERE 1 = 1" +
			" AND CONVERT(DATE,Dat_Referencia) >= '" + formataData(data_ini) + "'" +
			" AND CONVERT(DATE,Dat_Referencia) <= '" + formataData(data_fim) + "'" +
			restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true) +
			//restringe_consulta2("Cod_Segmento", filtro_segmentos, true) +
			" GROUP BY CONVERT(DATE,Dat_Referencia)" //+ seg1;
		stmt += ")";

		stmt += " SELECT rdg.DatReferencia, rdg.c, rdg.f, rdg.d, rdg.a, rdg.tempo,tcu.rl1,tcu.rl2,tcu.rl24,tcu.rl15,tcu.rl30,tcu.rl45 " //+ seg_group;
		stmt += " FROM rdg";
		stmt += " left outer join tcu on rdg.DatReferencia = tcu.DatReferencia";
		stmt += " ORDER BY rdg.DatReferencia" //+ seg_group;

		executaQuery = executaQuery2;

		log(stmt);
		$scope.filtros_usados += $scope.parcial ? " REGISTRO PARCIAL" : "";



		var stmtCountRows = stmtContaLinhas(stmt);

		// Alex 24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			$globals.numeroDeRegistros = columns[0].value;
		}

		function executaQuery2(columns) {
			var data_hora = columns["DatReferencia"].value,
				datReferencia = typeof columns[0].value === 'string' ? formataDataBRString(data_hora) : formataDataBR(data_hora),
				segmentos = columns["CodSegmento"] !== undefined ? (columns["CodSegmento"].value === null ? segmento = "ND" : cache.segs.indice[columns["CodSegmento"].value].nome) : undefined,
				qtdEntrantes = +columns["c"].value,
				qtdFinalizadas = +columns["f"].value,
				qtdAbandonadas = +columns["a"].value,
				qtdRetidas = qtdFinalizadas + qtdAbandonadas,
				qtdPercRetidas = qtdEntrantes !== 0 ? (100 * qtdRetidas / qtdEntrantes).toFixed(2) : 0,

				qtdRetidasLiq1h = columns["rl1"].value !== undefined ? +columns["rl1"].value : 0,
				qtdRetidasLiq2h = columns["rl2"].value !== undefined ? +columns["rl2"].value : 0,
				qtdRetidasLiq24h = columns["rl24"].value !== undefined ? +columns["rl24"].value : 0,
				qtdRetidasLiq15m = columns["rl15"].value !== undefined ? +columns["rl15"].value : 0,
				qtdRetidasLiq30m = columns["rl30"].value !== undefined ? +columns["rl30"].value : 0,
				qtdRetidasLiq45m = columns["rl45"].value !== undefined ? +columns["rl45"].value : 0,
				qtdPercRetidasLiq1h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq1h / qtdEntrantes).toFixed(2) : 0,
				qtdPercRetidasLiq2h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq2h / qtdEntrantes).toFixed(2) : 0,
				qtdPercRetidasLiq24h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq24h / qtdEntrantes).toFixed(2) : 0;
				qtdPercRetidasLiq15m = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq15m / qtdEntrantes).toFixed(2):0;
				qtdPercRetidasLiq30m = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq30m / qtdEntrantes).toFixed(2):0;
				qtdPercRetidasLiq45m = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq45m / qtdEntrantes).toFixed(2):0;


			$scope.dados.push({
				data_hora: data_hora,
				datReferencia: datReferencia,
				segmentos: segmentos,
				qtdEntrantes: qtdEntrantes,
				qtdRetidas: qtdRetidas,
				qtdPercRetidas: qtdPercRetidas,
				qtdRetidasLiq1h: qtdRetidasLiq1h,
				qtdRetidasLiq2h: qtdRetidasLiq2h,
				qtdRetidasLiq24h: qtdRetidasLiq24h,
				qtdRetidasLiq15m:qtdRetidasLiq15m,
				qtdRetidasLiq30m:qtdRetidasLiq30m,
				qtdRetidasLiq45m:qtdRetidasLiq45m,
				qtdPercRetidasLiq1h: qtdPercRetidasLiq1h,
				qtdPercRetidasLiq2h: qtdPercRetidasLiq2h,
				qtdPercRetidasLiq24h: qtdPercRetidasLiq24h,
				qtdPercRetidasLiq15m:qtdPercRetidasLiq15m,
				qtdPercRetidasLiq30m:qtdPercRetidasLiq30m,
				qtdPercRetidasLiq45m:qtdPercRetidasLiq15m,
			});

			var a = [];
			a.push(datReferencia);
			if (segmentos !== undefined && segmentos !== null) {
				a.push(segmentos);
			}

			a.push(
				qtdEntrantes,
				qtdRetidas,
				qtdPercRetidas,
				qtdRetidasLiq1h,
				qtdRetidasLiq2h,
				qtdRetidasLiq24h,
				qtdRetidasLiq15m,
				qtdRetidasLiq30m,
				qtdRetidasLiq45m,
				qtdPercRetidasLiq15m,
				qtdPercRetidasLiq30m,
				qtdPercRetidasLiq45m,
				qtdPercRetidasLiq1h,
				qtdPercRetidasLiq2h,
				qtdPercRetidasLiq24h
				
			)

			$scope.csv.push(a);

			if (typeof data_hora === 'string') {
				dadosDias.push({
					dataBR: formataDataBRString(data_hora),
					data: formataData2String(data_hora)
				});
			} else {
				dadosDias.push({
					dataBR: formataDataBR(data_hora),
					data: formataData2(data_hora)
				});
			}

			dadosSegmento.push(segmentos);
			dadosEntrantes.push(qtdEntrantes);
			dadosRetidas.push(qtdRetidas);
			
			dadosqtdRetidasLiq1h.push(qtdRetidasLiq1h);
			dadosqtdRetidasLiq2h.push(qtdRetidasLiq2h);
			dadosqtdRetidasLiq24h.push(qtdRetidasLiq24h);
			dadosPercRetidasLiq1h.push(qtdPercRetidasLiq1h);
			dadosPercRetidasLiq2h.push(qtdPercRetidasLiq2h);
			dadosPercRetidasLiq24h.push(qtdPercRetidasLiq24h);
			dadosqtdRetidasLiq15m.push(qtdPercRetidasLiq15m);
		dadosqtdRetidasLiq30m.push(qtdPercRetidasLiq30m)
		dadosqtdRetidasLiq45m.push(qtdPercRetidasLiq45m)

			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}

		db.query(stmt, executaQuery2, function (err, num_rows) {
			num_rows = $scope.dados.length;
			console.log("Executando query-> " + stmt + " " + num_rows);
			var datFim = "";

			retornaStatusQuery(num_rows, $scope, datFim);
			$btn_gerar.button('reset');
			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportar_csv.prop("disabled", false);
				$btn_exportar_dropdown.prop("disabled", false);
			}

		});

	};


	//Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);
		$btn_exportar.button('loading');

		var colunas = [];
		for (i = 0; i < $scope.colunas.length; i++) {
			if ($scope.colunas[i].field !== "ucid") colunas.push($scope.colunas[i].displayName);
		}

		// var colunasFinal = [colunas];

		console.log(colunas);

		var dadosExcel = [colunas].concat($scope.csv);

		var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

		var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
		var wb = XLSX.utils.book_new();
		var milis = new Date();
		var arquivoNome = 'tvendas_' + formataDataHoraMilis(milis) + '.xlsx';
		XLSX.utils.book_append_sheet(wb, ws)
		console.log(wb);
		XLSX.writeFile(wb, tempDir3() + arquivoNome, {
			compression: true
		});
		console.log('Arquivo escrito');
		childProcess.exec(tempDir3() + arquivoNome);

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);

	}

}
CtrlResumoRetencaoLiquida.$inject = ['$scope', '$globals'];