/*
 * CtrlIncentivo
 */
function CtrlResIncentivo($scope, $globals) {

    //Alex 10/05/2014 Andamento da extração
    intervaloExtratorStatus = undefined;
    intervaloExtratorStatusAux = undefined;

    intervaloExtratorStatusAux = setInterval(function () {
        if (extratorStatus !== "") {
            $scope.status_extrator = extratorStatus;
            $scope.$apply();
        }
    }, 500);

    win.title = "Resumo de Incentivos"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;


    //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
    $scope.filtros = {
        isHFiltroED: false
    };

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoVS"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-incentivo", "Resumo de Incentivos");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    //$scope.limite_registros = 3000;

    $scope.vendas = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "vendas",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.total_valores;
    $scope.total_aceite;
    $scope.total_naceite;
    $scope.ordenacao = ['data_hora', 'incentivo'];
    $scope.decrescente = false;

    $scope.extrator = false;
    $scope.status_extrator;

    $scope.log = [];


    /*// Filtro: região
    $scope.regioes = regioes;*/

    // Filtro: ddd
    $scope.ddds = cache.ddds;


    //filtros_usados
    $scope.filtros_usados = "";


    $scope.filtro_aplicacoes = [];

    $scope.valor = "incentivo";
    $scope.oferta = false;
    $scope.viewFormat = null;

    // Filtro: incentivo
    $scope.filtro_incentivos = [];

    /*cache.produtos_vendas.indice = geraIndice(cache.produtos_vendas);
    cache.grupos_produtos_vendas.indice = geraIndice(cache.grupos_produtos_vendas);*/


    $scope.aba = 2;


    function geraColunas() {
        var array = [];

        if ($scope.valor === "incentivo") {
            array.push({ field: "descricao", displayName: "Descrição", width: 300, pinned: false }
                      );
        } else if ($scope.valor === "hora") {
            array.push({ field: "data_hora_BR", displayName: "Data e hora", width: 200, pinned: false }
                      );
        } else if ($scope.valor === "dia") {
            array.push({ field: "data_hora_BR", displayName: "Data", width: 200, pinned: false }
                      );
        }
        if ($scope.oferta) {
          array.push({ field: "oferta", displayName: "Oferta", width: 150, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' })
        }


        array.push({ field: "total", displayName: "Total", width: 150, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "aceite", displayName: "Tot. Aceite", width: 150, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "naoaceite", displayName: "Tot. Não Aceite", width: 150, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                   { field: "percentual", displayName: "% Aceite", width: 150, pinned: false }
                  );
        return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = horaAnterior();
    Estalo.filtros.filtro_incentivos = [];

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-resumo-incentivo");
		

        //19/03/2014
        componenteDataHora($scope, $view);

        carregaAplicacoes($view, false, true, $scope);


        carregaSegmentosPorAplicacao($scope, $view, true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () { carregaSegmentosPorAplicacao($scope, $view) });

        carregaIncentivos($view, true);
        // Popula lista de  grupo de produtos a partir das aplicações selecionadas
        //$view.on("change", "select.filtro-aplicacao", function () { carregaGruposProdutoPorAplicacao($scope, $view) });

        carregaDDDs($view,true);
        // Popula lista de  DDDs a partir das aplicações selecionadas
        //$view.on("change", "select.filtro-aplicacao", function () { carregaDDDsPorAplicacao($scope, $view) });






        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        $view.on("change", "select.filtro-incentivo", function () {
            Estalo.filtros.filtro_incentivos = $(this).val();
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        //2014-11-27 transição de abas
        var abas = [2, 3, 4, 5];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function () { marcaTodosIndependente($('.filtro-ddd'), 'ddds') });

        // Marca todos os produtos
        $view.on("click", "#alinkProd", function () { marcaTodosIndependente($('.filtro-produto'), 'produtos') });


        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope) });






        /*  $view.find("select.filtro-regiao").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} regiÃµes',
        showSubtext: true
        });*/

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });


        $view.find("select.filtro-incentivo").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} incentivos'
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-ddd').removeClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-grupo-produto').addClass('open');
            }

        });



        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-incentivo').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-incentivo .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-incentivo').addClass('open');
                $('div.btn-group.filtro-incentivo>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-incentivo').mouseout(function () {
            $('div.btn-group.filtro-incentivo').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-incentivo");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-baixarT", function () {

            limpaProgressBar($scope, "#pag-resumo-incentivo");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            if (_02d(data_ini.getMonth() + 1) !== _02d(data_fim.getMonth() + 1)) {
                setTimeout(function () {
                    atualizaInfo($scope, "Operação válida somente para o mesmo mês.");
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.consultaConsolidadoT.apply(this);
        });

        $view.on("click", ".btn-baixarB", function () {
            limpaProgressBar($scope, "#pag-resumo-incentivo");
            $scope.consultaConsolidadoB.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
			$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
			$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
            componenteDataHora($scope, $view, "venda");

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }


        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-incentivo");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
    });






    // Lista dados conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        $scope.filtros_usados = "";
        //if (!connection) {
        //  db.connect(config, $scope.listaVendas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_incentivos = $view.find("select.filtro-incentivo").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR(dataConsoliReal(data_fim));
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_incentivos.length > 0) { $scope.filtros_usados += " Incentivos: " + filtro_incentivos; }
        if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }


        $scope.vendas = [];
        var stmt = "";
        var executaQuery = "";
        var sufixo = "";
        if (filtro_segmentos.length > 0) {
            sufixo = "seg";
        }




        if ($scope.valor === "dia") {

            stmt = db.use + "with Tab1 as ("
            + " select convert(date,datreferencia) as datreferencia,CodDDD,codincentivo,sum(QtdIncentivos) as TotalNaoAceite,"
            + ""+($scope.oferta ? "codoferta as oferta, " : "")+""
            + " isnull((select sum(QtdIncentivos) from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" where"
            + " convert(date,datreferencia) = convert(date,r.datreferencia) and Sucesso = 0 and "
            + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalAceite"
            + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
            + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
            + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 1 "
            + " group by convert(date,datreferencia),CodAplicacao,codincentivo, "+($scope.oferta ? "codoferta, " : "")+" CodDDD"

            + " union "

            stmt += " select convert(date,datreferencia) as datreferencia,CodDDD,codincentivo, "+($scope.oferta ? "codoferta as oferta, " : "")+""
             + " isnull((select sum(QtdIncentivos) from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" where"
             + " convert(date,datreferencia) = convert(date,r.datreferencia) and Sucesso = 1 and "
             + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalNaoAceite,sum(QtdIncentivos) as TotalAceite"
             + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
             + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
             + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 0 "
            + " group by convert(date,datreferencia),CodAplicacao,codincentivo, "+($scope.oferta ? "codoferta, " : "")+" CodDDD) "

            + " select datreferencia,SUM(TotalAceite) as TotalAceite,  SUM(TotalNaoAceite) as TotalNaoAceite"
            + ""+($scope.oferta ? " , oferta" : "")+""
            + " from Tab1 t inner join Incentivo I on t.codincentivo = I.CodIncentivo"
            + " group by "+($scope.oferta ? "oferta, " : "")+"datreferencia"
            + " order by datreferencia"

            //stmt = db.use + "SELECT convert(date,RVP.DatReferencia),"
            //+ " sum(ValorVendido) as valor,"
            //+ " Sum(QtdVendas) as qtd_vendas"
            //+ " from " + db.prefixo + "ResumoVendaProduto" + sufixo + " as RVP"
            //+ " left outer join " + db.prefixo + "ProdutosURA as PU"
            //+ " on RVP.CodProduto = PU.CodProduto"
            //+ " WHERE 1 = 1"
            //+ " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'"
            //+ " AND RVP.DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
            //stmt += restringe_consulta("RVP.CodAplicacao", filtro_aplicacoes, true)

            //if (filtro_segmentos.length > 0) {
            //    stmt += restringe_consulta("RVP.CodSegmento", filtro_segmentos, true)
            //}

            //stmt += restringe_consulta("RVP.CodProduto", filtro_produtos, true)
            //stmt += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true)
            ////stmt += restringe_consulta_ddd("RVP.codDDD", filtro_regioes)
            //stmt += restringe_consulta("RVP.codDDD", filtro_ddds, true)
            //stmt += restringe_consulta("RVP.Resultado", filtro_status, true)
            //stmt += restringe_consulta("RVP.CodErro", filtro_erros, true)
            //stmt += " group by convert(date,RVP.DatReferencia)"
            //stmt += " order by convert(date,RVP.DatReferencia) ASC";

            executaQuery = executaQuery2;
            $scope.viewFormat = "Dia";

        } else if ($scope.valor === "hora") {

            stmt = db.use + "with Tab1 as ("
             + " select datreferencia,CodDDD,codincentivo,sum(QtdIncentivos) as TotalNaoAceite,"
             + ""+($scope.oferta ? "codoferta as oferta, " : "")+""
             + " isnull((select sum(QtdIncentivos) from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" where"
             + " datreferencia = r.datreferencia and Sucesso = 0 and "
             + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalAceite"
             + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
             + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
             + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 1 "
            + " group by datreferencia,CodAplicacao,codincentivo,"+($scope.oferta ? "codoferta, " : "")+"CodDDD "

            + " union "

            stmt += " select datreferencia,CodDDD,codincentivo,"+($scope.oferta ? "codoferta as oferta, " : "")+""
             + " isnull((select sum(QtdIncentivos) from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" where"
             + " datreferencia = r.datreferencia and Sucesso = 1 and "
             + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalNaoAceite,sum(QtdIncentivos) as TotalAceite"
             + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
             + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
             + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 0 "
            + " group by datreferencia,CodAplicacao,codincentivo,"+($scope.oferta ? "codoferta, " : "")+"CodDDD) "

            + " select datreferencia,SUM(TotalAceite) as TotalAceite,SUM(TotalNaoAceite) as TotalNaoAceite"
            + ""+($scope.oferta ? " , oferta" : "")+""
            + " from Tab1 t inner join Incentivo I on t.codincentivo = I.CodIncentivo"
            + " group by "+($scope.oferta ? "oferta, " : "")+"datreferencia"
            + " order by datreferencia"

            executaQuery = executaQuery3;
            $scope.viewFormat = "Hora";


        } else {

            stmt = db.use + "with Tab1 as ("
            + " select datreferencia,CodDDD,codincentivo,sum(QtdIncentivos) as TotalNaoAceite,"
            + ""+($scope.oferta ? "codoferta as oferta, " : "")+""
            + " isnull((select sum(QtdIncentivos) from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" where"
            + " datreferencia = r.datreferencia and Sucesso = 0 and "
            + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalAceite"
            + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
            + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
            + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 1 "
            + " group by datreferencia,CodAplicacao,codincentivo,"+($scope.oferta ? "codoferta, " : "")+"CodDDD "

            + " union "

            stmt += " select datreferencia,CodDDD,codincentivo,"+($scope.oferta ? "codoferta as oferta, " : "")+""
              + " isnull((select sum(QtdIncentivos) from ResumoIncentivos where"
              + " datreferencia = r.datreferencia and Sucesso = 1 and "
              + " CodIncentivo = r.CodIncentivo and CodDDD = r.CodDDD and CodAplicacao = r.CodAplicacao),0) as TotalNaoAceite,sum(QtdIncentivos) as TotalAceite"
              + " from ResumoIncentivos"+($scope.oferta ? "oferta" : "")+" R where "
              + " DatReferencia >= '" + formataDataHora(data_ini) + "'"
              + " AND DatReferencia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("codincentivo", filtro_incentivos, true)
            stmt += restringe_consulta("codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("codDDD", filtro_ddds,true)

            stmt += " and Sucesso = 0 "
            + " group by datreferencia,CodAplicacao,codincentivo,"+($scope.oferta ? "codoferta, " : "")+"CodDDD) "

            + " select I.Descricao,SUM(TotalAceite) as TotalAceite,SUM(TotalNaoAceite) as TotalNaoAceite"
            + ""+($scope.oferta ? " , oferta" : "")+""
            + " from Tab1 t inner join Incentivo I on t.codincentivo = I.CodIncentivo"
            + " group by "+($scope.oferta ? "oferta, " : "")+"I.Descricao"
            + " order by I.Descricao"

            executaQuery = executaQuery1;
            $scope.viewFormat = "Grupo";
        }

        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery1(columns) {

            var descricao = columns[0].value,
                total = +columns[1].value.toFixed(2) + +columns[2].value.toFixed(2),  //valor = toFixed(columns[2].value, 2), //Não ordena
                aceite = +columns[1].value,
                naoaceite = +columns[2].value,
                oferta = columns[3] !== undefined ? columns[3].value : "";

            $scope.total_valores = $scope.total_valores + total;
            $scope.total_aceite = $scope.total_aceite + aceite;
            $scope.total_naceite = $scope.total_naceite + naoaceite;

            $scope.vendas.push({
                descricao: descricao,
                total: total,
                 oferta:oferta,
                aceite: aceite,
                naoaceite: naoaceite,
                percentual: (100 * aceite / total).toFixed(2)
            });
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length,$scope,"#pag-resumo-vendas");
            //console.log($scope.vendas.length+" "+$globals.numeroDeRegistros);
            if ($scope.vendas.length % 1000 === 0) {
                $scope.$apply();
            }

        }

        function executaQuery2(columns) {

            var data_hora = formataDataBRString(columns[0].value),
                total = +columns[1].value.toFixed(2) + +columns[2].value.toFixed(2),  //valor = toFixed(columns[2].value, 2), //Não ordena
                aceite = +columns[1].value,
                naoaceite = +columns[2].value,
                oferta = columns[3] !== undefined ? columns[3].value : "";

            $scope.total_valores = $scope.total_valores + total;
            $scope.total_aceite = $scope.total_aceite + aceite;
            $scope.total_naceite = $scope.total_naceite + naoaceite;

            $scope.vendas.push({
                data_hora_BR: data_hora,
                total: total,
                oferta:oferta,
                aceite: aceite,
                naoaceite: naoaceite,
                percentual: (100 * aceite / total).toFixed(2)
            });


            //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length,$scope,"#pag-resumo-vendas");
            //console.log($scope.vendas.length+" "+$globals.numeroDeRegistros);
            if ($scope.vendas.length % 1000 === 0) {
                $scope.$apply();
            }

        }

        function executaQuery3(columns) {

            var data_hora = formataDataHoraBR(columns[0].value),
              total = +columns[1].value.toFixed(2) + +columns[2].value.toFixed(2),  //valor = toFixed(columns[2].value, 2), //Não ordena
              aceite = +columns[1].value,
              naoaceite = +columns[2].value,
              oferta = columns[3] !== undefined ? columns[3].value : "";

            $scope.total_valores = $scope.total_valores + total;
            $scope.total_aceite = $scope.total_aceite + aceite;
             $scope.total_naceite = $scope.total_naceite + naoaceite;

            $scope.vendas.push({
                data_hora_BR: data_hora,
                total: total,
                oferta:oferta,
                aceite: aceite,
                naoaceite: naoaceite,
                percentual: (100 * aceite / total).toFixed(2)
            });

            //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length,$scope,"#pag-resumo-vendas");
            //console.log($scope.vendas.length+" "+$globals.numeroDeRegistros);
            if ($scope.vendas.length % 1000 === 0) {
                $scope.$apply();
            }

        }



        $scope.total_valores = 0;
        $scope.total_aceite = 0;
        $scope.total_naceite = 0;

        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
        //console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);
        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');

            if ($scope.viewFormat === 'Dia' || $scope.viewFormat === 'Hora') {
                $('#pag-resumo-incentivo').css('width', '1185px');
                $scope.vendas.push({
                    data_hora: "",
                    data_hora_BR: "TOTAL",
                    total: $scope.total_valores,
                    aceite: $scope.total_aceite,
                    naoaceite: $scope.total_naceite,
                    percentual: (100 * $scope.total_aceite / $scope.total_valores).toFixed(2)
                });
            } else {
                $('#pag-resumo-incentivo').css('width', '1185px');
                $scope.vendas.push({
                    grupo: "",
                    descricao: "TOTAL",
                    total: $scope.total_valores,
                    aceite: $scope.total_aceite,
                    naoaceite: $scope.total_naceite,
                    percentual: (100 * $scope.total_aceite / $scope.total_valores).toFixed(2)
                });
            }

            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
            }
        });
        //});

        // GILBERTOOOOOO 17/03/2014



        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

    };


    /*// Consulta consolidado resumo venda TEMPLATE
    $scope.consultaConsolidadoT = function () {

        var $btn_baixarT = $(this);
        $btn_baixarT.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var vendasDetalhada= [];
        var stmt = db.use + "SELECT GrupoProduto, CodAplicacao,DatReferencia, CodDDD, PU.CodProduto, PU.Descricao, Resultado, codErro,"
        + " ValorVendido, QtdVendas from " + db.prefixo + "ResumoVendaProduto as RVP"
        + " left outer join " + db.prefixo + "ProdutosURA as PU on RVP.CodProduto = PU.CodProduto WHERE 1 = 1"
        + " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'"
        + " AND RVP.DatReferencia <= '" + formataDataHora(data_fim) + "'"
        + " order by GrupoProduto ASC";

        log(stmt);
        $scope.info_status = "Aguarde esta consulta pode demorar alguns minutos.";
                    $scope.$apply(function () {
                    });

        db.query(stmt, function (columns) {
            var codAplicacao = columns[0].value,
                datReferencia = formataDataBR(columns[1].value),
                codDDD = columns[2].value,
                codProduto = columns[3].value,
                descricao = columns[4].value,
                grupoProduto = columns[5].value,
                resultado = columns[6].value,
                codErro = columns[7].value,
                valorVendido = columns[8].value,
                qtdVendas = columns[9].value;

            vendasDetalhada.push({
                codAplicacao: codAplicacao,
                datReferencia: datReferencia,
                codDDD: codDDD,
                codProduto: codProduto,
                descricao: descricao,
                grupoProduto: grupoProduto,
                resultado: resultado,
                codErro: codErro,
                valorVendido: valorVendido,
                qtdVendas: qtdVendas
                });

        }, function (err, num_rows) {
            $btn_baixarT.button('reset');
            retornaStatusQuery(num_rows,$scope);
            $scope.info_status += " Processando arquivo, aguarde...";
                    $scope.$apply(function () {
                    });

            var file = 'resumoVendas_'+formataData($scope.periodo.inicio)+'.xlsx';

            var newData;

            fs.readFile('templates/tResumoVS2.xlsx', function(err, data) {

            // Create a template
            var t = new XlsxTemplate(data);
                // Perform substitution
                t.substitute(1, {

                    planDados: vendasDetalhada,

                });
                // Get binary data
                newData = t.generate();
                $scope.info_status += " Concluído.";
                $scope.$apply(function () {
                });

                regraParaGerarBinario(file,newData,$scope,0);

            });

        });
        return true;

    };*/










    /*// Consulta consolidado resumo venda BD
    $scope.consultaConsolidadoB = function () {


        var $btn_baixarB = $(this);
        $btn_baixarB.button('loading');

        var data_ini = $scope.periodo.inicio;

        var file = "";


        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='VENDAS'";
        //+ " AND CONVERT(VARCHAR(25), DataAtualizacao, 126) LIKE '" + formataData(data_ini) + "%'";
        log(stmt);

        var emissor = new (require("events").EventEmitter);

        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;
          var milis = new Date();

          file = 'resumoVendas_'+formataData(dataAtualizacao)+'_gerado_'+formataDataHoraMilis(milis)+'.xlsx';

          var arqDb = toArrayBuffer(arquivo);

          emissor.on('fim', function(){
            var buffer = toBuffer(arqDb);
            regraParaGerarBinario(file,buffer,$scope,5000);
            atualizaProgressBar(arquivo.length, arquivo.length,$scope,"#pag-resumo-vendas");
          });



        }, function (err, num_rows) {

            $scope.filtros_usados = "";
            $btn_baixarB.button('reset');
            retornaStatusQuery(num_rows,$scope);
            emissor.emit('fim');
        });
        return true;
    };*/

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var template = "";
        console.log($scope.viewFormat);
        if ($scope.viewFormat === "Dia" || $scope.viewFormat === "Hora") {
            template = 'tResumoIncentivoData'
        } else {
            template = 'tResumoIncentivo';
        }


        //Alex 15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
             + " WHERE NomeRelatorio='" + template + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = 'tResumoIncentivo.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = 'resumoIncentivo_' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.vendas,
                    total: $scope.total_valores,
                    qtd: $scope.total_qtds
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)
            
            //?
        });

    };
    /*$scope.celula = function (cod_status) {
        return (['success', 'danger', 'warning'])[cod_status];
    };*/
}
CtrlResIncentivo.$inject = ['$scope', '$globals'];
