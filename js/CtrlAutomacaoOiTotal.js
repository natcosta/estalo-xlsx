function CtrlAutomacaoOiTotal($scope, $globals) {
	
    win.title = "Automação Oi Total"; //Paulo 17/07/2019
    $scope.versao = versao;

    travaBotaoFiltro(0, $scope, "#pag-automacao-oi-total", "Automação Oi Total");

    $scope.dados = [];
    $scope.colunas = [];
    $scope.csv = []; 
       
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
   // $scope.segmentos = [];
    $scope.ordenacao = ['assunto'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {       
        produtos: [],
        empresas: [],
        tipoAutomacoes: []
    };	
	//$scope.operador = false;

    function geraColunas() {
        var array = [];

        array.push(
            {
                field: "CodUcidIVR",
                displayName: "CodUcidIVR",
                width: 150
            }, 
            {
                field: "CtXId", 
                displayName: "CtXId",
                width: 150
            },
            {
                field: "TelTratado",
                displayName: "TelTratado",
                width: 150
            },
            {
                field: "Produto",
                displayName: "Produto",
                width: 150
            },
            {
                field: "Epsip",
                displayName: "Epsip",
                width: 150
            },
            {
                field: "TipoBundle",
                displayName: "TipoBundle",
                width: 150
            },
            {
                field: "NumANI",
                displayName: "NumANI",
                width: 150
            },
            {
                field: "UsuarioCPF",
                displayName: "UsuarioCPF",
                width: 150
            },
            {
                field: "EPS",
                displayName: "EPS",
                width: 150
            },
            {
                field: "DataHora",
                displayName: "DataHora",
                width: 150
            },
            {
                field: "TipoAutomacao",
                displayName: "TipoAutomacao",
                width: 280
            },
            {
                field: "QtdPop1",
                displayName: "QtdPop1",
                width: 150
            },
            {
                field: "QtdPop2",
                displayName: "QtdPop2",
                width: 150
            },
            {
                field: "QtdSucesso",
                displayName: "QtdSucesso",
                width: 150
            },
            {
                field: "QtdFalha",
                displayName: "QtdFalha",
                width: 150
            },          
            {
                field: "TME",
                displayName: "TME",
                width: 150
            }
        );

        return array;
    }
 
    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-automacao-oi-total");

        //19/03/2014
        componenteDataHora($scope, $view);        
        carregaEmpresas($view);
        carregaProdutosTU($view,true);
        carregaTiposAutomacoes($view,true);
		
        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });      

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {    
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {           
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-automacao-pacote-total");
        }
		
		$view.on("mouseover",".grid",function(){$('div.notification').fadeIn(500)});

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {    

            limpaProgressBar($scope, "#pag-automacao-pacote-total");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();            
            $scope.listaDados.apply(this);
        });           
    });

     // Listar dados principais
     $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_produtotu = $view.find("select.filtro-produtotu").val() || [];
        var filtro_tipo_automacao = $view.find("select.filtro-tipo-automacao").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }

        if (filtro_produtotu.length > 0) {
            $scope.filtros_usados += " Produtos TU: " + filtro_produtotu;
        }

        if (filtro_tipo_automacao.length > 0) {
            $scope.filtros_usados += " Tipos de Automação: " + filtro_tipo_automacao;
        }
    
        
        $scope.dados = [];
        $scope.csv = [];
        var stmt = "";
        
       
        // $scope.filtro_empresas_join = (filtro_empresas.length > 0) ? "'"+filtro_empresas.join("','")+"'" : '';
        // $scope.filtro_produtostu_join = (filtro_produtotu.length > 0 ) ? "'"+filtro_produtotu.join("','")+"'" : '';
        
        // stmt = db2.use +
        // "SELECT * FROM TUAUTOMACAO WHERE 1 = 1 AND " + 
        // " DataHora >= '" + formataDataHora(data_ini) + "' AND " +
        // " DataHora <= '" + formataDataHora(data_fim) + "' " +
        // ($scope.filtro_empresas_join ? " AND EPS in (" + $scope.filtro_empresas_join + ")" : '')+
        // ($scope.filtro_produtostu_join ? " AND PRODUTO in (" + $scope.filtro_produtostu_join + ")" : '');
        

        $scope.filtro_empresas_join = (filtro_empresas.length > 0) ? "''"+filtro_empresas.join("'',''")+"''" : '';
        $scope.filtro_produtostu_join = (filtro_produtotu.length > 0 ) ? "''"+filtro_produtotu.join("'',''")+"''" : '';
        $scope.filtro_tipo_automacao_join = (filtro_tipo_automacao.length > 0 ) ? "''"+filtro_tipo_automacao.join("'',''")+"''" : '';

        stmt = db2.use +
        "Exec uspTUAUTOMACAO" + 
        " @dataIni = '" + formataDataHora(data_ini) +
        "', @dataFim = '" + formataDataHora(data_fim) + "'" +
        ($scope.filtro_empresas_join ? ", @eps = '" + $scope.filtro_empresas_join + "'" : '') +
        ($scope.filtro_produtostu_join ? ", @produto = '" + $scope.filtro_produtostu_join + "'" : '') +
        ($scope.filtro_tipo_automacao_join ? ", @tipoAutomacao = '" + $scope.filtro_tipo_automacao_join + "'" : '');
          
        log(stmt);

           
        function executaQuery2(columns) {
           
            $scope.CodUcidIVR = columns["CodUcidIVR"].value ? columns["CodUcidIVR"].value : undefined;
            $scope.CtXId = columns["CtXId"].value ? columns["CtXId"].value : undefined;
            $scope.TelTratado = columns["TelTratado"].value ? columns["TelTratado"].value : undefined;
            $scope.Produto = columns["Produto"].value ? columns["Produto"].value : undefined;
            $scope.Epsip = columns["Epsip"].value ? columns["Epsip"].value : undefined;
            $scope.TipoBundle = columns["TipoBundle"].value ? columns["TipoBundle"].value : undefined;
            $scope.NumANI = columns["NumANI"].value ? columns["NumANI"].value : undefined;
            $scope.UsuarioCPF = columns["UsuarioCPF"].value ? columns["UsuarioCPF"].value : undefined;
            $scope.EPS = columns["EPS"].value ? columns["EPS"].value : undefined;
            $scope.DataHora = columns["DataHora"].value ? columns["DataHora"].value : undefined;
            $scope.TipoAutomacao = columns["TipoAutomacao"].value ? columns["TipoAutomacao"].value : undefined;
            $scope.QtdPop1 = columns["QtdPop1"].value ? columns["QtdPop1"].value : undefined;
            $scope.QtdPop2 = columns["QtdPop2"].value ? columns["QtdPop2"].value : undefined;
            $scope.QtdSucesso = columns["QtdSucesso"].value ? columns["QtdSucesso"].value : undefined;
            $scope.QtdFalha = columns["QtdFalha"].value ? columns["QtdFalha"].value : undefined;
            $scope.TME = columns["TME"].value ? columns["TME"].value : undefined;

			var s = {                
                CodUcidIVR : $scope.CodUcidIVR,
                CtXId :  $scope.CtXId,
                TelTratado : $scope.TelTratado,
                Produto : $scope.Produto,
                Epsip : $scope.Epsip,
                TipoBundle : $scope.TipoBundle,
                NumANI : $scope.NumANI,
                UsuarioCPF : $scope.UsuarioCPF,
                EPS : $scope.EPS,
                DataHora : formataDataHoraBR($scope.DataHora),
                TipoAutomacao : $scope.TipoAutomacao,
                QtdPop1 : $scope.QtdPop1,
                QtdPop2 : $scope.QtdPop2,
                QtdSucesso : $scope.QtdSucesso,
                QtdFalha : $scope.QtdFalha,
                TME : $scope.TME,                          
            }
                                       
            $scope.dados.push(s);            
            var a = [];
            for (var item in $scope.colunas) {               
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);

            $scope.cumulative = function( grid, myRow ) {
                var myRowFound = false;                
                grid.renderContainers.body.visibleRowCache.forEach( function( row, index ) {
                  if( !myRowFound ) {
                    cumulativeTotal += row.entity.widgets;
                    if( row === myRow ) {
                      myRowFound = true;
                    }
                  }
                });
                return cumulativeTotal;
              };
            
            // log($scope.dados);
            
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }            
        }

        db2.query(stmt, executaQuery2, function (err, num_rows) {            
            num_rows = $scope.dados.length;
            console.log("Executando query-> " + stmt + " " + num_rows);
            var datFim = "";
            
            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
      
        });
        
    };

   
    // Exportar planilha XLSX para grid comum
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.csv);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'AutomacaoOiTotal' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };  
       
}

CtrlAutomacaoOiTotal.$inject = ['$scope', '$globals'];