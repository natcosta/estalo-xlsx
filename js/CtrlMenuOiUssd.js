/*
 *
 */
function CtrlMenuOiUssd($scope, $globals) {

    win.title = "Resumo MenuOi x USSD"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 5000;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoED"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-menuoi-ussd", "Resumo MenuOi x USSD");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.csv = [];

    $scope.decrescente = true;


    $scope.log = [];



    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };




    $scope.aba = 2;

    function geraColunas() {
        var array = [];


        array.push(
          { field: "data", displayName: "Data", width: 150, pinned: false},
          { field: "qtdUssd", displayName: "Qtd. USSD", width: 120, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>' },
          { field: "qtdMenuOi", displayName: "Qtd. Menu Oi", width: 120, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>' },
          { field: "qtdCadastros", displayName: "Qtd. Cadastros", width: 120, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>' },
          { field: "qtdRecargas", displayName: "Qtd. Recargas", width: 120, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>' }

        );
        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-menuoi-ussd");
      treeView('#pag-resumo-menuoi-ussd #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-menuoi-ussd #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-menuoi-ussd #ed', 65,'ED','dvED');
      treeView('#pag-resumo-menuoi-ussd #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-menuoi-ussd #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-menuoi-ussd #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-menuoi-ussd #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-menuoi-ussd #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-menuoi-ussd #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-menuoi-ussd #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-resumo-menuoi-ussd #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-resumo-menuoi-ussd #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/

        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        //minuteStep: 5

        //Alex 21/02/2014 datetime picker data separada

        //19/03/2014
        componenteDataMaisHora($scope, $view);






        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-menuoi-ussd");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-menuoi-ussd");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        $globals.numeroDeRegistros = 0;
        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);


        $scope.dados = [];
        $scope.csv = [];

        var tabela = "CruzamentoUSSDxMenuOi";

        stmt = db.use + '';

        stmt += " Select Data ,QtdUSSD, QtdMenuOi , QtdCadastros, QtdRecargas"
        + " FROM " + db.prefixo + "" + tabela + ""
        + " WHERE 1 = 1"
        + " AND CONVERT(DATE,Data) >= '" + formataData(data_ini) + "'"
        + " AND CONVERT(DATE,Data) <= '" + formataData(data_fim) + "'"
        + " ORDER BY Data";


        log(stmt);
        $scope.filtros_usados += $scope.parcial ? " <b>Registro PARCIAL</b>" : "";



        function executaQuery(columns) {


            var data = formataDataHoraBR(columns["Data"].value),
                qtdUssd = +columns["QtdUSSD"].value,
                qtdMenuOi = +columns["QtdMenuOi"].value,
                qtdCadastros = +columns["QtdCadastros"].value,
                qtdRecargas = +columns["QtdRecargas"].value;

            $scope.dados.push({
                data: data,
                qtdUssd: qtdUssd,
                qtdMenuOi: qtdMenuOi,
                qtdCadastros: qtdCadastros,
                qtdRecargas: qtdRecargas
            });

            $scope.csv.push([
                data,
                qtdUssd,
                qtdMenuOi,
                qtdCadastros,
                qtdRecargas
            ])
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-resumo-estados");
            //if ($scope.dados.length % 1000 === 0) {
            //    $scope.$apply();
            //}
        }



            db.query(stmt, executaQuery, function (err, num_rows) {
                userLog(stmt, 'Cruzando dados', 2, err)
                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                //$scope.filtros_usados += $scope.parcial ? "PARCIAL" : "";
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                    $btn_exportar.prop("disabled", false);
                    $btn_exportar_csv.prop("disabled", false);
                    $btn_exportar_dropdown.prop("disabled", false);
                }
            });

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.estado_de", function () {
            var that = $(this);
            $('tr.estado_de.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };




    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');


        var template = "tResumoMenuOiUssd";


        //Alex 15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
             + " WHERE NomeRelatorio='" + template + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = 'tResumoMenuOiUssd.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = 'resumoMenuOiUssd' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.dados
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
              userLog(stmt, 'Exportar XLSX', 2, err)
            //?
        });

    };
}
CtrlMenuOiUssd.$inject = ['$scope', '$globals'];
