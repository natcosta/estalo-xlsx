function CtrlPerformanceDDDUD($scope, $globals) {

  intervaloExtratorStatus = undefined;
  intervaloExtratorStatusAux = undefined;

  intervaloExtratorStatusAux = setInterval(function(){
    if(extratorStatus !== ""){
      $scope.status_extrator = extratorStatus;
      $scope.$apply();
    }
  },0);


    win.title = "Resumo por DDD e último dígito";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 5000;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    travaBotaoFiltro(0, $scope, "#pag-performance-dddud", "Resumo por DDD e último dígito");

    $scope.status_progress_bar = 0;
    $scope.dados = [];
    $scope.csv = [];
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];
    $scope.ordenacao = ['DatReferencia','DDD'];
    $scope.decrescente = false;
    $scope.porRegiao = null;
    $scope.extrator = false;
    $scope.status_extrator;
    $scope.iit = false;
    $scope.tlv = false;
    $scope.umDigito = true;


    $scope.colunas = [];
    $scope.gridDados = {
      data: "dados",
      columnDefs: "colunas",
      enableColumnResize: true,
      enablePinning: true
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: [],
        relOrExt: "rel",
        porDia: false,
        IndicVendas: false,
        isHFiltroED: false,
        isHFiltroIC: false
    };

    // Filtro: ddd/ufs/ud
    $scope.ddds = cache.ddds;
    $scope.ufs = cache.ufs;
    $scope.ud = ud;

  /*cache.produtos_vendas.indice = geraIndice(cache.produtos_vendas);*/

  function geraColunas(){
    var array = [];


    if($scope.filtros.porDia === true){
      array.push({ field: "DatReferencia", displayName: "Data", width: 100, pinned: true });
    }else{
      array.push({ field: "DatReferencia", displayName: "Data e hora", width: 150, pinned: true });
    }



        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];

    if(filtro_regioes.length > 0 && original_ddds.length === 0){
      array.push({ field: "regiao", displayName: "Região", width: 100, pinned: false });
    }else{
      array.push({ field: "DDD", displayName: "DDD", width: 100, pinned: false });
    }


      array.push({ field: "QtdChamadas", displayName: "Chamadas", width: 130, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                 { field: "QtdDerivadas", displayName: "Derivadas", width: 130, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                 { field: "QtdAbandonadas", displayName: "Abandonadas", width: 130, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                 { field: "QtdFinalizadas", displayName: "Finalizadas", width: 130, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
                 { field: "tma", displayName: "TMA", width: 65, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>' }
                );

      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-performance-dddud");

    componenteDataHora($scope, $view);
    carregaRegioes($view);
    carregaUFs($view,true);
    carregaDigitos($view,true);
    carregaAplicacoes($view,false,false,$scope);
    carregaSites($view);
    carregaDoisDigitos($view,true);




    carregaDDDsPorUF($scope,$view,true);
    // Popula lista de  ddds a partir das UFs selecionadas
    $view.on("change", "select.filtro-uf", function(){ carregaDDDsPorUF($scope,$view)});

    carregaDDDsPorRegiao($scope,$view,true);
    // Popula lista de  ddds a partir das regiões selecionadas
    $view.on("change", "select.filtro-regiao", function(){ carregaDDDsPorRegiao($scope,$view)});

    carregaSegmentosPorAplicacao($scope,$view,true);
    // Popula lista de segmentos a partir das aplicações selecionadas
    $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-uf").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} UFs',
            showSubtext: true
        });

        $view.find("select.filtro-ud").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} último dígito',
            showSubtext: true
        });
        
        
        $view.find("select.filtro-udd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} finais de telefone',
            showSubtext: true
        });

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-op").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} operações'
        });

        $view.find("select.filtro-segmento").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} segmentos'
        });     

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });


        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

        // Marca todos os ufs
        $view.on("click", "#alinkUf", function(){ marcaTodosIndependente($('.filtro-uf'),'ufs')});

        // Marca todos os uds
        $view.on("click", "#alinkUltDig", function(){ marcaTodosIndependente($('.filtro-ud'),'uds')});
          
        //Marca um RANGE de dígitos
        $view.on("click", "#alinkUltDoisDig", function(){
            //marcaTodosIndependente($('.filtro-ud'),'uds')
            
            rangePrompt('Determine a gama de valores a serem incluídos como "00-15".', "Marcar Valores", function callback(valor1, valor2){
              function doubleDigit(n){
                  return (n).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
              }
              
              if(valor1==0&&valor2==0){
                $('.filtro-udd').selectpicker('val', []);
              };
              
              if(valor1&&valor2){//valor.indexOf("-")
                console.log("Possui -");
                //console.log("Valor 1:"+valor.split("-")[0]+" Segundo Valor: "+valor.split("-")[1]);
                
                //console.log("Valor 2:"+valor.split("-")[0]);
                var i=doubleDigit(valor1);//valor.split("-")[0];
                var j=doubleDigit(valor2);//valor.split("-")[1];
                console.log("Valor 1:"+i+" Valor2: "+j);
                console.log("I: "+i+" J: "+j);
                rangeOfValues = [];            
                while(i<=j){//7
                  if(i==0){
                    rangeOfValues.push("00");
                  console.log("Adicionou: "+i);
                  }else{
                    rangeOfValues.push(doubleDigit(i));
                    console.log("Adicionou: "+doubleDigit(i));
                  }
                  i++;
                  //i=doubleDigit(i)+1;
                 }
              //$('.filtro-udd').selectpicker().val();
              $('.filtro-udd').selectpicker('val', rangeOfValues);
              
              }else{
                console.log("Não Possui -");
              }
              
              
            });
           });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        // Marca todos os operações
        $view.on("click", "#alinkOp", function(){ marcaTodosIndependente($('.filtro-op'),'ops')});

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

        //Bernardo 20-02-2014 Marcar todas as ufs
        $view.on("click", "#alinkUf", function(){ marcaTodasUFs($('.filtro-uf'),$view,$scope)});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});



        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        $view.on("change", "select.filtro-op", function () {
            Estalo.filtros.filtro_operacoes = $(this).val();
        });        

        $view.on("change", "select.filtro-ud", function () {
            Estalo.filtros.filtro_ud = $(this).val();
        });
        
        $view.on("change", "select.filtro-udd", function () {
            Estalo.filtros.filtro_udd = $(this).val();
        });

        var limite = 999;
        $scope.valorCol = limite;

        // Exibe mais registros
        $scope.exibeMaisRegistros = function () {
            $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        };

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-performance-dddud");
            //Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }


            //Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
			
			
			var filtro_ed = $scope.filtroEscolhido.ed || [];
			var filtro_ic = $scope.filtroEscolhido.ic || [];

			var filtro_ud = $view.find("select.filtro-ud").val() || [];
			var filtro_udd = $view.find("select.filtro-udd").val() || [];
		    var dig = filtro_ud.concat(filtro_udd);


            if (filtro_ed[0]!== undefined && filtro_ic[0]!== undefined) {
                    setTimeout(function () {
                        atualizaInfo($scope, '<font color = "Red">Filtros de estados de diálogo e itens de controle <B>NÃO</B> podem ser usados ao mesmo tempo neste relatório.</font>');
                        effectNotification();
                        $view.find(".btn-gerar").button('reset');
                        $view.find(".btn-exportar").prop("disabled", "disabled");
                    }, 500);
                    return;
                }   
				
			
			if (filtro_ed[0]!== undefined && dig[0]!== undefined || filtro_ic[0]!== undefined && dig[0]!== undefined) {
                    setTimeout(function () {
                        atualizaInfo($scope, '<font color = "Red">Filtros de estados de diálogo e itens de controle <B>NÃO</B> podem ser usados com filtros de 	EDs ou ICs.</font>');
                        effectNotification();
                        $view.find(".btn-gerar").button('reset');
                        $view.find(".btn-exportar").prop("disabled", "disabled");
                    }, 500);
                    return;
                }
			
			
            //Testa se um ddd ou uf foi selecionado quando região foi marcada
            var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
            var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
            var filtro_ufs = $view.find("select.filtro-uf").val() || [];


            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        //Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);
            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        //Botão agora
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //02/04/2014
        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-performance-dddud");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    //Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var getDate = formataDataHoraBR(data_ini);
        var mes = getDate.substr(3, 2);

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_ufs = $view.find("select.filtro-uf").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ud = $view.find("select.filtro-ud").val() || [];
        var filtro_udd = $view.find("select.filtro-udd").val() || [];
        var filtro_operacoes = $view.find("select.filtro-op").val() || [];
		
		var filtro_ed = $scope.filtroEscolhido.ed || [];
		if(filtro_ed[0]!== undefined){ $scope.filtros.isHFiltroED = true;}else{$scope.filtros.isHFiltroED = false;}
		var filtro_ic = $scope.filtroEscolhido.ic || [];
		if(filtro_ic[0]!== undefined){ $scope.filtros.isHFiltroIC = true;}else{$scope.filtros.isHFiltroIC = false;}


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
        if (filtro_ufs.length > 0) { $scope.filtros_usados += " UFs: " + filtro_ufs; }
        if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
        if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
        if (filtro_ic.length > 0) { $scope.filtros_usados += " ICs: " + filtro_ic; }

        if (filtro_regioes.length > 0 && filtro_ddds.length === 0) {
            var options = [];
            var v = [];
            filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
            unique(v).forEach((function (filtro_ddds) {
                return function (codigo) {
                    var ddd = cache.ddds.indice[codigo];
                    filtro_ddds.push(codigo);
                };
            })(filtro_ddds));
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
        } else if (filtro_regioes.length > 0 && filtro_ddds.length !== 0) {
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
            $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }

        $scope.dados = [];
        var executaQuery = "";


          var incluiCampos;

          if (original_ddds.length !== 0 || filtro_regioes.length === 0) {
            incluiCampos = function () { return ", UD.DDD"; };
          } else {
            incluiCampos = function () { return ", UD.regiao"; };
          }


        var iit = "";
        var tlv = "";
        var mtlv =  "+ ISNULL(QtdTransfTLV, 0)";
        var tempoIIT = "";
        var tempoTLV = "";
        if($scope.iit === false) {
          iit =  "- ISNULL(QtdTransfUra, 0)";
          tempoIIT = "- ISNULL(TempoTransfURA, 0)";
        }
        if($scope.tlv === false){
          tlv =  "- ISNULL(QtdTransfTLV, 0)";
          mtlv = "";
          tempoTLV = "- ISNULL(TempoTransfTLV, 0)";
        }

        var tempoEncerradas = "TempoAbandonadas+TempoFinalizadas+TempoDerivadas+TempoTransfURA+TempoTransfTLV";


        if ($scope.filtros.porDia === false) {


          if($scope.filtros.isHFiltroIC){
        //XDXD

            stmt = db.use
            + " SELECT RE.DatReferencia as DatReferencia"+incluiCampos()
            + ", ISNULL(sum(QtdChamadas"+iit+tlv+"),0) as c, "           
            + " ISNULL(sum(QtdFinalizadas),0) as f, "
            + " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, "
            + " ISNULL(sum(QtdAbandonadas),0) as a, "
            + " ISNULL(sum(TempoEncerradas"+tempoIIT+tempoTLV+"),0) as tempo "
            + " FROM " + db.prefixo + "ResumoIC as RE "
            + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
            + " WHERE 1 = 1 "
            + " AND re.DatReferencia >= '" + formataDataHora(data_ini) + "' "
            + " AND re.DatReferencia <= '" + formataDataHora(data_fim) + "' "
            + restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true)
            + restringe_consulta("re.CodSite", filtro_sites, true)
            + restringe_consulta("re.CodSegmento", filtro_segmentos, true)
            stmt += restringe_consulta4("RE.CodIC", filtro_ic, $scope.oueIC)
            stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
            + " Group by re.DatReferencia"+incluiCampos()
            + " ORDER BY re.DatReferencia"+incluiCampos();

          }else if($scope.filtros.isHFiltroED){


            stmt = db.use
            + " SELECT RE.DAT_REFERENCIA as DatReferencia"+incluiCampos()
            + ", ISNULL(sum(QtdChamadas"+iit+tlv+"),0) as c, "
            + " ISNULL(sum(QtdFinalizadas),0) as f, "
            + " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, "
            + " ISNULL(sum(QtdAbandonadas),0) as a, "
            + " ISNULL(sum(TempoEncerradas"+tempoIIT+tempoTLV+"),0) as tempo "
            + " FROM " + db.prefixo + "ResumoDDDxED as RE "
            + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
            + " WHERE 1 = 1 "
            + " AND re.Dat_Referencia >= '" + formataDataHora(data_ini) + "' "
            + " AND re.Dat_Referencia <= '" + formataDataHora(data_fim) + "' "
            + restringe_consulta("re.Cod_Aplicacao", filtro_aplicacoes, true)
            + restringe_consulta("re.Cod_Site", filtro_sites, true)
            + restringe_consulta2("re.Cod_Segmento", filtro_segmentos, true)
            stmt += restringe_consulta4("RE.Cod_estado_dialogo", filtro_ed, $scope.oueED)
            stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
            + " Group by re.Dat_Referencia"+incluiCampos()
            + " ORDER BY re.Dat_Referencia"+incluiCampos();



          }else{
                 if($scope.umDigito){
                    tabelaResumoFinal= "resumoFinalTel1";
                }else{
                    tabelaResumoFinal= "resumoFinalTel2";  
                }
                var stmt = db.use + "SELECT "
                    + " DatReferencia as DatReferencia"+incluiCampos()
                    + " ,sum(QtdDerivadas"+mtlv+") as d,"
                    + " sum(QtdAbandonadas) as a,"
                    + " sum(QtdFinalizadas) as f,"
                    + " sum(QtdChamadas"+iit+tlv+") as c,"                  
                    + " SUM(duracao "+tempoIIT+tempoTLV+") as tempo"
                    + " From " + db.prefixo + ""+tabelaResumoFinal+" as RE"
                    //+ " From " + db.prefixo + "resumoAnalitico2 as RE"
                    + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
                    + " WHERE 1 = 1"
                    + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
                    + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
					stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
					stmt += restringe_consulta("CodSite", filtro_sites, true)
					stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
					stmt += restringe_consulta("UD.UF", filtro_ufs, true)
					stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
          if($scope.umDigito){
            stmt += restringe_consulta("FinalTel", filtro_ud, true)
          }else{
            stmt += restringe_consulta("FinalTel", filtro_udd, true)
          }
					stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)

					stmt += " group By DatReferencia"+incluiCampos()
					stmt += " ORDER BY DatReferencia"+incluiCampos();


          }


          if (original_ddds.length !== 0 || filtro_regioes.length === 0) {
            $scope.porRegiao = "DDD";
          } else {
            $scope.porRegiao = "Região";
          }
          executaQuery = executaQuery1;


            } else if ($scope.filtros.porDia === true) {

            if($scope.filtros.isHFiltroIC){

              stmt = db.use
            + " SELECT CONVERT(DATE,RE.DatReferencia) as DatReferencia"+incluiCampos()
            + " ,ISNULL(sum(QtdChamadas"+iit+tlv+"),0) as c, "
            + " ISNULL(sum(QtdFinalizadas),0) as f, "
            + " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, "
            + " ISNULL(sum(QtdAbandonadas),0) as a, "
            + " ISNULL(sum(TempoEncerradas"+tempoIIT+tempoTLV+"),0) as tempo "
            + " FROM " + db.prefixo + "ResumoICDIA as RE "
            + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
            + " WHERE 1 = 1 "
            + " AND CONVERT(DATE,re.DatReferencia) >= '" + formataDataHora(data_ini) + "' "
            + " AND CONVERT(DATE,re.DatReferencia) <= '" + formataDataHora(data_fim) + "' "
            + restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true)
            + restringe_consulta("re.CodSite", filtro_sites, true)
            + restringe_consulta("re.CodSegmento", filtro_segmentos, true)
            stmt += restringe_consulta4("RE.CodIC", filtro_ic, $scope.oueIC)
            stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
            + " Group by CONVERT(DATE,re.DatReferencia)"+incluiCampos()
            + " ORDER BY CONVERT(DATE,re.DatReferencia)"+incluiCampos();

            }else if($scope.filtros.isHFiltroED){

              stmt = db.use
            + " SELECT RE.DAT_REFERENCIA as DatReferencia"+incluiCampos()
            + " ,ISNULL(sum(QtdChamadas"+iit+tlv+"),0) as c, "
            + " ISNULL(sum(QtdFinalizadas),0) as f, "
            + " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, "
            + " ISNULL(sum(QtdAbandonadas),0) as a, "
            + " ISNULL(sum(TempoEncerradas"+tempoIIT+tempoTLV+"),0) as tempo "
            + " FROM " + db.prefixo + "ResumoDDDxEDDia as RE "
            + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
            + " WHERE 1 = 1 "
            + " AND re.Dat_Referencia >= '" + formataDataHora(data_ini) + "' "
            + " AND re.Dat_Referencia <= '" + formataDataHora(data_fim) + "' "
            + restringe_consulta("re.Cod_Aplicacao", filtro_aplicacoes, true)
            + restringe_consulta("re.Cod_Site", filtro_sites, true)
            + restringe_consulta("re.Cod_Segmento", filtro_segmentos, true)
            stmt += restringe_consulta4("RE.Cod_Estado_dialogo", filtro_ed, $scope.oueED)
            stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
            + " Group by re.Dat_Referencia"+incluiCampos()
            + " ORDER BY re.Dat_Referencia"+incluiCampos();

            }else{
                if($scope.umDigito){
                    tabelaResumoFinal= "resumoFinalTel1";
                }else{
                    tabelaResumoFinal= "resumoFinalTel2";  
                }
                
                var stmt = db.use + "SELECT "
                + " CONVERT(DATE,DatReferencia) as DatReferencia"+incluiCampos()
                + " ,sum(QtdDerivadas"+mtlv+") as d,"
                + " sum(QtdAbandonadas) as a,"
                + " sum(QtdFinalizadas) as f,"
                + " ISNULL(sum(QtdChamadas"+iit+tlv+"),0) as c, "
                + " SUM(duracao"+tempoIIT+tempoTLV+") as tempo"
                + " From " + db.prefixo + ""+tabelaResumoFinal+" as RE"
                //+ " From " + db.prefixo + "resumoAnalitico2 as RE"
                + " LEFT OUTER JOIN UF_DDD as UD on RE.DDD = UD.DDD"
                + " WHERE 1 = 1"
                + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
                + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
                stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
                stmt += restringe_consulta("CodSite", filtro_sites, true)
                stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
                stmt += restringe_consulta("UD.UF", filtro_ufs, true)
                stmt += restringe_consulta("RE.DDD", filtro_ddds, true)
                if($scope.umDigito){
                  stmt += restringe_consulta("FinalTel", filtro_ud, true)
                }else{
                  stmt += restringe_consulta("FinalTel", filtro_udd, true)
                }
                stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)

        stmt += " group By CONVERT(DATE,DatReferencia)"+incluiCampos()
        stmt += " ORDER BY CONVERT(DATE,DatReferencia)"+incluiCampos();
                }

            }

          if (original_ddds.length !== 0 || filtro_regioes.length === 0) {
            $scope.porRegiao = "DDD";
          } else {
            $scope.porRegiao = "Região";
          }
          executaQuery = executaQuery1;

        log(stmt);
        function executaQuery1(columns) {

            $scope.query = "";
                var
                    DatReferencia = typeof columns["DatReferencia"].value !== "string" ? formataDataHoraBR(columns["DatReferencia"].value) : formataDataHoraBRString(columns["DatReferencia"].value),
                    DDD =  columns["DDD"] !== undefined ? (isNaN(+columns["DDD"].value) ? 0 : +columns["DDD"].value) : '',
                    regiao = columns["regiao"] !== undefined ? (isNaN(+columns["regiao"].value) ? 0 : +columns["regiao"].value) : '',
                    QtdDerivadas = +columns["d"].value,
                    QtdAbandonadas = +columns["a"].value,
                    QtdFinalizadas = +columns["f"].value,
                    QtdChamadas =  +columns["c"].value,
                    tma = columns["tempo"] !== undefined ?
                    (+columns["tempo"].value / QtdChamadas).toFixed(2) :
                    undefined;


                $scope.dados.push({
                    DatReferencia: DatReferencia,
                    DDD: DDD,
                    regiao: regiao,
                    tma: tma,
                    QtdChamadas: QtdChamadas,
                    QtdDerivadas: QtdDerivadas,
                    QtdAbandonadas: QtdAbandonadas,
                    QtdFinalizadas: QtdFinalizadas

                });
                
                $scope.csv.push([
                    DatReferencia,
                    DDD,
                    QtdChamadas,
                    QtdDerivadas,
                    QtdAbandonadas,
                    QtdFinalizadas,
                    tma
                ]);

        }

            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length, $scope, "#pag-performance-dddud");
            if ($scope.dados.length % 1000 === 0) {
                if ($scope.filtros.porDia === true && $scope.dados.length > 0) {
                    //$('#pag-performance-dddud table').css({ 'margin-top': '100px', 'max-width': '1280px', 'margin-bottom': '100px', 'margin-left': 'auto', 'margin-right': 'auto' });
                    $('.ddd').hide();
                    $('.dia').show();
                } else if ($scope.filtros.porDia === false && $scope.dados.length > 0) {
                    $('.ddd').show();
                    $('.dia').hide();
                }
                $scope.$apply();
            }



            db.query(stmt, executaQuery, function (err, num_rows) {
				
				var complemento = "";
				
				if($scope.filtros.isHFiltroED) complemento +=" por ED";
				if($scope.filtros.isHFiltroIC) complemento +=" por IC";
				
                console.log("Executando query-> " + stmt + " " + num_rows);
                retornaStatusQuery(num_rows, $scope);
                $btn_gerar.button('reset');
                if (num_rows > 0) {
                    $btn_exportar.prop("disabled", false);
                    $btn_exportar_csv.prop("disabled", false);
                    $btn_exportar_dropdown.prop("disabled", false);
                }
                if ($scope.filtros.porDia === true && $scope.dados.length > 0) {
                    //$('#pag-performance-dddud table').css({ 'margin-top': '100px', 'max-width': '1280px', 'margin-bottom': '100px', 'margin-left': 'auto', 'margin-right': 'auto' });
                    $('.ddd').hide();
                    $('.dia').show();
                } else if ($scope.filtros.porDia === false && $scope.dados.length > 0) {
                    $('.ddd').show();
                    $('.dia').hide();
                }
				
				userLog(stmt, "Consulta "+complemento, 2, err);
            });


        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            console.log('passei');
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };

    // Exportar planilha XLSX
   $scope.exportaXLSX = function () {

   var $btn_exportar = $(this);
   $btn_exportar.button('loading');

   //15-02-2014 TEMPLATE

   var tName = "";

       if ($scope.porRegiao === "Região") {
           tName = "tPerformanceDDDUDDiaRegiao";
       } else {
           tName = "tPerformanceDDDUDDia";
       }


   //15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='"+tName+"'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = ''+tName+'.xlsx';

   var buffer = toBuffer(toArrayBuffer(arquivo));

   fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'p'+(tName.substring(2)).substring(0,tName.length)+'_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;
   $scope.dados.map(function (array) {
		 if(array.tma === "NaN") array.tma = "";		 
	   return array;
     });
     console.log($scope.dados);


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });
    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);
          }, function (err, num_rows) {
			  var complemento = "";			  
			  userLog(stmt, "Exportação "+complemento, 5, err);
            //?
          });
    };
	 //{ EDs Input Controle
  var modificaAlertaEDs = function (x) {
	if (x=="Elemento repetido ou número máximo de EDs acumulados") {
	  $('.notification .ng-binding').text(x).selectpicker('refresh');
	  setTimeout(function () {
		$('.notification .ng-binding').text("").selectpicker('refresh');
	  }, 3000);
	} else {
	  //$("#filtro-ed").attr("placeholder", x);
	}
  };
  var verificaRepetidoEDs = function (x, item) {
	console.log(x);
	if (x.length==3) return true;
	for (var z in x) {
	  try {
		if (x[z].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	  try {
		if (x[z+1].localeCompare(item) == 0) return true;
	  } catch (err) {
		console.log(err);
	  }
	}
	return false;
  };  
}
CtrlPerformanceDDDUD.$inject = ['$scope', '$globals'];
