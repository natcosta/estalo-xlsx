
//{21/04/2014 THREADS para o ESTALO. (NECESSÁRIO INCLUIR O executável do node na pasta raiz do estalo.)
var thread;

function executaThread(arquivo, dado, callback, tempo, periodo, dir, scop, extrator) {

	var cp = require('child_process');
	var path = require('path');
	var fs = require('fs');
	var baseFile = '' + arquivo + '.js';
	var file = "";
	var milis;
	tempo !== undefined ? milis = tempo : milis = new Date();
	dir !== undefined ? dir = tempDir() : dir = "";

	var nodePath = path.join(path.dirname(process.execPath), 'node');
	if (process.platform === 'win32') {
		nodePath += ".exe";
	}
	if (!fs.existsSync(nodePath)) {
		baixarNovaVersao(nodePath);
	} else {

		if (!fs.existsSync(baseFile)) {
			var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
				" WHERE NomeRelatorio='" + arquivo + "'";
			console.log(stmt);

			db.query(stmt, function (columns) {
				var dataAtualizacao = columns[0].value,
					nomeRelatorio = columns[1].value,
					arquivoB = columns[2].value;
				baseFile = '' + arquivo + '.js';
				var buffer = toBuffer(toArrayBuffer(arquivoB));
				fs.writeFileSync(baseFile, buffer, 'binary');
			}, function (err, num_rows) {
				if (fs.existsSync(baseFile)) {
					baseThread();
				}
			});
		} else {
			baseThread();
		}

		function baseThread() {
			file = dir + arquivo + '_' + formataDataHoraMilis(milis) + '.js';
			var data = fs.readFileSync("" + arquivo + ".js", "utf-8");
			var porta;
			config.server === "server01.versatec.com.br" ? porta = "\"\"" : porta = config.options.port;
			fs.writeFileSync(file, dado + 'var arrDatas = ["' + periodo[0] + '","' + periodo[1] + '","' + periodo[2] + '","' + periodo[3] + '","' + periodo[4] + '","' + periodo[5] + '","' + periodo[6] + '","' + periodo[7] + '","' + periodo[8] + '"]; var config = ["' + config.server + '","' + config.userName + '","' + config.password + '","' + config.database + '",' + porta + '];var baseFile ="' + baseFile + '"; var file ="' + file + '"; ' + data, executa());
		};


		function executa() {
			// Aqui abrimos um novo processo do node.exe, que chamaremos de thread.
			thread = cp.spawn(nodePath, [file]);
			thread.stdout.setEncoding('utf8');
			// callback do evento 'data' executará assim que o helper escrever alguma coisa com process.stdout.write()
			thread.stdout.on('data', function (data) {
				try {
					// aqui o callback é executado para tratar os dados que chegam da thread.
					callback && callback(data);
				} catch (e) {
					console.log(e + " " + e.message);
					
				}
			});

			thread.on('close', function (code) {
				console.log('closing code: ' + code);
				console.log(dado);


			});
		}
	}
}

//{ Thread usada somente no extrator geral
function executaThread2(datainicio, datafim, zeroMinSec, header, stmt_query, tipo, scop, extrator, extratorDatFim, numanis, porData, numero) {

	var milis = new Date();

	if (extratorDatFim != undefined) milis = new Date(extratorDatFim);

	var tempo = 0;
	var abrirArq = false;

	var periodo = [formataDataHora(datainicio), formataDataHora(datafim), extratorDatFim, tipo, zeroMinSec, scop.filtros_usados, numanis, porData, numero];

	scop.linkArq = tempDir3() + '' + (extrator !== undefined ? extrator : 'extratorDiaADia') + '_' + formataDataHoraMilis(milis) + '.txt';
	linkArq = tempDir3() + '' + (extrator !== undefined ? extrator : 'extratorDiaADia') + '_' + formataDataHoraMilis(milis) + '.txt';


	executaThread(extrator !== undefined ? extrator : 'extratorDiaADia', stringParaExtrator(stmt_query, header), function (dado) {
		console.log(dado);

	}, milis, periodo, undefined, scop, extrator);
}
//}

function executaThreadBasico(arquivo, dado, callback, scop, novonome) {

	var cp = require('child_process');
	var path = require('path');
	var fs = require('fs');
	var baseFile = '' + arquivo + '.js';
	var file = "";
	var milis = new Date();

	var nodePath = path.join(path.dirname(process.execPath), 'node');
	if (process.platform === 'win32') {
		nodePath += ".exe";
	}


	if (!fs.existsSync(baseFile)) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='" + arquivo + "Zip'";
		console.log(stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {			
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){					
				var dataAtualizacao = result[i][0].value,
				nomeRelatorio = result[i][1].value,
				arquivoB = result[i][2].value;		
				var baseFileZip = 'extratorDiaADia.js';
				var buffZip = toBuffer(toArrayBuffer(arquivoB));								
				  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
				  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + baseFileZip +"_temp")), function(err, buf) {      
				  if (err) console.log(err);
				  fs.writeFileSync(tempDir3() + baseFileZip, buf, 'binary');
					  try{
						fs.unlinkSync(tempDir3() + baseFileZip +"_temp");
					  }catch(ex){
					  }
					  baseThread();
				  });			
				}				
			}
		});		
	} else {
		baseThread();
	}


	function baseThread() {
		file = arquivo + '_' + formataDataHoraMilis(milis) + '.js';
		var data = fs.readFileSync("" + arquivo + ".js", "utf-8");
		var porta;
		config.server === "server01.versatec.com.br" ? porta = "\"\"" : porta = config.options.port;
		fs.writeFileSync(file, dado + 'var arrDatas = [];var config = ["' + config.server + '","' + config.userName + '","' + config.password + '","' + config.database + '",' + porta + '];var baseFile ="' + baseFile + '"; var file ="' + file + '"; ' + data, executa());
	};


	function executa() {
		// Aqui abrimos um novo processo do node.exe, que chamaremos de thread.
		thread = cp.spawn(nodePath, [file]);
		thrs.push(thread.pid);
		thread.stdout.setEncoding('utf8');
		// callback do evento 'data' executará assim que o helper escrever alguma coisa com process.stdout.write()
		thread.stdout.on('data', function (data) {
			try {
				// aqui o callback é executado para tratar os dados que chegam da thread.
				callback && callback(data);
			} catch (e) {
				console.log(e + " " + e.message);
			}
		});

		thread.on('close', function (code) {
			//console.log('closing code: ' + code + ' ' + formataDataHoraMilis(milis));
			//try{ console.log(dado.match(/(var query) = "(.+)"/)[2]); }catch(ex){}
			
			if (code === 1) {
				logCarregado = false;
			} else if (code === 0) {
				logCarregado = true;
			}

			if (scop === undefined) {
				if (fs.existsSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt')) {
					var valor = fs.readFileSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt', "utf-8");
					valor = valor.split('\t');


					if (novonome == undefined) {
						valor.forEach(function (v, index) {
							if (new Date(v).toString() !== "Invalid Date") {
								valor[index] !== "0" ? valor[index] = formataDataHoraBR2(new Date(v)) : valor = [null];

							}
						});
					}

					if (valor.indexOf(null) >= 0) {
						$('.alert-info span').text("A consulta não retornou nenhum resultado.");
					} else {
						if (novonome) {
							var temp = fs.readFileSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt', "utf-8");

							fs.appendFileSync(tempDir3() + novonome, temp);

							//fs.renameSync(tempDir3()+arquivo+'_'+formataDataHoraMilis(milis)+'.txt', novonome);
						} else {
							if (valor[0].trim() !== "") {
								if (valor.join(" ").match("TOOLH.EXE") !== null) {
									$('.alert-info span').text("Nova versão de homologação baixada");
								} else if (valor.join(" ").match("TOOLP.EXE") !== null) {
									$('.alert-info span').text("Nova versão de produção baixada");
								} else if (valor.join(" ").match("LOG_ORIGINAL_ZIPADO") !== null) {
									try{
										fs.unlinkSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt');
									}catch(ex){
									}
								} else if (valor.join(" ").match("AUDIO_PROMPT") !== null) {
									try{
										fs.unlinkSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt');
									}catch(ex){
									}
								} else {
									if(valor[0].match('<VDATA>')=== null) $('.alert-info span').text(valor.join(" "));
								}
							}
						}
					}
				} else {
					if (novonome) fs.writeFileSync(tempDir3() + novonome, "");
				}
			} else {
				if (fs.existsSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt')) {
					var valor = fs.readFileSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt', "utf-8");
					valor = valor.split("\t");
					scop.tempData = valor;
					fs.unlinkSync(tempDir3() + arquivo + '_' + formataDataHoraMilis(milis) + '.txt');
				}

			}

		});
	}

}
//}

function killThreads() {
	
	for (i = 0; i < thrs.length; i++) {
		require('child_process').exec('cmd /c TASKKILL /PID ' + thrs[i] + ' /F', function () {
			if (thrs[i] !== undefined) console.log(thrs[i] + ' processo finalizado');
		});
	}
	thrs = [];
}

//{02/05/2014 stringParaExtrator
function stringParaExtrator(query, headerTable) {
	var ht = "var headerTable = [\'" + headerTable.join("\',\'") + "\'];"
	return mod_dados = ht + 'var query = \"' + query + '\";';
}
//}

