function CtrlResumoTransferenciasTransferIDOper1($scope, $globals, $http) {
	var idView = "pag-resumo-transf-transferid-oper1";

	$scope.titulo = "Resumo de transferências por transferID e operação 1";
	win.title = $scope.titulo;

	$scope.nomeExportacao = "resumoTransfTransferidOper1";

	$scope.versao = versao;
	$scope.gruposRelatorios = gruposRelatorios;

	travaBotaoFiltro(0, $scope, "#" + idView, $scope.titulo);

	$scope.colunas = [];
	$scope.dados = [];
	$scope.csv = [];
	$scope.extra = [];

	$scope.gridDados = {
		data: $scope.dados,
		columnDefs: $scope.colunas,
		enableSorting: true,
		enablePinning: true,
		headerRowHeight: 100,
		exporterCsvFilename: 'myFile.csv',
		exporterCsvColumnSeparator: ';',
		exporterOlderExcelCompatibility: true,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect: true,
		enableRowSelection: true,
		enableFullRowSelection: true,
		multiSelect: true,
		modifierKeysToMultiSelect: true,
		exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		onRegisterApi: function (gridApi) {
			$scope.gridApi = gridApi;
		}
	};

	// Filtros
	$scope.filtros = {
		aplicacao: [],
		sites: [],
		segmento: [],
		transferid: [],
		porDia: false,
		porOper2: false
	};



	$scope.listarPorData = $scope.filtros.porDia;
	$scope.listarPorDestino = $scope.filtros.porOper2;

	$scope.gridOptions = {};

	$scope.gridOptions.columnDefs = [{
			name: 'datahora',
			width: 100,
			enablePinning: true
		},
		{
			name: 'datahoraBR',
			width: 100,
			enablePinning: true
		},
		{
			name: 'transferID',
			width: 200,
			enablePinning: true
		},
		{
			name: 'operacao1',
			width: 200,
			enablePinning: true
		},
		{
			name: 'qtdOper1',
			width: 130,
			enablePinning: true
		},
		{
			name: 'qtdOper2',
			width: 130,
			enablePinning: true
		},
		{
			name: 'percentTransf',
			width: 140,
			enablePinning: true
		}
	];

	$scope.gridOptions.data = $scope.dados;

	$scope.colunas1 = [{
			nome: "datahora",
			titulo: "Data",
			largura: 100,
			exibir: function () {}
		},
		{
			nome: "datahoraBR",
			titulo: "Data",
			largura: 100,
			exibir: function () {
				return $scope.listarPorData;
			},
			fixar: true
		},
		{
			nome: "transferID",
			titulo: "TransferID",
			largura: 200,
			fixar: true
		},
		{
			nome: "operacao1",
			titulo: "Operação 1",
			largura: 200,
			fixar: true
		},
		{
			nome: "qtdOper1",
			titulo: "Qtd Operação 1",
			largura: 130,
			numero: true,
			fixar: true
		},
		{
			nome: "qtdOper2",
			titulo: " Qtd Operação 2",
			largura: 130,
			numero: true,
			fixar: true
		},
		{
			nome: "percentTransf",
			titulo: "% Transferências",
			largura: 140,
			numero: true,
			fixar: true
		}
	];

	function geraColunas(colunas) {
		var numberTemplate = '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>';
		return colunas.map(function (col) {
			var o = {
				name: col.nome,
				displayName: col.titulo,
				width: col.largura,
				pinned: col.fixar !== undefined ? col.fixar : false,
				pinnable: col.fixavel !== undefined ? col.fixavel : true
			};
			if (col.numero) {
				o.cellTemplate = numberTemplate;
			}
			if (col.exibir !== undefined && !col.exibir()) {
				o.visible = false;
			}
			return o;
		});
	}

	function obtemLarguraColuna(titulo) {
		var largura = 0;
		//try {
		var elem = document.getElementById("abcdef");
		if (elem) {
			elem.remove();
		}
		document.body.innerHTML += '<span id="abcdef" style="position: absolute; visibility: hidden; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold;">' + titulo + '</span>';
		elem = document.getElementById("abcdef");
		largura = elem.getBoundingClientRect().width + 25;
		elem.remove();
		//} catch (ex) {}
		return largura;
	}

	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();
	var agora = new Date();
	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: ontem(dat_consoli),
		max: dat_consoli*/
	};



	var $view;

	$scope.$on('$viewContentLoaded', function () {
		$view = $("#" + idView);

		$(".aba2").css({
			'position': 'fixed',
			'left': '55px',
			'top': '40px'
		});

		// Exibir notificação
		$view.find('div.logoCliente').mouseover(function () {
			$view.find('div.notification').fadeIn(500);
		});

		// Ocultar a notificação
		$view.find('button.close').click(function () {
			$view.find('div.notification').fadeOut(500);
		});

		function exibeCalendario(qual) {
			if (qual === 'inicio') {
				$view.find("div.datahora-inicio").data("datetimepicker").show();
				$view.find("div.datahora-fim").data("datetimepicker").hide();
			} else if (qual === 'fim') {
				$view.find("div.datahora-inicio").data("datetimepicker").hide();
				$view.find("div.datahora-fim").data("datetimepicker").show();
			} else {
				$view.find("div.datahora-inicio").data("datetimepicker").hide();
				$view.find("div.datahora-fim").data("datetimepicker").hide();
			}
		}

		$view.find('.menu-relatorios > .dropdown-toggle').dropdownHover().dropdown();

		$view.find('.menu-relatorios > .dropdown-toggle').mouseover(function () {
			exibeCalendario(false);
		});

		$view.find('div.datahora-inicio span.add-on').mouseover(function () {
			exibeCalendario('inicio');
		});

		$view.find('div.datahora-fim span.add-on').mouseover(function () {
			exibeCalendario('fim');
		});

		$view.find("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
			$(this).hide();
		});

		$view.find(".topLine").mouseover(function () {
			exibeCalendario(false);
		});
		$view.find(".bottomLine").mouseover(function () {
			exibeCalendario(false);
		});

		componenteDataMaisHora($scope, $view);
		carregaAplicacoes($view, false, false, $scope);
		carregaSites($view);
		carregaSegmentosPorAplicacao($scope, $view, true);
		carregaRegioes($view);
		carregaDDDs($view);
		carregaEmpresas($view);
		carregaReconhecimentos($view);

		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			var valores = $(this).val() || [];
			$scope.filtros.aplicacao = valores;
			carregaSegmentosPorAplicacao($scope, $view);
		});

		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});


		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});

		$view.find("select.filtro-regiao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} regiões',
			showSubtext: true
		});

		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-resultado").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Resultados',
			showSubtext: true
		});


		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-segmento", function () {
			var valores = $(this).val() || [];
			$scope.filtros.segmento = valores;
			Estalo.filtros.filtro_segmentos = valores;
		});
		$view.on("change", "select.filtro-regiao", function () {
			carregaDDDsPorRegiao($scope, $view);
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});

		//2014-11-27 transição de abas
		var abas = [2, 3, 4, 5, 6, 7];

		$view.on("click", "#alinkAnt", function () {

			if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
			if ($scope.aba > abas[0]) {
				$scope.aba--;
				mudancaDeAba();
			}
		});

		$view.on("click", "#alinkPro", function () {

			if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

			if ($scope.aba < abas[abas.length - 1]) {
				$scope.aba++;
				mudancaDeAba();
			}

		});

		function mudancaDeAba() {
			abas.forEach(function (a) {
				if ($scope.aba === a) {
					$('.nav.aba' + a + '').fadeIn(500);
				} else {
					$('.nav.aba' + a + '').css('display', 'none');
				}
			});
		}

		// Marcar todas aplicações
		$view.on("click", ".alinkApl", function () {
			marcaTodasAplicacoes($view.find('.filtro-aplicacao'), $view, $scope);
		});

		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($view.find('.filtro-site'), 'sites');
		});

		// Marcar todos os segmentos
		$view.on("click", ".alinkSeg", function () {
			marcaTodosIndependente($view.find('.filtro-segmento'), 'segmentos');
		});

		// Marcar todas as empresas
		$view.on("click", ".alinkEmp", function () {
			marcaTodosIndependente($view.find('.filtro-empresa'), 'empresa');
		});
		// Marcar todas os resultados
		$view.on("click", "#alinkRes", function () {
			marcaTodosIndependente($view.find('.filtro-resultado'), 'reconhecimento');
		});

		// Marcar todos os transferIDs
		$view.on("click", ".alinkTid", function () {
			marcaTodosIndependente($view.find('.filtro-transferid'), 'transferid');
		});
		// Marca todos os DDDs
		$view.on("click", "#alinkDDD", function () {
			marcaTodosIndependente($('.filtro-ddd'), 'ddds')
		});

		// Marca todos os regiões
		$view.on("click", "#alinkReg", function () {
			$view.on("click", "#alinkReg", function () {
				marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
			});


		});
		$view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
			exibeCalendario(false);
			$(this).addClass('open');
			$(this).find('div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '350px'
			});
		});

		$view.find('div.btn-group.filtro-aplicacao').mouseout(function () {
			$(this).removeClass('open');
		});

		/*$view.find('div.btn-group.filtro-segmento:not-contains[.btn.disabled]').mouseover(function () {
		  exibeCalendario(false);
		  $(this).addClass('open');
		  // div.btn-group.filtro-segmento.open > div > ul { max-height: 500px; overflow-y: auto; min-height: 1px; max-width: 350px; }
		});*/

		// EXIBIR AO PASSAR O MOUSE
		// $('div>div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
		// Não mostrar filtros desabilitados
		// if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
		// $('div.btn-group.filtro-segmento').addClass('open');
		// $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		// }
		// });

		// EXIBIR AO PASSAR O MOUSE
		$view.find('div.btn-group.filtro-segmento').mouseover(function () {
			// Não mostrar filtros desabilitados
			if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
				$('div.btn-group.filtro-segmento').addClass('open');
				$('div.btn-group.filtro-segmento>div>ul').css({
					'max-height': '500px',
					'overflow-y': 'auto',
					'min-height': '1px',
					'max-width': '350px'
				});
			}
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
			$('div.btn-group.filtro-regiao').addClass('open');
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
			$('div.btn-group.filtro-regiao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
			$('div.btn-group.filtro-site').addClass('open');
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
			$('div.btn-group.filtro-site').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
			$('div.btn-group.filtro-ddd').addClass('open');
			$('div.btn-group.filtro-ddd>div>ul').css({
				'max-height': '600px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '400px'
			});
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
			$('div.btn-group.filtro-ddd').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-resultado').mouseover(function () {
			$('div.btn-group.filtro-resultado').addClass('open');
			$('div.btn-group.filtro-resultado>div>ul').css({
				'max-height': '600px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '400px'
			});
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-resultado').mouseout(function () {
			$('div.btn-group.filtro-resultado').removeClass('open');
		});


		// OCULTAR AO TIRAR O MOUSE
		$view.find('div.btn-group.filtro-segmento').mouseout(function () {
			$(this).removeClass('open');
		});

		$view.find('div.btn-group.filtro-transferid').mouseover(function () {
			exibeCalendario(false);
			$(this).addClass('open');
			$(this).find('div>ul').css({
				'max-height': '500px',
				'overflow-y': 'auto',
				'min-height': '1px',
				'max-width': '350px'
			});
		});

		$view.find('div.btn-group.filtro-transferid').mouseout(function () {
			$(this).removeClass('open');
		});

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.porDia = false;
			$scope.limpaFiltros();
		});

		$scope.limpaFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
			}, 500);
		}

		// Botão abortar
		$view.on("click", ".abortar", function () {
			$scope.abortar();
		});

		$scope.abortar = function () {
			abortar($scope, "#" + idView);
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {
			limpaProgressBar($scope, "#" + idView);

			if (!validaFiltros()) {
				return;
			}

			$scope.listaDados.apply(this);
		});

		//$view.on("double-click", ".grid-dados .ngRow", function () {});

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});

	function validaFiltros() {
		// Testa se data início é maior que a data fim
		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;
		if (formataData(data_ini) > formataData(data_fim)) {
			setTimeout(function () {
				atualizaInfo($scope, "A data inicial é maior que a data final.");
				effectNotification();
				$view.find(".btn-gerar").button('reset');
			}, 500);
			return false;
		}
		return true;
	}

	// Lista dados conforme filtros
	$scope.listaDados = function () {
		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacao = garanteVetor($view.find("select.filtro-aplicacao").val());
		var filtro_sites = garanteVetor($view.find("select.filtro-site").val());
		var filtro_segmento = garanteVetor($view.find("select.filtro-segmento").val());
		var filtro_empresa = garanteVetor($view.find("select.filtro-empresa").val());
		var filtro_transferid = garanteVetor($view.find("select.filtro-transferid").val());
		var filtro_ddd = garanteVetor($view.find("select.filtro-ddd").val());
		var filtro_regiao = garanteVetor($view.find("select.filtro-regiao").val());
		var filtro_resultado = garanteVetor($view.find("select.filtro-resultado").val());

		// Notificação de filtros usados
		$scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + " até " + formataDataBR($scope.periodo.fim) +
			(filtro_aplicacao.length > 0 ? " Aplicações: " + filtro_aplicacao : "") +
			(filtro_sites.length > 0 ? " Sites: " + filtro_sites : "") +
			(filtro_segmento.length > 0 ? " Segmentos: " + filtro_segmento : "") +
			(filtro_transferid.length > 0 ? " TransferIDs: " + filtro_transferid : "") +
			(filtro_regiao.length > 0 ? " Regiões: " + filtro_regiao : "") +
			(filtro_ddd.length > 0 ? " DDDs: " + filtro_ddd : "") +
			(filtro_resultado.length > 0 ? " Resultados: " + filtro_resultado : "");

		$scope.dados = [];
		$scope.pivot = [];
		$scope.pivot._colunas = {};
		$scope.pivot._totais = {};

		$scope.listarPorData = $scope.filtros.porDia;
		$scope.listarPorDestino = $scope.filtros.porOper2;

		$scope.colunas = geraColunas($scope.colunas1);

		function _join(values, sep) {
			var list = [];
			for (var i = 0; i < 3; i++) {
				if (values[i]) {
					list.push(values[i]);
				}
			}
			return list.join(sep);
		}



		if (filtro_regiao.length > 0 || filtro_ddd.length > 0 || filtro_resultado.length > 0) {

			var tabela = $scope.listarPorData ? "ResumoECHReconhecimentoVozDia" : "ResumoECHReconhecimentoVoz";
			var ddds = [];
			if (filtro_ddd.length == 0) {
				filtro_regiao.forEach(function (codigo) {
					ddds = ddds.concat(unique2(cache.ddds.map(function (ddd) {
						if (ddd.regiao === codigo) {
							return ddd.codigo
						}
					})) || []);
				});
			} else {
				ddds = filtro_ddd;
			}

			//var colunaDataHora = $scope.listarPorData ? "CONVERT(varchar, DatReferencia, 3)" : "LEFT(CONVERT(varchar, DatReferencia, 120), 16)";
			var chave = _join([
				$scope.listarPorData ? "LEFT(CONVERT(varchar,DatReferencia,120),10) AS DatReferencia, CONVERT(varchar,DatReferencia,3) AS DatReferenciaBR" : "",
				"TransferID, CodOperacao1",
				$scope.listarPorDestino ? "CodOperacao2" : ""
			], ", ");
			var ordem = _join([
				$scope.listarPorData ? "DatReferencia" : "",
				"TransferID, CodOperacao1",
				$scope.listarPorDestino ? "CodOperacao2" : ""
			], ", ");
			var stmt = db.use +
				" SELECT  " + chave + "," +
				"   SUM(QtdDerivadas) AS QtdDerivadas, SUM(QtdTransferidas) AS QtdTransferidas" +
				" FROM " + tabela +
				" WHERE DatReferencia >= '" + formataData(data_ini) + "'" +
				"   AND DatReferencia <= '" + formataData(data_fim) + "'" +
				"   AND CodAplicacao <> ''" +
				restringe_consulta("CodAplicacao", filtro_aplicacao, true) +
				restringe_consulta("CodSite", filtro_sites, true) +
				restringe_consulta("CodSegmento", filtro_segmento, true) +
				restringe_consulta("CodEmpresa", filtro_empresa, true) +
				restringe_consulta("TransferID", filtro_transferid, true) +
				restringe_consulta("DDD", ddds, true) +
				restringe_consulta("ResultadoRecVoz", filtro_resultado, true) +
				" GROUP BY " + ordem +
				" ORDER BY " + ordem;
		} else {
			//var colunaDataHora = $scope.listarPorData ? "CONVERT(varchar, DatReferencia, 3)" : "LEFT(CONVERT(varchar, DatReferencia, 120), 16)";
			var tabela = "ResumoECHTransferIDDia";
			var chave = _join([
				$scope.listarPorData ? "LEFT(CONVERT(varchar,DatReferencia,120),10) AS DatReferencia, CONVERT(varchar,DatReferencia,3) AS DatReferenciaBR" : "",
				"TransferID, CodOperacao1",
				$scope.listarPorDestino ? "CodOperacao2" : ""
			], ", ");
			var ordem = _join([
				$scope.listarPorData ? "DatReferencia" : "",
				"TransferID, CodOperacao1",
				$scope.listarPorDestino ? "CodOperacao2" : ""
			], ", ");

			var stmt = db.use +
				" SELECT  " + chave + "," +
				"   SUM(QtdDerivadas) AS QtdDerivadas, SUM(QtdTransferidas) AS QtdTransferidas" +
				" FROM " + tabela +
				" WHERE DatReferencia >= '" + formataData(data_ini) + "'" +
				"   AND DatReferencia <= '" + formataData(data_fim) + "'" +
				"   AND CodAplicacao <> ''" +
				restringe_consulta("CodAplicacao", filtro_aplicacao, true) +
				restringe_consulta("CodSegmento", filtro_segmento, true) +
				restringe_consulta("CodSite", filtro_sites, true) +
				restringe_consulta("CodEmpresa", filtro_empresa, true) +
				restringe_consulta("TransferID", filtro_transferid, true) +
				" GROUP BY " + ordem +
				" ORDER BY " + ordem;
		}

		log(stmt);

		function processaRegistro(columns) {
			var i = 0;
			var datahora, datahoraBR, transferID, operacao1, operacao2;
			if ($scope.listarPorData) {
				datahora = columns[i].value;
				i += 1;
				datahoraBR = columns[i].value;
				i += 1;
			}
			transferID = columns[i].value;
			i += 1;
			operacao1 = columns[i].value;
			i += 1;
			if ($scope.listarPorDestino) {
				operacao2 = columns[i].value;
				i += 1;
			}
			var qtdOper1 = +columns[i].value;
			i += 1;
			//if ($scope.listarPorDestino)
			var qtdOper2 = +columns[i].value;
			i += 1;

			var d = {
				datahora: datahora || " ",
				datahoraBR: datahoraBR || " ",
				transferID: transferID || " ",
				operacao1: operacao1 || "",
				operacao2: operacao2 || "",
				qtdOper1: qtdOper1,
				qtdOper2: qtdOper2,
				percentTransf: (100.0 * qtdOper2 / (qtdOper1 == 0 ? 1 : qtdOper1)).toFixed(2)
			};

			if ($scope.listarPorDestino) {
				var id = d.datahora + "/" + d.transferID + "/" + d.operacao1;
				var _d = $scope.pivot[id];
				if (_d === undefined) {
					_d = {
						datahora: d.datahora,
						datahoraBR: d.datahoraBR,
						transferID: d.transferID,
						operacao1: d.operacao1,
						qtdOper1: 0,
						qtdOper2: 0
					};
					$scope.pivot.push(_d);
					$scope.pivot[id] = _d;
				}
				_d.qtdOper1 += d.qtdOper1;
				_d.qtdOper2 += d.qtdOper2;
				if (d.operacao2 !== "") {
					_d["qtdOper2_" + d.operacao2] = d.qtdOper2;
					$scope.pivot._colunas[d.operacao2] = true;
					$scope.pivot._totais[d.operacao2] = ($scope.pivot._totais[d.operacao2] || 0) + d.qtdOper2;
				}
			} else {
				$scope.dados.push(d);
				var a = [];
				for (var item in d) a.push(d[item]);
				$scope.csv.push(a);
			}
		}

		db.query(stmt, processaRegistro, function (err, numRows) {
			log(numRows + " registros recebidos");
			userLog(stmt, 'Processa registros', 2, err)

			if ($scope.listarPorDestino) {
				$scope.pivot.forEach(function (d) {
					Object.keys($scope.pivot._colunas).forEach(function (s) {
						d["qtdOper2_" + s] = d["qtdOper2_" + s] || 0;
					});
					d.percentTransf = (100.0 * d.qtdOper2 / d.qtdOper1).toFixed(2);
				});
				$scope.dados = $scope.pivot;

				var extra = Object.keys($scope.pivot._colunas).sort(function (a, b) {
					return $scope.pivot._totais[b] - $scope.pivot._totais[a];
				});
				$scope.colunas = geraColunas($scope.colunas1.concat(extra.map(function (s) {
					return {
						nome: "qtdOper2_" + s,
						titulo: s,
						largura: 160 /* obtemLarguraColuna(s) */ ,
						numero: true,
						fixavel: false
					};
				})));

				$scope.extra = extra;
			}

			$view.find('table').css({
				'margin-left': 'auto',
				'margin-right': 'auto',
				'margin-top': '100px',
				'margin-bottom': '100px',
				'max-width': '1280px'
			});

			retornaStatusQuery($scope.dados.length, $scope, "");
			$btn_gerar.button('reset');
			if (numRows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportar_csv.prop("disabled", false);
				$btn_exportar_dropdown.prop("disabled", false);
			}

			$scope.gridDados.columnDefs = [];
			$scope.gridDados.data = $scope.dados;
			$scope.gridDados.columnDefs = $scope.colunas;
			$scope.$apply();
		});
	};

	$scope.reordenaColunas = function (d) {
		var extra = Object.keys($scope.pivot._colunas).sort(function (a, b) {
			return d["qtdOper2_" + b] - d["qtdOper2_" + a];
		});
		$scope.colunas = geraColunas($scope.colunas1.concat(extra.map(function (s) {
			return {
				nome: "qtdOper2_" + s,
				titulo: s,
				largura: 70 /* obtemLarguraColuna(s) */ ,
				numero: true
			};
		})));

		$scope.gridDados.columnDefs = [];
		$scope.gridDados.data = $scope.dados;
		$scope.gridDados.columnDefs = $scope.colunas;
		$scope.$apply();
	};

	// Exporta planilha XLSX
	$scope.exportaXLSX = function () {
		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

		var linhas = [];
		linhas.push($scope.colunas.filterMap(function (col) {
			if (col.visible || col.visible === undefined) {
				return col.displayName;
			}
		}));
		$scope.dados.forEach(function (dado) {
			linhas.push($scope.colunas.filterMap(function (col) {
				if (col.visible || col.visible === undefined) {
					return dado[col.name];
				}
			}));
		});

		exportaXLSX($scope.titulo, linhas, $scope.nomeExportacao, $scope.username);

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);
	};
}
CtrlResumoTransferenciasTransferIDOper1.$inject = ['$scope', '$globals'];