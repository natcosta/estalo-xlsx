function CtrlLogsEmAtraso($scope, $globals) {

  win.title = "Logs em Atraso";
  $scope.versao = versao;
  $scope.rotas = rotas;

  $scope.localeGridText = {
      contains: 'Contém',
      notContains: 'Não contém',
      equals: 'Igual',
      notEquals: 'Diferente',
      startsWith: 'Começa com',
      endsWith: 'Termina com'
  }

  var agora = new Date();

  var inicio = Estalo.filtros.filtro_data_hora[0] !== undefined ? Estalo.filtros.filtro_data_hora[0] : hoje();
  var fim = Estalo.filtros.filtro_data_hora[1] !== undefined ? Estalo.filtros.filtro_data_hora[1] : agora;

  $scope.periodo = {
      inicio: inicio,
      fim: fim,
      min: new Date(2013, 11, 1),
      max: agora // FIXME: atualizar ao virar o dia
  };

  $scope.linhaAtual = {}

  $scope.gridDados = {
      rowSelection: 'single',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      onGridReady: function (params) {
          params.api.sizeColumnsToFit();
      },
      overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
      overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado foi encontrado.</span>',
      columnDefs: [{
              headerName: 'Aplicação',
              field: 'codAplicacao'
          },
          {
              headerName: 'Dia',
              field: 'dia'
          },
          {
              headerName: 'Hora',
              field: 'hora'
          }, {
              headerName: 'Logs',
              field: 'logs'
          },
          {
              headerName: 'Minutos Médios',
              field: 'minutosMedios'
          },
          {
              headerName: 'Máximos',
              field: 'maximos'
          },
          {
              headerName: 'Horas Médias',
              field: 'horasMedias'
          }
      ],
      rowData: [],
      localeText: $scope.localeGridText
  };

  $scope.filtros = {
      consolFinalizados: false,
      diferencaDia: false
  }

  $scope.btnCriarInstancia = '';

  $scope.permCriarInstancia = false;
  var p = cache.aclbotoes.map(function (b) {
      return b.idview;
  }).indexOf('pag-logs-em-atraso');
  if (p >= 0) {
      var teste = cache.aclbotoes[p].permissoes.split(',');
      for (var i = 0; i < teste.length; i++) {
          if (teste[i] === 'instancia-consol') {
              $scope.permCriarInstancia = true;
          }
      }
  }

  var $view;

  $scope.dados = [];
  $scope.colunas = [];
  $scope.csv = [];
  $scope.difPorAplicacao = [];
  $scope.difAplicacoes = [];

  // Gerando colunas para exportação .csv
  for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
    $scope.colunas.push({
        displayName: $scope.gridDados.columnDefs[i].headerName
    });
  }

  $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-logs-em-atraso");

      $scope.gridDados.api.hideOverlay();

      $(".botoes").css({
          'position': 'fixed',
          'left': 'auto',
          'right': '25px',
          'margin-top': '35px'
      });

      console.log('DATES', $scope.periodo.inicio, $scope.periodo.fim, moment($scope.periodo.inicio).diff(moment($scope.periodo.fim), 'days'));

      componenteDataHora($scope, $view);
      carregaAplicacoes($view, false, false, $scope);

      $view.on("click", ".btn-gerar", function () {
          limpaProgressBar($scope, "#pag-logs-em-atraso");

          $scope.gridDados.rowData = [];
          $scope.dados = [];

          $scope.gridDados.api.showLoadingOverlay();

          /*

          if (moment($scope.periodo.inicio).diff(moment($scope.periodo.fim), 'days') < -7) {
              setTimeout(function () {
                  atualizaInfo($scope, 'Diferença de data não pode ser maior que 7 dias');
                  effectNotification();
                  $view.find(".btn-gerar").button('reset');
              }, 500);

              $scope.gridDados.api.hideOverlay();

              return;
          } */

          var consolFinalizados = [];
          var stmtConsolFinalizados = "SELECT ID FROM ControleExtratores WHERE DataIni = DataFim AND (ID LIKE 'TNLConsolidador%' OR ID LIKE 'TNLConsolAtrasadas%') "
          db.query(stmtConsolFinalizados, function (columns) {
              var numID = columns[0].value.replace(/\D+(\d+)/g, '$1');
              if (parseInt(numID) > 199) {
                  consolFinalizados.push(columns[0].value);
              }
          }, function (err, num_rows) {
              if (err) {
                  console.log('Erro ao buscar consolidadores finalizados. ' + err);
              } else {
                  console.log('Numero de consolidadores encontrados: ' + consolFinalizados.length);
                  if (consolFinalizados.length > 0) {
                      var stmtDelFinalizados = "DELETE FROM ControleExtratores WHERE DataINI = DataFIM AND ID IN (";
                      for (var i = 0; i < consolFinalizados.length; i++) {
                          stmtDelFinalizados = stmtDelFinalizados + "'" + consolFinalizados[i] + "',";
                      }

                      stmtDelFinalizados = stmtDelFinalizados.substr(0, stmtDelFinalizados.length - 1);
                      stmtDelFinalizados = stmtDelFinalizados.concat(')');
                      console.log('Query para DELETAR: ' + stmtDelFinalizados);
                      db.query(stmtDelFinalizados, function () {}, function (err, num_rows) {
                          if (err) {
                              console.log('Erro ao remover consolidadores finalizados');
                          } else {
                              console.log('Numero de consolidadores deletados: ' + num_rows);
                              // Testar se data início é maior que a data fim
                              var data_ini = $scope.periodo.inicio,
                                  data_fim = $scope.periodo.fim;
                              var testedata = testeDataMaisHora(data_ini, data_fim);
                              if (testedata !== "") {
                                  setTimeout(function () {
                                      atualizaInfo($scope, testedata);
                                      effectNotification();
                                      $view.find(".btn-gerar").button('reset');
                                  }, 500);
                                  return;
                              }

                              $scope.dados = [];
                              $scope.difPorAplicacao = [];
                              $scope.listaAtrasados.apply(this);
                          }
                      });
                  } else {
                      // Testar se data início é maior que a data fim
                      var data_ini = $scope.periodo.inicio,
                          data_fim = $scope.periodo.fim;
                      var testedata = testeDataMaisHora(data_ini, data_fim);
                      if (testedata !== "") {
                          setTimeout(function () {
                              atualizaInfo($scope, testedata);
                              effectNotification();
                              $view.find(".btn-gerar").button('reset');
                          }, 500);
                          return;
                      }

                      $scope.dados = [];
                      $scope.difPorAplicacao = [];
                      $scope.listaAtrasados.apply(this);
                  }

              }
          });
      });

      $view.on("click", ".btn-exportar", function () {
          $scope.exportaXLSX.apply(this);
      });

      // Botão abortar Alex 23/05/2014
      $view.on("click", ".abortar", function () {
          $scope.abortar.apply(this);
      });

      //Alex 23/05/2014
      $scope.abortar = function () {
          abortar($scope, "#pag-logs-em-atraso");
      }
  });

  $scope.listaAtrasados = function () {

      $scope.gridDados.rowData = [];
      $scope.gridDados.api.setRowData([]);

      $scope.gridDados.api.hideOverlay();
      $scope.gridDados.api.showLoadingOverlay();

      var $btn_exportar = $view.find(".btn-exportarCSV");
      $btn_exportar.prop("disabled", true);

      var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
      console.log("FiltroAplicacoes:" + filtro_aplicacoes);

      var $btn_gerar = $(this);
      $btn_gerar.button("loading");
      var data_ini = $scope.periodo.inicio,
          data_fim = $scope.periodo.fim;

      var data = $scope.periodo.inicio;

      var dataInicioZero = formataHoraZero(formataDataHora(data));
      var dataFimZeroMenosUm = formataHoraZeroMenosUm(formataDataHora(data))

      data.setDate(data.getDate() + 1);

      var dataFimMaisUmCinco = formataHoraCinco(formataDataHora(data))

      var stmt =
          "select CodAplicacao,datepart(day,DataHora_Inicio) as dia,datepart(hour,DataHora_Inicio) as hora,count(*) as Logs, " +
          "sum(datediff(minute, DataHora_Inicio, DataInsercao))/count(*) as MinutosMedios, " +
          "max(datediff(minute, DataHora_Inicio, DataInsercao)) as Maximos, ((sum(datediff(minute, DataHora_Inicio, DataInsercao))/count(*))/60) AS HorasMedias from IVRCDR " +
          "where DataHora_Inicio between '" + dataInicioZero + "' and '" + dataFimZeroMenosUm + "' and DataInsercao > '" + dataFimMaisUmCinco + "' " +
          "group by CodAplicacao,datepart(day,DataHora_Inicio),datepart(hour,DataHora_Inicio) " +
          "order by CodAplicacao,datepart(day,DataHora_Inicio),datepart(hour,DataHora_Inicio) "
      // var stmt = "uspVerificaConsolidacao2 \'" + formataDataHora($scope.periodo.inicio) + "','" + formataDataHora($scope.periodo.fim) + "'";
      db.query(stmt, function (columns) {
          var codAplicacao = columns[0].value,
              dia = columns[1].value,
              hora = columns[2].value,
              logs = columns[3].value,
              minutosMedios = columns[4].value,
              maximos = columns[5].value,
              horasMedias = columns[6].value

          $scope.dados.push({
              codAplicacao: codAplicacao,
              dia: dia,
              hora: hora,
              logs: logs,
              minutosMedios: minutosMedios,
              maximos: maximos,
              horasMedias: horasMedias
          });

          $scope.gridDados.rowData.push({
              codAplicacao: codAplicacao,
              dia: dia,
              hora: hora,
              logs: logs,
              minutosMedios: minutosMedios,
              maximos: maximos,
              horasMedias: horasMedias
          });

          $scope.csv.push([
              codAplicacao,
              dia,
              hora,
              logs,
              minutosMedios,
              maximos,
              horasMedias
          ]);
      }, function (err, num_rows) {

          if (err) {
              console.log('Erro ao buscar diferenças: ' + err);
          } else {

              $scope.gridDados.api.hideOverlay();
              retornaStatusQuery($scope.dados.length, $scope);

              if ($scope.gridDados.rowData.length < 1) {
                $scope.gridDados.api.showNoRowsOverlay();
              }

              $scope.gridDados.api.setRowData($scope.gridDados.rowData);
              $scope.gridDados.api.refreshCells({force: true});

              $btn_gerar.button("reset");
              if ($scope.dados.length > 0) {
                  $btn_exportar.prop("disabled", false);
              }
          }
      });
      console.log(stmt);
  }

  $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
          colunas.push($scope.gridDados.columnDefs[i].headerName);
      }

      // colunas.pop();

      console.log(colunas);

      var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [
          ['Filtros:' + $scope.filtros_usados]
      ].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosExcel);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'controleConsolidador_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);

      setTimeout(function () {
          $btn_exportar.button('reset');
      }, 500);
  };

  function reverteDataBR(dataBR) {
      return dataBR.replace(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}:\d{2})/, '$3-$2-$1 $4');
  }

  function formataHoraZero(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 00:00:00');
  }

  function formataHoraZeroMenosUm(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 23:59:59');
  }

  function formataHoraCinco(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 05:00:00');
  }

  function formataHoraCheia(dataHora) {
      return dataHora.replace(/(\d{4}-\d{2}-\d{2} \d{2}).+/g, '$1:00:00');
  }
}

CtrlLogsEmAtraso.$inject = ['$scope', '$globals'];
