//{ Diretivas de menu relatório / <li lista-relatorios></li>
Estalo.directive('listaRelatorios', function () {
    function link(scope, element, attrs) {
        scope.tradRel = ['Chamadas', 'Repetidas', 'Estados de Diálogo', 'Item de Controle', 'Transfer Id e VDN', 'Vendas', 'Falhas e Legados', 'Extratores', 'Parâmetros', 'Administrativo', 'Monitoração', 'Cradle to Grave', 'Cadastro e Migração', 'Ura Ativa'];
        var gruposRel = [];
        scope.rel = {};
        rotas.map(function (valor) {
            if (gruposRel.indexOf(valor.grupo) < 0) {
                gruposRel.push(valor.grupo);
            }
        });
        gruposRel.map(function (valor) {
            scope.rel[valor] = [];
        });
        gruposRel.map(function (valor) {
            rotas.map(function (val) {
                if (valor == val.grupo) {
                    scope.rel[valor].push(val);
                }
            });
        });
        $('.js-activated').mouseover(function () {
            $('#modalAlerta').modal('hide');
        });
        $('.js-activated').dropdownHover().dropdown();


        scope.abreAjuda = function (link) {
            //console.log("Abriu ajuda link : "+link);
            abreAjuda(link);
        }
        //console.log(scope.rel);
    }
    return {
        restrict: 'AEC',
        link: link,
        template: "  <div class='dropdown' style='float: none; padding: 10px; color: #777; text-decoration:none; cursor:pointer; '>" +
        "     <a class='dropdown-toggle js-activated' type='button' data-toggle='dropdown' style='text-decoration: none;'><span>" +
        "     <i class='icon-list-alt icon-white'></i></span>" +
        "     <span class='caret'></span></a> " +
        "     <ul class='dropdown-menu'> " +
        "       <li class='dropdown-submenu' ng-repeat='(x,y) in rel' ng-if='x!=\"Administrativo\" && x !=\"Parâmetros\" && x !=\"Suporte\"'> " +
        "         <a class='test' tabindex='-1' >{{x}} </a> " +
        "         <ul class='dropdown-menu'> " +
        "           <li ng-repeat='a in y' ng-if='!a.ocultar'><a tabindex='-1' href='#{{a.url}}'>{{a.nome}}</a></li> " +
        "         </ul> " +
        "       </li> " +
        "       <hr style='margin : 10px'> " +
        "       <li class='dropdown-submenu' ng-repeat='(x,y) in rel' ng-if='x==\"Administrativo\" || x ==\"Parâmetros\" || x ==\"Parâmetros\" || x ==\"Suporte\"'> " +
        "         <a class='test' tabindex='-1' href='#'>{{x}} </a> " +
        "         <ul class='dropdown-menu'> " +
        "           <li ng-repeat='a in y' ng-if='a.nome!=\"Ajuda\"'><a tabindex='-1' href='#{{a.url}}' >{{a.nome}}</a></li> " +
        "           <li ng-repeat='a in y' ng-if='a.nome==\"Ajuda\"'><a tabindex='-1' ng-click='abreAjuda(1)' >{{a.nome}}</a></li> " +
        "         </ul> " +
        "       </li> " +
        "     </ul> " +
        "   </div>",
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    //{PROJETO TABS
                    $('.nav-tabs a').mouseenter(function () {
                        //$(this).tab('show');
                    });
                    var tabResolucaoTemp = ((window.innerWidth / 1.65) / 5);
                    $(".nav-tabs li").css("width", tabResolucaoTemp);
                    $(".nav-tabs li:last-child a").tab('show');
                    $(".nav-tabs li:first-child a").tab('show');
                    $(".nav-tabs li a").hover(function () {
                        $("select").selectpicker('refresh');
                        //$(this).css("background-color","#e6e6e6;");
                    });
                    var tempReso = $(".row-fluid").outerHeight() + 10;
                    //$('#pag-chamadas .grid').css("margin-top",tempReso);
                    $(window).resize(function () {
                        var tempReso = $(".row-fluid").outerHeight() + 10;
                        //$('#pag-chamadas .grid').css("margin-top",tempReso);
                    });
                    //}
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroSegmento / <li filtro-segmento></li>
Estalo.directive('filtroSegmento', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkSeg" data-var="0" title="Clique para marcar todos" disabled=""><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-segmento selectpicker span1_3" multiple="" title="Segmentos" disabled=""> </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-segmento').mouseover(function () {
                        // Não mostrar filtros desabilitados
                        if ($('.filtro-segmento').attr('disabled') == undefined) {
                            $('.filtro-segmento').addClass('open');
                            $('.filtro-segmento>div>ul').css({
                                'max-height': '500px',
                                'overflow-y': 'auto',
                                'min-height': '1px',
                                'max-width': '350px'
                            });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-segmento').mouseout(function () {
                        $('.filtro-segmento').removeClass('open');
                    });

                });
            });
        }
    };
});
//}

//{ Diretivas de filtroCanalRecarga / <filtro-canal-recarga></filtro-canal-recarga>
Estalo.directive('filtroCanalRecarga', function () {
    return {
        restrict: 'AEC',
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkRecarga" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-recarga selectpicker span2" multiple="" title="Apl. Recarga"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-recarga').mouseover(function () {
                        // Não mostrar filtros desabilitados
                        if ($('.filtro-recarga').attr('disabled') == undefined) {
                            $('.filtro-recarga').addClass('open');
                            $('.filtro-recarga>div>ul').css({
                                'max-height': '500px',
                                'overflow-y': 'auto',
                                'min-height': '1px',
                                'max-width': '350px'
                            });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-recarga').mouseout(function () {
                        $('.filtro-recarga').removeClass('open');
                    });
                });
            });
        }
    };
});
//}


//{ Diretivas de filtroCanalRecargaM4U / <filtro-canal-recarga-m4u></filtro-canal-recarga-m4u>
Estalo.directive('filtroCanalRecargaM4u', function () {
    return {
        restrict: 'AEC',
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkRecargaM4U" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-canal-recarga-m4u selectpicker span1_3" multiple="" title="Canais"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) { 
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
					
					$("select.filtro-canal-recarga-m4u").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Canais',
                        showSubtext: true
                    });
					
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-canal-recarga-m4u').mouseover(function () {
                        // Não mostrar filtros desabilitados
                        if ($('.filtro-canal-recarga-m4u').attr('disabled') == undefined) {
                            $('.filtro-canal-recarga-m4u').addClass('open');
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-canal-recarga-m4u').mouseout(function () {
                        $('.filtro-canal-recarga-m4u').removeClass('open');
                    });
                });
            });
        }
    };
});
//}



//{ Diretivas de filtroChamadores / <filtro-chamadores></filtro-chamadores>
Estalo.directive('filtroChamadores', function () {
    return {
        restrict: 'AEC',
        template: '<div class="input-prepend">' +
        '<span class="add-on">' +
        '<b class="dropdown show-tick">' +
        '<!--<li class="dropdown">-->' +
        '<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">' +
        '<span><i class="icon-list-alt icon-white"></i></span>' +
        '<b class="caret"></b>' +
        '</a>' +
        '<ul class="dropdown-menu opcao" data-ng-model="opcaoMarcada">' +
        '<li><a ng-click="opcaoMarcada = \'Telefone Chamador\'">Telefone Chamador</a></li>' +
        '<li><a ng-click="opcaoMarcada = \'Telefone Tratado\'">Telefone Tratado</a></li>' +
        '<li><a ng-click="opcaoMarcada = \'CPF/CNPJ\'">CPF / CNPJ</a></li>' +
        '<li><a ng-click="opcaoMarcada = \'Contrato\'">Contrato</a></li>' +
        '<li><a ng-click="opcaoMarcada = \'Protocolo\'">Protocolo</a></li>' +
        '</ul>' +
        '</b>' +
        '</span>' +
        '<input type="tel" class="filtro-chamador form-control span2" placeholder="{{opcaoMarcada || \'Telefone Tratado\'}}" ng-model="filtros.chamador"/>' +
        '</div>'
    };
});
//}



//{ Diretivas de filtroCheckBoxDia / <li check-dia class="dirFiltro"></li>
Estalo.directive('checkDia', function () {
    return {
        restrict: 'AEC',
        template: '             <form class="navbar-form ng-pristine ng-valid">' +
        '      <div class="checkbox navbar-btn filtro-check">' +
        '        <input type="checkbox" ng-model="filtros.porDia" class="pordia ng-pristine ng-valid">Por dia' +
        '      </div>' +
        '     </form>'
    };
});
//}

// //{ Diretivas de filtroEmpresa / <li filtro-empresa class="dirFiltro"></li>
// Estalo.directive('filtroEmpresa', function() {
//   return {
//     restrict: 'AEC',
//     template:   '     <!-- Filtro: empresa --> '
// + '            <form class="navbar-form"> '
// + '                  <div class="input-append"> '
// + '                      <a class="btn btn-small checkAll2" id="alinkEmp" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> '
// + '                      <select class="filtro-empresa selectpicker span1_3" multiple="" title="Empresas" > </select> '
// + '                  </div> '
// + '             </form> '
//   };
// });
// //}


//{ Diretivas de filtroSite / <li filtro-site></li>
Estalo.directive('filtroSite', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkSite" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-site selectpicker span1_1" multiple="" title="Sites/POPs"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-site").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Sites/POPs',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-site').mouseover(function () {
                        $('div.btn-group.filtro-site').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-site').mouseout(function () {
                        $('div.btn-group.filtro-site').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroDdds / <li filtro-ddds class="dirFiltro"></li>
Estalo.directive('filtroDdds', function () {
    return {
        replace: true,
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="datahora-fim input-append date"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkDDD" data-var="0" title="Clique para marcar todos" disabled=""><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-ddd selectpicker span1_2" multiple="" title="DDDs" disabled=""></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-ddd').mouseover(function () {
                        $('div.btn-group.filtro-ddd').addClass('open');
                        $('div.btn-group.filtro-ddd>div>ul').position().left;
                        if ($('.datahora-fim')[1] != undefined) {
                            if ($('.datahora-fim')[1].offsetLeft + 252 > $('body').width()) {
                                $('div.btn-group.filtro-ddd>div').css({
                                    'left': '-150px',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-ddd>div>ul').css({
                                    'left': '-150px',
                                    'max-width': '252px'
                                })
                            } else {
                                $('div.btn-group.filtro-ddd>div').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-ddd>div>ul').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                            }
                        } else {
                            if ($('.datahora-fim').position().left + 252 > $('body').width()) {
                                $('div.btn-group.filtro-ddd>div').css({
                                    'left': '-150px',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-ddd>div>ul').css({
                                    'left': '-150px',
                                    'max-width': '252px'
                                })
                            } else {
                                $('div.btn-group.filtro-ddd>div').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-ddd>div>ul').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                            }
                        }

                        $('div.btn-group.filtro-ddd>div>ul').css({
                            'max-height': '450px',
                            'overflow-y': 'auto',
                            'min-height': '1px'
                        });
                        //$('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '450px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '250px' });

                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-ddd').mouseout(function () {
                        $('div.btn-group.filtro-ddd').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroInputTransferId / <filtro-input-transfer-id><filtro-input-transfer-id/>
Estalo.directive('filtroInputTransferId', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">' +
        '<span class="transferid" title="Insira um valor de TRANSFERID">TransferID: </span>' +
        '<div class="input-append"><input type="text" class="form-control" id="transferid" title="Insira um valor de TRANFERID"></div>' +
        '</form> '
    }
})
//}

//{ Diretivas de filtroTransferId / <filtro-transfer-id><filtro-transfer-id/>
Estalo.directive('filtroTransferId', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkTid" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-transferid selectpicker span1_3" multiple="" title="Transfer IDs" > </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-transferid").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} transferids'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-transferid').mouseover(function () {
                        if (!$('div.btn-group.filtro-transferid .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-transferid').addClass('open');
                            $('div.dropdown-menu.open').css({
                                'margin-left': '-30px'
                            });
                            $('div.btn-group.filtro-transferid>div>ul').css({
                                'max-height': '500px',
                                'overflow-y': 'auto',
                                'min-height': '1px',
                                'max-width': '350px'
                            });

                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-transferid').mouseout(function () {
                        $('div.btn-group.filtro-transferid').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de btnBotoes / <btn-botoes></btn-botoes>
Estalo.directive('btnBotoes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '           <form class="form-inline ng-pristine ng-valid" style="margin: 0px 0px 0px 0px;">  ' +
        '               <button class="btn btn-limpar-filtros" data-loading-text="..." title="Limpa todos os filtros selecionados"><i class="icon-trash"></i><br>Limpar</button>  ' +
        '               <button class="btn btn-gerar" data-loading-text="..."><i class="icon-ok"></i><br>Carregar</button>  ' +
        '               <button class="btn btn-exportar" data-loading-text="..." disabled="">  ' +
        '                  <i class="icon-download-alt"></i><br> Exportar</button>  ' +
        '           </form> '
    };
});
//}

//{ Diretivas de filtroRegioes / <li filtro-regioes></li>
Estalo.directive('filtroRegioes', function () {
    return {
        restrict: 'AEC',
        template: ' <!--form class="navbar-form"--> ' +
        ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkReg" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-regiao selectpicker span1_2" multiple="" title="Regiões"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-regiao").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} regiões',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-regiao').mouseover(function () {
                        $('div.btn-group.filtro-regiao').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-regiao').mouseout(function () {
                        $('div.btn-group.filtro-regiao').removeClass('open');
                    });
                });
            });
        }
    }
});
//}

//{ Diretivas de filtroIcsRepetidas / <li filtro-IcsRepetidas></li>
Estalo.directive('filtroIcsRepetidas', function () {
    return {
        restrict: 'AEC',
        template: ' <!--form class="navbar-form"--> ' +
        ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkReg" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-ics-repetidas selectpicker span1_2" multiple="" title="Ics"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-ics-repetidas").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Ics',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-ics-repetidas').mouseover(function () {
                        $('div.btn-group.filtro-ics-repetidas').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-ics-repetidas').mouseout(function () {
                        $('div.btn-group.filtro-ics-repetidas').removeClass('open');
                    });
                });
            });
        }
    }
});
//}

//{ Diretivas de dirNotification / <div dir-notification class="notification" ></div>
Estalo.directive('dirNotification', function () {
    return {
        restrict: 'AEC',
        template: ' <div class="alert alert-info"> ' +
        '   <button type="button" class="close">&times;</button> ' +
        '   <span ng-bind-html="info_status"></span> ' +
        ' </div> ' +
        ' <style> ' +
        '   .notification { ' +
        '     position:fixed; ' +
        '     bottom:0px; ' +
        '     width:97%; ' +
        '     margin-bottom: -18px; ' +
        '     z-index: 2; ' +
        '   } ' +
        ' </style> '
    };
});
//}

//{ Diretivas de filtroDataInicial / <filtro-data-inicial></filtro-data-inicial>
Estalo.directive('filtroDataInicial', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '      <li style="padding-left: 6px;"> ' +
        '              <form class="navbar-form">  ' +
        '                <div class="data-inicio input-append date">  ' +
        '                  <input type="text" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" />  ' +
        '                  <span class="add-on"><i data-date-icon="icon-calendar"></i></span>  ' +
        '                </div>  ' +
        '                <div class="hora-inicio input-append" style="margin-left:-3px;">  ' +
        '                  <input type="text" class="span1_4" data-format="hh:mm:ss" autocomplete="off"  />  ' +
        '                  <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>  ' +
        '                </div>  ' +
        '              </form>  ' +
        ' </li> '

    };
});
//}

//{ Diretivas de filtroDataFinal /   <filtro-data-final></filtro-data-final>
Estalo.directive('filtroDataFinal', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '      <li style="padding-left: 6px;"> ' +
        '    <form class="navbar-form"> ' +
        '               <div class="data-fim input-append date"> ' +
        '                 <input type="text" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" /> ' +
        '                 <span class="add-on"><i data-date-icon="icon-calendar"></i></span> ' +
        '               </div> ' +
        '               <div class="hora-fim input-append" style="margin-left:-3px;"> ' +
        '                 <input type="text" class="span1_4" data-format="hh:mm:ss" autocomplete="off"  /> ' +
        '                 <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span> ' +
        '               </div> ' +
        '       <button class="btn btn-agora" data-loading-text="Hora atual..." title="Recuperar hora atual" style="height: 30px;position: relative;bottom: 5px;">Agora</button> ' +
        '             </form> ' +
        '             </li> '
    };
});
//}

//{ Diretivas de filtroFalhas / <li filtro-falhas style="padding-left:8px;"></li>
Estalo.directive('filtroFalhas', function () {
    return {
        restrict: 'AEC',
        template: ' <!-- Filtro: Falhas --> ' +
        ' <form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <select class="filtro-falhas selectpicker span2 show-tick" id="filtro-falhas" title="Falhas"></select> ' +
        ' </div>' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE falhas
                    $('.filtro-falhas').mouseover(function () {
                        // Não mostrar filtros desabilitados
                        if (!$('div.btn-group.filtro-falhas .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-falhas').addClass('open');
                            $('div.btn-group.filtro-falhas>div>ul').css({
                                'max-width': '500px',
                                'max-height': '500px',
                                'overflow-y': 'auto',
                                'min-height': '1px'
                            });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE Falhas
                    $('.filtro-falhas').mouseout(function () {
                        $('div.btn-group.filtro-falhas').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroJson / <li filtro-json style="padding-left:8px;"></li>
Estalo.directive('filtroJson', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <select class="filtro-jsons selectpicker" id="filtro-jsons" title="Contexto" data-live-search="true" data-size="20" multiple="multiple"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-jsons').mouseover(function () {
                        $('div.btn-group.filtro-jsons').addClass('open');
                        $('div.btn-group.filtro-jsons>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '350px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-jsons').mouseout(function () {
                        $('div.btn-group.filtro-jsons').removeClass('open');
                    });
                });
            });
        }
    };
});
//}


//{ Diretivas de filtroAplicacao / <li filtro-aplicacao class="dirFiltro"></li>
// EXIBIR AO PASSAR O MOUSE
Estalo.directive('filtroAplicacao', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkApl" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-aplicacao selectpicker span1_3" multiple="" title="Aplicações"> ' +
        '     </select> ' +
        '   </div> ' +
        ' </form> ',
        // link: function (scope, element, attrs) {
        //   element.on('click',funcaoTeste);
        //   function funcaoTeste(){
        //     console.log('asdasdadadasd')
        //   }
        // },
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-aplicacao").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} aplicações',
                        showSubtext: true
                    });
                    //console.log("Entrou controller Aplicac")
                    $(".filtro-aplicacao").mouseover(function () {
                        if ($("div.data-inicio.input-append.date").length) {
                            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                        }
                        if ($("div.data-fim.input-append.date").length) {
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                        }
                        if ($("#pag-prompts").length == 0 && $("#pag-ICs").length == 0) {
                            $("div.btn-group.filtro-aplicacao").addClass("open");
                            $("div.btn-group.filtro-aplicacao>div>ul").css({
                                "max-height": "500px",
                                "overflow-y": "auto",
                                "min-height": "1px",
                                "max-width": "350px"
                            });
                        } else {
                            $("div.btn-group.filtro-aplicacao").addClass("open");
                            $("div.btn-group.filtro-aplicacao>div>ul").css({
                                "max-height": "200px",
                                "overflow-y": "auto",
                                "min-height": "1px",
                                "max-width": "350px"
                            });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-aplicacao').mouseout(function () {
                        if ($("#pag-prompts").length == 0 && $("#pag-ICs").length == 0) {
                            $('div.btn-group.filtro-aplicacao').removeClass('open');
                        }
                        $scope.filtroICSelect = []
                        filtro_ics = []
                        ics = []
                        $scope.$apply()
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroGrupoMetas / <li filtro-grupos-meta class="dirFiltro"></li>
// EXIBIR AO PASSAR O MOUSE
Estalo.directive('filtroGruposMeta', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkMeta" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-grupos-meta selectpicker span1_3" multiple="" title="Grupos"> ' +
        '     </select> ' +
        '   </div> ' +
        ' </form> ',
        // link: function (scope, element, attrs) {
        //   element.on('click',funcaoTeste);
        //   function funcaoTeste(){
        //     console.log('asdasdadadasd')
        //   }
        // },
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-grupos-meta").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} grupos',
                        showSubtext: true
                    });
                    $(".filtro-grupos-meta").mouseover(function () {
                        if ($("div.data-inicio.input-append.date").length) {
                            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                        }
                        if ($("div.data-fim.input-append.date").length) {
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                        }
                        $('div.btn-group.filtro-grupos-meta').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-grupos-meta').mouseout(function () {
                        $('div.btn-group.filtro-grupos-meta').removeClass('open');
                        $scope.$apply()
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroItensDeControle / <li filtro-itens-de-controle style="padding-left:8px;"></li>
Estalo.directive('filtroItensDeControle', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <input type="text" id="filtro-ic" data-provide="typeahead" placeholder="Itens de Controle"> ' +
        ' </form> ',
        controller: function ($scope) {
            // EXIBIR AO PASSAR O MOUSE item de controle
            $('div.btn-group.filtro-ic').mouseover(function () {
                // Não mostrar filtros desabilitados
                if (!$('div.btn-group.filtro-ic .btn').hasClass('disabled')) {
                    $('div.btn-group.filtro-ic').addClass('open');
                    $('div.btn-group.filtro-ic>div>ul').css({
                        'max-width': '500px',
                        'max-height': '500px',
                        'overflow-y': 'auto',
                        'min-height': '1px'
                    });
                }
            });

            // OCULTAR AO TIRAR O MOUSE item de controle
            $('div.btn-group.filtro-ic').mouseout(function () {
                $('div.btn-group.filtro-ic').removeClass('open');
            });

        }
    };
});
//}

//{ Diretivas de filtroEstadoDialogo / <li filtro-estado-dialogo style="padding-left:8px;"></li>
Estalo.directive('filtroEstadoDialogo', function () {
    return {
        restrict: 'AEC',

        template: ' <li style="padding-left: 0px;">' +
        ' <select class="filtro-edAuto selectpicker show-tick span3" id="filtro-edAuto" title="Estados de Diálogo" data-live-search="true" data-size="20">' +
        '     </select>' +
        '        </li>',


        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE estado de dialogo
                    $('div.btn-group.filtro-edAuto').mouseover(function () {
                        // Não mostrar filtros desabilitados

                        $('div.btn-group.filtro-edAuto').addClass('open');
                        $('div.btn-group.filtro-edAuto>div>ul').css({
                            'max-width': '400px',
                            'max-height': '400px',
                            'overflow-y': 'auto',
                            'min-height': '1px'
                        });

                    });

                    // OCULTAR AO TIRAR O MOUSE estado de dialogo
                    $('div.btn-group.filtro-edAuto').mouseout(function () {
                        $('div.btn-group.filtro-edAuto').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroUf / <li filtro-uf class="dirFiltro"></li>
// EXIBIR AO PASSAR O MOUSE
Estalo.directive('filtroUf', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkUf" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-uf selectpicker span1_1" multiple="" title="UFs"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('div.btn-group.filtro-uf').mouseover(function () {
                        $('div.btn-group.filtro-uf').addClass('open');
                        $('div.btn-group.filtro-uf>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '400px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('div.btn-group.filtro-uf').mouseout(function () {
                        $('div.btn-group.filtro-uf').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroDataInicialComHora / <filtro-data-inicial-com-hora></filtro-data-inicial-com-hora>
Estalo.directive('filtroDataInicialComHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="data-inicio input-append date"> ' +
        '     <input type="text" id="filtroDataInicio" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" /> ' +
        '     <span class="add-on"><i data-date-icon="icon-calendar"></i></span> ' +
        '   </div> ' +
        '   <div class="hora-inicio input-append" id="dt" style="margin-left:-3px; width: 100px">  ' +
        '     <input type="text" id="filtroHoraInicio" class="span1_4" data-format="hh:mm:ss" autocomplete="off"  style="width:60px;"/> ' +
        '     <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR CALENDARIO AO PASSAR O MOUSE
                    $('div.data-inicio > span.add-on').mouseover(function () {
                        $("div.data-inicio.input-append.date").data("datetimepicker").show();
                        $("div.data-fim.input-append.date").data("datetimepicker").hide();

                        //OCULTAR O CALENDÁRIO
                        $("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
                            $(this).hide();
                        });

                        $('li[lista-relatorios]').mouseover(function () {
                            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                        });

                        $("#topLine").mouseover(function () {
                            if ($("div.data-inicio.input-append.date").length > 0) {
                                $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                            }
                            if ($("div.data-fim.input-append.date").length > 0) {
                                $("div.data-fim.input-append.date").data("datetimepicker").hide();
                            }
                        });

                        $("#bottomLine").mouseover(function () {
                            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                        });
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroDataFinalComHora / <filtro-data-final-com-hora></filtro-data-final-com-hora>
Estalo.directive('filtroDataFinalComHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="data-fim input-append date"> ' +
        '     <input type="text" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" /> ' +
        '     <span class="add-on"><i data-date-icon="icon-calendar"></i></span> ' +
        '   </div> ' +
        '   <div class="hora-fim input-append" style="margin-left:-3px;"> ' +
        '     <input type="text" class="span1_4" data-format="hh:mm:ss" autocomplete="off" style="width:60px;"/> ' +
        '     <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    //EXIBIR CALENDARIO AO PASSAR O MOUSE
                    $('div.data-fim > span.add-on').mouseover(function () {
                        $("div.data-inicio.input-append.date").data("datetimepicker").hide();
                        $("div.data-fim.input-append.date").data("datetimepicker").show();

                        // OCULTAR O CALENDÁRIO
                        $("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
                            $(this).hide();
                        });

                        $('li[lista-relatorios]').mouseover(function () {
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                        });

                        $("#bottomLine").mouseover(function () {
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                            $("div.data-fim.input-append.date").data("datetimepicker").hide();
                        });
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de chkBoxConsolFinalizados / <chk-box-consol-finalizados></chk-box-consol-finalizados>
Estalo.directive('chkBoxConsolFinalizados', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.consolFinalizados" value="consolFinalizados" class="pordia"> Consolidadores finalizados</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDiferencaDia / <chk-box-diferenca-dia></chk-box-diferenca-dia>
Estalo.directive('chkBoxDiferencaDia', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.diferencaDia" value="diferencaDia" class="pordia"> Diferença por Dia</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDiferencaDia / <chk-box-forcar-consolidador-normal></chk-box-forcar-consolidador-normal>
Estalo.directive('chkBoxForcarConsolidadorNormal', function () {
  return {
      restrict: 'AEC',
      replace: true,
      template: '<form class="navbar-form">' +
      '<div class="checkbox navbar-btn filtro-check">' +
      '<input type="checkbox" ng-model="filtros.forcarConsolidadorNormal" value="forcarConsolidadorNormal" class="pordia"> Forçar consolidador normal</input>' +
      '</div>' +
      '</form>'
  }
})
//}

//{ Diretivas de chkBoxIgnoraIrrelevante / <chk-box-ignora-irrelevante></chk-box-ignora-irrelevante>
Estalo.directive('chkBoxIgnoraIrrelevante', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="ignorairrelevante" value="ignorairrelevante" class="ignorairrelevante"> Ignorar EDs irrelevantes</input>' +
        '</div>' +
        '</form>'
    }
})
//}


//{ Diretivas de chkBoxComSms / <chk-box-com-sms></chk-box-com-sms>
Estalo.directive('chkBoxComSms', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.comSMS" value="porsms" class="pordia"> Com envio de SMS</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxAgruparPorRegiao / <chk-box-agrupar-por-regiao></chk-box-agrupar-por-regiao>
Estalo.directive('chkBoxAgruparPorRegiao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="porRegEDDD"  value="porRegEDDD" ng-model="porRegEDDD" disabled="" title="Agrupar por regiões e/ou ddds">Agrupar</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxIncluirIit / <chk-box-incluir-iit></chk-box-incluir-iit>
Estalo.directive('chkBoxIncluirIit', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '' +
        ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="iit"  value="iit" ng-model="iit" title="Incluir chamadas IIT">IIT</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxIncluirtlv / <chk-box-incluir-tlv></chk-box-incluir-tlv>
Estalo.directive('chkBoxIncluirTlv', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '' +
        ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="tlv"  value="tlv" ng-model="tlv" title="Incluir chamadas TLV">TLV</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxIncluirtlv / <chk-box-incluir-reclamacao></chk-box-incluir-reclamacao>
Estalo.directive('chkBoxIncluirReclamacao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '' +
        ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="reclamacao"  value="reclamacao" ng-model="reclamacao" title="Incluir chamadas Reclamacao">Reclamacao</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxIncluirtlv / <chk-box-incluir-contas></chk-box-incluir-contas>
Estalo.directive('chkBoxIncluirContas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '' +
        ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="contas"  value="contas" ng-model="contas" title="Incluir chamadas Contas">Contas</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxIncluirtlv / <chk-box-incluir-repetidas></chk-box-incluir-repetidas>
Estalo.directive('chkBoxIncluirRepetidas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '' +
        ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" id ="repetidas"  value="repetidas" ng-model="repetidas" title="Incluir chamadas Repetidas">Repetidas</input> ' +
        ' </div> '
    }
})
//}


//{ Diretivas de filtroDataInicialSemHora / <filtro-data-inicial-sem-hora></filtro-data-inicial-sem-hora>
Estalo.directive('filtroDataInicialSemHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">  ' +
        '   <div class="datahora-inicio input-append date">  ' +
        '     <input type="text" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" />  ' +
        '     <span class="add-on"><i data-date-icon="icon-calendar"></i></span>  ' +
        '   </div>  ' +
        ' </form>  ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR CALENDARIO AO PASSAR O MOUSE
                    $('div.datahora-inicio > span.add-on').mouseover(function () {
                        $("div.datahora-inicio.input-append.date").data("datetimepicker").show();
                        if ($("div.datahora-fim.input-append.date").length > 0) {
                            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                        }

                        // OCULTAR O CALENDÁRIO
                        $("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
                            $(this).hide();
                        });

                        $('li[lista-relatorios]').mouseover(function () {
                            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
                        });

                        $("#topLine").mouseover(function () {
                            if ($("div.datahora-inicio.input-append.date").length > 0) {
                                $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
                            }
                            if ($("div.datahora-fim.input-append.date").length > 0) {
                                $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                            }
                        });

                        $("#bottomLine").mouseover(function () {
                            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
                            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                        });
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroDataMes / <filtro-data-mes></filtro-data-mes>
Estalo.directive('filtroDataMes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">  ' +
        '   <div class="datahora-mes input-append date">  ' +
        '     <input type="text" disabled="disabled" class="span1_1" data-format="MM/yyyy " autocomplete="off" />  ' +
        '     <span class="add-on"><i data-date-icon="icon-calendar"></i></span>  ' +
        '   </div>  ' +
        ' </form>  ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR CALENDARIO AO PASSAR O MOUSE
                    $('div.datahora-mes > span.add-on').mouseover(function () {
                        $("div.datahora-mes.input-append.date").data("datetimepicker").show();

                        // OCULTAR O CALENDÁRIO
                        $("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
                            $(this).hide();
                        });

                        $('li[lista-relatorios]').mouseover(function () {
                            $("div.datahora-mes.input-append.date").data("datetimepicker").hide();
                        });

                        $("#topLine").mouseover(function () {
                            if ($("div.datahora-mes.input-append.date").length > 0) {
                                $("div.datahora-mes.input-append.date").data("datetimepicker").hide();
                            }
                        });

                        $("#bottomLine").mouseover(function () {
                            $("div.datahora-mes.input-append.date").data("datetimepicker").hide();
                        });
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de filtroDataFinalSemHora / <filtro-data-final-sem-hora></filtro-data-final-sem-hora>
Estalo.directive('filtroDataFinalSemHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">  ' +
        '   <div class="datahora-fim input-append date">  ' +
        '     <input type="text" disabled="disabled" class="span1_1" data-format="dd/MM/yyyy " autocomplete="off" />  ' +
        '     <span class="add-on"><i data-date-icon="icon-calendar"></i></span>  ' +
        '   </div>  ' +
        ' </form>  ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR CALENDARIO AO PASSAR O MOUSE
                    $('div.datahora-fim > span.add-on').mouseover(function () {
                        $("div.datahora-fim.input-append.date").data("datetimepicker").show();
                        $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();

                        // OCULTAR O CALENDÁRIO
                        $("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
                            $(this).hide();
                        });

                        $(".menu-principal").mouseover(function () {
                            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                        });

                        $("#topLine").mouseover(function () {
                            if ($("div.datahora-fim.input-append.date").length > 0) {
                                $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                            }
                            if ($("div.datahora-inicio.input-append.date").length > 0) {
                                $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
                            }
                        });

                        $("#bottomLine").mouseover(function () {
                            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
                            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
                        });
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de chkBoxMensal / <chk-box-mensal></chk-box-mensal>
Estalo.directive('chkBoxMensal', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        '   <input class="chkMensal" type="checkbox" ng-model="chkMensal" title="Agrupar por Mês">Por mês</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de btnAgr / <btn-agr></btn-agr>
Estalo.directive('btnAgr', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-agora" data-loading-text="Hora atual..." title="Recuperar hora atual">Agora</button> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de btnLimpar / <btn-limpar></btn-limpar>
Estalo.directive('btnLimpar', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-limpar-filtros" data-loading-text="..." title="Limpa todos os filtros selecionados">Limpar filtros</button> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de btnGerarRelatorio / <btn-gerar-relatorio></btn-gerar-relatorio>
Estalo.directive('btnGerarRelatorio', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-gerar" data-loading-text="...">Carregar</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $('.btn-gerar').click(function () {
                $('div.notification').fadeIn(500);
            });
        }
    }
})
//}

//{ Diretivas de btnGerarNovo / <btn-gerar-novo></btn-gerar-novo>
Estalo.directive('btnGerarNovo', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-gerar" data-loading-text="Carregando...">Carregar</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $('.btn-gerar').click(function () {
                $('div.notification').fadeIn(500);
            });
        }
    }
})
//}

//{ Diretivas de btnGerarRelatorioExporta / <btn-gerar-relatorio-exporta></btn-gerar-relatorio-exporta>
Estalo.directive('btnGerarRelatorioExporta', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-gerar-teradata" data-loading-text="Carregando...">Exportação Teradata</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $('.btn-gerar').click(function () {
                $('div.notification').fadeIn(500);
            });
        }
    }
})
//}

//{ Diretivas de btnGerarRelatorioMailing / <btn-gerar-relatorio-mailing></btn-gerar-relatorio-mailing>
Estalo.directive('btnGerarRelatorioMailing', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-gerar-mailing" data-loading-text="Carregando...">Mailing</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $('.btn-gerar').click(function () {
                $('div.notification').fadeIn(500);
            });
        }
    }
})
//}

//{ Diretivas de btnConsolFinalizados / <btn-consol-finalizados></btn-consol-finalizados>
Estalo.directive('btnConsolFinalizados', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-consol-final" data-loading-text="Carregando...">Consolidadores</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $('.btn-gerar').click(function () {
            });
        }
    }
})
//}

//{ Diretivas de btnExportarRelatorio / <btn-exportar-relatorio></btn-exportar-relatorio>
Estalo.directive('btnExportarRelatorio', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-exportar" data-loading-text="..." disabled="">Exportar</button> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de btnExportarExcel / <btn-exportar-excel></btn-exportar-excel>
Estalo.directive('btnExportarExcel', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <button class="btn btn-exportar" data-loading-text="..." disabled="">Excel</button> ' +
        ' </form> ',
        controller: function ($scope) {
            $(".btn-exportar").click(function () {
                $(".dropdown-exportar").toggle();
            })
        }
    }
})
//}

/**
 * Este botão usa a variavel $scopo.colunas e $scope.csv para gerar um arquivo CSV.
 * a variavel $scope.colunas deve ser um array de objetos
 * a variavel $scope.csv deve ser um array de arrays.
 * cada array deve conter uma linha do arquivo.
 * exemplo de variável:
 * $scope.colunas = [
 *      {displayName: "Código"},    
 *      {displayName: "Descrição"},    
 *      {displayName: "Data"},    
 *      {displayName: "Total de chamadas"},
 *  ]
 * $scope.csv = [
 *     ["Código", "Descrição", "Data", "Total de chamadas"],
 *     ["CEP_PM001", "Pre menu", "11/03/2018", 278],
 *     ["ICAC001", "Check retornou três datas igual a sim", "12/03/2018", 0],
 *     ["ICAD006", "Check serviço bloqueio igual a não" ,"12/03/2018", 0]
 *  ]
 *
 * Para implementar o botão em um relatório é preciso preencher as variáveis $scope.colunas e $scope.csv e habilitar o botão
 * quando houver dados, pois assim como o de exportar para xlsx, ele começa desabilitado.
 */
Estalo.directive('btnExportarCsv', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '   <button class="btn btn-exportarCSV" data-loading-text="..." disabled="">Exportar</button> ' +
        '</form>',
        controller: function ($scope) {

            $(".btn-exportarCSV").click(function () {
                console.log($scope);
                if ($scope.csv.length > 0) {
                    separator = ";"
                    colunas = ""
                    file_name = tempDir3() + "export_" + Date.now()
                    for (i = 0; i < $scope.colunas.length; i++) {
                        colunas = colunas + $scope.colunas[i].displayName + separator
                    }
                    colunas = colunas + "\n"
                    for (i = 0; i < $scope.csv.length; i++) {
                        colunas = colunas + $scope.csv[i].join(separator) + "\n"
                    }
                    
                    exportaTXT(colunas, file_name, 'csv', true);
                    // $scope.csv = [];
                } else {
                    $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
                }
                $(".dropdown-exportar").toggle();
            })
        }
    }
})

Estalo.directive('btnExportarCsvInterface', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '   <button class="btn btn-exportarCSV-interface" data-loading-text="..." disabled="">Exportar</button> ' +
        '</form>'
    }
})

Estalo.directive('btnExportarDropdown', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '   <button class="btn btn-exportar-dropdown-form" data-loading-text="..." disabled="">Exportar &#8942;</button>' +
        '   <div class="dropdown-exportar" style="display: none">' +
        '       <btn-exportar-excel></btn-exportar-excel>' +
        '       <btn-exportar-csv></btn-exportar-csv>' +
        '   </div>' +
        '</form>',
        controller: function ($scope) {

            $(".btn-exportar-dropdown-form").click(function () {
                $(".dropdown-exportar").toggle();
            })
        }
    }
})

Estalo.directive('btnExportarDropdown2', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '   <button class="btn btn-exportar-dropdown-form" data-loading-text="..." disabled="">Exportar &#8942;</button>' +
        '   <div class="dropdown-exportar" style="display: none">' +
        '       <btn-exportar-excel></btn-exportar-excel>' +
        '       <btn-exportar-csv-interface></btn-exportar-csv-interface>' +
        '   </div>' +
        '</form>',
        controller: function ($scope) {

            $(".btn-exportar-dropdown-form").click(function () {
                $(".dropdown-exportar").toggle();
            })
        }
    }
})

Estalo.directive('btnExportarDropdown3', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '   <button class="btn btn-exportar-dropdown-form" data-loading-text="..." disabled="">Exportar &#8942;</button>' +
        '   <div class="dropdown-exportar" style="display: none">' +
        '       <btn-exportar-csv></btn-exportar-csv>' +
        '   </div>' +
        '</form>',
        controller: function ($scope) {

            $(".btn-exportar-dropdown-form").click(function () {
                $(".dropdown-exportar").toggle();
            })
        }
    }
})

//{ Diretivas de btnAjuda / <btn-ajuda></btn-help>
Estalo.directive('btnAjuda', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form" id="btnAjuda" style="position: absolute; right: -30px;">' +
        '<button class="btn" ng-click="abreAjuda(link)" title="Abre uma página com manuais de ajuda." style="padding: 4px 4px;"><font size="small"><i class="icon-question-sign"></i></font></button> ' +
        '</form>'
    }
});

//}

//{ Diretivas de filtroJsonVariaveis / <filtro-json-variaveis></filtro-json-variaveis>
Estalo.directive('filtroJsonVariaveis', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <input type="text" class="form-control" id="contextVal" title="Insira aqui os valores de variáveis separados por ponto e vírgula">' +
        ' </div>' +
        '</form>'
    }
});

//}

//{ Diretivas de filtroColunas / <filtro-colunas></filtro-colunas>
Estalo.directive('filtroColunas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkCols" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '<select class="filtro-cols selectpicker span1_3" multiple="" title="Colunas"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-cols").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} colunas',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-cols').mouseover(function () {
                        $('div.btn-group.filtro-cols').addClass('open');
                        $('div.btn-group.filtro-cols>div').css({
                            'max-height': '450px',
                            'overflow-y': 'auto',
                            'overflow-x': 'hidden',
                            'min-height': '1px'
                        });
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-cols').mouseout(function () {
                        $('div.btn-group.filtro-cols').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroUltimoDigito / <filtro-ultimo-digito></filtro-ultimo-digito>
Estalo.directive('filtroUltimoDigito', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkUltDig" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-ud selectpicker span2_1" multiple="" title="Último Dígito"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-ud").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} digitos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-ud').mouseover(function () {
                        $('div.btn-group.filtro-ud').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-ud').mouseout(function () {
                        $('div.btn-group.filtro-ud').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroDoisDigitos / <filtro-dois-digitos></filtro-dois-digitos>
Estalo.directive('filtroDoisDigitos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkUltDoisDig" data-var="0" title="Clique para carregar diversos valores"><i class="icon-wrench"></i></a>' +
        '   <select class="filtro-udd selectpicker span2_1" multiple="" title="Últimos Dígitos"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-udd").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} digitos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-udd').mouseover(function () {
                        $('div.btn-group.filtro-udd').addClass('open');
                        $('div.btn-group.filtro-udd>div').css({
                            'max-height': '450px',
                            'overflow-y': 'auto',
                            'min-height': '1px'
                        });
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-udd').mouseout(function () {
                        $('div.btn-group.filtro-udd').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroDigitoFinal / <filtro-digito-final></filtro-digito-final>
Estalo.directive('filtroDigitoFinal', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkFinal" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-final selectpicker span1_2" multiple="" title="Finais">' +
        '     <option value="0">0</option>' +
        '     <option value="1">1</option>' +
        '     <option value="2">2</option>' +
        '     <option value="3">3</option>' +
        '     <option value="4">4</option>' +
        '     <option value="5">5</option>' +
        '     <option value="6">6</option>' +
        '     <option value="7">7</option>' +
        '     <option value="8">8</option>' +
        '     <option value="9">9</option>' +
        '</select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-udd").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} digitos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-final').mouseover(function () {
                        $('div.btn-group.filtro-final').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-final').mouseout(function () {
                        $('div.btn-group.filtro-final').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroIntervaloMinutos / <filtro-intervalo-minutos></filtro-intervalo-minutos>
Estalo.directive('filtroIntervaloMinutos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <input type="number" class="filtro-intervalo form-control span1_2" placeholder="Intervalo" ng-model="filtros.intervalo" />' +
        '</form>'
    }
})
//}

//{ Diretivas de filtroMotivos / <filtro-motivos></filtro-motivos>
Estalo.directive('filtroMotivos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <select class="filtro-motivo selectpicker span2" multiple="" title="Motivo"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-motivo").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} motivos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-motivo').mouseover(function () {
                        $('div.btn-group.filtro-motivo').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-motivo').mouseout(function () {
                        $('div.btn-group.filtro-motivo').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroEntradas / <filtro-entradas></filtro-entradas>
Estalo.directive('filtroEntradas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <select class="filtro-entrada selectpicker span2" multiple="" title="Entrada"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-entrada").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} entradas',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-entrada').mouseover(function () {
                        $('div.btn-group.filtro-entrada').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-entrada').mouseout(function () {
                        $('div.btn-group.filtro-entrada').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroTiposPlanos / <filtro-tipos-planos></filtro-tipos-planos>
Estalo.directive('filtroTiposPlanos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkPlano" data-var="0" title="Clique para marcar todos">' +
        '     <i class="icon-ok"></i>' +
        '   </a>' +
        '   <select class="filtro-tipoplano selectpicker span1_3" multiple="" title="Plano">' +
        '     <option value="CCF">Controle Com Fatura</option>' +
        '     <option value="CSF">Controle Sem Fatura</option>' +
        '     <option value="TUN">Tarifa Única</option>' +
        '   </select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-tipoplano").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} planos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-tipoplano').mouseover(function () {
                        $('div.btn-group.filtro-tipoplano').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-tipoplano').mouseout(function () {
                        $('div.btn-group.filtro-tipoplano').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroTipos / <filtro-tipos></filtro-tipos>
Estalo.directive('filtroTipos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append" id="dvTipo">' +
        '<select class="filtro-tipo selectpicker span2 show-tick" disabled title="Tipo"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-tipo").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} tipos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-tipo').mouseover(function () {
                        $('div.btn-group.filtro-tipo').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-tipo').mouseout(function () {
                        $('div.btn-group.filtro-tipo').removeClass('open');
                    });
                });
            });
        }
    }
})
//}



//{ Diretivas de filtroTiposPagamentos / <filtro-tipos-pagamentos></filtro-tipos-pagamentos>
Estalo.directive('filtroTiposPagamentos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkPagamento" data-var="0" title="Clique para marcar todos">' +
        '     <i class="icon-ok"></i>' +
        '   </a>' +
        '   <select class="filtro-tipopagamento selectpicker span1_3" multiple="" title="Pagamento">' +
        '     <option value="BOL">Boleto</option>' +
        '     <option value="CAR">Cartão</option>' +
        '   </select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-tipopagamento").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} pagamentos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-tipopagamento').mouseover(function () {
                        $('div.btn-group.filtro-tipopagamento').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-tipoplano').mouseout(function () {
                        $('div.btn-group.filtro-tipopagamento').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroGruposProdutos / <filtro-grupos-produtos></filtro-grupos-produtos>
Estalo.directive('filtroGruposProdutos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkGru" data-var="0" title="Clique para marcar todos" disabled="">' +
        '     <i class="icon-ok"></i>' +
        '   </a>' +
        '   <select class="filtro-grupo-produto selectpicker span1_2" multiple="" title="Grupos" disabled=""></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-grupo-produto").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} grupos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-grupo-produto').mouseover(function () {
                        if (!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-grupo-produto').addClass('open');
                        }
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-grupo-produto').mouseout(function () {
                        $('div.btn-group.filtro-grupo-produto').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroProdutos / <filtro-produtos></filtro-produtos>
Estalo.directive('filtroProdutos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkProd" data-var="0" title="Clique para marcar todos" disabled=""><i class="icon-ok"></i></a>' +
        '   <select class="filtro-produto selectpicker span1_3" multiple="" title="Produtos" disabled=""></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-produto").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} produtos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-produto').mouseover(function () {
                        if (!$('div.btn-group.filtro-produto .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-produto').addClass('open');
                        }
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-produto').mouseout(function () {
                        $('div.btn-group.filtro-produto').removeClass('open');
                    });
                });
            });
        }
    }
})
//}


//{ Diretivas de filtroTipoAutomacao / <filtro-tipos-automacoes></filtro-tipos-automacoes>
Estalo.directive('filtroTiposAutomacoes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkProd" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-tipo-automacao selectpicker span1_3" multiple="" title="Automações" ></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-tipo-automacao").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} automações',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-tipo-automacao').mouseover(function () {
                        if (!$('div.btn-group.filtro-tipo-automacao .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-tipo-automacao').addClass('open');
                        }
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-tipo-automacao').mouseout(function () {
                        $('div.btn-group.filtro-tipo-automacao').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroOperacoes / <filtro-operacoes></filtro-operacoes>
Estalo.directive('filtroOperacoes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkOper" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-operacao selectpicker span1_3" multiple="" title="Operações"> </select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-operacao").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} operações',
                        showSubtext: true
                    });
                    $('.filtro-operacao').mouseover(function () {
                        $('div.btn-group.filtro-operacao').addClass('open');
                        $('div.dropdown-menu.open').css({ 'margin-left': '-3px' });
                        $('div.btn-group.filtro-operacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '250px' });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-operacao').mouseout(function () {
                        $('div.btn-group.filtro-operacao').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroOps / <filtro-ops></filtro-ops>
// Usado no resumo performanceDDDUD
Estalo.directive('filtroOps', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkOp" data-var="0" title="Clique para marcar todos" disabled="">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-op selectpicker span1_1" multiple="" title="Operações" disabled=""></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-op").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} operações',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
					$('.filtro-op').mouseover(function () {
                        $('div.btn-group.filtro-op').addClass('open');
                        $('.filtro-op div.dropdown-menu.open').css({ 'margin-left': '-133px' });
                        $('div.btn-group.filtro-op>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '250px' });
                    });
				
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-op').mouseout(function () {
                        $('div.btn-group.filtro-op').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroResultado / <filtro-resultado></filtro-resultado>
Estalo.directive('filtroResultado', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkRes" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-status selectpicker span1_3" multiple="" title="Resultados"></select>' +
        '   </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-status").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} resultados',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-status').mouseover(function () {
                        $('div.btn-group.filtro-status').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-status').mouseout(function () {
                        $('div.btn-group.filtro-status').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroNaoAbordados / <filtro-nao-abordados></filtro-nao-abordados>
Estalo.directive('filtroNaoAbordados', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form" ng-show="filtros.naoAbordado">' +
        '<div class="datahora-fim input-append date">' +
        '<a class="btn btn-small checkAll2" id="alinkRes" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-status-nao-abordado selectpicker span1_3" multiple="" title="Não Abordados"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-status-nao-abordado").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} não abordados',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-status-nao-abordado').mouseover(function () {
                        $('div.btn-group.filtro-status-nao-abordado').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-status-nao-abordado').mouseout(function () {
                        $('div.btn-group.filtro-status-nao-abordado').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroErros / <filtro-erros></filtro-erros>
Estalo.directive('filtroErros', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkErr" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-erro selectpicker span1_2" multiple="" title="Erros"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-erros").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} erros',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-erros').mouseover(function () {
                        $('div.btn-group.filtro-erros').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-erros').mouseout(function () {
                        $('div.btn-group.filtro-erros').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroParceiros / <filtro-parceiros></filtro-parceiros>
Estalo.directive('filtroParceiros', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkRecarga" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-empresaRecarga selectpicker span1_3" multiple="" title="Parceiros"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-empresaRecarga").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} parceiros',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-empresaRecarga').mouseover(function () {
                        $('div.btn-group.filtro-empresaRecarga').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-empresaRecarga').mouseout(function () {
                        $('div.btn-group.filtro-empresaRecarga').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroCanais / <filtro-canais></filtro-canais>
Estalo.directive('filtroCanais', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <a class="btn btn-small checkAll2" id="alinkRecarga" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-canal selectpicker span2" multiple="" title="Canal Recarga">' +
        '     <option value="ussd">USSD</option>' +
        '     <option value="outros">Outros</option>' +
        '   </select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-canal").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} canais',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-canal').mouseover(function () {
                        $('div.btn-group.filtro-canal').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-canal').mouseout(function () {
                        $('div.btn-group.filtro-canal').removeClass('open');
                    });
                });
            });
        }
    }
})


//{ Diretivas de filtroCodPromt / <filtro-codprompt></filtro-codprompt>
Estalo.directive('filtroCodPromt', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <input type="text" class="filtro-codprompt" placeholder="Entre com o Código do Prompt">' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
            });
        }
    }
})
//}


//{ Diretivas de filtroCodPromt / <filtro-codprompt></filtro-codprompt>
Estalo.directive('filtroCodPromt', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append">' +
        '   <input type="text" class="filtro-codprompt" placeholder="Entre com o Código do Prompt">' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
            });
        }
    }
})
//}


//{ Diretivas de filtroIncentivos / <filtro-incentivos></filtro-incentivos>
Estalo.directive('filtroIncentivos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="datahora-fim input-append date">' +
        '   <a class="btn btn-small checkAll2" id="alinkProd" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-incentivo selectpicker span1_3" multiple="" title="Incentivos"></select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-incentivo").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} incentivos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-incentivo').mouseover(function () {
                        $('div.btn-group.filtro-incentivo').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-incentivo').mouseout(function () {
                        $('div.btn-group.filtro-incentivo').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtroProdutoPrePago / <filtro-produto-prepago></filtro-produto-prepago>
Estalo.directive('filtroProdutoPrepago', function () {
    return {
        restrict: 'AEC',
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkPrepago" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-produto-prepago selectpicker span1_3" multiple="" title="Produto"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-produto-prepago").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Produto',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-produto-prepago').mouseover(function () {
                        $('div.btn-group.filtro-produto-prepago').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-produto-prepago').mouseout(function () {
                        $('div.btn-group.filtro-produto-prepago').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de importarTelefoneTxt <importar-telefone-txt></importar-telefone-txt>
Estalo.directive('importarTelefoneTxt', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div> ' +
        '   <button id="import2" class="btn" data-toggle="modal" data-target="#modalConfirma" title="Importar números chamadores ou tratados a partir de arquivo texto">Telefones (TXT)</button> ' +
        '   <input id="browser" class="hideMe" type="file"></input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de importarPlanilha <importar-planilha></importar-planilha>
Estalo.directive('importarPlanilha', function () {
    return {
        restrict: 'AEC',
        replace: true,

	    template: '<form class="navbar-form">' +
	   '<label for="contingenciaInput" class="btn labelBtn" title="">Importar</label>'+
	   '<input accept=".xls,.xlsx" type="file" class="form-control hide" style="display:none;" name="contingenciaInput" id="contingenciaInput"  />'+
	   '</form>'

    }
})
//}

//{ Diretivas de importarPlanilha <importar-planilha-gestao-estado></importar-planilha-gestao-estado>
Estalo.directive('importarPlanilhaGestaoEstado', function () {
    return {
        restrict: 'AEC',
        replace: true,

	    template: '<form class="navbar-form">' +
	   '<label for="gestaoInput" class="btn labelBtn" title="">Importar</label>'+
	   '<input accept=".xls,.xlsx" type="file" class="form-control hide" style="display:none;" name="gestaoInput" id="gestaoInput"  />'+
	   '</form>'

    }
})
//}

//{ Diretivas de btnCancelarExtracao <btn-cancelar-extracao></btn-cancelar-extracao>
Estalo.directive('btnCancelarExtracao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<button class="btn btn-cancelar" data-loading-text="..." title="Clique aqui para cancelar extração">Cancelar</button>' +
        '</form>'
    }
})
//}

//{ Diretivas de labelMenorMes / <label-menor-mes></label-menor-mes>
Estalo.directive('labelMenorMes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<span class="ultMes" title="Menor mês possível para extração de dados">Dados a partir de:{{datsUltMesDetCham}}</span>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxOueEd / <chk-box-oue-ed></chk-box-oue-ed>
Estalo.directive('chkBoxOueEd', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="oueED"  value="oueED" ng-model="oueED" title="O padrão é OU" >OU / E (ED)</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxOueIc / <chk-box-oue-ic></chk-box-oue-ic>
Estalo.directive('chkBoxOueIc', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="oueIC"  value="oueIC" ng-model="oueIC" title="O padrão é OU" >OU / E (IC)</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxUltimoEd / <chk-box-ultimo-ed></chk-box-ultimo-ed>
Estalo.directive('chkBoxUltimoEd', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">' +
        '   <div class="checkbox navbar-btn filtro-check"> ' +
        '     <input type="checkbox" id="ultimoED" value="UltimoED" ng-model="filtros.isUltimoED" ng-disabled="filtroEDSelect.length!=1" title="último estado de diálogo">ED</input> ' +
        '   </div> ' +
        ' </form> '
    }
})
//}

// { Diretivas de chkBoxNps / <chk-box-nps></chk-box-nps>
Estalo.directive('chkBoxNps', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        '   <input type="checkbox" id="iit" value="nps" ng-model="filtros.nps" title="Respostas de pesquisa NPS">NPS</input> ' +
        ' </div> '
    }
})
//}

// { Diretivas de chkNonoDigito / <chk-nono-digito></chk-nono-digito>
Estalo.directive('chkNonoDigito', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        '   <input type="checkbox" id="iit" value="nono" ng-model="nonodigito" title="Pesquisar com e sem nono dígito na consulta por telefone">9º DÍGITO</input> ' +
        ' </div> '
    }
})
//}

// { Diretivas de chkBoxNaoAbordados / <chk-box-nao-abordados></chk-box-nao-abordados>
Estalo.directive('chkBoxNaoAbordados', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.naoAbordado" class="filtro-naoAbordado ng-pristine ng-valid" title = "Não abordados">Não Abord.</input>' +
        '</div>'
    }
})
//}


//{ Diretivas de filtroBases / <filtro-bases></filtro-bases>
Estalo.directive('filtroBases', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        //'     <a class="btn btn-small checkAll2" id="alinkPesq" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-base selectpicker span1_2" multiple="" title="Bases"> </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
					
					
					$("select.filtro-base").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Bases'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-nota').mouseover(function () {
                        $('div.btn-group.filtro-base').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-nota').mouseout(function () {
                        $('div.btn-group.filtro-base').removeClass('open');
                    });
                })
            })
        }
    }
})
//}


//{ Diretivas de filtroServicos / <filtro-servicos></filtro-servicos>
Estalo.directive('filtroServicos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkPesq" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-servico selectpicker span1_3" multiple="" title="Serviços"> </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-servico').mouseover(function () {
                        $('div.btn-group.filtro-servico').addClass('open');
                        $('div.btn-group.filtro-servico>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '350px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-servico').mouseout(function () {
                        $('div.btn-group.filtro-servico').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de radioDiaMesPadrao / <radio-dia-mes-padrao></radio-dia-mes-padrao>
Estalo.directive('radioDiaMesPadrao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form radio-responsivo"> ' +
		'   <input type="radio"  ng-model="valor" value="p"><span class = "pordia">Padrão</span></input> ' +
        '   <input type="radio" ng-model="valor" value="d"><span class = "pordia">Dia</span></input> ' +
        '   <input type="radio" ng-model="valor" value="m"><span class = "pordia">Mês</span></input> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de radioDiaMes / <radio-dia-mes></radio-dia-mes>
Estalo.directive('radioDiaMes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form radio-responsivo"> ' +

        '   <input type="radio" ng-model="valor" value="d"><span class = "pordia">Dia</span></input> ' +
        '   <input type="radio" ng-model="valor" value="m"><span class = "pordia">Mês</span></input> ' +
        ' </form> '
    }
})
//}


//{ Diretivas de radioHoraDiaMesPadrao / <radio-hora-dia-mes-padrao></radio-hora-dia-mes-padrao>
Estalo.directive('radioHoraDiaMesPadrao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form radio-responsivo"> ' +
		'   <input type="radio"  ng-model="valor" value="p"><span class = "pordia">Padrão</span></input> ' +
        '   <input type="radio"  ng-model="valor" value="h"><span class = "pordia">Hora</span></input> ' +
        '   <input type="radio" ng-model="valor" value="d"><span class = "pordia">Dia</span></input> ' +
        '   <input type="radio" ng-model="valor" value="m"><span class = "pordia">Mês</span></input> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de radioHoraDiaMes / <radio-hora-dia-mes></radio-hora-dia-mes>
Estalo.directive('radioHoraDiaMes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form radio-responsivo"> ' +
        '   <input type="radio"  ng-model="valor" value="h"><span class = "pordia">Hora</span></input> ' +
        '   <input type="radio" ng-model="valor" value="d"><span class = "pordia">Dia</span></input> ' +
        '   <input type="radio" ng-model="valor" value="m"><span class = "pordia">Mês</span></input> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de radioProduto / <radio-produto></radio-produto>
Estalo.directive('radioProduto', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo rpd">' +
		'<input type="radio" id="pordia" ng-model="valor" value="operador">' +
        '<span class="pordia" title = "Operador">Op.</span>' +
		'<input type="radio" id="pordia" ng-model="valor" value="resultado">' +
        '<span class="pordia" title = "Resultado">Res.</span>' +
        '<input type="radio" id="pordia" ng-model="valor" value="produto">' +
        '<span class="pordia" title="Produto">Prod.</span>' +       
        '</form>'
    }
})
//}

//{ Diretivas de radioDiaHora / <radio-dia-hora></radio-dia-hora>
Estalo.directive('radioDiaHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo rdh">' +
		'<input type="radio" id="pordia" ng-model="valor2" value="padrao">' +
        '<span class="pordia" title="Padrão">P.</span>' +
        '<input type="radio" id="pordia" ng-model="valor2" value="dia">' +
        '<span class="pordia">Dia</span>' +
        '<input type="radio" id="pordia" ng-model="valor2" value="hora">' +
        '<span class="pordia">Hora</span>' +
        '</form>'
    }
})
//}

//{ Diretivas de radioIncentivoDiaHora / <radio-incentivo-dia-hora></radio-incentivo-dia-hora>
Estalo.directive('radioIncentivoDiaHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="radio" id="pordia" ng-model="valor" value="incentivo">' +
        '<span class="pordia">Por Incentivo</span>' +
        '<input type="radio" id="Radio1" ng-model="valor" value="dia">' +
        '<span class="pordia">Dia</span>' +
        '<input type="radio" id="Radio2" ng-model="valor" value="hora">' +
        '<span class="pordia">Hora</span>' +
        '</form>'
    }
})
//}



//{ Diretivas de radioIntervalo2448 / <radio-intervalo2448></radio-intervalo2448>
Estalo.directive('radioIntervalo2448', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="radio" id="24h" ng-model="valor" value="24">' +
        '<span class="por2421dia">24h</span>' +
        '<input type="radio" id="48h" ng-model="valor" value="48">' +
        '<span class="por2421dia">48h</span>' +
        '</form>'
    }
})
//}


//{ Diretivas de filtroNotas / <filtro-notas></filtro-notas>
Estalo.directive('filtroNotas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkNota" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-nota selectpicker span2" multiple="" title="Notas"></select>' +
        '</div>' +
        '</form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-nota").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Notas'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-nota').mouseover(function () {
                        $('div.btn-group.filtro-nota').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-nota').mouseout(function () {
                        $('div.btn-group.filtro-nota').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroProdutosPesquisa / <filtro-produtos-pesquisa></filtro-produtos-pesquisa>
Estalo.directive('filtroProdutosPesquisa', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkProdutoPesquisa" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-produto-pesquisa selectpicker span2" multiple="" title="Produtos"></select>' +
        '</div>' +
        '</form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-produto-pesquisa").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} 	Produtos'
                    });
					
					// EXIBIR AO PASSAR O MOUSE
					$('.filtro-produto-pesquisa').mouseover(function () {
                        $('div.btn-group.filtro-produto-pesquisa').addClass('open');
                        $('div.btn-group.filtro-produto-pesquisa>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '350px'
                        });
                    });
			
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-produto-pesquisa').mouseout(function () {
                        $('div.btn-group.filtro-produto-pesquisa').removeClass('open');
                    });
                })
            })
        }
    }
})
//}


//{ Diretivas de checkboxErroSucesso / <checkbox-erro-sucesso></checkbox-erro-sucesso>
Estalo.directive('checkboxErroSucesso', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="checkbox" id="pordia" ng-model="errSucErr">' +
        '<span class="pordia">Erro</span>' +
        '<input type="checkbox" id="pordia" ng-model="errSucSuc">' +
        '<span class="pordia">Sucesso</span>' +
        '</form>'
    }
})
//}


//{ Diretivas de radioErroSucesso / <radio-erro-sucesso></radio-erro-sucesso>
Estalo.directive('radioErroSucesso', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="radio" id="pordia" ng-model="errSuc" value="erro">' +
        '<span class="pordia">Erro</span>' +
        '<input type="radio" id="pordia" ng-model="errSuc" value="sucesso">' +
        '<span class="pordia">Sucesso</span>' +
        '</form>'
    }
})
//}


//{ Diretivas de radioPromocaoDiaHora / <radio-promocao-dia-hora></radio-promocao-dia-hora>
Estalo.directive('radioPromocaoDiaHora', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="radio" id="pordia" ng-model="valor" value="promocao">' +
        '<span class="pordia">Promoção</span>' +
        '<input type="radio" id="pordia" ng-model="valor" value="dia">' +
        '<span class="pordia">Dia</span>' +
        '<input type="radio" id="pordia" ng-model="valor" value="hora">' +
        '<span class="pordia">Hora</span>' +
        '</form>'
    }
})
//}

//{ Diretivas de radioSatisfacaoRecomendacao / <radio-satisfacao-recomendacao></radio-satisfacao-recomendacao>
Estalo.directive('radioSatisfacaoRecomendacao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form radio-responsivo"> ' +
        '   <input type="radio"  ng-model="valor2" value="s" name="nps"><span class = "pordia">Satisfação</span></input> ' +
        ' <input type="radio" ng-model="valor2" value="r" name="nps"><span class = "pordia">Recomendação</span></input> ' +
        ' </form> '
    }
})
//}



//{ Diretivas de chkBoxOferta / <chk-box-oferta></chk-box-oferta>
Estalo.directive('chkBoxOferta', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check">' +
        '<input class="chkOferta" type="checkbox" ng-model="oferta" title="Agrupar por Mês">Oferta</input>' +
        '</div>'
    }
})
//}

//{ Diretivas de chkBoxAgrupaAplicacoes / <chk-box-agrupa-aplicacoes></chk-box-agrupa-aplicacoes>
Estalo.directive('chkBoxAgrupaAplicacoes', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" value="agrupa" ng-model="agrupaApls" title="Agrupar aplicações">Agrupar aplicações</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxExibirServico / <chk-box-exibir-servico></chk-box-exibir-servico>
Estalo.directive('chkBoxExibirServico', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        ' <input type="checkbox" value="agrupa" ng-model="servico" title="Exibir serviço">Exibir serviço</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de chkBoxExibirIc / <chk-box-exibir-ic></chk-box-exibir-ic>
Estalo.directive('chkBoxExibirIc', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="checkbox navbar-btn filtro-check"> ' +
        '   <input type="checkbox" id ="iit"  value="agrupa" ng-model="item" title="Exibir IC">Exibir IC</input> ' +
        ' </div> '
    }
})
//}

//{ Diretivas de filtroEmpresas / <filtro-empresas></filtro-empresas>
Estalo.directive('filtroEmpresas', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">  ' +
        '   <div class="input-append">  ' +
        '     <a class="btn btn-small checkAll2" id="alinkEmp" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-empresa selectpicker span1_3" multiple="" title="Empresas" > ' +
        '   <option ng-repeat="item in arrayEmpresas" value="{{item.codigo}}">{{item.nome}}</option> </select>' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    var i = 0;
                    $scope.arrayEmpresas = [];
                    while (i < cache.empresas.length && cache.empresas[i] != 'indice') {
                        var obj = {
                            codigo: cache.empresas[i].codigo,
                            nome: cache.empresas[i].nome
                        };
                        $scope.arrayEmpresas.push(obj);
                        i++;
                    }
                    $("select.filtro-empresa").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Empresas'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-empresa').mouseover(function () {
                        $('div.btn-group.filtro-empresa').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-empresa').mouseout(function () {
                        $('div.btn-group.filtro-empresa').removeClass('open');
                    });
                    $('div.filtro-empresa').hover(function () {
                        $('select.filtro-empresa').selectpicker('refresh');
                    })
                })
            })
        },
    }
})
//}

//{ Diretivas de filtroRegrasNg / <filtro-regras-ng></filtro-regras-ng>
Estalo.directive('filtroRegrasNg', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkRegra" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-regra selectpicker span1_3" multiple="" title="Regras" data-live-search="true" data-size="20" ng-model="regrasSelecionadas" ng-options="item.codigo as item.nome for item in arrayRegras"></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    var i = 0;
                    $scope.arrayRegras = [];
                    while (i < cache.regras.length && cache.regras[i] != 'indice') {
                        var obj = {
                            codigo: cache.regras[i].codigo,
                            nome: cache.regras[i].nome
                        };
                        $scope.arrayRegras.push(obj);
                        i++;
                    }
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-regra').mouseover(function () {
                        $('div.btn-group.filtro-regra').addClass('open');
                        $('div.btn-group.filtro-regra>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '400px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-regra').mouseout(function () {
                        $('div.btn-group.filtro-regra').removeClass('open');
                    });
                    $('div.filtro-regra').hover(function () {
                        $('select.filtro-regra').selectpicker('refresh');
                    })
                })
            })
        },

    }
})
//}

//{ Diretivas de filtroRegras / <filtro-regras></filtro-regras>
Estalo.directive('filtroRegras', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkRegra" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '       <select class="filtro-regra selectpicker span1_3" multiple="" title="Regras" data-live-search="true" data-size="20"> </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-regra").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Regras'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-regra').mouseover(function () {
                        $('div.btn-group.filtro-regra').addClass('open');
                        $('div.btn-group.filtro-regra>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '400px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-regra').mouseout(function () {
                        $('div.btn-group.filtro-regra').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroClusters / <filtro-clusters></filtro-clusters>
Estalo.directive('filtroClusters', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkCluster" data-var="0" title="Clique para marcar todos">' +
        '<i class="icon-ok"></i>' +
        '</a>' +
        '<select class="filtro-cluster selectpicker span2" multiple="" title="Clusters"></select>' +
        '</div>' +
        '</form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-cluster").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Clusters'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-cluster').mouseover(function () {
                        $('div.btn-group.filtro-cluster').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-cluster').mouseout(function () {
                        $('div.btn-group.filtro-cluster').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroIc / <filtro-ic></filtro-ic>
Estalo.directive('filtroIc', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<div class="multiClassSelector filtro-multi">' +
        ' <ui-select id="filtro-ic" multiple ng-model="filtroEscolhido.ic" theme="bootstrap" limit="3" spinner-enabled="true" close-on-select="false" ' +
        '      title="Itens de controle" title="Escolha uma aplicação">' +
        '   <ui-select-match placeholder="IC\'s">{{$item.codigo}}</ui-select-match> ' +
        '   <ui-select-choices repeat="item.codigo as item in (filtroIcPopulado2 | filter: $select.search | limitTo:20) track by $index"> ' +
        '     <div ng-bind-html="item.codigo | highlight: $select.search"></div> ' +
        '     <small> ' +
        '       {{item.descricao}} ' +
        '     </small> ' +
        '   </ui-select-choices> ' +
        '   </ui-select> ' +
        ' </div> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $scope.filtroICSelect = [];
                    $scope.filtroEscolhido = {};
                    $scope.inputICS = [];
                    $scope.outputICS = [];

                    $scope.carregarICsArquivo = function (arrayAplicacoes) {                        
                        //ICS
                        $scope.filtroEscolhido.ic = [];
                        var __dirname = tempDir3();
                        var pastaIcs = (__dirname + 'ics/');
                        $scope.filtroIcPopulado = [];
                        $scope.icTemp = [];
                        $scope.tempArquivoArray = [];
                        $scope.unique = {};
                        $scope.filtroIcPopulado2 = [];

                        arrayAplicacoes = Array.isArray(arrayAplicacoes) ? arrayAplicacoes : [arrayAplicacoes];
                        if (arrayAplicacoes.length > 0) {
                            arrayAplicacoes.forEach(function (apl) {
                                if (fs.existsSync(__dirname + 'ics/' + apl + '.csv.gz')) {
                                    zlib.unzip(new Buffer(fs.readFileSync(__dirname + 'ics/' + apl + '.csv.gz')), function (err, buffer) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        $scope.tempArquivoArray = buffer.toString().split('\r\n'); 
                                        for (var i = 0; i < $scope.tempArquivoArray.length; i++) {
                                            var linha = $scope.tempArquivoArray[i].split('\t');
                                            // if($scope.icTemp.indexOf(linha[1]) != -1) {
                                            //     $scope.icTemp.push(linha[1]);
                                            //     continue;
                                            // }   
                                            $scope.filtroIcPopulado.push({
                                                codigo: linha[1],
                                                descricao: linha[2],
                                                aplicacao: linha[0]
                                            });                                                
                                        
                                            // $scope.icTemp.push(linha[1]); 
                                        } 
                                      
                                        for (var i in $scope.filtroIcPopulado) {
                                          if (typeof($scope.unique[$scope.filtroIcPopulado[i].codigo]) == "undefined") {
                                            $scope.filtroIcPopulado2.push($scope.filtroIcPopulado[i]);
                                          }
                                          $scope.unique[$scope.filtroIcPopulado[i].codigo] = 0;
                                        }                                     
                                    });			
                                    // MINI QUERY
                                    //IC
                                    if (arrayAplicacoes.length <= 3) {
                    
                                    //LIMITAÇÃO
                                        var tempQuery = "select Cod_Item,Descricao from ItemDeControle WHERE DataInclusao>='" + new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + "01' AND cod_aplicacao='" + apl + "'";
                                        mssqlQueryTedious(tempQuery, function (err, resultado) {
                                            if (err) console.log(err);   
                                            for (var i = 0; i < resultado.length; i++) {												
                                                if(cache.aplicacoes[apl]['itens_controle'] !== undefined){
                                   
                                                    cache.aplicacoes[apl]['itens_controle'].push(
                                                     {
                                                     codigo: resultado[i].Cod_Item.value,
                                                     descricao: resultado[i].Descricao.value,
                                                     aplicacao: apl
                                                     }
                                                     );
                                                     try{
                                                         cache.aplicacoes[apl]['itens_controle'].indice[resultado[i].Cod_Item.value] = {
                                                         codigo: resultado[i].Cod_Item.value,
                                                         descricao: resultado[i].Descricao.value,
                                                         aplicacao: apl
                                                         };
                                                     }catch(ex){
                                                         //console.log(ex);
                                                     }			 
                                                 }
                                            }						
                                        });
                                    }
                                } else {
                                    cache.aplicacoes[apl]['itens_controle'] = [];  
                                   
                                    var tempQuery = "select Cod_Item,Descricao from ItemDeControle WHERE  cod_aplicacao='" + apl + "'";
                                    mssqlQueryTedious(tempQuery, function (err, resultado) {
                                        if (err) console.log(err);
                                        for (var i = 0; i < resultado.length; i++) {
                                            $scope.filtroIcPopulado.push({
                                                codigo: resultado[i].Cod_Item.value,
                                                descricao: resultado[i].Descricao.value,
                                                aplicacao: apl
                                            });
                                        }
                                        for (var i = 0; i < $scope.filtroIcPopulado.length; i++) {
                                            try {
                                                cache.aplicacoes[apl]['itens_controle'].push($scope.filtroIcPopulado[i]);
                                            } catch (ex) { }
                                        }				
                                    });
                                }
                            });
                        };
                    }

                    $scope.$watch('filtroEscolhido.ic', function (newValue, oldValue) {
                        if (Array.isArray(newValue)) {
                            $('.multiClassSelector #filtro-ic [type="search"]').css('display', (newValue.length < 3 ? '' : 'none'));
                            $scope.filtroICSelect = newValue;
                        }
                    });

                    $(".filtro-aplicacao").change(function () {
                        $scope.filtroIcPopulado = [];
                        $scope.filtroICSelect = [];                     
                        if ($(".filtro-aplicacao").val()) {
                            if ($(".filtro-aplicacao").val().length > 0) {
                                $scope.carregarICsArquivo($(".filtro-aplicacao").val());  
                            }
                        }
                       
                       
                    });

                    $scope.filtroIcPopulado = [];                  
                    if ($(".filtro-aplicacao").val()) {
                        if ($(".filtro-aplicacao").val().length > 0) {
                            $scope.carregarICsArquivo($(".filtro-aplicacao").val()); 
                        }
                    }
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroEd / <filtro-ed></filtro-ed>
Estalo.directive('filtroEd', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <div class="multiClassSelector filtro-multi"> '
        + '   <ui-select id="filtro-ed" '
        + '              multiple '
        + '              ng-model="filtroEscolhido.ed" '
        + '              theme="bootstrap" '
        + '              limit="3" '
        + '              spinner-enabled="true" '
        + '              close-on-select="false" '
        + '              title="Estados de diálogo" '
        + '              title="Escolha uma aplicação"> '
        + '     <ui-select-match placeholder="ED\'s">{{$item | limitTo:3 }}</ui-select-match> '
        + '     <ui-select-choices style="width:450px; max-height: 512px" repeat="item in (arrayFiltroEds| filter: $select.search | limitTo:20) track by $index"> '
        + '       <div ng-bind-html="item | highlight: $select.search"></div> '
        + '     </ui-select-choices> '
        + '   </ui-select> '
        + ' </div> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $scope.filtroEDSelect = [];                    
                    $scope.filtroEscolhido = {};                    

                    $scope.carregarEdsArquivo = function (arrayAplicacoes) {
                        //  EDS
                        $scope.filtroEscolhido.ed = [];
                        var __dirname = tempDir3();
                        var pastaEds = (__dirname + 'eds/');
                        $scope.filtroEdPopulado = [];
                        $scope.filtroEdPopulado2 = {};
                        $scope.arrayFiltroEds = [];
                        arrayAplicacoes = Array.isArray(arrayAplicacoes) ? arrayAplicacoes : [arrayAplicacoes];
                        if (arrayAplicacoes.length > 0) {
                            arrayAplicacoes.forEach(function (apl) {
                                if (fs.existsSync(__dirname + 'eds/' + apl + '.csv.gz')) {
                                    zlib.unzip(new Buffer(fs.readFileSync(__dirname + 'eds/' + apl + '.csv.gz')), function (err, buffer) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        var tempArquivoArray = buffer.toString().split('\r\n');
                                        for (var i = 0; i < tempArquivoArray.length; i++) {
                                            var linha = tempArquivoArray[i].split('\t');
                                            $scope.filtroEdPopulado.push({
                                                codigo: linha[1],
                                                nome: linha[2],
                                                aplicacao: linha[0],
                                                exibir: (linha[3] === "S")
                                            });
                    
                                            if ($scope.filtroEdPopulado2[linha[2]]) {
                                                if ($scope.filtroEdPopulado2[linha[2]].indexOf(linha[1]) == -1)
                                                    $scope.filtroEdPopulado2[linha[2]].push(linha[1]);
                                            } else {
                                                $scope.filtroEdPopulado2[linha[2]] = [linha[1]];
                                                $scope.arrayFiltroEds.push(linha[2])
                                            }
                                        }
                                    });
                                    if (arrayAplicacoes.length <= 3) {
                                        //LIMITAÇÃO
                                        var tempQuery = "select Cod_Estado,Nom_Estado, Indic_Exibe from Estado_Dialogo WHERE DataInclusao>='" + new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + "01' AND cod_aplicacao='" + apl + "'";
                                        mssqlQueryTedious(tempQuery, function (err, resultado) {
                                            if (err) console.log(err);
                                            for (var i = 0; i < resultado.length; i++) {
                                                if(cache.aplicacoes[apl]['estados'] !== undefined){
                                                    cache.aplicacoes[apl]['estados'].push({
                                                    codigo: resultado[i].Cod_Estado.value,
                                                    nome: resultado[i].Nom_Estado.value,
                                                    aplicacao: apl,
                                                    exibir : (resultado[i].Indic_Exibe.value === "S")
                                                    });
                                                    
                                                try{				
                                                    cache.aplicacoes[apl]['estados'].indice[resultado[i].Cod_Estado.value] = {
                                                    codigo: resultado[i].Cod_Estado.value,
                                                    nome: resultado[i].Nom_Estado.value,
                                                    aplicacao: apl,
                                                    exibir : (resultado[i].Indic_Exibe.value === "S")
                                                    };
                                                }catch(ex){
                                                    //console.log(ex);
                                                }
                                                }
                                                if ($scope.filtroEdPopulado2[resultado[i].Nom_Estado.value]) {
                                                    if ($scope.filtroEdPopulado2[resultado[i].Nom_Estado.value].indexOf(resultado[i].Cod_Estado.value) == -1)
                                                        $scope.filtroEdPopulado2[resultado[i].Nom_Estado.value].push(resultado[i].Cod_Estado.value);
                                                } else {
                                                    $scope.filtroEdPopulado2[resultado[i].Nom_Estado.value] = [resultado[i].Cod_Estado.value];
                                                    $scope.arrayFiltroEds.push(resultado[i].Nom_Estado.value);
                                                }
                                            }
                                            $scope.arrayFiltroEds.sort();
                                        });
                                            
                                        }
                                } else {				
                                    cache.aplicacoes[apl]['estados'] = [];                                
                                    var tempQuery = "select Cod_Item,Descricao from ItemDeControle WHERE  cod_aplicacao='" + apl + "'";
                                    mssqlQueryTedious(tempQuery, function (err, resultado) {
                                        if (err) console.log(err);
										if($scope.filtroIcPopulado === undefined) $scope.filtroIcPopulado = [];
                                        for (var i = 0; i < resultado.length; i++) {											
                                            $scope.filtroIcPopulado.push({
                                                codigo: resultado[i].Cod_Item.value,
                                                descricao: resultado[i].Descricao.value,
                                                aplicacao: apl
                                            });
                                        }
                    
                                        var tempQuery = "select Cod_Estado,Nom_Estado, Indic_Exibe from Estado_Dialogo WHERE cod_aplicacao='" + apl + "'";
                                        console.log('temp', tempQuery)
                                        mssqlQueryTedious(tempQuery, function (err, resultado) {
                                            if (err) console.log(err);
                                            for (var i = 0; i < resultado.length; i++) {
                                                $scope.filtroEdPopulado.push({
                                                    codigo: resultado[i].Cod_Estado.value,
                                                    nome: resultado[i].Nom_Estado.value,
                                                    aplicacao: apl,
                                                    exibir: (resultado[i].Indic_Exibe.value === "S")
                                                });
                    
                                                if ($scope.filtroEdPopulado2[resultado[i].Nom_Estado.value]) {
                                                    if ($scope.filtroEdPopulado2[resultado[i].Nom_Estado.value].indexOf(resultado[i].Cod_Estado.value) == -1)
                                                        $scope.filtroEdPopulado2[resultado[i].Nom_Estado.value].push(resultado[i].Cod_Estado.value);
                                                } else {
                                                    $scope.filtroEdPopulado2[resultado[i].Nom_Estado.value] = [resultado[i].Cod_Estado.value];
                                                    $scope.arrayFiltroEds.push(resultado[i].Nom_Estado.value);
                                                }
                    
                                                $scope.arrayFiltroEds.sort();
                                            }					
                                            for (var i = 0; i < $scope.filtroEdPopulado.length; i++) {
                                                try {
                                                    cache.aplicacoes[apl]['estados'].push($scope.filtroEdPopulado[i]);
                                                } catch (ex) { }
                                            }
                                        });
                                    });
                                }
                                
                            });
                        };
                    }
                  
                    $scope.$watch('filtroEscolhido.ed', function (newValue, oldValue) {
                        if (Array.isArray(newValue)) {
                            $scope.filtroEDSelect = [];
                            $('.multiClassSelector #filtro-ed [type="search"]').css('display', (newValue.length < 3 ? '' : 'none'));
                            for (var i = 0; i < newValue.length; i++) {
                                for (var j = 0; j < $scope.filtroEdPopulado2[newValue[i]].length; j++) {
                                    $scope.filtroEDSelect.push($scope.filtroEdPopulado2[newValue[i]][j])
                                }
                            }
                            if ($scope.filtroEDSelect.length != 1) {
                                $scope.filtros.isUltimoED = false;
                            }
                        }
                    });

                    $(".filtro-aplicacao").change(function () {                       
                        $scope.filtroEdPopulado = [];
                        $scope.filtroEdPopulado2 = {};
                        $scope.arrayFiltroEds = [];
                        if ($(".filtro-aplicacao").val()) {
                            if ($(".filtro-aplicacao").val().length > 0) {
                                $scope.carregarEdsArquivo($(".filtro-aplicacao").val());
                            }
                        }
                    });
                    
                    $scope.filtroEdPopulado = [];
                    $scope.filtroEdPopulado2 = {};
                    $scope.arrayFiltroEds = [];
                    if ($(".filtro-aplicacao").val()) {
                        if ($(".filtro-aplicacao").val().length > 0) {
                            $scope.carregarEdsArquivo($(".filtro-aplicacao").val());
                        }
                    }
                })
            })
        }
    }
})

//{ Diretivas de filtroIc / <filtro-tag></filtro-tag>
Estalo.directive('filtroTag', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<div class="multiClassSelector filtro-multi">' +
        ' <ui-select id="filtro-tag" multiple ng-model="filtroEscolhido.tag" theme="bootstrap" limit="3" spinner-enabled="true" close-on-select="false" ' +
        '      title="Tag" title="Escolha uma aplicação">' +
        '   <ui-select-match placeholder="TAG\'s">{{$item.codigo}}</ui-select-match> ' +
        '   <ui-select-choices repeat="item.codigo as item in (filtroTagPopulado | filter: $select.search | limitTo:20) track by $index"> ' +
        '     <div ng-bind-html="item.codigo | highlight: $select.search"></div> ' +
        '     <small> ' +
        '       {{item.descricao}} ' +
        '     </small> ' +
        '   </ui-select-choices> ' +
        '   </ui-select> ' +
        ' </div> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $scope.filtroTAGSelect = [];
                    $scope.filtroEscolhido = {};
                    $scope.inputTag = [];
                    $scope.outputTags = [];

                    $scope.carregarTAGsArquivo = function (arrayAplicacoes) {                        
                        //ICS
                        $scope.filtroEscolhido.tag = [];
                        var __dirname = tempDir3();
                        var pastaIcs = (__dirname + 'ics/');
                        $scope.filtroTagPopulado = [];
                        $scope.TagTemp = [];
                        $scope.tempArquivoArray = [];
                        $scope.unique = {};
                        $scope.filtroTagPopulado2 = [];
                        var tempQuery = "";

                        arrayAplicacoes = Array.isArray(arrayAplicacoes) ? arrayAplicacoes : [arrayAplicacoes];
                        if (arrayAplicacoes.length > 0) {
                            arrayAplicacoes.forEach(function (apl) {
                                    cache.aplicacoes[apl]['tags'] = [];  
                                   
                                  //  tempQuery = "select Nometag ,Descricao from Tag WHERE  CodAplicacao='" + apl + "'";
                                    tempQuery = "select distinct Nometag ,Descricao from Tag order by nomeTag";

                                    //console.log(tempQuery)
                                    mssqlQueryTedious(tempQuery, function (err, resultado) {
                                        if (err) console.log(err);
                                        for (var i = 0; i < resultado.length; i++) {
                                            $scope.filtroTagPopulado.push({
                                                codigo: resultado[i].Nometag.value,
                                                descricao: resultado[i].Descricao.value,
                                                aplicacao: apl
                                            });
                                        }
                                        for (var i = 0; i < $scope.filtroTagPopulado.length; i++) {
                                            try {
                                                cache.aplicacoes[apl]['tags'].push($scope.filtroTagPopulado[i]);
                                            } catch (ex) { }
                                        }				
                                    });
                                
                            });
                        };
                    }

                    $scope.$watch('filtroEscolhido.tag', function (newValue, oldValue) {
                        if (Array.isArray(newValue)) {
                            $('.multiClassSelector #filtro-tag [type="search"]').css('display', (newValue.length < 3 ? '' : 'none'));
                            $scope.filtroTAGSelect = newValue;
                        }
                    });

                    $(".filtro-aplicacao").change(function () {
                        $scope.filtroTagPopulado = [];
                        $scope.filtroTAGSelect = [];                     
                        if ($(".filtro-aplicacao").val()) {
                            if ($(".filtro-aplicacao").val().length > 0) {
                                $scope.carregarTAGsArquivo($(".filtro-aplicacao").val());  
                            }
                        }
                       
                       
                    });

                    $(".filtro-ics-repetidas").change(function () {
                      var ics = $(".filtro-ics-repetidas").val();
                      var apls = [];
                      for (var i=0 ; i< ics.length; i++) {
                        for (var x=0; x< cache.icsRepetidas.length ; x++){
                            if (ics[i] == cache.icsRepetidas[x].CodIc) apls.push(cache.icsRepetidas[x].CodAplicacao);
                        }
                      }
                      $scope.filtroTagPopulado = [];
                      $scope.filtroTAGSelect = [];  
                      if (apls.length > 0) $scope.carregarTAGsArquivo(apls); 
                    });

                    $scope.filtroTagPopulado = [];                  
                    if ($(".filtro-aplicacao").val()) {
                        if ($(".filtro-aplicacao").val().length > 0) {
                            $scope.carregarTAGsArquivo($(".filtro-aplicacao").val()); 
                        }
                    }
                })
            })
        }
    }
})
//}
//}

// Estalo.run(function($rootScope) {
//   $rootScope.$on("$routeChangeStart", function($event, next, current) {
//       // handle route changes
//       console.log($event);
//       console.log(next);
//       console.log(current);
//       // var myString = "<div id=\"pag-chamadas\" data-role=\"page\">fgthdghyfd";
//       // var myRegexp = /(?:<div id=")(.*?)(?:" data-role="page">)/g;
//       // var match = myRegexp.exec(myString);
//       // console.log(match[1]); // abc
//       $view = $("#pag-chamadas");
//   });
// });

//{ Diretivas de filtroEdsTransf / <filtro-eds-transf><filtro-eds-transf/>
Estalo.directive('filtroEdsTransf', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkEDTr" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-edstransf selectpicker span1_3" multiple="" title="EDs" > </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-edstransf").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} EDs'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-edstransf').mouseover(function () {
                        if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                            $('div.btn-group.filtro-edstransf').addClass('open');
                            $('div.dropdown-menu.open').css({
                                'margin-left': '-3px'
                            });
                            $('div.btn-group.filtro-edstransf>div>ul').css({
                                'max-height': '500px',
                                'overflow-y': 'auto',
                                'min-height': '1px',
                                'max-width': '350px'
                            });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-edstransf').mouseout(function () {
                        $('div.btn-group.filtro-edstransf').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroSkills / <filtro-skills><filtro-skills/>
Estalo.directive('filtroSkills', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkSkill" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a>' +
        '<select class="filtro-skill selectpicker span1_3" multiple="" title="Skills" data-live-search="true" data-size="20"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-skill").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Skills'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-skill').mouseover(function () {
                        $('div.btn-group.filtro-skill').addClass('open');
						$('div.btn-group.filtro-skill>div>ul').css({
                                'max-height': '400px',
                                'overflow-y': 'auto',
                                'min-height': '1px',
                                'max-width': '400px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-skill').mouseout(function () {
                        $('div.btn-group.filtro-skill').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroProdutotu / <filtro-produtotu><filtro-produtotu/>
Estalo.directive('filtroProdutostu', function () {
    return {
        restrict: 'AEC',
        replace: true,
        
        template: ' <form class="navbar-form">' +
        '<div class="input-append">' +   
        // '  <a class="btn btn-small checkAll2" id="alinkSite" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a> ' +     
        '  <select class="filtro-produtotu selectpicker span1_3" multiple="" title="Produtos TU"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-produtotu").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Produtos TU'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-produtotu').mouseover(function () {
                        $('div.btn-group.filtro-produtotu').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-produtotu').mouseout(function () {
                        $('div.btn-group.filtro-produtotu').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de radioEmpresaRegra / <radio-empresa-regra/>
Estalo.directive('radioEmpresaRegra', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<input type="radio"  ng-model="valor" name="radioB" value="empresa"><span class = "pordia">Empresa</span></input>' +
        '<!--<input type="radio" ng-model="valor" name="radioB" value="destino"><span class = "pordia">Skill</span>-->' +
        '<input type="radio" ng-model="valor" name="radioB" value="regra"><span class = "pordia">Regra</span></input>' +
        '</form>'
    }
})
//}

//{ Diretivas de radioHoraDiaMesEmpresaDestino / <radio-hora-dia-mes-empresa-destino/>
Estalo.directive('radioHoraDiaMesEmpresaDestino', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo">' +
        '<div style="display: table">' +
        '<div class="checkbox navbar-btn filtro-check" style="display: table-cell;">' +
        '<input type="radio" value="hora" name="radioPeriodo" ng-model="filtros.periodo" class="porhora">Por Hora</input>' +
        '</div>'

        +
        '<div class="checkbox navbar-btn filtro-check" style="display: table-cell;">' +
        '<input type="radio" value="dia" name="radioPeriodo" ng-model="filtros.periodo" class="pordia">Por dia</input>' +
        '</div>'

        +
        '<div class="checkbox navbar-btn filtro-check" ng-show="valor==\'regra\'" style="display: table-cell;">' +
        '<input type="radio" value="mes" name="radioPeriodo" ng-model="filtros.periodo" class="pormes">Por Mês</input>' +
        '</div>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxOperacaoDois / <chk-box-operacao-dois></chk-box-operacao-dois>
Estalo.directive('chkBoxOperacaoDois', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.porOper2" class="por-oper2">Por operação 2</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxNlu / <chk-box-nlu></chk-box-nlu>
Estalo.directive('chkBoxNlu', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<input type="checkbox" name="reclamacao" value="exibeReclamacao" id= "reclamacao">' +
        '<span style="color:white">Reclamação    </span>' +	
        '<input type="checkbox" name="contas" value="exibeContas" id = "contas">' +
		'<span style="color:white">Conta    </span>' +	
        '<input type="checkbox" name="fibra" value="exibeFibraRep" id = "fibra">' +
		'<span style="color:white">Repetidas Fibra    </span>' +
		'<input type="checkbox" name="atendente" value="exibeNLU" id = "atendente">' +
		'<span style="color:white">Atendente    </span>' +
		'<input type="checkbox" name="todos" value="exibePadrao" checked id = "todos">' +
		'<span style="color:white">Todas    </span>' +	
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxNlu / <chk-box-tag></chk-box-tag>
Estalo.directive('chkBoxTag', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form radio-responsivo" style="color: white">' +
        '<input type="checkbox" name="atendente" ng-model="filtro_atendente" value="exibeAtendente" id="atendente">' +
        '<span style="color:white">Atendente&nbsp</span>' +	
        '<input type="checkbox" name="reclamacao" ng-model="filtro_reclamacao" value="exibeReclamacao" id="reclamacao">' +
        '<span style="color:white">Reclamação&nbsp</span>' +	
        '<input type="checkbox" name="contas" ng-model="filtro_contas" value="exibeContas" id="contas">' +
		'<span style="color:white">Contas&nbsp</span>' +	
        '<input type="checkbox" name="fibra" ng-model="filtro_fibra" value="exibeFibraRep" id="fibra">' +
		'<span style="color:white">Repetidas&nbsp</span>' +
		'<input type="checkbox" name="todos" ng-model="filtro_todos" value="exibePadrao" checked id="todos">' +
		'<span style="color:white">Todas&nbsp</span>' +	
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxPorDiaRecarga / <chk-box-por-dia-recarga></chk-box-por-dia-recarga>
Estalo.directive('chkBoxPorDiaRecarga', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form" style="color:white">' +
        '<input type="checkbox" id="chkDia" checked> Por Dia</input>' +
        '<input type="checkbox" id="chkRecarga" checked> Por Canal de Recarga</input>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxConsol / <chk-box-consol></chk-box-consol>
Estalo.directive('chkBoxConsol', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form" style="color:white">' +
		//'<input type="checkbox" id="chkResposta3" title="Resposta 3 válida"> Resp 3 </input>' +
        '<input type="checkbox" id="chkConsol" title="Visão consolidada"> Consol</input>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxUmDigito / <chk-box-um-digito/>
Estalo.directive('chkBoxUmDigito', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="checkbox navbar-btn filtro-check">' +
        '   <input type="checkbox" ng-model="umDigito" value="umDigito" class="umDigito" id="umDigito" checked>Um Digito</input>' +
        ' </div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxUmDia / <chk-box-um-dia/>
Estalo.directive('chkBoxUmDia', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="checkbox navbar-btn filtro-check">' +
        '   <input type="checkbox" ng-model="umDigito" value="umDigito" class="umDigito" id="umDigito" checked>Um Digito</input>' +
        ' </div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxPeriodo / <chk-box-periodo></chk-box-periodo>
Estalo.directive('chkBoxPeriodo', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<input type="radio" name="tela" value="0">' +
		'<span style="color:white"> Hora </span>' +
		'<input type="radio" name="tela" value="1" checked>' +
		'<span style="color:white"> Dia </span>' +
		'<input type="radio" name="tela" value="2">' +
		'<span style="color:white"> Mês </span>' +	
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxPorDia / <chk-box-por-dia/>
Estalo.directive('chkBoxPorDia', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="checkbox navbar-btn filtro-check"> ' +
        '     <input type="checkbox" ng-model="filtros.porDia"  value="pordia" class="pordia"> Por dia </input> ' +
        '   </div> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de chkBoxOperador / <chk-box-operador/>
Estalo.directive('chkBoxOperador', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="checkbox navbar-btn filtro-check"> ' +
        '     <input type="checkbox" ng-model="operador"  value="poroperador" class="pordia"> Operador </input> ' +
        '   </div> ' +
        ' </form> '
    }
})
//}

//{ Diretivas de chkBoxPorDdd / <chk-box-por-ddd/>
Estalo.directive('chkBoxPorDdd', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.porDdd" value="porddd" class="porddd"> DDD</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxPorIit / <chk-box-por-iit/>
Estalo.directive('chkBoxPorIit', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" ng-model="filtros.porIit" value="iit" class="iit"> IIT</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxTresMil / <chk-box-tres-mil/>
Estalo.directive('chkBoxTresMil', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="checkbox navbar-btn filtro-check">' +
        '   <input type="checkbox" id="tresmil" ng-model="tresmil" value="tresmil" class="pordia">3000</input>' +
        ' </div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxOitoOito / <chk-box-oito-oito/>
Estalo.directive('chkBoxOitoOito', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="checkbox navbar-btn filtro-check">' +
        '   <input type="checkbox" id="oitooito" ng-model="oitooito" value="oitooito" class="pordia">880 (ANTIGA)</input>' +
        ' </div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDadosPessoais / <chk-box-dados-pessoais/>
Estalo.directive('chkBoxDadosPessoais', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="dadosP" value="dadosP" ng-model="dadosP" title="Dados pessoais" >Dados cadastrais</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDadosBoleto / <chk-box-dados-boleto/>
Estalo.directive('chkBoxDadosBoleto', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="dadosBOLETO" value="dadosBOLETO" ng-model="dadosBOLETO" title="Envio de boleto" >BOLETO</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDadosContestacao / <chk-box-dados-contestacao/>
Estalo.directive('chkBoxDadosContestacao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="dadosCONTESTACAO"  value="dadosCONTESTACAO" ng-model="dadosCONTESTACAO" title="Envio de Contestacao" >CONTESTAÇÃO</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de chkBoxDadosSms / <chk-box-dados-sms/>
Estalo.directive('chkBoxDadosSms', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        '<div class="checkbox navbar-btn filtro-check">' +
        '<input type="checkbox" id ="dadosSMS"  value="dadosSMS" ng-model="dadosSMS" title="Envio de SMS" >SMS</input>' +
        '</div>' +
        '</form>'
    }
})
//}

//{ Diretivas de sliderConfianca / <slider-confianca></slider-confianca>
Estalo.directive('sliderConfianca', function () {
    return {
        restrict: 'AEC',
        template: '<div id="proRangeSlider" ng-show=\'confianca\'>' +
        '<label class="icon-comment icon-white" title="Intervalo de confiança do reconhecimento de voz"></label>' +
        '<input type="range" name="costPerDay" class="range orange" id="rangeSlider1" value="0" oninput="proRangeSlider(this.id, \'output1\', \'orange\')">' +
        '<output id="output1" ng-show=\'confianca\'>0</output>' +
        '</div>'
    };
});
//}

//{ Diretivas de filtroContaOnline / <filtro-conta-online></filtro-conta-online>
Estalo.directive('filtroContaOnline', function () {
    return {
        restrict: 'AEC',
        template: '<form class="navbar-form">' +
        '<div class="input-append">' +
        '<a class="btn btn-small checkAll2" id="alinkCline" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '<select class="filtro-contaonline selectpicker span1_3" multiple="" title="Conta online"></select>' +
        '</div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-contaonline').mouseover(function () {
                        // Não mostrar filtros desabilitados
                        if ($('.filtro-contaonline').attr('disabled') == undefined) {
                            $('.filtro-contaonline').addClass('open');
                            // $('.filtro-contaonline>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
                        }
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-contaonline').mouseout(function () {
                        $('.filtro-contaonline').removeClass('open');
                    });
                });
            });
        }
    };
});
//}

//{ Diretivas de grupoBotoesRelatorio / <grupo-botoes-relatorio/>
Estalo.directive('grupoBotoesRelatorio', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <ul class="nav botoes"> ' +
        '   <li style="padding-left:5px"> ' +
        '    <form class="navbar-form"> ' +
        '       <button class="btn btn-agora" data-loading-text="Hora atual..." title="Recuperar hora atual">Agora</button> ' +
        '     </form> ' +
        '   </li> ' +
        '   <li style="padding-left:5px"> ' +
        '     <form class="navbar-form"> ' +
        '       <button class="btn btn-limpar-filtros" data-loading-text="..." title="Limpa todos os filtros selecionados">Limpar filtros</button> ' +
        '     </form> ' +
        '   </li> ' +
        '   <li style="padding-left:5px"> ' +
        '     <form class="navbar-form"> ' +
        '       <button class="btn btn-gerar" data-loading-text="...">Carregar</button> ' +
        '     </form> ' +
        '   </li> ' +
        '   <li style="padding-left:5px"> ' +
        '     <form class="navbar-form"> ' +
        '       <button class="btn btn-exportar" data-loading-text="..." disabled="">Exportar</button> ' +
        '     </form> ' +
        '   </li> ' +
        '   <li> ' +
        '     <form class="navbar-form" id="btnAjuda" style="position: absolute; right: -30px;"> ' +
        '   <button class="btn" ng-click="abreAjuda(0)" title="Abre uma página com manuais de ajuda." style="padding: 4px 4px;"><font size="small"><i class="icon-question-sign"></i></font></button> ' +
        '     </form> ' +
        '   </li> ' +
        ' </ul>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {

                })
            })
        },
    }
})
//}

//{ Diretivas de operacao / <operacao><operacao/>
Estalo.directive('operacao', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkOper" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-operacao selectpicker span1_3" multiple="" title="Operações" ></select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-operacao").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Operações',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-operacao').mouseover(function () {
                        $('div.btn-group.filtro-operacao').addClass('open');
                        $('div.dropdown-menu.open').css({
                            'margin-left': '-3px'
                        });
                        $('div.btn-group.filtro-operacao>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '350px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-operacao').mouseout(function () {
                        $('div.btn-group.filtro-operacao').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{diretiva categoryHeader
Estalo.directive('categoryHeader', function () {
    function link(scope, element, attrs) {
        // create cols as soon as $gridscope is avavilable
        // grids in tabs with lazy loading come later, so we need to
        // setup a watcher
        scope.$watch('categoryHeader.$gridScope', function (gridScope, oldVal) {
            if (!gridScope) {
                return;
            }
            // setup listener for scroll events to sync categories with table
            var viewPort = scope.categoryHeader.$gridScope.domAccessProvider.grid.$viewport[0];
            var headerContainer = scope.categoryHeader.$gridScope.domAccessProvider.grid.$headerContainer[0];

            // watch out, this line usually works, but not always, because under certains conditions
            // headerContainer.clientHeight is 0
            // unclear how to fix this. a workaround is to set a constant value that equals your row height
            scope.headerRowHeight = headerContainer.clientHeight;

            angular.element(viewPort).bind("scroll", function () {
                // copy total width to compensate scrollbar width
                $(element).find(".categoryHeaderScroller")
                    .width($(headerContainer).find(".ngHeaderScroller").width());
                $(element).find(".ngHeaderContainer")
                    .scrollLeft($(this).scrollLeft());
            });

            // setup listener for table changes to update categories
            scope.categoryHeader.$gridScope.$on('ngGridEventColumns', function (event, reorderedColumns) {
                createCategories(event, reorderedColumns);
            });
        });
        var createCategories = function (event, cols) {
            scope.categories = [];
            var lastDisplayName = "";
            var totalWidth = 0;
            var left = 0;
            angular.forEach(cols, function (col, key) {
                if (!col.visible) {
                    return;
                }
                totalWidth += col.width;
                var displayName = typeof (col.colDef.categoryDisplayName) === "undefined" ? "\u00A0" : col.colDef.categoryDisplayName;
                if (displayName !== lastDisplayName) {
                    scope.categories.push({
                        displayName: lastDisplayName,
                        width: totalWidth - col.width,
                        left: left
                    });
                    left += (totalWidth - col.width);
                    totalWidth = col.width;
                    lastDisplayName = displayName;
                }
            });
            if (totalWidth > 0) {
                scope.categories.push({
                    displayName: lastDisplayName,
                    width: totalWidth,
                    left: left
                });
            }
        };
    }
    return {
        scope: {
            categoryHeader: '='
        },
        restrict: 'EA',
        templateUrl: base + 'category_header.html',
        link: link
    };
});
//}

//{ Diretivas de grupo / <grupo><grupo/>
Estalo.directive('grupo', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form"> ' +
        '   <div class="input-append"> ' +
        '     <a class="btn btn-small checkAll2" id="alinkGrupo" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-grupo selectpicker span1_3" title="Grupos" > ' +
        '       <option ng-repeat="grupo in gruposTeste">{{ grupo.nome }}</option>' +
        '     </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $scope.gruposTeste = [{
                        nome: 'Ações Informativas'
                    },
                    {
                        nome: 'Cobranca'
                    }
                    ]
                    $("select.filtro-empresa").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Grupos'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-grupo').mouseover(function () {
                        $('div.btn-group.filtro-grupo').addClass('open');
                        $('div.dropdown-menu.open').css({
                            'margin-left': '-3px'
                        });
                        $('div.btn-group.filtro-grupo>div>ul').css({
                            'max-height': '500px',
                            'overflow-y': 'auto',
                            'min-height': '1px',
                            'max-width': '350px'
                        });
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-grupo').mouseout(function () {
                        $('div.btn-group.filtro-grupo').removeClass('open');
                    });
                })
            })
        }
    }
})
//}

//{ Diretivas de filtroGrupos / <filtro-grupos></filtro-grupos>
Estalo.directive('filtroGrupos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: ' <form class="navbar-form">  ' +
        '   <div class="input-append">  ' +
        '     <a class="btn btn-small checkAll2" id="alinkGru" data-var="0" title="Clique para marcar todos" ><i class="icon-ok"></i></a> ' +
        '     <select class="filtro-grupo selectpicker span1_3" multiple="" title="Grupos" ng-model="gruposSelecionados" > ' +
        '       <option>Ações Informativas</option> ' +
        '       <option>Ações Interativas</option> ' +
        '       <option>CO Digital</option> ' +
        '       <option>Cobranca</option> ' +
        '       <option>Fixo Controle</option> ' +
        '       <option>Mercado Informativo</option> ' +
        '       <option>Mercado Interativo</option> ' +
        '       <option>Pesquisas DRC</option> ' +
        '       <option>Pesquisas Eletrônicas</option> ' +
        '       <option>Pesquisas NPS</option> ' +
        '       <option>Recarga</option> ' +
        '       <option>Venda Pacote</option> ' +
        '     </select> ' +
        '   </div> ' +
        ' </form> ',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-grupo").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} Grupos'
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-grupo').mouseover(function () {
                        $('div.btn-group.filtro-grupo').addClass('open');
                    });
                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-grupo').mouseout(function () {
                        $('div.btn-group.filtro-grupo').removeClass('open');
                    });
                    $('div.filtro-grupo').hover(function () {
                        $('select.filtro-grupo').selectpicker('refresh');
                    })
                })
            })
        },
    }
})
//}

//{ Diretivas de radioGenerico / <radio-generico ngm="ng-model" l="24h, 2h, 1h" v="24, 2, 1"></radio-generico>
Estalo.directive('radioGenerico', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: function (elem, attr) {
            var t = '<form class="navbar-form radio-responsivo" style="color: white">';
            var arrayItens = attr.l.split(/,\s*/);
            var arrayVal = attr.v.split(/,\s*/);
            for (var i = 0; i < arrayItens.length; i++) {
                t += '<input type="radio" id="' + arrayItens[i] + '" ng-model="' + attr.ngm + '" value="' + arrayVal[i] + '">' + arrayItens[i] + ' </input>';
            }
            t += '</form>';
            return t;
        }
    }
})

//{ Diretivas de filtroAssuntos / <filtro-Assuntos></filtro-Assuntos>
Estalo.directive('filtroAssuntos', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<form class="navbar-form">' +
        ' <div class="input-append div-assunto">' +
        '   <a class="btn btn-small checkAll2" id="alinkAssunto" data-var="0" title="Clique para marcar todos"><i class="icon-ok"></i></a>' +
        '   <select class="filtro-assunto selectpicker span1_2" multiple="" title="Assuntos"> </select>' +
        ' </div>' +
        '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {
                    $("select.filtro-assunto").selectpicker({
                        selectedTextFormat: 'count',
                        countSelectedText: '{0} assuntos',
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-assunto').mouseover(function () {
                        if ($('.div-assunto') != undefined) {

                            if ($('.div-assunto').offset().left + 234 > $('body').width()) {
                                $('div.btn-group.filtro-assunto>div').css({
                                    'left': '-95px',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-assunto>div>ul').css({
                                    'left': '-95px',
                                    'overflow-y': 'auto',
                                    'max-width': '252px',
                                    'max-height': '450px'
                                })
                            } else {
                                $('div.btn-group.filtro-assunto>div').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-assunto>div>ul').css({
                                    'left': '0',
                                    'overflow-y': 'auto',
                                    'max-width': '252px',
                                    'max-height': '450px'
                                })
                            }
                        } else {
                            if ($('.div-assunto').position().left + 234 > $('body').width()) {
                                $('div.btn-group.filtro-assunto>div').css({
                                    'left': '-95px',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-assunto>div>ul').css({
                                    'left': '-95px',
                                    'overflow-y': 'auto',
                                    'max-width': '252px',
                                    'max-height': '450px'
                                })
                            } else {
                                $('div.btn-group.filtro-assunto>div').css({
                                    'left': '0',
                                    'max-width': '252px'
                                })
                                $('div.btn-group.filtro-assunto>div>ul').css({
                                    'left': '0',
                                    'overflow-y': 'auto',
                                    'max-width': '252px',
                                    'max-height': '450px'
                                })
                            }
                        }
                        $('div.btn-group.filtro-assunto').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-assunto').mouseout(function () {
                        $('div.btn-group.filtro-assunto').removeClass('open');
                    });
                });
            });
        }
    }
})
//}

//{ Diretivas de filtro-movel-fixo / <filtro-Movel-Fixo></filtro-Movel-Fixo>
Estalo.directive('filtroMovelFixo', function () {
    return {
        restrict: 'AE',
        replace: true,
        template: '<form class="navbar-form">'
        + ' <div class="input-append">'
        /*+ ' <a class="btn btn-small checkAll2" id="alinkMovelFixo" data-var="0" title="Clique para marcar todos">'
        +	 '<i class="icon-ok"></i>'
        + 	'</a>'*/
        + '   <select class="filtro-movel-fixo selectpicker span1_2" multiple="" title="Tipo">'
        + '     <option value="MOVEL">Móvel</option>'
        + '     <option value="FIXO">Fixo</option>'
        + '</select>'
        + '</div>'
        + '</form>',
        controller: function ($scope) {
            $(function () {
                $scope.$on('$viewContentLoaded', function () {

                    $("select.filtro-movel-fixo").selectpicker({
                        showSubtext: true
                    });
                    // EXIBIR AO PASSAR O MOUSE
                    $('.filtro-movel-fixo').mouseover(function () {
                        $('div.btn-group.filtro-movel-fixo').addClass('open');
                    });

                    // OCULTAR AO TIRAR O MOUSE
                    $('.filtro-movel-fixo').mouseout(function () {
                        $('div.btn-group.filtro-movel-fixo').removeClass('open');
                    });
                });
            });
        }
    }
})

//{ Diretivas de estiloResponsivo / <estilo-responsivo>
Estalo.directive('estiloResponsivo', function () {
    return {
        restrict: 'AEC',
        replace: true,
        template: '<style type="text/css">' +
        '* { border-radius: 0 !important; }' +
        '.ultMes, #alinkAnt, #alinkPro:hover { cursor: pointer; }' +
        '#btnHelp { top: 5px; }' +
        '.hideMe { display: none; }' +
        '.pag-container { display: flex; flex-direction: column; min-height: 100%; }' +
        '.menu-barra { display: flex; flex-direction: row; flex-wrap: no-wrap; }' +
        '.menu-aplicacoes { background-color: #222; min-width: 50px; }' +
        '.menu-filtros { flex-grow: 1; display: flex; flex-direction: row; flex-wrap: wrap; justify-content: flex-start; background: #222; }' +
        '.menu-filtros > * { margin-right: 5px; margin-bottom: 0; }' +
        '.menu-checkbox { flex-grow: 1; display: flex; flex-direction: row; align-items: center; justify-content: flex-end; padding-bottom: 5px; }' +
        '.menu-checkbox > * { margin-right: 5px; }' +
        '.menu-botoes { display: flex; flex-direction: row; align-items: center; justify-content: flex-end; flex-grow: 1; padding-bottom: 5px; }' +
        '.menu-botoes > * { margin-right: 5px; }' +
        '.menu-versao { min-width: 50px; display: flex; background: #222; color: #FFF; padding-top: 5px; box-sizing: border-box; text-align: center; font-size: 9px; }' +
        '.grid-container { flex-grow: 1; padding: 15px; box-sizing: border-box; }' +
        '.alert-container { width: 100%; text-align: center; }' +
        '.input-append { margin-bottom: 0; }' +
        '.input-prepend { margin-top: 5px; }' +
        '.grid { margin-top: 0; }' +
        '.multiClassSelector, .form-control { padding: 0 !important; }' +
        '.form-control { height: 28px !important; }' +
        '.navbar-form .checkbox { margin-top: 0 !important; }' +
        '.input-number > input { height: 28px !important; padding-left: 2px !important; }' +
        '.filtro-multi { margin-top: 5px; margin-bottom: 5px; }' +
        '.radio-responsivo { border: 1px solid #FFF; display: flex; color: #FFF; }' +
        '.dropdown-exportar { position: absolute; background: #FFF; box-shadow: 2px 2px 6px #333; margin-top: 1px; border: 1px solid #DDD; box-sizing: border-box; padding: 5px; z-index: 2; }' +
        '.dropdown-exportar button { width: 100%; }' +
        'filtro-chamadores .filtro-chamador { height: 28px }' +
        'filtro-json form,' +
        'filtro-json div.input-append div.btn-group,' +
        'filtro-json button { width: 115px; }' +
        '</style>',
    }
})
//}
