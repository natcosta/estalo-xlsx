function CtrlHomologacaoPrompt($scope, $globals) {

	win.title="Homologação de Prompts"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;
	$scope.prompt = [];
	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;


	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-homologacao-prompt", "Homologação de Prompts");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;
	$scope.codigoAudio = "";
	$scope.dados = [];


	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};
	//$scope.ordenacao = 'data_hora';
	//$scope.decrescente = true;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy


	// Filtros
	$scope.filtros = {
		aplicacoes: []
	};



	$scope.aba = 2;
	var distinctEmpresas = [];


	//var empresas = cache.empresas.map(function(a){return a.codigo});




	function geraColunas(){

	  var array = [];

	  array.push(
		{ field: "datCadastro", displayName: "Data", width: 120, pinned: true },
		{ field: "codPrompt", displayName: "Código", width: 250, pinned: true },
		{ field: "codAplicacao", displayName: "Cód. Aplicação", width: 120, pinned: true },
		{ field: "descricao", displayName: "Descrição", width: 770, pinned: true },
		{ field: "homologado", visible: false},
		{ field: "coerente", visible: false},
		{ field: "codigoAudio", visible: false},
		//{ field: "acao", displayName: "Gerenciar", width: 70, pinned: true }
		{ displayName: 'Ações', cellTemplate: 
		'<div class="grid-action-cell">'+
		'<span ng-cell-text data-toggle="modal" data-target="#audioModal"><a href="#modal-log" target="_self" data-toggle="modal" ng-click="abrirModalAudio(row.entity)" class="icon-edit"></a></span></div>'}
	  );

  

	  return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

		$scope.periodo = {
			inicio:inicio,
			fim: fim,
			min: new Date(2013, 11, 1),
			max: agora // FIXME: atualizar ao virar o dia
			/*min: ontem(dat_consoli),
			max: dat_consoli*/
		};

	var $view;


	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-homologacao-prompt");
		  cache.carregaRecVoz();

		carregaAplicacoes($view,false,false,$scope);
		
		  

	  $view.on("change","input:radio[name=radioB]",function() {

		if($(this).val() === "empresa"){
		  $('#porDDD').attr('disabled', 'disabled');
		  $('#porDDD').prop('checked',false);
		}else{
		  $('#porDDD').attr('disabled', false);
		}

	  });


		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


		// GILBERTO 18/02/2014



		// OCULTAR AO TIRAR O MOUSE estado de dialogo
		$('div.btn-group.filtro-ed').mouseout(function () {
			$('div.btn-group.filtro-ed').removeClass('open');
		});


		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
		  $('.filtro-codprompt').val("");
		  $scope.limparFiltros.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
	
		$scope.limparFiltros = function () {
			iniciaFiltros();
			$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
			$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
			componenteDataHora ($scope,$view,true);

			var partsPath = window.location.pathname.split("/");
			var part  = partsPath[partsPath.length-1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function(){
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			},500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$scope.agora = function () {
			iniciaAgora($view,$scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
		abortar($scope, "#pag-homologacao-prompt");
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

		  $scope.pivot = false;
		  limpaProgressBar($scope, "#pag-homologacao-prompt");
			//22/03/2014 Testa se data início é maior que a data fim
			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});




		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
	  $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};

	$scope.abrirModalAudio = function (row){
		//$('#descricaoPrompt').val(row.descricao);
		$scope.prompt = row;
		$scope.descricaoPrompt = row.descricao;
		$scope.hdescricaoPrompt = row.descricao;
		$scope.textoReconhecido = "";
		$scope.promptHomologado = row.homologado == "S" ? true : false;
		$scope.textoCoerente = row.coerente == "S" ? true : false;
		$scope.codigoAudio = row.codigoAudio;
		console.log($scope.codigoAudio)

		$('#textoReconhecido').val("");
		var chaveApi = "oF7vqfUkGZ8F7Ei92OrRVqAkKx0GAqhBwxaZ2BNFetuE";
		var urlApiWithParams = "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?model=pt-BR_BroadbandModel";
		var pathFile = row.codPrompt + row.codAplicacao;
		var typeFile = "ogg";
		var comando1 = tempDir3()+'ffmpeg -i '+tempDir3()+pathFile+'.'+typeFile+' -ar 44100 '+tempDir3()+pathFile+'_44100.'+typeFile+'';
		var comando2 = tempDir3()+'curl -X POST -u "apikey:'+chaveApi+'" --header "Content-Type: audio/'+typeFile+'" --data-binary @"'+tempDir3()+pathFile+'_44100.'+typeFile+'" "'+urlApiWithParams+'"';
		
		$('#playPrompt').remove();
		if (!promptExist(row.codPrompt,row.codAplicacao)){
			executaExtracaoPrompts(row.codPrompt,row.codAplicacao);
			var tmrPlay = setInterval(function() {
				//if (promptExist(row.codPrompt,row.codAplicacao)){
					$scope.textoReconhecido = "Aguarde o reconhecimento de voz...";
					$('#textoReconhecido').val("Aguarde o reconhecimento de voz...")
					var audio = row.codPrompt + row.codAplicacao + '.ogg';
					var audioApi = row.codPrompt + row.codAplicacao + '_44100.ogg';
					$('#dvAudio').append('<audio id="playPrompt" controls><source src="' + tempDir3() + audio + '" type="audio/ogg"></audio>');
					executeCommand(comando1,comando2,tempDir3() + audioApi);
				//}
				clearInterval(tmrPlay);
			}, 3000);
		}else{
			$scope.textoReconhecido = "Aguarde o reconhecimento de voz...";
			$('#textoReconhecido').val("Aguarde o reconhecimento de voz...")
			var audio = row.codPrompt + row.codAplicacao + '.ogg';
			var audioApi = row.codPrompt + row.codAplicacao + '_44100.ogg';
			$('#dvAudio').append('<audio id="playPrompt" controls><source src="' + tempDir3() + audio + '" type="audio/ogg"></audio>');
			executeCommand(comando1,comando2,tempDir3() + audioApi);
		
		}
	}

	function executeCommand(comando1,comando2,audio){
		require('child_process').exec(comando1, function(e1,out1) {
			if (e1){
				console.log(comando1);
				console.log(e1);
				//process.exit(0);			
			}else{
				console.log(comando2);
				require('child_process').exec(comando2, function(e2,out2) {
					if (e2) console.log(e2);		
					console.log(out2);
					var a = JSON.parse(out2);
					$scope.textoReconhecido = a.results[0].alternatives[0].transcript;
					$('#textoReconhecido').val(a.results[0].alternatives[0].transcript);
					fs.unlinkSync(audio);
				//	process.exit(0);
				});
			}
			
		});
	}

	// Lista chamadas conforme filtros
	$scope.listaDados = function () {
		distinctEmpresas = [];
		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
		$btn_exportar.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_codPrompt = $scope.filtroPrompt == undefined ? "" : $scope.filtroPrompt;

		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";
		$scope.datas = [];
	
		if (filtro_aplicacoes.length === 0) {
			atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
			$btn_gerar.button('reset');
			return;
		}

	  var comAudio = $scope.comAudio ? "inner" : "left";
	  stmt = db.use
	  + " SELECT distinct m.codPrompt,desdetalhada,m.codAplicacao,a.SomaMD5,a.textoHomologado,a.textoCoerente"
	  + " from Prompt m "
	  + comAudio + " join VersionamentoPrompt v on m.CodPrompt = v.CodPrompt "
	  + comAudio + " join AudioPrompt a on a.SomaMD5 = v.SomaMD5Comprimido "
	  + comAudio + " join MapeamentoAplicacoes ma on ma.PastaRaiz = v.PastaAplicacao and ma.CodAplicacao = m.CodAplicacao"
	  + " WHERE 1 = 1";
	  if (filtro_codPrompt != "") stmt += " AND m.CodPrompt like '%" + filtro_codPrompt + "%'";
	  if ($scope.semDescricao) stmt += " AND m.DesDetalhada = ''";
	  if ($scope.homologados) stmt += " AND a.TextoHomologado = 'N'";
	  if ($scope.coerentes) stmt += " AND (a.TextoCoerente = 'N' or a.TextoCoerente is null)";
	  stmt += restringe_consulta("m.CodAplicacao", filtro_aplicacoes, true)
	  stmt += " ORDER BY m.codPrompt";


    
		executaQuery = executaQuery2;
		log(stmt);

		function formataData2(data) {
			var y = data.getFullYear(),
			  m = data.getMonth() + 1,
			  d = data.getDate();
			return y + _02d(m) + _02d(d);
		}

		function formataDataAnoMes(data) {
			var y = data.getFullYear(),
				m = data.getMonth()+1;
				return y + _02d(m)
		}

		function executaQuery2(columns){

			$scope.dados.push({
				dataCadastro: "",
				codPrompt: columns["codPrompt"].value,
				codAplicacao : columns["codAplicacao"].value,
				descricao: columns["desdetalhada"].value,
				codigoAudio: columns["SomaMD5"].value,
				homologado: columns["textoHomologado"].value,
				coerente: columns["textoCoerente"].value
			});

			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
		   if($scope.dados.length%1000===0){
				$scope.$apply();
			}
		}


	db.query(stmt,executaQuery, function (err, num_rows) {
	    userLog(stmt, 'Carrega dados', 2, err)
					
	    $scope.$apply();


		
		retornaStatusQuery(num_rows, $scope);
		$($('.ngHeaderContainer')[0]).css('height','30px');
		$btn_gerar.button('reset');

	  if(num_rows>0){
		$btn_exportar.prop("disabled", false);
	  }
	 

	});




			// GILBERTOOOOOO 17/03/2014
			$view.on("mouseup", "tr.resumo", function () {
				var that = $(this);
				$('tr.resumo.marcado').toggleClass('marcado');
				$scope.$apply(function () {
					that.toggleClass('marcado');
				});
			});


         
			
	};

	$scope.salvar = function (){
		//$scope.prompt.descricao = $scope.descricaoPrompt;
		var homologado = $scope.promptHomologado ? "S" : "N";
		var coerente = $scope.textoCoerente ? "S" : "N";

		var sql = "update Prompt set desDetalhada = '" + $scope.descricaoPrompt + "' where codAplicacao = '" + $scope.prompt.codAplicacao + "' and codprompt = '" + $scope.prompt.codPrompt + "'";
		console.log(sql);
		var sqlAudio = "update AudioPrompt set texto = '" + $scope.descricaoPrompt + "',textoHomologado = '" + homologado + "', textoCoerente = '" + coerente + "' where SomaMD5 = '" +$scope.codigoAudio + "'";
		var sqlLog = "insert into LogPrompts values (getDate(),'" + $scope.prompt.codAplicacao + "','" + $scope.prompt.codPrompt + "',";
		sqlLog += "'" + $scope.hdescricaoPrompt + "','" + $scope.descricaoPrompt + "','" + loginUsuario + "','" + dominioUsuario + "')"; 
		console.log(sqlAudio);

		db.query(sql + sqlAudio + sqlLog, null, function (err, num_rows) {
            console.log("Executando query-> " + sql + " " + num_rows);            
            if (err) {
                console.log("ERRO: " + err);
				alert("ERRO: " + err);
            } else {                
				alert("Prompt salvo com sucesso");	
				$scope.listaDados();
            }  
        });	
		
		//gravar log
	}

	$scope.trocarTexto = function(){
		console.log($scope.trocaTexto)
		if (!$scope.trocaTexto){
			$scope.descricaoPrompt = $scope.textoReconhecido;
		}else{
			$scope.descricaoPrompt = $scope.hdescricaoPrompt;
		}
	}




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

	  var linhas  = [];


	  if($scope.valor === "regra"){



		// Criar cabeçalho
		var cabecalho = $scope.datas.map(function (d) {
			return { value: d.data_BR, bold: 1, hAlign: 'center', autoWidth: true };
		});

		// Inserir primeira coluna: item de controle
		cabecalho.unshift({ value: "Regra", bold: 1, hAlign: 'center', autoWidth: true });

		// Inserir última coluna: total por linha
		cabecalho.push({ value: "Total", bold: 1, hAlign: 'center', autoWidth: true });

		var linhas = $scope.dados.map(function (dado) {	
			var linha = $scope.datas.map(function (d) {
				try {
										
					return { value: dado.totais[d.data].qtd_entrantes || 0 };
				} catch (ex) {
					return 0;
				}
			});

			// Inserir primeira coluna: item de controle
			//linha.unshift({ value: dado.cod_ic + " - " + dado.nome_ic });
			linha.unshift({ value: dado.cod_ic});

			// Inserir última coluna: total por linha
			linha.push({ value: dado.totais.geral.qtd_entrantes });

			return linha;
		});

		// Criar última linha, com os totais por data
		var linha_totais = $scope.datas.map(function (d) {
			try {
				return { value: $scope.totais[d.data].qtd_entrantes || 0 };
			} catch (ex) {
				return 0;
			}
		});

		// Inserir primeira coluna
		linha_totais.unshift({ value: "Total", bold: 1 });

		// Inserir última coluna: total geral
		linha_totais.push({ value: $scope.totais.geral.qtd_entrantes });

		// Inserir primeira linha: cabeçalho
		linhas.unshift(cabecalho);

		// Inserir última linha: totais por data
		linhas.push(linha_totais);



	  }else{

		var header = distinctEmpresas;


		var h =[];
		  
		  
		  h.push({value: "Data" , autoWidth:true});

		for(var i= 0; i<header.length;i++){

		  h.push({value: header[i] , autoWidth:true});
		}
		linhas.push(h);


		$scope.dados.map(function (dado) {
		  var l = [];
			 l.push({value:dado.datReferencia, autoWidth:true});
		  for(var i= 0; i<header.length;i++){
			l.push({value:dado["qtd_"+header[i]], autoWidth:true});
		  }

		  return linhas.push(l);
		});


	  }





		var planilha = {
			creator: "Estalo",
			lastModifiedBy: $scope.username || "Estalo",
			worksheets: [{ name: 'Resumo Chamadas Pré-roteamento', data: linhas, table: true }],
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: { first: 1 }
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');


		var milis = new Date();
		var file = 'resumoEmpresaDestino_' + formataDataHoraMilis(milis) + '.xlsx';


		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, planilha.base64, 'base64');
			childProcess.exec(file);
		}

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);

	  empresas = cache.empresas.map(function(a){return a.codigo});
	  
	  var complemento = "";	
              if($scope.valor === "empresa") complemento +=" EMPRESA";
			  if($scope.valor === "regra") complemento +=" REGRA";	  
			  userLog("", "Exportação "+complemento, 5, "");

	};

}

CtrlHomologacaoPrompt.$inject = ['$scope', '$globals'];
