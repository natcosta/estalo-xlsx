/*
 * CtrlResumo880x144
 */

function CtrlResumo880x144($scope, $globals) {

    win.title = "Resumo 880 (3000) x 144"; // GILBERTOOOO 18/03/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio

    travaBotaoFiltro(0, $scope, "#pag-resumo-880x144", "Resumo 880 (3000) x 144");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.csv = [];
    $scope.dadosPorDia = [];
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;


    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy

    $scope.segmentos = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013

        segmentos: [],
        chkDia: false
    };

    $scope.tresmil = false;
    $scope.oitooito = false;
    //template para exportação
    var nomeArquivo = "";
    $scope.aba = 2;


    function geraColunas() {
        var array = [];

        if (!$scope.filtros.chkDia) {
            array.push({
                field: "data",
                displayName: "Data",
                width: 150,
                pinned: false
            });
        } else {
            array.push({
                field: "data",
                displayName: "Data e hora",
                width: 150,
                pinned: false
            });
        }

        if (!$scope.tresmil) {
            array.push({
                field: "qtd880",
                displayName: "*880",
                width: 150,
                pinned: false,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
            });
        } else {
            array.push({
                field: "qtd880",
                displayName: "*3000",
                width: 150,
                pinned: false,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
            });
        }

        array.push({
            field: "qtd144",
            displayName: "*144",
            width: 150,
            pinned: false,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number }}</span></div>'
        }, {
            field: "porcento",
            displayName: "%",
            width: 150,
            pinned: false,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
        });
        return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: ontem(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-880x144");

 
        // minuteStep: 5

        // 19/03/2014
        componenteDataHora($scope, $view);

        carregaRegioes($view);



        carregaDDDsPorRegiao($scope, $view, true);
        // Popula lista de  ddds a partir das regioes selecionadas
        $view.on("change", "select.filtro-regiao", function () {
            carregaDDDsPorRegiao($scope, $view)
        });


        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs'
        });

        $view.find("select.filtro-segmento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} segmentos'
        });


        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });


        $view.find('select.filtro-final').selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} finais',
            showSubtext: true
        });


        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function () {
            marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
        });

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function () {
            marcaTodosIndependente($('.filtro-ddd'), 'ddds')
        });


        //Bernardo 20-02-2014 Marcar todos os finais
        $view.on("click", "#alinkFinal", function () {
            var valor = $('#alinkFinal').attr("data-var");
            if (valor === "1") {
                $('.filtro-final').selectpicker('deselectAll');
                $('#alinkFinal').attr("data-var", "0");
            } else {
                $('.filtro-final').selectpicker('selectAll');
                $('#alinkFinal').attr("data-var", "1");
            }
        });

        //GILBERTO - change de segmentos

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });


        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.btn-group.filtro-ddd>div>ul').css({
                    'max-height': '600px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '400px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        $('div.container > ul > li >>div> div.btn-group.filtro-final').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-final').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-final').mouseout(function () {
            $('div.btn-group.filtro-final').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-880x144");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, '<font color = "Red">' + testedata + '</font>');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);

            // OUTROS HANDLERS QUAISQUER
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-880x144");
        }


        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
        //} // fim do segundo if
    }); // fim do $viewContentLoaded



    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };


    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        nomeArquivo = $scope.tresmil ? 'tResumo3000x144' : 'tResumo880x144';

        $globals.numeroDeRegistros = 0;

        //if (!connection) {
        //  db.connect(config, $scope.listaChamadas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.prop("disabled", true);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;


        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_finais = $view.find("select.filtro-final").val() || [];
        var filtro_intervalo = $scope.filtros.intervalo || 0;

        // filtros usados para preencher a div de notificação
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        //if (filtro_regioes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_regioes; }
        if (filtro_regioes.length > 0) {
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
        if (filtro_ddds.length > 0) {
            $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }
        if (filtro_finais.length > 0) {
            $scope.filtros_usados += " Finais: " + filtro_finais;
        }

        $scope.dados = [];



        //---------------------------- PRIMEIRA QUERY -(Ligaram para 880 e nao para 144 ) --------------------------------
        //---------------------------------------------------------------------------------------------------------
        //
        //        if (!$scope.filtros.chkDia) {
        //            stmt = "SELECT datepart(year,data880) as ano, datepart(month,data880) as mes, datepart(day,data880) as dia, datepart(hour,data880) as hora, count(distinct telefone) as qtd"
        //        }
        //        else {
        //            stmt = "SELECT datepart(year,data880) as ano, datepart(month,data880) as mes, datepart(day,data880) as dia, count(distinct telefone) as qtd"
        //        }

        //28/08/2014 Lógica para pegar novas tabelas consolidadas





        var nomeTabela = 'Resumo_Ligacoes_' + ($scope.tresmil ? '3000' : ($scope.oitooito ? '880' : 'NOVA880')) + '_144';

        var datfim = "<= '" + formataDataHora(data_fim) + "'"

        // Gilberto - ES-36 - Usar somente um dia quando não marcar "por dia"
        if (!$scope.filtros.chkDia) {
            var granularidade = "Hour"
            datfim = "<= '" + formataDataHora(precisaoHoraFim(data_ini)) + "'";

        } else {
            var granularidade = "Day"
        }

        var stmt = db.use + 'with cte as ('
        stmt += " SELECT dateadd(" + granularidade + ", datediff(" + granularidade + ", 0, data880), 0) as DatReferencia,"
        stmt += " Ligou144, COUNT(distinct Telefone) as Qtd "
        stmt += " From " + db.prefixo + nomeTabela
        stmt += " WHERE data880 >= '" + formataDataHora(data_ini) + "' and data880 " + datfim

        if (filtro_intervalo > 0) {
            stmt += " AND DATEADD(minute," + filtro_intervalo + ", Data880) < Data144";
        }

        if (filtro_regioes !== "") {
            stmt += restringe_consulta3("CodRegiao", filtro_regioes, true);
        }

        if (filtro_segmentos !== "") {
            stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
        }

        // 19/02/2013 - 27/03/2014
        if (filtro_ddds !== "") {
            stmt += restringe_consulta("(SUBSTRING(telefone,1,2))", filtro_ddds, true);
        }

        if (filtro_finais !== "") {
            stmt += restringe_consulta("(SUBSTRING(telefone,Len(telefone),1))", filtro_finais, true);
        }

        stmt += " GROUP BY dateadd(" + granularidade + ", datediff(" + granularidade + ", 0, data880), 0), Ligou144)"
        stmt += " select cte.DatReferencia,"
        stmt += " SUM(case when cte.Ligou144 = 0 then Qtd else 0 end) as QtdConcluiu880,"
        stmt += " SUM(case when cte.Ligou144 = 1 then Qtd else 0 end) as QtdConcluiu144"
        stmt += " from cte"
        stmt += " group by cte.DatReferencia"
        stmt += " ORDER BY cte.DatReferencia"


        log(stmt);


        //----------------------------FIM DA SEGUNDA QUERY -------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------
        var cbProcessaQuery = function (columns) {
            try {
                var data = formataDataHoraBR(columns[0].value)
                rawDate = formataDataHora(columns[0].value),
                    qtdConcluiu880 = (+columns[1].value),
                    qtdConcluiu144 = +columns[2].value;

                var s = {
                    data: data,
                    rawDate: rawDate,
                    qtd144: qtdConcluiu144,
                    qtd880: qtdConcluiu880,
                    porcento: 100 * qtdConcluiu144 / qtdConcluiu880
                }

                $scope.dados.push(s);

                var a = [];
                for (var item in $scope.colunas) {
                    a.push(s[$scope.colunas[item].field]);
                }

                $scope.csv.push(a);

                // $scope.csv.push([
                //     data,
                //     rawDate,
                //     qtdConcluiu144,
                //     qtdConcluiu880,
                //     100 * qtdConcluiu144 / qtdConcluiu880
                // ]);

                
            } catch (e) {
                console.log(e.message);
            }

            //atualizaProgressBar(100, 100, $scope, "#pag-resumo-880x144");

        };

        db.query(stmt, cbProcessaQuery, function (err, num_rows) {
            var complemento = "";

            if ($scope.tresmil) complemento += " 3000";
            if ($scope.oitooito) complemento += " 880";


            if (err) {
                console.log(err);
            }
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
            }

            userLog(stmt, "Consulta " + complemento, 2, err);

        });



        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    }; // fim de listaDados();



    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        //Alex 15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
            " WHERE NomeRelatorio='" + nomeArquivo + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = '' + nomeArquivo + '.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = '' + nomeArquivo + '_' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.dados
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
            var complemento = "";
            if ($scope.tresmil) complemento += " 3000";
            if ($scope.oitooito) complemento += " 880";
            userLog(stmt, "Exportação " + complemento, 5, err);

            //?
        });

    };
}

//$view.on("click", "filtros.isHora", function () {
//    $scope.filtros.isDia = false;
//});

//$view.on("click", "filtros.isDia", function () {
//    $scope.filtros.isHora = false;
//});
//    // Filtros data e hora
//    var agora = new Date();
//    $scope.periodo = {
//        inicio: ontem(),
//        fim: ontemfim(),
//        min: new Date(2012,10,1),
//        max: agora
//    }

//    var $view;
//    $scope.$on('$viewContentLoaded',function(){
//        $view = $("pag-resumo-880x144");

//        var $datahora_inicio = $view.find(".datahora-inicio");
//        $datahora_inicio.datetimepicker({
//            language: 'pt-BR',
//            pickSeconds: false,
//            pickTime: false,
//            startDate: formataDataBR($scope.periodo.min),
//            value: formataDataBR($scope.periodo.inicio)
//    }

CtrlResumo880x144.$inject = ['$scope', '$globals'];