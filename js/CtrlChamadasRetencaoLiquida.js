/*
 * CtrlChamadasRetencaoLiquida
 */
function CtrlChamadasRetencaoLiquida($scope, $globals) {



    win.title = "Detalhamento de chamadas repetidas do reset"; //27/02/2014

    $scope.nomeExportacao = "DetalhadoRetliq";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-chamada-retencao-liquida", "Detalhamento de chamadas repetidas do reset");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.csv = [];


    $scope.gridDados = {
        headerTemplate: base + 'header-template.html',
        data: $scope.dados,
        columnDefs: $scope.colunas,
        enableRowHeaderSelection: false,
        enableSelectAll: true,
        multiSelect: true,
        enableRowSelection: true,
        enableFullRowSelection: true,
        multiSelect: true,
        modifierKeysToMultiSelect: true
    };


    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;

    $scope.log = [];

    $scope.aplicacoes = cache.apls; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        sites: [],
        segmentos: []
    };

    $scope.tabela = "";
    $scope.valor = "24h";
    $scope.agrupar = false;
    $scope.perc = false;



    function geraColunas(tipo) {



        var array = [];
        array.push({
            field: "data_hora_BR",
            displayName: "Data",
            width: 150,
            pinned: true
        }, {
            field: "chamador",
            displayName: "Chamador",
            width: 100,
            pinned: false
        });

        array.push({
            field: "tratado",
            displayName: "Tratado",
            width: 100,
            pinned: false
        });

        array.push({
            field: "aplicacao",
            displayName: "Aplicação",
            width: 130,
            pinned: false
        }, {
            field: "segmento",
            displayName: "Segmento",
            width: 100,
            pinned: false
        }, {
            field: "encerramento",
            displayName: "Encerramento",
            width: 150,
            pinned: false
        }, {
            field: "estado_final",
            displayName: "Último estado",
            width: 400,
            pinned: false
        });


        /*
    array.push({
      field: "ucid",
      displayName: "Log",
      width: 65,
      cellClass: "grid-align",
      pinned: false,
      cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-log" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>'
    });
	*/



        return array;

    }

    $scope.aba = 2;

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {


        $view = $("#pag-chamada-retencao-liquida");

        $(".aba3").css({
            'position': 'fixed',
            'left': '55px',
            'top': '40px'
        });
        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });


        componenteDataHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view, false, false, $scope);
        carregaDoisDigitos($view, true);
        carregaMotivos($view);
        carregaEntradas($view);



        carregaSegmentosPorAplicacao($scope, $view, true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });


        $view.on("change", "select.filtro-aplicacao", function () {
            var arr = $(this).val();
            if (arr !== null && arr[0] === 'NOVAOITV') {
                $('.filtro-chamador').prop('disabled', false);
            } else {
                $('.filtro-chamador').prop('disabled', 'disabled');
            }
        });


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-motivo").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} motivos',
            showSubtext: true
        });

        $view.find("select.filtro-entrada").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} entradas',
            showSubtext: true
        });





        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });

        //Marca um RANGE de dígitos
        $view.on("click", "#alinkUltDoisDig", function () {
            //marcaTodosIndependente($('.filtro-ud'),'uds')

            rangePrompt('Determine a gama de valores a serem incluídos como "00-15".', "Marcar Valores", function callback(valor1, valor2) {
                function doubleDigit(n) {
                    return (n).toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    })
                }

                if (valor1 == 0 && valor2 == 0) {
                    $('.filtro-udd').selectpicker('val', []);
                };

                if (valor1 && valor2) { //valor.indexOf("-")
                    console.log("Possui -");
                    //console.log("Valor 1:"+valor.split("-")[0]+" Segundo Valor: "+valor.split("-")[1]);

                    //console.log("Valor 2:"+valor.split("-")[0]);
                    var i = doubleDigit(valor1); //valor.split("-")[0];
                    var j = doubleDigit(valor2); //valor.split("-")[1];
                    console.log("Valor 1:" + i + " Valor2: " + j);
                    console.log("I: " + i + " J: " + j);
                    rangeOfValues = [];
                    while (i <= j) { //7
                        if (i == 0) {
                            rangeOfValues.push("00");
                            console.log("Adicionou: " + i);
                        } else {
                            rangeOfValues.push(doubleDigit(i));
                            console.log("Adicionou: " + doubleDigit(i));
                        }
                        i++;
                        //i=doubleDigit(i)+1;
                    }
                    //$('.filtro-udd').selectpicker().val();
                    $('.filtro-udd').selectpicker('val', rangeOfValues);

                } else {
                    console.log("Não Possui -");
                }


            });
        });


        // EXIBIR AO PASSAR O MOUSE     

        $('div > ul > li >>div> div.btn-group.filtro-udd').mouseover(function () {
            $('div.btn-group.filtro-udd').addClass('open');
            $('div.btn-group.filtro-udd>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '400px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE

        $('div > ul > li >>div> div.btn-group.filtro-udd').mouseout(function () {
            $('div.btn-group.filtro-udd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '400px'
            });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ud').mouseover(function () {
            $('div.btn-group.filtro-ud').addClass('open');
            $('div.btn-group.filtro-ud>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '400px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ud').mouseout(function () {
            $('div.btn-group.filtro-ud').removeClass('open');
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-retencao-liquida");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas(1);

            $scope.listaDados.apply(this);

        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);


            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });



        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-chamada-retencao-liquida");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };




    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        var datasComDados = [];

        var sufixo = $scope.valor;

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];


        if (filtro_aplicacoes.length === 0) {
            atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
            $btn_gerar.button('reset');
            return;
        }
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_udd = $view.find("select.filtro-udd").val() || [];
        var filtro_motivo = $view.find("select.filtro-motivo").val() || [];
        var filtro_entrada = $view.find("select.filtro-entrada").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }

        $scope.dados = [];
        $scope.csv = [];

        var consultas = 0;




        var valor = "10000";
        var stmt = db.use +
            " SELECT" + testaLimite(valor) +
            "   IVR.CodUCIDIVR, IVR.DataHora_Inicio, NumANI, ClienteTratado, IVR.CodAplicacao," +
            "   QtdSegsDuracao, SegmentoChamada, IndicSucessoGeral, CodUltEstadoRec " +
            ",'TRN' AS IndicDelig, transferid, ctxid";
        stmt += " FROM " + db.prefixo + "ChamadasResetPorta IVR" +
            " WHERE 1 = 1" +
            " AND IVR.DataHora_Inicio >= '" + formataDataHora(data_ini) + "'" +
            " AND IVR.DataHora_Inicio <= '" + formataDataHora(data_fim) + "'"
        stmt += restringe_consulta("IVR.CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("IVR.SegmentoChamada", filtro_segmentos, true)

        stmt += " order by IVR.DataHora_Inicio ASC";


        log(stmt);


        var stmtCountRows = stmtContaLinhasRetencao(stmt);

        //24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            datasComDados.push(columns[1].value);
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery(columns) {


            var UCID = columns[0].value,
                data_hora = formataDataHora(columns[1].value),
                data_hora_BR = formataDataHoraBR(columns[1].value),
                data = columns[1].value;


            var chamador = columns[2].value,
                tratado = columns[3].value,
                cod_aplicacao = columns[4].value,
                aplicacao = obtemNomeAplicacao(columns[4].value),
                duracao = columns[5].value,
                cod_segmento = columns[6].value,
                segmento = cod_segmento,
                encerramento = obtemNomeEncerramento(columns[7].value, columns["IndicDelig"].value),
                estado_final = obtemEstado(cod_aplicacao, columns[8].value),
                transferid = columns["transferid"].value,
                ctxid = columns["ctxid"].value !== null ? columns["ctxid"].value : "";

            var s = {
                UCID: UCID,
                data_hora: data_hora,
                data_hora_BR: data_hora_BR,
                data: data,
                chamador: chamador,
                tratado: tratado,
                cod_aplicacao: cod_aplicacao,
                aplicacao: aplicacao,
                duracao: duracao,
                segmento: obtemNomeSegmento(segmento),
                cod_segmento: cod_segmento,
                encerramento: encerramento,
                estado_final: estado_final.hasOwnProperty('nome') ? estado_final.nome : "",
                transferid: transferid,
                ctxid: ctxid
            }
            $scope.dados.push(s);

            var a = [];
            for (var item in $scope.colunas) {
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);

            // $scope.csv.push([
            //     UCID,
            //     data_hora,
            //     data_hora_BR,
            //     data,
            //     chamador,
            //     tratado,
            //     cod_aplicacao,
            //     aplicacao,
            //     duracao,
            //     obtemNomeSegmento(segmento),
            //     cod_segmento,
            //     encerramento,
            //     "",
            //     transferid,
            //     ctxid
            // ]);
        }


        //13/06/2014 Query de 5 em 5 minutos
        var stmts = arrayCincoMinutosQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);

        //13/06/2014 Data final do penúltimo array com base na hora final do filtro, último array é uma flag
        stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(/DataHora_Inicio <= \'.+\' /g, "DataHora_Inicio <= \'" + formataDataHora(new Date(data_fim)) + "\' ")];

        var controle = 0;
        var total = 0;

        function proximaHora(stmt) {
            var pularQuery = false;
            for (var i = 0; i < datasComDados.length; i++) {
                var d = new RegExp('DataHora_Inicio >= \'' + datasComDados[i] + '(.*?)\'');
                if (stmt.match(d) === null) {
                    pularQuery = true;
                } else {
                    pularQuery = false;
                    datasComDados.splice(0, i - 1);
                    break;
                }
            }

            function comOuSemDados(err, c) {
                $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, stmts.length) + " Encontrados: " + total).selectpicker('refresh');
                controle++;
                $scope.$apply();

                //Executa dbquery enquanto array de querys menor que length-1
                if (controle < stmts.length - 1) {
                    proximaHora(stmts[controle].toString());
                }

                //Evento fim
                if (controle === stmts.length - 1) {
                    if (c === undefined) console.log("fim " + controle);
                    if (err) {
                        retornaStatusQuery(undefined, $scope);
                    } else {
                        retornaStatusQuery($scope.dados.length, $scope);
                    }

                    $btn_gerar.button('reset');
                    if ($scope.dados.length > 0) {
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
                    } else {
                        $btn_exportar.prop("disabled", true);
                        $btn_exportar_csv.prop("disabled", true);
                        $btn_exportar_dropdown.prop("disabled", true);
                    }
                }
            }

            if (pularQuery) {
                comOuSemDados(false, "console");
            } else {
                db.query(stmt, executaQuery, function (err, num_rows) {
                    console.log("Executando query-> " + stmt + " " + num_rows);
                    num_rows !== undefined ? total += num_rows : total += 0;
                    comOuSemDados(err);


                    $scope.gridDados.data = $scope.dados;
                    $scope.gridDados.columnDefs = $scope.colunas;
                    $scope.$apply();

                });
            }
        }

        db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
            console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);
            if ($globals.numeroDeRegistros === 0) {
                retornaStatusQuery(0, $scope);
                $btn_gerar.button('reset');
            } else {
                //Disparo inicial
                proximaHora(stmts[0].toString());
                $scope.colunas = geraColunas(1);
            }
        });







        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.repetidas", function () {
            var that = $(this);
            $('tr.repetidas.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });




    };



    // Exporta planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);

        $btn_exportar.button('loading');

        var linhas = [];
        linhas.push($scope.colunas.filterMap(function (col, index) {
            if (col.visible || col.visible === undefined) {
                return col.displayName;
            }
        }));
        $scope.dados.forEach(function (dado) {
            linhas.push($scope.colunas.filterMap(function (col, index) {
                if (col.visible || col.visible === undefined) {
                    return dado[col.field];
                }
            }));
        });




        var works = [];

        if (linhas.length > 0) works.push({
            name: "Chamadas",
            data: linhas,
            table: true
        });


        var planilha = {
            creator: "Estalo",
            lastModifiedBy: loginUsuario || "Estalo",
            worksheets: works,
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');

        var caminho = $scope.nomeExportacao + "_" + formataDataHoraMilis(new Date()) + ".xlsx";
        fs.writeFileSync(caminho, planilha.base64, 'base64');
        childProcess.exec(caminho);

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };
}
CtrlChamadasRetencaoLiquida.$inject = ['$scope', '$globals'];