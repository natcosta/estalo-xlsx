/*
 * CtrlResumoPorPromo
 */
function CtrlResumoPorPromo($scope, $globals) {

    win.title = "Resumo por promoção vendas"; //Alex 27/02/2014
    $scope.versao = versao;
    $scope.rotas = rotas;
    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-resumo-por-promo", "Resumo por promoção vendas");

    //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
    $scope.filtros = {
        isHFiltroED: false
    };


    $scope.vendas = [];
    $scope.csv = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "vendas",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.total_valores;
    $scope.total_qtds;
    $scope.ordenacao = 'codOferta';
    $scope.decrescente = false;


    $scope.log = [];

    // Filtro: status_vendas
    $scope.estatos = status_vendas;

    //filtros_usados
    $scope.filtros_usados = "";

    $scope.valor = "promocao";
	$scope.errSucErr = false;
	$scope.errSucSuc = false;
    $scope.viewFormat = null;


    function geraColunas() {
        var array = [];

        if ($scope.valor === "hora") {
            array.push({
                field: "dataHora",
                displayName: "Data e hora",
                width: 155,
                pinned: false
            });
        } else if ($scope.valor === "dia") {
            array.push({
                field: "dataHora",
                displayName: "Data",
                width: 95,
                pinned: false
            });
        }
		
		
		
		if($("select.filtro-ddd").val() !== null){
			array.push({
			field: "ddd",
				displayName: "DDD",
				width: 50,
				pinned: false
			})
		}
		
		

        array.push({	
            field: "codOferta",
            displayName: "Código oferta",
            width: 125,
            pinned: false
        }, {
            field: "aplicacao",
            displayName: "Aplicação",
            width: 150,
            pinned: false
        });
		
		if($("select.filtro-aplicacao").val().indexOf('NOVAOITV') < 0 && $("select.filtro-aplicacao").val().indexOf('URACAD') < 0){
			array.push({
				field: "tipoTransacao", displayName: "Transação", width: 50,	pinned: false });
		}
		
		if($("select.filtro-aplicacao").val().indexOf('URACAD') >= 0){
			array.push({
				field: "tipoTransacao", displayName: "Fonte", width: 100, pinned: false });
		}
		
		
		if(!$scope.errSucErr && $scope.errSucSuc){		
			array.push({
				field: "qtd",
				displayName: "Qtd",
				width: 80,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			});
		}else{		
			array.push({
				field: "qtd",
				displayName: "Qtd",
				width: 80,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			},{
				field: "descricaoErro",
				displayName: "Descrição erro",
				width: 465,
				pinned: false//,
				//cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
			});
		}
		
        if (dominioUsuario.toUpperCase() !== "CONTAX-BR") {
            array.push({
                field: "valor",
                displayName: "Valor",
                width: 150,
                pinned: false,
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
            });
        }
        return array;
    }

    // Filtros: data e hora
    var agora = new Date();


    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora 
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-resumo-por-promo");

        //19/03/2014
        componenteDataHora($scope, $view);

        carregaAplicacoes($view, false, true, $scope);

        carregaProdutosPorAplicacao($scope, $view, true);
        // Popula lista de produtos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaProdutosPorAplicacao($scope, $view)
        });

        carregaSegmentosPorAplicacao($scope, $view, true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });

		carregaDDDs($view,true);
		carregaErros($view,true);
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});
		
        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        $view.on("change", "select.filtro-grupo-produto", function () {
            Estalo.filtros.filtro_grupos_produtos = $(this).val() || [];
        });

        $view.on("change", "select.filtro-produto", function () {
            Estalo.filtros.filtro_produtos = $(this).val() || [];
        });

        $view.on("change", "select.filtro-erro", function () {
            Estalo.filtros.filtro_erros = $(this).val() || [];
        });

        $view.on("change", "select.filtro-status", function () {
            Estalo.filtros.filtro_resultados = $(this).val() || [];
        });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });        

        // Marca todos os produtos
        $view.on("click", "#alinkProd", function () {
            marcaTodosIndependente($('.filtro-produto'), 'produtos')
        });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope, true)
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        }); 
		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});		

        

        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {

            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;

            //13/06/2014 Limita pesquisa a um ano
            if (_02d(data_ini.getFullYear()) !== _02d(data_fim.getFullYear())) {
                setTimeout(function () {
                    atualizaInfo($scope, "Operação válida somente para o mesmo ano.");
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            limpaProgressBar($scope, "#pag-resumo-por-promo");
            //22/03/2014 Testa se data início é maior que a data fim

            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
			
			//22/03/2014 Testa se uma aplicação foi selecionada
			var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
			if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
			    setTimeout(function(){
			        atualizaInfo($scope,'Selecione uma aplicação');
			        effectNotification();
			        $view.find(".btn-gerar").button('reset');
			    },500);
			    return;
			}
			
            $scope.colunas = geraColunas();
            $scope.listaVendas.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-por-promo");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');
    });


    // Lista vendas conforme filtros
    $scope.listaVendas = function () {

        $globals.numeroDeRegistros = 0;
        $scope.filtros_usados = "";

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_produtos = $view.find("select.filtro-produto").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_produtos.length > 0) {
            $scope.filtros_usados += " Produtos: " + filtro_produtos;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }

        $scope.vendas = [];
        $scope.csv = [];


        var stmt = db.use + "SELECT " + 
		($scope.valor !== "promocao" ? "dateadd(minute, datediff(minute,0,DataHora) / " + ($scope.valor != "hora" ? "1440 * 1440" : "60 * 60") + ", 0) as datahora," : "") + 
		""+(filtro_ddds.length > 0 ? "SUBSTRING(NumANI,0,3) as DDD," : "")+" codOferta,VUP.codAplicacao," +
            " sum(valor) as valor," +
            " TipoTransacao, count (*) as qtd,";
			if($scope.errSucErr || !$scope.errSucErr && !$scope.errSucSuc)stmt +=" codErro,";
			stmt += " PU.GrupoProduto"+
            " FROM " + db.prefixo + "VendasURAPromo as VUP" +
			" left outer join " + db.prefixo + "ProdutosURA as PU" +
            " on VUP.CodProduto = PU.CodProduto" +
            " WHERE 1 = 1" +
            " AND DataHora >= '" + formataDataHora(data_ini) + "'" +
            " AND DataHora <= '" + formataDataHora(data_fim) + "'" +
            " AND codOferta<>''";
			if($scope.errSucSuc && !$scope.errSucErr){
				stmt +=" and Status in (0,20) ";
			}else if (!$scope.errSucSuc && $scope.errSucErr){
				stmt +=" and Status not in (0,20) ";
			}
			//$scope.errSuc === "sucesso" ? stmt +=" and Status in (0,20) " :	stmt +=" and Status not in (0,20) ";
		stmt += restringe_consulta("VUP.CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("VUP.CodProduto", filtro_produtos, true)
		stmt += restringe_consulta("SUBSTRING(NumANI,0,3)", filtro_ddds, true)
        stmt += restringe_consulta("VUP.CodSegmento", filtro_segmentos, true)
        stmt += " group by " + ($scope.valor !== "promocao" ? "dateadd(minute, datediff(minute,0,DataHora) / " + ($scope.valor != "hora" ? "1440 * 1440" : "60 * 60") + ", 0)," : "") + 
		" "+(filtro_ddds.length > 0 ? "SUBSTRING(NumANI,0,3)," : "")+"  VUP.codAplicacao, codOferta, TipoTransacao,";
		if($scope.errSuc !== "sucesso") stmt += "codErro,";		
		stmt += "PU.GrupoProduto order by " + 
		($scope.valor !== "promocao" ? "dateadd(minute, datediff(minute,0,DataHora) / " + 
		($scope.valor != "hora" ? "1440 * 1440" : "60 * 60") + ", 0)," : "") + 
		" "+(filtro_ddds.length > 0 ? "SUBSTRING(NumANI,0,3)," : "")+" codOferta ASC";

        $scope.viewFormat = $scope.valor;

        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery(columns) {

            var dataHora = columns["datahora"] !== undefined ? ($scope.valor != "hora" ? formataDataBR(columns["datahora"].value) : formataDataHoraBR(columns["datahora"].value)) : undefined,
				ddd = columns["DDD"]!==undefined ? columns["DDD"].value : undefined,
                codOferta = columns["codOferta"].value,
                descricao = obtemDescricaoPromocao(columns["codAplicacao"].value, columns["codOferta"].value),
                aplicacao = columns["codAplicacao"].value,
                valor = +columns["valor"].value.toFixed(2),
                tipoTransacao = columns['TipoTransacao'].value,
                qtd = +columns['qtd'].value,				
				grupo = columns['GrupoProduto'].value,
				codErro = columns['codErro'] !== undefined ? columns['codErro'].value : undefined,
				descricaoErro = columns['codErro'] !== undefined ? obtemNomeErroVenda(codErro,grupo) : undefined;
				

            $scope.total_valores = $scope.total_valores + valor;
            $scope.total_qtds = $scope.total_qtds + qtd;
			
				var obj = {
						dataHora: dataHora,
						ddd: ddd,
						codOferta: codOferta,
						aplicacao: aplicacao,
						tipoTransacao: tipoTransacao,
						qtd: qtd,
						descricaoErro:descricaoErro,
						valor: dominioUsuario.toUpperCase() !== "CONTAX-BR" ? valor : "NA"
					};

				Object.keys(obj).forEach(function(key){
					obj[key] === undefined && delete obj[key];
				});
				$scope.vendas.push(obj);

				var array = [];
				Object.keys(obj).forEach(function(key){
				if(obj[key] !== undefined) array.push(obj[key]); 
				});

				$scope.csv.push(array);
			
	

            if ($scope.vendas.length % 1000 === 0) {
                $scope.$apply();
            }

        }

        $scope.total_valores = 0;
        $scope.total_qtds = 0;


        db.query(stmt, executaQuery, function (err, num_rows) {
            //userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
			
			var obj = {};
			var colunas = [];
			for (i = 0; i < $scope.colunas.length; i++) {
				if($scope.colunas[i].field === "tipoTransacao"){
					colunas.push("TOTAL");
					obj[$scope.colunas[i].field] = "TOTAL";
				}else if($scope.colunas[i].field === "qtd"){
					colunas.push($scope.total_qtds);
					obj[$scope.colunas[i].field] = $scope.total_qtds;
				}else if($scope.colunas[i].field === "valor"){
					colunas.push(dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA");
					obj[$scope.colunas[i].field] = dominioUsuario.toUpperCase() !== "CONTAX-BR" ? $scope.total_valores : "NA";
				}else{
					obj[$scope.colunas[i].field] = "";
					colunas.push("");
				}
				
			}
		   $scope.vendas.push(obj);
		   $scope.csv.push(colunas);			   



            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
        });


    };
    
  // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.csv);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'ResumoPorPromo' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };
}
CtrlResumoPorPromo.$inject = ['$scope', '$globals'];