function CtrlMonitorTeradata($scope, $globals) {
  win.title = "Monitor Teradata"; //Brunno 16/01/2019
  $scope.versao = versao;
  $scope.rotas = rotas;

  $scope.localeGridText = {
    contains: "Contém",
    notContains: "Não contém",
    equals: "Igual",
    notEquals: "Diferente",
    startsWith: "Começa com",
    endsWith: "Termina com"
  };

  var $view;

  $scope.gridExporta = {
    rowSelection: "single",
    enableColResize: true,
    enableSorting: true,
    enableFilter: true,
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
    },
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    overlayNoRowsTemplate:
      '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',

    columnDefs: [
      { headerName: "Processo", field: "procExportacao", width: 350 },
      { headerName: "Atualizado até", field: "proxExecExport" },
      { headerName: "Data Gravação", field: "proxExecData" },
      { headerName: "Periodicidade", field: "proxExecPerido", width: 160 },
      { headerName: "Status", field: "proxExecStatus", width: 120 },
      { headerName: "Tarefa", field: "proxExecTarefa",  width: 160},
      { headerName: "TimeStamp", field: "proxExecTimeStamp",  width: 220}
    ],

    rowData: [],
    localeText: $scope.localeGridText,
    suppressHorizontalScroll: false
  };
  $scope.gridExporta.getRowStyle = function(params) {
    if (params.data.proxExecStatus === "Normal") {
      return { background: "rgb(223,240,216)" };
    } else {
      return { background: "#EFF692", color: "red" };
    }
  };

  $scope.gridImporta = {
    rowSelection: "single",
    enableColResize: true,
    enableSorting: true,
    enableFilter: true,
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
    },
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    overlayNoRowsTemplate:
      '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
    columnDefs: [
      { headerName: "Processo", field: "procImporta" },
      { headerName: "Atualizado até", field: "proxExecImporta" },
      {
        headerName: "URL",
        field: "url",
        width: 80,
        cellRenderer: linkCellRendererFunc
      }
    ],
    rowData: [],
    angularCompileRows: true,
    localeText: $scope.localeGridText,
    suppressHorizontalScroll: true
  };

  function linkClicked(val) {
    // alert("val clicked: " + val);
    gui.Shell.openExternal(val);
  }

  function linkCellRendererFunc(params) {
    params.$scope.linkClicked = linkClicked;
    return '<button ng-click="linkClicked(data.url)">Doc</button>';
  }

  $scope.$on("$viewContentLoaded", function() {
    $view = $("#pag-monitor-teradata");

    limpaProgressBar($scope, "#pag-monitor-teradata");
    $scope.gridExporta.rowData = [];
    $scope.listarExporta.apply(this);

    // Botão abortar Alex 23/05/2014
    $view.on("click", ".abortar", function() {
      $scope.abortar.apply(this);
    });

    //Alex 23/05/2014
    $scope.abortar = function() {
      abortar($scope, "#pag-monitor-teradata");
    };
  });

  $scope.listarExporta = function() {
    console.log("Buscando em ControleExtratores o ID LIKE Exporta");
    var $btn_gerar_exporta = $(this);
    $btn_gerar_exporta.button("loading");
    var stmtExporta =
      "SELECT Hostname, id, Habilitado, DataIni, DataFim, Config, [Status], StatusJSON, DataIni-1 as datagravacao FROM ControleExtratores WHERE ID LIKE '%Export%' and Habilitado = 1 and Config NOT LIKE '%EXIBICAO=FALSE%' ";
    var data = new Date();
    var dataFormatada =
      (function() {
        var day =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : data.getDate();
        return day >= 0 && day < 10 ? "0" + day : day;
      })() +
      "/" +
      (function() {
        var month =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : data.getMonth() + 1;
        return month >= 0 && month < 10 ? "0" + month : month;
      })() +
      "/" +
      data.getFullYear();
    var dataFormatada1 = dataFormatada.replace(
      (function() {
        var day =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : data.getDate();
        return day >= 0 && day < 10 ? "0" + day : day;
      })(),
      (function() {
        var day =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : data.getDate() - 1;
        return day >= 0 && day < 10 ? "0" + day : day;
      })()
    );
    var dataMonth = data.getMonth();
    var dataDay = data.getDate();

    db.query(
      stmtExporta,
      function(columns) {       
        var dataBanco = formataDataHoraBR(columns[3].value).replace(
          "00:00:00",
          ""
        );
        number = columns[5].value.indexOf("PERIOD=2");
        var dataFormatada1 = dataFormatada.replace(
          (function() {
            var day =
              arguments.length > 0 && arguments[0] !== undefined
                ? arguments[0]
                : data.getDate();
            return day >= 0 && day < 10 ? "0" + day : day;
          })(),
          (function() {
            var day =
              arguments.length > 0 && arguments[0] !== undefined
                ? arguments[0]
                : data.getDate() - 1;
            return day >= 0 && day < 10 ? "0" + day : day;
          })()
        );
        if (number >= 0) {
          periodo = "D-2";
          switch (dataMonth) {
            case 0: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/01", "31/12");
              }
              break;
            }
            case 1: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/01", "31/01");
              } else break;
            }
            case 2: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/03", "28/02");
              }
              break;
            }
            case 3: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/04", "31/03");
              }
              break;
            }
            case 4: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/05", "30/04");
              }
              break;
            }
            case 5: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/06", "31/05");
              }
              break;
            }
            case 6: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/07", "30/06");
              }
              break;
            }
            case 7: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/08", "31/07");
              }
              break;
            }
            case 8: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/09", "31/08");
              }
              break;
            }
            case 9: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/10", "30/09");
              }
              break;
            }
            case 10: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/11", "31/10");
              }
              break;
            }
            case 11: {
              if (dataDay === 1) {
                dataFormatada1 = dataFormatada.replace("01/12", "30/11");
              }
              break;
            }
            default: {
              break;
            }
          }
          if (dataBanco.match(dataFormatada1) === null || undefined) {
            status = "Atrasado";
          } else {
            status = "Normal";
          }
        } else {
          periodo = "D-1";
          if (dataBanco.match(dataFormatada) === null) {
            status = "Atrasado";
          } else {
            status = "Normal";
          }
        }
        var StatusJSON = columns['StatusJSON']['value'];
        StatusJSON = StatusJSON ? JSON.parse(StatusJSON) : {};        
        $scope.gridExporta.rowData.push({
          procExportacao: columns[1].value.replace("TNL", ""),
          proxExecExport: formataDataHoraBR(columns[3].value),
          proxExecPerido: periodo,
          proxExecData: formataDataHoraBR(columns[8].value),
          proxExecStatus: status,
          proxExecTarefa: StatusJSON.tarefa ? StatusJSON.tarefa : 'Ocioso',
          proxExecTimeStamp: StatusJSON.ts
        });
      },
      function(err, num_rows) {
        if (err) {
          console.log("Erro ao buscar exportação: " + err);
        } else {
          console.log("Registros encontrados: " + num_rows);
          retornaStatusQuery($scope.gridExporta.rowData.length, $scope);

          $scope.gridExporta.api.setRowData($scope.gridExporta.rowData);
          $scope.gridExporta.api.refreshCells({ force: true });

          $btn_gerar_exporta.button("reset");
          limpaProgressBar($scope, "#pag-monitor-teradata");
          $scope.gridImporta.rowData = [];
          $scope.listarImporta.apply(this);
        }
      }
    );
  };

  $scope.listarImporta = function() {
    console.log("Buscando em ControleExtratores o ID LIKE Import");
    var $btn_gerar_import = $(this);
    $btn_gerar_import.button("loading");
    var stmtMailing =
      "SELECT * FROM ControleExtratores WHERE ID LIKE '%Import%'";
    db.query(
      stmtMailing,
      function(columns) {
        $scope.gridImporta.rowData.push({
          procImporta: columns[1].value.replace("TNL", ""),
          proxExecImporta: formataDataHoraBR(columns[3].value),
          url:
            columns[7].value.match(/"url":"(.*?)"/) !== null
              ? columns[7].value.match(/"url":"(.*?)"/)[1] !== null
                ? columns[7].value.match(/"url":"(.*?)"/)[1]
                : '{"url":""}'
              : '{"url":""}'
        });
      },
      function(err, num_rows) {
        if (err) {
          console.log("Erro ao buscar Importa: " + err);
        } else {
          console.log("Registros encontrados: " + num_rows);
          retornaStatusQuery($scope.gridImporta.rowData.length, $scope);

          $scope.gridImporta.api.setRowData($scope.gridImporta.rowData);
          $scope.gridImporta.api.refreshCells({ force: true });

          $btn_gerar_import.button("reset");

          limpaProgressBar($scope, "#pag-monitor-teradata");
        }
      }
    );
  };
}

CtrlMonitorTeradata.$inject = ["$scope", "$globals"];
