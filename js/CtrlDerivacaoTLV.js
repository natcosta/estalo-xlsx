function CtrlDerivacaoTLV($scope, $globals) {

    win.title = "Derivação de chamadas TLV";
    $scope.versao = versao;
    $scope.loop = []
    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-derivacao-tlv", "Derivação TLV");

    $scope.status_progress_bar = 0;
    $scope.proxhide = true;
    
    var periodo = { headerName: 'Período', field: 'procPeriodo' , width: 100}
    var produto = {headerName: 'Produto' , field: 'proxProduto', width: 100}
    var derivadas = { headerName: 'Derivadas para TLV', field: 'proxDerivadas' , width: 150}
    var derivadaspercent =  { headerName: '% Derivadas para TLV', field: 'proxDerivadasPercent', width: 150,
    cellStyle: function(params) {
        if(params.data.derivadasPercent < 4.5){
        return {
            color:'red'
        }}else{return ''}
    } }
    var bolhas = { headerName: 'Pró ativa (BOLHAS)', field: 'proxBolhas', width: 150}
    var bolhaspercent =  { headerName: '% Pró ativa (BOLHAS)', field: 'proxBolhasPercent', width: 150,
    cellStyle: function(params) {
        if(params.data.bolhasPercent < 45.00){
        return {
            color:'red'
        }}else{return ''}
    } }
    var menu = { headerName: 'Sob demanda (SUB-MENU)', field: 'proxMenu', width: 150}
    var menupercent = { headerName: '% Sob demanda (SUB-MENU)', field: 'proxMenuPercent',width: 150,
    cellStyle: function(params) {
        if(params.data.menuPercent < 15.00){
        return {
            color:'red'
        }}else{return ''}
    } }
    var escolha =  { headerName: 'Escolha Cliente', field: 'proxEscolha', width: 100}
    var escolhapercent = { headerName: '% Escolha Cliente', field: 'proxEscolhaPercent', width: 100,
    cellStyle: function(params) {
        if(params.data.escolhaPercent < 7.5){
        return {
            color:'red'
        }}else{return ''}
    } }
    $scope.colunas = [
        periodo,derivadas,derivadaspercent,bolhas,bolhaspercent,menu,menupercent,escolha,escolhapercent    
];
//console.log($scope.colunas);
    
    $scope.dados = [];
    // Grid dos dados
    $scope.gridDados = {
        defaultColDef: {
            sortable: true,
            filter: true
        },
        rowSelection: 'single',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        onGridReady: function(params) {
                params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
       
        columnDefs: $scope.colunas,
        
        rowData: [],
        localeText: $scope.localeGridText,
        suppressHorizontalScroll: false
};


    $scope.datas = [];
    
   
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.csv = []
    $scope.csvCabecalho = ["Código","Descrição"]
    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
    };

    /*for (var cod_aplicacao in cache.aplicacoes) {
      var apl = cache.aplicacoes[cod_aplicacao];
      if (apl.itens_controle) {
        apl.itens_controle.indice = geraIndice(apl.itens_controle);
      }
    }*/

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;
    function restringe_aplicacao(valores) {
        if (valores.length === 0) {
            return  console.log("valor 0");
        } else {
            aplicacao = "'";
            for (i = 0; i < valores.length - 1; i++) { 
                aplicacao = aplicacao + valores[i] + ","
              }
              
              return aplicacao + valores[valores.length-1]+"'";
    }
    }

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-derivacao-tlv");

        $scope.gridDados.api.hideOverlay();

        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });


        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view, false, false, $scope);
        
     
        //GILBERTO - change de filtros


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });
        $view.find("select.filtro-produto-prepago").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} produto',
            showSubtext: true
        });

        //Marcar todos   

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });

        
        var produto_prepago = $view.find("select.filtro-produto-prepago");
        produto_prepago.html("").prop("disabled", true).selectpicker('refresh')
        $('#alinkPrepago').attr('disabled','disabled').selectpicker('refresh'); 
        
        var filtro_aplicacoes = [];
        //O filtro produto prepago só funciona quando a aplicação prepago estive selecionada
        
        $view.on("change", "select.filtro-aplicacao", function () {
            filtro_aplicacoes = $(this).val();
            var aplicacao = "";
            if (filtro_aplicacoes.length > 0 || filtro_aplicacoes[0] !== null) {
            produto_prepago.prop("disabled", false)
		    $('#alinkPrepago').removeAttr('disabled'); 
            carregaProdutoPrepago($view,true);
            
        }else if(filtro_aplicacoes === null){
            produto_prepago.html("").prop("disabled", true).selectpicker('refresh')
            $('#alinkPrepago').attr('disabled','disabled').selectpicker('refresh'); 
        }
         });

         $view.on("change","select.filtro-produto-prepago",function(){
            filtro_produto_prepago = $(this).val();
                        
         })
        
        //if(filtro_aplicacoes)
        $view.on("click", "#alinkPrepago", function(){ marcaTodosIndependente($('.filtro-produto-prepago'),'produto_prepago')});

        
      

        $view.on("change", "select.filtro-produto-prepago", function () {
           Estalo.filtros.filtro_operacoes = $(this).val();
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            var filtro_produto_prepago = $view.find("select.filtro-produto-prepago").val() || [];
           // console.log("tamanho filtro " + filtro_produto_prepago.length);
            //console.log("Tamanho coluna 0 "+$scope.colunas.length);
            if (filtro_produto_prepago.length !== undefined){
                $scope.colunas = [periodo,produto,derivadas,derivadaspercent,bolhas,bolhaspercent,menu,menupercent,escolha,escolhapercent];
                $scope.gridDados.api.setColumnDefs($scope.colunas);
                //console.log("Tamanho coluna "+$scope.colunas.length);            
            }else{
                $scope.colunas = [periodo,derivadas,derivadaspercent,bolhas,bolhaspercent,menu,menupercent,escolha,escolhapercent];
                $scope.gridDados.api.setColumnDefs($scope.colunas);
            }
            // if(filtro_produto_prepago.length === 0 ){
            //     $scope.colunas.pop();
            //     $scope.gridDados.api.setColumnDefs($scope.colunas);
            //     //console.log("Tamanho coluna 3 "+$scope.colunas.length);
            // }
            
            $scope.gridDados.rowData = [];
            $scope.gridDados.api.showLoadingOverlay();
            limpaProgressBar($scope, "#pag-derivacao-tlv");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.listaDados.apply(this);

        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        $view.on("click", ".btn-exportarCSV-interface", function() {
            

            if ($scope.gridDados.rowData.length > 0) {
                
                // var params = {
                //     allColumns: true,
                //     fileName: 'export_' + Date.now(),
                //     columnSeparator: ';'
                // }
    
                // $scope.gridDados.api.exportDataAsCsv(params);
                var filtro_produto_prepago = $view.find("select.filtro-produto-prepago").val() || [];
                if ($scope.gridDados.rowData.length > 0) {
                separator = ";";
                colunas = "";
                file_name = tempDir3() + "export_" + Date.now();
                
                if(filtro_produto_prepago.length > 0){
                    colunas = 'Período;Produto;Derivadas para TLV;% Derivadas para TLV;Pró ativa (BOLHAS);% Pró ativa (BOLHAS);Sob demanda (SUB-MENU);% Sob demanda (SUB-MENU);Escolha Cliente;% Escolha Cliente;'
            }else{
                colunas = 'Período;Derivadas para TLV;% Derivadas para TLV;Pró ativa (BOLHAS);% Pró ativa (BOLHAS);Sob demanda (SUB-MENU);% Sob demanda (SUB-MENU);Escolha Cliente;% Escolha Cliente;'

            }
                // for (i = 0; i < $scope.gridDados.columnDefs.length; i++) {
                //     colunas = colunas + $scope.gridDados.columnDefs[i].headerName + separator
                // }
                colunas = colunas + "\n"

                for (var i = 0; i < $scope.gridDados.rowData.length; i++) {
                    for (var v in $scope.gridDados.rowData[i]) {
                        if(v !== "derivadasPercent" && v !== "bolhasPercent" && v !== "menuPercent" && v !== "escolhaPercent" ){
                        colunas = colunas + $scope.gridDados.rowData[i][v] + separator;
                    }
                    }
                    colunas = colunas + "\n";
                }
                // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                // }
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'csv', true);
                $scope.csv = [];
                $view.find(".btn-exportar").button('click');
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }}
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

         // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-derivacao-tlv");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');


    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_produto_prepago = $view.find("select.filtro-produto-prepago").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + "" +
            " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }

        if (filtro_produto_prepago.length > 0) {
            $scope.filtros_usados += " Produto: " + filtro_produto_prepago;
        }
        // if (filtro_segmentos.length > 0) {
        //     $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        // }
        // if (filtro_regioes.length > 0) {
        //     $scope.filtros_usados += " Regiões: " + filtro_regioes;
        // }
        //if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }


        //$scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
        if(filtro_produto_prepago.length > 0){
            var stmt = db.use +
            "declare @dataIni datetime = '" + formataDataHora(precisaoHoraIni(data_ini)) + "';"+
            "declare @dataFim datetime = '"+ formataDataHora(precisaoHoraFim(data_fim)) +"';"+
            "declare @codAplicacao varchar(20) = " +restringe_aplicacao(filtro_aplicacoes)+";"+
            "declare @codProduto varchar(30) =" +restringe_aplicacao(filtro_produto_prepago)+";"+
            "with cte as ( "+
            "select convert(date, DatReferencia) as Dia, CodProduto, sum(QtdMenu-QtdMenuIIT) as QtdMenu, sum(QtdBolhas-QtdBolhasIIT) as QtdBolhas, sum(QtdChamadas-QtdChamadasIIT) as QtdEscolhas "+
            "from ResumoAcompanhamentoTLV "+
            "where DatReferencia >= @dataIni and DatReferencia < @dataFim and CodAplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) and CodProduto in(SELECT * FROM [splitList](@codProduto, ',')) "+
            "group by convert(date, DatReferencia), CodProduto ), cte2 as ( "+
            "select Dat_Referencia as Dia, sum(QtdDerivadas+QtdAbandonadas+QtdFinalizadas+QtdTransfTLV) as QtdTotal, sum(QtdTransfTLV) as QtdTLV "+
            "from ResumoDesempenhoGeral "+
            "where Dat_Referencia >= @dataIni and Dat_Referencia < @dataFim "+
            "and Cod_Aplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) and Cod_Segmento = '' "+
            "group by Dat_Referencia) "+
            "select cte2.Dia, cte.CodProduto, cte2.QtdTLV as [Derivadas para TLV], "+
            "cast(100.0 * cte2.QtdTLV / cte2.QtdTotal as decimal(10,2)) as [% Derivadas para TLV], "+
            "cte.QtdBolhas as [Bolhas], cast(100.0 * cte.QtdBolhas / cte2.QtdTotal as decimal(10,2)) as [% Bolhas], "+
            "cte.QtdMenu as [Menu], "+
            "cast(100.0 * cte.QtdMenu / cte2.QtdTotal as decimal(10,2)) as [% Menu], "+
            "cte.QtdEscolhas as [Escolhas], cast(100.0 * cte.QtdEscolhas / cte2.QtdTotal as decimal(10,2)) as [% Escolhas] "+
            "from cte join cte2 on cte.Dia = cte2.Dia order by cte.Dia, cte.CodProduto"
            console.log("Executando query-> " + stmt); 
        }else{
        var stmt = "";
       
            //console.log(restringe_aplicacao(filtro_aplicacoes));
            stmt = db.use + "declare @dataIni datetime = '" + formataDataHora(precisaoHoraIni(data_ini)) + "';"+
            "declare @dataFim datetime = '"+ formataDataHora(precisaoHoraFim(data_fim)) +"';"+
            "declare @codAplicacao varchar(100) = " +restringe_aplicacao(filtro_aplicacoes)+";"+
            "with cteEntrantes as ("+
            " select Dat_Referencia as Dia, sum(QtdDerivadas+QtdAbandonadas+QtdFinalizadas+QtdTransfTLV) as QtdTotal, sum(QtdTransfTLV) as QtdTLV "+
            "from ResumoDesempenhoGeral where Dat_Referencia >= @dataIni and Dat_Referencia < @dataFim "+
            "and Cod_Aplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) and Cod_Segmento = '' group by Dat_Referencia), cteBolha as ("+
            "select DataHora as Dia, sum(Qtd-QtdIIT) as Qtd from ConsolidaItemControleD "+
            "where DataHora >= @dataIni and DataHora < @dataFim and Cod_Aplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) "+
            "and Cod_ItemDeControle in (select CodIC from ICAcompanhamentoTLV where CodAplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) and Categoria = 'BOLHA') "+
            "group by DataHora), cteMenu as (select DataHora as Dia, sum(Qtd-QtdIIT) as Qtd from ConsolidaItemControleD "+
            "where DataHora >= @dataIni and DataHora < @dataFim and Cod_Aplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) "+
            "and Cod_ItemDeControle in (select CodIC from ICAcompanhamentoTLV where CodAplicacao in(SELECT * FROM [splitList](@codAplicacao, ',')) and Categoria = 'MENU') "+
            "group by DataHora) "+
            ", cteEscolha as ("+
            " select convert(date, DatReferencia) as Dia, sum(QtdChamadas-QtdChamadasIIT) as Qtd"+
            " from ResumoAcompanhamentoTLV"+
            " where DatReferencia >= @dataIni and DatReferencia < @dataFim"+
            " and CodAplicacao in(SELECT * FROM [splitList](@codAplicacao, ','))"+
            " group by convert(date, DatReferencia))"+
            "select cteEntrantes.Dia, cteEntrantes.QtdTLV as [Derivadas para TLV], 100.0 * cteEntrantes.QtdTLV / cteEntrantes.QtdTotal as [% Derivadas para TLV], "+
            "isnull(cteBolha.Qtd, 0) as [Bolhas], 100.0 * isnull(cteBolha.Qtd, 0) / cteEntrantes.QtdTotal as [% Bolhas], "+
            "isnull(cteMenu.Qtd, 0) as [Menu], 100.0 * isnull(cteMenu.Qtd, 0) / cteEntrantes.QtdTotal as [% Menu], "+
            "isnull(cteEscolha.Qtd, 0) as [Escolha], "+
            "100.0 * isnull(cteEscolha.Qtd, 0) / cteEntrantes.QtdTotal as [% Escolha] "+
            "from cteEntrantes left outer join cteBolha on cteEntrantes.Dia = cteBolha.Dia "+
            "left outer join cteMenu on cteEntrantes.Dia = cteMenu.Dia "+
            "left outer join cteEscolha on cteEntrantes.Dia = cteEscolha.Dia "+
            "order by cteEntrantes.Dia"

            console.log("Executando query-> " + stmt); 
        }
        function formataData2(data) {
            var y = data.getFullYear(),
                m = data.getMonth() + 1,
                d = data.getDate();
            return y +"/"+ _02d(m) +"/"+ _02d(d);
        }
        function formatpercent(data){
            return parseFloat(data).toFixed(2)+"%"
        }

        db.query(stmt, function(columns) {
            if(filtro_produto_prepago.length > 0)
            { 
                
                $scope.gridDados.rowData.push({
                    procPeriodo: formataData2(columns[0].value),
                    proxProduto: columns[1].value,
                    proxDerivadas: columns[2].value,
                    proxDerivadasPercent: formatpercent(columns[3].value),
                    proxBolhas: columns[4].value,
                    proxBolhasPercent: formatpercent(columns[5].value),
                    proxMenu:columns[6].value,
                    proxMenuPercent: formatpercent(columns[7].value),
                    proxEscolha: columns[8].value,
                    proxEscolhaPercent: formatpercent(columns[9].value),
                    derivadasPercent:columns[3].value,
                    bolhasPercent:columns[5].value,
                    menuPercent:columns[7].value,
                    escolhaPercent:columns[9].value,
                    
                });
            }else{
                
            $scope.gridDados.rowData.push({
                procPeriodo: formataData2(columns[0].value),
                proxDerivadas: columns[1].value,
                proxDerivadasPercent: formatpercent(columns[2].value),
                proxBolhas: columns[3].value,
                proxBolhasPercent: formatpercent(columns[4].value),
                proxMenu:columns[5].value,
                proxMenuPercent: formatpercent(columns[6].value),
                proxEscolha: columns[7].value,
                proxEscolhaPercent: formatpercent(columns[8].value),
                derivadasPercent:columns[2].value,
                bolhasPercent:columns[4].value,
                menuPercent:columns[6].value,
                escolhaPercent:columns[8].value
                
            });
        }

            
    }, function(err, num_rows) {
            if (err) {
                    console.log('Erro ao buscar Dados: ' + err);
            } else {
                  
                console.log('Registros encontrados: ' + num_rows);
                    retornaStatusQuery($scope.gridDados.rowData.length, $scope);
                    if (num_rows > 0) {
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
                        $scope.gridDados.api.showNoRowsOverlay();
                    } else {
                        $scope.gridDados.api.hideOverlay();
                    }
                    $view.find(".btn-gerar").button('reset');
                    $scope.gridDados.api.setRowData($scope.gridDados.rowData);
                    $scope.gridDados.api.refreshCells({force: true});

                    //$btn_gerar_import.button("reset");

                    //limpaProgressBar($scope, "#pag-monitor-teradata");
                  
            }
    });

        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

        $btn_gerar.button('reset');
        $btn_exportar.prop("disabled", false);
        $btn_exportar_csv.prop("disabled", false);
        $btn_exportar_dropdown.prop("disabled", false);
        return
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].headerName);
      }
    var a =[];
    for (var i = 0; i < $scope.gridDados.rowData.length; i++) {
        for (var v in $scope.gridDados.rowData[i]) {
            if(v !== "derivadasPercent" && v !== "bolhasPercent" && v !== "menuPercent" && v !== "escolhaPercent" ){
            a.push($scope.gridDados.rowData[i][v]);
        }
        }
        $scope.csv.push(a);
        a=[];
    }
      	
      console.log(colunas);
      var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'tderivacaotlv_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);


      setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);
  };

    $scope.abreAjuda = function (link) {
        abreAjuda(link);
    }

}
CtrlDerivacaoTLV.$inject = ['$scope', '$globals'];