/*
 * CtrlDerivacoesR
 */
function CtrlMetaURA($scope, $globals) {

	win.title = "Controle de Metas"; //Brunno 11/11/2019
	$scope.versao = versao;
	
	//Alex 08/02/2014
	travaBotaoFiltro(0, $scope, "#pag-controle-metas", "Controle de Metas");

	$scope.dados = [];
	$scope.csv = [];	
	
	var columnDefs = [
		{
			headerName: '',		
			children: [
				{ field: "data_hora_BR", headerName: "Data", width: 130, pinned:true }, 
				{ field: "grupo", headerName: "Grupo", width: 180, filter: 'agTextColumnFilter', pinned:true }
			]
		
		},
		{
			headerName: 'Chamadas',
			children: [
				{headerName: 'Entrantes', columnGroupShow: 'open', field: 'qtdEntrantes', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: 'Finalizadas', columnGroupShow: 'open', field: 'qtdFinalizadas', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: 'Derivadas', columnGroupShow: 'open', field: 'qtdDerivadas', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: 'Abandonadas', columnGroupShow: 'open', field: 'qtdAbandonadas', width: 125, filter: 'agNumberColumnFilter'}
				
			]
		},
		{
			headerName: 'Repetidas',
			children: [
				{headerName: 'Telefone', columnGroupShow: 'open', field: 'qtdRepetidasTel', width: 100, filter: 'agNumberColumnFilter'},
				{headerName: 'CPF', columnGroupShow: 'open', field: 'qtdRepetidasCPF', width: 80, filter: 'agNumberColumnFilter'},
				{headerName: '2h até 24h', columnGroupShow: 'open', field: 'qtdRepetidasTel2hAte24h', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '1h até 2h', columnGroupShow: 'open', field: 'qtdRepetidasTel1hAte2h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '45min até 1h', columnGroupShow: 'open', field: 'qtdRepetidasTel45minAte1h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '30min até 45mim', columnGroupShow: 'open', field: 'qtdRepetidasTel30minAte45min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: '15min até 30mim', columnGroupShow: 'open', field: 'qtdRepetidasTel15minAte30min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: 'até 15mim', columnGroupShow: 'open', field: 'qtdRepetidasTelAte15min', width: 125, filter: 'agNumberColumnFilter'}
			]
		},	
		
		{
			headerName: 'Retidas Líquidas',
			children: [
				{headerName: '24h', columnGroupShow: 'open', field: 'qtdRetidasLiq24h', width: 90, filter: 'agNumberColumnFilter'},
				{headerName: '2h', columnGroupShow: 'open', field: 'qtdRetidasLiq2h', width: 90, filter: 'agNumberColumnFilter'},
				{headerName: '1h', columnGroupShow: 'open', field: 'qtdRetidasLiq1h', width: 90, filter: 'agNumberColumnFilter'},
				{headerName: '45min', columnGroupShow: 'open', field: 'qtdRetidasLiq45min', width: 90, filter: 'agNumberColumnFilter'},
				{headerName: '30min', columnGroupShow: 'open', field: 'qtdRetidasLiq30min', width: 90, filter: 'agNumberColumnFilter'},
				{headerName: '15min', columnGroupShow: 'open', field: 'qtdRetidasLiq15min', width: 90, filter: 'agNumberColumnFilter'}
			]
		},		
		{
			headerName: 'Repetidas após retidas',
			children: [
				{headerName: '2h até 24h', columnGroupShow: 'open', field: 'qtdRepDerAposRetTel2hAte24h', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '1h até 2h', columnGroupShow: 'open', field: 'qtdRepAposRetTel1hAte2h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '45min até 1h', columnGroupShow: 'open', field: 'qtdRepAposRetTel45minAte1h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '30min até 45mim', columnGroupShow: 'open', field: 'qtdRepAposRetTel30minAte45min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: '15min até 30mim', columnGroupShow: 'open', field: 'qtdRepAposRetTel15minAte30min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: 'até 15mim', columnGroupShow: 'open', field: 'qtdRepAposRetTel15min', width: 125, filter: 'agNumberColumnFilter'}
			]
		},
		{
			headerName: 'Repetidas derivadas após retidas',
			children: [
				{headerName: '2h até 24h', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel2hAte24h', width: 110, filter: 'agNumberColumnFilter'},
				{headerName: '1h até 2h', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel1hAte2h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '45min até 1h', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel45minAte1h', width: 125, filter: 'agNumberColumnFilter'},
				{headerName: '30min até 45mim', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel30minAte45min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: '15min até 30mim', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel15minAte30min', width: 150, filter: 'agNumberColumnFilter'},
				{headerName: 'até 15mim', columnGroupShow: 'open', field: 'qtdRepDerivAposRetTel15min', width: 125, filter: 'agNumberColumnFilter'}
			]
		}
	];

	$scope.gridOptions = {
		defaultColDef: {
			sortable: true,
			resizable: true,
			filter: true
		},
		debug: true,
		columnDefs: [],
		rowData: []
	};

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		sites: [],
		segmentos: [],
		chamador: ""
	};

	$scope.tabela = "";
	$scope.valor = "24h";
	$scope.datahora = "data"	
	var sufixo = "";
	var sufixoTabAnt = "";

	// Filtros: data e hora
	var agora = new Date();


	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
	};

	var $view;

	$scope.$on('$viewContentLoaded', function () {
		$view = $("#pag-controle-metas");

		componenteDataMaisHora($scope, $view);

		//ALEX - Carregando filtros
		carregaGruposMeta($view, false,$scope);

		$view.on("click", "#alinkMeta", function(){ marcaTodosIndependente($('.filtro-grupos-meta'),'sites')});
		
		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {
			limpaProgressBar($scope, "#pag-controle-metas");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio,
				data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini, data_fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}		
			
			$scope.listaDados.apply(this);
		});

		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora($scope, $view, true);
			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');
			$('select.filtro-grupos-meta').removeAttr( "disabled" ).selectpicker('refresh');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});


		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-controle-metas");
		}

		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	// Lista chamadas conforme filtros
	$scope.listaDados = function () {	

		var filtro_grupos = $view.find("select.filtro-grupos-meta").val() || [];		

		var $btn_exportar = $view.find(".btn-exportar");
		$btn_exportar.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;


		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
			" até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
		if (filtro_grupos.length > 0) {
			$scope.filtros_usados += " Grupos: " + filtro_grupos;
		}
		
		$scope.dados = [];
		$scope.csv = [];
		
		var stmt = db.use;	
	
		stmt += "Select [Dia], c.[Grupo], [QtdDerivadas], [QtdFinalizadas], [QtdAbandonadas], [QtdRepetidas]	,[QtdRepetidasFinal]"+
		",[QtdRepetidasDerivAposFinal],[QtdRepetidasTel],[QtdRepetidasCPF],[QtdChamadasTel],[QtdChamadasCPF],[QtdClientesTel]"+
		",[QtdClientesCPF],[QtdRepetidasTel2h],[QtdRepetidasTel1h],[QtdRepetidasTel45min],[QtdRepetidasTel30min],[QtdRepetidasTel15min]"+
		",[QtdRetidasLiq24h],[QtdRetidasLiq2h],[QtdRetidasLiq1h],[QtdRetidasLiq45min],[QtdRetidasLiq30min],[QtdRetidasLiq15min]"+
		",[QtdRepetidasAposRetTel],[QtdRepetidasAposRetTel2h],[QtdRepetidasAposRetTel1h],[QtdRepetidasAposRetTel45min],[QtdRepetidasAposRetTel30min]"+
		",[QtdRepetidasAposRetTel15min],[QtdRepDerivAposRetTel],[QtdRepDerivAposRetTel2h],[QtdRepDerivAposRetTel1h],[QtdRepDerivAposRetTel45min]"+
		",[QtdRepDerivAposRetTel30min],[QtdRepDerivAposRetTel15min] from ControleMetasURA c inner join GruposMeta g on c.grupo = g.descricao"
		stmt += " WHERE 1 = 1" +
			" AND Dia >= '" + formataDataHora(data_ini) + "'" +
			" AND Dia <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'"
		stmt += restringe_consulta("g.grupo", filtro_grupos, true);
		

		log(stmt);
		
		$scope.gridOptions.api.setColumnDefs([]);
		$scope.gridOptions.api.setRowData([]);


		function executaQuery(columns) {
			
			var data_hora = columns[0].value,
				data_hora_BR = typeof data_hora === 'string' ? formataDataBRString(data_hora) : formataDataHoraBR(data_hora),
				grupo = (columns[1].value),				
				
				qtdFinalizadas = +columns["QtdFinalizadas"].value,
				qtdDerivadas = +columns["QtdDerivadas"].value,
				qtdAbandonadas = +columns["QtdAbandonadas"].value,
				qtdEntrantes = qtdFinalizadas+qtdDerivadas+qtdAbandonadas,
				qtdRetidas = qtdFinalizadas + qtdAbandonadas,
				
				qtdRepetidasTel = +columns["QtdRepetidasTel"].value,
				qtdRepetidasCPF = +columns["QtdRepetidasCPF"].value,				
				
				qtdRepetidasTel2hAte24h = columns["QtdRepetidasTel"].value - columns["QtdRepetidasTel2h"].value,
				qtdRepetidasTel1hAte2h = columns["QtdRepetidasTel2h"].value - columns["QtdRepetidasTel1h"].value,
				qtdRepetidasTel45minAte1h = columns["QtdRepetidasTel1h"].value - columns["QtdRepetidasTel45min"].value,
				qtdRepetidasTel30minAte45min = columns["QtdRepetidasTel45min"].value - columns["QtdRepetidasTel30min"].value,
				qtdRepetidasTel15minAte30min = columns["QtdRepetidasTel30min"].value - columns["QtdRepetidasTel15min"].value,
				qtdRepetidasTelAte15min = columns["QtdRepetidasTel15min"].value,
				
				qtdRetidasLiq24h = +columns["QtdRetidasLiq24h"].value,
				qtdRetidasLiq2h = +columns["QtdRetidasLiq2h"].value,
				qtdRetidasLiq1h = +columns["QtdRetidasLiq1h"].value,
				qtdRetidasLiq45min = +columns["QtdRetidasLiq45min"].value,
				qtdRetidasLiq30min = +columns["QtdRetidasLiq30min"].value,
				qtdRetidasLiq15min = +columns["QtdRetidasLiq15min"].value,
				

				qtdRepDerAposRetTel2hAte24h   = columns["QtdRepetidasAposRetTel"].value - columns["QtdRepetidasAposRetTel2h"].value,
				qtdRepAposRetTel1hAte2h       = columns["QtdRepetidasAposRetTel2h"].value - columns["QtdRepetidasAposRetTel1h"].value,
				qtdRepAposRetTel45minAte1h    = columns["QtdRepetidasAposRetTel1h"].value - columns["QtdRepetidasAposRetTel45min"].value,
				qtdRepAposRetTel30minAte45min = columns["QtdRepetidasAposRetTel45min"].value - columns["QtdRepetidasAposRetTel30min"].value,
				qtdRepAposRetTel15minAte30min = columns["QtdRepetidasAposRetTel30min"].value - columns["QtdRepetidasAposRetTel15min"].value,
				qtdRepAposRetTel15min         = columns["QtdRepetidasAposRetTel15min"].value,

				qtdRepDerivAposRetTel2hAte24h      = columns["QtdRepDerivAposRetTel"].value - columns["QtdRepDerivAposRetTel2h"].value,
				qtdRepDerivAposRetTel1hAte2h       = columns["QtdRepDerivAposRetTel2h"].value - columns["QtdRepDerivAposRetTel1h"].value,
				qtdRepDerivAposRetTel45minAte1h    = columns["QtdRepDerivAposRetTel1h"].value - columns["QtdRepDerivAposRetTel45min"].value,
				qtdRepDerivAposRetTel30minAte45min = columns["QtdRepDerivAposRetTel45min"].value - columns["QtdRepDerivAposRetTel30min"].value,
				qtdRepDerivAposRetTel15minAte30min = columns["QtdRepDerivAposRetTel30min"].value - columns["QtdRepDerivAposRetTel15min"].value,
				qtdRepDerivAposRetTel15min         = columns["QtdRepDerivAposRetTel15min"].value;
			


			$scope.dados.push({
				data_hora: data_hora,
				data_hora_BR: $scope.datahora === "hora"?data_hora_BR:data_hora_BR.substring(0, 10),
				grupo: grupo,
				
				qtdEntrantes: qtdEntrantes,
				qtdDerivadas:qtdDerivadas,
				qtdRetidas: qtdRetidas,
				qtdFinalizadas:qtdFinalizadas,
				qtdAbandonadas:qtdAbandonadas,			
				
				qtdRepetidasTel: qtdRepetidasTel,
				qtdRepetidasCPF: qtdRepetidasCPF,

				qtdRepetidasTel2hAte24h: qtdRepetidasTel2hAte24h,
				qtdRepetidasTel1hAte2h: qtdRepetidasTel1hAte2h,
				qtdRepetidasTel45minAte1h: qtdRepetidasTel45minAte1h,
				qtdRepetidasTel30minAte45min: qtdRepetidasTel30minAte45min,
				qtdRepetidasTel15minAte30min: qtdRepetidasTel15minAte30min,
				qtdRepetidasTelAte15min: qtdRepetidasTelAte15min,

				qtdRetidasLiq24h: qtdRetidasLiq24h,
				qtdRetidasLiq2h: qtdRetidasLiq2h,
				qtdRetidasLiq1h: qtdRetidasLiq1h,
				qtdRetidasLiq45min: qtdRetidasLiq45min,
				qtdRetidasLiq30min: qtdRetidasLiq30min,
				qtdRetidasLiq15min: qtdRetidasLiq15min,
				
				qtdRepDerAposRetTel2hAte24h:qtdRepDerAposRetTel2hAte24h,
				qtdRepAposRetTel1hAte2h:qtdRepAposRetTel1hAte2h,
				qtdRepAposRetTel45minAte1h:qtdRepAposRetTel45minAte1h,
				qtdRepAposRetTel30minAte45min:qtdRepAposRetTel30minAte45min,
				qtdRepAposRetTel15minAte30min:qtdRepAposRetTel15minAte30min,
				qtdRepAposRetTel15min:qtdRepAposRetTel15min,


				qtdRepDerivAposRetTel2hAte24h:qtdRepDerivAposRetTel2hAte24h,
				qtdRepDerivAposRetTel1hAte2h:qtdRepDerivAposRetTel1hAte2h,
				qtdRepDerivAposRetTel45minAte1h:qtdRepDerivAposRetTel45minAte1h,
				qtdRepDerivAposRetTel30minAte45min:qtdRepDerivAposRetTel30minAte45min,
				qtdRepDerivAposRetTel15minAte30min:qtdRepDerivAposRetTel15minAte30min,
				qtdRepDerivAposRetTel15min:qtdRepDerivAposRetTel15min,

			});

			$scope.csv.push([
				data_hora_BR.substring(0, 10),
				grupo,
				
				qtdEntrantes,
				qtdDerivadas,
				qtdRetidas,
				qtdFinalizadas,
				qtdAbandonadas,			
				
				qtdRepetidasTel,
				qtdRepetidasCPF,

				qtdRepetidasTel2hAte24h,
				qtdRepetidasTel1hAte2h,
				qtdRepetidasTel45minAte1h,
				qtdRepetidasTel30minAte45min,
				qtdRepetidasTel15minAte30min,
				qtdRepetidasTelAte15min,

				qtdRetidasLiq24h,
				qtdRetidasLiq2h,
				qtdRetidasLiq1h,
				qtdRetidasLiq45min,
				qtdRetidasLiq30min,
				qtdRetidasLiq15min,
				
				qtdRepDerAposRetTel2hAte24h,
				qtdRepAposRetTel1hAte2h,
				qtdRepAposRetTel45minAte1h,
				qtdRepAposRetTel30minAte45min,
				qtdRepAposRetTel15minAte30min,
				qtdRepAposRetTel15min,


				qtdRepDerivAposRetTel2hAte24h,
				qtdRepDerivAposRetTel1hAte2h,
				qtdRepDerivAposRetTel45minAte1h,
				qtdRepDerivAposRetTel30minAte45min,
				qtdRepDerivAposRetTel15minAte30min,
				qtdRepDerivAposRetTel15min,

			]);	
		}

		db.query(stmt, executaQuery, function (err, num_rows) {
			console.log("Executando query-> " + stmt + " " + num_rows);
			retornaStatusQuery(num_rows, $scope);
			$btn_gerar.button('reset');

			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$scope.gridOptions.columnDefs = columnDefs;				
				$scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
				$scope.gridOptions.api.setRowData($scope.dados);				
			}
		});
	};
	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);
		$btn_exportar.button('loading');

		if ($scope.dados.length > 0) {
			console.log($scope.dados)  
			var filtro_grupos = $view.find("select.filtro-grupos-meta").val() || [];
			if ($scope.dados.length > 0) {
			separator = ";";
			colunas = "";
			file_name = tempDir3() + "export_" + Date.now();
			
			if($scope.gridOptions.columnDefs.length > 0){
				
				for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
				
				
					
				
				for (var x = 0; x < $scope.gridOptions.columnDefs[i].children.length; x++) {
				
				colunas += $scope.gridOptions.columnDefs[i].headerName+" "	
				colunas += $scope.gridOptions.columnDefs[i].children[x].headerName +';'
				
				}
			}
		}
			colunas = colunas + "\n"

			$scope.csv.map(function(num) {
					
			num.map(function(dados){
				colunas += dados+separator;
				
			})	
			return colunas += "\n"
				  });
				colunas = colunas + "\n";

			// for (i = 0; i < $scope.gridDados.rowData.length; i++) {
			//     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
			// }
			//$(".dropdown-exportar").toggle();
			exportaTXT(colunas, file_name, 'csv', true);
			$scope.csv = [];
			$view.find(".btn-exportar").button('click');
			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);
		} else {
			$('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
			setTimeout(function () {
				$btn_exportar.button('reset');
			}, 500);
		}}

	};
}
CtrlMetaURA.$inject = ['$scope', '$globals'];