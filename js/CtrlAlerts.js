function CtrlAlerts($scope, $globals) {

	win.title="Controle de Alertas"; //Matheus 28/02/2017
	$scope.versao = versao;

	$scope.sqlBusy = false;
  
	$scope.rotas = rotas;
	//$scope.usuario = {};
	$scope.alertsData = []; // Dados do grid de usuários
    
	//Colunas do grid de alertas
	$scope.colunas = [
		{ field: "Alerta", displayName: "Nome", cellClass: "grid-align", pinned: true },// cellClass:"span1", width: 150,
		//{ field: "Query", displayName: "Query", cellClass: "grid-align", width: '40%', pinned: true },// cellClass:"span2", width: 80,
		{ field: "Delay", displayName: "Intervalo", cellClass: "grid-align", width: 80, pinned: true },//  cellClass:"span1", width: 80,
		{ field: "Para", displayName: "Email", cellClass: "grid-align", pinned: true },// cellClass:"span2",width: 80,
		//{ field: "CC", displayName: "Copia", cellClass: "grid-align", pinned: false},//  cellClass:"span4", width: 120,
		{ field: "Tools", displayName: "Funções",cellClass: "grid-align", width: 80, pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a ng-click="editAlert(row.entity)" title="Editar" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
	];

	$scope.filtros = {
		filterText: ""
	};

	// Filtro de UsuariosEstatura
	/*$scope.filterUser = function() {
		var filterText = $scope.userFilter; ///'name:' +
		if (filterText !== '') {
		  $scope.filtros.filterText = filterText;
		} else {
		  $scope.filtros.filterText = '';
		}
	  };*/
	// FIm do Filtro

	// Grid de usuários
	$scope.gridDados = {
		data: "alertsData",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true/*,
		filterOptions: $scope.filtros*/
		/*
		showFilter: true*/
	};

	// $scope.permFiltros = {
		// filterText: "",
		// useExternalFilter: true
	// };
	//* Tree view responsável pelo menu de navegação
  

    var $view;

    $scope.$on('$viewContentLoaded', function () {
      
      
		
    $view = $("#pag-alertas");
    treeView('#pag-alertas #chamadas', 15, 'Chamadas', 'dvchamadas');
    treeView('#pag-alertas #repetidas', 35, 'Repetidas', 'dvRepetidas');
    treeView('#pag-alertas #ed', 65, 'ED', 'dvED');
    treeView('#pag-alertas #ic', 95, 'IC', 'dvIC');
    treeView('#pag-alertas #tid', 115, 'TID', 'dvTid');
    treeView('#pag-alertas #vendas', 145, 'Vendas', 'dvVendas');
    treeView('#pag-alertas #falhas', 165, 'Falhas', 'dvFalhas');
    treeView('#pag-alertas #extratores', 195, 'Extratores', 'dvExtratores');
    treeView('#pag-alertas #parametros', 225, 'Parametros', 'dvParam');
    treeView('#pag-alertas #admin', 255, 'Administrativo', 'dvAdmin');
    treeView('#pag-alertas #monitoracao', 275, 'Monitoração', 'dvReparo');
    treeView('#pag-alertas #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
    treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');
     $("#pag-alertas .botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
	 
	 // Fim do treeview responsável pelo menu de navegação

		//carregaRotas($view);

		$view.find("select.filtro-rotas").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Permissões',
			showSubtext: true
		});

		$("#pag-alertas #modal-register").on('hidden', function () {
			$("#pag-alertas .registerAlert").hide();
			$("#pag-alertas .registerAlertText").text("");
			$("#pag-alertas .container button").blur();
			//console.log("Blurred buttons");
		});

		// Inflam o grid dentro do modal
		
		$scope.listaAlerts();

		//preenchendo list com os alertas

		var options2 = [];
		cache.relatorios.forEach(function (form) {

				  options2.push('<option value="' + form.codigo + '">' + form.nome + '</option>');

		});

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$view.on("click", ".modal-backdrop", function () {
			$(document).focus();
			//console.log("Clicou no backdrop");
		});

		$view.on("click", ".close", function () {
			$(document).focus();
			//console.log("Clicou no fechar modal");
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		/*$view.on("click", ".icon-search", function () {
			var $this = $(this);
			var rowLogin = $this.data("value");
			// data-value="{{row.getProperty(\'LoginUsuario\')}}" Removido
			//console.log("Row Login: "+rowLogin);
		});*/


		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-alertas");
		}
    
	});
  
  $scope.novoAlerta = function(){
    $(".modal-register").modal('show');
    console.log("Show Modal Register");
  }
  
  
  
  	function populateAlerts(columns){
        var gridNome = columns[0].value,
				gridQuery = columns[1].value,
				gridDelay = columns[2].value,
				gridTo = columns[3].value,
				gridCC = columns[4].value,
        gridCCO = columns[5].value,
        gridDataCadastro = columns[6].value,
				inutil = "";

			 dado = {
					Alerta: gridNome,
					Query: gridQuery,
					Delay: gridDelay,
					Para: gridTo,
					Copia: gridCC,
          DataCadastro: gridDataCadastro
			};


		$scope.alertsData.push(dado);
	}
  
  function carregaAlerts(quantidade){
				// Função responsável por selecionar quantidade de usuários para preencher inicialmente o grid vinculado a array
				array = [];
				stmt = "";
				
				if(quantidade>0){
					stmt = "Select top "+quantidade+" Nome, Query, Delay, Para, CC, CCO, DataCadastro from solicitacaoAlertas ORDER BY DataCadastro DESC";
				}else{
					stmt = "Select Nome, Query, Delay, Para, CC, CCO, DataCadastro from solicitacaoAlertas ORDER BY DataCadastro DESC";
				}
				console.log("Carregando ALERTAS; "+stmt);
				
				db.query(stmt, populateAlerts, function(err, num_rows){
					
					if(err){
						console.log("Erro na sintaxe: "+err);
					}
					
					retornaStatusQuery(num_rows, $scope);
					console.log(""+num_rows);
				});
			}
  
	$scope.listaAlerts = function (){
    //$btn_gerar = $(this);
		//$btn_gerar.button('loading');
		$scope.alertsData = [];

		//console.log("Listando Users");

		// Fim da permissão responsavel por popular o grid de usuários
		carregaAlerts(0);

		//setTimeout($btn_gerar.button('reset'), 600);

	};
  
	$scope.registraAlerta = function (){
    
			var sAlert = $scope.Alerta;
			var sIntervalo = $scope.Intervalo;
			var sQuery = $scope.Query;
			var sEmail = $scope.Email;
			var sCopy = $scope.Copy || "";
      
    function checkNameToRegister(){    
			
			stmt = "Select Nome from solicitacaoAlertas where Nome = '"+sAlert+"'";
			function getRetorno(columns){
				colunax = columns[0].value;
				if(colunax==sAlert){
					console.log("Alerta com nome "+sAlert+" já existente no banco de dados!!");
				}
			}
			
			db.query(stmt, getRetorno, function(err, num_rows){
					if(err){
						console.log("Erro na sintaxe: "+err);
					}
					
					if(num_rows>=1){
						
						alerte("Alerta com nome "+sAlert+" já existente no banco de dados, utilize outro nome.", "Falha em execução de cadastro.");
					}
					
					if(num_rows==0){
						console.log("Não existe no banco Rows: "+num_rows);
              registerAlert();
					}
					
			});
		}

    function registerAlert(){
            
        var stmt = db.use
        + "INSERT " + db.prefixo + " solicitacaoAlertas(Nome,Query,Delay,Para,CC)"
        +" VALUES "
        +"('"+sAlert+"','"+sQuery.replace("'","''")+"','"+sIntervalo+"',"
        +"'"+sEmail+"','"+sCopy+"')";
        
        
        
				db.query(stmt, function(){}, function(err, num_rows){
					userLog(stmt, "Cadastro de Alerta", 1, err);
					if(err){
						console.log("Erro na sintaxe: "+err);
            alerte("Erro ao registrar alerta: "+err, "Falha no cadastro");
					}
          
          alerte("Novo alerta '"+sAlert+"' cadastrado com sucesso.", "Ação executada.");
        setTimeout(function(){
          $("#bootstrap-alert-box-modal").modal('hide');
          $(".modal-register").modal('hide');
          $scope.Alerta = "";
          $scope.Intervalo = "";
          $scope.Query = "";
          $scope.Email = "";
          $scope.Copy = "";
        }, 2000);
          //
          $scope.listaAlerts();
				});
        
        
    }

		function validForm(){
      console.log("Verificando formulário!")
			$("#pag-alertas .registerAlertText").text("");
			//var sCpf = $scope.Cpf;

			//console.log("Login registrado: "+sLogin+" Email: "+sEmail);
      //Alerta, Intervalo, Query, Email, Copy
			if(
				(sAlert==""||sAlert==undefined)||
					(sIntervalo==""||sIntervalo==undefined)||
						(sQuery==""||sQuery==undefined)||
							(sEmail==""||sEmail==undefined)
				){
				var resultstring = "Favor preencha ";

				if(sAlert==""||sAlert==undefined){
					resultstring = resultstring+"Nome do alerta, ";
					sAlert="";
				}
				if(sIntervalo==""||sIntervalo==undefined){
					resultstring = resultstring+"Intervalo, ";
					sIntervalo="";
				}
				if(sQuery==""||sQuery==undefined){
					resultstring = resultstring+"SQL Query, ";
					sQuery="";
				}
				if(sEmail==""||sEmail==undefined){
					resultstring = resultstring+"email, ";
					sEmail="";
				}
				resultstring = resultstring+" e tente novamente.";
				//console.log("Error: "+resultstring);
				$("#pag-alertas .registerAlertText").text(resultstring);
				$("#pag-alertas .registerAlert").show();
				return false;
			}
			/*if(!$scope.UserController.$valid){
				$("#pag-alertas .registerAlertText").text("Formulário Inválido. "+$scope.UserController.$valid);
				$("#pag-alertas .registerAlert").show();
				return false;
			}*/

		$("#pag-alertas .registerAlert").hide();
			return true;
		}

		if(validForm()){
				checkNameToRegister()
				setTimeout(function(){
					$("#pag-alertas #modal-register").modal('hide');
					$("#pag-alertas #modal-register input").val("");
				}, 500);
		}else{
      //alerte
      console.log("INFORMAÇÔES INVALIDAS"+"Preencha corretamente o formulário");
    }

	};
	
	$scope.editAlert = function (row){
    
    $(".modal-view").modal('show');
    
    /* Colinha
   Alerta: gridNome,
	 Query: gridQuery,
	 Delay: gridDelay,
	 Para: gridTo,
	 Copia: gridCC,
   DataCadastro: gridDataCadastro
    */
    
      $scope.oldAlerta = row.Alerta;
      $scope.oldEmail = row.Para;
			$scope.editAlerta= row.Alerta;
			$scope.editQuery= row.Query;
			$scope.editIntervalo= row.Delay || "";
			$scope.editEmail= row.Para || "";
			$scope.editCopy= row.Copia || "";
		$view.find(".row1").css('line-height', 'normal');
    
	};

   $scope.updateAlert = function (row){
     
      oldA =$scope.oldAlerta;
      oldE =$scope.oldEmail;
			newAlerta = $scope.editAlerta;
			newQuery = $scope.editQuery;
			newDelay = $scope.editIntervalo;
			newEmail = $scope.editEmail;
			newCopy = $scope.editCopy;
    
    if(!newAlerta||!newQuery||!newEmail){
      alerte("Por favor, preencha as informações em branco para que as alterações sejam salvas.", "Falha ao atualizar serviço de alerta.");
      return false;
    }
    
    
    var updateStmt = "update solicitacaoAlertas set Nome = '"+newAlerta+"', Query = '"+newQuery.replace("'","''")+"', Delay = '"+newDelay+"', para = '"+newEmail+"', CC = '"+newCopy+"' where Nome = '"+oldA+"' AND para = '"+oldE+"' ";
    //console.log("Trying to updateUser");
    
    log(updateStmt);
    db.query(updateStmt, function(){}, function(err, num_rows){
      userLog(stmt, "Alteração de Alerta", 3, err);
      if(err){
        //console.log("Erro ao atualizar usuário: "+err);
          
          alerte("Falha ao atualizar serviço de alerta  '"+oldA+"' para "+oldE+".", "Falha em execução.")
        setTimeout(function(){
          $("#bootstrap-alert-box-modal").modal('hide');
        }, 2000);
      }else{
          $scope.oldAlerta = newAlerta;
          $scope.oldEmail = newEmail;
          alerte("Serviço de alerta  '"+oldA+"' para "+oldE+" atualizado com sucesso.", "Ação executada.");
        setTimeout(function(){
          $("#bootstrap-alert-box-modal").modal('hide');
        }, 2000);
        $(".modal-view").modal('hide');
        $scope.listaAlerts();
      }
      
    });
		
	};
  
  
  
  $scope.deleteAlerta = function(){    
  
      oldA = $scope.oldAlerta;
      oldE = $scope.oldEmail;
      var sAlert = $scope.editAlerta;
      
        function deleteAlert(){
            
        var stmt = "Delete FROM solicitacaoAlertas WHERE Nome =  '"+oldA+"' AND Para = '"+oldE+"'";
        
        
    db.query(stmt, function(){}, function(err, num_rows){
      userLog(stmt, "Remoção de Alerta", 4, err);
     
     if(err){
      console.log("Erro na sintaxe: "+err);
            alerte("Erro ao apagar alerta: "+err, "Falha na exclusão");
     }else{
          
            alerte("Alerta '"+sAlert+"' deletado com sucesso.", "Ação executada.");
            setTimeout(function(){
              $("#bootstrap-alert-box-modal").modal("hide");
              $(".modal-view").modal("hide");
              $scope.Alerta = "";
              $scope.Intervalo = "";
              $scope.Query = "";
              $scope.Email = "";
              $scope.Copy = "";
            }, 2000);
            $scope.listaAlerts();
          }
    
        });       
    }
            
      confirme("Você deseja apagar "+sAlert+" da lista de alertas?", "Excluir", "Sim", function(realize){
        if(realize){
          deleteAlert();
          
        }else{
          console.log("Desistiu de apagar.");
        }
        
      });
      
  };

  
}



CtrlAlerts.$inject = ['$scope','$globals'];
