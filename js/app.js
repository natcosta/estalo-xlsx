function _try(a) {
    a()
}

function forEachPair(a, t) {
    for (var e in a) t(a[e], e)
}

function sortHashMap(a, t) {
    t || (t = function(a, t) {
        return a.value - t.value
    })
    var e = []
    for (var o in a) e.push({
        key: o,
        value: a[o]
    })
    return e.sort(t), e
}

function _02d(a) {
    return (9 > a ? "0" : "") + a
}

function formataDataHora(a) {
    var t = a.getFullYear(),
        e = a.getMonth() + 1,
        o = a.getDate(),
        r = a.getHours(),
        s = a.getMinutes(),
        i = a.getSeconds()
    return t + "-" + _02d(e) + "-" + _02d(o) + " " + _02d(r) + ":" + _02d(s) + ":" + _02d(i)
}

function alternaTelas() {
    d3.select("#tela" + telaAtiva).style("left", "-3000px"), telaAtiva === telas.length && (telaAtiva = 0), telaAtiva++, d3.select("#header .center").html(d3.select("#tela" + telaAtiva + " h1").html()), d3.select("#tela" + telaAtiva).style("left", "0"), timeoutIDs.alternaTelas = setTimeout(alternaTelas, telas[telaAtiva - 1].duracao)
}

function verificaTamanhoTela(a) {
    function t(a) {
        void 0 === a && (a = !0)
        var r = document.getElementById("tela0").getBoundingClientRect()
        a && (e !== r.width || o !== r.height) && charts.forEach(function(a) {
            a(0, 700)
        }), e = r.width, o = r.height, timeoutIDs.verificaTamanhoTela = setTimeout(t, 1e3)
    }
    var e, o
    t(a)
}

function obtemDadosReparo(a) {
    function t(t) {
        return t >= 0 && t < r.length - 1 ? void r[t + 1]() : (t === r.length - 1 && (_try(function() {
            atualizaVisualizacao(e, o)
        }), _dados = e, _dados2 = o, void 0 === timeoutIDs.alternaTelas && alternaTelas(), void 0 === timeoutIDs.verificaTamanhoTela && verificaTamanhoTela(!1)), setTimeout(function() {
            obtemDadosReparo(a)
        }, 6e4), void setTimeout(function() {
            document.location.reload(!0)
        }, 36e5))
    }
    var e = {
            grupos: {},
            gruposMin: {},
            datas: {},
            datasMin: {},
            datasOrdenadas: [],
            datasMinOrdenadas: []
        },
        o = {
            grupos: {},
            datas: {},
            datasOrdenadas: []
        },
        r = []
    r.push(function() {
        e.datas = {}
        var o = uspObtemDadosReparo.replace("$dataIni", "DATEADD(hour, DATEDIFF(hour, 0, DATEADD(minute, DATEDIFF(minute, 0, GETDATE()) - 10 * 60, 0)), 0)").replace("$dataFim", "GETDATE()")
        console.log(o), a.query(o, processaRegistroHora(e), function(a) {
            if (a) return console.log(a), void t(-1)
            console.log("Preparando dados..."), e.datasOrdenadas = []
            for (var o in e.datas) e.datasOrdenadas.push(o)
            e.datasOrdenadas.sort(), e.totaisArea3 = preparaSeriesTemporais(e, "hora", 1, 10), console.log(e.totaisArea3), t(0)
        })
    }), r.push(function() {
        o.datas = {}
        var e = uspObtemDadosReparo.replace("$dataIni", "DATEADD(hour, DATEDIFF(hour, 0, DATEADD(minute, DATEDIFF(minute, 0, GETDATE()) - 7 * 24 * 60 - 600, 0)), 0)").replace("$dataFim", "DATEADD(hour, DATEDIFF(hour, 0, DATEADD(minute, DATEDIFF(minute, 0, GETDATE()) - 7 * 24 * 60, 0)), 0)")
        console.log(e), a.query(e, processaRegistroHora(o), function(a) {
            if (a) return console.log(a), void t(-1)
            o.datasOrdenadas = []
            for (var e in o.datas) o.datasOrdenadas.push(e)
            o.datasOrdenadas.sort(), o.totaisArea3 = preparaSeriesTemporais(o, "hora", 1, 10), console.log(o.totaisArea3), t(1)
        })
    }), r.push(function() {
        e.datasMin = {}
        var o = uspObtemDadosReparoMin.replace("$dataIni", "DATEADD(minute, DATEDIFF(minute, 0, GETDATE()) - 70, 0)").replace("$dataFim", "GETDATE()")
        console.log(o), a.query(o, processaRegistroMin(e), function(a) {
            if (a) return console.log(a), void t(-1)
            e.datasMinOrdenadas = []
            for (var o in e.datasMin) e.datasMinOrdenadas.push(o)
            e.datasMinOrdenadas.sort(), e.totaisArea4 = preparaSeriesTemporais(e, "minuto", 1, 60, function(a) {
                return Date.now() - 39e5 <= a && a < Date.now() - 3e5
            }), console.log(e.totaisArea4), t(2)
        })
    }), r[0]()
}

function processaRegistroHora(a) {
    return function(t) {
        var e = t[0].value,
            o = t[1].value,
            r = t[2].value,
            s = +t[3].value,
            i = +t[4].value
        if (e.match(/\d\d/)) {
            var n
            a.grupos[e] || (a.grupos[e] = {}), n = a.grupos[e]
            var d = o
            o = +o, a.datas[o] = !0, n[o] || (n[o] = {
                date: d,
                total: 0,
                qtdReparosFixo: 0,
                qtdReparosVelox: 0,
                qtdReparosTV: 0,
                qtdChamadasFixa: 0,
                qtdChamadasTV: 0
            }), n = n[o], "FIXO" === r ? (n.qtdReparosFixo = s, n.qtdChamadasFixa = i, n.percentReparosFixo = 0 !== i ? 100 * s / i : 0) : "VELOX" === r ? (n.qtdReparosVelox = s, n.qtdChamadasFixa = i, n.percentReparosVelox = 0 !== i ? 100 * s / i : 0) : "TV" === r && (n.qtdReparosTV = s, n.qtdChamadasTV = i, n.percentReparosTV = 0 !== i ? 100 * s / i : 0), n.total += s, n.totalPercentual = n.total / (n.qtdChamadasFixa + n.qtdChamadasTV)
        }
    }
}

function processaRegistroMin(a) {
    return function(t) {
        var e = t[0].value,
            o = t[1].value,
            r = +t[2].value,
            s = +t[3].value,
            i = +t[4].value,
            n = +t[5].value,
            d = +t[6].value
        if (e.match(/\d\d/)) {
            var c
            a.gruposMin[e] || (a.gruposMin[e] = {}), c = a.gruposMin[e]
            var u = o
            o = +o, a.datasMin[o] = !0, c[o] || (c[o] = {
                date: u
            }), c = c[o], c.qtdReparosFixo = r, c.qtdReparosVelox = s, c.qtdReparosTV = i, c.total = r + s + i, c.qtdChamadasFixa = n, c.qtdChamadasTV = d, c.percentReparosFixo = 0 !== c.qtdChamadasFixa ? 100 * c.qtdReparosFixo / c.qtdChamadasFixa : 0, c.percentReparosVelox = 0 !== c.qtdChamadasFixa ? 100 * c.qtdReparosVelox / c.qtdChamadasFixa : 0, c.percentReparosTV = 0 !== c.qtdChamadasTV ? 100 * c.qtdReparosTV / c.qtdChamadasTV : 0, c.totalPercentual = c.total / (n + d)
        }
    }
}

function geraTotaisMomento(a) {
    var t = [],
        e = Date.now
    forEachPair(a.grupos, function(a, o) {
        var r = {
            DDD: o,
            qtdReparosFixo: 0,
            qtdReparosVelox: 0,
            qtdReparosTV: 0,
            total: 0,
            qtdChamadasFixa: 0,
            qtdChamadasTV: 0
        }
        forEachPair(a, function(a, t) {
            t >= e - 36e5 && e > t && (r.qtdReparosFixo += a.qtdReparosFixo, r.qtdReparosVelox += a.qtdReparosVelox, r.qtdReparosTV += a.qtdReparosTV, r.total += a.total, r.qtdChamadasFixa += a.qtdChamadasFixa, r.qtdChamadasTV += a.qtdChamadasTV)
        }), r.total > 0 && (r.percentReparosFixo = 0 !== r.qtdChamadasFixa ? 100 * r.qtdReparosFixo / r.qtdChamadasFixa : 0, r.percentReparosVelox = 0 !== r.qtdChamadasFixa ? 100 * r.qtdReparosVelox / r.qtdChamadasFixa : 0, r.percentReparosTV = 0 !== r.qtdChamadasTV ? 100 * r.qtdReparosTV / r.qtdChamadasTV : 0, r.totalPercentual = 100 * r.total / (r.qtdChamadasFixa + r.qtdChamadasTV || 1), t.push(r))
    }), t.sort(function(a, t) {
        return t.total - a.total
    }), a.totais = t
}

function geraTotaisRegiao(a) {
    for (var t = [], e = 0; 3 > e; e++) {
        var o = {
            regiao: e + 1,
            total: 0,
            qtdChamadasFixa: 0,
            qtdChamadasTV: 0
        }
        dimensoes.forEach(function(a) {
            o[a.field] = 0
        }), t.push(o)
    }
    a.totais.forEach(function(a) {
        var e = obtemRegiao(a.DDD)
        if (void 0 !== e) {
            var o = t[e.substring(1, 2) - 1]
            dimensoes.forEach(function(t) {
                o[t.field] += a[t.field]
            }), o.total += a.total, o.qtdChamadasFixa += a.qtdChamadasFixa, o.qtdChamadasTV += a.qtdChamadasTV
        }
    }), t.forEach(function(a) {
        a.totalPercentual = 100 * a.total / (a.qtdChamadasFixa + a.qtdChamadasTV || 1)
    }), a.totaisRegiao = t
}

function geraTotaisUF(a) {
    for (var t = [], e = 0; 3 > e; e++) {
        var o = {
            UF: e + 1,
            total: 0,
            qtdChamadasFixa: 0,
            qtdChamadasTV: 0
        }
        dimensoes.forEach(function(a) {
            o[a.field] = 0
        }), t.push(o)
    }
    a.totais.forEach(function(a) {
        var e = obtemUF(a.DDD)
        if (void 0 !== e) {
            var o = t[e - 1]
            dimensoes.forEach(function(t) {
                o[t.field] += a[t.field]
            }), o.total += a.total, o.qtdChamadasFixa += a.qtdChamadasFixa, o.qtdChamadasTV += a.qtdChamadasTV
        }
    }), t.forEach(function(a) {
        a.totalPercentual = 100 * a.total / (a.qtdChamadasFixa + a.qtdChamadasTV || 1)
    }), a.totaisUF = t
}

function preparaSeriesTemporais(a, t, e, o, r) {
    function s(a) {
        var t = c.map(function(a) {
            return a
        })
        t.push("total")
        var e = {}
        return t.forEach(function(t) {
            var o = {}
            f.filter(function(t) {
                return t.tipo === a
            }).forEach(function(a) {
                o[a.grupo] = a.estatisticas.picos[t]
            }), e[t] = sortHashMap(o, function(a, t) {
                return t.value - a.value
            })
        }), e
    }

    function i(a) {
        var t = c.map(function(a) {
            return a
        })
        t.push("total")
        var e = {}
        return t.forEach(function(t) {
            var o = {}
            f.filter(function(t) {
                return t.tipo === a
            }).forEach(function(a) {
                o[a.grupo] = a.estatisticas.volumes[t]
            }), e[t] = sortHashMap(o, function(a, t) {
                return t.value - a.value
            })
        }), e
    }
    e || (e = 1), r || (r = function() {
        return !0
    })
    var n, d, c = ["qtdReparosFixo", "qtdReparosVelox", "qtdReparosTV"],
        u = [{
            type: "total",
            group: function() {
                return "Total"
            },
            title: function() {
                return "Total"
            }
        }, {
            type: "regiao",
            group: function(a) {
                return obtemRegiao(a)
            },
            title: function(a) {
                return obtemRegiao(a)
            }
        }, {
            type: "UF",
            group: function(a) {
                return obtemUF(a)
            },
            title: function(a) {
                return obtemUF(a)
            }
        }, {
            type: "DDD",
            group: function(a) {
                return "DDD " + a
            },
            title: function(a) {
                return "DDD " + a
            }
        }],
        l = function(a) {
            return a
        }
    "hora" === t ? (n = a.grupos, d = a.datasOrdenadas) : "minuto" === t && (n = a.gruposMin, d = a.datasMinOrdenadas, l = function(a) {
        var t = 6e4 * e
        return t * Math.floor(a / t)
    })
    var f = []
    return f.indices = {}, d.forEach(function(a) {
        if (r(a)) {
            var t = {
                date: new Date(a)
            }
            c.forEach(function(a) {
                t[a] = 0
            }), forEachPair(UFs, function(e) {
                e.forEach(function(e) {
                    var o = n[e],
                        r = o && o[a] || t,
                        s = l(+a)
                    u.forEach(function(a) {
                        var t, o = a.group(e),
                            i = f.indices[o]
                        void 0 === i ? (t = [], t.grupo = o, t.tipo = a.type, t.estatisticas = {
                            picos: {},
                            volumes: {}
                        }, f.indices[o] = f.push(t) - 1) : t = f[i]
                        var n = t[t.length - 1];
                        (void 0 === n || n.datReferencia !== s) && (n = {
                            title: o,
                            datReferencia: s,
                            date: new Date(s)
                        }, c.forEach(function(a) {
                            n[a] = 0
                        }), t.push(n)), c.forEach(function(a) {
                            n[a] += r[a]
                        })
                    })
                })
            })
        }
    }), f.forEach(function(a) {
        c.forEach(function(t) {
            a.estatisticas.picos[t] = 0, a.estatisticas.picos.total = 0, a.estatisticas.volumes[t] = 0, a.estatisticas.volumes.total = 0
        }), a.forEach(function(t) {
            var e = 0
            c.forEach(function(o) {
                a.estatisticas.picos[o] < t[o] && (a.estatisticas.picos[o] = t[o]), a.estatisticas.volumes[o] += t[o], e += t[o]
            }), a.estatisticas.picos.total < e && (a.estatisticas.picos.total = e), a.estatisticas.volumes.total += e
        })
    }), f.DDDsPorPico = s("DDD"), f.UFsPorPico = s("UF"), f.DDDsPorVolume = i("DDD"), f.UFsPorVolume = i("UF"), f
}

function todasAsDatas(a, t, e) {
    for (var o = [], r = a, s = 0; t > s; s++) o.push(r), r += e
    return o
}

function atualizaVisualizacao(a, t) {
    charts.length = 0
    var e = d3.scale.category10(),
        o = [{
            field: "qtdReparosFixo",
            color: e(0)
        }, {
            field: "qtdReparosVelox",
            color: e(1)
        }, {
            field: "qtdReparosTV",
            color: e(2)
        }],
        r = [],
        s = [],
        i = 0
    r.push(a.totaisArea3[a.totaisArea3.indices.Total]), a.totaisArea3.DDDsPorVolume.total.forEach(function(t, e) {
        if (!(e >= 5)) {
            var o = t.key
            r.push(a.totaisArea3[a.totaisArea3.indices[o]])
        }
    }), r[0].forEach(function(a) {
        o.forEach(function(t) {
            var e = a[t.field]
            e > i && (i = e)
        })
    }), r.forEach(function(a) {
        s.push(t.totaisArea3[t.totaisArea3.indices[a.grupo]])
    }), s[0].forEach(function(a) {
        o.forEach(function(t) {
            var e = a[t.field]
            e > i && (i = e)
        })
    })
    var n = [],
        d = [],
        c = 0
    n.push(a.totaisArea3[a.totaisArea3.indices.Total]), a.totaisArea3.UFsPorVolume.total.forEach(function(t, e) {
        if (!(e >= 5)) {
            var o = t.key
            n.push(a.totaisArea3[a.totaisArea3.indices[o]])
        }
    }), n[0].forEach(function(a) {
        o.forEach(function(t) {
            var e = a[t.field]
            e > c && (c = e)
        })
    }), n.forEach(function(a) {
        d.push(t.totaisArea3[t.totaisArea3.indices[a.grupo]])
    }), d[0].forEach(function(a) {
        o.forEach(function(t) {
            var e = a[t.field]
            e > c && (c = e)
        })
    }), charts.push(multiBarChart(n, o, d3.select("#tela1 .ultimas10hUF .areaChart"), 0, 700, {
        maxDataPoint: c
    })), charts.push(multiBarChart(d, o, d3.select("#tela1 .ultimas10hUFComparativo .areaChart"), 0, 700, {
        maxDataPoint: c
    }))
    var u = []
    u.push(a.totaisArea4[a.totaisArea4.indices.Total]), a.totaisArea4.UFsPorVolume.total.forEach(function(t, e) {
        if (!(e >= 5)) {
            var o = t.key
            u.push(a.totaisArea4[a.totaisArea4.indices[o]])
        }
    }), charts.push(multiAreaChart(u, o, d3.select("#tela1 .ultima1hUF .areaChart"), 0, 700, {
        chartType: "line",
        xTickFormat: function(a) {
            return d3.time.format("%H:%M")(a)
        }
    })), legenda(o, d3.select("#tela1 .canto-sup-dir .legend"))
    var l = o.filter(function(a) {
            return a.field.match(/TV$/)
        }),
        f = Math.max(n[0].estatisticas.picos.qtdReparosTV, d[0].estatisticas.picos.qtdReparosTV),
        p = [],
        h = []
    p.push(a.totaisArea3[a.totaisArea3.indices.Total]), a.totaisArea3.UFsPorVolume.qtdReparosTV.forEach(function(t, e) {
        if (!(e >= 5)) {
            var o = t.key
            p.push(a.totaisArea3[a.totaisArea3.indices[o]])
        }
    }), p.forEach(function(a) {
        h.push(t.totaisArea3[t.totaisArea3.indices[a.grupo]])
    }), charts.push(multiBarChart(p, l, d3.select("#tela2 .ultimas10hUF .areaChart"), 0, 700, {
        maxDataPoint: f
    })), charts.push(multiBarChart(h, l, d3.select("#tela2 .ultimas10hUFComparativo .areaChart"), 0, 700, {
        maxDataPoint: f
    }))
    var D = []
    D.push(a.totaisArea4[a.totaisArea4.indices.Total]), a.totaisArea4.UFsPorVolume.qtdReparosTV.forEach(function(t, e) {
        if (!(e >= 5)) {
            var o = t.key
            D.push(a.totaisArea4[a.totaisArea4.indices[o]])
        }
    }), charts.push(multiAreaChart(D, l, d3.select("#tela2 .ultima1hUF .areaChart"), 0, 700, {
        chartType: "line",
        xTickFormat: function(a) {
            return d3.time.format("%H:%M")(a)
        }
    })), legenda(l, d3.select("#tela2 .canto-sup-dir .legend"))
}

function obtemDadosReparoJSON(a) {
    function t(a) {
        return a >= 0 && a < r.length - 1 ? void r[a + 1]() : (a === r.length - 1 && (_try(function() {
            atualizaVisualizacao(e, o)
        }), _dados = e, _dados2 = o, void 0 === timeoutIDs.alternaTelas && alternaTelas(), void 0 === timeoutIDs.verificaTamanhoTela && verificaTamanhoTela(!1)), setTimeout(function() {
            obtemDadosReparoJSON()
        }, 6e4), void setTimeout(function() {
            document.location.reload(!0)
        }, 36e5))
    }
    a || (a = "")
    var e = {
            grupos: {},
            gruposMin: {},
            datas: {},
            datasMin: {},
            datasOrdenadas: [],
            datasMinOrdenadas: []
        },
        o = {
            grupos: {},
            datas: {},
            datasOrdenadas: []
        },
        r = []
    r.push(function() {
        var o = processaRegistroHora(e)
       // d3.tsv(a + "data/reparo_hora.tsv", function (a, r) {
        d3.tsv("http://versatil.technology/temp2/reparo_hora.tsv", function (a, r) {
            if (a) return console.log(a), void t(-1)
            r.forEach(function(a) {
                var t = a.DatReferencia,
                    e = +t.substring(0, 4),
                    r = +t.substring(5, 7),
                    s = +t.substring(8, 10),
                    i = +t.substring(11, 13),
                    n = +t.substring(14, 16)
                t = e + "/" + _02d(r) + "/" + _02d(s) + " " + _02d(i) + ":" + _02d(n) + ":00", o([{
                    value: a.DDD
                }, {
                    value: new Date(t)
                }, {
                    value: a.TipoReparo
                }, {
                    value: a.QtdReparos
                }, {
                    value: a.QtdChamadas || 1
                }, {
                    value: a.QtdChamadas || 1
                }])
            }), e.datasOrdenadas = []
            for (var s in e.datas) e.datasOrdenadas.push(s)
            e.datasOrdenadas.sort(), e.totaisArea3 = preparaSeriesTemporais(e, "hora", 1, 10), console.log(e.totaisArea3), t(0)
        })
    }), r.push(function() {
        var e = processaRegistroHora(o)
        //d3.tsv(a + "data/reparo_hora_comparativo.tsv", function (a, r) {
        d3.tsv("http://versatil.technology/temp2/reparo_hora_comparativo.tsv", function (a, r) {
            
            if (a) return console.log(a), void t(-1)
            r.forEach(function(a) {
                var t = a.DatReferencia,
                    o = +t.substring(0, 4),
                    r = +t.substring(5, 7),
                    s = +t.substring(8, 10),
                    i = +t.substring(11, 13),
                    n = +t.substring(14, 16)
                t = o + "/" + _02d(r) + "/" + _02d(s) + " " + _02d(i) + ":" + _02d(n) + ":00", e([{
                    value: a.DDD
                }, {
                    value: new Date(t)
                }, {
                    value: a.TipoReparo
                }, {
                    value: a.QtdReparos
                }, {
                    value: a.QtdChamadas || 1
                }])
            }), o.datasOrdenadas = []
            for (var s in o.datas) o.datasOrdenadas.push(s)
            o.datasOrdenadas.sort(), o.totaisArea3 = preparaSeriesTemporais(o, "hora", 1, 10), console.log(o.totaisArea3), t(1)
        })
    }), r.push(function() {
        var o = processaRegistroMin(e)
        //d3.tsv(a + "data/reparo_minuto.tsv", function (a, r) {
        d3.tsv("http://versatil.technology/temp2/reparo_minuto.tsv", function (a, r) {
        
            if (a) return console.log(a), void t(-1)
            r.forEach(function(a) {
                var t = a.DatReferencia,
                    e = +t.substring(0, 4),
                    r = +t.substring(5, 7),
                    s = +t.substring(8, 10),
                    i = +t.substring(11, 13),
                    n = +t.substring(14, 16)
                t = e + "/" + _02d(r) + "/" + _02d(s) + " " + _02d(i) + ":" + _02d(n) + ":00", o([{
                    value: a.DDD
                }, {
                    value: new Date(t)
                }, {
                    value: a.QtdReparosFixo
                }, {
                    value: a.QtdReparosVelox
                }, {
                    value: a.QtdReparosTV
                }, {
                    value: a.QtdChamadasFixa || 1
                }, {
                    value: a.QtdChamadasTV || 1
                }])
            }), e.datasMinOrdenadas = []
            for (var s in e.datasMin) e.datasMinOrdenadas.push(+s)
            e.datasMinOrdenadas.sort(), e.totaisArea4 = preparaSeriesTemporais(e, "minuto", 1, 60, function(a) {
                return Date.now() - 39e5 <= a && a < Date.now() - 3e5
            }), console.log(e.totaisArea4), t(2)
        })
    }), r[0]()
}

function geraTelaPeriodos(a) {
    var t = (a.append("h1").html("HistÃ³rico de Reparos por UF"), a.append("div").attr("class", "ultimas10hUFComparativo"))
    t.append("h2").html("Uma semana atrÃ¡s"), t.append("svg").attr("class", "areaChart")
    var e = a.append("div").attr("class", "ultimas10hUF")
    e.append("h2").html("Ãšltimas 10 h fechadas"), e.append("svg").attr("class", "areaChart")
    var o = a.append("div").attr("class", "ultima1hUF")
    o.append("h2").html("Ãšltimos 60 min"), o.append("svg").attr("class", "areaChart")
}

function legenda(a, t) {
    var e = 18,
        o = 8
    t.selectAll("g").remove()
    var r = t.attr("width", 85 * a.length).attr("height", 50),
        s = r.selectAll(".legend").data(a).enter().append("g").attr("transform", function(a, t) {
            var e = 85 * t
            return "translate(" + e + ",0)"
        })
    s.append("rect").attr("width", e).attr("height", e).style("fill", function(a) {
        return a.color
    }).style("stroke", function(a) {
        return a.color
    }), s.append("text").attr("class", "legend").attr("x", e + o).attr("y", e / 2).attr("dy", ".35em").text(function(a) {
        return a.field.substring(10)
    })
}
var regioes = {
        R1: ["21", "22", "24", "27", "28", "31", "32", "33", "34", "35", "37", "38", "71", "73", "74", "75", "77", "79", "81", "82", "83", "84", "85", "86", "87", "88", "89", "91", "92", "93", "94", "95", "96", "97", "98", "99"],
        R2: ["41", "42", "43", "44", "45", "46", "47", "48", "49", "51", "53", "54", "55", "61", "62", "63", "64", "65", "66", "67", "68", "69"],
        R3: ["11", "12", "13", "14", "15", "16", "17", "18", "19"]
    },
    obtemRegiao = function() {
        var a = {}
        return forEachPair(regioes, function(t, e) {
                t.forEach(function(t) {
                    a[t] = e
                })
            }),
            function(t) {
                return a[t]
            }
    }(),
    UFs = {
        AC: ["68"],
        AL: ["82"],
        AM: ["92", "97"],
        AP: ["96"],
        BA: ["71", "73", "74", "75", "77"],
        CE: ["85", "88"],
        ES: ["27", "28"],
        MG: ["31", "32", "33", "34", "35", "37", "38"],
        MS: ["67"],
        MT: ["65", "66"],
        PA: ["91", "93", "94"],
        PB: ["83"],
        PI: ["86", "89"],
        PR: ["41", "42", "43", "44", "45", "46"],
        RJ: ["21", "22", "24"],
        RN: ["84"],
        RO: ["69"],
        RR: ["95"],
        RS: ["51", "53", "54", "55"],
        SC: ["47", "48", "49"],
        SE: ["79"],
        SP: ["11", "12", "13", "14", "15", "16", "17", "18", "19"],
        TO: ["63"],
        PE: ["81", "87"],
        MA: ["98", "99"],
        GO: ["61", "62", "64"]
    },
    obtemUF = function() {
        var a = {}
        return forEachPair(UFs, function(t, e) {
                t.forEach(function(t) {
                    a[t] = e
                })
            }),
            function(t) {
                return a[t]
            }
    }(),
    uspObtemDadosReparo = " declare @dataIni smalldatetime = $dataIni; declare @dataFim smalldatetime = $dataFim; with cte as (   select DDD, DatReferencia, TipoReparo, sum(QtdChamadas) as QtdChamadas   from ResumoDDDxReparo   where DatReferencia >= @dataIni and DatReferencia < @dataFim   group by DDD, DatReferencia, TipoReparo ), cte2 as (   select DDD, DatReferencia, CodAplicacao, sum(QtdChamadas) as QtdChamadas   from ResumoAnalitico2   where DatReferencia >= @dataIni and DatReferencia < @dataFim     and CodAplicacao in ('10331UNIF', 'NOVAOITV')   group by DDD, DatReferencia, CodAplicacao ) select cte.DDD, cte.DatReferencia, cte.TipoReparo, cte.QtdChamadas as QtdReparos,   isnull(cte2.QtdChamadas, 0) as QtdChamadas from cte left outer join cte2 on cte.DDD = cte2.DDD and cte.DatReferencia = cte2.DatReferencia   and ((cte.TipoReparo in ('FIXO','VELOX') and cte2.CodAplicacao = '10331UNIF') or (cte.TipoReparo = 'TV' and cte2.CodAplicacao = 'NOVAOITV')) order by DDD, DatReferencia, TipoReparo",
    uspObtemDadosReparoMin = " declare @dataIni smalldatetime = $dataIni; declare @dataFim smalldatetime = $dataFim; with cte as (   select DDD, DatReferencia,     sum(case when CodIC in ('ICRP019','ICRP024','ICRP028','ICRP042','ICRP037','ICRP038','ICRP023','ICRV001','ICRV022','ICRV023','ICCV005','ICCV038','ICCV039','ICCV007','ICCR013','ICCR007','ICRG006','ICRG008','ICRG011','ICMA003','ICVC011','ICVC008','ICVC027','ICVC029','ICVC033','ICD8037','ICD9037','ICPR9038','ICPR9034','ICPR9033') then QtdChamadas else 0 end) as QtdReparosVelox,     sum(case when CodIC in ('ICRP019','ICRP024','ICRP028','ICRP042','ICRP037','ICRP038','ICRP023','ICRV001','ICRV022','ICRV023','ICCV005','ICCV038','ICCV039','ICCV007','ICCR013','ICCR007','ICRG006','ICRG008','ICRG011','ICMA003','ICVC011','ICVC008','ICVC027','ICVC029','ICVC033','ICD8037','ICD9037','ICPR9038','ICPR9034','ICPR9033') then 0 else QtdChamadas end) as QtdReparosFixo   from ResumoDDDxICMin   where DatReferencia >= @dataIni and DatReferencia < @dataFim   group by DDD, DatReferencia ), cte2 as (   select DDD, DatReferencia, sum(QtdChamadas) as QtdReparosTV   from ResumoDDDxTransferIDMin   where DatReferencia >= @dataIni and DatReferencia < @dataFim   group by DDD, DatReferencia ), cte3 as (   select DDD, DatReferencia,     sum(case CodAplicacao when '10331UNIF' then QtdChamadas else 0 end) as QtdChamadasFixa,     sum(case CodAplicacao when 'NOVAOITV' then QtdChamadas else 0 end) as QtdChamadasTV   from ResumoAnaliticoMin   where DatReferencia >= @dataIni and DatReferencia < @dataFim     and CodAplicacao in ('10331UNIF', 'NOVAOITV')   group by DDD, DatReferencia ), cte4 as (   select cte.DDD, cte.DatReferencia,     cte.QtdReparosFixo, cte.QtdReparosVelox, isnull(cte2.QtdReparosTV, 0) as QtdReparosTV,     isnull(cte3.QtdChamadasFixa, 0) as QtdChamadasFixa, isnull(cte3.QtdChamadasTV, 0) as QtdChamadasTV   from cte   left outer join cte2 on cte.DDD = cte2.DDD and cte.DatReferencia = cte2.DatReferencia   left outer join cte3 on cte.DDD = cte3.DDD and DATEADD(hour, DATEDIFF(hour, 0, cte.DatReferencia), 0) = cte3.DatReferencia ), cte5 as (   select cte2.DDD, cte2.DatReferencia, 0 as QtdReparosFixo, 0 as QtdReparosVelox, cte2.QtdReparosTV,     isnull(cte3.QtdChamadasFixa, 0) as QtdChamadasFixa, isnull(cte3.QtdChamadasTV, 0) as QtdChamadasTV   from cte2   left outer join cte on cte2.DDD = cte.DDD and cte2.DatReferencia = cte.DatReferencia   left outer join cte3 on cte2.DDD = cte3.DDD and cte2.DatReferencia = cte3.DatReferencia   where cte.DDD is null ) select cte4.DDD, cte4.DatReferencia,   cte4.QtdReparosFixo, cte4.QtdReparosVelox, cte4.QtdReparosTV, cte4.QtdChamadasFixa, cte4.QtdChamadasTV from cte4 union all select cte5.DDD, cte5.DatReferencia,   0 as QtdReparosFixo, 0 as QtdReparosVelox, cte5.QtdReparosTV, cte5.QtdChamadasFixa, cte5.QtdChamadasTV from cte5 order by DDD, DatReferencia",
    db, config = {
        server: "172.16.8.1",
        userName: "ESTQRY",
        password: "sql2008@ESTQ31",
        database: "Estatura",
        options: {
            port: 55759
        }
    }
if (nodejs) {
    var gui = require("nw.gui"),
        win = gui.Window.get()
    win.maximize()
}
var charts = [],
    _dados, _dados2, timeoutIDs = {},
    dimensoes = [{
        title: "Fixo",
        field: "qtdReparosFixo"
    }, {
        title: "Velox",
        field: "qtdReparosVelox"
    }, {
        title: "TV",
        field: "qtdReparosTV"
    }],
    map, telas = [{
        duracao: 6e4
    }, {
        duracao: 1e4
    }],
    telaAtiva = 0,
    cores = ["#cccccc", "#f7b6ba", "#ed6169", "#e30513"]
window.onload = function() {
    _try(nodejs ? function() {
        db = new DB("MonitorReparo"), console.log("Conectando..."), db.connect(config, function(a) {
            return a ? void console.log(a) : (console.log("ConexÃ£o realizada com sucesso"), void _try(function() {
                obtemDadosReparo(db)
            }))
        })
    } : function() {
        obtemDadosReparoJSON()
    })
}