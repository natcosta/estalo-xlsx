/*
 * CtrlHigienização de Cadastro
 */

 function CtrlHigienizacaoCadastro($scope, $globals) {
	
	var pagina = "#pag-higienizacao-cadastro";
	var titulo = "Resumo higienização de cadastro";
	
	win.title = titulo;
	$scope.versao = versao;
	$scope.rotas = rotas;

	//Alex 08/02/2014	
	travaBotaoFiltro(0, $scope, pagina, titulo);
	
	$scope.xlsx = [];
	$scope.exportar = false;
	
	$scope.dados = [];
	$scope.dados[0] = [];
	$scope.dados[1] = [];
    $scope.gridDados = [];	
	$scope.colunas = [];
	$scope.colunas[0] = [];
	$scope.colunas[1] = [];
	var dataFormatada = new Date(2019, 05, 02);

	
	$scope.gridDados[0] = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados[0],
		columnDefs: $scope.colunas[0],
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	$scope.gridDados[1] = {
		headerTemplate: base + 'header-template.html',		
		data: $scope.dados[1],
		columnDefs: $scope.colunas[1],
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};
	
	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		segmentos: [],
		sites: []
	};	
	
	$scope.iit = true;
	$scope.tlv = true;
	
	function activaTab(tab){
		$('.nav-tabs a[href="#' + tab + '"]').tab('show');
	};
	
	
	function rce (v){
		v = v.replace(/\/|\(|\)|\=|\?|\./gi,'');
		v = v.replace(/  /gi,' ');
		return v;
	}
	
	
	function geraColunas(tipo) {
		var array = [];
		
		
		array.push({field: "dia_br",displayName: "Data",width:150,pinned: true});
		
		var cols = [];
		if(tipo === "144"){
			
			
			cols = unique2(cache.criteriosConsolGenerica.map(function(a){
				if(a.CodProjeto === "ResumoHigienizacao" && a.CodAplicacao === "NGRPRE") return rce(a.Descricao);
			}));
			
	
		}else if(tipo === "USSD"){
			
			cols = unique2(cache.criteriosConsolGenerica.map(function(a){
				if(a.CodProjeto === "ResumoHigienizacao" && a.CodAplicacao.match("USSD")) return rce(a.Descricao);
			}));
			
			
		}else if(tipo==="generico"){
			cols = [
			
			
				"ANI",
				"TRATADO",
				"DURAÇÃO",
				"FINALIZAÇÃO",
				"DESTINO",
				"DNIS",
				"APLICAÇÃO",
				"APP SERVER",
				"URA",
				"CPF/CNPJ",
				"Siebel"
				
			];
			
		}

		for (var i = 0; i<cols.length; i++){
			array.push({field: "e"+pad(i+1)+"",displayName: cols[i],width:250,pinned: false});
		}

		return array;
	}
	
	var stmtCols144Names = [];
	var stmtCols144 = [];
	var stmtColsUSSDNames = [];
	var stmtColsUSSD = [];
	var casos = [];
	
	
	if(cache.criteriosConsolGenerica.length === 0){
		cache.carregaCriterios(function(){populaCriterio();});		
	}else{
		populaCriterio()
	}
	
	function populaCriterio(){
		var ss1= [];
		var ss2= [];		
		cache.criteriosConsolGenerica.forEach(function(a){
			if(a.CodProjeto === "ResumoHigienizacao" && a.CodAplicacao === "NGRPRE"){
				if(ss1.indexOf(rce(a.Descricao))>=0){
					ss2[ss1.indexOf(rce(a.Descricao))] +="','"+a.CodCombinacao;
				}else{
					ss1.push(rce(a.Descricao));
					ss2.push(a.CodCombinacao);
				}
			}
		});
		
		stmtCols144Names = ss1;
		stmtCols144 = ss2;	
		ss1= [];
		ss2= [];
		
		cache.criteriosConsolGenerica.forEach(function(a){
			if(a.CodProjeto === "ResumoHigienizacao" && a.CodAplicacao.match("USSD")){
				if(ss1.indexOf(rce(a.Descricao))>=0){
					ss2[ss1.indexOf(rce(a.Descricao))] +="','"+a.CodCombinacao;
				}else{
					ss1.push(rce(a.Descricao));
					ss2.push(a.CodCombinacao);
				}
			}
		});
		
		stmtColsUSSDNames = ss1;
		stmtColsUSSD = ss2;	
		ss1 = [];
		ss2 = [];		
		
		for (var i = 0; i < stmtCols144.length; i++){
			var s = "";
			try{
				s = "COMBINACOES like ('%" + stmtCols144[i].split('\',\'').join(';%\') OR COMBINACOES like (\'%')+';%\')';
			
			}catch(ex){
				s = "COMBINACOES like ('%"+stmtCols144[i]+";%')";
			}
			//Brunno ES-4310
			
			casos.push(
			{ header: ""+stmtCols144Names[i]+" - 144", query: "SELECT DATAHORA_INICIO,NUMANI,CLIENTETRATADO, DURACAO,INDICDELIG, '_' as DESTINO, NUMDNIS,CODAPLICACAO, CODHWIVR AS APPSERVER,HOSTNAMEURA AS URA, NumCPFCNPJ as [CPF/CNPJ], j.Descricao as errosiebel FROM ChamadasHigCadastro LEFT JOIN ErrosSiebel as j on CodErroSiebel = j.CodErro WHERE DATAHORA_INICIO >= '__DATAINI__' AND DATAHORA_INICIO <= '__DATAFIM__' AND CODAPLICACAO = 'NGRPRE' AND ("+s+")"}
			//"+($scope.iit ? "" : "AND IndicDelig <> 'IIT'")+"
			);
		}
		
		for (var i = 0; i < stmtColsUSSD.length; i++){
			var s = "";
			try{
				s = "COMBINACOES like ('%" + stmtColsUSSD[i].split('\',\'').join(';%\') OR COMBINACOES like (\'%')+';%\')';
			
			}catch(ex){
				s = "COMBINACOES like ('%"+stmtColsUSSD[i]+";%')";
			}
			
			casos.push(
			{ header: ""+stmtColsUSSDNames[i]+" - USSD", query: "SELECT DATAHORA_INICIO,NUMANI,CLIENTETRATADO, DURACAO,INDICDELIG, '_' as DESTINO, NUMDNIS,CODAPLICACAO, CODHWIVR AS APPSERVER,HOSTNAMEURA AS URA, NumCPFCNPJ as [CPF/CNPJ], j.Descricao as errosiebel FROM ChamadasHigCadastro LEFT JOIN ErrosSiebel as j on CodErroSiebel = j.CodErro WHERE DATAHORA_INICIO >= '__DATAINI__' AND DATAHORA_INICIO <= '__DATAFIM__' AND CODAPLICACAO IN ('USSD', 'USSD144', 'USSD880') AND ("+s+")"}
			//"+($scope.iit ? "" : "AND IndicDelig <> 'IIT'")+"
		    );
		}
	}
	

	
	// Filtros: data e hora
	var agora = new Date();

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]) : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]) : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora
	};
	
	
	
	var $view;
	

	$scope.$on('$viewContentLoaded', function () {			

		$view = $(pagina);		
		
		carregaRegioes($view);
		carregaAplicacoes($view, false, false, $scope);
		carregaSites($view);
		carregaAssuntos($view, true);
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		//19/03/2014
		componenteDataMaisHora($scope, $view);
		carregaRegioes($view);
		carregaSites($view);
		carregaSegmentosPorAplicacao($scope, $view, true);
		
		
		// Guardar filtros        		
		viewOnChangeGuardaFiltros ($('select'), $scope, $view);
		
		// Marcar todos
		viewOnClickMarcaTodos($("[id^=alink]"),$scope,$view);
		
		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-higienizacao-cadastro");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

			$scope.colunas[0] = geraColunas("144");            
			$scope.colunas[1] = geraColunas("USSD");
			
			//BUG
			if($('.nav-tabs li.active').text() !== " 144")	activaTab("visao144");
            $scope.listaDados.apply(this);
        });
		
		$view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);
           
            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }
	
		var testeClick = setInterval(function() {					
			if($scope.exportar){
				if(eElement !== undefined){	
					for (var i=0;i < casos.length;i++){		
																		
						if(eElement === casos[i].header.split(' - ')[0] && ePath['visao144'] && casos[i].header.split(' - ')[1] === $(ePath['visao144']).attr("id").replace('visao','') && casos[i].header.split(' - ')[0] === eElement && !$('.nav.nav-tabs>li>a').text().match(new RegExp(casos[i].header))){
							$('.nav.nav-tabs').append('<li class="" style="width: 170.75px;"><a href="#visao144'+eElement.replace(/ /g,'')+'" target="_self" data-toggle="tab"><i></i> '+casos[i].header+'</a></li>');
							$($('.tab-content>div')[$('.nav.nav-tabs>li>a').length - 1]).attr("id","visao144"+eElement.replace(/ /g,'')+"");
							getQtdChamadas(casos[i].query,casos[i].header);
							eElement = undefined;
							break;							
						}else if(eElement === casos[i].header.split(' - ')[0] && ePath['visaoUSSD'] && casos[i].header.split(' - ')[1] === $(ePath['visaoUSSD']).attr("id").replace('visao','') && casos[i].header.split(' - ')[0] === eElement && !$('.nav.nav-tabs>li>a').text().match(new RegExp(casos[i].header))){
							$('.nav.nav-tabs').append('<li class="" style="width: 170.75px;"><a href="#visaoUSSD'+eElement.replace(/ /g,'')+'" target="_self" data-toggle="tab"><i></i> '+casos[i].header+'</a></li>');
							$($('.tab-content>div')[$('.nav.nav-tabs>li>a').length - 1]).attr("id","visaoUSSD"+eElement.replace(/ /g,'')+"");
							getQtdChamadas(casos[i].query,casos[i].header);
							eElement = undefined;
							break;
						}
						
					}
				}
			}	
		}, 500);
		
		function getQtdChamadas(stmt,header) {
			limpaProgressBar($scope, pagina);
		$scope.qtdChamadas = [];
		
		
		stmt = stmt.replace('__DATAINI__',formataDataHora($scope.periodo.inicio));
		stmt = stmt.replace('__DATAFIM__',formataDataHora($scope.periodo.fim));
		
		var stmts = arrayHorasQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
		stmts.pop();
		
		$scope.gridDados[$('.nav.nav-tabs>li>a').length - 1] = {
			headerTemplate: base + 'header-template.html',		
			data: $scope.dados[$('.nav.nav-tabs>li>a').length - 1],
			columnDefs: $scope.colunas[$('.nav.nav-tabs>li>a').length - 1],
			enableRowHeaderSelection: false,
			enableSelectAll: true,
			multiSelect : true,
			enableRowSelection : true,
			enableFullRowSelection : true,
			multiSelect : true,
			modifierKeysToMultiSelect : true
		};
		
		$scope.dados[$('.nav.nav-tabs>li>a').length - 1] = [];
		$scope.colunas[$('.nav.nav-tabs>li>a').length - 1] = geraColunas("generico");
		$scope.gridDados[$('.nav.nav-tabs>li>a').length - 1].data = $scope.dados[$('.nav.nav-tabs>li>a').length - 1];
		$scope.gridDados[$('.nav.nav-tabs>li>a').length - 1].columnDefs = $scope.colunas[$('.nav.nav-tabs>li>a').length - 1];		
		$scope.$apply();
		
		function retorno(r){				
			retornaStatusQuery(r, $scope, header);					
		}
		
		var total = 0;
		var execs = 0;
		stmts.forEach(function (f,indexf) {
			
			mssqlQueryTedious(f.toString(), function (err, result) {
				if (err){
					console.log(err);
					retornaStatusQuery(undefined, $scope, "Erro de conexão");
				}
				
				total += result.length;				
				result.forEach(function (e) {
					
					$scope.dados[$('.nav.nav-tabs>li>a').length - 1].push({
						dia_br: formataDataHoraBR(e[0].value),
						e01: e[1].value,
						e02: e[2].value,
						e03: e[3].value,
						e04: e[4].value,
						e05: e[5].value,
						e06: e[6].value,
						e07: e[7].value,
						e08: e[8].value,
						e09: e[9].value,
						e10: e[10].value,
						e11: e[11].value
					});
					
					if ($scope.dados[$('.nav.nav-tabs>li>a').length - 1] % 500 === 0) {
						$scope.$apply();
					}
					
					
					
				});	
				execs++;				
				if(execs === stmts.length){ 
					retorno(total);											
				}
				
			});
			
			if(indexf === 0){ 
				activaTab("visao"+header.split(' - ')[1]+header.split(' - ')[0].replace(/ /g,''));			
			}
			
			
			
		});
	}
		
		// Botão agora
        $view.on("click", ".btn-agora", function() {
            $scope.agora.apply(this);
        });

        $scope.agora = function() {
            iniciaAgora($view, $scope);
        };

        // Botão abortar
        $view.on("click", ".abortar", function() {
            killThreads();        
            $scope.abortar.apply(this);
        });

        $scope.abortar = function() {
            abortar($scope, pagina);
        };
        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');	
		

	});
	
	// Lista chamadas conforme filtros
	$scope.listaDados = function () {
		
		$('.nav.nav-tabs').html('<li class="active" style="width: 170.75px;"><a href="#visao144" target="_self" data-toggle="tab"><i></i> 144</a></li><li class="" style="width: 170.75px;"><a href="#visaoUSSD" target="_self" data-toggle="tab"><i></i> USSD</a></li>')
		for (var i=0; i<$scope.dados.length;i++){
			$scope.dados[i] = [];
			$scope.colunas[i] = [];
			if(i > 1) $($('.tab-content>div')[i]).attr("id","visaoGenerica");
		}
		
		$scope.$apply();
		$scope.exportar = false;
		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("enable", true);
        $btn_exportar_xlsx.prop("disabled", true);
        $btn_exportar_dropdown.prop("enable", true);  
	
		var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];

		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + "" +
			" até " + formataDataBR($scope.periodo.fim);
		if (filtro_aplicacoes.length > 0) {
		$scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
		}
		if (filtro_sites.length > 0) {
			$scope.filtros_usados += " Sites: " + filtro_sites;
		}
		if (filtro_segmentos.length > 0) {
			$scope.filtros_usados += " Segmentos: " + filtro_segmentos;
		}					
				
		var iit = "";
		var tlv = "";
		var tempoIIT = "";
		var tempoTLV = "";
		if ($scope.iit === false) {
			iit = "- ISNULL(QtdTransfURA, 0)";
			tempoIIT = "- ISNULL(TempoTransfURA, 0)";
		}
		if ($scope.tlv === false) {
			tlv = "- ISNULL(QtdChamadasTLV, 0)";
			tempoTLV = "- ISNULL(TempoTransfTLV, 0)";
		}
		
		var stmts = [];
		var executaQuerys = [];
		
		var totais1 = {};
		var totais2 = {};
		

		var stmt = db.use + '';
		stmt += " with a as("
		stmt += " SELECT 'ic_'+CONVERT(varchar(5), CodCombinacao) as CODIC,"
		
		for (var i = 0; i < stmtCols144.length; i++){
			//"+iit+"
			//Brunno ES-4310
			
			if(stmtCols144Names[i] === "Abandonada Ativo" | stmtCols144Names[i] === "Abandonada Bloqueio"){
				stmt += " CASE WHEN CodCombinacao in('"+stmtCols144[i]+"') then ISNULL(SUM(QtdAbandonadas),0) ELSE 0 END AS  c"+pad((i+1))+""
				if(i !== stmtCols144.length-1) stmt += ",";
			}
			else{
			stmt += " CASE WHEN CodCombinacao in('"+stmtCols144[i]+"') then ISNULL(SUM(QtdChamadas),0) ELSE 0 END AS  c"+pad((i+1))+""
			if(i !== stmtCols144.length-1) stmt += ",";
			}
			
		}
		
		stmt += " FROM ResumoGenerico"
		stmt += " WHERE "
		stmt += " cast(DatReferencia as DATE) = '"+formataData(data_ini)+"'"		
		stmt += " group by CodCombinacao"
		stmt += " )"
		
		stmt += " select '"+formataData(data_ini)+"' as dia, "
		
		
		for (var i = 0; i < stmtCols144.length; i++){
			
				stmt += " isnull(sum (a.c"+pad((i+1))+"),0) as e"+pad((i+1))+""
			if(i !== stmtCols144.length-1) stmt += ",";
		}
		
		stmt += " from a";
		stmts.push(stmt);
		
		var stmt = db.use + '';
		stmt += " with a as("
		stmt += " SELECT 'ic_'+CONVERT(varchar(5), CodCombinacao) as CODIC,"
		for (var i = 0; i < stmtColsUSSD.length; i++){
			//"+iit+"
			stmt += " CASE WHEN CodCombinacao in('"+stmtColsUSSD[i]+"') then ISNULL(SUM(QtdChamadas),0) ELSE 0 END AS  c"+pad((i+1))+""
			if(i !== stmtColsUSSD.length-1) stmt += ",";
		}		
		stmt += " FROM ResumoGenerico "
		stmt += " WHERE "
		stmt += " cast(DatReferencia as DATE) = '"+formataData(data_ini)+"'"
		//stmt += " AND CodAplicacao in ('PUSHNOCREDITREC','PUSHNOCREDITSVA','USSD','USSD144','USSD2020','USSD3000','USSD880','USSDPUSH','USSDPUSHACM','USSDSMS')"
		stmt += " group by CodCombinacao"
		stmt += " )"
		stmt += " select '"+formataData(data_ini)+"' as dia, "
		
		for (var i = 0; i < stmtColsUSSD.length; i++){
			stmt += " isnull(sum (a.c"+pad((i+1))+"),0) as e"+pad((i+1))+""
			if(i !== stmtColsUSSD.length-1) stmt += ",";
		}
		
		stmt += " from a";
		stmts.push(stmt);	

		function executaQuery(columns) {
			var dia = columns["dia"].value,
			    dia_br = formataDataBRString(dia);
				
			var ss3 = "";
			for (var i = 0; i < stmtCols144.length; i++){
				eval('var e'+pad((i+1))+' = columns["e'+pad((i+1))+'"].value;');
				ss3 += "e"+pad((i+1))+": e"+pad((i+1))+""
				if(i !== stmtCols144.length-1) ss3 += ",";
			}
			eval('var s = {	dia: dia, dia_br: dia_br,'+ss3+' }');
			
			$scope.dados[0].push(s);
			
			
			
			$.each(s, function(index, value) {
				if(index ==='dia' || index === 'dia_br') {
					totais1[index]="TOTAL";
				}else{ 
					if(totais1[index] === undefined) totais1[index] = 0;
					totais1[index]+= +value;
				}				
			}); 
			

			//$scope.xlsx.push([""])
		}

		function executaQuery2(columns) {
			var dia = columns["dia"].value,
			    dia_br = formataDataBRString(dia);
				
			var ss3 = "";
			for (var i = 0; i < stmtColsUSSD.length; i++){
				eval('var e'+pad((i+1))+' = columns["e'+pad((i+1))+'"].value;');
				ss3 += "e"+pad((i+1))+": e"+pad((i+1))+""
				if(i !== stmtColsUSSD.length-1) ss3 += ",";
			}
			eval('var s = {	dia: dia, dia_br: dia_br,'+ss3+' }');
				
			$scope.dados[1].push(s);
			
			$.each(s, function(index, value) {
				if(index ==='dia' || index === 'dia_br') {
					totais2[index]="TOTAL";
				}else{ 
					if(totais2[index] === undefined) totais2[index] = 0;
					totais2[index]+= +value;
				}				
			}); 

			//$scope.xlsx.push([""])
		}
		
		executaQuerys.push(executaQuery);
		executaQuerys.push(executaQuery2);
		
		
		function acaoFinal(){
			numStmts++;
			if(numStmts < stmts.length){
				inicializacao();
				proxima();
				return;
			}
			$scope.dados[0].push(totais1);	
			$scope.dados[1].push(totais2);	
			$scope.$apply();
			
			retornaStatusQuery(contador, $scope);				
			$btn_gerar.button('reset');
			$btn_exportar.prop("disabled", false);
			$btn_exportar_xlsx.prop("disabled", false);
			$btn_exportar_dropdown.prop("disabled", false);
			$scope.exportar = true;
		}

		var	fim,comeco,contador = 0,executa,numStmts = 0;	
		
		function inicializacao(){
			executa = executaQuerys[numStmts];
			stmt = stmts[numStmts];						
			fim = precisaoHoraIni($scope.periodo.fim).getTime();
			comeco = $scope.periodo.inicio.getTime();					
		}
		
		function proxima(i){				
			var stmtMod = stmt;
			
			
			if(i){
				comeco = comeco + 86400000;
				stmtMod = stmt.replace(new RegExp(formataData($scope.periodo.inicio), 'g'),formataData(new Date(comeco)));
			
			}
			// Precisei fazer isso Brunno 
			var testedata = moment(new Date(comeco)).diff(moment(dataFormatada), "days");
				if(testedata <= 0){
					stmtPro = stmtMod
					stmtMod = stmtPro.replace(new RegExp("1148",'g'),"45")
				}
			db.query(stmtMod, executa, function (err, num_rows) {				
				console.log("Executando query-> " + stmt + " " + num_rows);
				
				$scope.colunas[0] = geraColunas("144");                			 
				$scope.gridDados[0].data = $scope.dados[0];
				$scope.gridDados[0].columnDefs = $scope.colunas[0];
				$scope.colunas[1] = geraColunas("USSD");               				
				$scope.gridDados[1].data = $scope.dados[1];
				$scope.gridDados[1].columnDefs = $scope.colunas[1];
				$scope.$apply();
				
				contador++;	
				if (num_rows > 0 && comeco === fim) {
					inicializacao();
					acaoFinal();
					return;
				}
				proxima(true);				
			});
		}
		
		inicializacao();
		proxima();
		
		
	};
	
	// Exporta planilha XLSX
	$scope.exportaXLSX = function() {
	  
		var $btn_exportar = $(this);
		$btn_exportar.button('loading');
		
			var works = [];
			
			for (var i= 0; i < $('.nav.nav-tabs>li>a').length; i++){	
			var linhas  = [];
				linhas.push($scope.colunas[i].filterMap(function (col) {
					if (col.visible || col.visible === undefined) { return col.displayName; } 
				}));
				$scope.dados[i].forEach(function (dado) {
					linhas.push($scope.colunas[i].filterMap(function (col) {
						  if (col.visible || col.visible === undefined) {
							  if(dado[col.field] === 0) dado[col.field] = "0";
							  return dado[col.field];
						  } 
					}));
				});	
				
				works.push({ name: $($('.nav.nav-tabs>li>a')[i]).text().trim(), data: linhas, table: true });

				
			}
			
		
		var planilha = {
			creator: "Estalo",
			lastModifiedBy: loginUsuario || "Estalo",
			worksheets: works,
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: { first: 1 }
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');

		var caminho = titulo.replace(/ /g,'').substring(0,15) + "_" + formataDataHoraMilis(new Date()) + ".xlsx";
		fs.writeFileSync(caminho, planilha.base64, 'base64');
		childProcess.exec(caminho);
		        
    };
}
CtrlHigienizacaoCadastro.$inject = ['$scope', '$globals'];