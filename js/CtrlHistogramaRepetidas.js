function CtrlHistogramaRepetidas($scope, $globals) {

    win.title = "Histograma repetidas";//Brunno 16/01/2019
    var idView = "pag-histograma-repetidas-periodo";
    var pagina = "#pag-histograma-repetidas-periodo"
    $scope.versao = versao;
    $scope.rotas = rotas;
    var dadoExportacao = [];
    var dado = [];
    var busca = ""
    var pause = false
    var excel = false
    var stmts = []

    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    var $view;
    travaBotaoFiltro(0, $scope, "#pag-histograma-repetidas-periodo", win.title);
    //Grafico

    var resetCanvas = function () {
        $('#myChart').remove(); // this is my <canvas> element
        $('#canvas-container').append('<canvas id="myChart"><canvas>');
        canvas = document.getElementById('myChart');
        ctx = canvas.getContext('2d');
        ctx.canvas.width = $('#graph').width(); // resize to parent width
        ctx.canvas.height = $('#graph').height(); // resize to parent height
        var x = canvas.width / 2;
        var y = canvas.height / 2;
        ctx.font = '10pt Verdana';
        ctx.textAlign = 'center';
        ctx.fillText('This text is centered on the canvas', x, y);
    };
    function restringe_aplicacao(valores) {
        if (valores.length === 0) {
            return console.log("valor 0");
        } else {
            var aplicacao = "";
            for (i = 0; i < valores.length; i++) {
                aplicacao = aplicacao + "'" + valores[i] + "'"
                if (i < valores.length - 1) aplicacao += ","
            }

            return aplicacao;
        }
    }

    var periodo = { headerName: 'Data', field: 'data', width: 150 }
    var derivadas = { headerName: 'Chamador', field: 'chamador', width: 120 }
    var derivadaspercent = {
        headerName: 'Tratado', field: 'tratado', width: 120
    }
    var bolhas = { headerName: 'Aplicação', field: 'aplicacao', width: 100 }
    var bolhaspercent = {
        headerName: 'Segmento', field: 'segmento', width: 150
    }
    var menu = { headerName: 'Último Estado', field: 'estado', width: 100 }
    var escolha = { headerName: 'Encerramento', field: 'encerramento', width: 100 }
    var escolhapercent = {
        headerName: 'Duração', field: 'duracao', width: 100
    }
    $scope.colunas = [
        periodo, derivadas, derivadaspercent, bolhas, bolhaspercent, menu, escolha, escolhapercent
    ];
    $scope.gridDados = {
        defaultColDef: {
            sortable: true,
            filter: true
        },
        rowSelection: 'single',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',

        columnDefs: $scope.colunas,

        rowData: [],
        localeText: $scope.localeGridText,
        suppressHorizontalScroll: false
    };
    ctx = document.getElementById('myChart').getContext('2d');

    // Filtros: data e hora
    var agora = new Date();
    var inicio = Estalo.filtros.filtro_data_hora[0] || hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] || hojeFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#" + idView);


        $view.find(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        componenteDataMaisHora($scope, $view);
        carregaAplicacoes($view, false, false, $scope);

        // Carregar filtros


        // Popula lista de segmentos a partir das aplicações selecionadas


        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($view.find('.filtro-site'), 'sites'); });



        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
            $view.find("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $view.find("div.datahora-fim.input-append.date").data("datetimepicker").hide();
        });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseover(function () {
            $view.find('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseout(function () {
            $view.find('div.btn-group.filtro-site').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#" + idView);

            // Testar se a data inicial é maior que a data final
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length > 0) {
                dado = [];

                $scope.listarTempo.apply(this);
            } else {
                $scope.filtros_usados = ""
                retornaStatusQuery("erro: tem que selecionar uma aplicação", $scope);
            }


        });

        $view.on("click", ".btn-exportar", function () {
            pause = false
            $scope.listarDetalhado.apply(this);  
            excel = true;
        });
        /*$view.on("click", ".btn-gerar-jpg", function () {
            function downloadURI(uri, name) {
                var link = document.createElement("a");
                link.download = name;
                link.href = uri;
                link.click();
            }
            console.log(url);
            downloadURI(url, "chart.png");
                         
                   
        });*/
        // Botão abortar
        $view.on("click", ".abortar", function () {
            killThreads();
            $scope.abortar.apply(this);

        });

        $scope.abortar = function () {
            abortar($scope, pagina,db);
            $view.find(".btn-gerar").button('reset');
            $view.find(".btn-exportar").button('reset');
            $view.find('.ampulheta').hide();
            pause = true
            counter = stmts.length
            if (excel === true) $scope.exportaXLSX.apply(this);
        };

        $view.on("click", ".btn-exportarxlsx-interface", function () {

            // var params = {
            //     allColumns: true,
            //     fileName: 'export_' + Date.now(),
            //     columnSeparator: ';'
            // }

            // $scope.gridDados.api.exportDataAsxlsx(params);
            if (dado.length > 0) {
                separator = ";";
                var colunas = "";
                file_name = tempDir3() + "export_" + Date.now();

                var colunas = "Entrantes;Repetidas_30d;%Repetidas_30d;Repetidas_15d;%Repetidas_15d;Repetidas_7;%Repetidas_7d;Repetidas_24h;%Repetidas_24h;Repetidas_2h;%Repetidas_2h;Repetidas_1h;%Repetidas_1h";
                colunas = colunas + "\n";

                colunas += dado[0].Entrantes + separator + dado[0].Repetidas_30d + separator + parseFloat(dado[0].percent_Repetidas_30d.toFixed(0)) + separator +
                    dado[0].Repetidas_15d + separator + parseFloat(dado[0].percent_Repetidas_15d.toFixed(0)) + separator + dado[0].Repetidas_7d + separator + parseFloat(dado[0].percent_Repetidas_7d.toFixed(0)) + separator +
                    dado[0].Repetidas_24h + separator + parseFloat(dado[0].percent_Repetidas_24h.toFixed(0)) + separator + dado[0].Repetidas_2h + separator + parseFloat(dado[0].percent_Repetidas_2h.toFixed(0)) + separator +
                    dado[0].Repetidas_1h + separator + parseFloat(dado[0].percent_Repetidas_1h.toFixed(0));

                // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                // }
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'xlsx', true);
                $scope.xlsx = [];
                $view.find(".btn-exportar").button('click');
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
        });

    });

    $scope.listarTempo = function () {
        $scope.filtros_usados = ""
        resetCanvas();
        console.log('Lista Consolidado')

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
        $scope.filtros_usados += " Data inicio: " + formataDataHora(data_ini) + " Data fim: " + formataDataHora(data_fim)
        $scope.filtros_usados += " Aplicações: " + restringe_aplicacao(filtro_aplicacoes)
        var $btn_gerar_import = $(this);
        $btn_gerar_import.button("loading");
        var stmtTempo = " with entrantes as (" +
            "	select sum(QtdFinalizadas+QtdAbandonadas) as Retidas, sum(QtdDerivadas) as Derivadas" +
            "	from ResumoDesempenhoGeral" +
            "	where Dat_Referencia >= '" + formataDataHora(data_ini) + "' and Dat_Referencia < '" + formataDataHora(data_fim) + "'" +
            "		and Cod_Aplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "		and Cod_Segmento = ''" +
            "), _30d as (" +
            "	select sum(QtdChamadas) as Chamadas," +
            "		sum(QtdRepetidas) as [Repetidas 30d], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 30d], sum(QtdRepDeriv) as [Repetidas derivadas 30d]" +
            "	from TotaisRepetidasHora30d" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "), _15d as (" +
            "	select sum(QtdRepetidas) as [Repetidas 15d], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 15d], sum(QtdRepDeriv) as [Repetidas derivadas 15d]" +
            "	from TotaisRepetidasHora15d" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "), _7d as (" +
            "	select sum(QtdRepetidas) as [Repetidas 7d], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 7d], sum(QtdRepDeriv) as [Repetidas derivadas 7d]" +
            "	from TotaisRepetidasHora7d" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "), _24h as (" +
            "	select sum(QtdRepetidas) as [Repetidas 24h], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 24h], sum(QtdRepDeriv) as [Repetidas derivadas 24h]" +
            "	from TotaisRepetidasHora24h" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "), _2h as (" +
            "	select sum(QtdRepetidas) as [Repetidas 2h], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 2h], sum(QtdRepDeriv) as [Repetidas derivadas 2h]" +
            "	from TotaisRepetidasHora2h" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            "), _1h as (" +
            "	select sum(QtdRepetidas) as [Repetidas 1h], sum(QtdRepFinal+QtdRepAband) as [Repetidas retidas 1h], sum(QtdRepDeriv) as [Repetidas derivadas 1h]" +
            "	from TotaisRepetidasHora1h" +
            "	where DatReferencia >= '" + formataDataHora(data_ini) + "' and DatReferencia < '" + formataDataHora(data_fim) + "'" +
            "		and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ")" +
            ")" +
            "select Retidas + Derivadas as Entrantes, Retidas, Derivadas," +
            "	[Repetidas 30d], [Repetidas retidas 30d], [Repetidas derivadas 30d]," +
            "	[Repetidas 15d], [Repetidas retidas 15d], [Repetidas derivadas 15d]," +
            "	[Repetidas 7d], [Repetidas retidas 7d], [Repetidas derivadas 7d]," +
            "	[Repetidas 24h], [Repetidas retidas 24h], [Repetidas derivadas 24h]," +
            "	[Repetidas 2h], [Repetidas retidas 2h], [Repetidas derivadas 2h]," +
            "	[Repetidas 1h], [Repetidas retidas 1h], [Repetidas derivadas 1h]" +
            "from entrantes, _30d, _15d, _7d, _24h, _2h, _1h";

        log(stmtTempo);

        db.query(stmtTempo, function (columns) {

            dado.push({
                Entrantes: columns[0].value,
                //Retidas: columns[1].value,	
                //Derivadas: columns[2].value,	
                Repetidas_30d: columns[3].value,
                percent_Repetidas_30d: 100 * columns[3].value / columns[0].value,
                //Repetidas_retidas_30d: columns[4].value,	
                //Repetidas_derivadas_30d: columns[5].value,		
                Repetidas_15d: columns[6].value,
                percent_Repetidas_15d: 100 * columns[6].value / columns[0].value,
                //Repetidas_retidas_15d: columns[7].value,		
                //Repetidas_derivadas_15d: columns[8].value,	
                Repetidas_7d: columns[9].value,
                percent_Repetidas_7d: 100 * columns[9].value / columns[0].value,
                //Repetidas_retidas_7d: columns[10].value,		
                //Repetidas_derivadas_7d: columns[11].value,		
                Repetidas_24h: columns[12].value,
                percent_Repetidas_24h: 100 * columns[12].value / columns[0].value,
                //Repetidas_retidas_24h: columns[13].value,		
                //Repetidas_derivadas_24h: columns[14].value,		
                Repetidas_2h: columns[15].value,
                percent_Repetidas_2h: 100 * columns[15].value / columns[0].value,
                //Repetidas_retidas_2h: columns[16].value,		
                //Repetidas_derivadas_2h: columns[17].value,		
                Repetidas_1h: columns[18].value,
                percent_Repetidas_1h: 100 * columns[18].value / columns[0].value,
                //Repetidas_retidas_1h: columns[19].value,		
                //Repetidas_derivadas_1h: columns[20].value						
            });

        }, function (err, num_rows) {
            if (err) {
                console.log('Erro ao buscar Importa: ' + err);
                $btn_gerar_import.button("reset");
                limpaProgressBar($scope, "pag-histograma-repetidas-periodo");
            } else {
                var $btn_exportar = $view.find(".btn-exportar");
                var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
                var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
                $btn_exportar.prop("enable", true);
                $btn_exportar_xlsx.prop("disabled", true);
                $btn_exportar_dropdown.prop("enable", true);

               
                $btn_exportar_jpg.prop("disabled", false);
                
                console.log('Registros encontrados: ' + num_rows);
                retornaStatusQuery(dado.length, $scope);
                $btn_gerar_import.button("reset");

                //limpaProgressBar($scope, "pag-acesso-usuarios");

                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Entrantes", "Repetidas 30d", "Repetidas 15d", "Repetidas 7d", "Repetidas 24h", "Repetidas 2h", "Repetidas 1h"],
                        datasets: [
                            {
                                type: 'bar',
                                label: '',
                                hidden: false,
                                data: [dado[0].Entrantes, 0, 0, 0, 0, 0, 0],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } ,       
                                backgroundColor: [
                                    'Blue', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, dado[0].Repetidas_30d, 0, 0, 0, 0, 0],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, 0, dado[0].Repetidas_15d, 0, 0, 0, 0],
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, 0, 0, dado[0].Repetidas_7d, 0, 0, 0],
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, 0, 0, 0, dado[0].Repetidas_24h, 0, 0],
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, 0, 0, 0, 0, dado[0].Repetidas_2h, 0],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                yAxisID: 'client',
                            },
                            {
                                type: 'bar',
                                label: '',
                                data: [0, 0, 0, 0, 0, 0, dado[0].Repetidas_1h],
                                backgroundColor: [
                                    'Red', 'Red', 'Red', 'Red', 'Red', 'Red', 'Red'
                                ],
                                datalabels: {
                                    anchor: 'end',
                                    align: 'start',
                                    color: 'white',
                                    formatter: function(value) {
                                        return "";
                                    },
                                } , 
                                yAxisID: 'client',
                            }
                        ]
                    },
                    options: {

                        legend: {
                            display: false,
                            
                        },
                        scales: {
                            yAxes: [{
                                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                id: 'client',
                                stacked: true,
                                ticks: {
                                    callback: function (value, index, values) {
                                        if (value >= 1000) {
                                            formatado = formataNumGrande(value);
                                        } else {
                                            formatado = value;
                                        }

                                        return formatado
                                    }
                                },
                            },

                            ],
                            xAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    suggestedMax: 50
                                },
                                display: false
                            }],
                        },

                        // tooltips: {
                        //     callbacks: {
                        //         label: function (tooltipItem, data) {
                        //             if (tooltipItem.datasetIndex === 9) {
                        //                 return (Math.round(tooltipItem.yLabel * 100)) + '%';
                        //             } else {
                        //                 return tooltipItem.yLabel;
                        //             }
                        //         }
                        //     }
                        // },
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            datalabels: {
                                align: 'end',
                                anchor: 'end',
                                display: function (ctx) {
                                    return ctx
                                }
                            }
                        },
                        hover: {
                            animationDuration: 1
                        },
                        animation: {
                            duration: 100,
                            onComplete: function () {
                                var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;
                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = dataset.data[index];
                                        if (data !== 0) {
                                            if (bar._model.y < 15.5) {
                                                ctx.fillStyle = "#FFFFFF";
                                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                                            }

                                            else {
                                                ctx.fillStyle = "#000000";
                                                if (data >= 1000) {
                                                    modificado = formataNumGrande(data)
                                                } else {
                                                    modificado = data
                                                }

                                                if (data !== dado[0].Entrantes) {
                                                    var percent = 100 * data / dado[0].Entrantes
                                                    valor = parseFloat(percent.toFixed(0));
                                                    ctx.fillText("(" + valor + "%)", bar._model.x, bar._model.y - 3)
                                                    ctx.fillText(modificado, bar._model.x, bar._model.y - 20);
                                                } else {
                                                    ctx.fillText(modificado, bar._model.x, bar._model.y);
                                                }


                                            }
                                        }
                                    });
                                });
                            }
                        },
                        //bezierCurve : false,
                        //onAnimationComplete: done
                    },

                });

                // $("#myChart").click(
                //     function (evt) {
                //         var helpers = Chart.helpers;

                //         var eventPosition = helpers.getRelativePosition(evt, myChart.chart);
                //         var mouseX = eventPosition.x;
                //         var mouseY = eventPosition.y;
                //         $scope.gridDados.api.hideOverlay();
                //         if (mouseX > 190 && mouseX < 275) {
                           
                //             busca = "r30"
                //             $view.find('.ampulheta').show();
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                            
                            
                //         }
                //         if (mouseX > 305 && mouseX < 390) {
                           
                //             busca = "r15"
                            
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                           
                //         }
                //         if (mouseX > 420 && mouseX < 505) {
                            
                //             busca = "r7"
                //             $view.find('.ampulheta').show();
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                            
                //         }
                //         if (mouseX > 535 && mouseX < 620 && mouseY > 120) {
                            
                //             busca = "r24"
                //             $view.find('.ampulheta').show();
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                            
                //         }
                //         if (mouseX > 650 && mouseX < 735 && mouseY > 200) {
                            
                //             busca = "r2"
                //             $view.find('.ampulheta').show();
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                           
                //         }
                //         if (mouseX > 765 && mouseX < 850 && mouseY > 230) {
                //            busca = "r1"
                //             $view.find('.ampulheta').show();
                //             dadoExportacao = [];
                //             $scope.listarDetalhado.apply(this);
                           
                //         }

                //     }
                // );
            }
        });
    }

    $scope.listarDetalhado = function () {
        limpaProgressBar($scope, "#" + idView);
        console.log('Lista de exportação')
        pause = false
        var total = 0
        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];        
            var stmt = "select DataHora_Inicio	,NumANI	,ClienteTratado	,NumCPFCNPJ	,CodAplicacao ,QtdSegsDuracao, IndicSucessoGeral, IndicDelig,"+ 
        "SegmentoChamada ,CodUltEstadoRec , DataHoraChamadaAnterior, Repetida30d,Repetida15d,Repetida7d,Repetida24h,Repetida2h,Repetida1h from ChamadasRepetidas where " +
            "DataHora_Inicio >= '" + formataDataHora($scope.periodo.inicio) + "' " +
            "AND DataHora_Inicio <= '" + formataDataHora($scope.periodo.fim) + "'" +
            "and CodAplicacao IN (" + restringe_aplicacao(filtro_aplicacoes) + ") ";
       
        switch (busca) {
            case "r30":
                {
                    stmt += "and Repetida30d = 1";
                    break;
                };
            case "r15": {
                stmt += "and Repetida15d = 1"
                break;
            };
            case "r7":
                {
                    stmt += "and Repetida7d = 1"
                    break;
                };
            case "r24":
                {
                    stmt += "and Repetida24h = 1"
                    break;
                };
            case "r2":
                {
                    stmt += "and Repetida2h = 1"
                    break;
                };
            case "r1":
                {
                    stmt += "and Repetida1h = 1"
                    break;
                };
            default: {
                busca = ""
                break;
            }
        }

        stmts = arrayDezMinutosQuery(stmt, new Date($scope.periodo.inicio), new Date($scope.periodo.fim));
       
        function start(counter){
            if(counter < stmts.length){
              setTimeout(function(){
                stmt = stmts[counter].toString()
                counter++;
                
                if (pause === false){
                mssqlQueryTedious(stmt, function (err, result) {
                    if (err){
                        console.log(err);
                        retornaStatusQuery(undefined, $scope, "Erro de conexão");
                        pause = true
                    }else{
                    
                    total += result.length;				
                    result.forEach(function (e) {
                        
                        $scope.gridDados.rowData.push({
                            data: formataDataHoraBR(e[0].value),
                            chamador: e[1].value,
                            tratado: e[2].value,
                            cpfcnpj: e[3].value,
                            aplicacao: obtemNomeAplicacao(e[4].value),
                            duracao: e[5].value,
                            encerramento: obtemNomeEncerramento(e[6].value, e[7].value),
                            segmento: obtemNomeSegmento(e[8].value),
                            ultestado: obtemEstado(e[4].value, e[9].value) !== undefined ? obtemEstado(e[4].value, e[9].value).nome : "",
                            chamadaAnterior: e[10].value !== null ? formataDataHoraBR(e[10].value) : "",
                            repetida30d: e[11].value,
                            repetida15d: e[12].value,
                            repetida7d: e[13].value,
                            repetida24h: e[14].value,
                            repetida2h: e[15].value,
                            repetida1h: e[16].value
                        });
                        
                    
                        
                        
                        
                    });	
                
                    
                    if(total < 10000 && total > 1){
                        $scope.gridDados.api.setRowData($scope.gridDados.rowData);
                        $scope.gridDados.api.refreshCells({force: true});
                        if(busca !== ""){
                        $("#modal-log").modal();
                        }
                    }
                    porcentagem = counter/stmts.length * 100
                    
                    atualizaInfo2( $scope,"Extração: "+porcentagem.toFixed(0)+"%");
                                    
                    if(counter === stmts.length){ 
                        $view.find('.ampulheta').hide();
                        console.log(total)
                        if (excel === true) $scope.exportaXLSX.apply(this);
                        //retorno(total);											
                    }
                }
           
                });
                
           }else{
               counter = stmts.length
           }
                start(counter);
              }, 500);
            }
          }
          start(0);
        
       

        
        //     stmts.forEach(function (f,indexf) { 
                
          
        
        //     });
         
       
        

        

    
    }
    // function done() {
    //     url=myChart.toBase64Image();
    //     //document.getElementById("url").href=url;
    //       }
    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
          
        var colunas = [
        'Data'
        ,'Chamador'
        ,'Tratado'
        ,'CPF/CNPJ'
        ,'Aplicação'
        ,'Duração'
        ,'Encerração'
        ,'Segmento'
        ,'UltimoEstadoDialogo'
        ,'Data Chamada Anterior'
        ,'Repetida30d'
        ,'Repetida15d'
        ,'Repetida7d'
        ,'Repetida24h'
        ,'Repetida2h'
        ,'Repetida1h'];
        a=[]
        for (var i = 0;i <$scope.gridDados.rowData.length;i++){
            a.push($scope.gridDados.rowData[i].data)
            a.push($scope.gridDados.rowData[i].chamador)
            a.push($scope.gridDados.rowData[i].tratado)
            a.push($scope.gridDados.rowData[i].cpfcnpf)
            a.push($scope.gridDados.rowData[i].aplicacao)
            a.push($scope.gridDados.rowData[i].duracao)
            a.push($scope.gridDados.rowData[i].encerramento)
            a.push($scope.gridDados.rowData[i].segmento)
            a.push($scope.gridDados.rowData[i].ultestado)
            a.push($scope.gridDados.rowData[i].chamadaAnterior)
            a.push($scope.gridDados.rowData[i].repetida30d)
            a.push($scope.gridDados.rowData[i].repetida15d)
            a.push($scope.gridDados.rowData[i].repetida7d)
            a.push($scope.gridDados.rowData[i].repetida24h)
            a.push($scope.gridDados.rowData[i].repetida2h)
            a.push($scope.gridDados.rowData[i].repetida1h)
            $scope.xlsx.push(a);
            a=[]
        }
        console.log(colunas);
        var dadosExcel = [colunas].concat($scope.xlsx);

        var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

        var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
        var wb = XLSX.utils.book_new();
        var milis = new Date();
        var arquivoNome = 'HistogramaRepetidas_' + formataDataHoraMilis(milis) + '.xlsx';
        XLSX.utils.book_append_sheet(wb, ws)
        console.log(wb);
        XLSX.writeFile(wb, tempDir3() + arquivoNome, {
            compression: true
        });
        console.log('Arquivo escrito');
        childProcess.exec(tempDir3() + arquivoNome);


        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
        colunas = [];

    };

}

CtrlHistogramaRepetidas.$inject = ['$scope', '$globals'];