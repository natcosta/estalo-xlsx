function CtrlOfensoresRepetidas($scope, $globals) {

    var contains = function(needle) {
        // Per spec, the way to identify NaN is that it is not equal to itself
        var findNaN = needle !== needle;
        var indexOf;
    
        if(!findNaN && typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;
    
                for(i = 0; i < this.length; i++) {
                    var item = this[i];
    
                    if((findNaN && item !== item) || item === needle) {
                        index = i;
                        break;
                    }
                }
    
                return index;
            };
        }
    
        return indexOf.call(this, needle) > -1;
    };

    win.title = "Ofensores Repetidas";//Brunno 16/01/2019
    var idView = "pag-ofensores-repetidas";
    $scope.versao = versao;
    $scope.rotas = rotas;
    $scope.janela = '1';
    var data = [];
    var dado = [];
    var url;

    var eds = [];
    var repetidas = [];
    var porcentagemRepetidas = []
    
    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    var $view;
    travaBotaoFiltro(0, $scope, "#pag-ofensores-repetidas", win.title);
    //Grafico

    var resetCanvas = function () {
        $('#myChart').remove(); // this is my <canvas> element
        $('#canvas-container').append('<canvas id="myChart"><canvas>');
        canvas = document.getElementById('myChart');
        ctx = canvas.getContext('2d');
        ctx.canvas.width = $('#graph').width(); // resize to parent width
        ctx.canvas.height = $('#graph').height(); // resize to parent height
        var x = canvas.width / 2;
        var y = canvas.height / 2;
        ctx.font = '10pt Verdana';
        ctx.textAlign = 'center';
        ctx.fillText('This text is centered on the canvas', x, y);
    };
    function restringe_aplicacao(valores) {
        if (valores.length === 0) {
            return console.log("valor 0");
        } else {
            var aplicacao = "";
            for (i = 0; i < valores.length; i++) {
                aplicacao = aplicacao + "'" + valores[i] + "'"
                if (i < valores.length - 1) aplicacao += ","
            }

            return aplicacao;
        }
    }

    ctx = document.getElementById('myChart').getContext('2d');
    // Filtros: data e hora
    var agora = new Date();
    var inicio = Estalo.filtros.filtro_data_hora[0] || hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] || hojeFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#" + idView);


        $view.find(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        componenteDataMaisHora($scope, $view);
        carregaAplicacoes($view, false, false, $scope);

        // Carregar filtros


        // Popula lista de segmentos a partir das aplicações selecionadas


        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($view.find('.filtro-site'), 'sites'); });



        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
            $view.find("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $view.find("div.datahora-fim.input-append.date").data("datetimepicker").hide();
        });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseover(function () {
            $view.find('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseout(function () {
            $view.find('div.btn-group.filtro-site').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#" + idView);

            // Testar se a data inicial é maior que a data final
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length > 0) {
                dado = [];
                eds = [];
                repetidas = [];

                $scope.listarTempo.apply(this);
            } else { retornaStatusQuery("erro: tem que selecionar uma aplicação", $scope); }

        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });
        /*$view.on("click", ".btn-gerar-jpg", function () {
            function downloadURI(uri, name) {
                var link = document.createElement("a");
                link.download = name;
                link.href = uri;
                link.click();
            }
            console.log(url);
            downloadURI(url, "chart.png");
                         
                   
        });*/


        $view.on("click", ".btn-exportarCSV-interface", function () {

            // var params = {
            //     allColumns: true,
            //     fileName: 'export_' + Date.now(),
            //     columnSeparator: ';'
            // }

            // $scope.gridDados.api.exportDataAsCsv(params);
            if (dado.length > 0) {
                separator = ";";
                var colunas = "";
                file_name = tempDir3() + "export_" + Date.now();
               
                var colunas = "Estado de Diálogo;" + labelRepetidas;
                colunas = colunas + "\n";

                colunas += dado[0].estadoDialogo+separator+dado[0].repetidas;
              
                // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                // }
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'csv', true);
                $scope.csv = [];
                $view.find(".btn-exportar").button('click');
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
        });

    });

    $scope.listarTempo = function () {
        $scope.filtros_usados = ""
        
        resetCanvas();
        
        console.log('Lista de tempo')
      
        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
            $scope.filtros_usados += " Data inicio: "+formataDataHora(data_ini)+" Data fim: "+formataDataHora(data_fim)
            $scope.filtros_usados += " Aplicações: "+restringe_aplicacao(filtro_aplicacoes)
        
        var $btn_gerar_import = $(this);
        
        $btn_gerar_import.button("loading");
        
        var stmtQuery = "" +
            "SELECT " +
                "j.Nom_Estado, ";

        switch ($scope.janela) {
            case '1':
                stmtQuery = stmtQuery + "SUM(t.QtdRepetidas1h) ";

                break;
            case '2':
                stmtQuery = stmtQuery + "SUM(t.QtdRepetidas2h) ";

                break;
            case '24':
                stmtQuery = stmtQuery + "SUM(t.QtdRepetidas24h) ";    

                break;
        }

        stmtQuery = stmtQuery +
            "FROM " +
                "ResumoEDRepetidas AS t " +
            "INNER JOIN " +
                "Estado_Dialogo AS j ON ( " +
                    "t.CodED = j.Cod_Estado " +
                ")" +
            "WHERE 1=1 " + 
                "AND t.Dia >= '" + formataDataHora(data_ini) + "' " +
                "AND t.Dia <= '" + formataDataHora(data_fim) + "' " +
                "AND t.CodAplicacao IN (" + restringe_aplicacao(filtro_aplicacoes) + ") " +
            "GROUP BY " +
                "j.Nom_Estado " + 
            "ORDER BY ";

            switch ($scope.janela) {
                case '1':
                    stmtQuery = stmtQuery + "SUM(t.QtdRepetidas1h) DESC";
    
                    break;
                case '2':
                    stmtQuery = stmtQuery + "SUM(t.QtdRepetidas2h) DESC";
    
                    break;
                case '24':
                    stmtQuery = stmtQuery + "SUM(t.QtdRepetidas24h) DESC";    
    
                    break;
            }

        log(stmtQuery);

        db.query(stmtQuery, function (columns) {
            if (!contains.call(eds, columns[0].value)) {
                eds.push(columns[0].value);
            }

            repetidas.push(columns[1].value);

            dado.push({
                estadoDialogo: columns[0].value,
                repetidas: columns[1].value
            });
        }, function (err, num_rows) {
            if (err) {
                console.log('Erro ao buscar Importa: ' + err);
                $btn_gerar_import.button("reset");
                limpaProgressBar($scope, "pag-ofensores-repetidas");
            } else {
                var $btn_exportar = $view.find(".btn-exportar");
                var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
                var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
                var $btn_exportar_jpg = $view.find(".btn-gerar-jpg");
              
                $btn_exportar.prop("disabled", false);
                $btn_exportar_jpg.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
                console.log('Registros encontrados: ' + num_rows);
                retornaStatusQuery(dado.length, $scope);
                $btn_gerar_import.button("reset");

                //limpaProgressBar($scope, "pag-acesso-usuarios");

                /*
                myChart = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                      datasets: [
                        {
                          label: "Quantidade de Chamadas",
                          backgroundColor: ["#8e5ea2", "#8e5ea2","#8e5ea2","#8e5ea2","#8e5ea2"],
                          data: [2478,5267,734,784,433]
                        }
                      ]
                    },
                    options: {
                      legend: { display: false },
                      title: {
                        display: true,
                        text: 'Quantidade de chamadas de ofensores de repetidas'
                      }
                    }
                });*/
                
                $('#canvas-container').height(eds.length * 40);
                $('#chart-container').height(window.innerHeight - 220);

                resetCanvas();

                var labelRepetidas;

                switch ($scope.janela) {
                    case '1':
                        labelRepetidas = "Repetidas em 1h";
        
                        break;
                    case '2':
                        labelRepetidas = "Repetidas em 2h";
        
                        break;
                    case '24':
                        labelRepetidas = "Repetidas em 24h";    
        
                        break;
                }
                var soma = 0;
                for (var i = 0; i < repetidas.length; i++) {
                    soma += repetidas[i]
                 }
                myChart = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: eds,
                        datasets: [{
                            label: labelRepetidas,
                            backgroundColor: '#c2a7db',
                            borderColor: '#c2a7db',
                            borderWidth: 1,
                            data: repetidas,
                            datalabels: {
                                anchor: 'center',
                                align: 'right',
                                color: 'black',
                                display: 'auto',
                                formatter: function(value) {
                                    porcentagem  = value/soma *100
                                    if (porcentagem > 1){
                                        valor = parseFloat(porcentagem.toFixed(0));
                                    }else{
                                        valor = parseFloat(porcentagem.toFixed(2))
                                    }
                                    if (value < 1000) {
                                        
                                        return value +" (" + valor + "%)";
                                    }
                                    return formataNumGrande(value) +" (" + valor + "%)";
                                },
                            }        
                        }]
            
                    },
                    options: {
                        // Elements options apply to all of the options unless overridden in a dataset
                        // In this case, we are setting the border of each horizontal bar to be 2px wide
                        elements: {
                            rectangle: {
                                borderWidth: 2,
                            }
                        },
                        responsive: true,
                        legend: {
                            position: 'right',
                        },
                        title: {
                            display: false,
                        },
                        scales: {
                            yAxes: [{
                                barPercentage: 0.8
                            }]
                        },
                    }
                });
                done();
                }
        });
    }
    function done() {
        url=myChart.toBase64Image();
        //document.getElementById("url").href=url;
          }
    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
      $btn_exportar.button('loading');

    var colunas = ["Estado de Diálogo",labelRepetidas];
      
    var a =[dado[0].estadoDialogo,dado[0].repetidas];
    $scope.csv.push(a);
    a=[];
    
      	
      console.log(colunas);
      var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'OfensoresRepetidas_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);


      setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);
      colunas = [];
      
  };

}

CtrlOfensoresRepetidas.$inject = ['$scope', '$globals'];