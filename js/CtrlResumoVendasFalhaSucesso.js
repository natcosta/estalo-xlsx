
function CtrlResumoVendasFalhaSucesso($scope, $globals) {

    win.title = "Resumo de Vendas (Falha / Sucesso)";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-resumo-venda-falha-sucesso", "Resumo de Vendas (Falha / Sucesso)");

    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.colunas = [
        { field: "cod_ic", displayName: "Código", width: 80, pinned: true },
        { field: "nome_ic", displayName: "Descrição", width: 500, pinned: true }
    ];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.datas = [];
    $scope.totais = {};
    $scope.ordenacao = 'nome_ic';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.operacoes = [];
    $scope.iit = false;

    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
        operacoes: []
    };

    /*for (var cod_aplicacao in cache.aplicacoes) {
      var apl = cache.aplicacoes[cod_aplicacao];
      if (apl.itens_controle) {
        apl.itens_controle.indice = geraIndice(apl.itens_controle);
      }
    }*/

    $scope.aba = 2;

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-venda-falha-sucesso");
      treeView('#pag-resumo-venda-falha-sucesso #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-venda-falha-sucesso #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-venda-falha-sucesso #ed', 65,'ED','dvED');
      treeView('#pag-resumo-venda-falha-sucesso #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-venda-falha-sucesso #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-venda-falha-sucesso #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-venda-falha-sucesso #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-venda-falha-sucesso #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-venda-falha-sucesso #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-venda-falha-sucesso #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-resumo-venda-falha-sucesso #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-resumo-venda-falha-sucesso #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

      $(".aba3").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});

      $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});


        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoes($view,false,true,$scope);
        carregaResultados($view);
        carregaSites($view);
        carregaRegioes($view);
		carregaErros($view,true);
        carregaSegmentosPorAplicacao($scope, $view, true);

      carregaGruposProdutoPorAplicacao($scope,$view,true);
      // Popula lista de  grupo de produtos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ 
        carregaGruposProdutoPorAplicacao($scope,$view);
        carregaProdutosPorGrupo($scope,$view);
      });
      carregaProdutosPorGrupo($scope,$view,true);
      // Popula lista de  produtos a partir dos grupo de produtos selecionados
      $view.on("change", "select.filtro-grupo-produto", function(){ carregaProdutosPorGrupo($scope,$view)});


        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
          carregaSegmentosPorAplicacao($scope, $view);
          //obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val());
        });


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-status").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} resultados',
            showSubtext: true
        });

        $view.find("select.filtro-grupo-produto").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} grupos'
        });

        $view.find("select.filtro-produto").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} produtos'
        });


        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });









      //2014-11-27 transição de abas
      var abas = [2,3];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }

        //Marcar todos
        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($('.filtro-site'), 'sites') });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope) });

        // Marca todos os regiões
        $view.on("click", "#alinkReg", function(){
          $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});
        });

        //Marcar todas grupos
        $view.on("click", "#alinkGru", function(){ marcaTodosGrupos($('.filtro-grupo-produto'),$view,$scope)});

        // Marca todos os produtos
        $view.on("click", "#alinkProd", function(){ marcaTodosIndependente($('.filtro-produto'),'produtos')});

        // Marca todos os resultados
        $view.on("click", "#alinkRes", function(){ marcaTodosIndependente($('.filtro-status'),'resultados')});



        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        //$view.on("change", "select.filtro-op", function () {
        //    Estalo.filtros.filtro_operacoes = $(this).val();
        //});



      // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')){
            $('div.btn-group.filtro-grupo-produto').addClass('open');
          }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseout(function () {
            $('div.btn-group.filtro-grupo-produto').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-produto').mouseover(function () {
           //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-produto .btn').hasClass('disabled')){
            $('div.btn-group.filtro-produto').addClass('open');
          $('div.btn-group.filtro-produto>div>ul').css({'max-height':'500px','overflow-y':'auto','min-height':'1px'});
          }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-produto').mouseout(function () {
            $('div.btn-group.filtro-produto').removeClass('open');
        });


      // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-status').mouseover(function () {
            $('div.btn-group.filtro-status').addClass('open');
            $('div.dropdown-menu.open').css({ 'margin-left':'-52px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-status').mouseout(function () {
            $('div.btn-group.filtro-status').removeClass('open');
            $('div.dropdown-menu.open').css({ 'margin-left':'0px' });
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $("div.datahora-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        //$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseover(function () {
        //    $('div.btn-group.filtro-op').addClass('open');
        //    $('div.btn-group.filtro-op>div>ul').css({ 'max-height': '600px', 'overflow-y': 'auto', 'min-height': '1px' });
        //});

        // OCULTAR AO TIRAR O MOUSE
        //$('div.container > ul > li >>div> div.btn-group.filtro-op').mouseout(function () {
        //    $('div.btn-group.filtro-op').removeClass('open');
        //});


      //25/05/2015 Alex - Informação sobre consolidação
      delay($(".filtro-aplicacao >.dropdown-menu li a"), function() {
        cache.apls.forEach(function(apl){
          if(apl.nome === aplFocus){
            console.log(apl.codigo);
            var stmt = db.use+" SELECT 'Aplicação "+apl.codigo+" com registros até',MAX(DatReferencia) FROM ResumoVendaProduto WHERE CodAplicacao = '"+apl.codigo+"' AND DatReferencia > GETDATE() - 2";
            executaThreadBasico('extratorDiaADia',stringParaExtrator(stmt,[]), function(dado){console.log(dado)});
          }
        });
      });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-venda-falha-sucesso");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }


            //var todas = undefined;
            /*for (var i = 0; i <= filtro_aplicacoes.length -1; i++) {
                if (cache.aplicacoes[filtro_aplicacoes[i]].hasOwnProperty('itens_controle')) {
                    if (cache.aplicacoes[filtro_aplicacoes[i]].itens_controle.length <= 100) {
                        delete cache.aplicacoes[filtro_aplicacoes[i]].itens_controle;
                        //todas = true;
                    }
                }
            }*/
			
			
			$scope.listaDados.apply(this);


           /*obtemItensDeControleDasAplicacoes($('select.filtro-aplicacao').val(),true);

           var seguir = [];

            var teste  = setInterval(function(){

              var itens = typeof $('select.filtro-aplicacao').val() ==='string' ? [$('select.filtro-aplicacao').val()] : $('select.filtro-aplicacao').val();

                  for(var i=0; i < itens.length; i++){
                    if(cache.aplicacoes[itens[i]].hasOwnProperty('itens_controle')){
                      seguir.push([itens[i]]);
                    }
                  }

              if(seguir.length === itens.length){
                clearInterval(teste);
                $scope.listaDados.apply(this);
              }
            },500);
          //exibeDataUltAtualAplic();*/

        });

        function exibeDataUltAtualAplic() {

            //Data da última atualização da aplicação
            stmt = db.use + "SELECT top 1 dataAtualizacao FROM ConsolidaItemControle "
            + "WHERE 1 = 1 "
            //stmt += restringe_consulta("ic.Cod_Aplicacao", filtro_aplicacoes, true);

        }

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-venda-falha-sucesso");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');

        /*$(window).scroll(function () {
            $view.find(".fixo-topo-esquerda").css({
                'padding-top': $(this).scrollTop() + 100,
                'padding-left': $(this).scrollLeft() + 30
            });
            $view.find(".fixo-esquerda").css({
                'padding-left': $(this).scrollLeft() + 30
            });
            $view.find(".fixo-topo").css({
                'padding-top': $(this).scrollTop() + 100
            });
        });*/
    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];

      var filtro_produtos = $view.find("select.filtro-produto").val() || [];
      var filtro_grupo_produtos = $view.find("select.filtro-grupo-produto").val() || [];
      var filtro_status = $view.find("select.filtro-status").val() || [];
      if (filtro_status.indexOf("0") !== -1) { filtro_status.push("20"); }


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_regioes.length > 0) { $scope.filtros_usados += " Regiões: " + filtro_regioes; }
        //if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }


        $scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
        var stmt = "";
        var stmt_parts = [];
        $scope.ICs = [];
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };




        var ddds = [];
          filtro_regioes.forEach(function (codigo) { ddds = ddds.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });






        stmt = db.use + "with rv as ( "

        if(filtro_status.indexOf("1")>=0 || filtro_status.length ===0){

        var s_part = " SELECT 'ERRO_'+CAST(CodErro as varchar(max)) as Cod_Item,"
        + " CONVERT(DATE,DATREFERENCIA) as d, SUM(qtdvendas) as q"
        + " FROM RESUMOVENDAPRODUTO AS RV"
        + " left outer join " + db.prefixo + "ProdutosURA as PU"
          + " on RV.CodProduto = PU.CodProduto"
        + " WHERE 1 = 1"
        s_part += " AND DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
        + " AND '"+ formataDataHora(dataConsoliReal(data_fim)) +"'"
        s_part += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
        s_part += restringe_consulta("codDdd", ddds, true);
        if (filtro_segmentos.length > 0) {
          s_part += restringe_consulta("CodSegmento", filtro_segmentos, true);
        }
        s_part += restringe_consulta("CodSite", filtro_sites, true);
        s_part += restringe_consulta("RV.CodProduto", filtro_produtos, true);
        s_part += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true);
        s_part += " and resultado not in ('0','20','2') and coderro not in ('0','2660')";
        //s_part += restringe_consulta("Resultado", filtro_status, true);
        s_part += " GROUP BY coderro, CONVERT(DATE,DATREFERENCIA)";
          stmt_parts.push(s_part);
        }


        if(filtro_status.indexOf("0")>=0 || filtro_status.length ===0){

        var s_part = " SELECT 'Sucesso' as Cod_Item,"
        + " CONVERT(DATE,DATREFERENCIA) as d, SUM(qtdvendas) as q"
        + " FROM RESUMOVENDAPRODUTO AS RV"
        + " left outer join " + db.prefixo + "ProdutosURA as PU"
          + " on RV.CodProduto = PU.CodProduto"
        + " WHERE 1 = 1"
        s_part += " AND DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
        + " AND '"+ formataDataHora(dataConsoliReal(data_fim)) +"'"
        s_part += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
        s_part += restringe_consulta("codDdd", ddds, true);
        if (filtro_segmentos.length > 0) {
          s_part += restringe_consulta("CodSegmento", filtro_segmentos, true);
        }
        s_part += restringe_consulta("CodSite", filtro_sites, true);
        s_part += restringe_consulta("RV.CodProduto", filtro_produtos, true);
        s_part += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true);
        s_part += " and (resultado IN ('0','20') or CodErro in( '2660'))";
        //s_part += restringe_consulta("Resultado", filtro_status, true);
        s_part += " GROUP BY coderro, CONVERT(DATE,DATREFERENCIA)";
        stmt_parts.push(s_part);

        }



        if(filtro_status.indexOf("2")>=0 || filtro_status.length ===0){

        var s_part = " SELECT 'Em andamento' as Cod_Item,"
        + " CONVERT(DATE,DATREFERENCIA) as d, SUM(qtdvendas) as q"
        + " FROM RESUMOVENDAPRODUTO AS RV"
        + " left outer join " + db.prefixo + "ProdutosURA as PU"
          + " on RV.CodProduto = PU.CodProduto"
        + " WHERE 1 = 1"
        s_part += " AND DatReferencia BETWEEN '" + formataDataHora(precisaoHoraIni(data_ini)) + "'"
        + " AND '"+ formataDataHora(dataConsoliReal(data_fim)) +"'"
        s_part += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
        s_part += restringe_consulta("codDdd", ddds, true);
        if (filtro_segmentos.length > 0) {
          s_part += restringe_consulta("CodSegmento", filtro_segmentos, true);
        }
        s_part += restringe_consulta("CodSite", filtro_sites, true);
        s_part += restringe_consulta("RV.CodProduto", filtro_produtos, true);
        s_part += restringe_consulta("PU.GrupoProduto", filtro_grupo_produtos, true);
        s_part += " and resultado = '2'";
        //s_part += restringe_consulta("Resultado", filtro_status, true);
        s_part += " GROUP BY coderro, CONVERT(DATE,DATREFERENCIA)";
        stmt_parts.push(s_part);

        }

        stmt += stmt_parts.join(" UNION ALL ");
        stmt_parts = [];
        stmt += "),";

      stmt += "ev as ("

      if(filtro_status.indexOf("0")>=0 || filtro_status.length ===0){
        var s_part =  " SELECT 'Sucesso' as Cod_Item";
        stmt_parts.push(s_part);
      }

      if(filtro_status.indexOf("2")>=0 || filtro_status.length ===0){
      var s_part  = " SELECT 'Em andamento' as Cod_Item"
      stmt_parts.push(s_part);
      }

      if(filtro_status.indexOf("1")>=0 || filtro_status.length ===0){
       var s_part = " select DISTINCT 'ERRO_'+CAST(CodErro as varchar(max)) as Cod_Item"
       s_part += " from ERROSVENDAS"
       s_part += " WHERE 1 = 1"
       s_part += restringe_consulta("GrupoProduto", filtro_grupo_produtos, true)
       s_part += " and coderro not in ('0','2660')"
       stmt_parts.push(s_part);
      }

      stmt += stmt_parts.join(" UNION ALL ");



      stmt += ") select ev.Cod_Item, rv.d, isnull(rv.q,0) from rv right outer join ev on ev.Cod_Item = rv.Cod_Item"
      stmt += " WHERE 1 = 1"
      /*if(filtro_status.indexOf("0") >=0 && filtro_status.indexOf("1") === -1 || filtro_status.indexOf("2") >=0 && filtro_status.indexOf("1") === -1){
        stmt += " AND ev.Cod_item = 'ERRO_0'"
      }else if (filtro_status.indexOf("1") >=0 && filtro_status.indexOf("0") === -1){
        stmt += " AND ev.Cod_item <> 'ERRO_0'"
      }*/
      stmt += " order by Cod_Item";




        log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery(columns) {

            if ($.isNumeric(columns[0].value) == false) {



                var cod_ic = columns[0].value,
              data = columns[1].value === null ? null : "_" + formataData2(new Date(columns[1].value+ " 00:00:00")),
              data_BR = columns[1].value === null ? null : formataDataBR(new Date(columns[1].value+ " 00:00:00")),
              qtd_entrantes = +columns[2].value

                if (data != null) {
                    var d = $scope.datas[data];
                    if (d === undefined) {
                        d = { data: data, data_BR: data_BR };
                        $scope.datas.push(d);
                        $scope.datas[data] = d;
                        $scope.totais[data] = { qtd_entrantes: 0 };
                    }
                }

                var dado = $scope.dados[cod_ic];
                if (dado === undefined) {
                    dado = {
                        cod_ic: cod_ic,
                        nome_ic: obtemNomeErroVendaArray(cod_ic.replace('ERRO_',''),filtro_grupo_produtos),
                        totais: { geral: { qtd_entrantes: 0 } }
                    };
                    $scope.dados.push(dado);
                    $scope.dados[cod_ic] = dado;
                }

                if (data != null) {
                    dado.totais[data] = { qtd_entrantes: qtd_entrantes };
                    dado.totais.geral.qtd_entrantes += qtd_entrantes;
                    $scope.totais[data].qtd_entrantes += qtd_entrantes;
                    $scope.totais.geral.qtd_entrantes += qtd_entrantes;
                }

                if ($scope.dados.length % 1000 === 0) {
                    $scope.$apply();
                }



            }




        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            console.log("Executando query-> " + stmt + " " + num_rows);
            userLog(stmt, 'Carrega dados', 2, err)
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
            }


            /*//Filtrar sucesso
            if(filtro_status.indexOf("0") >=0 && filtro_status.indexOf("1") === -1 || filtro_status.indexOf("2") >=0 && filtro_status.indexOf("1") === -1){
            $scope.dados = $scope.dados.filter(function (dado) {
              if(dado.cod_ic === "ERRO_0")
                return dado;
            });


            //Filtrar erro
            }else if (filtro_status.indexOf("1") >=0 && filtro_status.indexOf("0") === -1){
              $scope.dados = $scope.dados.filter(function (dado) {
                if(dado.cod_ic !== "ERRO_0")
                  return dado;
              });
            }*/



            // Preencher cod_ic sem prefixo de erro
            $scope.dados.forEach(function (dado) {
              dado.cod_ic = dado.cod_ic.replace('ERRO_','');
              dado.cod_ic = dado.cod_ic.replace('Em andamento','');
              dado.cod_ic = dado.cod_ic.replace('Sucesso','');
            });

            // Preencher com zero datas inexistentes em cada erro
            $scope.dados.forEach(function (dado) {
                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = { qtd_entrantes: 0 };
                    }
                })
            });


            $scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
            $scope.datas.forEach(function (d) {
                $scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: 100 });
            });

            $scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: 100 });
            $scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });

            $scope.$apply();
        });

        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

        $btn_gerar.button('reset');
        $btn_exportar.prop("disabled", false);
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho = $scope.datas.map(function (d) {
            return { value: d.data_BR, bold: 1, hAlign: 'center', autoWidth: true };
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({ value: "Erro", bold: 1, hAlign: 'center', autoWidth: true });

        // Inserir última coluna: total por linha
        cabecalho.push({ value: "Total", bold: 1, hAlign: 'center', autoWidth: true });

        var linhas = $scope.dados.map(function (dado) {
            var linha = $scope.datas.map(function (d) {
                try {
                    return { value: dado.totais[d.data].qtd_entrantes || 0 };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            linha.unshift({ value: dado.cod_ic + " - " + dado.nome_ic });

            // Inserir última coluna: total por linha
            linha.push({ value: dado.totais.geral.qtd_entrantes });

            return linha;
        });

        // Criar última linha, com os totais por data
        var linha_totais = $scope.datas.map(function (d) {
            try {
                return { value: $scope.totais[d.data].qtd_entrantes || 0 };
            } catch (ex) {
                return 0;
            }
        });

        // Inserir primeira coluna
        linha_totais.unshift({ value: "Total", bold: 1 });

        // Inserir última coluna: total geral
        linha_totais.push({ value: $scope.totais.geral.qtd_entrantes });

        // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);

        // Inserir última linha: totais por data
        linhas.push(linha_totais);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'Resumo de vendas', data: linhas, table: true }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 1 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoVendaFalhaSucesso_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}
CtrlResumoVendasFalhaSucesso.$inject = ['$scope', '$globals'];
