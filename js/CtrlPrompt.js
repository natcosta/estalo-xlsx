function CtrlPrompt($scope, $globals) {

    win.title = "Cadastro de Prompt"; //Matheus 28/02/2017
    $scope.versao = versao;

    //$scope.usuario = {};
    $scope.promptData = []; // Dados do grid de usuários
    $scope.promptData2 = []; // Dados Repetidos
    $scope.aplicacoes = []; //
    $scope.grupoAplicacoes = []; // Grupos de Aplicacoes

    $scope.aplicacoes = "";
    $scope.promptCods = [];
    $scope.promptRepetidos = [];

    $scope.prompts = [
        { field: "codprompt", displayName: "Cod. Prompt", width: '25%' },// cellClass:"span1", width: 150,
        { field: "desc", displayName: "Descrição", width: '75%' }//, cellClass:"span2", width: 80,
        //{ field: "checked", displayName: "Hab.", width: '5%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellRepetida" ><input type="radio" name="{{row.entity.codprompt}}" value="{{row.entity.desc}}" class="repeatedRadioBox" ng-model="form" ng-change="updateReplics({{row.entity.codprompt}}, {{row.entity.desc}})"></span></div>' }
        //{ field: "fluxo", displayName: "Fluxo", cellClass: "grid-align", pinned: true },//  cellClass:"span1", width: 80,
        //{ field: "aplicacao", displayName: "Aplicação", cellClass: "grid-align", pinned: true }
    ];


    $scope.gridPrompt = {
        data: $scope.promptData,
        columnDefs: $scope.prompts,
        rowHeight: 55,
        enableColumnResizing: true
    };

    //* Tree view responsável pelo menu de navegação

    var $view;
    var workbook = "";


    var files = document.getElementById('promptInput');

	/*$scope.$watch('filtroApl', function(newValue, oldValue){
		console.log("FiltroAPL: old:"+oldValue+" New: "+newValue);
		$scope.aplicacoes = newValue;	
		//console.log("Grupos: "+$scope.grupoAplicacoes);
		i=0;
		if(newValue!==undefined&&newValue!=null){
			while(i<newValue.length){
				console.log("Comparando: "+newValue[i]);
				var ret = sugereGrupo(newValue[i]);//sugereGrupo($scope.grupoAplicacoes[i].codAplicacao);
				if (ret !== undefined) {
					//$("#modal-dialog").modal('show');
					console.log("Seu tapado, não esqueceu nada não?");
					console.log(ret.aplicacoes);
				} else {
					console.log("Partiu cadastro então")
					console.log("RET: "+ret);
				}
				i++;
			}
		}
		
	});*/


	/*function popAplicacao(){
			console.log("entrou em Pop");
			if(!($scope.filtroApl==null&&$scope.filtroApl.length<=0)){
				console.log("Aplicacoes: "+$scope.aplicacoes);
				$(".labelBtn").popover('show');	
				/*
				setTimeout(function(){
					$(".labelBtn").popover('hide');	
				}, 2000);
				return;
			}
	};*/

    //Função responsável pelo texto em baixo do footsteps
    function promptAlert(texto, html) {

        $(".confirmBtn").prop("disabled", false);

        if (html) {
            $(".promptAlert").html(texto);
            return;
        }

        if (texto != null && texto != undefined) {
            texto =
                $(".promptAlert").html("<strong>" + texto + "</strong>");
            return;
        } else {
            $(".promptAlert").text("");
            return;
        }

    };

    function sugereGrupo(apl) {
        var x = $scope.grupoAplicacoes.filter(function (o) { return o.codAplicacao === apl; });
        if (x.length === 0) return;
        var grupo = x[0].groupName;
        var aplicacoes = $scope.grupoAplicacoes.filter(function (o) { return o.groupName === grupo; }).map(function (o) { return o.codAplicacao; });
        return { groupName: grupo, aplicacoes: aplicacoes };
    }

    function adicionaGrupos(ret) {
        // console.log("Original Aplicacoes: "+$scope.aplicacoes);
        // console.log("Ret Aplicacoes: "+ret.aplicacoes);
        $scope.aplicacoes = $scope.aplicacoes.concat(ret.aplicacoes);
        // console.log("Aplicacoes depois do concat:"+$scope.aplicacoes);
        $scope.aplicacoes = unique($scope.aplicacoes);
        // console.log("Scope Unique: "+$scope.aplicacoes);			
    }

    //Fim da função responsável pelo texto em baixo do footsteps
    /* fixdata and rABS are defined in the drag and drop example */
    function handleFile(e) {
        //promptAlert();
        // console.log("Scopo Apl: "+$scope.aplicacoes);
        if ($scope.aplicacoes.length <= 0 || $scope.aplicacoes == false) {
            $scope.aplicacoes = $(".filtro-aplicacao").val();
        }

        //console.log("Filtro: "+$scope.aplicacoes+" Array: "+$(".filtro-aplicacao").val());
		/*if($scope.aplicacoes==null||$scope.aplicacoes.length<=0){
			promptAlert("Selecione um filtro para prosseguir o cadastro.");
			//notificate("Selecione um filtro para prosseguir o cadastro de prompt's.", $scope);
			return;
		}else{ */

        $scope.promptCods = [];
        $scope.promptRepetidos = [];
        $scope.promptData = [];
        $scope.promptData2 = [];
        var fname = $("#promptInput").val();
        var fextension = fname.substr((~-fname.lastIndexOf(".") >>> 0) + 2);
        // console.log("Fextension: "+fextension);

        var startIndex = (fname.indexOf('\\') >= 0 ? fname.lastIndexOf('\\') : fname.lastIndexOf('/'));
        var filename = fname.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }

        if (fextension.toLowerCase() != "xlsx" && fextension.toLowerCase() != "xls") {
            alerte("Tipo de arquivo não suportado, utilize o template de prompts no formato XLSX!", "Falha ao enviar arquivo");
            //notificate(filename+" possui tipo de arquivo não suportado (."+fextension+"), utilize o template Versátil com o formato .xlsx.", $scope);
            return false;
        }


        var files = e.target.files;
        var i, f;
        var rABS = true;
        for (i = 0; i != files.length; ++i) {
            f = files[i];
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                var data = e.target.result;

                var workbook;
                try {
                    if (rABS) {
                        /* if binary string, read with type 'binary' */
                        workbook = XLSX.read(data, { type: 'binary' });
                    } else {
                        /* if array buffer, convert to base64 */
                        var arr = fixdata(data);
                        workbook = XLSX.read(btoa(arr), { type: 'base64' });
                    }
                } catch (error) {
                    //notificate("Arquivo não suportado ("+error+"), favor utilizar o template Versátil", $scope);
                    $scope.gridPrompt.data = $scope.promptData;
                }
                //$scope.workbook = workbook;
                console.log("Workbook: " + workbook);


                prepareWorkbook(workbook, filename, fextension);
                //}
                /* DO SOMETHING WITH workbook HERE */
            };
            reader.readAsBinaryString(f);
        }
    }

    /*function dialog(message, yesCallback, noCallback) {
        $('.modal-message').html(message);
        var dialog = $('#modal_dialog').dialog();
    
        $('#btnYes').click(function() {
            dialog.dialog('close');
            yesCallback();
        });
        $('#btnNo').click(function() {
            dialog.dialog('close');
            noCallback();
        });
    }*/

    $scope.listaAplicacoes = function () {
        $scope.grupoAplicacoes = [];
        stmt = "Select CodGrupo, CodAplicacao, NomeGrupo from GrupoAplCadastroValores";

        //console.log("Consulta GrupoAplCadastroValores STMT: "+stmt);
        log(stmt);
        db.query(stmt, populateAplicacoes, function (err, num_rows) {

            if (err) {
                console.log("Erro na sintaxe: "+err);
            }

            // console.log(""+num_rows);
        });
    }

    function populateAplicacoes(columns) {
        //console.log("Columns: "+columns);
        var codGrupo = columns[0].value,
            codAplicacao = columns[1].value,
            NomeGrupo = columns[2].value;

        dado = {
            codAplicacao: codAplicacao,
            groupName: NomeGrupo,
            codGroup: codGrupo
        };

        //console.log("Pushando dados para DataGridGroups "+NomeGrupo+" cod: "+codGrupo);

        $scope.grupoAplicacoes.push(dado);
    }

    // Função responsável por tratar a planilha
    function prepareWorkbook(workbook, filename, fextension) {

        $scope.promptCods = [];
        $scope.promptRepetidos = [];
        $scope.promptData = [];
        $scope.promptData2 = [];
        $scope.retotal = [];
        grupos = [];


        // console.log("File Name: "+filename+" file extension: "+fextension);

        var first_sheet_name = workbook.SheetNames[0]; // Sheet 0 = Prompt
        var i = 8; // Célula inicial de conteúdo para prompt = D8
        var j = 0; // Conta vazias
        var totalCells = 0;

        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        var sheet_name_list = workbook.SheetNames;

        if (worksheet["D7"] == undefined || worksheet["D7"] == null || worksheet["E7"] == undefined || worksheet["E7"] == null) {
            // console.log("Cabeçalhos de código e conteúdo não encontrados, planilha padrão não está sendo utilizada");
            alerte("Falha ao carregar arquivo, utilize o template da Versátil para o tratamento de Prompts.", "Falha ao carregar arquivo");
            //notificate("Falha ao carregar, por favor utilize o <a href='http://www.versatec.com.br/downloads/bases/TemplatePrompt.xlsx'>template</a> da Versátil para inclusão, exclusão e alteração de prompts.", $scope);
            return false;
        }


        if (worksheet["D7"].v != "Prompt" || !(worksheet["E7"].v == "Conteúdo" || worksheet["E7"].v == "Conteudo")) {
            // console.log("Prompt: "+worksheet["D7"].v+" Conteudo: "+worksheet["E7"].v);
            alerte("Falha ao carregar arquivo, utilize o template da Versátil para o tratamento de Prompts.", "Falha ao carregar arquivo");
            //notificate("Falha ao carregar, por favor utilize o template da Versátil para inclusão, exclusão e alteração de prompts.", $scope);
            return false;
        }

        while (j < 10) {

            var address_of_cell = 'E' + i; // Célula inicial de conteúdo para prompt = E8
            /* Find desired cell */
            var desired_cell = worksheet[address_of_cell];

            if (desired_cell == null || desired_cell == "") {//||desired_value==undefined){
                j++;
            } else {
                var codigoPrompt = "";

                var fluxoPrompt = "";
                if (worksheet["D" + i] == undefined || worksheet["D" + i] == null) {
                    codigoPrompt = "";
                } else {
                    //console.log("Antes do TRIM? " + worksheet["D" + i].v);
                    codigoPrompt = worksheet["D" + i].v.toString().trim();
                    codigoPrompt = codigoPrompt.replace(".wav", "");
                    codigoPrompt = codigoPrompt.replace(/(?:\r\n|\r|\n)/g, '');
                }

                if (worksheet["G" + i] == undefined || worksheet["G" + i] == null) {
                    fluxoPrompt = "";
                } else {
                    fluxoPrompt = worksheet["G" + i].v;
                }

                var codAplicacao = "";
                if (worksheet["C5"] == undefined || worksheet["C5"] == null) { } else {
                    var codAplicacao = worksheet["C5"].v;
                }
                var linha = "";
                /* Get the value */
                var desired_value = desired_cell.v.toString();
				// desired_value = desired_value.replace(/(?:\r\n|\r|\n)/g, '');
				desired_value = desired_value.replace(/[']/g, "") 
				// desired_value = desired_value.replace(/["]/g, "''") 
				// desired_value = desired_value.unquote();
                desired_value = mysql_real_escape_string(desired_value);

                //console.log("Valor i: "+i+" J: "+j+" desired value: "+desired_value);//Codigo Prompt: "+codigoPrompt+"  Fluxo: "+fluxoPrompt);
                if (codigoPrompt != "" && $scope.promptCods.indexOf(codigoPrompt) == -1) {
                    $scope.promptCods.push(codigoPrompt);
                    linha = { codprompt: codigoPrompt, desc: desired_value, fluxo: fluxoPrompt, aplicacao: codAplicacao }
                    $scope.promptData.push(linha);
                    totalCells++;
                    //console.log("Prompts em memória: "+$scope.promptCods.toString());
                } else {
                    if (codigoPrompt != "" && $scope.promptCods.indexOf(codigoPrompt) > -1) {
                        console.log("Comparando Linhas para o código: " + codigoPrompt);
                        linha = { codprompt: codigoPrompt, desc: desired_value, fluxo: fluxoPrompt, aplicacao: codAplicacao };
                        $scope.promptData.map(function (linhas) {
                            if (linhas.codprompt == linha.codprompt) {
                                // console.log("codpromptA: "+linhas.codprompt+" codprompt: "+linha.codprompt);
                                // console.log("DescriptionA: "+linhas.desc+" DescriptionB: "+linha.desc);
                                if (linhas.desc != linha.desc) {
                                    // console.log("As descrições são diferentes: "+linhas.desc+" codigo:"+linhas.codprompt);
                                    //alerte("O Prompt "+linhas.codprompt+" foi inserido duas ou mais vezes com descrições diferentes, corrija e tente novamente", "Falha ao cadastrar");
                                    $scope.promptData2.push(linhas);
                                    $scope.promptData2.push(linha);
                                    //j = 10;
                                }
                                if (linhas.desc == linha.desc) {
                                    $scope.promptRepetidos.push(codigoPrompt);
                                }
                            }
                        });

                        // console.log("promptData2: "+$scope.promptData2.toString());						
                    }
                }
            }

            i++;
        }

        if (j >= 10) {//
            var solicitante = "";
            if (worksheet["C4"] == undefined || worksheet["C4"] == null) {
                solicitante = "";
            } else {
                solicitante = " solicitada por " + worksheet["C4"].v;
            }            

            $scope.gridPrompt.data = $scope.promptData2.length > 0 ? $scope.promptData2 : $scope.promptData;            
            $scope.step1 = "completed";
            $scope.step2 = "active";
            $('.grid').resize();
            

            if ($scope.promptData2.length > 0) {
                // console.log("Length PD2: "+$scope.promptData2.length+" J: "+j);
                promptAlert("Alguns Prompts aparecem na lista com <font color='red'>descrições diferentes</font>, corrija a planilha. Upload <font color='red'>cancelado!</font>", true);
                $(".confirmBtn").attr("disabled", true)
                //$scope.emptyData();
            } else {
                promptAlert("<strong>" + $scope.promptData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + ".", true);
            }

            ///Tratamento de aplicações selecionadas e grupos a quais pertencem
            i = 0;
            $scope.retotal = [];
            while (i < $scope.aplicacoes.length) {
                // console.log("Comparando: "+$scope.aplicacoes[i]);
                var ret = sugereGrupo($scope.aplicacoes[i]);//sugereGrupo($scope.grupoAplicacoes[i].codAplicacao);

                if (ret !== undefined) {
                    $scope.retotal.push(ret);
                    //console.log("Seu tapado, não esqueceu nada não?");
                    // console.log(ret.aplicacoes);
                } else {
                    // console.log("Partiu cadastro então")
                    // console.log("RET: "+ret);
                }
                i++;
            }

            if ($scope.retotal.length > 0 && $scope.promptData2.length <= 0) {
                $scope.retotal = uniqueApl($scope.retotal);
                i = 0, grupos = "";
                // console.log("$scope.retotal Length: "+$scope.retotal.length+" $scope.retotal String: "+$scope.retotal.toString());

                while (i < $scope.retotal.length) {
                    // console.log("$scope.retotal I:"+i+" grupo: "+$scope.retotal[i].groupName);
                    if (i == 0) {
                        grupos = $scope.retotal[i].groupName;
                    } else {
                        grupos = $scope.retotal[i].groupName + ", " + grupos;
                    }
                    i++;
                }


                confirme("Você selecionou " + $scope.aplicacoes + ", gostaria de inserir em todas as aplicações " + grupos + "?", "Confirme", "Sim", function (realize) {
                    if (realize) {
                        i = 0;
                        while (i < $scope.retotal.length) {
                            // console.log("Ret de I:"+i+" RET: "+$scope.retotal[i]);
                            adicionaGrupos($scope.retotal[i]);
                            i++;
                        }
                        // console.log("BEFORE IF AFTER ADICIONAGRUPOS");
                        if ($scope.promptRepetidos.length >= 1) {
                            // console.log("primeiro if");
                            promptAlert("<strong>" + $scope.promptData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + " com " + $scope.promptRepetidos.length + " duplicadas.", true);
                            // console.log("\n Prompts Repetidos: "+$scope.promptRepetidos);
                        } else {
                            // console.log("segundo if");
                            promptAlert("<strong>" + $scope.promptData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + ".", true);
                            // console.log("Sem registros repetidos na planilha inserida");
                        }

                        // console.log("Scopo Final Aplicacoes: "+$scope.aplicacoes);
                    } else {
                        // console.log("Não realizou a adição de aplicacoes!");
                    }
                });
                if ($scope.promptRepetidos.length >= 1) {
                    promptAlert("<strong>" + $scope.promptData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + " com " + $scope.promptRepetidos.length + " duplicadas.", true);
                    // console.log("\n Prompts Repetidos: "+$scope.promptRepetidos);
                }
                ///Fim do tratamento de aplicações selecionadas e grupos a quais pertencem
            } else {

            }

        }
    }

    //Fim da Função responsável por tratar a planilha 
    $scope.step1 = "active";
    $scope.step2 = "disabled";
    $scope.step3 = "disabled";

    // Função vinculada ao botão cancelar, utilizada para limpar dados importados 
    $scope.emptyData = function () {
        $scope.promptData = [];
        $scope.gridPrompt.data = [];
        $("#promptInput").val('');
        var string = "<strong>Atenção!</strong> utilize o <a href='http://www.versatec.com.br/downloads/bases/TemplatePrompt.xlsx'>template <i class='fa fa-download'></i></a> da Versátil para utilizar a tela cadastro de prompts.";
        promptAlert(string, true);
        // console.log("Dados de GRID e PROMPT esvaziados! FIles:"+$("#promptInput").val(''));

        $scope.aplicacoes = [];
        $(".filtro-aplicacao").val("");
        $scope.step1 = "active";
        $scope.step2 = "disabled";
        $scope.step3 = "disabled";
        $(".confirmBtn").prop("disabled", false);
        $("body").resize();
        $(".filtro-aplicacao").selectpicker('refresh');

    };
    // Fim da função vinculada ao botão cancelar, utilizada para limpar dados importados 
    $scope.registraprompts = function () {
        $(".confirmBtn").prop("disabled", true);
        var mergestmt = "MERGE dbo.prompt as A USING tempdb..#promptTC as B " +
            "ON A.CodPrompt = B.CodPrompt And A.CodAplicacao = B.CodAplicacao " + // and A.indicFinal = 'N'
            "WHEN MATCHED THEN UPDATE SET DesDetalhada = B.DesDetalhada " +
            "WHEN NOT MATCHED THEN INSERT (CodPrompt,DesDetalhada,CodAplicacao, indicFinal) " +
            "VALUES(B.CodPrompt,B.DesDetalhada,B.CodAplicacao,'N');"

        var apl = 0, j = 0, fiveh = 1;
        var insert = "INSERT INTO tempdb..#promptTC(CodPrompt, desdetalhada, Codaplicacao) VALUES ";
        var insertstmt = insert;
        // console.log("Aplenght: "+$scope.aplicacoes.length+" promptsLength: "+$scope.promptData.length);
        while (apl < $scope.aplicacoes.length) {
            codAplicacao = $scope.aplicacoes[apl];
            i = 0;
            while (i < $scope.promptData.length) {
                /*if(i==0&&apl==0){
                    insert = insert+"('"+$scope.promptData[i].codprompt+"', '"+$scope.promptData[i].desc+"', '"+codAplicacao+"'),";
                */
                if ((j / 500) < fiveh) {
                    if ((i + 1 == $scope.promptData.length && (apl + 1 == $scope.aplicacoes.length)) || ((j + 1) / 500 == fiveh)) {
                        // console.log("CodPROMPT: "+$scope.promptData[i].codprompt+" I: "+i+" J: "+j);
                        insert = insert + "('" + $scope.promptData[i].codprompt + "', '" + $scope.promptData[i].desc + "', '" + codAplicacao + "');";
                    } else {
                        insert = insert + "('" + $scope.promptData[i].codprompt + "', '" + $scope.promptData[i].desc + "', '" + codAplicacao + "'),";
                    }
                }
                if (j / 500 == fiveh) {
                    // console.log("i/500=fiveh CodPROMPT: "+$scope.promptData[i].codprompt+" I: "+i+" J: "+j);
                    insert = insert + " " + insertstmt + "('" + $scope.promptData[i].codprompt + "', '" + $scope.promptData[i].desc + "', '" + codAplicacao + "'),";

                    fiveh++;
                }

                i++; //Contador dessa aplicação
                j++; ///Contador total
            }
            apl++;
        }

        // console.log("Insert Final: "+insert);

        // console.log("Registra prompts....");
        if (!$scope.aplicacoes) {
            alerte("Nenhuma aplicação foi selecionada para o cadastro de prompt's, por favor inicie o processo novamente.", "Falha ao confirmar cadastro.");
            $scope.emptyData();
            return false;
        }

        stmt = "if OBJECT_ID('tempdb..#promptTC') is null" +
            " CREATE TABLE #promptTC(" +
            "[CodPrompt] [varchar](120) NOT NULL," +
            "[DesDetalhada] [varchar](max) NOT NULL," +
            "[CodAplicacao] [varchar](20) NOT NULL)" +
            " else truncate table #promptTC " + insert + " " + mergestmt;

        // console.log("Complete Stmt: "+stmt);
        log(stmt);
        db.query(stmt, function () { }, function (err, num_rows) {
            // console.log("Realizou query!");
            userLog(stmt, "Insert de Prompts ", 1, err);
            if (err) {
                // console.log("Erro na sintaxe: "+err);
                promptAlert("Não foi possível realizar o cadastro da planilha, por favor solicite-o a suporte@versatec.com.br");

            } else {
                //insertprompts();
                // console.log("Cadastros efetuados com sucesso!");
                // console.log("DB Merge OK!");
                // console.log("End Step1: "+$scope.step1+" Step2: "+$scope.step2+" Scope3: "+$scope.step3);
                $scope.step1 = "completed";
                $scope.step2 = "completed";
                $scope.step3 = "completed";
                // console.log("Done Step1: "+$scope.step1+" Step2: "+$scope.step2+" Scope3: "+$scope.step3);
                promptAlert("<strong>" + $scope.promptData.length + " prompts </strong> cadastrados.", true);
                $("body").resize();
                /*setTimeout(function(){
                    alerte("Dados cadastrados com sucesso nas aplicações "+$scope.aplicacoes.toString()+".", "Dados cadastrados");
                    $scope.emptyData();	
                }, 1000);*/
            }
            // console.log(""+num_rows);
        });
    };

    $scope.listaAplicacoes();

    /// Função responsável por carregar as funções quando o conteúdo estiver carregado
    $scope.$on('$viewContentLoaded', function () {
        // console.log("PromptData: "+$scope.promptData+" Length: "+$scope.promptData.length);

        $view = $("#pag-prompts");
        $("#pag-prompts .botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        carregaListaAplicacoes($view, false, $scope);
        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });
        // Fim do treeview responsável pelo menu de navegação

        //carregaRotas($view);

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-prompts");
        }
        //var files = document.getElementById('promptInput');
        if (files.addEventListener) files.addEventListener('change', handleFile, false);



        /*$('.labelBtn').popover({
            html: true,
            trigger: 'hover',
            placement: 'bottom',
            content: function () {
                return '<div class="box">Favor selecione a aplicação</div>';
            },
            animation: false
        }).on({
        	
            show: function (e) {
                if(!($scope.filtroApl==null&&$scope.filtroApl.length<=0)){
                	
                    var $this = $(this);
    
                    // Currently hovering popover
                    $this.data("hoveringPopover", true);
    
                    // If it's still waiting to determine if it can be hovered, don't allow other handlers
                    if ($this.data("waitingForPopoverTO")) {
                        e.stopImmediatePropagation();
                    }
                }
            },
            hide: function (e) {
                    var $this = $(this);
                	
                    // If timeout was reached, allow hide to occur
                    if ($this.data("forceHidePopover")) {
                        $this.data("forceHidePopover", false);
                        return true;
                    }
    
                    // Prevent other `hide` handlers from executing
                    e.stopImmediatePropagation();
    
                    // Reset timeout checker
                    clearTimeout($this.data("popoverTO"));
    
                    // No longer hovering popover
                    $this.data("hoveringPopover", false);
    
                    // Flag for `show` event
                    $this.data("waitingForPopoverTO", true);
    
                    // In 1500ms, check to see if the popover is still not being hovered
                    $this.data("popoverTO", setTimeout(function () {
                        // If not being hovered, force the hide
                        if (!$this.data("hoveringPopover")) {
                            $this.data("forceHidePopover", true);
                            $this.data("waitingForPopoverTO", false);
                            $this.popover("hide");
                        }
                    }, 1500));
    
                    // Stop default behavior
                    return false;
            }
        });
            */

    });
}

CtrlPrompt.$inject = ['$scope', '$globals'];
