function CtrlMonitorExportacaoMailing($scope, $globals) {

    win.title = "Status D -1";
    $scope.versao = versao;
    $scope.rotas = rotas;

    $scope.localeGridText = {
            contains: 'Contém',
            notContains: 'Não contém',
            equals: 'Igual',
            notEquals: 'Diferente',
            startsWith: 'Começa com',
            endsWith: 'Termina com'
    }

    var $view;

    $scope.gridExporta = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
            columnDefs: [
                    { headerName: 'Processo', field: 'procExportacao' },
                    { headerName: 'Atualizado até', field: 'proxExecExport' }
            ],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true
    };

    $scope.gridMailing = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado</span>',
            columnDefs: [
                    { headerName: 'Processo', field: 'procMailing' },
                    { headerName: 'Atualizado até', field: 'proxExecMailing' },
                    { headerName: 'Última verificação', field: 'statusMailing' }
            ],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true
    }

    $scope.gridConsolidacao = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado</span>',
            columnDefs: [
                    {
                            headerName: 'Aplicação',
                            field: 'codAplicacao'
                    },
                    {
                            headerName: 'Data Hora',
                            field: 'dataHora'
                    },
                    {
                            headerName: 'Chamadas',
                            field: 'qtdChamadas'
                    }, {
                            headerName: 'Consolidadas',
                            field: 'qtdChamadas2'
                    },
                    {
                            headerName: 'Diferença',
                            field: 'diferenca'
                    },
                    {
                            headerName: 'Diferença Percentual',
                            field: 'diferencaPercent'
                    }
            ],
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true
    }

    $scope.dados = [];
    $scope.csv = [];
    $scope.difPorAplicacao = [];
    $scope.difAplicacoes = [];

    $scope.$on('$viewContentLoaded', function () {
            $view = $("#pag-monitor-exportacao-mailing");

            limpaProgressBar($scope, "#pag-monitor-exportacao-teradata");
            $scope.gridExporta.rowData = [];
            $scope.listarExporta.apply(this);

            /*

            $view.on("click", ".btn-gerar-teradata", function() {
                    limpaProgressBar($scope, "#pag-monitor-exportacao-teradata");
                    $scope.gridExporta.rowData = [];
                    $scope.listarExporta.apply(this)

            });

            $view.on("click", ".btn-gerar-mailing", function() {
                    limpaProgressBar($scope, "#pag-monitor-exportacao-mailing");
                    $scope.gridMailing.rowData = [];
                    $scope.listarMailing.apply(this)

            }); */

            $view.on("click", ".btn-exportar", function() {
                    $scope.exportaXLSX.apply(this);
            });

            // Botão abortar Alex 23/05/2014
            $view.on("click", ".abortar", function() {
                    $scope.abortar.apply(this);
            });

            //Alex 23/05/2014
            $scope.abortar = function() {
                    abortar($scope, "#pag-monitor-exportacao-mailing");
            }
    });

    $scope.listarExporta = function() {
            console.log('Buscando em ControleExtratores o Batimento de recarga boleto e cartão')
            var $btn_gerar_exporta = $(this);
            $btn_gerar_exporta.button("loading");
            var stmtExporta = "SELECT id, dataini - 1 FROM ControleExtratores WHERE ID LIKE '%Batimento%'  and dataini > '2019-01-01'"
            db.query(stmtExporta, function(columns) {

                    $scope.gridExporta.rowData.push({
                            procExportacao: columns[0].value.replace('TNL', ''),
                            proxExecExport: formataDataHoraBR(columns[1].value)
                    });

            }, function(err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar exportação: ' + err);
                    } else {
                            console.log('Registros encontrados: ' + num_rows);
                            retornaStatusQuery($scope.gridExporta.rowData.length, $scope);

                            $scope.gridExporta.api.setRowData($scope.gridExporta.rowData);
                            $scope.gridExporta.api.refreshCells({force: true});

                            $btn_gerar_exporta.button("reset");

                            limpaProgressBar($scope, "#pag-monitor-exportacao-mailing");
                            $scope.gridMailing.rowData = [];
                            $scope.listarMailing.apply(this)
                    }
            });
    }

    $scope.listarMailing = function() {
            console.log('Buscando em ControleExtratores o ID LIKE Mailing')
            var $btn_gerar_mailing = $(this);
            $btn_gerar_mailing.button("loading");
            var stmtMailing = "SELECT * FROM ControleExtratores WHERE ID LIKE '%TNLMailing%'"
            db.query(stmtMailing, function(columns) {

                    $scope.gridMailing.rowData.push({
                            procMailing: columns[1].value.replace('TNL', ''),
                            proxExecMailing: formataDataHoraBR(columns[3].value),
                            statusMailing: JSON.parse(columns[7].value).ts.replace(/(\d{4})-(\d{2})-(\d{2}) (.+)/g, "$3/$2/$1 $4")
                    });

            }, function(err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar mailing: ' + err);
                    } else {
                            console.log('Registros encontrados: ' + num_rows);
                            retornaStatusQuery($scope.gridMailing.rowData.length, $scope);

                            $scope.gridMailing.api.setRowData($scope.gridMailing.rowData);
                            $scope.gridMailing.api.refreshCells({force: true});

                            $btn_gerar_mailing.button("reset");

                            limpaProgressBar($scope, "#pag-monitor-exportacao-mailing");
                            $scope.gridConsolidacao.rowData = [];
                            $scope.listarConsolidacao.apply(this)
                    }
            });
    }

    $scope.listarConsolidacao = function () {

            $scope.gridConsolidacao.rowData = [];
            $scope.gridConsolidacao.api.setRowData([]);

            var $btn_exportar = $view.find(".btn-exportar");
            $btn_exportar.prop("disabled", true);

            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            console.log("FiltroAplicacoes:" + filtro_aplicacoes);

            var dataDZeroHora = new Date();
            var dataDMenusUmZeroHora = new Date();

            dataDZeroHora.setHours(0, 0, 0, 0);
            dataDMenusUmZeroHora.setHours(0, 0, 0, 0);

            dataDMenusUmZeroHora.setDate(dataDMenusUmZeroHora.getDate() - 1);

            $scope.inicioConsolidacao = ("0" + dataDMenusUmZeroHora.getDate()).slice(-2) + '/' + ("0" + (dataDMenusUmZeroHora.getMonth() + 1)).slice(-2) + '/' + dataDMenusUmZeroHora.getFullYear()
            $scope.fimConsolidacao = ("0" + dataDZeroHora.getDate()).slice(-2) + '/' + ("0" + (dataDZeroHora.getMonth() + 1)).slice(-2) + '/' + dataDZeroHora.getFullYear()

            var $btn_gerar = $(this);
            $btn_gerar.button("loading");
            var stmt =
                    " with cte as (" +
                    " select Cod_Aplicacao as CodAplicacao, dateadd(day, datediff(day, 0, Dat_Referencia), 0) as DataHora," +
                    " sum(QtdFinalizadas+QtdDerivadas+QtdAbandonadas+QtdTransfURA) as QtdChamadas" +
                    " from ResumoDesempenhoGeralHora" +
                    " where Dat_Referencia >= '" + formataHoraZero(formataDataHora(dataDMenusUmZeroHora)) + "' and Dat_Referencia < '" + formataHoraZero(formataDataHora(dataDZeroHora)) + "'" +
                    " and Cod_Segmento = ''";
            // if (filtro_aplicacoes.length > 0) stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
            stmt +=
                    " group by Cod_Aplicacao, dateadd(day, datediff(day, 0, Dat_Referencia), 0)" +
                    " ), cte2 as (" +
                    " select Cod_Aplicacao as CodAplicacao, Dat_Referencia as DataHora," +
                    " sum(QtdFinalizadas+QtdDerivadas+QtdAbandonadas+QtdTransfURA) as QtdChamadas" +
                    " from ResumoDesempenhoGeral" +
                    " where Dat_Referencia >= '" + formataHoraZero(formataDataHora(dataDMenusUmZeroHora)) + "' and Dat_Referencia < '" + formataHoraZero(formataDataHora(dataDZeroHora)) + "'" +
                    " and Cod_Segmento = ''" +
                    " group by Cod_Aplicacao, Dat_Referencia" +
                    " )" +
                    " select cte.CodAplicacao, cte.DataHora," +
                    " cte.QtdChamadas, isnull(cte2.QtdChamadas, 0) as QtdChamadas2," +
                    " cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) as Diferenca," +
                    " str((100.0 * (cte.QtdChamadas - isnull(cte2.QtdChamadas, 0))) / cte.QtdChamadas, 4, 2) + '%' as DiferencaPercent" +
                    " from cte" +
                    " left outer join cte2 on cte.CodAplicacao = cte2.CodAplicacao and cte.DataHora = cte2.DataHora" +
                    " where cte.QtdChamadas - isnull(cte2.QtdChamadas, 0) <> 0" +
                    " order by cte.CodAplicacao, cte.DataHora";
            db.query(stmt, function (columns) {
                    var codAplicacao = columns[0].value,
                            dataHora = columns[1].value,
                            qtdChamadas = columns[2].value,
                            qtdChamadas2 = columns[3].value,
                            diferenca = columns[4].value,
                            diferencaPercent = columns[5].value

                    $scope.dados.push({
                            codAplicacao: codAplicacao,
                            dataHora: formataDataHoraBR(dataHora),
                            qtdChamadas: qtdChamadas,
                            qtdChamadas2: qtdChamadas2,
                            diferenca: diferenca,
                            diferencaPercent: diferencaPercent
                    });

                    $scope.gridConsolidacao.rowData.push({
                            codAplicacao: codAplicacao,
                            dataHora: formataDataHoraBR(dataHora),
                            qtdChamadas: qtdChamadas,
                            qtdChamadas2: qtdChamadas2,
                            diferenca: diferenca,
                            diferencaPercent: diferencaPercent
                    });

                    $scope.csv.push([
                            codAplicacao,
                            formataDataHoraBR(dataHora),
                            qtdChamadas,
                            qtdChamadas2,
                            diferenca,
                            diferencaPercent
                    ]);

                    if ($scope.difPorAplicacao[codAplicacao]) {
                            $scope.difPorAplicacao[codAplicacao] += formataDataHora(dataHora) + ',';
                            $scope.difAplicacoes[codAplicacao] = true;
                    } else {
                            $scope.difPorAplicacao[codAplicacao] = formataDataHora(dataHora) + ',';
                            $scope.difAplicacoes[codAplicacao] = false;
                    }
            }, function (err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar diferenças: ' + err);
                    } else {

                            $scope.gridConsolidacao.api.hideOverlay();
                            retornaStatusQuery($scope.gridConsolidacao.rowData.length, $scope);

                            $scope.gridConsolidacao.api.setRowData($scope.gridConsolidacao.rowData);
                            $scope.gridConsolidacao.api.refreshCells({force: true});

                            for (var i in $scope.difAplicacoes) {
                                    console.log('Iterando para ' + i);
                                    if ($scope.difAplicacoes[i]) console.log(i + ' encontrado');
                            }
                            $btn_gerar.button("reset");
                            if ($scope.dados.length > 0) {
                                    $btn_exportar.prop("disabled", false);
                            }
                    }

            });
            console.log(stmt);

    }

    function reverteDataBR(dataBR) {
            return dataBR.replace(/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}:\d{2}:\d{2})/, '$3-$2-$1 $4');
    }

    function formataHoraZero(dataHora) {
            return dataHora.replace(/(\d{4}-\d{2}-\d{2}).+/g, '$1 00:00:00');
    }

    function formataHoraCheia(dataHora) {
            return dataHora.replace(/(\d{4}-\d{2}-\d{2} \d{2}).+/g, '$1:00:00');
    }
}

CtrlMonitorExportacaoMailing.$inject = ['$scope', '$globals'];
