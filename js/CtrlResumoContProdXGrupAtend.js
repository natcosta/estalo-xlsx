function CtrlResumoContProdXGrupAtend($scope, $globals) {

    win.title="Resumo contagem produto x grupo atendimento"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-contagem-prod-x-grupo-atend", "Resumo contagem produto x grupo atendimento");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;



    $scope.total_qtdDerivadas;
    $scope.total_qtdTransferidas;
    $scope.total_qtdNEncontradas;
    $scope.total_totalGeral;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013
        sites: [],
        segmentos: [],
        isHFiltroED: false,
        porDia: false,
        porDDD: false
    };

    // Filtro: ddd
    $scope.ddds = cache.ddds;

    $scope.porDDD = null;

    //02/06/2014 Flag
    $scope.porRegEDDD = $globals.agrupar;

    $scope.aba = 2;


    var empresas = cache.empresas.map(function(a){return a.codigo});

    $scope.valor = "regra";



    function geraColunas(){
      var array = [];
array.push(
         { field: "datReferencia", displayName: "Data", width: 150, pinned: true }
       );

      if($scope.valor === "empresa"){



      for(var i= 0; i<empresas.length;i++){
        array.push(
        { field: empresas[i], displayName: empresas[i], width: 100, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' });

      }
    }

      if($scope.valor === "regra"){

        array.push(
          { field: "segmento", displayName: "Segmento", width: 145, pinned: true },
          { field: "produto", displayName: "Operação", width: 145, pinned: true },
          { field: "qtdDerivadas", displayName: "Qtd Operação 1", width: 145, pinned: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' },
          { field: "qtdTransferidas", displayName: "Qtd Operação 2", width: 145, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'},
      { field: "percTransf", displayName: "% Operação 2", width: 145, pinned: true, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>' }/* ,
          { field: "qtdNEncontradas", displayName: "Não encontradas", width: 145, pinned: true },
          { field: "totalGeral", displayName: "Total geral", width: 145, pinned: true } */
        );

      }


      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-contagem-prod-x-grupo-atend");
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #ed', 65,'ED','dvED');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-contagem-prod-x-grupo-atend #admin', 255,'Administrativo','dvAdmin');
        treeView('#pag-resumo-contagem-prod-x-grupo-atend #monitoracao', 275, 'Monitoração', 'dvReparo');
            treeView('#pag-resumo-contagem-prod-x-grupo-atend #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
			treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/


       $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});


        $(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
        $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
        $('.navbar-inner').css('height','70px');
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

        //minuteStep: 5

      //19/03/2014
      componenteDataHora ($scope,$view);

        carregaRegioes($view);
        carregaAplicacoes($view,false,false,$scope);
        carregaOperacoesECH($view);
        carregaSites($view);
        carregaEmpresas($view);
        carregaRegras($view);
        carregaSkills($view);



        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


      carregaDDDsPorAplicacao($scope,$view,true);
      // Popula lista de  DDDs a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaDDDsPorAplicacao($scope,$view)});


      $view.on("change","input:radio[name=radioB]",function() {

        if($(this).val() === "empresa"){
          $('#porDDD').attr('disabled', 'disabled');
          $('#porDDD').prop('checked',false);
        }else{
          $('#porDDD').attr('disabled', false);
        }

      });


        /*$view.find(".selectpicker").selectpicker({
        //noneSelectedText: 'Nenhum item selecionado',
        countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

       $view.find("select.filtro-operacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Operações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-ed").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} EDs'
        });


        $view.find("select.filtro-empresa").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Empresas'
        });

        $view.find("select.filtro-regra").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Regras'
        });

        $view.find("select.filtro-skill").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Skills'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-ddd", function () {
            var filtro_ddds = $(this).val() || [];
            Estalo.filtros.filtro_ddds = filtro_ddds;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

      //2014-11-27 transição de abas
      var abas = [2,3,4];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        // Marca todos os empresas
        $view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});

        // Marca todos os operacoes
        $view.on("click", "#alinkOper", function(){ marcaTodosIndependente($('.filtro-operacao'),'operacoes')});

        // Marca todos os regras
        $view.on("click", "#alinkRegra", function(){ marcaTodosIndependente($('.filtro-regra'),'regra')});

        // Marca todos os skills
        $view.on("click", "#alinkSkill", function(){ marcaTodosIndependente($('.filtro-skill'),'skill')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        // GILBERTO 18/02/2014



      // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.btn-group.filtro-ddd>div>ul').css({'max-height':'600px','overflow-y':'auto','min-height':'1px'});
          }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.dropdown-menu.open').css({ 'margin-left':'-110px' });
            $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.dropdown-menu.open').css({ 'margin-left':'0px' });
            $('div.btn-group.filtro-ddd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
            $('div.btn-group.filtro-empresa').addClass('open');
            $('div.btn-group.filtro-empresa>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '300px' })

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
            $('div.btn-group.filtro-empresa').removeClass('open');

        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseover(function () {
            $('div.btn-group.filtro-operacao').addClass('open');
            $('div.btn-group.filtro-operacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseout(function () {
            $('div.btn-group.filtro-operacao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseover(function () {
            $('div.btn-group.filtro-regra').addClass('open');
            $('div.btn-group.filtro-regra>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseout(function () {
            $('div.btn-group.filtro-regra').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseover(function () {
            $('div.btn-group.filtro-skill').addClass('open');
            $('div.btn-group.filtro-skill>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseout(function () {
            $('div.btn-group.filtro-skill').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ed').addClass('open');
                $('div.btn-group.filtro-ed>div>ul').css(
                  { 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' }
                );
            }
        });



        // OCULTAR AO TIRAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseout(function () {
            $('div.btn-group.filtro-ed').removeClass('open');
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-resumo-contagem-prod-x-grupo-atend");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-resumo-contagem-prod-x-grupo-atend");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });







        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };








    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_ed = $view.find("select.filtro-ed").val() || [];
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_regras = $view.find("select.filtro-regra").val() || [];
        var filtro_skills = $view.find("select.filtro-skill").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
        if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
        if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }
        if (filtro_regras.length > 0) { $scope.filtros_usados += " Regras: " + filtro_regras; }
        if (filtro_skills.length > 0) {

          var filtro_skills_mod = [];
          for(var i = 0; i < filtro_skills.length; i++){
            var lsRegExp = /%/g;
            filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp,'&lt;'));
          }
          $scope.filtros_usados += " Skills: " + filtro_skills_mod;
        }

        if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
          var options = [];
          var v = [];
          filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
          unique(v).forEach((function (filtro_ddds) {
            return function (codigo) {
              var ddd = cache.ddds.indice[codigo];
              filtro_ddds.push(codigo);
            };
          })(filtro_ddds));
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
        }else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
          $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }


        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };


      empSelect = "";
      empSelectIn = "";
      empObjParts = "";

      if($scope.valor === "empresa"){

      for(var i= 0; i<empresas.length;i++){ empSelect+="["+empresas[i]+"] as "+empresas[i]+"," }
      for(var i= 0; i<empresas.length;i++){ empSelectIn+="["+empresas[i]+"]," }
      for(var i= 0; i<empresas.length;i++){ empObjParts+= empresas[i]+ ":" +empresas[i]+"," }

      empSelect = empSelect.substring(0,empSelect.length-1);
      empSelectIn = empSelectIn.substring(0,empSelectIn.length-1);
      empObjParts = empObjParts.substring(0,empObjParts.length-1);

      stmt = db.use
      + " SELECT DatReferencia, " + empSelect
      + " FROM ("
      + " SELECT qtdchamadas, codempresa"
      + " , "+($scope.filtros.porDia ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""

         + " FROM ResumoECH"

      + " WHERE 1 = 1"
      + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
      stmt += restringe_consulta("CodSite", filtro_sites, true)
      stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
      stmt += restringe_consulta("CodRegra", filtro_regras, true)
      stmt += restringe_consulta("ddd", filtro_ddds, true)
      stmt += restringe_consulta_like2("CodSkill", filtro_skills)
      stmt += ") p"
      + " PIVOT"
      + " ("
      + " SUM (qtdchamadas)"
      + " FOR codempresa IN ( "+empSelectIn+" )"
      + ") AS pvt"
      + " ORDER BY DatReferencia";


      }else if($scope.valor === "regra"){
        stmt = db.use
        + " SELECT "+($scope.filtros.porDia ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""

        //+ ", CodAplicacao, CodSegmento, CodEmpresa, CodOperacao"
    + ", s.DescSegmento, CodOperacao"
        + ", SUM(QtdDerivadas) as qtdDer"
        + ", SUM(QtdTransferidas) qtdTransf"
        + ", SUM(QtdNaoEncontradas) as qtdNaoEncon"
        + ", SUM(qtdchamadas) as qtd"
        + " FROM ResumoECH r"
        + " left outer join Segmentos s"
        + " on r.CodSegmento = s.CodSegmento"
        + " WHERE 1 = 1"
        + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
        + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
        + " AND QtdDerivadas > 0"
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
        stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
        stmt += restringe_consulta("CodSite", filtro_sites, true)
        stmt += restringe_consulta("r.CodSegmento", filtro_segmentos, true)
        stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
        stmt += restringe_consulta("CodRegra", filtro_regras, true)
        stmt += restringe_consulta("ddd", filtro_ddds, true)
        stmt += restringe_consulta_like2("CodSkill", filtro_skills)
        stmt += " GROUP BY "+($scope.filtros.porDia ? "convert(date,DatReferencia)" : "DatReferencia")+", s.DescSegmento, CodOperacao";
        stmt += " ORDER BY "+($scope.filtros.porDia ? "convert(date,DatReferencia)" : "DatReferencia")+", s.DescSegmento, CodOperacao";

      }






        executaQuery = executaQuery2;
        log(stmt);


        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function executaQuery2(columns){




  var data_hora = new Date(columns["DatReferencia"].value.length === 10 ? columns["DatReferencia"].value + " 00:00:00" : columns["DatReferencia"].value),
              datReferencia = $scope.filtros.porDia ? formataDataBR(data_hora) : formataDataHoraBR(data_hora);


          if($scope.valor === "empresa"){
            for(var i= 0; i<empresas.length;i++){
              eval("var " + empresas[i] + "=+columns['"+empresas[i]+"'].value;");
            }
            empObjParts = "";
            for(var i= 0; i<empresas.length;i++){ empObjParts+="\""+empresas[i]+ "\":\"" +eval(empresas[i])+"\"," }
            empObjParts = empObjParts.substring(0,empObjParts.length-1);
            var obj = {};
            eval("var obj = '{ \"data_hora\": \"" + data_hora + "\", \"datReferencia\": \"" + datReferencia + "\", " + empObjParts + "}'");
            $scope.dados.push(JSON.parse(obj));

          }else if($scope.valor === "regra"){


//if($scope.filtros.porDia){

var

        produto = columns["CodOperacao"].value,
        segmento = columns["DescSegmento"].value,
              qtdDerivadas = +columns["qtdDer"].value,
              qtdTransferidas = +columns["qtdTransf"].value ,
              qtdNEncontradas = +columns["qtdNaoEncon"].value,
        percTransf = (100 * qtdTransferidas / qtdDerivadas).toFixed(2)
              totalGeral = qtdDerivadas + qtdTransferidas + qtdNEncontradas;



            $scope.total_qtdDerivadas = $scope.total_qtdDerivadas + qtdDerivadas;
            $scope.total_qtdTransferidas = $scope.total_qtdTransferidas + qtdTransferidas;
            $scope.total_qtdNEncontradas = $scope.total_qtdNEncontradas + qtdNEncontradas;
            $scope.total_totalGeral = $scope.total_totalGeral + totalGeral;


            $scope.dados.push({
        datReferencia:datReferencia,
              produto: produto,
        segmento: obtemNomeSegmento(segmento),
              qtdDerivadas: qtdDerivadas,
              qtdTransferidas: qtdTransferidas,
        percTransf: percTransf/* ,
              qtdNEncontradas: qtdNEncontradas,
              totalGeral: totalGeral */
            });





//}else{


//}



          }


            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
           if($scope.dados.length%1000===0){
                $scope.$apply();
              }
        }



      $scope.total_qtdDerivadas = 0;
    $scope.total_qtdTransferidas = 0;
    $scope.total_qtdNEncontradas = 0;
    $scope.total_totalGeral = 0;


    db.query(stmt, executaQuery, function (err, num_rows) {
        userLog(stmt, 'Carrega dados', 2, err)


                  if($scope.porRegEDDD===true){
                    $('#pag-resumo-contagem-prod-x-grupo-atend table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1170px','margin-bottom':'100px'});
                  }else{
                    $('#pag-resumo-contagem-prod-x-grupo-atend table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1280px','margin-bottom':'100px'});
                  }
                    console.log("Executando query-> "+stmt+" "+num_rows);


                  var datFim = "";
                  if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1>limiteHoras/24){
                    datFim = "";
                  }

                    retornaStatusQuery(num_rows, $scope, datFim);
                    $btn_gerar.button('reset');
                  if(num_rows>0){
                    $btn_exportar.prop("disabled", false);
                  }




                  $scope.dados.push({
                    datReferencia:"",
                    produto: "Total geral",
                    segmento: "",
                    qtdDerivadas: $scope.total_qtdDerivadas,
                    qtdTransferidas: $scope.total_qtdTransferidas,
          /* qtdNEncontradas: $scope.total_qtdNEncontradas,
                    totalGeral: $scope.total_totalGeral */
                  });

                  //if(executaQuery !== executaQuery2){
                    $('.btn.btn-pivot').prop("disabled","false");
                    if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1<=limiteHoras/24 && num_rows>0){
                        $('.btn.btn-pivot').button('reset');
                    }
                  //}



                  /*if($scope.valor === "regra"){
                    // Preencher com zero datas inexistentes em cada erro
            $scope.dados.forEach(function (dado) {
                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = { qtd_entrantes: 0 };
                    }
                })
            });


            $scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
            $scope.datas.forEach(function (d) {
                $scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: $scope.filtros.porDia ? 100 : 88 });
            });

            $scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: $scope.filtros.porDia ? 100 : 88 });
            $scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });
                  }*/





                });
            //});

            // GILBERTOOOOOO 17/03/2014
            $view.on("mouseup", "tr.resumo", function () {
                var that = $(this);
                $('tr.resumo.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });
    };




    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);

        $btn_exportar.button('loading');

      var linhas  = [];


      if($scope.valor === "destino"){

        if($scope.filtros.porDDD){
          linhas.push([{value:'Data', autoWidth:true}, {value:'Skill', autoWidth:true}, {value:'ddd', autoWidth:true},{value:'qtd', autoWidth:true}]);
          $scope.dados.map(function (dado) {
            return linhas.push([{value:dado.datReferencia, autoWidth:true}, {value:dado.codskill, autoWidth:true}, {value:dado.ddd, autoWidth:true}, {value:dado.qtd, autoWidth:true}]);
          });

        }else{
          linhas.push([{value:'Data', autoWidth:true}, {value:'Skill', autoWidth:true}, {value:'qtd', autoWidth:true}]);
          $scope.dados.map(function (dado) {
            return linhas.push([{value:dado.datReferencia, autoWidth:true}, {value:dado.codskill, autoWidth:true}, {value:dado.qtd, autoWidth:true}]);
          });
        }
      }else if($scope.valor === "regra"){



      linhas.push([{value:'Data', autoWidth:true},{value:'Segmento', autoWidth:true}, {value:'Operação', autoWidth:true}, {value:'Qtd Operação 1', autoWidth:true},{value:'Qtd Operação 2', autoWidth:true}]);
          $scope.dados.map(function (dado) {
            return linhas.push([{value:dado.datReferencia, autoWidth:true}, {value:dado.segmento, autoWidth:true},{value:dado.produto, autoWidth:true}, {value:dado.qtdDerivadas, autoWidth:true}, {value:dado.qtdTransferidas, autoWidth:true}]);
          });


      }else{

        var header = empresas;
        header.unshift("datReferencia");

        var h =[];

        for(var i= 0; i<header.length;i++){

          h.push({value: header[i] === "datReferencia" ? "Data" : header[i] , autoWidth:true});
        }
        linhas.push(h);


        $scope.dados.map(function (dado) {
          var l = [];
          for(var i= 0; i<header.length;i++){
            l.push({value:dado[header[i]], autoWidth:true});
          }

          return linhas.push(l);
        });


      }





        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'Resumo Produto x atendimento', data: linhas, table: true }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 1 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoProdutoVSAtend_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}

CtrlResumoContProdXGrupAtend.$inject = ['$scope', '$globals'];
