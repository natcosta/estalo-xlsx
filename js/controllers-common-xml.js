﻿var codigos_xml = {
	TG_APPL: "ap",
	TG_ANI: "an",
	TG_ANIANA: "at",
	TG_APPDATA: "ad",
	TG_ANIREL: "ar",
	TG_BLOQFP: "bf",
	TG_CID: "ci",
	TG_CODREL: "cr",
	TG_CLIANA: "ca",
	TG_CATERM: "ct",
	TG_CADPFJ: "cp",
	TG_CONFIDENCE: "cf",
	TG_DNIS: "dn",
	TG_EXTENSION: "ex",
	TG_END: "en",
	TG_FIELD: "fi",
	TG_HUP: "hu",
	TG_HWID: "hw",
	TG_SITE: "lc",
	TG_ISALTOVALOR: "tv",
	TG_ISOICONTROLE: "cn",
	TG_ISNOVOOICTR: "nr",
	TG_ISCLIENTECORPORATIVO: "co",
	TG_ISCLIENTEEMPRESARIAL: "em",
	TG_ISDIAMANTE: "di",
	TG_ISFONEOI: "fo",
	TG_ISOIJOIA: "jo",
	TG_ISPLATINUM: "ip",
	TG_ISPOSPAGO: "po",
	TG_ISPREPAGO: "pr",
	TG_ISPROPENSAOANATEL: "pa",
	TG_OICONTATOTAL: "oc",
	TG_ANICONTATOTAL: "ot",
	TG_NAME: "na",
	TG_PROMPT: "pt",
	TG_PFJTMP: "c2",
	TG_RESULT: "re",
	TG_SEGMER: "sm",
	TG_STATUS: "su",
	TG_START: "st",
	TG_STATE: "se",
	TG_TERADI: "ta",
	TG_TIPROD: "tp",
	TG_TIPTER: "tt",
	TG_TIPOTRN: "tr",
	TG_TIME: "ti",
	TG_TIPPES: "te",
	TG_TYPE: "ty",
	TG_UCID: "uc",
	TG_UDATA: "ud",
	TG_UNINEG: "un",
	TG_VALUE: "va",
	TG_VERS: "ve",
	TG_VDN: "vd",
	TG_UTTFILE: "uf",
	TG_ANIAV: "aa",
	TG_CARTAOPREDILETO: "p1",
	TG_CONTROLEPREDILETO: "p2",
	TG_CONTABONUS7200: "p3",
	TG_CONTROLEBONUS600: "p4",
	TG_CONTROLEBONUS6000: "p5",
	TG_CONTROLEBONUS400: "p6",
	TG_LIGADOR10: "p7",
	TG_LIGADOR20: "p8",
	TG_PROMOMAES: "p9",
	TG_LIGADORNATAL: "ln",
	TG_OFFPROMO: "of",
	TG_FINALDESTINATION: "fd",
	TG_TEMRECLAMACAOANATEL: "s1",
	TG_SCOREANATEL: "s2",
	TG_SCOREJEC: "s3",
	TG_ISPLANOALTERNATIVO: "s4",
	TG_ITEMCONTROLE: "pc",
	TG_TELTRATADO: "to",
	TG_CODIGORELACIONAMENTOCLIENTE: "s5",
	TG_ISFALHASTC: "s6",
	TG_ISAMCEL: "ac",
	TG_FALHACRM: "fs",
	TG_PROTOCOLOGERAL: "pg",
	TG_STATUSPROTOCOLO: "s7",
	TG_PLANO: "pl",
	TG_ISMANCHA: "mc",
	TG_ISTABELAPREPAGOAVMV: "mv",
	TG_ISTABELAPREPAGOBV: "bv",
	TG_BLOQUEIOCREDITO: "bc",
	TG_ISTABELARECARGA: "rc",
	TG_ISATRASOMAIOR45DIAS: "mm",
	TG_ISTABELAUPGRADE: "ug",
	TG_ISTABELAPAGGO: "gg",
	TG_CONSTABASEPROPENSOANATEL: "ba",
	TG_ISTABELADENTROMAPA: "dm",
	TG_ISTABELAFORAMAPA: "fm",
	TG_DEBITOAUTOMATICO: "da",
	TG_PROMOCAODESFAVORAVEL: "pd",
	TG_URA: "ur",
	TG_PROPENSOVELOX: "pv",
	TG_NOMEVDN: "nv",
	TG_DESTINO: "de",
	TG_TRANSFERID: "td",
	TG_IDBENEFICIO: "be",
	TG_IDPROGRAMA: "pm",
	TG_INADESAOMONURA: "ia",

	TG_INCONSULTAMONURA: "ic",
	TG_INPERFORMANCE: "in",
	TG_INCHAMADACONSULTA: "i1",
	TG_INCHAMADAADESAO: "i2",
	TG_INCHAMADAMIGRACAO: "i3",
	TG_INCHAMADAGARANTIA: "i4",
	TG_PROMO: "np",
	TG_ATRATAR: "ia",
	TG_ISVIP: "iv",
	TG_reparoAberto: "ra",
	//TG_CONSTABASEPROPENSOANATEL: "m4",
	TG_prazoReparoVencido: "rv",
	//TG_CONTROLEBONUS6000: "nd",
	TG_NUMDISCADO: "nd",
	//TG_DESTINO: "nt",
	TG_NUMTRATAR: "nt",
	TG_RETORNOTRANS: "r1",
	TG_OSABERTA: "sa",
	TG_TRANSACAO: "t1",
	TG_PRAZOEVENTOVENCIDO: "t1",
	TG_TIPOTRANSACAO: "uu",
	TG_eventoAberto: "ea",
	//TG_PRAZOEVENTOVENCIDO: "ev",
	//TG_OSABERTA: "oa",
	TG_prazoOSVencido: "ov",
	TG_osAbertaVelox: "ox",
	TG_prazoVeloxVencido: "vv",
	TG_operacao: "op",
	TG_retornoTransacao: "rt",
	//TG_TIPOTRANSACAO: "tn",
	TG_gra_numeroTelefonico: "gr",
	TG_ELEGIVEL: "el",
	APP_M4URECARGACONSULTA: "ml",
	APP_M4URECARGAPERFORMANCE: "mp",
	APP_M4URECARGAREGISTRANOVOCARTAO: "mn",
	APP_M4URECARGAREGISTRARECARGA: "mr",
	APP_M4URECARGACANCELARCADASTRO: "md",
	APP_M4URECARGAREGISTRACLIENTE: "mt",
	APP_NUMEROASSOCIADO: "ny",
	APP_OPERACAO: "ms",
	APP_RECARGAVALOR: "mi",
	APP_RECARGABANDEIRA: "mb",
	APP_VAR_PRODUTO1: "f1",
	APP_VAR_VALOR1: "f2",
	APP_VAR_SUCESSOVENDA1: "f3",
	APP_VAR_CODIGO_ERRO1: "f4",
	APP_VAR_CODIGOOFERTA1: "o1",
	APP_VAR_PRODUTO2: "f5",
	APP_VAR_VALOR2: "f6",
	APP_VAR_SUCESSOVENDA2: "f7",
	APP_VAR_CODIGO_ERRO2: "f8",
	APP_VAR_CODIGOOFERTA2: "o2",
	APP_VAR_PRODUTO3: "g1",
	APP_VAR_VALOR3: "g2",
	APP_VAR_SUCESSOVENDA3: "g3",
	APP_VAR_CODIGO_ERRO3: "g4",
	APP_VAR_CODIGOOFERTA3: "o3",
	APP_VAR_PRODUTO4: "g5",
	APP_VAR_VALOR4: "g6",
	APP_VAR_SUCESSOVENDA4: "g7",
	APP_VAR_CODIGO_ERRO4: "g8",
	APP_VAR_CODIGOOFERTA4: "o4",
	APP_VAR_PRODUTO5: "h1",
	APP_VAR_VALOR5: "h2",
	APP_VAR_SUCESSOVENDA5: "h3",
	APP_VAR_CODIGO_ERRO5: "h4",
	APP_VAR_CODIGOOFERTA5: "o5",
	APP_VAR_PRODUTO6: "h5",
	APP_VAR_VALOR6: "h6",
	APP_VAR_SUCESSOVENDA6: "h7",
	APP_VAR_CODIGO_ERRO6: "h8",
	APP_VAR_CODIGOOFERTA6: "o6",
	APP_WS_SiebelPrincipal: "APP_WS_SiebelPrincipal",
	APP_DB_Whitelist: "APP_DB_Whitelist",
	APP_WS_M4UConsulta: "APP_WS_M4UConsulta",
	APP_WS_InConsulta: "APP_WS_InConsulta",
	APP_DB_Pacotes: "APP_DB_Pacotes", // GILBERTO -- 23/01/2014
	APP_RECVOZ_CONFIANCA: "r3", // Brunno 04/07/2019
	APP_RECVOZ_ELOCUCAO: "r4", // Brunno 04/07/2019
	APP_RECVOZ_RESULT: "r5", // Brunno 04/07/2019
	APP_RECVOZ_SSM_SCORE: "r6" // Brunno 04/07/2019
};


function carregaVariaveisAntigas() {
	var t = codigos_xml;

	cache.variaveis = [{
			codigo: t.TG_ANIREL,
			descricao: "Segmento do chamador"
		},
		{
			codigo: t.TG_ANIAV,
			descricao: "Alto Valor?"
		},
		{
			codigo: t.TG_ISALTOVALOR,
			descricao: "Alto Valor?"
		},
		{
			codigo: t.TG_CODREL,
			descricao: "Segmento"
		},
		{
			codigo: t.TG_CLIANA,
			descricao: "Cliente Anatel"
		},
		{
			codigo: t.TG_TERADI,
			descricao: "Terminal Adicional"
		},
		{
			codigo: t.TG_CATERM,
			descricao: "Categoria do Terminal"
		},
		{
			codigo: t.TG_TIPTER,
			descricao: "Tipo de Terminal"
		},
		{
			codigo: t.TG_TIPPES,
			descricao: "Tipo de Pessoa"
		},
		{
			codigo: t.TG_CADPFJ,
			descricao: "CPF/CNPJ"
		},
		{
			codigo: t.TG_SEGMER,
			descricao: "Segmento de Mercado"
		},
		{
			codigo: t.TG_TIPOTRN,
			descricao: "Transferido via"
		},
		{
			codigo: t.TG_VDN,
			descricao: "VDN"
		},
		{
			codigo: t.TG_UNINEG,
			descricao: "Unidade de negócio"
		},
		{
			codigo: t.TG_ISPLATINUM,
			descricao: "Platinum?"
		},
		{
			codigo: t.TG_ISVIP,
			descricao: "VIP?"
		},
		{
			codigo: t.TG_NUMDISCADO,
			descricao: "Num discado"
		},
		{
			codigo: t.TG_NUMTRATAR,
			descricao: "Num tratado?"
		},
		{
			codigo: t.TG_TRANSACAO,
			descricao: "Transação"
		},
		{
			codigo: t.TG_RETORNOTRANS,
			descricao: "Retorno transação"
		},
		{
			codigo: t.TG_TIPOTRANSACAO,
			descricao: "UUI"
		},
		{
			codigo: t.TG_BLOQFP,
			descricao: "Bloqueio por falta de pagamento"
		},
		{
			codigo: t.TG_STATUS,
			descricao: "Status"
		},
		{
			codigo: t.TG_ISALTOVALOR,
			descricao: "Alto Valor"
		},
		{
			codigo: t.TG_ISOICONTROLE,
			descricao: "Oi Controle"
		},
		{
			codigo: t.TG_ISCLIENTECORPORATIVO,
			descricao: "Cliente Corp."
		},
		{
			codigo: t.TG_ISCLIENTEEMPRESARIAL,
			descricao: "Cliente Empresarial"
		},
		{
			codigo: t.TG_ISDIAMANTE,
			descricao: "Diamante"
		},
		{
			codigo: t.TG_ISFONEOI,
			descricao: "Fone Oi"
		},
		{
			codigo: t.TG_ISOIJOIA,
			descricao: "Oi Jóia"
		},
		{
			codigo: t.TG_ISPOSPAGO,
			descricao: "Pós Pago"
		},
		{
			codigo: t.TG_ELEGIVEL,
			descricao: "Elegível nova promoção?"
		},
		{
			codigo: t.TG_ISPREPAGO,
			descricao: "Pré Pago"
		},
		{
			codigo: t.TG_ISAMCEL,
			descricao: "Amaz.Cel"
		},
		{
			codigo: t.TG_FALHACRM,
			descricao: "Status CRM"
		},
		{
			codigo: t.TG_ISPROPENSAOANATEL,
			descricao: "Propenso a Anatel"
		},
		{
			codigo: t.TG_OICONTATOTAL,
			descricao: "Conta Total"
		},
		{
			codigo: t.TG_ANICONTATOTAL,
			descricao: "Conta Total"
		},
		{
			codigo: t.TG_CARTAOPREDILETO,
			descricao: "Cartão Predileto"
		},
		{
			codigo: t.TG_CONTROLEPREDILETO,
			descricao: "Controle Predileto"
		},
		{
			codigo: t.TG_CONTABONUS7200,
			descricao: "Conta Bonus 7200"
		},
		{
			codigo: t.TG_CONTROLEBONUS600,
			descricao: "Controle Bonus 600"
		},
		{
			codigo: t.TG_CONTROLEBONUS6000,
			descricao: "Controle Bonus 6000"
		},
		{
			codigo: t.TG_CONTROLEBONUS400,
			descricao: "Controle Bonus 400"
		},
		{
			codigo: t.TG_LIGADOR10,
			descricao: "Ligador 10"
		},
		{
			codigo: t.TG_LIGADOR20,
			descricao: "Ligador 20"
		},
		{
			codigo: t.TG_LIGADORNATAL,
			descricao: "Ligador Natal"
		},
		{
			codigo: t.TG_OFFPROMO,
			descricao: "Ligador OffPromo"
		},
		{
			codigo: t.TG_PROMOMAES,
			descricao: "Ligador Mães"
		},
		{
			codigo: t.TG_FINALDESTINATION,
			descricao: "Destino"
		},
		{
			codigo: t.TG_TEMRECLAMACAOANATEL,
			descricao: "Reclamação Anatel"
		},
		{
			codigo: t.TG_SCOREANATEL,
			descricao: "Score Anatel"
		},
		{
			codigo: t.TG_SCOREJEC,
			descricao: "Score JEC"
		},
		{
			codigo: t.TG_ISPLANOALTERNATIVO,
			descricao: "Plano Alternativo"
		},
		{
			codigo: t.TG_ITEMCONTROLE,
			descricao: "Item de Controle"
		},
		{
			codigo: t.TG_TELTRATADO,
			descricao: "Tel Tratado"
		},
		{
			codigo: t.TG_CODIGORELACIONAMENTOCLIENTE,
			descricao: "Codigo Relacionamento"
		},
		{
			codigo: t.TG_ISFALHASTC,
			descricao: "Falha STC"
		},
		{
			codigo: t.TG_TIPROD,
			descricao: "Velox"
		},
		{
			codigo: t.TG_PROTOCOLOGERAL,
			descricao: "Protocolo Geral"
		},
		{
			codigo: t.TG_STATUSPROTOCOLO,
			descricao: "Status Protocolo"
		},
		{
			codigo: t.TG_ISNOVOOICTR,
			descricao: "Novo OiControle"
		},
		{
			codigo: t.TG_ISMANCHA,
			descricao: "Mancha"
		},
		{
			codigo: t.TG_PLANO,
			descricao: "Plano"
		},
		{
			codigo: t.TG_ISTABELAPREPAGOAVMV,
			descricao: "Tabela Pre-AV/MV"
		},
		{
			codigo: t.TG_ISTABELAPREPAGOBV,
			descricao: "Tabela Pre-BV"
		},
		{
			codigo: t.TG_BLOQUEIOCREDITO,
			descricao: "Bloqueio Crédito"
		},
		{
			codigo: t.TG_ISTABELARECARGA,
			descricao: "Tabela Recarga"
		},
		{
			codigo: t.TG_ISATRASOMAIOR45DIAS,
			descricao: "Atraso > 45d"
		},
		{
			codigo: t.TG_ISTABELAUPGRADE,
			descricao: "Tabela Upgrade"
		},
		{
			codigo: t.TG_ISTABELAPAGGO,
			descricao: "Tabela Paggo"
		},
		{
			codigo: t.TG_CONSTABASEPROPENSOANATEL,
			descricao: "Base Propenso Anatel"
		},
		{
			codigo: t.TG_ISTABELADENTROMAPA,
			descricao: "Tabela Dentro Mapa"
		},
		{
			codigo: t.TG_ISTABELAFORAMAPA,
			descricao: "Tabela Fora Mapa"
		},
		{
			codigo: t.TG_DEBITOAUTOMATICO,
			descricao: "Débito Automático"
		},
		{
			codigo: t.TG_PROMOCAODESFAVORAVEL,
			descricao: "Promo Desfavoravel"
		},
		{
			codigo: t.TG_URA,
			descricao: "URA"
		},
		{
			codigo: t.TG_PROPENSOVELOX,
			descricao: "Propenso Velox"
		},
		{
			codigo: t.TG_NOMEVDN,
			descricao: "Nome VDN"
		},
		{
			codigo: t.TG_DESTINO,
			descricao: "Destino"
		},
		{
			codigo: t.TG_TRANSFERID,
			descricao: "Transfer ID"
		},
		{
			codigo: t.APP_NUMEROASSOCIADO,
			descricao: "Número Associado"
		},
		{
			codigo: t.APP_M4URECARGACONSULTA,
			descricao: "Recarga Consulta"
		},
		{
			codigo: t.APP_M4URECARGACANCELARCADASTRO,
			descricao: "Cancela Cadastro"
		},
		{
			codigo: t.APP_M4URECARGAPERFORMANCE,
			descricao: "Performance M4U"
		},
		{
			codigo: t.APP_M4URECARGAREGISTRACLIENTE,
			descricao: "Registra Cliente"
		},
		{
			codigo: t.APP_M4URECARGAREGISTRANOVOCARTAO,
			descricao: "Registra Novo Cartão"
		},
		{
			codigo: t.APP_M4URECARGAREGISTRARECARGA,
			descricao: "Recarga no Cartão"
		},
		{
			codigo: t.APP_RECARGAVALOR,
			descricao: "Valor da Recarga"
		},
		{
			codigo: t.APP_RECARGABANDEIRA,
			descricao: "Bandeira"
		},
		{
			codigo: t.APP_OPERACAO,
			descricao: "Operação"
		},
		{
			codigo: t.TG_IDBENEFICIO,
			descricao: "Benefício"
		},
		{
			codigo: t.TG_IDPROGRAMA,
			descricao: "ID do Programa"
		},
		{
			codigo: t.TG_INADESAOMONURA,
			descricao: "Adesão via IN"
		},
		{
			codigo: t.TG_INCONSULTAMONURA,
			descricao: "Consulta à IN"
		},
		{
			codigo: t.TG_INPERFORMANCE,
			descricao: "Performance IN"
		},
		{
			codigo: t.TG_INCHAMADACONSULTA,
			descricao: "Chamada Consulta?"
		},
		{
			codigo: t.TG_INCHAMADAADESAO,
			descricao: "Chamada Adesão?"
		},
		{
			codigo: t.TG_INCHAMADAMIGRACAO,
			descricao: "Chamada Migração?"
		},
		{
			codigo: t.TG_INCHAMADAGARANTIA,
			descricao: "Chamada Garantia?"
		},
		{
			codigo: t.TG_PROMO,
			descricao: "Promoção Vigente?"
		},
		{
			codigo: t.TG_reparoAberto,
			descricao: "Reparo Aberto?"
		},
		{
			codigo: t.TG_prazoReparoVencido,
			descricao: "Prazo Reparo Vencido?"
		},
		{
			codigo: t.TG_eventoAberto,
			descricao: "Evento Aberto?"
		},
		{
			codigo: t.TG_PRAZOEVENTOVENCIDO,
			descricao: "Prazo Evento Vencido?"
		},
		{
			codigo: t.TG_OSABERTA,
			descricao: "OS Aberta?"
		},
		{
			codigo: t.TG_prazoOSVencido,
			descricao: "Prazo OS Vencido?"
		},
		{
			codigo: t.TG_osAbertaVelox,
			descricao: "OS Aberta Velox?"
		},
		{
			codigo: t.TG_prazoVeloxVencido,
			descricao: "Prazo Velox Vencido?"
		},
		{
			codigo: t.TG_operacao,
			descricao: "Operação"
		},
		{
			codigo: t.TG_retornoTransacao,
			descricao: "Retorno Transação"
		},
		{
			codigo: t.TG_TIPOTRANSACAO,
			descricao: "Tipo de Transação"
		},
		{
			codigo: t.TG_gra_numeroTelefonico,
			descricao: "GRA"
		},
		{
			codigo: t.APP_VAR_PRODUTO1,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR1,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA1,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO1,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA1,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_VAR_PRODUTO2,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR2,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA2,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO2,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA2,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_VAR_PRODUTO3,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR3,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA3,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO3,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA3,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_VAR_PRODUTO4,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR4,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA4,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO4,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA4,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_VAR_PRODUTO5,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR5,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA5,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO5,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA5,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_VAR_PRODUTO6,
			descricao: "Produto"
		},
		{
			codigo: t.APP_VAR_VALOR6,
			descricao: "Valor"
		},
		{
			codigo: t.APP_VAR_SUCESSOVENDA6,
			descricao: "Sucesso"
		},
		{
			codigo: t.APP_VAR_CODIGO_ERRO6,
			descricao: "Erro"
		},
		{
			codigo: t.APP_VAR_CODIGOOFERTA6,
			descricao: "Oferta"
		},
		{
			codigo: t.APP_WS_SiebelPrincipal,
			descricao: "Consulta ao Siebel"
		},
		{
			codigo: t.APP_DB_Whitelist,
			descricao: "Consulta à Whitelist"
		},
		{
			codigo: t.APP_WS_M4UConsulta,
			descricao: "Consulta à M4U"
		},
		{
			codigo: t.APP_WS_InConsulta,
			descricao: "Consulta à IN"
		},
		{
			codigo: t.APP_DB_Pacotes,
			descricao: "Consulta aos Pacotes"
		},		
	];
	cache.variaveis.indice = geraIndice(cache.variaveis);
}

carregaVariaveisAntigas();

var sufixosVenda = [
	[codigos_xml.APP_VAR_PRODUTO1, codigos_xml.APP_VAR_VALOR1, codigos_xml.APP_VAR_SUCESSOVENDA1, codigos_xml.APP_VAR_CODIGO_ERRO1, codigos_xml.APP_VAR_CODIGOOFERTA1],
	[codigos_xml.APP_VAR_PRODUTO2, codigos_xml.APP_VAR_VALOR2, codigos_xml.APP_VAR_SUCESSOVENDA2, codigos_xml.APP_VAR_CODIGO_ERRO2, codigos_xml.APP_VAR_CODIGOOFERTA2],
	[codigos_xml.APP_VAR_PRODUTO3, codigos_xml.APP_VAR_VALOR3, codigos_xml.APP_VAR_SUCESSOVENDA3, codigos_xml.APP_VAR_CODIGO_ERRO3, codigos_xml.APP_VAR_CODIGOOFERTA3],
	[codigos_xml.APP_VAR_PRODUTO4, codigos_xml.APP_VAR_VALOR4, codigos_xml.APP_VAR_SUCESSOVENDA4, codigos_xml.APP_VAR_CODIGO_ERRO4, codigos_xml.APP_VAR_CODIGOOFERTA4],
	[codigos_xml.APP_VAR_PRODUTO5, codigos_xml.APP_VAR_VALOR5, codigos_xml.APP_VAR_SUCESSOVENDA5, codigos_xml.APP_VAR_CODIGO_ERRO5, codigos_xml.APP_VAR_CODIGOOFERTA5],
	[codigos_xml.APP_VAR_PRODUTO6, codigos_xml.APP_VAR_VALOR6, codigos_xml.APP_VAR_SUCESSOVENDA6, codigos_xml.APP_VAR_CODIGO_ERRO6, codigos_xml.APP_VAR_CODIGOOFERTA6]
];
sufixosVenda.forEach(function (variaveis, i) {
	variaveis.forEach(function (variavel) {
		sufixosVenda[variavel] = i + 1;
	});
});

function obtemSufixoVenda(variavel) {
	return sufixosVenda[variavel];
}

function parseXML(xml, chamada, ic, icAll, numCPFCNPJ,routing,routingRet) {
	try {
		var xml_doc = $.parseXML(xml),
			$xml = $(xml_doc);
	} catch (e) {
		throw e;
	}
	var sTipoTagXML,
		ultimo_estado = "",
		tel_chamador = "";
	var call_log = [];
	call_log.prompts_iniciais = [];
	call_log.info_finais = [];
	var tags_top = {
		TAG_URA: {
			codigo: "hostname",
			titulo: "Hostname URA"
		},
		TAG_HWID: {
			codigo: "hardware",
			titulo: "Hardware",
			hook: function (valor) {
				FalaAppServer = valor;
			}
		},
		TAG_START: {
			codigo: "inicio",
			titulo: "Início"
		},
		TAG_END: {
			codigo: "fim",
			titulo: "Fim"
		},
		TAG_ANI: {
			codigo: "chamador",
			titulo: "Chamador",
			hook: function (valor) {
				tel_chamador = valor;
			}
		},
		TAG_DNIS: {
			codigo: "vdn_in",
			titulo: "VDN de Entrada"
		},
		TAG_SITE: {
			codigo: "vdn_in",
			titulo: "VDN de Entrada"
		}
	};
	var f = function (d, elem) {
		var info = {
			titulo: d.titulo,
			valor: elem.text()
		};
		call_log[d.codigo] = info;
		call_log.push(info);
		if (d.hook) d.hook(elem.text());
	};
	// Brunno 18/02/2019 coloca o CPF/CNPJ
	if(numCPFCNPJ === null || numCPFCNPJ === "" ) numCPFCNPJ = "ND"
	// console.log("CPF2:", numCPFCNPJ)
	var inicio;
	try {
		//Gambiarra master senior de Alex e Bernardo(mas funciona)
		var st = $xml.find("VDATA st");
		st.length > 0 ? st = st[0].firstChild : st.nodeValue = "";
		var ur = $xml.find("VDATA ur");
		ur.length > 0 ? ur = ur[0].firstChild : ur.nodeValue = "";
		var an = $xml.find("VDATA an");
		an.length > 0 ? an = an[0].firstChild : an.nodeValue = "";
		var dn = $xml.find("VDATA dn");
		dn.length > 0 ? dn = dn[0].firstChild : dn.nodeValue = "";
		var ap = $xml.find("VDATA ap");
		ap.length > 0 ? ap = ap[0].firstChild : ap.nodeValue = "";
		var lc = $xml.find("VDATA lc");
		lc.length > 0 ? lc = lc[0].firstChild : lc.nodeValue = "";
		var hw = $xml.find("VDATA hw");
		hw.length > 0 ? hw = hw[0].firstChild : hw.nodeValue = "";
		
			
		call_log.infos_iniciais = [{
				nome: "Início",
				valor: st.nodeValue
			},
			{
				nome: "Chamador",
				valor: an.nodeValue
			},
			{
				nome: "CPF/CNPJ",
				valor: numCPFCNPJ,
			},
			{
				nome: "URA",
				valor: ur.nodeValue
			},
			{
				nome: "VDN",
				valor: dn.nodeValue
			},
			{
				nome: "Aplicação",
				valor: ap.nodeValue
			},
			{
				nome: "Site",
				valor: lc.nodeValue
			},
			{
				nome: "Serv Apl",
				valor: hw.nodeValue
			}
		];


		call_log.info_finais = [
			{
				nome: "Empresa",
				valor: (routing+routingRet).match(/a0:"(.*?)"/) !== null ? (routing+routingRet).match(/a0:"(.*?)"/)[1] :'ND'
			}
			,
			{
				nome: "Serviço destino",
				valor: (routing+routingRet).match(/a4:"(.*?)"/) !== null ? (routing+routingRet).match(/a4:"(.*?)"/)[1] : ((routing+routingRet).match(/dt:"(.*?)"/) !== null ? (routing+routingRet).match(/dt:"(.*?)"/)[1] : 'ND')
			}
			,
			{
				nome: "Regra",
				valor: (routing+routingRet).match(/aa:"(.*?)"/) !== null ? (routing+routingRet).match(/aa:"(.*?)"/)[1] : 'ND'
			}
		];
		call_log.info_finais.icone = "icon-cog";
		call_log.info_finais.derivada = chamada.encerramento === "Derivada" ? true : false;

	} catch (ex) {
		console.log(ex.message);
		//call_log.infos_iniciais = [];
		call_log.info_finais = [];
	}
	$xml.find("VDATA >pt").each(function () {
		var $pt = $(this);
		// Inserir prompt concatenado
		var info = {
			codigo: "prompt",
			titulo: "Prompt",
			icone: "",
			valor: [],
			codAplicacao: chamada.cod_aplicacao,
			codPrompt: ""
		};
		$pt.text().split(",").forEach(function (cod_prompt) {
			if (cod_prompt === "empty") return;
			var texto = obtemTextoPrompt(chamada.cod_aplicacao, cod_prompt);
			info.valor.push(texto);
			info.codPrompt = cod_prompt;
		});
		call_log.prompts_iniciais.push(info);
	});
	$xml.find("VDATA se").each(function () {
		var $node = $(this);
		var info = {
			codigo: "estado-dialogo",
			titulo: "Estado de Diálogo",
			valor: $node.attr("na"),
			prompts: [],
			filhos: []
		};
		var ts = $node.attr("ti");
		ts = new Date(2001, 0, 1, +ts.substr(0, 2), +ts.substr(3, 2), +ts.substr(6, 2));
		if (inicio === undefined) {
			inicio = ts;
			//info.inicio = "0:00";
			info.inicio = "0s";
		} else {
			var diff = new Date(ts.getTime() - inicio.getTime()),
				min = diff.getMinutes(),
				seg = diff.getSeconds();
			//info.inicio = min + ":" + _02d(seg);
			info.inicio = min === 0 ? seg + "s" : min + "min" + _02d(seg) + "s";
		}
		var prompt_concatenado;
		var substituir = [];
		var proximo_a_substituir = 0;
		//var codPromo = "";
		var infos_venda = [];
		$node.children().each(function () {
			var $filho = $(this);
			// Inserir prompt concatenado
			if (!$filho.is("pt") && prompt_concatenado !== undefined) {
				//prompt_concatenado.valor = prompt_concatenado.valor.join(" ");
				//info.filhos.push(prompt_concatenado);
				prompt_concatenado = undefined;
			}
			if ($filho.is("pt")) {
				if (chamada.cod_aplicacao.substring(0, 4) === "USSD") {
					$filho.text().split(",").forEach(function (cod_prompt) {
						var texto = obtemTextoPrompt(chamada.cod_aplicacao, cod_prompt);
						var prompt = {
							codigo: "prompt",
							titulo: "Prompt",
							icone: "",
							valor: texto,
							codAplicacao: chamada.cod_aplicacao,
							codPrompt: cod_prompt
						};
						var formato_cartao = texto.match('[0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9]');
						if (texto.match(/<[^>]+>/)) {
							substituir.push({
								prompt: prompt,
								tipo: "<>"
							});
						} else if (texto.match(/ xxxx$/)) {
							substituir.push({
								prompt: prompt,
								tipo: "x"
							});
						} else if (formato_cartao) {
							//substituir.push({ prompt: prompt, tipo: "x" });
							//var formato = texto.match('[0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9]');
							texto = texto.replace(formato_cartao, 'XXXX-XXXX-XXXX-XXXX');
							prompt.valor = texto;
						}
						info.filhos.push(prompt);
					});
				} else {
					prompt_concatenado = {
						codigo: "prompt",
						titulo: "Prompt",
						icone: "",
						valor: [],
						codAplicacao: chamada.cod_aplicacao,
						codPrompt: ""
					};
					$filho.text().split(",").forEach(function (cod_prompt, index, arr) {
						if (cod_prompt === "empty") return;
						prompt_concatenado.codPrompt += cod_prompt + ',';
						var texto = obtemTextoPrompt(chamada.cod_aplicacao, cod_prompt);
						if (chamada.cod_aplicacao === "MENUOI") {
							if (texto.match(/<[^>]+>/)) {} else {
								prompt_concatenado.valor.push(texto);
							}
						} else {
							if (texto.match(/<[^>]+>/)) {
								texto = texto.replace(/<[^>]+>/, '');
							}
							prompt_concatenado.valor.push(texto);
						}
					});
					prompt_concatenado.codPrompt = prompt_concatenado.codPrompt.slice(0, -1);
					//Alex 04/02/2014
					italico = "<i>";
					prompt_concatenado.valor = italico.concat(prompt_concatenado.valor, "</i>&nbsp;");
					info.filhos.push(prompt_concatenado);
				}
			} else if ($filho.is("p2")) {
				//Alex 02/02/2016 Tratando cartão
				var filhoTexto = $filho.text().split(";");
				if ($filho.text().match("[0-9]+[;][0-9]+[;][0-9]+[,][0-9]+[;][0-9]+[;][0-9][0-9][0-9][0-9]") ||
					$filho.text().match("[0-9]+[;][0-9]+[,][0-9]+[;][0-9]+[;][0-9][0-9][0-9][0-9]") ||
					$filho.text().match("[0-9]+[,][0-9]+[;][0-9]+[;][0-9][0-9][0-9][0-9]") ||
					$filho.text().match("[0-9]+[.][0-9]+[;][0-9][0-9][0-9][0-9]")) {
					filhoTexto.moveArrayUar(filhoTexto.length - 1, filhoTexto.length - 2);
				}
				filhoTexto.forEach(function (v) {
					try {
						//var prompt = substituir.pop();
						var s = substituir[proximo_a_substituir];
						s.prompt.valor = s.prompt.valor.replace(s.tipo === "<>" ? /<[^>]+>/ : /xxxx$/, v);
						if (!s.prompt.valor.match(/<[^>]+>/) && !s.prompt.valor.match(/ xxxx$/) && !s.prompt.valor.match('[0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9][ ][-][ ][0-9][0-9][0-9][0-9]')) proximo_a_substituir++;
					} catch (e) {
						console.log(e)
					}
				});
			} else if ($filho.is("p3")) {
				var texto = $filho.text();
				var prompt = {
					codigo: "prompt",
					titulo: "Prompt",
					icone: "",
					valor: texto,
					codAplicacao: chamada.cod_aplicacao,
					codPrompt: cod_prompt
				};
				info.filhos.push(prompt);
			} else if ($filho.is("ud")) {
				
				var resposta = validaCartao($filho.find("re").text(),true);		
				if(info.valor === "CadastroCartao_SB_03") resposta = "XXX";
				if(info.valor === "DtValidadeCartao_SB_02") resposta = "XXXX";
				resposta = resposta.replace("(", " (");
				//Alex 06/02/2014
				negrito = "<b>";
				resposta = negrito.concat(resposta, "</b>&nbsp;");
				info.filhos.push({
					codigo: "resposta",
					titulo: "Resposta",
					icone: "",
					valor: "Resposta: " + resposta
				});
				// Obtém as Variáveis
			} else if ($filho.is("ad")) {
				var variavel;
				
				// Variáveis Rec Voz
					var exibir = false;	
					var r3 = "";
					var r3v = "";
					var r4 = "";
					var r4v = "";
					var r5 = "";
					var r5v = "";
					var r6 = "";
					var r6v = "";
				// Fim
				$filho.children().each(function () {
					var $neto = $(this);
					if ($neto.is("ty")) {
						var textoNeto = $neto.text();
						variavel = obtemVariavel(chamada.cod_aplicacao, textoNeto);
					} else if ($neto.is("va")) {
						var valor = $neto.text();
						// variavel = obtemVariavel(chamada.cod_aplicacao, textoNeto);
						// Buscar ITEM DE CONTROLE
						if (variavel != undefined) {
							if (variavel.codigo === codigos_xml.TG_ITEMCONTROLE) {
								var cod_item = valor;
								//var descricao = obtemDescricaoItemControle(chamada.cod_aplicacao, cod_item);
								//var complemento = "";
								//if (cod_item == "ICDV003" || cod_item == "ICDV004")
								//complemento = chamada.reconhecimentoVoz;
								//var descricao = obtemDescricaoItemControleOnDemand(chamada.cod_aplicacao, cod_item);
								var descricao = obtemDescricaoItemControle(chamada.cod_aplicacao, cod_item);
								if (ic === true && icAll === false) {
									//? = No, ? = Não, ?=no, ?=Não, ?=não,? = não.
									if (descricao.substring(descricao.length - 3, descricao.length) !== "(N)"
									&& descricao.substring(descricao.length - 3, descricao.length).toUpperCase().trim() !== "NAO"
									&& descricao.substring(descricao.length - 3, descricao.length).toUpperCase().trim() !== "NÃO"
									&& descricao.substring(descricao.length - 3, descricao.length).toUpperCase().trim() !== "NO"
									&& descricao.substring(descricao.length - 3, descricao.length) !== "" 
									&& descricao.toUpperCase().match("FALSE") !== "FALSE") {
										info.filhos.push({
											codigo: "item-de-controle",
											titulo: "Item de Controle",
											icone: "icon-flag",
											valor: (descricao === cod_item ? 'IC NÃO CADASTRADO <em>[' + cod_item + ']</em>' : descricao + ' <em>[' + cod_item + ']</em>')
										});
									}
								} else if (ic === false && icAll === true) {
									//Não inclui nenhum IC
								} else if (ic === false && icAll === false) {
									info.filhos.push({
										codigo: "item-de-controle",
										titulo: "Item de Controle",
										icone: "icon-flag",
										valor: (descricao === cod_item ? 'IC NÃO CADASTRADO <em>[' + cod_item + ']</em>' : descricao + ' <em>[' + cod_item + ']</em>')
									});
									/* if (complemento != "")
									info.filhos.push({
									codigo: "item-de-controle",
									titulo: "Item de Controle",
									icone: "icon-volume-down",
									valor: complemento
									}); */
								}
							} else if (variavel.codigo === codigos_xml.APP_VAR_PRODUTO1 ||
								variavel.codigo === codigos_xml.APP_VAR_PRODUTO2 ||
								variavel.codigo === codigos_xml.APP_VAR_PRODUTO3 ||
								variavel.codigo === codigos_xml.APP_VAR_PRODUTO4 ||
								variavel.codigo === codigos_xml.APP_VAR_PRODUTO5 ||
								variavel.codigo === codigos_xml.APP_VAR_PRODUTO6) {
								var sufixo = obtemSufixoVenda(variavel.codigo);
								var info_venda = {
									codigo: "venda",
									titulo: "Venda",
									icone: "icon-shopping-cart",
									venda: {}
								};
								infos_venda.push(info_venda);
								infos_venda["_" + sufixo] = info_venda;
								/*if (info_venda === undefined || info_venda.sufixo !== sufixo) {
								info_venda = {
								codigo: "venda",
								titulo: "Venda",
								icone: "icon-shopping-cart",
								venda: {}
								};
								infos_venda.push(info_venda);
								}*/
								info_venda.venda.cod_produto = +valor;
								info.filhos.push(info_venda);
							} else if (variavel.codigo === codigos_xml.APP_VAR_VALOR1 ||
								variavel.codigo === codigos_xml.APP_VAR_VALOR2 ||
								variavel.codigo === codigos_xml.APP_VAR_VALOR3 ||
								variavel.codigo === codigos_xml.APP_VAR_VALOR4 ||
								variavel.codigo === codigos_xml.APP_VAR_VALOR5 ||
								variavel.codigo === codigos_xml.APP_VAR_VALOR6) {
								var sufixo = obtemSufixoVenda(variavel.codigo);
								var info_venda = infos_venda["_" + sufixo] !== undefined ? infos_venda["_" + sufixo] : {
									codigo: "venda",
									titulo: "Venda",
									icone: "icon-shopping-cart",
									venda: {
										cod_produto: 0
									}
								};
								info_venda.venda.valor = +valor.replace(",", ".");
							} else if (variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA1 ||
								variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA2 ||
								variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA3 ||
								variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA4 ||
								variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA5 ||
								variavel.codigo === codigos_xml.APP_VAR_SUCESSOVENDA6) {
								var sufixo = obtemSufixoVenda(variavel.codigo);
								var info_venda = infos_venda["_" + sufixo];
								if (info_venda === undefined) {
									var info_venda = {
										codigo: "venda",
										titulo: "Venda",
										icone: "icon-shopping-cart",
										venda: {}
									};
									infos_venda.push(info_venda);
									infos_venda["_" + sufixo] = info_venda;
									info_venda.venda.cod_produto = "Indeterminado";
									if (info_venda.venda.valor === undefined) {
										info_venda.venda.valor = "Indeterminado";
									}
									info.filhos.push(info_venda);
								}
								info_venda.venda.status = +valor;
							} else if (variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO1 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO2 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO3 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO4 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO5 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGO_ERRO6) {
								var sufixo = obtemSufixoVenda(variavel.codigo);
								var info_venda = infos_venda["_" + sufixo];
								if (info_venda !== undefined) info_venda.venda.erro = valor;
							} else if (variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA1 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA2 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA3 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA4 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA5 ||
								variavel.codigo === codigos_xml.APP_VAR_CODIGOOFERTA6) {
								var sufixo = obtemSufixoVenda(variavel.codigo);
								var info_venda = infos_venda["_" + sufixo];
								if (info_venda !== undefined) info_venda.venda.oferta = valor;
							} // Rec Voz
							else if(variavel.codigo === "r3" || variavel.codigo === "r4" || variavel.codigo === "r5"|| variavel.codigo === "r6"){
								if(r3 === "") {
									r3 = variavel.descricao 
									r3v = valor
								}
								else if (r4 === ""){
									r4 = variavel.descricao 
									r4v = valor
								}
								else if (r5 === ""){
									r5 = variavel.descricao 
									r5v = valor
								}
								else if (r6 === ""){
									r6 = variavel.descricao 
									r6v = valor
								}
								if(r3 !== "" && r4 !== "" && r5 !== ""&& r6 !== "") exibir = true
								if(exibir === true){
								var info_var = {
									codigo: variavel.codigo,
									descricao: variavel.descricao,
									icone: "icon-user",
									valor: r4 +": "+r4v + " ("+r3+": "+r3v+";"+" "+r5+": "+r5v+";"+" "+r6+": "+r6v+")"
								};
								info.filhos.push(info_var);
							}
							}
							else if (variavel.codigo) {
								// se o tipo da variável estiver contido no array de tipos( em memória ), então
								// montar o objeto info_var com a descrição e o valor
								// GILBERTO 28/01/2014
								var iconeChange = "icon-cog";
								if (variavel.nome !== undefined) {
									variavel.nome.substring(0, variavel.nome.length - 1) === "APP_VAR_INCENTIVO" || variavel.nome.substring(0, variavel.nome.length - 1) === "APP_VAR_SUCESSOINCENTIVO" ? iconeChange = "icon-thumbs-up" : iconeChange = "icon-cog";
								}
								var info_var = {
									codigo: variavel.codigo,
									descricao: variavel.descricao,
									icone: iconeChange,
									valor: (variavel.descricao) + ": " + valor
								};
								if (!valor.match(/^\d{10,11}?$/) && !valor.match(/^\d{2}\/\d{2}\/\d{4}$/)) {
									//buscaValorVariavel(variavel.nome, valor, (function (info_var) {
									//    return function (valor) {
									//        info_var.valor += valor;
									//    };
									//})(info_var));
									//console.log("valoresVar");
									info_var.valor = (variavel.descricao) + ": " + valor;
								}
								
								if (valor.match(/.*_(\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9]).*([0-9]|[0-5][0-9]))/)){
									var inicio = new Date(BrDataToDefault2(call_log.infos_iniciais[0].valor));
									var fim = new Date(valor.match(/.*_(\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9]).*([0-9]|[0-5][0-9]))/)[1]);
									var dif = new Date(fim.getTime() - inicio.getTime());
									var minu = dif.getMinutes();
									var segu = dif.getSeconds();						
							        info_var.valor = (variavel.descricao) + ": " +(minu === 0 ? segu + "s" : minu + "min" + _02d(segu) + "s");
								}
								
								info.filhos.push(info_var);
							} else {
								var info_var = {
									codigo: "variavel",
									titulo: "Variável",
									icone: "icon-cog",
									valor: (variavel.descricao || variavel.nome) + ": "
								};
								if (valor === "") {
									info_var.valor += "(Em branco)";
								} else if (valor === "true") {
									info_var.valor += "Sim";
								} else if (valor === "false") {
									info_var.valor += "Não";
								} else if (valor === "null") {
									info_var.valor += "Nulo";
								} else if (!valor.match(/^\d{10,11}?$/) && !valor.match(/^\d{2}\/\d{2}\/\d{4}$/)) {
									//buscaValorVariavel(variavel.nome, valor, (function (info_var) {
									//    return function (valor) {
									//        info_var.valor += valor;
									//    };
									//})(info_var));
									//console.log("valoresVar2");
									info_var.valor = (variavel.descricao) + ": " + valor;
								}
								info.filhos.push(info_var);
							}
						//} else {
						//	var re = new RegExp('^[0-9][0-9][.][0-9][0-9]?$');
						//	if (re.test(valor)) codPromo = valor;
						}
						variavel = undefined;
					}
				});
			} else {
				var info_var = {
					codigo: "variavel",
					titulo: "Variável",
					icone: "icon-time",
					valor: ""
				};
				var a = cache.variaveis2;
				for (var i = 0; i < a.length; i++) {
					if ($filho.is(a[i].codigo)) {
						console.log(a[i].codigo);
						info_var.valor = a[i].nome + ": " + $filho.text();
						info.filhos.push(info_var);
						break;
					}
				}
			}
		});
		infos_venda.forEach(function (info_venda) {
			var venda = info_venda.venda;
			var descricao = "";
			if (venda.cod_produto === "Indeterminado") {
				descricao += "Produto indeterminado";
			} else {
				descricao += obtemNomeProdutoVenda(venda.cod_produto);
			}
			if (venda.cod_produto === "Indeterminado") {
				descricao += " com valor indeterminado";
			} else {
				descricao += " a R$" + (venda.valor !== undefined ? toFixed(venda.valor, 2) : "")
			}
			if (venda.status === 0) {
				var promocao = venda.oferta !== undefined ? ", PROMOÇÃO:" + venda.oferta : '';
				descricao += ' (Status: Sucesso' + promocao + ')';
			} else if (venda.status === 1) {
				var _erro = venda.erro !== undefined ? ' <em>[' + venda.erro + ']</em>' : '';
				descricao += ' (Status: Erro' + _erro + ')';
			}
			info_venda.valor = descricao;
		});
		var excecoes = ["SubDesligaClintesBTCC"];
		if (info.valor.indexOf(excecoes) < 0) call_log.push(info);
	});
	killThreads();
	return call_log;
}

function RemoverEDsExcedentes(Texto) {
	var Estados = [];
	var i = 0;
	var j = 0;
	var k = 0;
	var l = 0;
	var InicioED = 0;
	var primeiro = 0;
	var fimUltimoED = 0;
	var primeiroED = 0;
	var TextoLimpo = "";
	var TextoCompacto = "";
	var TextoEnxuto = "";
	var SegmentoCapado = "";
	var nMax = 0;
	var nPrompts = 0;
	var textowork = "";
	var SegmentoED = "";
	var PosPrompt = 0;
	var PosDigitacao = 0;
	var PosDigEDAnt = -1;
	var NaoEhPrimeiroED = 0;
	var nomeED = "";
	var nomeEDAnt = "";
	TextoLimpo = Texto;
	nMax = TextoLimpo.length;
	//alert (Texto);
	fimUltimoED = 0;
	primeiro = 0;
	EhPrimeiroED = 1;
	for (i = 0; i <= nMax; i++) {
		// procura o início do estado de dialogo
		if (TextoLimpo.slice(i, i + 7) === "<se na=") {
			if (primeiro === 0) {
				// pega tudo que vem antes do primeiro estado de dialogo
				TextoCompacto = TextoLimpo.slice(0, i);
				//		alert ("Texto antes do primeiro estado de dialogo " + TextoCompacto);
				//console.log ("Antes Primeiro ED=" + TextoCompacto)
			}
			primeiro++;
			//      capturar o nome do estado de dialogo, só pra exibir
			textowork = TextoLimpo.slice(i + 7, i + 60);
			k = textowork.indexOf(" ");
			//console.log ("Achei no indice=" + k + " " + "EstadoDialogo=" + TextoLimpo.slice(i+8, i+6+k));
			//		alert ("EstadoDialogo=" + TextoLimpo.slice(i+8, i+6+k));
			nomeED = TextoLimpo.slice(i + 8, i + 6 + k);
			InicioED = i;
		} else {
			// procura o </se> identificando o fim do estado de dialogo
			if (TextoLimpo.slice(i, i + 5) === "</se>") {
				fimUltimoED = i + 5;
				SegmentoED = TextoLimpo.slice(InicioED, fimUltimoED);
				//		alert ("Segmento=" + SegmentoED)

				// Nele procura <pt> e guarda posição e procura <ud> e grava posicao
				// se nao tiver nem PT nem UD, remove o header e o trailler do ultimo estado de dialogo
				// se tiver UD e nao tiver PT, remover o estado de dialogo <se.. e remover a terminacao
				PosPrompt = -1;
				PosPrompt = SegmentoED.indexOf("<pt>");
				PosDigita = -1;
				PosDigita = SegmentoED.indexOf("ty><re>");

				// se nao achou prompt e não achou resposta apenda
				if (((PosPrompt < 0 && EhPrimeiroED != 1) && (PosDigita < 0 && EhPrimeiroED != 1)) || (nomeED === nomeEDAnt)) {
					//		   alert ("Nem Prompt nem Resposta - Segmento= " + SegmentoED);
					// remover o header do estado de dialogo,,, e appendar
					if (TextoCompacto.slice(TextoCompacto.length - 6, TextoCompacto.length - 1) === "</se>") {
						TextoCompacto = TextoCompacto.slice(0, TextoCompacto.length - 6); // tirar o trailler do estado de dialogoSegmentoED;
						//			  alert ("textocapado " + TextoCompacto);
						// tirar o header do segmentoED e appendar
						if (SegmentoED.indexOf(">") > 0) {
							SegmentoCapado = SegmentoED.slice(SegmentoED.indexOf(">") + 1, SegmentoED.length - 1);
							//console.log ("Capado=" + SegmentoCapado);
							TextoCompacto = TextoCompacto + SegmentoCapado + "</se>";

						}
					} else {
						SegmentoCapado = SegmentoED.slice(SegmentoED.indexOf(">") + 1, SegmentoED.length - 5);
						//console.log ("Capado2=" + SegmentoCapado);
						TextoCompacto = TextoCompacto.slice(0, TextoCompacto.length - 5) + SegmentoCapado + "</se>";
					}
					//console.log ("Acumulado=" + TextoCompacto);
					PosDigEDAnt = PosDigita;
					EhPrimeiroED = 0;
				} else {
					// se for o primeiro ED ou se tiver encontrado prompt segue por aqui.
					EhPrimeiroED = 0;
					//		TextoCompacto = TextoCompacto.slice(0,TextoCompacto.length-5);
					PosDigEDAnt = PosDigita;
					TextoCompacto = TextoCompacto + SegmentoED;
					//console.log ("Achou Prompt ou Primeiro" + TextoCompacto);

					// tirar o </se> do texto compacto e remover o header do SegmentoED
				}
				nomeEDAnt = nomeED;
			}

		}

	}

	l = TextoLimpo.length;
	TextoCompacto = TextoCompacto + TextoLimpo.slice(fimUltimoED, l);


	//console.log ("Resultado Final=" + TextoCompacto);

	TextoEnxuto = TextoCompacto.replace(/<\/ad><ad>/g, "");

	console.log("Resultado Final=" + TextoEnxuto);


	return TextoEnxuto;

}

function parseXMLPrompts(xml, chamada) {

	var valores = [];

	try {
		var xml_doc = $.parseXML(xml),
			$xml = $(xml_doc);
	} catch (e) {
		throw e;
	}

	$xml.find("VDATA >pt").each(function () {
		var $pt = $(this);
		$pt.text().split(",").forEach(function (cod_prompt) {
			valores.push(cod_prompt);
		});
	});

	$xml.find("VDATA se").each(function () {
		var $node = $(this);
		$node.children().each(function () {
			var $filho = $(this);
			if ($filho.is("pt")) {
				$filho.text().split(",").forEach(function (cod_prompt) {
					valores.push(cod_prompt);
				});
			}
		});
	});
	obtemPromptsDasAplicacoes(chamada.cod_aplicacao, unique2(valores));
}

function parseXMLICs(xml, chamada) {
	var valores = [];
	try {
		var xml_doc = $.parseXML(xml),
			$xml = $(xml_doc);
	} catch (e) {
		throw e;
	}

	$xml.find("VDATA va").each(function () {
		var $va = $(this);
		// Inserir prompt concatenado

		$va.text().split(",").forEach(function (cod_ic) {
			if (!cod_ic.match('"')) valores.push(cod_ic);
		});
	});

	if (chamada.cod_aplicacao.toLowerCase == 'telaunica' || chamada.cod_aplicacao == 'TELAUNICA') {
		console.log("TelaUnica");
		valores.push('IC001');
	}
	obtemIcsOnDemandDasAplicacoes(chamada.cod_aplicacao, unique2(valores));
}

// Função que utiliza o XPath e retorna a consulta ao XML
var consultaXPath = function (codXml, xml) {
	var parser = new DOMParser();
	var xml_doc = parser.parseFromString(xml, "text/xml");
	if (codXml != "") {
		/*      var caminhoConsultaObj = {
			st : "/VDATA/st",
			an : "/VDATA/an",
			ur : "/VDATA/ur",
			dn : "/VDATA/dn",
			ap : "/VDATA/ap",
			lc : "/VDATA/lc",
			hw : "/VDATA/hw",
			dn : "/VDATA/dn",
			uu : "/VDATA/uu",
			id : "/VDATA/id",
			uc : "/VDATA/uc",
			en : "/VDATA/en"
		}*/
		codXml = "/VDATA/" + codXml;
		var nodes = xml_doc.evaluate(codXml, xml_doc, null, XPathResult.ANY_TYPE, null);
		var result = nodes.iterateNext();
		if (result.childNodes[0].nodeValue != null) {
			console.log(result.childNodes[0].nodeValue);
			return result.childNodes[0].nodeValue;
		} else {
			result.childNodes[0].nodeValue = "";
			return result.childNodes[0].nodeValue;
		}
	}
}
/*
var tmrChat = setInterval(function(){  
	if ($('.SearchTool .span2').val() == "") {
		$('.ng-scope .prompt span').addClass('bubble');
		$('.ng-scope .prompt span').addClass('ura');
		$('.ng-scope .resposta span').addClass('bubble');

		$('.ng-scope .resposta span').addClass('me'); 
	}  
	
	if ($('.ura .img_play').length == 0) $('.ura').append('<img src="' + tempDir3() + 'img/play.png" class="img_play" onclick="playerPrompt(this)">');
	if ($('#modal-log').css('display') == "none") closePlayer();
	if ($('.SearchTool .span2').val() != "") {
		$('.ng-scope .prompt span').removeClass('bubble');
		$('.ng-scope .prompt span').removeClass('ura');
		$('.ng-scope .resposta span').removeClass('bubble');
		$('.ng-scope .resposta span').removeClass('me'); 
	}
}, 500);
*/

function playerPrompt(elem){
	closePlayer();

	var pai = $(elem).parent();
	var avo = $(pai).parent();
	var codPrompt = $(avo).children()[1].value;
	var codAplicacao = $(avo).children()[2].value;

	
		if (promptExist(codPrompt,codAplicacao)){
			play(elem,codPrompt,false,'audioPrompt',codAplicacao);
			return;
		}
		
		$(elem).attr('src',tempDir3() + 'img/aguarde.gif');

		if (codPrompt.indexOf(',') > -1){
			var lista = codPrompt.split(',');

			//listaAudios = lista;

			for (var i=0; i<lista.length ;i++){
				listaAudios.push({"id" : $(pai).attr('id'), "nome" : lista[i], "aplicacao" : codAplicacao });
				if (!promptExist(lista[i],codAplicacao)){
					executaExtracaoPrompts(lista[i],codAplicacao)
				};
				
			}

			var tmrPlay = setInterval(function() {
				if (promptExist(lista[0],codAplicacao)){
					play(elem,listaAudios[0].nome,true,'audioPrompt',codAplicacao);
					clearInterval(tmrPlay); 
				}else{
					$(elem).attr('src',tempDir3() + 'img/block.png');
					$(elem).attr('title','Áudio não disponível');
				}
				clearInterval(tmrPlay);

			}, 3000);
			
		}else{
			
			executaExtracaoPrompts(codPrompt,codAplicacao)
		
			var tmrPlay = setInterval(function() {
				if (promptExist(codPrompt,codAplicacao)){
					play(elem,codPrompt,false,'audioPrompt',codAplicacao);
				}else{
					$(elem).attr('src',tempDir3() + 'img/block.png');
					$(elem).attr('title','Áudio não disponível');
				}
				clearInterval(tmrPlay);
			}, 2000);
		}

};

function play(elem,codigo,multiPrompt,classe,codAplicacao){
	$('.bubble').removeClass('emFoco');
	//$('.bubble').addClass('ura');
	$(elem).attr('src',tempDir3() + 'img/play.png');
	//$(elem).parent().removeClass('ura');
	var audio;

	if(classe == "audioPrompt"){
		$(elem).parent().addClass('emFoco');
		audio = codigo+codAplicacao+'.ogg';
	}else{
		audio = listaAudios.filter(function(item){ if (item.id == codigo) return item.nome; })
		audio = audio[0].nome+codAplicacao+'.ogg';
		$('#' + codigo).addClass('emFoco');
	}

	if (multiPrompt){
		indexlistaAudios+=1;
		if(classe == "audioPrompt") { var nextPlay = 'onended="nextPromptPlay()"' } else {var nextPlay = 'onended="nextPromptPlay_()"'}
		$(elem).parent().append('<audio class="' + classe + '" id="playPrompt" autoplay controls ' + nextPlay + '"><source src="' + tempDir3() + audio + '" type="audio/ogg"></audio><img class="img_close" src="' +tempDir3()+ 'img/close.png" onclick="closePlayer()">');
		
	}else{
		$(elem).parent().append('<audio class="' + classe + '" id="playPrompt" autoplay controls><source src="' + tempDir3() + audio + '" type="audio/ogg"></audio><img class="img_close" src="' +tempDir3()+ 'img/close.png" onclick="closePlayer()">');
	}

	var posicao = $('audio').css('left');
	posicao = parseInt(posicao.replace('px',''));
	if(classe == "audioPrompt"){ posicao += 317; } else {posicao += 300;}
	$('.img_close').css('left',posicao + 'px');
	
}

function closePlayer(){
	$("audio").remove();
	$(".img_close").remove();
	$('#caption').remove();
	$('.bubble').removeClass('emFoco');
	indexlistaAudios = 0;
	listaAudios = [];
	$('.modal-footer').css('display','block');

	//$('.bubble').addClass('ura');
}

function playAllPrompts(elem){
	closePlayer();
	$(elem).attr('value','Aguarde processamento...');
	$('.modal-footer').css('display','none');

	var codAplicacao;
	indexlistaAudios = 0;
	listaAudios = [];
	$('.hPrompt').each(function(index,item){
		if (item.value.indexOf(',') > -1){
			var pai = $(item).parent();
			var span = $(pai).children()[3];
			$(span).attr('id','pt' + index);
			codAplicacao = $('.hAplicacao')[index].value;

			var lista = item.value.split(',');
			for (var y=0; y<lista.length ;y++){
				listaAudios.push({"id" : "pt" + index, "nome" : lista[y] , "aplicacao" : codAplicacao});
				if (!promptExist(lista[y],codAplicacao)){
					executaExtracaoPrompts(lista[y],codAplicacao);
				}
			}
		}else{
			
			if (item.value != "") 	{
				var pai = $(item).parent();
				var span = $(pai).children()[3];
				$(span).attr('id','pt' + index);
				codAplicacao = $('.hAplicacao')[index].value;

				listaAudios.push({"id" : "pt" + index, "nome" : item.value, "aplicacao" : codAplicacao});
			}

			if (!promptExist(item.value,codAplicacao)){
				if (item.value != "") executaExtracaoPrompts(item.value,codAplicacao);
			}
		}
		
	})
	
	var tmrPlay = setInterval(function() {
		$(elem).attr('value','Tocar todos prompts da Chamada');
		if (promptExist(listaAudios[0].nome,listaAudios[0].aplicacao)){
			play(elem , listaAudios[0].id,true,'audioTodosPrompt',listaAudios[0].aplicacao);
			clearInterval(tmrPlay); 
		};
	}, 3000);


}

var listaAudios = [];
var indexlistaAudios = 0;

function nextPromptPlay_(){
	var nextAudio = listaAudios[indexlistaAudios].nome;
	fs.readFile(tempDir3() + nextAudio + listaAudios[indexlistaAudios].aplicacao + '.ogg', function(err, body) {
		$('.bubble').removeClass('emFoco');
		$('#' + listaAudios[indexlistaAudios].id).addClass('emFoco');
		if (err != null) {
			if (listaAudios.length-1 <= indexlistaAudios) {
				$('#playPrompt').removeAttr('onended');
				indexlistaAudios = 0;
				listaAudios = [];
				return;
			}else{
				indexlistaAudios += 1;
				nextPromptPlay_();
			}
		}else{
			var audioPlayer = document.getElementById('playPrompt');
			if (audioPlayer != null){
				audioPlayer.src = tempDir3() + nextAudio + listaAudios[indexlistaAudios].aplicacao + '.ogg';
				audioPlayer.play();
			}
			indexlistaAudios+=1;
			if (listaAudios.length == indexlistaAudios) {
				$('#playPrompt').removeAttr('onended');
				indexlistaAudios = 0;
				listaAudios = [];
			}
		}
		
	});

	
	
}

function nextPromptPlay(){
	var nextAudio = listaAudios[indexlistaAudios].nome;

	fs.readFile(tempDir3() + nextAudio + listaAudios[indexlistaAudios].aplicacao + '.ogg', function(err, body) {
		if (err != null) {
			if (listaAudios.length-1 <= indexlistaAudios) {
				$('#playPrompt').removeAttr('onended');
				indexlistaAudios = 0;
				listaAudios = [];
				return;
			}else{
				indexlistaAudios += 1;
				nextPromptPlay();
			}
			
		}else{
			var audioPlayer = document.getElementById('playPrompt');
			if (audioPlayer != null){
				audioPlayer.src = tempDir3()+ nextAudio + listaAudios[indexlistaAudios].aplicacao + '.ogg';
				audioPlayer.play();
			}
			indexlistaAudios+=1;
			if (listaAudios.length-1 == indexlistaAudios) {
				$('#playPrompt').removeAttr('onended');
				indexlistaAudios = 0;
				listaAudios = [];
			}
		}
	});

	//$('.bubble').removeClass('emFoco');
	//$(elem).addClass('emFoco');

	
}


function promptExist(codPrompt,codAplicacao){
	return fs.existsSync(tempDir3() + codPrompt + codAplicacao + '.ogg');
}

function executaExtracaoPrompts(codPrompt,codAplicacao){
	var stmt = "SELECT TOP 1 Conteudo AS ARQUIVO from AudioPrompt a" 
		stmt+= " inner join VersionamentoPrompt v on a.SomaMD5 = v.SomaMD5Comprimido"
		stmt+= " inner join MapeamentoAplicacoes m on m.PastaRaiz = v.PastaAplicacao"
		stmt+= " where v.CodPrompt = '" + codPrompt + "' and m.CodAplicacao = '" + codAplicacao + "'";
		
	executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function(dado) {});
}





// FIM Função que utiliza o XPath e retorna a consulta ao XML. //