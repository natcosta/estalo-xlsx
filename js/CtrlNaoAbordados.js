/*
 * CtrlNaoAbordados
 */
function CtrlNaoAbordados($scope,$globals) {

  //Alex 10/05/2014 Andamento da extração
  if(extratorStatus != ""){
  intervaloExtratorStatusAux = setInterval(function(){
	  $('.extrator').prop("disabled",true);
	  $scope.status_extrator = extratorStatus;
	  //console.log("tô rodando aqui");
	  $scope.$apply();
	},500);
  }else{
	$('.extrator').prop("disabled",false);
	clearInterval(intervaloExtratorStatus);
	clearInterval(intervaloExtratorStatusAux);

  }

  win.title="Detalhamento Clientes não Abordados"; //Alex 27/02/2014
  $scope.versao = versao;

  $scope.rotas = rotas;

  $scope.limite_registros = 0;
  $scope.incrementoRegistrosExibidos = 100;
  $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;
  $scope.estadoEnx = false;
  $scope.itemEnx = false;
  $scope.itemEnxAll = false;

  //09/03/2015 Evitar conflito com função global que popula filtro de segmentos
  $scope.filtros = {
		isHFiltroED: false
	};


  travaBotaoFiltro(0, $scope, "#pag-nao-abordados", "Detalhamento de Clientes não Abordados");

  //Alex 24/02/2014
  $scope.status_progress_bar = 0;

  $scope.vendas = [];
  $scope.ordenacao = ['data_hora','chamador'];
  $scope.decrescente = false;
  $scope.ocultar = {
	tratado: false,
	log_formatado: false,
	log_original: true
  };

  $scope.colunas = [];
	$scope.gridDados = {
	  data: "vendas",

	  //rowTemplate: '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="{expirado: row.getProperty(\'expirado\')  == true }" class="ngCell {{col.cellClass}} {{col.colIndex()}}" ng-cell></div>',

	  rowTemplate: '<div ng-dblclick="formataXML(row.entity)" ng-repeat="col in renderedColumns" ng-class="{\'sucesso\':row.getProperty(\'cod_status\') == 0,\'alerta\':row.getProperty(\'cod_status\') == 2,\'erro\':row.getProperty(\'cod_status\') == 1 }" class="ngCell {{col.cellClass}} {{col.colIndex()}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',

	  columnDefs: "colunas",
	  enableColumnResize: true,
	  enablePinning: true
	};

  $scope.venda_atual = undefined;
  $scope.callLog = "";
  $scope.logxml = "";
  $scope.extrator = false;
  $scope.status_extrator;

  $scope.log = [];


  $scope.filtro_aplicacoes = [];

  // Filtro: produto
  $scope.filtro_produtos = [];


  // Filtro: status_vendas
  $scope.estatos = status_vendas;

  // Filtro: erros_vendas
  $scope.erros = cache.erros;

  $scope.aba = 2;

  /*cache.produtos_vendas.indice = geraIndice(cache.produtos_vendas);*/

  function apenasUSSD($filtro) {
	var filtro_aplicacoes = $filtro.val() || [];
	if (typeof filtro_aplicacoes === 'string') { filtro_aplicacoes = [ filtro_aplicacoes ]; }
	return filtro_aplicacoes.every(function (apl) { return apl.match(/^(USSD|PUSH)/); });
  }


  function geraColunas(){
	var array = [];

	array.push({ field: "data_hora_BR", displayName: "Data", width: 143, pinned: true },
			   { field: "chamador", displayName: "Chamador", width: 98, pinned: false });
	  array.push({ field: "tel_tratado", displayName: "Tratado", width: 98, pinned: false });

	var templateWithTooltip = '<div class="ngCellText" ng-class="col.colIndex()"><span title="{{row.getProperty(\'erro\')}}" ng-cell-text>{{row.getProperty(col.field)}}</span></div>';

	  array.push({ field: "aplicacao", displayName: "Aplicação", width: 91, pinned: false },
				 { field: "codSegmento", displayName: "Segmento", width: 90, pinned: false });
	  array.push({ field: "status", displayName: "Status", width: 120, pinned: false });
	  array.push({ field: "codOperador", displayName: "Operador", width: 120, pinned: false });
    array.push({ field: "motivo", displayName: "MotivoNaoAbordagem", width: 120, pinned: false });
	  //array.push({ field: "codOferta", displayName: "idPromo", width: 179 , pinned: false });
	  array.push({ field: "ucid", displayName: "Log", width: 59, cellClass: "grid-align", pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal" target="_self" data-toggle="modal" ng-click="formataXML(row.entity)" class="icon-search"></a></span></div>' }

				);

	  return array;
	}

  // Filtros: data e hora
  var agora = new Date();

  //Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

		$scope.periodo = {
			inicio:inicio,
			fim: fim,
			min: new Date(2013, 11, 1),
			max: agora // FIXME: atualizar ao virar o dia
		};

  var $view;

  $scope.$on('$viewContentLoaded', function () {
	$view = $("#pag-nao-abordados");
	  treeView('#pag-nao-abordados #chamadas', 15,'Chamadas','dvchamadas');
	  treeView('#pag-nao-abordados #repetidas', 35,'Repetidas','dvRepetidas');
	  treeView('#pag-nao-abordados #ed', 65,'ED','dvED');
	  treeView('#pag-nao-abordados #ic', 95,'IC','dvIC');
	  treeView('#pag-nao-abordados #tid', 115,'TID','dvTid');
	  treeView('#pag-nao-abordados #vendas', 145,'Vendas','dvVendas');
	  treeView('#pag-nao-abordados #falhas', 165,'Falhas','dvFalhas');
	  treeView('#pag-nao-abordados #extratores', 195,'Extratores','dvExtratores');
	  treeView('#pag-nao-abordados #parametros', 225,'Parametros','dvParam');
	  treeView('#pag-nao-abordados #admin', 255,'Administrativo','dvAdmin');
	  treeView('#pag-nao-abordados #monitoracao', 275, 'Monitoração', 'dvReparo');
	  treeView('#pag-nao-abordados #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
	  treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

		/*$('.nav.aba3').css('display','none');
		$('.nav.aba4').css('display','none');*/

	$(".aba3").css({'position':'fixed','left':'60px','top':'40px'});
	$(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

	// Bloco responsável pela SEARCHTOOL
  function searchAndHighlight(searchTerm, selector, resultElement) {
			if (searchTerm&&!(searchTerm===" ")) {
		  var selector = selector;
		  
		  $('.highlighted').removeClass('highlighted');
		if(selector==""||!selector){
		  if( $("div.log-formatado").css('display') == 'block') {
			selector = "div.log-formatado";
			// console.log("Div log formatado ativa");
		  }
		  if( $("div.log-original").css('display') == 'block') {
			selector = "div.log-original";
			// console.log("Div log original ativa");
		  }
		  if( $("div.log-prerouting").css('display') == 'block') {
			selector = "div.log-prerouting";
			// console.log("Div log prerouting ativa");
		  }
		  if( $("div.log-preroutingret").css('display') == 'block') {
			selector = "div.log-preroutingret";
			// console.log("Div log preroutingret ativa");
		  }
		}

		console.log("BEFORE BUGGING BEFORE BUGGING Selector: "+selector);
		//searchTerm = searchTerm.trim();
		
		$(selector).highlight(searchTerm, {className: 'match'});
				var matches = $('.match');
		//console.log("Matches: "+matches.length+" searchTerm "+searchTerm);
		// Funções dependentes de resultados a seguir
				if (matches != null && matches.length > 0) {

		  try{
			resultElement.text(matches.length+' encontrados'); // Conta a quantidade de elementos encontrados
		  }catch(er){
			console.log("Erro na função de pesquisa "+er);
		  }

		  // Marca o primeiro elemento MATCH como highlighted ao efetuar uma pesquisa
					$('.match:first').addClass('highlighted');

					var i = 0;

		  // Função responsável por "scrollar" e marcar o próximo elemento match como highlighted
		  $('.searchtool_next').on('click', function () {

						i++;

						if (i >= $('.match').length) i = 0;

			  //Remove o atual e marca o próximo elemento match
						$('.match').removeClass('highlighted');
						$('.match').eq(i).addClass('highlighted');

			// Cálculo final até a posição do elemento marcado highlighted
			var container = $('.modal-body'), scrollTo = $('.match').eq(i);
			var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

			container.scrollTop(
			  wheretogo-125
			);
			$('.highlighted').focus();

			// Fim do cálculo e scroll
					});

			// Função para "Scrollar" e "Marcar" o anterior
		  $('.searchtool_prev').on('click', function () {

						i--;
						if (i < 0) i = $('.match').length - 1;
			  //Remove o atual e marca o elemento anterior
						$('.match').removeClass('highlighted');
						$('.match').eq(i).addClass('highlighted');
			  //Cálculo final da posição para o scroll
			var container = $('.modal-body'), scrollTo = $('.match').eq(i);
			var wheretogo = (scrollTo.offset().top - container.offset().top + container.scrollTop())

			container.scrollTop(
			  wheretogo-125
			);

			$('.highlighted').focus();

		  });


					if ($('.highlighted:first').length) { //if match found, scroll to where the first one appears
						$(window).scrollTop($('.highlighted:first').position().top);
					}
					return true;
				}
			}
			return false;
		}

	// Função responsável por checar atualizações no INPUT text
	$('.searchtool_text').each(function() {

	  // ResultElement é o elemento onde a quantidade de resultados é exibida
	  var resultElement = $('.search_results');
	  var searchElement = $(this);

	  search();
	  // Save current value of element
	  $(this).data('oldVal', $(this));

	  // Look for changes in the value
	  $(this).bind("propertychange keyup input paste", function(event){

		  // If value has changed...
		   if ($(this).data('oldVal') != $(this).val()) {
		  // Updated stored value
		  $(this).data('oldVal', $(this).val());

			  $("div .log-original").unhighlight({className: 'match'});
			  $("div .log-formatado").unhighlight({className: 'match'});
			  $("div .log-prerouting").unhighlight({className: 'match'});
			  $("div .log-preroutingret").unhighlight({className: 'match'});
		  if (!searchAndHighlight($(this).val(), "", resultElement)) {
			  resultElement.html('Sem resultados.');
		  }
		  }
	  });
		}).delay(300);

	$(".changeSearchLog").click(function(){
		var resultElement = $('.search_results');
	  clearSearchTool("", resultElement);
	  // console.log("Chamou a função para limpar");
	});

	$("#modal-log").on('hidden', function () {
	  var resultElement = $('.search_results');
	  var searchElement = $('.searchtool_text');
	  clearSearchTool(searchElement, resultElement);
	  // console.log("Chamou a função para limpar");
	});

	
	
	// Fim do bloco responsável pela searchtool
	//minuteStep: 5

	//19/03/2014
	componenteDataHora ($scope,$view);
	carregaSites($view);
	//carregaAplicacoes($view,false,true,$scope);
	carregaAplicacoesGrupo($view,false,false,$scope,"Service to Sales");
	carregaSegmentosPorAplicacao($scope,$view,true);
	// Popula lista de segmentos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});

	carregaGruposProdutoPorAplicacao($scope,$view,true);
	// Popula lista de  grupo de produtos a partir das aplicações selecionadas
	$view.on("change", "select.filtro-aplicacao", function(){ carregaGruposProdutoPorAplicacao($scope,$view)});

	carregaProdutosPorGrupo($scope,$view,true);
	// Popula lista de  produtos a partir dos grupo de produtos selecionados
	$view.on("change", "select.filtro-grupo-produto", function(){ carregaProdutosPorGrupo($scope,$view)});


	carregaResultados($view);
	carregaErros($view);
	carregaEmpresasRecarga($view);
	  //2014-11-27 transição de abas
	  var abas = [2,3,4];

	  $view.on("click", "#alinkAnt", function () {

		if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
		if($scope.aba > abas[0]){
		  $scope.aba--;
		  mudancaDeAba();
		}
	  });

	  $view.on("click", "#alinkPro", function () {

		if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

		if($scope.aba < abas[abas.length-1]){
		  $scope.aba++;
		  mudancaDeAba();
		}

	  });

	  function mudancaDeAba(){
		abas.forEach(function(a){
		  if($scope.aba === a){
			$('.nav.aba'+a+'').fadeIn(500);
		  }else{
			$('.nav.aba'+a+'').css('display','none');
		  }
		});
	  }


	//Marcar todas aplicações
	$view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope,true)});

	 // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($('.filtro-site'), 'sites') });

	// Marca todos os segmentos
	$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});


	$view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
    });

	$view.on("change", "select.filtro-segmento", function () {
		  Estalo.filtros.filtro_segmentos = $(this).val();
	});

	/*$view.find(".selectpicker").selectpicker({
	  //noneSelectedText: 'Nenhum item selecionado',
	  countSelectedText: '{0} itens selecionados'
	});*/

	$view.find("select.filtro-aplicacao").selectpicker({
	  selectedTextFormat: 'count',
	  countSelectedText: '{0} aplicações',
	  showSubtext: true
	});

   $view.find("select.filtro-site").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} Sites/POPs',
        showSubtext: true
    });


	// EXIBIR AO PASSAR O MOUSE
  $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
	  $("div.data-inicio.input-append.date").data("datetimepicker").hide();
	  $("div.data-fim.input-append.date").data("datetimepicker").hide();
	  $('div.btn-group.filtro-aplicacao').addClass('open');
	  $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
			$('div.btn-group.filtro-aplicacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseover(function () {
			//11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-grupo-produto .btn').hasClass('disabled')){
			$('div.btn-group.filtro-grupo-produto').addClass('open');
		  }

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-grupo-produto').mouseout(function () {
			$('div.btn-group.filtro-grupo-produto').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-produto').mouseover(function () {
		   //11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-produto .btn').hasClass('disabled')){
			$('div.btn-group.filtro-produto').addClass('open');
		  $('div.btn-group.filtro-produto>div>ul').css({'max-height':'500px','overflow-y':'auto','min-height':'1px'});
		  }

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-produto').mouseout(function () {
			$('div.btn-group.filtro-produto').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-erro').mouseover(function () {
			$('div.btn-group.filtro-erro').addClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'-30px' });
			$('div.btn-group.filtro-erro>div>ul').css({'max-width':'450px','max-height':'500px','overflow-y':'auto','min-height':'1px'});

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-erro').mouseout(function () {
			$('div.btn-group.filtro-erro').removeClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'0px' });
			$('div.btn-group.filtro-erro>div>ul').css({'max-width':'450px','max-height':'500px','overflow-y':'auto','min-height':'1px'});
		});
		
			// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseover(function () {
			$('div.btn-group.filtro-empresaRecarga').addClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'-30px' });
			$('div.btn-group.filtro-empresaRecarga>div>ul').css({'max-width':'450px','max-height':'500px','overflow-y':'auto','min-height':'1px'});

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-empresaRecarga').mouseout(function () {
			$('div.btn-group.filtro-empresaRecarga').removeClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'0px' });
			$('div.btn-group.filtro-empresaRecarga>div>ul').css({'max-width':'450px','max-height':'500px','overflow-y':'auto','min-height':'1px'});
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-status').mouseover(function () {
			$('div.btn-group.filtro-status').addClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'-52px' });

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-status').mouseout(function () {
			$('div.btn-group.filtro-status').removeClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'0px' });
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
		  //11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
			$('div.btn-group.filtro-segmento').addClass('open');
			$('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		  }
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
			$('div.btn-group.filtro-segmento').removeClass('open');
		});
		  // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });



			$view.on("dblclick", "div.ng-scope.ngRow", function () {
				//$scope.formataXML($scope.chamada_atual);
				//$view.find(".btn-log-original").parent().removeClass("active");
				//$view.find(".btn-log-formatado").parent().addClass("active");
				$scope.$apply(function () {
					$scope.ocultar.log_original = true;
					$scope.ocultar.log_formatado = false;
				});
				$view.find("#modal").modal('show');
			});


	// Lista vendas conforme filtros
	$view.on("click", ".btn-gerar", function () {

	  limpaProgressBar($scope, "#pag-nao-abordados");

		//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini,data_fim);
			if(testedata!==""){
				setTimeout(function(){
					atualizaInfo($scope,testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				},500);
				return;
			}

	  //13/06/2014 Limita pesquisa a um mês
	  /*if(_02d(data_ini.getMonth() + 1) !== _02d(data_fim.getMonth() + 1)){
		setTimeout(function(){
		  atualizaInfo($scope,"Operação válida somente para o mesmo mês.");
		  effectNotification();
		  $view.find(".btn-gerar").button('reset');
		},500);
		return;
	  }*/

			//22/03/2014 Testa se uma aplicação foi selecionada
			var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
			if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
				setTimeout(function(){
					atualizaInfo($scope,'Selecione uma aplicação');
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				},500);
				return;
			}
	   $scope.colunas = geraColunas();

	  /*var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val();
	  obtemPromptsDasAplicacoes(filtro_aplicacoes);
	  if(!filtro_aplicacoes.indexOf('CORPO') || filtro_aplicacoes !=='CORPO'){
	  obtemItensDeControleDasAplicacoes(filtro_aplicacoes,true);
	  }*/

	  $scope.listaVendas.apply(this);
	});

	$view.on("click", ".btn-log-original", function (ev) {
		
	  ev.preventDefault();
	  $scope.consultaCallLog($scope.venda_atual);
	  //$view.find(".btn-log-formatado").parent().removeClass("active");
	  //$view.find(".btn-log-original").parent().addClass("active");
	  $scope.$apply(function () {
		$scope.ocultar.log_formatado = true;
		$scope.ocultar.log_original = false;
	  });
	});

	$view.on("click", ".btn-log-formatado", function (ev) {
      killThreads();
	  ev.preventDefault();
	  $scope.formataXML($scope.venda_atual);
	  //$view.find(".btn-log-original").parent().removeClass("active");
	  //$view.find(".btn-log-formatado").parent().addClass("active");
	  $scope.$apply(function () {
		$scope.ocultar.log_original = true;
		$scope.ocultar.log_formatado = false;
	  });
	});

	$view.on("click", ".btn-exportar", function () {
	  killThreads();
	  $scope.exportaXLSX.apply(this);
	});

	// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora ($scope,$view,true);

			var partsPath = window.location.pathname.split("/");
			var part  = partsPath[partsPath.length-1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function(){
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			},500);
		}

	// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view,$scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
		abortar($scope, "#pag-nao-abordados");
		}

	$view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  });

  // Exibe mais registros
	  $scope.exibeMaisRegistros = function () {
		$scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
		  /*$scope.$apply(function(){
		  });
		  var h = document.getElementById('pag-nao-abordados').scrollHeight;
		  $('body').scrollTop(h);*/
		//retornaStatusQuery($scope.chamadas.length, $scope, $scope.numRegistrosExibidos);

	  };

  // Lista vendas conforme filtros
	  $scope.listaVendas = function () {
		  
		  var datasComDados = [];

		if($scope.extrator){ $scope.limite_registros = 0;}

		  $globals.numeroDeRegistros = 0;

		  //if (!connection) {
		  //  db.connect(config, $scope.listaVendas);
		  //  return;
		  //}

		  var $btn_exportar = $view.find(".btn-exportar");
		  $btn_exportar.prop("disabled", true);

		  var $btn_gerar = $(this);
		  $btn_gerar.button('loading');

		  var aplicacao = $scope.aplicacao;

		  var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

		  var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		  var filtro_produtos = $view.find("select.filtro-produto").val() || [];
		  var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		  var filtro_grupo_produtos = $view.find("select.filtro-grupo-produto").val() || [];
		  var filtro_status = $view.find("select.filtro-status").val() || [];
		  var filtro_erros = $view.find("select.filtro-erro").val() || [];
		  var filtro_parceiros = $view.find("select.filtro-empresaRecarga").val() || [];

		  if (filtro_status.indexOf("0") !== -1) { filtro_status.push("20"); }

		  //filtros usados
		  $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
		  + " até " + formataDataHoraBR($scope.periodo.fim);
		  if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		  if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		  if (filtro_produtos.length > 0) { $scope.filtros_usados += " Produtos: " + filtro_produtos; }
		  if (filtro_grupo_produtos.length > 0) { $scope.filtros_usados += " Grupos: " + filtro_grupo_produtos; }
		  if (filtro_status.length > 0) { $scope.filtros_usados += " Status: " + filtro_status; }
		  if (filtro_erros.length > 0) { $scope.filtros_usados += " Erros: " + filtro_erros; }
		  if (filtro_parceiros.length > 0) { $scope.filtros_usados += " Parceiros: " + filtro_parceiros; }



		  $scope.vendas = [];
		  
		  
		  var stmt = db.use + "SELECT" + testaLimite($scope.limite_registros)
		  + "   I.CodUCID, I.DataHora, I.NumANI, I.TelDigitado, I.CodAplicacao, I.CodSegmento, M.Descricao as MotivoNaoAbordagem,";
		  stmt += " I.Status, I.codOperador";
		  stmt += " FROM " + db.prefixo + "ChamadasSemVendaS2S as I, MotivosNaoAbordagemS2S M"		  
		  stmt += " WHERE 1 = 1 AND ISNULL(I.MotivoNaoAbordagem, '0') = M.CodMotivo"		  
		  + "   AND DataHora >= '" + formataDataHora(data_ini) + "' AND  DataHora <= '" + formataDataHora(data_fim) + "'";

		  stmt += restringe_consulta("I.CodAplicacao", filtro_aplicacoes, true);		  
		  stmt += restringe_consulta("status", filtro_status, true);		  

		  stmt = stmt + " order by DataHora, NumANI";

		  $scope.extrator ? log("Extrator: "+stmt) : log(stmt);

		  
		  var stmtCountRows = stmtContaLinhasVendas(stmt);
		  
		  // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
		  function contaLinhas(columns) {
			  
			  datasComDados.push(columns[1].value);
			  //if(datainicio_mod === "") datainicio_mod = columns[1].value +":00:00";
			  //datafim_mod = columns[1].value +":59:59";
			  //console.log(columns[1].value);
				  $globals.numeroDeRegistros = columns[0].value;
		  }		  

		  //db.query(stmt, function (columns) {
		  function executaQuery(columns) {

			var UCID = columns["CodUCID"].value,
			data_hora = formataDataHora(columns["DataHora"].value),
			data_hora_BR = formataDataHoraBR(columns["DataHora"].value),
			data = columns["DataHora"].value,
			chamador = columns["NumANI"].value,
			motivo = columns["MotivoNaoAbordagem"].value || "",    
			tel_tratado = columns["TelDigitado"].value,
			cod_aplicacao = columns["CodAplicacao"].value,
			aplicacao = obtemNomeAplicacao(cod_aplicacao),
			status = columns["Status"].value,
            //codOferta = columns["CodOferta"].value,			
			codSegmento = obtemNomeSegmento(columns["CodSegmento"].value),
			codOperador = columns["codOperador"] !== undefined ? columns["codOperador"].value : undefined;
      console.log("Colunas: ");
      console.log(columns);
						


				  $scope.vendas.push({
					  UCID: UCID,
					  data_hora: data_hora,
					  data_hora_BR: data_hora_BR,
					  data: data,
					  chamador: chamador,
					  tel_tratado: tel_tratado,
					  cod_aplicacao: cod_aplicacao,
					  codSegmento: codSegmento,
					  aplicacao: aplicacao,					  					  					  
					  status: status,					
					  //codOferta: codOferta,					  
            motivo: motivo,
					  codOperador: codOperador
				  });
				  //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length, $scope, "#pag-nao-abordados");

			  if($scope.vendas.length % 1000===0){
					$scope.$apply();
				}

		  } /*, function (err, num_rows) {
				retornaStatusQuery(num_rows, $scope);
				// Limpar o grid
				$scope.$apply(function () {
				});
				$btn_gerar.button('reset');
				$btn_exportar.prop("disabled", false);
			});*/

		  //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
			  //console.log("Executando query-> " + stmtCountRows + " " + $globals.numeroDeRegistros);

		if(!$scope.extrator){

		  //13/06/2014 Query de hora em hora
		  //var stmts = arrayHorasQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);
		  //13/06/2014 Query de 5 em 5 minutos
		  var stmts = arrayCincoMinutosQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);


		  //13/06/2014 Data final do penúltimo array com base na hora final do filtro, último array é uma flag
		  //stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(formataDataHora(precisaoMinutoFim(data_fim)),formataDataHora(data_fim))];
		  stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(/DataHora <= \'.+\' /g,"DataHora <= \'"  +formataDataHora(new Date(data_fim))+"\' ")];



		  var controle = 0;
		  var total = 0;

		  function proximaHora(stmt){
			  
			  
		  var pularQuery = false;
			  
		  for (var i = 0; i < datasComDados.length; i++){
			  var d = new RegExp ('DataHora >= \''+datasComDados[i]+'(.*?)\'');
			  if(stmt.match(d) === null){
				  pularQuery = true;
			  }else{
				  pularQuery = false;
				  datasComDados.splice(0,i-1);
				  break;
			  }
		  }

		    function comOuSemDados(err,c){
                $('.notification .ng-binding').text("Aguarde... "+atualizaProgressExtrator(controle,stmts.length)+ " Encontrados: "+total).selectpicker('refresh');
				controle++;
				$scope.$apply();

				//Executa dbquery enquanto array de querys menor que length-1
				if(controle < stmts.length - 1){
				  proximaHora(stmts[controle].toString());
				  if(c === undefined) console.log(controle);
				}

				//Evento fim
				if(controle === stmts.length - 1){
				  if(c === undefined) console.log("fim " + controle);
				  if(err){
					retornaStatusQuery(undefined, $scope);
				  }else{
					retornaStatusQuery($scope.vendas.length, $scope);
				  }

					$btn_gerar.button('reset');
					if($scope.vendas.length > 0){
					  $btn_exportar.prop("disabled", false);
					}else{
					  $btn_exportar.prop("disabled", "disabled");
					}
				}
			}
		  

			if(pularQuery){
				comOuSemDados(false,"console");		  
			}else{		  
			  
			  //console.log("uararrr ->"+stmt);
			  
			  db.query(stmt, executaQuery, function (err, num_rows) {
			      console.log("Executando query-> " + stmt + " " + num_rows);
			      //userLog(stmt, 'Tenta encontrar', 2, err)
				num_rows !== undefined ? total += num_rows : total += 0;
			    comOuSemDados(err);

			  });		  
			}
		  }
		  
		  
		  db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
			  console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);
			  
			  if($globals.numeroDeRegistros === 0){
				  retornaStatusQuery(0, $scope);
				  $btn_gerar.button('reset');
			  }else{
				  //Disparo inicial
				  proximaHora(stmts[0].toString());
			  }
		  
		  });
		  
		  
		  





			  /*db.query(stmt, executaQuery, function (err, num_rows) {
				  console.log("Executando query-> " + stmt + " " + num_rows);
				  retornaStatusQuery(num_rows, $scope);
				  $btn_gerar.button('reset');
				  if(num_rows>0){
					$btn_exportar.prop("disabled", false);
				  }
			  });*/
		}else{


		  var datainicio,datafim;

		   function minMax(columns) {
			 datainicio = columns[0].value;
			 datafim = columns[1].value;
		   }

		  var stmt_min_max = stmtMinMaxPivot(stmt, 'DataHora');
		  var stmt_mod;

		  db.query(stmt_min_max, minMax, function (err, num_rows) {
			console.log("Executando query-> " + stmt_min_max + " " + num_rows);
			  if(datainicio !== null || datafim !== null){
				stmt_mod = stmt.replace(formataDataHora($scope.periodo.inicio),formataDataHora(datainicio));
				stmt_mod = stmt_mod.replace(formataDataHora($scope.periodo.fim),formataDataHora(datafim));
				executa();
			  }else{
				retornaStatusQuery(0, $scope);
				$btn_gerar.button('reset');
			  }
			});

		  function executa(){
			var header = ['UCID','Data e hora','Chamador','tel_tratado','Aplicação','Transação','Produto','Valor', 'Status','Código erro'];
			executaThread2(datainicio, datafim, 1000, header, stmt_mod, false);
			intervaloExtratorStatus = setInterval(function(){
			  $('.extrator').prop("disabled",true);
			  $scope.status_extrator = extratorStatus;
			  //console.log("tô rodando");
			  $scope.$apply();
			},500);
			atualizaInfo($scope, "O arquivo abrirá automaticamente quando estiver pronto. Mantenha o Estalo aberto.");
			$btn_gerar.button('reset');
			$scope.extrator = false;
			$scope.limite_registros = 5000;
			$scope.$apply();
		  }
		}
		  //});





		  $view.on("mouseup", "tr.detalhamento", function () {
			  var that = $(this);
			  $('tr.detalhamento.marcado').toggleClass('marcado');
			  $scope.$apply(function () {
				  that.toggleClass('marcado');
			  });
		  });
	  };

 	// Consulta o log da chamada
  $scope.consultaCallLog = function (chamada) {
    
    // Se a chamada acabou de ser consultada (incluindo o log original), não há nada a fazer
    if (chamada === $scope.venda_atual && $scope.callLog !== "" && $scope.logxml !== "") return true;

    var tabela = "CallLog_";
    if (unique2(cache.apls.map(function (a) {
        if (a.nvp) return a.codigo;
      })).indexOf(chamada.cod_aplicacao) >= 0) {
      tabela = "CallLogNVP_";
    }

    if (chamada.cod_aplicacao === "MENUOI") {
      tabela = "CallLogM4U_";
    }

    if (chamada.cod_aplicacao.match('TELAUNICA')) {
      tabela = "CallLogTelaUnica_";
    }
	
	if (chamada.cod_aplicacao.match('S2S')) {
      tabela = "CruzamentoS2S";
    }
    
    $scope.callLog = '';
    $scope.$apply();
      
    var verificaLogs = "sp_spaceused " + tabela;
    if(tabela != "CruzamentoS2S"){
       verificaLogs+= chamada.data_hora.substr(5, 2);
    }
    mssqlQueryTedious(verificaLogs, function (err, retorno) {
      if (err) console.log(err)
      // console.log('b')
      // console.log(retorno)
      // console.log(retorno[0][1])
      if(retorno[0][1].value == 0) {
        var mesString = formataDataMesString(chamada.data_hora);
        $scope.callLog = 'Não há dados de log original de ' + mesString + ' no banco de dados.'
        $scope.$apply();
        // $('pre.callLogValue').html('Não há dados de <b>Log Original</b> de ' + mesString + ' no banco de dados.');
      } else {
        if(tabela === "CruzamentoS2S") {
                var stmt = "SELECT TOP 1 conteudoFinal AS CONTEUDOORIGINAL " 
                + " FROM CruzamentoS2S "
                + " WHERE CodUCID = '" + chamada.UCID + "'";                
        }else{
                var stmt = "SELECT TOP 1 Comprimido AS ARQUIVO "
                + "FROM "+ tabela + ""+ chamada.data_hora.substr(5, 2)
                + " WHERE CodUCID = '" + chamada.UCID + "'"
                + "  OR CodUCID = '" + obtemUCIDAntigo(chamada.UCID, chamada.cod_aplicacao) + "'" 
                + " ORDER BY LEN(CodUCID)";
        }
        log(stmt);

        $scope.chamadaCallLogData = chamada.data;
        $scope.tabelaCallLog = tabela;
        $scope.downloadLog = "";
        
        if(tabela === "CruzamentoS2S") {
          
          function salvaLog(columns){
            $scope.downloadLog = columns[0].value;
            //retorna apenas um valor            
          }
          
          db.query(stmt, salvaLog, function (err, num_rows) {  
            if(err) {console.log(err)}            
          });
          
          
        }else{
          executaThreadBasico('extratorDiaADia', stringParaExtrator(stmt, []), function(dado) {});
        }

        var teste4 = false;

        var teste3 = setInterval(function() {
          if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip') || $scope.downloadLog) {
			  
			  clearInterval(teste3);
			  
			if(tabela === "CallLogS2S_"){
				
				$scope.$apply(function() {
					
					$scope.callLog = fs.readFileSync(tempDir3() + chamada.UCID + '_log.zip').toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					//console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip')) {
					  fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');
					  teste4 = false;
					}
				});
				
				
			}else if(tabela === "CruzamentoS2S"){
        
				$scope.$apply(function() {
        $scope.callLog = $scope.downloadLog;
        //.toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					//console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if ($scope.downloadLog) {
					  clearInterval(teste3);
					 /* fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');*/
					  teste4 = false;
					}
				});
				
        
      }else{
				// Descomprimir o log original
				zlib.unzip(new Buffer(fs.readFileSync(tempDir3() + chamada.UCID + '_log.zip')), function(err, buffer) {
				  $scope.$apply(function() {
					if (err) {
					  clearInterval(teste3);
					  log(err);
					  return;
					}
					//console.log(buffer.toString());
					$scope.callLog = buffer.toString();
					$scope.logValidado = validaLogGeral($scope.callLog);
					$scope.callLog = mascaraInformacoes($scope.callLog);
					console.log($scope.logValidado);
					$('#myModalLabel').text("Log da chamada");
					search(".log-original");
					if (fs.existsSync(tempDir3() + chamada.UCID + '_log.zip')) {
					  fs.unlinkSync(tempDir3() + chamada.UCID + '_log.zip');
					  teste4 = false;
					}
				  });
				});
			
			}
			
			
          }
        }, 500);
      }
    }, function (err, num_rows){
      console.log("Retornei :) "+num_rows);
      if(err) console.log(err);
    });

    function tempoLimiteCarregarLogOriginal() {
      setTimeout(function() {
        if (!fs.existsSync(tempDir3() + chamada.UCID + '_log.zip') && teste4 ) {
          clearInterval(teste3);
          $('#myModalLabel').text("Log demorando para carregar ou indisponível");

        }
      }, 20000);
    }
    return true;
  };
	  
	  

  


  $scope.infos = [];


  // Consulta e formata o XML da chamada
    $scope.formataXML = function (venda, check) {
	 
		//alert("ED "+$scope.itemEnx+" IC "+$scope.estadoEnx);

		$('#myModalLabel').text("Log da chamada");

		/*if (!cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts')) {
		  obtemPromptsDasAplicacoes(chamada.cod_aplicacao);
		}*/

		delete cache.aplicacoes[venda.cod_aplicacao].prompts;
		delete cache.aplicacoes[venda.cod_aplicacao].itens_controleOnDemand;

		/*if (!cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controle')) {
		  if (chamada.cod_aplicacao !== 'CORPO') {

			//obtemItensDeControleDasAplicacoes(chamada.cod_aplicacao, true);
		  }
		} else {
		  //if (cache.aplicacoes[chamada.cod_aplicacao].itens_controle.length <= 100) {
		  //  delete cache.aplicacoes[chamada.cod_aplicacao].itens_controle;
		  //  obtemItensDeControleDasAplicacoes(chamada.cod_aplicacao,true);
		  //}
		}*/

		// Se a chamada acabou de ser consultada, não há nada a fazer
		if (check === undefined) {
		  if (venda === $scope.venda_atual && logCarregado) return true;
		}

		// Limpar o log original da chamada consultada anteriormente
		$scope.venda_atual = venda;
		$scope.callLog = "";
		$scope.ocultar.log_original = true;
		$scope.ocultar.log_prerouting = true;
		$scope.ocultar.log_preroutingret = true;
		$scope.ocultar.log_formatado = false;
		$scope.logxml = "";

		// Buscar o log em XML
		var stmt = "SELECT XMLIVR,PREROTEAMENTO,PREROTEAMENTORET,RECONHECIMENTOVOZ FROM IVRCDR WHERE CodUCIDIVR = '" + venda.UCID + "'";
		log(stmt);

		var xml = "",
			routing = "",
			routingRet = "",
			recVoz = "";
			
			
			executaThreadBasico('extratorDiaADia',stringParaExtrator(stmt,[]), function(dado){console.log(dado)},undefined,'' + venda.UCID + '_temp.txt');
			var teste = setInterval(function () {
			  if(fs.existsSync(tempDir3()+venda.UCID + '_temp.txt')){
				  clearInterval(teste);			
				var dados_temp = fs.readFileSync(tempDir3()+venda.UCID + '_temp.txt','UTF-8');
				dados_temp = dados_temp.split("\t");
				
				xml = dados_temp[0];
				routing = dados_temp[1];
				routingRet = dados_temp[2];
				parseXMLPrompts(dados_temp[0], venda);
				//parseXMLICs(dados_temp[0], chamada);
				recVoz = dados_temp[3];
				
			  }
			}, 500);
		
		
		var teste2 = setInterval(function () {
		  
		  
			/*if (cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('prompts') && 
				cache.aplicacoes[chamada.cod_aplicacao].hasOwnProperty('itens_controleOnDemand') && 
				cache.aplicacoes[chamada.cod_aplicacao].itens_controleOnDemand['indice'] !== undefined){*/
			if (cache.aplicacoes[venda.cod_aplicacao].hasOwnProperty('prompts') && 
				cache.aplicacoes[venda.cod_aplicacao].hasOwnProperty('itens_controle') && 
				cache.aplicacoes[venda.cod_aplicacao].itens_controle['indice'] !== undefined){
				clearInterval(teste2);
		  
			  $('div.modal-header h3').text("Log da chamada");
			  $scope.$apply(function () {
				  
				// Formatar o log em XML			
				if ($scope.estadoEnx === true) {
				  $scope.logxml = parseXML(RemoverEDsExcedentes(xml), venda, $scope.itemEnx, $scope.itemEnxAll);
				} else {
			
				  $scope.logxml = parseXML(xml, venda, $scope.itemEnx, $scope.itemEnxAll);
				}

				//$scope.prerouting = obterRouting(routing);
				//$scope.preroutingret = obterRouting(routingRet);
				
				if(fs.existsSync(tempDir3()+venda.UCID + '_temp.txt')){
					fs.unlinkSync(tempDir3()+venda.UCID + '_temp.txt');
				}
				
			  });
			} else {
				$('div.modal-header h3').text("Carregando...");
			}
		}, 500);
		
	

	return true;
    };




  /*// Consulta e formata o XML da chamada
  $scope.formataXML = function (venda) {
	// Se a chamada acabou de ser consultada, não há nada a fazer
	if (venda === $scope.venda_atual) return true;

	// Limpar o log original da chamada consultada anteriormente
	$scope.venda_atual = venda;
	$scope.callLog = "";
	$scope.ocultar.log_original = true;
	$scope.ocultar.log_formatado = false;

	//var trecho  = "AND CodTrecho = '01'";
	var trecho  = "";

	var stmt = "SELECT XMLIVR FROM IVRCDR_" + venda.data_hora.substr(5, 2)
	  + " WHERE CodUCIDIVR = '" + venda.UCID + "' "+trecho+"";
	log(stmt);
	db.query(stmt, function (columns) {
	  $scope.$apply(function () {
		$scope.logxml = parseXML(columns[0].value, venda);
	});

});

return true;



  };*/


  // Exportar planilha XLSX
	$scope.exportaXLSX = function () {


	  var $btn_exportar = $(this);
	  $btn_exportar.button('loading');


	  if($scope.vendas.length > 50000){

		if(fs.existsSync(tempDir3()+'tVendasMore.xlsm')){
		  fs.unlinkSync(tempDir3()+'tVendasMore.xlsm');
		}
		  
		  
	  

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='tVendasMore'";
		log(stmt);
		db.query(stmt, function (columns) {
		  var dataAtualizacao = columns[0].value,
			  nomeRelatorio = columns[1].value,
			  arquivo = columns[2].value;

		  var milis = new Date();
		  var baseFile = 'tVendasMore.xlsm';
		  var buffer = toBuffer(toArrayBuffer(arquivo));
		  fs.writeFileSync(tempDir3()+baseFile, buffer, 'binary');
		}, function (err, num_rows) {
		  if(fs.existsSync(tempDir3()+'dados_vendas.txt')){
			fs.unlinkSync(tempDir3()+'dados_vendas.txt');
		  }
		  var i = 0;
		  $scope.vendas.forEach(function(v){            fs.appendFileSync(tempDir3()+'dados_vendas.txt',v.data_hora_BR+'\t'+v.chamador+'\t'+v.aplicacao+'\t'+v.codSegmento+'\t'+v.tipo+'\t'+v.grupo+'\t'+v.produto+'\t'+v.valor+'\t'+v.status+'\t'+v.codErro+'\t'+v.erro+'\t'+v.codOferta+'\t'+v.codOperador+'\t'+v.cpf+'\t'+v.contrato+'\t\n');

		  i++;
		  if(i === $scope.vendas.length){
			childProcess.exec(tempDir3()+'tVendasMore.xlsm');
		  }
		  });

		  setTimeout(function(){
			$btn_exportar.button('reset');
		  },500);

		});

	  }else{

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
		+ " WHERE NomeRelatorio='tNaoVendas'";
		log(stmt);
		db.query(stmt, function (columns) {
			var dataAtualizacao = columns[0].value,
				nomeRelatorio = columns[1].value,
				arquivo = columns[2].value;


   var milis = new Date();
   var baseFile = 'tNaoVendas.xlsx';
   var buffer = toBuffer(toArrayBuffer(arquivo));
   fs.writeFileSync(baseFile, buffer, 'binary');



  //var milis = new Date();
  //var baseFile = 'templates/tVendas.xlsx';


   var file = 'detalhamentoNaoVendas_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {

   // Gilberto 28/03/2014 reordenação por data
	//$scope.vendas.sort(function(primeiro, segundo){
					  //return primeiro.data.getTime()-segundo.data.getTime();
					//});


	// var vendas  =  $scope.vendas;

	 //vendas.map(function (array) {
	   //array.erro = array.codErro + array.erro;
	   //return array;
	 //});


   // Create a template
	var t = new XlsxTemplate(data);

				// Perform substitution
				t.substitute(1, {
				 filtros: $scope.filtros_usados,
				  planDados: $scope.vendas
				});

	// Get binary data
	newData = t.generate();


	if(!fs.existsSync(file)){
		fs.writeFileSync(file, newData, 'binary');
		childProcess.exec(file);
	}
   });

	setTimeout(function(){
	  $btn_exportar.button('reset');
	},500);



		  }, function (err, num_rows) {
			//?
		  });

	};

  $scope.celula = function (cod_status) {
	return ([ 'success', 'danger', 'warning' ])[cod_status];
  };
  
	}
	
	
  /* Bloco Responsável pela pesquisa */
	
	function clearSearchTool(searchElement, resultElement){
		
	  // Função responsável por resetar a função de pesquisa
	  //console.log("Limpou a ferramenta de pesquisa de logs");
	  
	  if(resultElement==""||resultElement==null){
		  resultElement = $('.search_results');
	  }
	  $(resultElement).html('');
	  $(searchElement).val('');
	  $(searchElement).attr("placeholder", "Buscar...");
	  $("div .log-original").unhighlight({className: 'match'});
	  $("div .log-formatado").unhighlight({className: 'match'});
	  $("div .log-prerouting").unhighlight({className: 'match'});
	  $("div .log-preroutingret").unhighlight({className: 'match'});
	};
	
	function search(selector){
		var resultElement = $('.search_results');
		var searchElement = $(".searchtool_text");
		clearSearchTool("", resultElement);
		
		// console.log("Search!!!!!!");
		
		if(selector==""||!selector){
		  if( $("div.log-formatado").css('display') == 'block') {
			selector = "div.log-formatado";
		  }
		  if( $("div.log-original").css('display') == 'block') {
			selector = "div.log-original";
		  }
		  if( $("div.log-prerouting").css('display') == 'block') {
			selector = "div.log-prerouting";
		  }
		  if( $("div.log-preroutingret").css('display') == 'block') {
			selector = "div.log-preroutingret";
		  }
		}
		
		if(searchElement.val()!=""&&searchElement.val()!=undefined&&searchElement.val()!=null){
			try{				
				$(selector).highlight(searchElement.val(), {className: 'match'});
				$('.match:first').addClass('highlighted');
				var matches = $(".match");
				resultElement.text(matches.length+' encontrados');
				
			} catch(err){
				console.log("Error: "+err);
			}
		}
	}
  $scope.$watch('callLog', function(newValue, oldValue) {
	  setTimeout(function(){
					search(".log-original");
		}, 1000);
	});	
	$scope.$watch('ocultar.log_formatado', function(newValue, oldValue) {
		if(newValue==false){
			search(".log-formatado");
		}
	});	
		
	$scope.$watch('ocultar.log_original', function(newValue, oldValue) {
			if(newValue==false){
				search(".log-original");
			}
	});
	
	/* Fim do bloco responsável pelas tarefas de pesquisa */
  
	
}
CtrlNaoAbordados.$inject = ['$scope','$globals'];
