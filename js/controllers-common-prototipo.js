
String.prototype.includes = function () {
	'use strict';
	return String.prototype.indexOf.apply(this, arguments) !== -1;
};

Array.prototype.remove = function () { //Remover VALOR de ARRAY
	var what, a = arguments,
		L = a.length,
		ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
};

//Função para remover aspas duplas do início e do fim de uma string
String.prototype.unquote = function () {
	return this.replace(/(^")|("$)/g, '')
}

Array.prototype.filterMap = function (fun /*, thisp */ ) {
	"use strict";

	if (this == null)
		throw new TypeError();

	var t = Object(this);
	var len = t.length >>> 0;
	if (typeof fun != "function")
		throw new TypeError();

	var res = [];
	var thisp = arguments[1];
	for (var i = 0; i < len; i++) {
		if (i in t) {
			var val = t[i]; // in case fun mutates this
			var ret = fun.call(thisp, val, i, t);
			if (ret)
				res.push(ret);
		}
	}

	return res;
};

Array.prototype.indexBy = function (key) {
	var self = this;
	this.forEach(function (item) {
		self[item[key]] = item;
	});
};

