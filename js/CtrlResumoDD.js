function CtrlResumoDD($scope, $globals) {
	
	win.title = "Resumo dia a dia"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	//Alex 08/02/2014	
	travaBotaoFiltro(0, $scope, "#pag-resumo-dia", "Resumo dia a dia");

	$scope.dados = [];
	$scope.csv = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;

	$scope.colunas = [];
	$scope.gridDados = {
		data: "dados",
		columnDefs: "colunas",
		enableColumnResize: true,
		enablePinning: true
	};


	$scope.log = [];
	$scope.aplicacoes = [];
	$scope.sites = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.tlv = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		segmentos: [],
		sites: [],
		isHFiltroED: false,
		isHFiltroIC: false
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;


	$scope.porDDD = null;

	//02/06/2014 Flag
	$scope.porRegEDDD = $globals.agrupar;
	$scope.aba = 2;


	function geraColunas() {
		var array = [];

		array.push({
			field: "datReferencia",
			displayName: "Data",
			width: 100,
			pinned: true
		});
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

		if ($('#porRegEDDD').prop('checked') === false && filtro_segmentos.length > 0 && $('.chkMensal').prop('checked') === false){
			array.push({
					field: "segmentos",
					displayName:"Segmento",
					width: 95,
					pinned: false,
				});
		}

		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];

		if ($('#porRegEDDD').prop('checked') === false && filtro_regioes.length > 0 && original_ddds.length === 0) {
			array.push({
				field: "regiao",
				displayName: "Região",
				width: 100,
				pinned: false
			});
		} else if (original_ddds.length > 0) {
			array.push({
				field: "ddd",
				displayName: "DDD",
				width: 100,
				pinned: false
			});
		}

		array.push({
			field: "qtdEntrantes",
			displayName: "Entrantes",
			width: 100,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdDerivadas",
			displayName: "Derivadas",
			width: 100,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdFinalizadas",
			displayName: "Finalizadas",
			width: 100,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdAbandonadas",
			displayName: "Abandonadas",
			width: 115,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		}, {
			field: "qtdRetidas",
			displayName: "Retidas",
			width: 85,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		});

		//Brunno 19/02/2019
		//if (filtro_regioes.length === 0 && original_ddds.length === 0) {
			array.push({
				field: "tma",
				displayName: "TMA",
				width: 65,
				pinned: false,
				cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
			});
		//}

		array.push({
			field: "qtdPercDerivadas",
			displayName: "% Der",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdPercFinalizadas",
			displayName: "% Fin",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdPercAbandonadas",
			displayName: "% Aba",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdPercRetidas",
			displayName: "% Ret",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
		}, {
			field: "qtdClientesEntrantes",
			displayName: "CE",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		}, {
			field: "qtdClientesDerivados",
			displayName: "CD",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		}, {
			field: "qtdPercClientesEntrantes",
			displayName: "% CE",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		}, {
			field: "qtdPercClientesDerivados",
			displayName: "% CD",
			width: 65,
			pinned: false,
			cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		});
		var filtro_ed = $scope.filtroEDSelect || [];
		var filtro_ic = $scope.filtroICSelect || [];
		// if(filtro_ed[0] === undefined && filtro_ic[0] === undefined){
		// 	array.push({
		// 		field: "qtdRetidasLiq1h",
		// 		displayName: "Ret Líq 1H",
		// 		width: 85,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		// 	},
		// 	{
		// 		field: "qtdRetidasLiq2h",
		// 		displayName: "Ret Líq 2H",
		// 		width: 85,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		// 	},
		// 	{
		// 		field: "qtdRetidasLiq24h",
		// 		displayName: "Ret Líq 24H",
		// 		width: 95,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
		// 	}
		// 	/*,{
		// 		field: "qtdPercRetidasLiq24h",
		// 		displayName: "% RL 1H",
		// 		width: 65,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		// 	},{
		// 		field: "qtdPercRetidasLiq2h",
		// 		displayName: "% RL 2H",
		// 		width: 65,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		// 	},{
		// 		field: "qtdPercRetidasLiq24h",
		// 		displayName: "% RL 24H",
		// 		width: 65,
		// 		pinned: false,
		// 		cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{ row.getProperty(col.field) | numeroOuNao }}</span></div>'
		// 	}*/
		// 	);
		// }


		return array;
	}


	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);


	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]) : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]) : fim = ontemFim();

	$scope.periodo = {
		inicio: inicio,
		fim: fim,
		min: new Date(2013, 11, 1),
		max: agora // FIXME: atualizar ao virar o dia
		/*min: mesAnterior(dat_consoli),
		max: dat_consoli*/
	};


	var $view;

	$scope.$on('$viewContentLoaded', function () {

		$view = $("#pag-resumo-dia");


		$(".aba5").css({
			'position': 'fixed',
			'left': '55px',
			'right': 'auto',
			'margin-top': '35px',
			'z-index': '1'
		});
		$(".aba6").css({
			'position': 'fixed',
			'left': '390px',
			'right': 'auto',
			'margin-top': '35px',
			'z-index': '1'
		});
		$('.navbar-inner').css('height', '70px');
		$(".botoes").css({
			'position': 'fixed',
			'left': 'auto',
			'right': '25px',
			'margin-top': '35px'
		});

	  carregaRegioes($view);
	  carregaAplicacoes($view, false, false, $scope);
	  carregaSites($view);	  
	 
	  carregaAssuntos($view, true);

		//19/03/2014
		componenteDataMaisHora($scope, $view);

		carregaRegioes($view);
		carregaSites($view);

		carregaSegmentosPorAplicacao($scope, $view, true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function () {
			carregaSegmentosPorAplicacao($scope, $view)
		});

		//habilita o checkbox agrupar para segmento
		$view.on("change", "select.filtro-segmento", function(){
			$('#porRegEDDD').removeAttr('disabled');
			$('#porRegEDDD').prop('checked', false);
		})

		carregaDDDsPorRegiao($scope, $view, true);
		// Popula lista de  ddds a partir das regioes selecionadas
		$view.on("change", "select.filtro-regiao", function () {

			$('#alinkApl').removeAttr('disabled');
			$(".filtro-aplicacao").attr('multiple', 'multiple');
			$(".filtro-assunto").attr('multiple', 'multiple');
			$(".filtro-movel-fixo").attr('multiple', 'multiple');
			$('#HFiltroED').prop('checked', false);
			$scope.filtros.isHFiltroED = false;
			$('#oueED').attr('disabled', 'disabled');
			$('#oueED').prop('checked', false);

			carregaDDDsPorRegiao($scope, $view);

		});

		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});


		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-regiao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} regiões',
			showSubtext: true
		});

		$view.on("change", "select.filtro-site", function () {
			var filtro_sites = $(this).val() || [];
			Estalo.filtros.filtro_sites = filtro_sites;
		});

		$view.on("change", "select.filtro-ddd", function () {
			var filtro_ddds = $(this).val() || [];
			Estalo.filtros.filtro_ddds = filtro_ddds;
		});

		$view.on("change", "select.filtro-regiao", function () {
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});
		
		$view.on("change", "select.filtro-assunto", function () {
			var filtro_assuntos = $(this).val() || [];
			Estalo.filtros.filtro_assunto = filtro_assuntos;
		});
		
		$view.on("change", "select.filtro-tipo", function () {
			var filtro_tipos = $(this).val() || [];
			Estalo.filtros.filtro_tipo = filtro_tipos;
		});

		//GILBERTO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});



		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function () {
			marcaTodosIndependente($('.filtro-ddd'), 'ddds')
		});

		//Bernardo 20-02-2014 Marcar todas as regiões
		$view.on("click", "#alinkReg", function () {
			marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
		});

		// Marca todos os sites
		$view.on("click", "#alinkSite", function () {
			marcaTodosIndependente($('.filtro-site'), 'sites')
		});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});
		
		// Marca todos os assunto
		$view.on("click", "#alinkAssunto", function(){ marcaTodosIndependente($('.filtro-assunto'),'assuntos')});
		

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function () {
			marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
		});




		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

			$scope.pivot = false;
			$('.chkMensal').prop('disabled', false);

			limpaProgressBar($scope, "#pag-resumo-dia");
			//22/03/2014 Testa se data início é maior que a data fim
			var testedata = testeDataMaisHora($scope.periodo.inicio, $scope.periodo.fim);
			if (testedata !== "") {
				setTimeout(function () {
					atualizaInfo($scope, testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}


			if ($scope.chkMensal === true && $scope.periodo.fim > hojeFim()) {
				setTimeout(function () {
					atualizaInfo($scope, "Operação inválida para opção MENSAL, tente selecionar um período diferente");
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				}, 500);
				return;
			}

			var filtro_ed = $scope.filtroEDSelect || [];
			var filtro_ic = $scope.filtroICSelect || [];	
			var filtro_assuntos = $view.find("select.filtro-assunto").val() || [];
			var filtro_tipos = $view.find("select.filtro-movel-fixo").val() || [];
			var filtro_movel_fixo = $view.find("select.filtro-movel-fixo").val() || [];
			var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
			var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
			//var filtro_assunto


            if (filtro_ed[0]!== undefined && filtro_ic[0]!== undefined) {
                    setTimeout(function () {
                        atualizaInfo($scope, '<font color = "Red">Filtros de estados de diálogo e itens de controle <B>NÃO</B> podem ser usados ao mesmo tempo neste relatório.</font>');
                        effectNotification();
                        $view.find(".btn-gerar").button('reset');
                        $view.find(".btn-exportar").prop("disabled", "disabled");
                    }, 500);
                    return;
			}
			
			if (filtro_assuntos.length > 0 && (filtro_ed[0]!== undefined || filtro_ic[0]!== undefined || filtro_movel_fixo[0]!== undefined)) {
                    setTimeout(function () {
                        atualizaInfo($scope, 'Filtros de Tipo, ED, IC e assunto <B>DEVEM</B> ser usados individualmente neste relatório.');
                        effectNotification();
                        $view.find(".btn-gerar").button('reset');
                        $view.find(".btn-exportar").prop("disabled", "disabled");
                    }, 500);
                    return;
			}
			if (filtro_movel_fixo.length > 0 && (filtro_ed[0]!== undefined || filtro_ic[0]!== undefined || filtro_assuntos[0]!== undefined)) {
                    setTimeout(function () {
                        atualizaInfo($scope, 'Filtros de Tipo, ED, IC e assunto <B>DEVEM</B> ser usados individualmente neste relatório.');
                        effectNotification();
                        $view.find(".btn-gerar").button('reset');
                        $view.find(".btn-exportar").prop("disabled", "disabled");
                    }, 500);
                    return;
			}
			
			// if ((filtro_ed[0]!== undefined || filtro_ic[0]!== undefined) && filtro_ddds.length > 0 || filtro_regioes.length >0) {
			// 	setTimeout(function () {
			// 		atualizaInfo($scope, "Filtros de Tipo, ED, IC, DDD e Região <B>DEVEM</B> ser usados individualmente neste relatório.");
			// 		effectNotification();
			// 		$view.find(".btn-gerar").button('reset');
			// 	}, 500);
			// 	return;
			// }

			$scope.colunas = geraColunas();
			$scope.listaDados.apply(this);
		});


		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
			$scope.porRegEDDD = false;
			$scope.chkMensal = false;
			$scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {

			iniciaFiltros();
			componenteDataMaisHora ($scope,$view,true);
			var partsPath = window.location.pathname.split("/");
			var part = partsPath[partsPath.length - 1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function () {
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash + '/';
			}, 500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		//Alex 02/04/2014
		$scope.agora = function () {
			iniciaAgora($view, $scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
			abortar($scope, "#pag-resumo-dia");
		}

		//Alex 27/02/2014
		$view.on("click", ".btn-pivot", function () {



			if ($scope.pivot === true) {
				$scope.pivot = false;
				$scope.colunas = geraColunas();
				$scope.listaDados.apply(this);

			} else {
				limpaProgressBar($scope, "#pag-resumo-dia");
				$('.chkMensal').prop('disabled', true);
				$scope.pivot = true;
				$scope.colunas = geraColunas();
				$scope.listaDadosPivot.apply(this);
			}



		});



		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	var dadosDias = [],
		dadosSegmentos = [],
		dadosEntrantes = [],
		dadosRetidas = [],
		dadosTMAs = [],
		dadosDDDs = [],
		dadosRegioes = [],
		dadosDerivadas = [],
		dadosFinalizadas = [],		
		dadosAbandonadas = [],
		dadosPercDerivadas = [],
		dadosPercFinalizadas = [],
		dadosPercAbandonadas = [],
		dadosClientesEntrantes = [],
		dadosClientesDerivados = [],
		dadosPercClientesEntrantes = [],
		dadosPercClientesDerivados = [],
		temTotaisClientes = [];



	$scope.listaDadosPivot = function () {

		$scope.colunas = [];
		$scope.dados = [];

		var temp = [];

		dadosDias.unshift("data");
		dadosEntrantes.unshift("Entrantes");
		temp.push(dadosEntrantes);
		dadosDerivadas.unshift("Derivadas");
		temp.push(dadosDerivadas);
		dadosFinalizadas.unshift("Finalizadas");
		temp.push(dadosFinalizadas);
		dadosAbandonadas.unshift("Abandonadas");
		temp.push(dadosAbandonadas);
		dadosRetidas.unshift("Retidas");
		temp.push(dadosRetidas);

		if (unique(dadosTMAs).toString() !== "") {
			dadosTMAs.unshift("TMAs");
			temp.push(dadosTMAs);
		}

		if (unique(dadosDDDs).toString() !== "") {
			dadosDDDs.unshift("DDD");
			temp.push(dadosDDDs);
		}

		if (unique(dadosRegioes).toString() !== "") {
			dadosRegioes.unshift("Região");
			temp.push(dadosRegioes);
		}

		dadosPercDerivadas.unshift("% Deriv.");
		temp.push(dadosPercDerivadas);
		dadosPercFinalizadas.unshift("% Final.");
		temp.push(dadosPercFinalizadas);
		dadosPercAbandonadas.unshift("% Aband.");
		temp.push(dadosPercAbandonadas);
		dadosClientesEntrantes.unshift("CE");
		temp.push(dadosClientesEntrantes);
		dadosClientesDerivados.unshift("CD");
		temp.push(dadosClientesDerivados);
		dadosPercClientesEntrantes.unshift("% CE");
		temp.push(dadosPercClientesEntrantes);
		dadosPercClientesDerivados.unshift("% CD");
		temp.push(dadosPercClientesDerivados);



		for (var i = 0; i < temp.length; i++) {
			var item = {};
			for (var j = 0; j < dadosDias.length; j++) {

				if (typeof dadosDias[j] === "object") {

					item[dadosDias[j].data] = temp[i][j];
				} else {

					item[dadosDias[j]] = temp[i][j];
				}


			}
			$scope.dados.push(item);
		}

		for (var j = 0; j < dadosDias.length; j++) {
			if (j !== 0) {
				$scope.colunas.push({
					field: dadosDias[j].data,
					displayName: dadosDias[j].dataBR,
					width: 127,
					pinned: false,
					cellClass: "grid-align"
				});
			} else {
				$scope.colunas.push({
					field: dadosDias[j],
					displayName: "Data",
					width: 120,
					pinned: true
				});
			}
		}

		retornaStatusQuery($scope.dados.length, $scope);
		//$scope.$apply();

	};



	// Lista chamadas conforme filtros
	$scope.listaDados = function () {

		dadosDias = [],
		dadosSegmento = [],
			dadosEntrantes = [],
			dadosRetidas = [],
			dadosTMAs = [],
			dadosDDDs = [],
			dadosRegioes = [],
			dadosDerivadas = [],
			dadosFinalizadas = [],			
			dadosAbandonadas = [],
			dadosPercDerivadas = [],
			dadosPercFinalizadas = [],
			dadosPercAbandonadas = [],
			dadosClientesEntrantes = [],
			dadosClientesDerivados = [],
			dadosPercClientesEntrantes = [],
			dadosPercClientesDerivados = [],
			temTotaisClientes = [];
			dadosqtdRetidasLiq1h=[];
			dadosqtdRetidasLiq2h=[];
			dadosqtdRetidasLiq24h=[];


		$globals.numeroDeRegistros = 0;

		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", true);
		$btn_exportar_csv.prop("disabled", true);
		$btn_exportar_dropdown.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
			data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_assuntos = $view.find("select.filtro-assunto").val() || [];
		var filtro_tipos = $view.find("select.filtro-movel-fixo").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ed = $scope.filtroEDSelect || [];
		if (filtro_ed[0] !== undefined) {
			$scope.filtros.isHFiltroED = true;
		} else {
			$scope.filtros.isHFiltroED = false;
		}
		var filtro_ic = $scope.filtroICSelect || [];
		if(filtro_ic[0]!== undefined){ $scope.filtros.isHFiltroIC = true;}else{$scope.filtros.isHFiltroIC = false;}
		var filtro_ed = $scope.filtroEDSelect || [];
		if(filtro_ed[0]!== undefined){ $scope.filtros.isHFiltroED = true;}else{$scope.filtros.isHFiltroED = false;}



		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
		+ " até " + formataDataBR($scope.periodo.fim);
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
		if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
		if (filtro_ic.length > 0) { $scope.filtros_usados += " ICs: " + filtro_ic; }
		if (filtro_assuntos.length > 0) { $scope.filtros_usados += " Assuntos: " + filtro_assuntos; }
		if (filtro_tipos.length > 0) { $scope.filtros_usados += " Tipos: " + filtro_tipos; }
		


		if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
		  var options = [];
		  var v = [];
		  filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
		  unique(v).forEach((function (filtro_ddds) {
			return function (codigo) {
			  var ddd = cache.ddds.indice[codigo];
			  filtro_ddds.push(codigo);
			};
		  })(filtro_ddds));
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		}else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		  $scope.filtros_usados += " DDDs: " + filtro_ddds;
		}else if(filtro_regioes.length === 0 && filtro_ddds.length !== 0){
			$scope.filtros_usados += " DDDs: " + filtro_ddds;
		}

		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";
		var tabela = "";
		var GrupoApl = "";
		var tabelaMensal = "";

		var iit = "";

		var tlv = "";
		var mtlv = "+ ISNULL(QtdTransfTLV, 0)";
		var tempoiit = "";
		var tempotlv = "";
		var mtempotlv = " + ISNULL(TempoTransfTLV, 0)";

		if ($scope.iit === false) {
			iit = " - ISNULL(QtdTransfURA, 0)";
			tempoiit = " - ISNULL(TempoTransfURA, 0)";
		}
		if ($scope.tlv === false) {
			tlv = " - ISNULL(QtdTransfTLV, 0)";
			mtlv = "";
			tempotlv = " - ISNULL(TempoTransfTLV, 0)";
		}

		var tempoEncerradas = "TempoAbandonadas+TempoFinalizadas+TempoDerivadas+TempoTransfURA+TempoTransfTLV";
		//var tempoEncerradas = "TempoAbandonadas+TempoFinalizadas+TempoDerivadas+TempoTransfURA";
		//waiting for Gabriel

		if (typeof (filtro_aplicacoes) === "string") filtro_aplicacoes = [filtro_aplicacoes];


		if (cache.grupoaplicacoes.indexOf(filtro_aplicacoes.sort().join()) >= 0) {
			GrupoApl = "GrupoApl";
		}


		if ($('.chkMensal').prop('checked') === true) {
			tabelaMensal = "Mes2";
		}

		//02/06/2014 Visão por ddd(s) ou por região(ões)
		// Apenas fazer o join com clientes únicos se sites e segmentos forem todos selecionados,
		// e aplicações forem uma ou todas selecionadas
		if (filtro_regioes.length > 0 || filtro_ddds.length > 0) {


		  var incluiCampos;
		  if ($scope.porRegEDDD) {
			incluiCampos = function () { return ""; };
			$scope.porDDD = "DDDRegiao";
		  } else if (original_ddds.length !== 0) {
			incluiCampos = function () { return ", ddd"; };
			$scope.porDDD = "DDD";
		  } else {
			incluiCampos = function () { return ", regiao"; };
			$scope.porDDD = "Regiao";
			}

			//Brunno
			function segmentovarivel(vseg) {
				if($('#porRegEDDD').prop('checked') === false && filtro_segmentos.length > 0){
				  return seg = " , "+vseg;
			  }else{
				  return seg = "";
			  }
			}

		  $scope.filtros.isHFiltroIC ? tabela = 'resumoICDia' : tabela =  filtro_tipos.length > 0 ? 'resumoFixoMovel' : $scope.filtros.isHFiltroED? tabela = 'ResumoDDDxEDDia': 'resumoAnalitico2';

		  // Nenhuma aplicação selecionada (= todas as aplicações)
		  // Exibir total de clientes únicos independente de aplicação (CodAplicacao = '')
		  if ( filtro_aplicacoes.length === 0 && filtro_sites.length === 0 && !$scope.filtros.isHFiltroIC && filtro_assuntos.length == 0 && filtro_tipos.length == 0) {
			  
			  $scope.parcial = false;


			  stmt = db.use +
				  " with t as (" +
				  "   SELECT CONVERT(DATE,RA.DATREFERENCIA) as DatReferencia, DDD,"+
				  "   SUM(RA.QtdChamadas" + iit + tlv +") as c," +
				  "   SUM(RA.QtdFinalizadas) as f, SUM(RA.QtdDerivadas"+mtlv+") as d, SUM(RA.QtdAbandonadas) as a," +
				  "   ISNULL(sum(" + (tabela === "resumoAnalitico2" ? tempoEncerradas : "CAST(TempoEncerradas AS BIGINT)") + "" + tempoiit + tempotlv + "),0) as tempo " +
				  "   FROM " + db.prefixo + "" + tabela + " as RA" +
				  "   WHERE 1 = 1" +
				  "     AND CONVERT(DATE,RA.DATREFERENCIA) >= '" + formataData(data_ini) + "'" +
				  "     AND CONVERT(DATE,RA.DATREFERENCIA') <= '" + formataData(data_fim) + "'";
			  stmt += restringe_consulta("RA.ddd", filtro_ddds, true);

			  if (tabela === "resumoAnalitico2") {
				  stmt += restringe_consulta("RA.CodSegmento", filtro_segmentos, true);
			  } else {
				  stmt += restringe_consulta2('RA.CodSegmento', filtro_segmentos, true);
			  }
			  
			  if (tabela === "resumoFixoMovel") {
				  stmt += restringe_consulta("RA.CodTipo", filtro_tipos, true);          
			  }
			  
			  stmt += " GROUP BY CONVERT(DATE,RA.DATREFERENCIA'), DDD";
			  
			  stmt += " )";
			  
			  stmt += "select t.DatReferencia "+
			  			incluiCampos();
			  if ($scope.porDDD === "DDDRegiao" || $scope.porDDD === "Regiao") {
				  stmt += " , sum(t.c) as c, sum(t.f) as f, sum(t.d) as d, sum(t.a) as a, sum(TCU.QtdClientes) as qc, sum(TCU.QtdClientesDerivados) as qcd ";
			  } else {
				  stmt += " , t.c, t.f, t.d, t.a, TCU.QtdClientes as qc, TCU.QtdClientesDerivados as qcd ";
			  }
			  stmt += " from t" +
				  " left outer join " + db.prefixo + "TotaisClientesUnicos" + GrupoApl + tabelaMensal + " as TCU" +
				  "   on t.DatReferencia = TCU.DatReferencia and TCU.CodAplicacao = '' and t.DDD = TCU.CodDDD";
			  stmt += restringe_consulta2("TCU.CodSegmento", filtro_segmentos, true);


			  if (original_ddds.length === 0) {
				  stmt += " LEFT OUTER JOIN UF_DDD as UD on t.DDD = UD.DDD";
			  }
			  
			  if ($scope.porDDD === "Regiao") {
				  stmt += " GROUP BY t.DatReferencia, regiao" ;
			  } else if ($scope.porDDD === "DDDRegiao") {
				  stmt += " GROUP BY t.DatReferencia";
			  }

			  stmt += " ORDER BY t.DatReferencia" +
				  incluiCampos();

			  // Uma ou mais aplicações
			  // Exibir clientes únicos apenas se uma única aplicação for selecionada
		  } else {
			  if($('#porRegEDDD').prop('checked') === false && filtro_segmentos.length > 0){
				  seg = $scope.filtros.isHFiltroED? ',RA.COD_SEGMENTO': ',RA.CODSEGMENTO';
			  }else{
				  seg = "";
			  }
			  $scope.parcial = false;
			  stmt = db.use +
				  " SELECT CONVERT(DATE,RA."
				$scope.filtros.isHFiltroED? stmt+='DAT_REFERENCIA': stmt+='DATREFERENCIA'  
				stmt+= ") as DatReferencia" + seg +
				  incluiCampos() +
				  "   , SUM(RA.QtdChamadas" + iit + tlv +") as c" +
				  "   , SUM(RA.QtdFinalizadas) as f, SUM(RA.QtdDerivadas"+mtlv+") as d, SUM(RA.QtdAbandonadas) as a," +
				  " ISNULL(sum(" + ((tabela === "resumoAnalitico2" || tabela === "resumoFixoMovel") ? tempoEncerradas : "CAST(TempoEncerradas AS BIGINT)") + "" + tempoiit + tempotlv + "),0) as tempo "

			  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 && !$scope.porRegEDDD || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && !$scope.porRegEDDD && GrupoApl !== "") {
				  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 && !$scope.porRegEDDD && $scope.porDDD !== "Regiao" && tabela !== "resumoICDia"){
				  //if ($scope.porDDD !== "Regiao" && tabela !== "resumoICDia") {
					  stmt += ", TCU.QtdClientes as qc, TCU.QtdClientesDerivados as qcd";
				  }
			  }
			  stmt += " FROM " + db.prefixo + "" + tabela + " as RA";
			  if (original_ddds.length === 0) {
				  stmt += " LEFT OUTER JOIN UF_DDD as UD on RA.DDD = UD.DDD";
			  }


			  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 && !$scope.porRegEDDD && !$scope.filtros.isHFiltroIC || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && !$scope.porRegEDDD && !$scope.filtros.isHFiltroIC && GrupoApl !== "" && filtro_assuntos.length > 1) {
				  if ($scope.porDDD !== "Regiao") {
					  stmt += " left outer join " + db.prefixo + "TotaisClientesUnicos" + tabelaMensal + GrupoApl + " as TCU" +
						  " on CONVERT(DATE,RA.DatReferencia) = TCU.DatReferencia"
					  if (GrupoApl === "") {
						  stmt += " and ra."
						  $scope.filtros.isHFiltroED? stmt+='COD_APLICACAO': stmt+='CODAPLICACAO'
						  stmt+=" = TCU.CodAplicacao"
					  }
					  stmt += " AND RA.DDD = TCU.CodDDD";
					  if (GrupoApl !== "") {
						  stmt += " and TCU.GrupoApl ='" + filtro_aplicacoes.sort().join() + "'";
					  }
					  stmt += restringe_consulta2("TCU.CodSegmento", filtro_segmentos, true);
				  }
			  }
			  stmt += " WHERE 1 = 1" +
				  " AND CONVERT(DATE,RA."
				  $scope.filtros.isHFiltroED? stmt+='DAT_REFERENCIA': stmt+='DATREFERENCIA'
				  stmt +=") >= '" + formataData(data_ini) + "'" +
				  " AND CONVERT(DATE,RA."
				  $scope.filtros.isHFiltroED? stmt +='DAT_REFERENCIA': stmt+='DATREFERENCIA'
				  stmt+=") <= '" + formataData(data_fim) + "'";
			  stmt += restringe_consulta($scope.filtros.isHFiltroED? 'RA.COD_APLICACAO': 'RA.CODAPLICACAO', filtro_aplicacoes, true);
			  stmt += restringe_consulta($scope.filtros.isHFiltroED? 'RA.COD_SITE': 'RA.CODSITE', filtro_sites, true);
			  stmt += restringe_consulta("RA.ddd", filtro_ddds, true);
			  stmt += restringe_consulta($scope.filtros.isHFiltroED? 'RA.COD_SEGMENTO': 'RA.CODSEGMENTO', filtro_segmentos, true);
			  $scope.filtros.isHFiltroED? stmt += restringe_consulta4("RA.Cod_Estado_Dialogo", filtro_ed, $scope.oueED): ''
			  
			  if (tabela === "resumoFixoMovel") {
				  stmt += restringe_consulta("RA.CodTipo", filtro_tipos, true);          
			  }

			  if ($scope.filtros.isHFiltroIC) {
				  stmt += restringe_consulta4("RA.CodIC", filtro_ic, $scope.oueIC);
			  }

			  stmt += " GROUP BY CONVERT(DATE,RA."
			  $scope.filtros.isHFiltroED? stmt+='DAT_REFERENCIA': stmt+='DATREFERENCIA'
			  stmt+=")" + seg +
				  incluiCampos();
			  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 && !$scope.porRegEDDD || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && !$scope.porRegEDDD && GrupoApl !== "") {
				  //if ($scope.porDDD !== "Regiao" && tabela !== "resumoICDia") {
				  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 && !$scope.porRegEDDD && $scope.porDDD !== "Regiao" && tabela !== "resumoICDia"){
					  
					  stmt += ", TCU.QtdClientes, TCU.QtdClientesDerivados";
				  }
			  }
			  stmt += " ORDER BY CONVERT(DATE,RA."
			  $scope.filtros.isHFiltroED? stmt+='DAT_REFERENCIA': stmt+='DATREFERENCIA'
			  stmt+=") " + seg +
				  incluiCampos();
		  }


		  //02/06/2014 Visão sem considerar região(ões) e/ou ddd(s) - - apenas fazer o join com clientes únicos se sites, aplicações e segmentos forem um ou todos selecionados
	  } else if (filtro_regioes.length === 0) {
		  
		  

		  if ($scope.filtros.isHFiltroED) {
			  //19-03/2015 Parcial dia a dia
			  var diaAtual = "D";
			  if (formataData(data_ini) === formataData(hoje()) || formataData(data_fim) === formataData(hoje())) {
				  diaAtual = "";
				  $scope.parcial = true;
			  } else {
				  diaAtual = "D";
				  $scope.parcial = false;
			  }
			  
			  iit = " - ISNULL(QtdChamadasIIT, 0)";
				segmentovarivel("RE.Cod_Segmento as CodSegmento");

			  stmt = db.use +
				  " Select CONVERT(DATE,RE.DAT_REFERENCIA) as DatReferencia " + seg +
				  " , ISNULL(sum(QtdChamadas" + iit + tlv +"),0) as c, " +
				  " ISNULL(sum(QtdFinalizadas),0) as f, " +
				  " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, " +
				  " ISNULL(sum(QtdAbandonadas),0) as a, " +
				  " ISNULL(sum(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + "),0) as tempo "
					stmt += " FROM " + db.prefixo + "Resumo_EncerramentoURAVOZ" + diaAtual + " as RE "
					stmt += " WHERE 1 = 1 " +
				  " AND CONVERT(DATE,re.Dat_Referencia) >= '" + formataData(data_ini) + "' " +
				  " AND CONVERT(DATE,re.Dat_Referencia) <= '" + formataData(data_fim) + "' " +
				  restringe_consulta("re.Cod_Aplicacao", filtro_aplicacoes, true) +
				  restringe_consulta("re.Cod_Site", filtro_sites, true) +
				  restringe_consulta2("re.Cod_Segmento", filtro_segmentos, true)
					stmt += restringe_consulta("Re.ddd", filtro_ddds, true);
			  stmt += restringe_consulta4("re.Cod_Estado_Dialogo", filtro_ed, $scope.oueED) +
				  " Group by CONVERT(DATE,re.Dat_Referencia)" + seg +
				  " ORDER BY CONVERT(DATE,re.Dat_Referencia)" + seg;
				  
			   } else if ($scope.filtros.isHFiltroIC) {
					segmentovarivel("re.CodSegmento")

			  stmt = db.use +
				  " Select CONVERT(DATE,RE.DATREFERENCIA) as DatReferencia " + seg +
				  " , ISNULL(sum(QtdChamadas" + iit + tlv +"),0) as c, " +
				  " ISNULL(sum(QtdFinalizadas),0) as f, " +
				  " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, " +
				  " ISNULL(sum(QtdAbandonadas),0) as a, " +
				  " ISNULL(sum(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + "),0) as tempo "

				  +
				  " FROM " + db.prefixo + "ResumoICDia as RE " +
				  " WHERE 1 = 1 " +
				  " AND CONVERT(DATE,re.DatReferencia) >= '" + formataData(data_ini) + "' " +
				  " AND CONVERT(DATE,re.DatReferencia) <= '" + formataData(data_fim) + "' " +
				  restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true) +
				  restringe_consulta("re.CodSite", filtro_sites, true) +
				  restringe_consulta("re.CodSegmento", filtro_segmentos, true)
			  stmt += restringe_consulta4("RE.CodIC", filtro_ic, $scope.oueIC) +
				  " Group by CONVERT(DATE,re.DatReferencia)" + seg +
				  " ORDER BY CONVERT(DATE,re.DatReferencia)";



		  } else if (filtro_assuntos.length > 0) {
				segmentovarivel("re.CodSegmento");

			  stmt = db.use +
				  " Select CONVERT(DATE,RE.DATREFERENCIA) as DatReferencia " + seg  +
				  " , ISNULL(sum(QtdChamadas" + iit + tlv +"),0) as c, " +
				  " ISNULL(sum(QtdFinalizadas),0) as f, " +
				  " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, " +
				  " ISNULL(sum(QtdAbandonadas),0) as a, " +
				  " ISNULL(sum(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + "),0) as tempo "

				  +
				  " FROM " + db.prefixo + "resumoDDDxAssunto as RE " +
				  " WHERE 1 = 1 " +
				  " AND CONVERT(DATE,re.DatReferencia) >= '" + formataData(data_ini) + "' " +
				  " AND CONVERT(DATE,re.DatReferencia) <= '" + formataData(data_fim) + "' " +
				  restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true) +
				  restringe_consulta("re.CodSite", filtro_sites, true) +
				  restringe_consulta("re.CodSegmento", filtro_segmentos, true)
			  stmt += restringe_consulta("RE.Assunto", filtro_assuntos, true) +
				  " Group by CONVERT(DATE,re.DatReferencia)" + seg +
				  " ORDER BY CONVERT(DATE,re.DatReferencia)";



		  } else if (filtro_tipos.length > 0 ) {
				segmentovarivel("RE.CodSegmento")

			  stmt = db.use +
				  " Select CONVERT(DATE,RE.DATREFERENCIA) as DatReferencia " + seg +
				  " , ISNULL(sum(QtdChamadas" + iit + tlv +"),0) as c, " +
				  " ISNULL(sum(QtdFinalizadas),0) as f, " +
				  " ISNULL(sum(QtdDerivadas"+mtlv+"),0) as d, " +
				  " ISNULL(sum(QtdAbandonadas),0) as a, " +
				  " ISNULL(sum(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + "),0) as tempo "

				  +
				  " FROM " + db.prefixo + "resumoFixoMovel as RE " +
				  " WHERE 1 = 1 " +
				  " AND CONVERT(DATE,re.DatReferencia) >= '" + formataData(data_ini) + "' " +
				  " AND CONVERT(DATE,re.DatReferencia) <= '" + formataData(data_fim) + "' " +
				  restringe_consulta("re.CodAplicacao", filtro_aplicacoes, true) +
				  restringe_consulta("re.CodSite", filtro_sites, true) +
				  restringe_consulta("re.CodSegmento", filtro_segmentos, true)
			  stmt += restringe_consulta("RE.CodTipo", filtro_tipos, true) +
				  " Group by CONVERT(DATE,re.DatReferencia)" + seg +
				  " ORDER BY CONVERT(DATE,re.DatReferencia)";
				  
		  } else {
			  console.log("Filtro Tipos: "+filtro_tipos.length);
			  // Nenhuma aplicação selecionada (= todas as aplicações)
			  // Exibir total de clientes únicos idependente de aplicação (CodAplicacao = '')
			  if ([filtro_aplicacoes].length === 0 && filtro_sites.length === 0) {

				  
				  //19-03/2015 Parcial dia a dia
				  var diaAtual = "";
				  if (formataData(data_ini) === formataData(hoje()) || formataData(data_fim) === formataData(hoje())) {
					  diaAtual = "Hora";
					  $scope.parcial = true;
				  } else {
					  diaAtual = "";
					  $scope.parcial = false;
				  }
					
				  stmt = db.use +
					  " with t as (" +
					  "   SELECT CONVERT(DATE,DAT_REFERENCIA) as DatReferencia"
					  "   , SUM(QtdChamadas" + iit + tlv +") as c," +
					  "   SUM(QtdFinalizadas) as f, SUM(QtdDerivadas"+mtlv+") as d, SUM(QtdAbandonadas) as a," +
					  //"   SUM(ISNULL(QtdRetidasLiq1h,0)) as rl1, SUM(ISNULL(QtdRetidasLiq2h,0)) as rl2, SUM(ISNULL(QtdRetidasLiq24h,0)) as rl24,"+
					  "   SUM(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + ") as tempo" +
					  "   FROM " + db.prefixo + "resumodesempenhogeral" + diaAtual + "" +
					  "   WHERE 1 = 1" +
					  "     AND CONVERT(DATE,Dat_Referencia) >= '" + formataData(data_ini) + "'" +
					  "     AND CONVERT(DATE,Dat_Referencia) <= '" + formataData(data_fim) + "'" +
					  "     AND Cod_Segmento = ''" +
					  "   GROUP BY CONVERT(DATE,DAT_REFERENCIA)";

					segmentovarivel("CodSegmento")
				  stmt += " ), tcu as (" +
					  "   SELECT DatReferencia" + seg +
					  "   ,   SUM(QtdClientes) as qc, SUM(QtdClientesDerivados) as qcd" +
					  "   FROM " + db.prefixo + "TotaisClientesUnicos" + GrupoApl + tabelaMensal + "" +
					  "   WHERE 1 = 1" +
					  "     AND DatReferencia >= '" + formataData(data_ini) + "'" +
					  "     AND DatReferencia <= '" + formataData(data_fim) + "'" +
					  "     AND CodAplicacao = ''";
				  stmt += restringe_consulta2("CodSegmento", filtro_segmentos, true);
				  stmt += "   GROUP BY DatReferencia" + seg +
					  " )" +
					  " select t.DatReferencia, t.c, t.f, t.d, t.a, t.tempo, tcu.qc, tcu.qcd";
					  
					  if($('#porRegEDDD').prop('checked') === false && filtro_segmentos.length > 0){
						  if(filtro_segmentos.length > 0){
							  stmt += ", t.CodSegmento";
						  }else{
							  stmt += "";
						  }
					  }

					  stmt += " from t" +
					  " left outer join tcu" +
					  "   on t.DatReferencia = tcu.DatReferencia" +
					  " ORDER BY t.DatReferencia";
				  // Uma ou mais aplicações
				  // Exibir clientes únicos apenas se uma única aplicação for selecionada
			  } else {

				  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && GrupoApl !== "") {
					  
						segmentovarivel("CodSegmento")

					  stmt = db.use +
						  " with tcu as (" +
						  "   SELECT DatReferencia" + seg +
						  "      , SUM(QtdClientes) as qc, SUM(QtdClientesDerivados) as qcd" +
						  "   FROM " + db.prefixo + "TotaisClientesUnicos" + GrupoApl + tabelaMensal + "" +
						  "   WHERE 1 = 1" +
						  "     AND DatReferencia >= '" + formataData(data_ini) + "'" +
						  "     AND DatReferencia <= '" + formataData(data_fim) + "'";
					  if (GrupoApl !== "") {
						  stmt += "and GrupoApl ='" + filtro_aplicacoes.sort().join() + "'";
					  } else {
						  stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
					  }
					  stmt += restringe_consulta2("CodSegmento", filtro_segmentos, true);
					  stmt += "   GROUP BY DatReferencia" + seg +
						  " )";
				  }

				  //19-03/2015 Parcial dia a dia
				  var diaAtual = "";
				  if (formataData(data_ini) === formataData(hoje()) || formataData(data_fim) === formataData(hoje())) {
					  diaAtual = "Hora";
					  $scope.parcial = true;
				  } else {
					  diaAtual = "";
					  $scope.parcial = false;
				  }
				  
				  stmt = (stmt == "") ? db.use + " with" : stmt + ",";
				 
				  
				  stmt += " rdg as (" +
					  " SELECT CONVERT(DATE,DAT_REFERENCIA) as DatReferencia, ";
					  
						  if($('#porRegEDDD').prop('checked') === false && filtro_segmentos.length > 0){
							  stmt += "Cod_Segmento as CodSegmento, ";
							  seg1 = " ,Cod_Segmento";
							  seg_group = " ,rdg.CodSegmento"
						  }else{
							   stmt += "";
							   seg_group = "";
							   seg1 = "";
						  }
					  
					  stmt += "   SUM(QtdChamadas" + iit + tlv +") as c," +
					  //QtdChamadas" + iit + tlv +"
					  "   SUM(QtdFinalizadas) as f, SUM(QtdDerivadas" + mtlv + ") as d, SUM(QtdAbandonadas) as a," +
					  //"	  SUM(ISNULL(QtdRetidasLiq1h,0)) as rl1, SUM(ISNULL(QtdRetidasLiq2h,0)) as rl2, SUM(ISNULL(QtdRetidasLiq24h,0)) as rl24,"+
					  "   SUM(CAST(TempoEncerradas AS BIGINT)" + tempoiit + tempotlv + ") as tempo" +
					  " FROM " + db.prefixo + "resumodesempenhogeral" + diaAtual + "" +
					  " WHERE 1 = 1" +
					  " AND CONVERT(DATE,Dat_Referencia) >= '" + formataData(data_ini) + "'" +
					  " AND CONVERT(DATE,Dat_Referencia) <= '" + formataData(data_fim) + "'" +
					  restringe_consulta("Cod_Aplicacao", filtro_aplicacoes, true) +
					  restringe_consulta("Cod_Site", filtro_sites, true) +
					  restringe_consulta2("Cod_Segmento", filtro_segmentos, true) + 
					  " GROUP BY CONVERT(DATE,Dat_Referencia)" + seg1;
				  console.log("Filtro_sites: " + filtro_sites);
				  stmt += ")";

				  stmt += " SELECT rdg.DatReferencia, rdg.c, rdg.f, rdg.d, rdg.a, rdg.tempo" + seg_group; //,rdg.rl1,rdg.rl2,rdg.rl24
				  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && GrupoApl !== "") {
					  stmt += ", tcu.qc, tcu.qcd";
				  }

				  stmt += " FROM rdg";
				  if (filtro_sites.length === 0 && filtro_aplicacoes.length === 1 || filtro_sites.length === 0 && filtro_aplicacoes.length > 1 && GrupoApl !== "") {
					  stmt += " left outer join tcu on rdg.DatReferencia = tcu.DatReferencia";
				  }
				  stmt += " ORDER BY rdg.DatReferencia" + seg_group;

			  }
		  }

		  $scope.porDDD = null;
	  }

		executaQuery = executaQuery2;

		log(stmt);
		$scope.filtros_usados += $scope.parcial ? " REGISTRO PARCIAL" : "";



		var stmtCountRows = stmtContaLinhas(stmt);

		// Alex 24/02/2014 Contador de linhas para auxiliar progress bar
		function contaLinhas(columns) {
			$globals.numeroDeRegistros = columns[0].value;
		}

		// 02/06/2014 Visão por ddd(s) e regiões(s)
		function executaQuery2(columns) {

			var data_hora = columns["DatReferencia"].value,
				datReferencia = typeof columns[0].value === 'string' ? formataDataBRString(data_hora) : formataDataBR(data_hora),
				segmentos = columns["CodSegmento"] !== undefined ? (columns["CodSegmento"].value === null ? segmento = "ND" : cache.segs.indice[columns["CodSegmento"].value].nome) : undefined,
				qtdDerivadas = +columns["d"].value,
				qtdFinalizadas = +columns["f"].value,				
				qtdAbandonadas = +columns["a"].value,
				qtdEntrantes = +columns["c"].value,
				qtdRetidas = qtdFinalizadas + qtdAbandonadas,
				tma = qtdEntrantes !== 0 ? columns["tempo"] !== undefined ?
				((+columns["tempo"].value) / qtdEntrantes).toFixed(2) :
				undefined: 0,
				ddd = columns["ddd"] !== undefined ? +columns["ddd"].value : undefined,
				regiao = columns["regiao"] !== undefined ? (columns["regiao"].value === null ? regiao = "ND" : columns["regiao"].value) : undefined,
				qtdPercDerivadas = qtdEntrantes !== 0 ? (100 * qtdDerivadas / qtdEntrantes).toFixed(2): 0,
				qtdPercFinalizadas = qtdEntrantes !== 0 ? (100 * qtdFinalizadas / qtdEntrantes).toFixed(2): 0,				
				qtdPercAbandonadas = qtdEntrantes !== 0 ? (100 * qtdAbandonadas / qtdEntrantes).toFixed(2):0,
				qtdPercRetidas = qtdEntrantes !== 0 ? (100 * qtdRetidas / qtdEntrantes).toFixed(2):0,
				qtdClientesEntrantes = columns["qc"] === undefined ? "NA" : columns["qc"].value === null ? "ND" : +columns["qc"].value,
				qtdClientesDerivados = columns["qcd"] === undefined ? "NA" : columns["qcd"].value === null ? "ND" : +columns["qcd"].value,
				qtdPercClientesEntrantes = qtdClientesEntrantes,
				qtdPercClientesDerivados = qtdClientesDerivados;
			if (typeof qtdClientesEntrantes === "number" && qtdEntrantes > 0) {
				qtdPercClientesEntrantes = (100 * qtdClientesEntrantes / qtdEntrantes).toFixed(2);
				qtdPercClientesDerivados = (100 * qtdClientesDerivados / qtdEntrantes).toFixed(2);
			}
			//qtdRetidasLiq1h = columns["rl1"].value !== undefined ? +columns["rl1"].value : 0,
			//qtdRetidasLiq2h = columns["rl2"].value !== undefined ? +columns["rl2"].value : 0,
			//qtdRetidasLiq24h = columns["rl24"].value !== undefined ? +columns["rl24"].value : 0,
				//qtdPercRetidasLiq1h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq1h / qtdEntrantes).toFixed(2):0,
				//qtdPercRetidasLiq2h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq2h / qtdEntrantes).toFixed(2):0,
				//qtdPercRetidasLiq24h = qtdEntrantes !== 0 ? (100 * qtdRetidasLiq24h / qtdEntrantes).toFixed(2):0,
			

			$scope.dados.push({
				data_hora: data_hora,
				datReferencia: datReferencia,
				segmentos: segmentos,
				qtdDerivadas: qtdDerivadas,
				qtdFinalizadas: qtdFinalizadas,				
				qtdAbandonadas: qtdAbandonadas,
				qtdEntrantes: qtdEntrantes,
				qtdRetidas: qtdRetidas,
				tma: tma,
				ddd: ddd,
				regiao: regiao,
				qtdPercDerivadas: qtdPercDerivadas,
				qtdPercFinalizadas: qtdPercFinalizadas,				
				qtdPercAbandonadas: qtdPercAbandonadas,
				qtdPercRetidas: qtdPercRetidas,
				qtdClientesEntrantes: qtdClientesEntrantes,
				qtdClientesDerivados: qtdClientesDerivados,
				qtdPercClientesEntrantes: qtdPercClientesEntrantes,
				qtdPercClientesDerivados: qtdPercClientesDerivados,
				//qtdRetidasLiq1h:qtdRetidasLiq1h,
				//qtdRetidasLiq2h:qtdRetidasLiq2h,
				//qtdRetidasLiq24h:qtdRetidasLiq24h,
				//qtdPercRetidasLiq1h:qtdPercRetidasLiq1h,
				//qtdPercRetidasLiq2h:qtdPercRetidasLiq2h,
				//qtdPercRetidasLiq24h:qtdPercRetidasLiq24h,
				temTotaisClientes: typeof qtdClientesEntrantes === "number"
			});

			var a = [];
			a.push(datReferencia);
			if(segmentos !== undefined && segmentos !== null){
				a.push(segmentos);
			}
			
			if (ddd !== undefined) {
				a.push(ddd);
			} else {
				if (regiao !== undefined) {
					a.push(regiao);
				}
			}

			a.push(
				qtdEntrantes,
				qtdDerivadas,
				qtdFinalizadas,
				qtdAbandonadas,
				qtdRetidas
			)

			//if (ddd === undefined || regiao === undefined) 
			a.push(tma);

			a.push(
				qtdPercDerivadas,
				qtdPercFinalizadas,				
				qtdPercAbandonadas,
				qtdPercRetidas,
				qtdClientesEntrantes,
				qtdClientesDerivados,
				qtdPercClientesEntrantes,
				qtdPercClientesDerivados
				//qtdRetidasLiq1h,
				//qtdRetidasLiq2h,
				//qtdRetidasLiq24h
			)

			$scope.csv.push(a);

			if (typeof data_hora === 'string') {
				dadosDias.push({
					dataBR: formataDataBRString(data_hora),
					data: formataData2String(data_hora)
				});
			} else {
				dadosDias.push({
					dataBR: formataDataBR(data_hora),
					data: formataData2(data_hora)
				});
			}

			dadosSegmento.push(segmentos);
			dadosDDDs.push(ddd);
			dadosRegioes.push(regiao);
			dadosTMAs.push(tma);
			dadosDerivadas.push(qtdDerivadas);
			dadosFinalizadas.push(qtdFinalizadas);			
			dadosAbandonadas.push(qtdAbandonadas);
			dadosEntrantes.push(qtdEntrantes);
			dadosRetidas.push(qtdRetidas);
			dadosPercDerivadas.push(qtdPercDerivadas);
			dadosPercFinalizadas.push(qtdPercFinalizadas);			
			dadosPercAbandonadas.push(qtdPercAbandonadas);
			dadosClientesEntrantes.push(qtdClientesEntrantes);
			dadosClientesDerivados.push(qtdClientesDerivados);
			dadosPercClientesEntrantes.push(qtdPercClientesEntrantes);
			dadosPercClientesDerivados.push(qtdPercClientesDerivados);
			temTotaisClientes.push(typeof qtdClientesEntrantes === "number");
			//dadosqtdRetidasLiq1h.push(qtdRetidasLiq1h);
			//dadosqtdRetidasLiq2h.push(qtdRetidasLiq2h);
			//dadosqtdRetidasLiq24h.push(qtdRetidasLiq24h);


			//atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}
		}



		db.query(stmt, executaQuery, function (err, num_rows) {

			var complemento = "";

			//TotaisClientesUnicos
			if (stmt.match('TotaisClientesUnicos') !== null) complemento += " clientes únicos";

			if ($scope.porRegEDDD === true) {
				$('#pag-resumo-dia table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1230px',
					'margin-bottom': '100px'
				});
			} else {
				$('#pag-resumo-dia table').css({
					'margin-left': 'auto',
					'margin-right': 'auto',
					'margin-top': '100px',
					'max-width': '1310px',
					'margin-bottom': '100px'
				});
			}

			console.log("Executando query-> " + stmt + " " + num_rows);

			var datFim = "";
			if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 > limiteDias) {
				datFim = " Botão transpor habilitado para intervalos de " + limiteDias + " dias.";
			}



			// GILBERTO 09/07/2014 -- agrupar por MÊS
			// ________________________________________________________
			// ________________________________________________________
			// crio uma nova propriedade em cada objeto do array dados, chamado comp, para ser usado como critério de agrupamento
			// neste caso específico, pego o mês e ano da string (caractere de 3 a 10) de data.

			if ($('.chkMensal').prop('checked') === true) {


				if (filtro_regioes.length === 0 || $('#porRegEDDD').prop('checked') === true) {

					$scope.dados.forEach(function (d) {
						d.comp = d.datReferencia.substr(3, 10);
					});


					var objetoAgrupado = _.groupBy($scope.dados, 'comp');

					$scope.dadosMes = [];

					complemento += " mensal";

					for (var m in objetoAgrupado) {

						if (Object.keys(objetoAgrupado)[0] === m) complemento += " " + m;
						if (Object.keys(objetoAgrupado).length > 1 && Object.keys(objetoAgrupado)[Object.keys(objetoAgrupado).length - 1] === m) complemento += " " + m;





						var mes = objetoAgrupado[m].reduce(function (a, b) {
							
							var testeAdicional = formataDataHora(new Date(objetoAgrupado[m][0].data_hora + " 00:00:00")) === formataDataHora(primeiroDiaDoMes(new Date(objetoAgrupado[m][0].data_hora + " 00:00:00")));
							
							return {

								data_hora: a.data_hora,
								datReferencia: b.comp,
								qtdDerivadas: a.qtdDerivadas + b.qtdDerivadas,
								qtdFinalizadas: a.qtdFinalizadas + b.qtdFinalizadas,
								qtdAbandonadas: a.qtdAbandonadas + b.qtdAbandonadas,
								qtdEntrantes: a.qtdEntrantes + b.qtdEntrantes,
								qtdRetidas: a.qtdRetidas + b.qtdRetidas,
								//Brunno 11/03/2018 Correção para o TMA quando é nulo
								tma: a.tma !== undefined ? ((Number(a.tma) + Number(b.tma)) / 2).toFixed(2) : undefined,
								qtdClientesEntrantes: typeof objetoAgrupado[m][objetoAgrupado[m].length - 1].qtdClientesEntrantes === "number" && testeAdicional ? objetoAgrupado[m][objetoAgrupado[m].length - 1].qtdClientesEntrantes : "NA",
								qtdClientesDerivados: typeof objetoAgrupado[m][objetoAgrupado[m].length - 1].qtdClientesDerivados === "number" && testeAdicional ? objetoAgrupado[m][objetoAgrupado[m].length - 1].qtdClientesDerivados : "NA",
								//qtdRetidasLiq1h : a.qtdRetidasLiq1h + b.qtdRetidasLiq1h,
								//qtdRetidasLiq2h : a.qtdRetidasLiq2h + b.qtdRetidasLiq2h,
								//qtdRetidasLiq24h : a.qtdRetidasLiq24h + b.qtdRetidasLiq24h

							};
							
						});
						
						mes.qtdPercDerivadas = ((mes.qtdDerivadas * 100) / mes.qtdEntrantes).toFixed(2);
						mes.qtdPercFinalizadas = ((mes.qtdFinalizadas * 100) / mes.qtdEntrantes).toFixed(2);						
						mes.qtdPercAbandonadas = ((mes.qtdAbandonadas * 100) / mes.qtdEntrantes).toFixed(2);
						mes.qtdPercRetidas = ((mes.qtdRetidas * 100) / mes.qtdEntrantes).toFixed(2);

						mes.qtdPercClientesEntrantes = typeof mes.qtdClientesEntrantes === "number" ? ((mes.qtdClientesEntrantes * 100) / mes.qtdEntrantes).toFixed(2) : "NA";
						mes.qtdPercClientesDerivados = typeof mes.qtdClientesDerivados === "number" ? ((mes.qtdClientesDerivados * 100) / mes.qtdEntrantes).toFixed(2) : "NA";

						$scope.dadosMes.push(mes);
					}

					//if por mes

					if ($scope.chkMensal) {
						$scope.dados = $scope.dadosMes;
						num_rows = $scope.dados.length;

						// Setando $scope.csv para ser utilizada na exportação
						$scope.csv = [];
						$scope.dados.forEach(function (dado) {
							$scope.csv.push([
								dado["datReferencia"],
								dado["qtdEntrantes"],
								dado["qtdDerivadas"],
								dado["qtdFinalizadas"],
								dado["qtdAbandonadas"],
								dado["qtdRetidas"],
								dado["tma"],

								dado["qtdPercDerivadas"],
								dado["qtdPercFinalizadas"],
								dado["qtdPercAbandonadas"],
								dado["qtdPercRetidas"],

								dado["qtdClientesEntrantes"],
								dado["qtdClientesDerivados"],

								dado["qtdPercClientesEntrantes"],
								dado["qtdPercClientesDerivados"]
							]);
						});
					}
					// ________________________________________________________
					// ________________________________________________________
				}
			}

			retornaStatusQuery(num_rows, $scope, datFim);

			$btn_gerar.button('reset');

			if (num_rows > 0) {
				$btn_exportar.prop("disabled", false);
				$btn_exportar_csv.prop("disabled", false);
				$btn_exportar_dropdown.prop("disabled", false);
			}

			//if(executaQuery !== executaQuery2){
			$('.btn.btn-pivot').prop("disabled", "false");
			if (moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days") + 1 <= limiteDias && num_rows > 0) {
				$('.btn.btn-pivot').button('reset');
			}
			//}



			userLog(stmt, "Consulta " + complemento, 2, err);

		});
		//});

		// GILBERTOOOOOO 17/03/2014
		$view.on("mouseup", "tr.resumo", function () {
			var that = $(this);
			$('tr.resumo.marcado').toggleClass('marcado');
			$scope.$apply(function () {
				that.toggleClass('marcado');
			});
		});

	};


		//Exportar planilha XLSX
		$scope.exportaXLSX = function () {
		
			var $btn_exportar = $(this);
			$btn_exportar.button('loading');
	
			var colunas = [];
			for (i = 0; i < $scope.colunas.length; i++) {
				colunas.push($scope.colunas[i].displayName);
			}
	
			// var colunasFinal = [colunas];
	
			console.log(colunas);
	
			var dadosExcel = [colunas].concat($scope.csv);
	
			var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);
	
			var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
			var wb = XLSX.utils.book_new();
			var milis = new Date();
			var arquivoNome = 'tresumodd_' + formataDataHoraMilis(milis) + '.xlsx';
			XLSX.utils.book_append_sheet(wb, ws)
			console.log(wb);
			XLSX.writeFile(wb, tempDir3() + arquivoNome, {
				compression: true
			});
			console.log('Arquivo escrito');
			childProcess.exec(tempDir3() + arquivoNome);
			
			setTimeout(function() {
				$btn_exportar.button('reset');
			}, 500);		
		
		}

	//{ EDs Input Controle
	var modificaAlertaEDs = function (x) {
		if (x == "Elemento repetido ou número máximo de EDs acumulados") {
			$('.notification .ng-binding').text(x).selectpicker('refresh');
			setTimeout(function () {
				$('.notification .ng-binding').text("").selectpicker('refresh');
			}, 3000);
		} else {
			//$("#filtro-ed").attr("placeholder", x);
		}
	};


	$scope.abreAjuda = function (link) {
		abreAjuda(link);
	}
	
}
CtrlResumoDD.$inject = ['$scope', '$globals'];