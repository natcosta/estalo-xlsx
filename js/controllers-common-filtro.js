//{ Funções para carregar filtros
function carregaAplicacoesGrupo(view, refresh, aplsVenda, scop, grupo) {
	var idview = view.attr('id');
	var apls = cache.apls;
	if (idview === "pag-derivacoes-repetidas-televendas") {
		apls = cache.apls.filter(function (apl) {
			if (apl.codigo.substring(0, 3) === "TLV") {
				return apl
			}
		});
	}

	if (aplsVenda === true) {
		var aplicacoes_com_vendas = cache.aplsVenda;
		scop.aplicacoes = cache.apls.filter(function (apl) {
			return aplicacoes_com_vendas.indexOf(apl.codigo) >= 0;
		});
		//Aplicações com vendas
		Estalo.filtros.filtro_aplicacoes = Estalo.filtros.filtro_aplicacoes.filter(function (apl) {
			return aplicacoes_com_vendas.indexOf(apl) >= 0;
		});
		apls = scop.aplicacoes;
	}

	apls = blacklistWhitelist(apls, idview);

	//preenchendo list com as aplicacoes
	var options = apls.map(function (array) {

		//25/09/2014 Carregamento de aplicações de acordo com a datas de implantação e expiração
		//datainicio maior que datainicio vigencia e datainicio menor que a datafim vigencia
		//ou
		//datainicio menor data inicio vigencia e datafim maior que data inicio vigencia
		if (scop.periodo.inicio >= new Date(array.inicio) && scop.periodo.inicio <= new Date(array.fim) ||
			scop.periodo.inicio < new Date(array.inicio) && scop.periodo.fim >= new Date(array.inicio)) {
			var subtexto = subtexto = array.ngr ? ' data-subtext="NGR"' : '';
			var contem = false;
			if (grupo == array.grupo) return '<option value="' + array.codigo + '"' + subtexto + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		}
	});
	if (refresh === true) {
		view.find("select.filtro-aplicacao").html(options.filter(function (i) {
			return i
		}).join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-aplicacao").html(options.filter(function (i) {
			return i
		}).join());
	}
}
var filtroApl = [];

function gruposAplicacoes() {
	var grupo = "";
	var primeiroAcesso = true;
	var conteudo;
	var outros = [];
	for (var i = 0; i < filtroApl.length; i++) {
		if (filtroApl[i] !== undefined) {
			if (filtroApl[i].grupo == "Outros") {
				outros.push({
					codigo: filtroApl[i].codigo,
					nome: filtroApl[i].nome,
					grupo: filtroApl[i].grupo,
					marcado: filtroApl[i].marcado
				});
			} else if (grupo !== filtroApl[i].grupo && grupo !== "") {
				conteudo = conteudo + '</optgroup>';
				conteudo = conteudo + '<optgroup label="' + filtroApl[i].grupo + '">';
				conteudo = conteudo + '<option ' + filtroApl[i].marcado + ' value="' + filtroApl[i].codigo + '">' + filtroApl[i].nome + '</option>';
				grupo = filtroApl[i].grupo;
			} else if (grupo == "") {
				conteudo = '<optgroup label="' + filtroApl[i].grupo + '">';
				conteudo = conteudo + '<option ' + filtroApl[i].marcado + ' value="' + filtroApl[i].codigo + '">' + filtroApl[i].nome + '</option>';
				grupo = filtroApl[i].grupo;
			} else {
				conteudo = conteudo + '<option ' + filtroApl[i].marcado + ' value="' + filtroApl[i].codigo + '">' + filtroApl[i].nome + '</option>';
			}
		}
	}
	conteudo = conteudo + '<optgroup label="Outros">';
	for (var i = 0; i < outros.length; i++) {
		conteudo = conteudo + '<option ' + outros[i].marcado + ' value="' + outros[i].codigo + '">' + outros[i].nome + '</option>';
	}
	conteudo = conteudo + '</optgroup>';
	$('select.filtro-aplicacao').append(conteudo);
}

function carregaAplicacoes(view, refresh, aplsVenda, scop) {
	var idview = view.attr('id');
	var apls = cache.apls;

	if (idview === "pag-derivacoes-repetidas-televendas") {
		apls = cache.apls.filter(function (apl) {
			if (apl.codigo.substring(0, 3) === "TLV") {
				return apl
			}
		});
	}
	if (idview.match('telaunica') !== null) {
		apls = cache.apls.filter(function (apl) {
			if (apl.codigo.match('TELAUNICA') !== null) {
				return apl
			}
		});
	}
	
	if (idview === "pag-derivacoes-repetidas-ic") {
		scop.aplicacoes = apls.filter(function (apl) {
				return aplicacaoic.indexOf(apl.codigo) >= 0;
			});
			apls = scop.aplicacoes
	}

	if (aplsVenda === true) {
		var aplicacoes_com_vendas = cache.aplsVenda;
		scop.aplicacoes = apls.filter(function (apl) {
			return aplicacoes_com_vendas.indexOf(apl.codigo) >= 0;
		});
		//Aplicações com vendas
		Estalo.filtros.filtro_aplicacoes = Estalo.filtros.filtro_aplicacoes.filter(function (apl) {
			return aplicacoes_com_vendas.indexOf(apl) >= 0;
		});
		apls = scop.aplicacoes;
	}

	apls = blacklistWhitelist(apls, idview);

	//preenchendo list com as aplicacoes
	filtroApl = apls.map(function (array) {

		//25/09/2014 Carregamento de aplicações de acordo com a datas de implantação e expiração
		//datainicio maior que datainicio vigencia e datainicio menor que a datafim vigencia
		//ou
		//datainicio menor data inicio vigencia e datafim maior que data inicio vigencia
		if (scop.periodo.inicio >= new Date(array.inicio) && scop.periodo.inicio <= new Date(array.fim) ||
			scop.periodo.inicio < new Date(array.inicio) && scop.periodo.fim >= new Date(array.inicio)) {
			var subtexto = subtexto = array.ngr ? ' data-subtext="NGR"' : '';
			var contem = (Estalo.filtros.filtro_aplicacoes.indexOf("" + array.codigo) >= 0);
			return ({
				codigo: array.codigo,
				nome: array.nome,
				grupo: array.grupo,
				marcado: (contem ? " selected" : "")
			});
		}
	});
	if (!refresh) gruposAplicacoes();

	//02/11/2017
	view.on("click", "div.filtro-aplicacao > div > ul > li > dt", function () {
		var grupoAplicacaoText = $(this).text();
		var optionGroupAplicacao = $('select.filtro-aplicacao > optgroup');
		for (var i = 0; i < optionGroupAplicacao.length; i++) {
			if (grupoAplicacaoText == optionGroupAplicacao[i].label) {
				var selecaoGrupo = $('select.filtro-aplicacao > optgroup')[i].getAttribute("aplicacaoselecionada");
				if (selecaoGrupo == null || selecaoGrupo == "false") {
					selecaoGrupo = true;
				} else {
					selecaoGrupo = false;
				}
				$('select.filtro-aplicacao > optgroup')[i].setAttribute("aplicacaoselecionada", selecaoGrupo)
				for (var j = 0; j < optionGroupAplicacao[i].childNodes.length; j++) {
					optionGroupAplicacao[i].childNodes[j].selected = selecaoGrupo;
				}
			}
		}
		$('select.filtro-aplicacao').selectpicker('refresh');
		carregaSegmentosPorAplicacao(scop, view);
		$(".filtro-aplicacao").trigger('change');
	});

}

function carregaListaAplicacoes(view, refresh, scop) {
	//preenchendo list com as aplicacoes
	filtroApl = cache.apls.map(function (array) {
		//25/09/2014 Carregamento de aplicações de acordo com a datas de implantação e expiração
		//datainicio maior que datainicio vigencia e datainicio menor que a datafim vigencia
		//ou
		//datainicio menor data inicio vigencia e datafim maior que data inicio vigencia
		var subtexto = subtexto = array.ngr ? ' data-subtext="NGR"' : '';
		var contem = (Estalo.filtros.filtro_aplicacoes.indexOf("" + array.codigo) >= 0);
		var now = new Date();
		if (now < array.fim) {
			return ({
				codigo: array.codigo,
				nome: array.nome,
				grupo: array.grupo,
				marcado: (contem ? " selected" : "")
			});
		}
	});

	if (!refresh) gruposAplicacoes();
}

function carregaFiltroEDAuto(view) {
	// Popula lista de eds a partir das aplicações selecionadas
	var $sel_eds = view.find("select.filtro-edAuto");
	var filtro_aplicacoes = view.find('select.filtro-aplicacao').val() || [];
	if (filtro_aplicacoes.map === undefined) {
		filtro_aplicacoes = [filtro_aplicacoes];
	}

	if (filtro_aplicacoes.length === 0) {
		$sel_eds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinked').attr('disabled', 'disabled');
		return;
	}
	var filtro_eds = {};
	var eds = obtemEdsDasAplicacoes(filtro_aplicacoes) || [];
	var options4 = [];
	var k = "";

	var teste = setInterval(function () {
		if (cache.aplicacoes[filtro_aplicacoes].hasOwnProperty('estados')) {
			clearInterval(teste);
			$('.notification .ng-binding').text("EDs carregados").selectpicker('refresh');
			eds = [cache.aplicacoes[filtro_aplicacoes].estados];
			eds.forEach(function (filtro_eds) {
				v = filtro_eds || [];
				unique(v).forEach((function (filtro_eds) {
					return function (codigo) {
						for (var i = 0; i < v.length; i++) {
							if(v[i]['nome'] !== undefined){
								if(v[i]['trash'] && v[i]['volume']){
									var contem = (Estalo.filtros.filtro_ed.indexOf("" + v[i]['codigo']) >= 0);
									var nome = v[i]['nome'].substring(0, 47) + (v[i]['nome'].length > 47 ? "..." : "");
									options4.push('<option value="' + v[i]['codigo'] + '" ' + (contem ? " selected" : "") + '>' + v[i]['codigo'] + " - " + nome + '</option>');
								}
							}
						}
					};
				})(filtro_eds));
				$sel_eds.html(options4.join());
				$sel_eds
					.prop("disabled", false)
					.selectpicker('refresh');
			});
		} else {
			$('.notification .ng-binding').text("Carregando EDs...").selectpicker('refresh');
			$sel_eds
				.html("")
				.prop("disabled", true)
				.selectpicker('refresh');
			$('#alinked').attr('disabled', 'disabled');
		}
	}, 500);
}

function carregaEDsT(scop, view, inicia) {
	//03/01/2017 Preenchendo list com edstransf
	var $sel_edstransf = view.find("select.filtro-edstransf");
	var filtro_aplicacoes = view.find('select.filtro-aplicacao').val() || [];

	if (filtro_aplicacoes.length === 0) {
		$sel_edstransf
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkEDTr').attr('disabled', 'disabled');
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	var edstransf = [];
	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		edstransf = obtemEDSTDasAplicacoes(Estalo.filtros.filtro_aplicacoes);
	} else {
		edstransf = obtemEDSTDasAplicacoes(filtro_aplicacoes);
	}

	var filtro_edstransf = {};
	($sel_edstransf.val() || []).forEach(function (codigo) {
		filtro_edstransf[codigo] = true
	});

	view.find("select.filtro-edstransf").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} edstransf'
	});

	var options8 = [];
	var t = [];

	var filtro_edstransf = {};
	if (filtro_aplicacoes.map === undefined) {
		filtro_aplicacoes = [filtro_aplicacoes];
	}

	edstransf.forEach(function (filtro_edstransf) {
		t = filtro_edstransf || [];
		unique(t).forEach((function (filtro_edstransf) {
			return function (item) {
				for (var i = 0; i < t.length; i++) {
					var op = t[i];
					var contem = (Estalo.filtros.filtro_edstransf.indexOf("" + op.codigo) >= 0);
					options8.push('<option value="' + op.codigo + '"' + (contem ? " selected" : "") + '>' + op.nome + '</option>');
				}
			};
		})(filtro_edstransf));
		$sel_edstransf.html(options8.join());
	});


	if (options8.length > 0) {
		$sel_edstransf
			.prop("disabled", false);
		$('#alinkEDTr').removeAttr('disabled'); //20/02/2014
	} else {
		$sel_edstransf
			.prop("disabled", true);
		$('#alinkEDTr').attr('disabled', 'disabled');
	}
	$sel_edstransf.selectpicker('refresh');
}

function carregaTIds(scop, view, inicia) {
	//03/01/2017 Preenchendo list com transferids
	var $sel_transferids = view.find("select.filtro-transferid");
	var filtro_aplicacoes = view.find('select.filtro-aplicacao').val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_transferids
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkTid').attr('disabled', 'disabled'); //20-02-2014
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	var transferids = [];
	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		transferids = obtemTidsDasAplicacoes(Estalo.filtros.filtro_aplicacoes);
	} else {
		transferids = obtemTidsDasAplicacoes(filtro_aplicacoes);
	}

	//28/02/2014 Preenchendo list com transferids
	var filtro_transferids = {};
	($sel_transferids.val() || []).forEach(function (codigo) {
		filtro_transferids[codigo] = true;
	});

	view.find("select.filtro-transferid").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} transferids'
	});

	var options7 = [];
	var u = [];

	var filtro_transferids = {};
	if (filtro_aplicacoes.map === undefined) {
		filtro_aplicacoes = [filtro_aplicacoes];
	}

	transferids.forEach(function (filtro_transferids) {
		u = filtro_transferids || [];
		unique(u).forEach((function (filtro_transferids) {
			return function (item) {
				for (var i = 0; i < u.length; i++) {
					var op = u[i];
					var contem = (Estalo.filtros.filtro_transferids.indexOf("" + op.codigo) >= 0);
					options7.push('<option value="' + op.codigo + '"' + (contem ? " selected" : "") + '>' + op.codigo + '</option>');
				}
			};

		})(filtro_transferids));
		$sel_transferids.html(options7.join());
	});

	if (options7.length > 0) {
		$sel_transferids
			.prop("disabled", false);
		$('#alinkTid').removeAttr('disabled'); //Bernardo 20-02-2014
	} else {
		$sel_transferids
			.prop("disabled", true);
		$('#alinkTid').attr('disabled', 'disabled');
	}
	$sel_transferids.selectpicker('refresh');
}

function carregaOps(scop, view, inicia) {
	//03/01/2017 Preenchendo list com operacoes
	var $sel_operacoes = view.find("select.filtro-op");
	var filtro_aplicacoes = view.find('select.filtro-aplicacao').val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_operacoes
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkOp').attr('disabled', 'disabled'); //20/02/2014
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	var operacoes = [];

	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {

		operacoes = obtemOperacoesDasAplicacoes(Estalo.filtros.filtro_aplicacoes);
	} else {

		operacoes = obtemOperacoesDasAplicacoes(filtro_aplicacoes);
	}

	//28/02/2014 Preenchendo list com operacoes
	var filtro_operacoes = {};
	($sel_operacoes.val() || []).forEach(function (codigo) {
		filtro_operacoes[codigo] = true;
	});

	view.find("select.filtro-op").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} operações'
	});

	var options3 = [];
	var x = [];

	var filtro_operacoes = {};
	if (filtro_aplicacoes.map === undefined) {
		filtro_aplicacoes = [filtro_aplicacoes];
	}

	operacoes.forEach(function (filtro_operacoes) {
		x = filtro_operacoes || [];
		unique(x).forEach((function (filtro_operacoes) {
			return function (item) {
				for (var i = 0; i < x.length; i++) {
					var op = x[i];
					var contem = (Estalo.filtros.filtro_operacoes.indexOf("" + op.codigo) >= 0);
					options3.push('<option value="' + op.codigo + '"' + (contem ? " selected" : "") + '>' + op.nome + '</option>');
				}
			};

		})(filtro_operacoes));
		$sel_operacoes.html(options3.join());
	});

	if (options3.length > 0) {
		$sel_operacoes
			.prop("disabled", false);
		$('#alinkOp').removeAttr('disabled'); //20/02/2014
	} else {
		$sel_operacoes
			.prop("disabled", true);
		$('#alinkOp').attr('disabled', 'disabled');
	}
	$sel_operacoes.selectpicker('refresh');
}

function carregaSegmentosPorAplicacao(scop, view, inicia) {
	testaIndices();

	if (Estalo.filtros.filtro_segmentos === null) {
		Estalo.filtros.filtro_segmentos = []
	};

	// Popula lista de segmentos e/ou operações a partir das aplicações selecionadas
	// lista EDs
	// lista ICs

	var $sel_segmentos = view.find("select.filtro-segmento");
	var $sel_eds = view.find("select.filtro-ed");
	var $sel_ics = view.find("select.filtro-ic");



	var filtro_aplicacoes = view.find('select.filtro-aplicacao').val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_segmentos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkSeg').attr('disabled', 'disabled'); //20/02/2014
		$sel_eds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$sel_ics
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	var filtro_segmentos = {};
	($sel_segmentos.val() || []).forEach(function (codigo) {
		filtro_segmentos[codigo] = true;
	});

	view.find("select.filtro-segmento").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} segmentos'
	});

	var options2 = [];
	var optionsGroup = [];
	var optionsGroupTemp = [];
	var v = [];
	var z = [];

	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		if (typeof (filtro_aplicacoes) === 'string') {
			Estalo.filtros.filtro_aplicacoes = [Estalo.filtros.filtro_aplicacoes[Estalo.filtros.filtro_aplicacoes.length - 1]];
			filtro_aplicacoes = Estalo.filtros.filtro_aplicacoes;
			carregaAplicacoes(view, false, false, scop);
		}
		Estalo.filtros.filtro_aplicacoes.forEach(function (codigo) {
			z = z.concat(unique(cache.aplicacoes[codigo].segmentos.map(function (seg) {
				return seg.codigo2
			})) || []);
		});
		Estalo.filtros.filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].segmentos.map(function (seg) {
				return seg.codigo
			}) || []);
		});
	} else {
		if (typeof (filtro_aplicacoes) === 'string') {
			filtro_aplicacoes = [filtro_aplicacoes];
		}
		filtro_aplicacoes.forEach(function (codigo) {
			z = z.concat(unique(cache.aplicacoes[codigo].segmentos.map(function (seg) {
				return seg.codigo2
			})) || []);
		});
		filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].segmentos.map(function (seg) {
				return seg.codigo
			}) || []);
		});
	}

	var objSegmento2 = {};	
	scop.objSegmento2 = objSegmento2;
	
	unique(z).forEach(function (grupo) {
		var nome = obtemNomeGrupoSegmentos(grupo);
		options2.push('<optgroup label="' + nome + '">');
		unique(v).forEach((function (filtro_segmentos) {
			return function (codigo) {
				var contem = Estalo.filtros.filtro_segmentos.indexOf(codigo) >= 0;
				var seg = cache.segs.indice[codigo];
				//scop.objSegmento[codigo] = seg.datacriacao;
				scop.objSegmento2[codigo] = {
					nome: seg.nome,
					data: seg.datacriacao,
					nomeFiltro: seg.nomeFiltro
				};
				
				isDentroDoPeriodo = scop.periodo.inicio >= new Date(seg.datacriacao) && scop.periodo.inicio <= new Date(seg.dataexpiracao) ||
			scop.periodo.inicio < new Date(seg.datacriacao) && scop.periodo.fim >= new Date(seg.datacriacao);
				
				//isDentroDoPeriodo = seg.datacriacao <= scop.periodo.fim;
				if (seg.grupo === grupo && (isDentroDoPeriodo || seg.datacriacao == null)) {
					contem ? options2.push('<option value="' + seg.codigo + '" selected="">' + seg.nome + '</option>') :
						options2.push('<option value="' + seg.codigo + '">' + (seg.nomeFiltro !== null ? seg.nomeFiltro : seg.nome) + '</option>');
						
				}
			};
		})(filtro_segmentos));

		options2.push('</optgroup>');
		options2.sort(function (a, b) {
			if(a.match(/<option .*?>(.*?)<\/option>/) !== null && b.match(/<option .*?>(.*?)<\/option>/) !== null){
				if (a.match(/<option .*?>(.*?)<\/option>/)[1] > b.match(/<option .*?>(.*?)<\/option>/)[1]) {
					return 1;
				}
				if (a.match(/<option .*?>(.*?)<\/option>/)[1] < b.match(/<option .*?>(.*?)<\/option>/)[1]) {
					return -1;
				}
				// a must be equal to b
				return 0;
			}
		});		
		optionsGroupTemp.push(options2.reduce(function(prev, curr) {
			return prev.concat(curr);
		}));
		optionsGroupTemp.sort(function (a, b) {
			if(a.match(/<optgroup .*?>(.*?)<\/optgroup>/) !== null && b.match(/<optgroup .*?>(.*?)<\/optgroup>/) !== null){
				if (a.match(/<optgroup .*?>(.*?)<\/optgroup>/)[0] > b.match(/<optgroup .*?>(.*?)<\/optgroup>/)[0]) {
					return 1;
				}
				if (a.match(/<optgroup .*?>(.*?)<\/optgroup>/)[0] < b.match(/<optgroup .*?>(.*?)<\/optgroup>/)[0]) {
					return -1;
				}
				// a must be equal to b
				return 0;
			}
		});
		optionsGroup = optionsGroupTemp.reduce(function(prev, curr) {
			return prev.concat(curr);
		});
		options2 = [];
		$sel_segmentos.html(optionsGroup);

	});

	//11/07/2014 Habilitar segmentos somente se houver segmentos para aplicação
	if (optionsGroup.length > 0) {
		$sel_segmentos
			.prop("disabled", false);
		$('#alinkSeg').removeAttr('disabled'); //20/02/2014
	} else {
		$sel_segmentos
			.html("")
			.prop("disabled", true);
		$('#alinkSeg').attr('disabled', 'disabled');
	}
	$sel_segmentos.selectpicker('refresh');

	if (view.attr('id') !== "pag-consolidado-ed" && view.attr('id') !== "pag-transferID") {
		carregaEDsT(scop, view, inicia);
		carregaTIds(scop, view, inicia);
		carregaOps(scop, view, inicia);
	}

	////{ Limpa Filtro IC
	$("#filtro-ic").attr("placeholder", "Itens de Controle");
	$("#filtro-ic").val("");
	scop.filtroICSelect = [];
	if (scop.filtros !== undefined) scop.filtros.isHFiltroIC = false;

	$("#filtro-ed").attr("placeholder", "Estados de Diálogo");
	$("#filtro-ed").val("");
	scop.filtroEDSelect = [];

	var $sel_eds = view.find("select.filtro-ed");
	$sel_eds
		.prop("disabled", "disabled")
		.selectpicker('refresh');

	if (view.attr('id') === "pag-consolidado-ed") carregaFiltroEDAuto(view);

	//?
	var temp = filtro_aplicacoes;
	var item = jQuery.grep(filtro_aplicacoes, function (item) {
		return jQuery.inArray(item, Estalo.filtros.filtro_aplicacoes) < 0;
	});

	if (item.length === 1) temp = $.map(filtro_aplicacoes, function (apl) {
		if (apl !== item.toString()) {
			return apl
		}
	});
	temp = temp.concat(item);
	Estalo.filtros.filtro_aplicacoes = temp;
	//?

	//02/11/2017
	view.on("click", "div.filtro-segmento > div > ul > li > dt", function () {
		var grupoSegmentoText = $(this).text();
		var optionGroupSegmento = $('select.filtro-segmento > optgroup');
		for (var i = 0; i < optionGroupSegmento.length; i++) {
			if (grupoSegmentoText == optionGroupSegmento[i].label) {
				var selecaoGrupoSegmento = $('select.filtro-segmento > optgroup')[i].getAttribute("segmentoselecionado");
				if (selecaoGrupoSegmento == null || selecaoGrupoSegmento == "false") {
					selecaoGrupoSegmento = true;
				} else {
					selecaoGrupoSegmento = false;
				}
				$('select.filtro-segmento > optgroup')[i].setAttribute("segmentoselecionado", selecaoGrupoSegmento)
				for (var j = 0; j < optionGroupSegmento[i].childNodes.length; j++) {
					optionGroupSegmento[i].childNodes[j].selected = selecaoGrupoSegmento;
				}
			}
		}
		$('select.filtro-segmento').selectpicker('refresh');
	});
}


function carregaDDDsPorRegiao(scop, view, inicia) {
	testaIndices();

	//Alex 28/02/2014 Preenchendo list com DDDs
	var $sel_ddds = view.find("select.filtro-ddd");
	var $sel_ufs = view.find("select.filtro-uf");
	if (view.attr('id') === 'pag-resumo-880x144') {
		var $sel_segmentos = view.find("select.filtro-segmento");
	}


	var filtro_regioes = $("select.filtro-regiao").val() || [];
	if (filtro_regioes.length === 0) {
		$sel_ddds
			.prop("disabled", false)
			.selectpicker('refresh');
		$('#alinkDDD').removeAttr('disabled').selectpicker('refresh')
		carregaDDDs(view, true)
		return;
	}


	var filtro_ddds = {};
	($sel_ddds.val() || []).forEach(function (codigo) {
		filtro_ddds[codigo] = true;
	});

	view.find("select.filtro-ddd").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} DDDs',
		showSubtext: true
	});

	var options3 = [];
	var v = [];



	if (Estalo.filtros.filtro_regioes.length > 0 && inicia === true) {
		Estalo.filtros.filtro_regioes.forEach(function (codigo) {
			v = v.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.regiao === codigo) {
					return ddd.codigo
				}
			})) || []).sort();
		});
	} else {
		filtro_regioes.forEach(function (codigo) {
			v = v.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.regiao === codigo) {
					return ddd.codigo
				}
			})) || []);
		});
	}

	unique(v).forEach((function (filtro_ddds) {
		return function (codigo) {
			var d = cache.ddds.indice[codigo];
			if ($.map(cache.ddds, function (ddd) {
					return ddd.codigo
				}).indexOf(codigo.toString()) >= 0) {
				var contem = (Estalo.filtros.filtro_ddds.indexOf("" + d.codigo) >= 0);
				var nome = d.nome.substring(0, 40) + (d.nome.length > 40 ? "..." : "");
				options3.push('<option value="' + d.codigo + '"' + (contem ? " selected" : "") + '>' + nome + '</option>');
			}
		};
	})(filtro_ddds));
	$sel_ddds.html(options3.join());




	var filtro_ufs = {};
	($sel_ufs.val() || []).forEach(function (codigo) {
		filtro_ufs[codigo] = true;
	});

	view.find("select.filtro-uf").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} UFs',
		showSubtext: true
	});

	var options1 = [];
	var z = [];


	if (Estalo.filtros.filtro_regioes.length > 0 && inicia === true) {
		Estalo.filtros.filtro_ufs.forEach(function (codigo) {
			z = z.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.regiao === codigo) {
					return ddd.uf
				}
			})) || []).sort();
		});
	} else {
		filtro_regioes.forEach(function (codigo) {
			z = z.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.regiao === codigo) {
					return ddd.uf
				}
			})) || []);
		});
	}


	unique(z).forEach((function (filtro_ufs) {
		return function (codigo) {
			var uf = cache.ufs.indice[codigo];
			var contem = (Estalo.filtros.filtro_ufs.indexOf("" + uf.codigo) >= 0);
			options1.push('<option value="' + uf.codigo + '"' + (contem ? " selected" : "") + '>' + uf.nome + '</option>');
		};
	})(filtro_ufs));
	$sel_ufs.html(options1.join());



	if (options3.length > 0) {
		$sel_ddds
			.prop("disabled", false)
			.selectpicker('refresh');
		$('#porRegEDDD').attr('disabled', false).selectpicker('refresh');
		$('#alinkDDD').removeAttr('disabled'); //Bernardo 20-02-2014
		if (view.attr('id') === 'pag-resumo-880x144') {
			$('#alinkSeg').removeAttr('disabled');
		}
		$('.btn.btn-pivot').prop("disabled", "false");
		$sel_ufs
			.prop("disabled", false)
			.selectpicker('refresh');
		$('#alinkUf').removeAttr('disabled'); // 20-02-2014

	} else {
		$sel_ddds
			.prop("disabled", "disabled")
			.selectpicker('refresh');
		$('#porRegEDDD').attr('disabled', true).selectpicker('refresh');
		$('#alinkDDD').attr('disabled', 'disabled').selectpicker('refresh'); //Bernardo 20-02-2014
		$('#porRegEDDD').prop('checked', false);
		$('.btn.btn-pivot').prop("disabled", "false");
	}


	if (view.attr('id') === 'pag-resumo-880x144') {

		var segs = $.map(cache.segs, function (seg) {
			if (cache.aplicacoes['880R1IN'].segmentos.map(function (seg) {
					return seg.codigo
				}).indexOf(seg.codigo) >= 0) {
				return seg;
			}
		});


		//Preenchendo lista com todos os segmentos
		var options2 = [];
		segs.forEach(function (seg) {
			//options2.push('<option data-divider="true"></option>');
			options2.push('<option value="' + seg.codigo + '">' + seg.nome + '</option>');
		});
		$sel_segmentos.html(options2.join());

		$sel_segmentos
			//              .html("")
			.prop("disabled", false)
			.selectpicker('refresh');
	}

}


function carregaDDDsPorUF(scop, view, inicia) {
	testaIndices();

	//Preenchendo list com DDDs
	var $sel_ddds = view.find("select.filtro-ddd");
	var $sel_gras = view.find("select.filtro-gras");

	var filtro_ufs = $("select.filtro-uf").val() || [];
	if (filtro_ufs.length === 0) {
		$sel_ddds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkDDD').attr('disabled', 'disabled'); //Bernardo 20-02-2014

		$sel_gras
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkGRA').attr('disabled', 'disabled'); //Bernardo 20-02-2014
		return;
	}

	var filtro_ddds = {};
	($sel_ddds.val() || []).forEach(function (codigo) {
		filtro_ddds[codigo] = true;
	});

	view.find("select.filtro-ddd").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} DDDs'
	});

	var options3 = [];
	var v = [];

	if (Estalo.filtros.filtro_ufs.length > 0 && inicia === true) {
		Estalo.filtros.filtro_ufs.forEach(function (codigo) {
			v = v.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.uf === codigo) {
					return ddd.codigo
				}
			})) || []).sort();
		});
	} else {
		filtro_ufs.forEach(function (codigo) {
			v = v.concat(unique2(cache.ddds.map(function (ddd) {
				if (ddd.uf === codigo) {
					return ddd.codigo
				}
			})) || []);
		});
	}

	Estalo.filtros.filtro_ufs.forEach(function (codigo) {
		v = v.concat(unique2(cache.ddds.map(function (ddd) {
			if (ddd.uf === codigo) {
				return ddd.codigo
			}
		})) || []).sort();
	});
	unique(v).forEach((function (filtro_ddds) {
		return function (codigo) {
			var d = cache.ddds.indice[codigo];
			var contem = (Estalo.filtros.filtro_ddds.indexOf("" + d.codigo) >= 0);
			var nome = d.nome.substring(0, 40) + (d.nome.length > 40 ? "..." : "");
			contem ? options3.push('<option value="' + d.codigo + '" selected="">' + nome + '</option>') :
				options3.push('<option value="' + d.codigo + '">' + nome + '</option>');
		};
	})(filtro_ddds));
	$sel_ddds.html(options3.join());

	var $sel_gras = view.find("select.filtro-gras");
	$('#alinkGRA').attr('disabled', 'disabled');
	$sel_gras
		.html("")
		.prop("disabled", true)
		.selectpicker('refresh');
	$sel_ddds
		.prop("disabled", false)
		.selectpicker('refresh');
	$('#alinkDDD').removeAttr('disabled');
}


function carregaGruposProdutoPorAplicacao(scop, view, inicia) {

	testaIndices();

	var $sel_grupo_produtos = view.find("select.filtro-grupo-produto");
	var filtro_grupos_produtos = {};
	($sel_grupo_produtos.val() || []).forEach(function (codigo) {
		filtro_grupos_produtos[codigo] = true;
	});


	var filtro_aplicacoes = $("select.filtro-aplicacao").val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_grupo_produtos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkGru').attr('disabled', 'disabled'); //Bernardo 20-02-2014
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	view.find("select.filtro-grupo-produto").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} grupos'
	});

	var options = [];
	var v = [];

	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		Estalo.filtros.filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(unique(cache.aplicacoes[codigo].produtos.map(function (prod) {
				return prod.grupo
			})) || []);
		});
	} else {
		filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(unique(cache.aplicacoes[codigo].produtos.map(function (prod) {
				return prod.grupo
			})) || []);
		});
	}

	unique(v).forEach((function (filtro_grupos_produtos) {
		return function (codigo) {
			var grup = cache.grupos_produtos_vendas.indice[codigo];
			var contem = (Estalo.filtros.filtro_grupos_produtos.indexOf("" + grup.codigo) >= 0);
			options.push('<option value="' + grup.codigo + '"' + (contem ? " selected" : "") + '>' + grup.nome + '</option>');
		};
	})(filtro_grupos_produtos));
	$sel_grupo_produtos.html(options.join());

	if (options.length > 0) {
		$sel_grupo_produtos
			.prop("disabled", false)
			.selectpicker('refresh');
		$('#alinkGru').removeAttr('disabled'); //Bernardo 20-02-2014
	}
	Estalo.filtros.filtro_aplicacoes = filtro_aplicacoes;

}


function carregaProdutosPorAplicacao(scop, view, inicia) {

	testaIndices();

	var $sel_produtos = view.find("select.filtro-produto");
	var filtro_produtos = {};
	($sel_produtos.val() || []).forEach(function (codigo) {
		filtro_produtos[codigo] = true;
	});


	var filtro_aplicacoes = $("select.filtro-aplicacao").val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_produtos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkProd').attr('disabled', 'disabled'); //Bernardo 20-02-2014
		Estalo.filtros.filtro_aplicacoes = [];
		return;
	}

	view.find("select.filtro-produto").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} produtos'
	});

	var options = [];
	var v = [];

	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		Estalo.filtros.filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].produtos.map(function (prod) {
				return prod.codigo
			}) || []);
		});
	} else {
		filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].produtos.map(function (prod) {
				return prod.codigo
			}) || []);
		});
	}

	unique(v).forEach((function (filtro_produtos) {
		return function (codigo) {

			var prod = cache.produtos_vendas.indice[codigo];
			var contem = (Estalo.filtros.filtro_produtos.indexOf("" + prod.codigo) >= 0);
			options.push('<option value="' + prod.codigo + '"' + (contem ? " selected" : "") + '>' + prod.nome + '</option>');

		};
	})(filtro_produtos));
	$sel_produtos.html(options.join());

	if (options.length > 0) {
		$sel_produtos
			.prop("disabled", false)
			.selectpicker('refresh');
		$('#alinkProd').removeAttr('disabled'); //Bernardo 20-02-2014
	}
	Estalo.filtros.filtro_aplicacoes = filtro_aplicacoes;

}



function carregaDDDsPorAplicacao(scop, view, inicia) {

	testaIndices();

	var $sel_ddds = view.find("select.filtro-ddd");
	var filtro_ddds = {};
	($sel_ddds.val() || []).forEach(function (codigo) {
		filtro_ddds[codigo] = true;
	});


	var filtro_aplicacoes = $("select.filtro-aplicacao").val() || [];
	if (filtro_aplicacoes.length === 0) {
		$sel_ddds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkDDD').attr('disabled', 'disabled'); //Bernardo 20-02-2014


		return;
	}

	var filtro_ddds = {};
	($sel_ddds.val() || []).forEach(function (codigo) {
		filtro_ddds[codigo] = true;
	});

	view.find("select.filtro-ddd").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} DDDs'
	});

	var options = [];
	var v = [];

	if (Estalo.filtros.filtro_aplicacoes.length > 0 && inicia === true) {
		Estalo.filtros.filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].dddsApl || []);
		});
	} else {
		filtro_aplicacoes.forEach(function (codigo) {
			v = v.concat(cache.aplicacoes[codigo].dddsApl || []);
		});
	}



	unique(v).forEach((function (filtro_ddds) {
		return function (codigo) {
			var ddd = cache.ddds.indice[codigo];
			var nome = ddd.nome.substring(0, 40) + (ddd.nome.length > 40 ? "..." : "");
			var contem = (Estalo.filtros.filtro_ddds.indexOf("" + ddd.codigo) >= 0);
			options.push('<option value="' + ddd.codigo + '"' + (contem ? " selected" : "") + '>' + nome + '</option>');
		};
	})(filtro_ddds));
	$sel_ddds.html(options.join());

	$sel_ddds
		.prop("disabled", false)
		.selectpicker('refresh');
	$('#alinkDDD').removeAttr('disabled'); //Bernardo 20-02-2014
	Estalo.filtros.filtro_aplicacoes = filtro_aplicacoes;
}


function marcaTodosIndependente(filtro, cache) {

	testaIndices();

	//Bernardo 20-02-2014 Marcar todos do filtro
	//Desmarcando todas
	var valor = $(this).attr("data-var");
	if (valor === "1") {
		filtro.selectpicker('deselectAll');
		$(this).attr("data-var", "0");
		if (cache === 'sites') {
			Estalo.filtros.filtro_sites = [];
			console.log('logou sites');
		} else if (cache === 'segmentos') {
			Estalo.filtros.filtro_segmentos = [];
		} else if (cache === 'servicos') {
			Estalo.filtros.filtro_servicos = [];
		} else if (cache === 'uras') {
			Estalo.filtros.filtro_uras = [];
			console.log('logou uras');
		} else if (cache === 'uras') {
			Estalo.filtros.filtro_uras = [];
		} else if (cache === 'incentivos') {
			Estalo.filtros.filtro_incentivos = [];
			console.log('logou incentivos');
		} else if (cache === 'produto_prepago') {
		Estalo.filtros.filtro_produto_prepago = [];
		console.log('logou produto_prepago');
		} else if (cache === 'telcallflow') {
			Estalo.filtros.filtro_telcallflow = [];
			console.log('logou telcallflow');
		} else if (cache === 'legados') {
			Estalo.filtros.filtro_legados = [];
			console.log('logou legados');
		} else if (cache === 'varjson') {
			Estalo.filtros.filtro_varjson = [];
			console.log('logou varjson');
		} else if (cache === 'reparos') {
			Estalo.filtros.filtro_reparos = [];
			console.log('logou reparos');
		} else if (cache === 'ddds') {
			Estalo.filtros.filtro_ddds = [];
		} else if (cache === 'ufs') {
			Estalo.filtros.filtro_ufs = [];
		} else if (cache === 'uds') {
			Estalo.filtros.filtro_ud = [];
		} else if (cache === 'ops') {
			Estalo.filtros.filtro_operacoes = [];
		} else if (cache === 'produtos') {
			Estalo.filtros.filtro_produtos = [];
		} else if (cache === 'erros') {
			Estalo.filtros.filtro_erros = [];
		} else if (cache === 'resultados') {
			Estalo.filtros.filtro_resultados = [];
		} else if (cache === 'empresas') {
			Estalo.filtros.filtro_empresas = [];
		} else if (cache === 'icsRepetidas') {
			Estalo.filtros.filtro_icsRepetidas = [];
		} else if (cache === 'reconhecimento') {
			Estalo.filtros.filtro_reconhecimentos = [];
		} else if (cache === 'operacoes') {
			Estalo.filtros.filtro_operacoes = [];
		} else if (cache === 'regras') {
			Estalo.filtros.filtro_regras = [];
		} else if (cache === 'skills') {
			Estalo.filtros.filtro_skills = [];
		} else if (cache === 'produtotu') {
			Estalo.filtros.filtro_produtotu = [];
		}

	} else {
		//Marcando todas
		filtro.selectpicker('selectAll');
		$(this).attr("data-var", "1");
	}

}

function marcaTodasAplicacoes(filtro, view, scop, produtoPorAplicacao) {

	testaIndices();
	//Desmarcando todas
	var valor = $('#alinkApl').attr("data-var");
	var $sel_segmentos = view.find("select.filtro-segmento");
	var $sel_operacoes = view.find("select.filtro-op");
	var $sel_ddds = view.find("select.filtro-ddd");
	var $sel_grupos = view.find("select.filtro-grupo-produto");
	var $sel_produtos = view.find("select.filtro-produto");

	if (valor === "1") {
		filtro.selectpicker('deselectAll');
		$('#alinkApl').attr("data-var", "0");
		$('#alinkSeg').attr('disabled', 'disabled');
		$('#alinkOp').attr('disabled', 'disabled');
		if (view.attr('id') !== "pag-resumo-dia") {
			$('#alinkDDD').attr('disabled', 'disabled');
		}
		$('#alinkGru').attr('disabled', 'disabled');
		$('#alinkProd').attr('disabled', 'disabled');
		$sel_segmentos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$sel_operacoes
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		if (view.attr('id') !== "pag-resumo-dia") {
			$sel_ddds
				.html("")
				.prop("disabled", true)
				.selectpicker('refresh');
		}
		$sel_grupos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$sel_produtos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');

	} else {
		//Marcando todas
		filtro.selectpicker('selectAll');
		$('#alinkApl').attr("data-var", "1");
		$('#alinkSeg').removeAttr('disabled');
		$('#alinkOp').removeAttr('disabled');
		if (view.attr('id') !== "pag-resumo-dia") {
			$('#alinkDDD').removeAttr('disabled');
		}
		$('#alinkGru').removeAttr('disabled');
		if (Estalo.filtros.filtro_produtos.length > 0 || produtoPorAplicacao) {
			$('#alinkProd').removeAttr('disabled');
		}


		//Preenchendo lista com todos os segmentos

		var options = [];

		//11/07/2014 Marcar todos agrupando segmentos
		var segs_por_apl = [];
		filtro.val().forEach(function (codigo) {
			segs_por_apl = segs_por_apl.concat(cache.aplicacoes[codigo]['segmentos'] !== undefined ? cache.aplicacoes[codigo].segmentos.map(function (seg) {
				return seg.codigo
			}) : []);
		});

		$.map(cache.gruposSegs, function (grupo) {
			var nome = obtemNomeGrupoSegmentos(grupo.codigo);
			options.push('<optgroup label="' + nome + '">');

			cache.segs.forEach(function (seg) {
				if (seg.grupo === grupo.codigo && (segs_por_apl.indexOf("" + seg.codigo) >= 0)) {
					options.push('<option value="' + seg.codigo + '">' + seg.nome + '</option>');
				}
			});
			$sel_segmentos.html(options.join());

			options.push('</optgroup>');
		});

		$sel_segmentos
			.prop("disabled", false)
			.selectpicker('refresh');

		//Preenchendo lista com todos as operacoes
		var options2 = [];

		cache.apls.map(function (apl) {
			if (cache.aplicacoes[apl.codigo] && cache.aplicacoes[apl.codigo].hasOwnProperty('operacoes')) {
				cache.aplicacoes[apl.codigo].operacoes.forEach(function (op) {
					var contem = (Estalo.filtros.filtro_operacoes.indexOf("" + op.codigo) >= 0);
					options2.push('<option value="' + op.codigo + '"' + (contem ? " selected" : "") + '>' + op.nome + '</option>');
				});
			}
		});

		$sel_operacoes.html(options2.join());

		$sel_operacoes
			.prop("disabled", false)
			.selectpicker('refresh');

		//Preenchendo lista com todos os grupos, produtos e ddds
		var options = [];
		cache.ddds.forEach(function (ddd) {
			var contem = (Estalo.filtros.filtro_ddds.indexOf("" + ddd.codigo) >= 0);
			var nome = ddd.nome.substring(0, 40) + (ddd.nome.length > 40 ? "..." : "");
			options.push('<option value="' + ddd.codigo + '"' + (contem ? " selected" : "") + '>' + nome + '</option>');
		});
		$sel_ddds.html(options.join());

		if (view.attr('id') !== "pag-resumo-dia") {
			$sel_ddds
				//.html("")
				.prop("disabled", false)
				.selectpicker('refresh');
		}

		var options2 = [];
		cache.grupos_produtos_vendas.forEach(function (gru) {
			var contem = (Estalo.filtros.filtro_grupos_produtos.indexOf("" + gru.codigo) >= 0);
			options2.push('<option value="' + gru.codigo + '"' + (contem ? " selected" : "") + '>' + gru.nome + '</option>');
		});
		$sel_grupos.html(options2.join());

		$sel_grupos
			//.html("")
			.prop("disabled", false)
			.selectpicker('refresh');

		var options3 = [];
		cache.produtos_vendas.forEach(function (prod) {
			var contem = (Estalo.filtros.filtro_produtos.indexOf("" + prod.codigo) >= 0);
			options3.push('<option value="' + prod.codigo + '"' + (contem ? " selected" : "") + '>' + prod.nome + '</option>');
		});
		$sel_produtos.html(options3.join());

		if (Estalo.filtros.filtro_produtos.length > 0 || produtoPorAplicacao) {
			$sel_produtos
				//.html("")
				.prop("disabled", false)
				.selectpicker('refresh');
		}
	}
}



function marcaTodasRegioes(filtro, view, scop) {

	testaIndices();

	//Desmarcando todas
	var valor = $(this).attr("data-var");
	var $sel_ddds = view.find("select.filtro-ddd");
	var $sel_ufs = view.find("select.filtro-uf");
	if (view.attr('id') === 'pag-resumo-880x144') {
		var $sel_segmentos = view.find("select.filtro-segmento");
	}
	if (valor === "1") {
		scop.porRegEDDD = false;
		scop.$apply();
		filtro.selectpicker('deselectAll');
		$(this).attr("data-var", "0");
		//$('#alinkDDD').attr('disabled', 'disabled');
		if (view.attr('id') === 'pag-resumo-880x144') {
			$('#alinkSeg').attr('disabled', 'disabled');
		}
		/* $sel_ddds
		.html("")
		.prop("disabled", true)
		.selectpicker('refresh'); */

		if (view.attr('id') === 'pag-resumo-880x144') {
			$sel_segmentos
				.html("")
				.prop("disabled", true)
				.selectpicker('refresh');
		}
		$('.btn.btn-pivot').prop("disabled", "false");
		$('#porRegEDDD').attr('disabled', 'disabled');
		$('#porRegEDDD').prop('checked', false);

	} else {

		//Marcando todas
		$('#HFiltroED').prop('checked', false);
		// scop.filtros.isHFiltroED = false;
		$('#oueED').attr('disabled', 'disabled');
		$('#oueED').prop('checked', false);
		var $sel_eds = view.find("select.filtro-ed");
		$sel_eds
			//.html("")
			.prop("disabled", "disabled")
			.selectpicker('refresh');

		filtro.selectpicker('selectAll');
		$(this).attr("data-var", "1");
		$('#alinkDDD').removeAttr('disabled');
		$('#alinkUf').removeAttr('disabled');

		if (view.attr('id') === 'pag-resumo-880x144') {
			$('#alinkSeg').removeAttr('disabled');
		}


		//Preenchendo lista com todos os DDDs
		var options = [];
		cache.ddds.forEach(function (d) {

			var nome = d.nome.substring(0, 40) + (d.nome.length > 40 ? "..." : "");
			options.push('<option value="' + d.codigo + '">' + nome + '</option>');

		});
		$sel_ddds.html(options.join());

		$sel_ddds
			//              .html("")
			.prop("disabled", false)
			.selectpicker('refresh');


		//Preenchendo lista com todas as UFs
		var options1 = [];
		cache.ufs.forEach(function (u) {
			options.push('<option value="' + u.codigo + '">' + u.nome + '</option>');
		});
		$sel_ufs.html(options1.join());
		$sel_ufs
			//              .html("")
			.prop("disabled", false)
			.selectpicker('refresh');

		$('#porRegEDDD').attr('disabled', false).selectpicker('refresh');

		if (view.attr('id') === 'pag-resumo-880x144') {

			var segs = $.map(cache.segs, function (seg) {
				if (cache.aplicacoes["880R1IN"].segmentos.map(function (seg) {
						return seg.codigo
					}).indexOf(seg.codigo) >= 0) {
					return seg;
				}
			});


			//Preenchendo lista com todos os segmentos
			var options2 = [];
			segs.forEach(function (seg) {
				//options2.push('<option data-divider="true"></option>');
				options2.push('<option value="' + seg.codigo + '">' + seg.nome + '</option>');
			});
			$sel_segmentos.html(options2.join());

			$sel_segmentos
				//              .html("")
				.prop("disabled", false)
				.selectpicker('refresh');
		}

		var filtro_regioes = $("select.filtro-regiao").val() || [];
		Estalo.filtros.filtro_regioes = filtro_regioes;

	}
}

function marcaTodasUFs(filtro, view, scop) {

	testaIndices();
	//Bernardo 20-02-2014 Marcarega todas as UFs
	var $btn = view.find(".btn-atualizarGRAS");
	$btn.prop("disabled", true);

	//Desmarcando todas
	var valor = $('#alinkUf').attr("data-var");
	var $sel_ddds = view.find("select.filtro-ddd");
	var $sel_gras = view.find("select.filtro-gras");

	if (valor === "1") {
		filtro.selectpicker('deselectAll');
		$('#alinkUf').attr("data-var", "0");
		$('#alinkDDD').attr('disabled', 'disabled');
		$('#alinkGRA').attr('disabled', 'disabled');
		$sel_ddds
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$sel_gras
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');

	} else {

		//Marcando todas
		filtro.selectpicker('selectAll');
		$('#alinkUf').attr("data-var", "1");
		$('#alinkDDD').removeAttr('disabled');
		//$('#alinkGRA').removeAttr('disabled');

		//Preenchendo lista com todos os ddds
		var options = [];
		cache.ddds.forEach(function (ddd) {
			var nome = ddd.nome.substring(0, 40) + (ddd.nome.length > 40 ? "..." : "");
			var contem = (Estalo.filtros.filtro_ddds.indexOf("" + ddd.codigo) >= 0);
			options.push('<option value="' + ddd.codigo + '"' + (contem ? " selected" : "") + '>' + nome + '</option>');
		});
		$sel_ddds.html(options.join());
		$sel_ddds
			//.html("")
			.prop("disabled", false)
			.selectpicker('refresh');
	}
}


function marcaTodosGrupos(filtro, view, scop) {

	testaIndices();

	//Desmarcando todas
	var valor = $('#alinkGru').attr("data-var");
	var $sel_produtos = view.find("select.filtro-produto");
	if (valor === "1") {
		$('.filtro-grupo-produto').selectpicker('deselectAll');
		$('#alinkGru').attr("data-var", "0");
		$('#alinkProd').attr('disabled', 'disabled');
		$sel_produtos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');

	} else {
		//Marcando todas
		$('.filtro-grupo-produto').selectpicker('selectAll');
		$('#alinkGru').attr("data-var", "1");
		$('#alinkProd').removeAttr('disabled');

		//Preenchendo lista com todos os produtos
		var options = [];
		cache.produtos_vendas.forEach(function (gru) {
			var contem = (Estalo.filtros.filtro_produtos.indexOf("" + gru.codigo) >= 0);
			options.push('<option value="' + gru.codigo + '"' + (contem ? " selected" : "") + '>' + gru.nome + '</option>');
		});
		$sel_produtos.html(options.join());

		$sel_produtos
			//.html("")
			.prop("disabled", false)
			.selectpicker('refresh');

	}
}




function carregaSegmentos(view, refresh) {
	//preenchendo list com os segmentos
	var options2 = cache.segs.map(function (array) {
		var contem = (Estalo.filtros.filtro_segmentos.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-segmento").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-segmento").html(options2.join());
	}
}

function carregaOperacoes(view, refresh) {	
	//Preenchendo lista com todos as operacoes
	var options2 = [];

	cache.apls.map(function (apl) {
		if (cache.aplicacoes[apl.codigo] && cache.aplicacoes[array.codigo].hasOwnProperty('operacoes')) {
			cache.aplicacoes[apl.codigo].operacoes.forEach(function (op) {
				var contem = (Estalo.filtros.filtro_operacoes.indexOf("" + op.codigo) >= 0);
				options2.push('<option value="' + op.codigo + '"' + (contem ? " selected" : "") + '>' + op.nome + '</option>');
			});
		}
	});

	if (refresh === true) {
		view.find("select.filtro-op").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-op").html(options2.join());
	}
}

function carregaSites(view, refresh) {
	//preenchendo list com os sites
	var options2 = cache.sites.map(function (array) {
		var contem = (Estalo.filtros.filtro_sites.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-site").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-site").html(options2.join());
	}
}

function carregaFontes(view, refresh) {
	//preenchendo list com os tipos de fontes(bases)
	var options2 = cache.fontes.map(function (array) {
		var contem = (Estalo.filtros.filtro_fontes.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-fonte").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-fonte").html(options2.join());
	}
}


function carregaIncentivos(view, refresh) {
	function testaIncentivo(){
		if(cache.servicos.length === 0){
			cache.carregaIncentivos(function(){ populaIncentivo();});			
		}else{
			populaIncentivo();	
		}
	}
	function populaIncentivo(){
		//preenchendo list com os incentivos
		var options2 = cache.incentivos.map(function (array) {
			var contem = (Estalo.filtros.filtro_incentivos.indexOf("" + array.codigo) >= 0);
			return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-incentivo").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-incentivo").html(options2.join());
		}
	}
	testaIncentivo();
}

function carregaProdutoPrepago(view, refresh) {
	function testaProdutoPrepago(){
		if(cache.produto_prepago.length === 0){
			cache.carregaProdutoPrepago(function(){ ProdutoPrepago();});			
		}else{
			ProdutoPrepago();	
		}
	}
	function ProdutoPrepago(){
			//preenchendo list com os Produto Prepago
			var options2 = cache.produto_prepago.map(function (array) {
				var contem = (Estalo.filtros.filtro_produto_prepago.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-produto-prepago").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-produto-prepago").html(options2.join());
			}
	}
	testaProdutoPrepago();	
}

function carregaServicos(view, refresh) {	
	function testaServico(){
		if(cache.servicos.length === 0){
			cache.carregaServicos(function(){ populaServico();});			
		}else{
			populaServico();	
		}
	}
	function populaServico(){
			//preenchendo list com os servicos
			var options2 = cache.servicos.map(function (array) {
				var contem = (Estalo.filtros.filtro_servicos.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-servico").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-servico").html(options2.join());
			}
	}
	testaServico();
}

function carregaBases(view, refresh) {
	function testaBase(){
		if(cache.bases.length === 0){
			cache.carregaBases(function(){ populaBase();});			
		}else{
			populaBase();	
		}
	}
	function populaBase(){
		//preenchendo list com os servicos
		var options2 = cache.bases.map(function (array) {
			var contem = (Estalo.filtros.filtro_bases.indexOf("" + array.codigo) >= 0);
			return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-base").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-base").html(options2.join());
		}
	}
	testaBase();
}

function carregaProdutosTU(view, refresh) {
	function testaProdutoTU(){
		if(cache.produtosTu.length === 0){
			cache.carregaProdutosTU(function(){ populaProdutoTU();});			
		}else{
			populaProdutoTU();	
		}
	}
	function populaProdutoTU(){
		 //preenchendo list com os produtos
		var options2 = cache.produtosTu.map(function (array) {		
			return '<option value="' + array.codigo + '">' + array.nome + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-produtotu").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-produtotu").html(options2.join());
		}
	}
	testaProdutoTU();
}

function carregaGruposMeta(view, refresh,scop) {
	function testaGruposMeta(){
		if(cache.carregaGruposMeta.length === 0){
			cache.GruposMeta(function(){ populaGruposMeta();});			
		}else{
			populaGruposMeta();	
		}
	}
	function populaGruposMeta(){
		
	var options = [];
	var gruposMeta = cache.gruposMeta
	gruposMeta.map(function (c) {
		if (scop.periodo.inicio >= new Date(c.inicio) && scop.periodo.inicio <= new Date(c.fim) ||
				scop.periodo.inicio < new Date(c.inicio) && scop.periodo.fim >= new Date(c.inicio)) {
		options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');
				}
	});

		
		if (refresh === true) {
			view.find("select.filtro-grupos-meta").html(options.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-grupos-meta").html(options.join());
		}
	}
	testaGruposMeta();
}

function carregaTiposAutomacoes(view, refresh) {
	function testaTipoAutomacao(){
		if(cache.tiposAutomacoes.length === 0){
			cache.carregaTiposAutomacoes(function(){ populaTiposAutomacao();});			
		}else{
			populaTiposAutomacao();	
		}
	}
	function populaTiposAutomacao(){
		//preenchendo list com os tipos de automações TU
		var options2 = cache.tiposAutomacoes.map(function (array) {		
			return '<option value="' + array.tipoAutomacao + '">' + array.tipoAutomacao + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-tipo-automacao").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-tipo-automacao").html(options2.join());
		}
	}
	testaTipoAutomacao();
	
}


function carregaCanaisM4U(view, refresh) {
	function testaCanaisM4U(){
		if(cache.canaisM4U.length === 0){
			cache.carregaCanaisM4U(function(){ populaCanaisM4U();});			
		}else{
			populaCanaisM4U();	
		}
	}
	function populaCanaisM4U(){
		//preenchendo list com os canais M4U
		var options2 = cache.canaisM4U.map(function (array) {		
			return '<option value="' + array.canal + '">' + array.canal + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-canal-recarga-m4u").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-canal-recarga-m4u").html(options2.join());
		}
	}
	testaCanaisM4U();
	
}

function carregaEmpresas(view, refresh) {
	function testaEmpresa(){
		if(cache.empresas.length === 0){
			cache.carregaEmpresas(function(){ populaEmpresa();});			
		}else{
			populaEmpresa();	
		}
	}
	function populaEmpresa(){
		 //preenchendo list com os empresas
		 var options2 = cache.empresas.map(function (array) {
			var contem = (Estalo.filtros.filtro_empresas.indexOf("" + array.codigo) >= 0);
			return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		 });
		 if (refresh === true) {
			view.find("select.filtro-empresa").html(options2.join()).selectpicker('refresh');
		 } else {
			view.find("select.filtro-empresa").html(options2.join());
		 }
	}
	testaEmpresa();	
}

function carregaIcsRepetidas(view, refresh,isTag) {
	function testaIcsRepetidas(){
		if(cache.icsRepetidas.length === 0){
			cache.carregaIcsRepetidas(function(){ populaIcsRepetidas(isTag);});			
		}else{
			populaIcsRepetidas(isTag);	
		}
	}
	function populaIcsRepetidas(isTag){
		 //preenchendo list com os empresas
		 var options2 = cache.icsRepetidas.map(function (array) {
			//Estalo.filtros.filtro_IcsRepetidas = [];
			if (!isTag || (isTag && array.RelatorioTag)){
			//	var contem = (Estalo.filtros.filtro_IcsRepetidas.indexOf("" + array.CodIc) >= 0);
				return '<option data-apl = "' + array.CodAplicacao + '"   value="' + array.CodIc + '">' + array.CodIc + ' - ' + array.Descricao +'</option>';
			}
		 });
		 if (refresh === true) {
			view.find("select.filtro-ics-repetidas").html(options2.join()).selectpicker('refresh');
		 } else {
			view.find("select.filtro-ics-repetidas").html(options2.join());
		 }
		 
	}
	testaIcsRepetidas();	
}

function carregaIcsRecVoz(view, refresh) {
	function testaIcsRecVoz(){
		if(cache.icsRecVoz.length === 0){
			cache.carregaIcsRecVoz(function(){ populaIcsRecVoz();});			
		}else{
			populaIcsRecVoz();	
		}
	}
	function populaIcsRecVoz(){
		 //preenchendo list com os empresas
		 var options2 = cache.icsRecVoz.map(function (array) {
			//Estalo.filtros.filtro_IcsRepetidas = [];
			//	var contem = (Estalo.filtros.filtro_IcsRepetidas.indexOf("" + array.CodIc) >= 0);
				return '<option data-apl = "' + array.CodAplicacao + '"   value="' + array.CodIc + '">' + array.CodIc + ' - ' + array.Descricao +'</option>';
		 });
		 if (refresh === true) {
			view.find("select.filtro-ics-recvoz").html(options2.join()).selectpicker('refresh');
		 } else {
			view.find("select.filtro-ics-recvoz").html(options2.join());
		 }
		 
	}
	testaIcsRecVoz();	
}

function carregaEmpresasRecarga(view, refresh) {
	
	function testaEmpresaRecarga(){
		if(cache.empresasRecarga.length === 0){
			cache.carregaEmpresasRecarga(function(){ populaEmpresaRecarga();});			
		}else{
			populaEmpresaRecarga();	
		}
	}
	function populaEmpresaRecarga(){		 
		 //preenchendo list com os empresas
		 var options2 = cache.empresasRecarga.map(function (array) {
	   
		 return '<option value="' + array.codigo + '">' + array.nome + '</option>';
		 });
		 if(refresh === true){
			view.find("select.filtro-empresaRecarga").html(options2.join()).selectpicker('refresh');
		 }else{
			view.find("select.filtro-empresaRecarga").html(options2.join());
		 }
	}
	testaEmpresaRecarga();
}


function carregaOperacoesECH(view, refresh) {
	function testaOperacaoECH(){
		if(cache.operacoes.length === 0){
			cache.carregaOperacoesECH(function(){ populaOperacaoECH();});			
		}else{
			populaOperacaoECH();	
		}
	}
	function populaOperacaoECH(){
		 //preenchendo list com as operacoes
		var options2 = cache.operacoes.map(function (array) {
			var contem = (Estalo.filtros.filtro_operacoes.indexOf("" + array.codigo) >= 0);
			return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		});
		if (refresh === true) {
			view.find("select.filtro-operacao").html(options2.join()).selectpicker('refresh');
		} else {
			view.find("select.filtro-operacao").html(options2.join());
		}
	}
	testaOperacaoECH();	
}

function carregaDDDs(view, refresh) {
	function testaDDD(){
		if(cache.ddds.length === 0){
			cache.carregaDDDs(function(){ populaDDD();});			
		}else{
			populaDDD();	
		}
	}
	function populaDDD(){
		 //preenchendo list com ddds
			var options = cache.ddds.map(function (array) {
				var contem = (Estalo.filtros.filtro_ddds.indexOf("" + array.codigo) >= 0);
				var nome = array.nome.substring(0, 47) + (array.nome.length > 47 ? "..." : "");
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-ddd").html(options.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-ddd").html(options.join());
			}
	}
	testaDDD();
}

function carregaAssuntos(view, refresh) {	
	function testaAssunto(){
		if(cache.assuntos.length === 0){
			cache.carregaAssuntos(function(){ populaAssunto();});			
		}else{
			populaAssunto();	
		}
	}
	function populaAssunto(){
			//preenchendo list com as assuntos
			var options2 = cache.assuntos.map(function (array) {
				var contem = (Estalo.filtros.filtro_assuntos.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-assunto").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-assunto").html(options2.join());
			}
	}
	testaAssunto();
}


function carregaSkills(view, refresh) {	
	function testaSkill(){
		if(cache.skills.length === 0){
			cache.carregaSkills(function(){ populaSkill();});			
		}else{
			populaSkill();	
		}
	}
	function populaSkill(){
			//preenchendo list com os skills
			var options2 = cache.skills.map(function (array) {
				var contem = (Estalo.filtros.filtro_skills.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-skill").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-skill").html(options2.join());
			}
	}
	testaSkill();	
}


function carregaRegras(view, refresh) {
	function testaRegra(){
		if(cache.regras.length === 0){
			cache.carregaRegras(function(){ populaRegra();});			
		}else{
			populaRegra();	
		}
	}
	function populaRegra(){
			//preenchendo list com os regras
			var options2 = cache.regras.map(function (array) {
				var contem = (Estalo.filtros.filtro_regras.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-regra").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-regra").html(options2.join());
			}
	}
	testaRegra();
}

function carregaLegados(view, refresh) {
	//preenchendo list com os legados
	var options2 = cache.legados.map(function (array) {
		var contem = (Estalo.filtros.filtro_legados.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-legado").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-legado").html(options2.join());
	}
}

function carregaReparos(view, refresh) {
	//preenchendo list com os reparos
	var options2 = cache.reparos.map(function (array) {
		var contem = (Estalo.filtros.filtro_reparos.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.nome + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-reparo").html(options2.join()).selectpicker('refresh');
		view.find("select.filtro-rep").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-reparo").html(options2.join());
		view.find("select.filtro-rep").html(options2.join());
	}
}


function carregaJsons(view, refresh) {
	
	view.find("select.filtro-jsons").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Variáveis'
        });
	//preenchendo list com os jsons
	var options2 = cache.varjson.map(function (array) {
		var contem = (Estalo.filtros.varjson.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-jsons").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-jsons").html(options2.join());
	}
}


function carregaDominios(view, refresh) {
	//preenchendo list com os dominios
	var options2 = cache.dominios.map(function (array) {
		return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-dominio").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-dominio").html(options2.join());
	}
}

function carregaUras(view, refresh) {
	function testaUra(){
		if(cache.uras.length === 0){
			cache.carregaUras(function(){ populaUra();});			
		}else{
			populaUra();	
		}
	}
	function populaUra(){
			//preenchendo list com as uras			
			var options2 = cache.uras.map(function (array) {
				var contem = (Estalo.filtros.filtro_uras.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-ura").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-ura").html(options2.join());
			}
	}
	testaUra();
	
}

function carregaParametros(view, refresh) {
	function testaParametro(){
		if(cache.parametros.length === 0){
			cache.carregaParametros(function(){ populaParametro();});			
		}else{
			populaParametro();	
		}
	}
	function populaParametro(){
			//preenchendo list com os parametros gra
			var options2 = cache.parametros.map(function (array) {
				//var contem = (Estalo.filtros.filtro_uras.indexOf("" + array.codigo) >= 0);
				return '<option value="' + array.codigo + '">' + array.nome + '</option>';
			});
			if (refresh === true) {
				view.find("select.filtro-parametros").html(options2.join()).selectpicker('refresh');
				view.find("select.filtro-param").html(options2.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-parametros").html(options2.join());
				view.find("select.filtro-param").html(options2.join());
			}
	}
	testaParametro();
	
}

function carregaParametrosOiTV(view, refresh) {
	//preenchendo list com os parametros gra

	var options2 = cache.parametrosOiTV.map(function (array) {
		//var contem = (Estalo.filtros.filtro_uras.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '">' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-parametros").html(options2.join()).selectpicker('refresh');
		view.find("select.filtro-param").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-parametros").html(options2.join());
		view.find("select.filtro-param").html(options2.join());
	}
}

function carregaUras(view, refresh) {
	//preenchendo list com as uras
	console.log('aqui passa');
	var options2 = cache.uras.map(function (array) {
		var contem = (Estalo.filtros.filtro_uras.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-ura").html(options2.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-ura").html(options2.join());
	}
}


function carregaRegioes(view, refresh) {

	var idview = view.attr('id');

	//preenchendo list com as regioes
	var options = regioes.map(function (array) {

		if (idview !== "pag-resumo-880x144" || array.codigo !== "ND") {
			var contem = (Estalo.filtros.filtro_regioes.indexOf("" + array.codigo) >= 0);
			return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
		}
	});
	if (refresh === true) {
		view.find("select.filtro-regiao").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-regiao").html(options.join());
	}
}

function carregaUFs(view, refresh) {
	function testaUF(){
		if(cache.ufs.length === 0){
			cache.carregaDDDs(function(){ populaUF();});			
		}else{
			populaUF();	
		}
	}
	function populaUF(){
			//preenchendo list com ufs
			var ordem = cache.ufs.sort(function (obj1, obj2) {
				if (obj1.codigo < obj2.codigo)
					return -1;
				if (obj1.codigo > obj2.codigo)
					return 1;
				return 0;
			});

			var options = [];

			for (var i = 0; i < ordem.length; i++) {
				var contem = false;
				options.push('<option value="' + ordem[i].codigo + '"' + (contem ? " selected" : "") + '>' + ordem[i].nome + '</option>');
			}

			if (refresh === true) {
				view.find("select.filtro-uf").html(options.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-uf").html(options.join());
			}
	}
	testaUF();
}

function carregaUFsModal(view, refresh) {
	var ordem = cache.ufs.sort(function (obj1, obj2) {
		if (obj1.codigo < obj2.codigo)
			return -1;
		if (obj1.codigo > obj2.codigo)
			return 1;
		return 0;
	});

	var options = [];

	for (var i = 0; i < ordem.length; i++) {
		var contem = false;
		options.push('<option value="' + ordem[i].codigo + '"' + (contem ? " selected" : "") + '>' + ordem[i].nome + '</option>');
	}

	if (refresh === true) {
		view.find("select.filtro-uf-Modal").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-uf-Modal").html(options.join());
	}

}

function carregaDigitos(view, refresh, grupo) {

	//Popula lista de últimos digitos
	var options = [];

	i = 0;
	options.push('<optgroup label="Último Dígito">');
	while (i <= 9) {
		//doisdigitos.push(i);
		if (i <= 9) {
			options.push('<option value="' + i + '" ' + ((grupo === "Último Dígito") ? " selected" : "") + '>' + i + '</option>');
		} else {
			options.push('<option value="' + i + '" ' + ((grupo === "Último Dígito") ? " selected" : "") + '>' + i + '</option>');
		}
		i++;
	}
	options.push('</optgroup>');

	/*var controle = ["0","1","2","3","4"];
	var piloto = ["5","6","7","8","9"];*/
	/* Removido
  options.push('<optgroup label="Controle">');
  ud.forEach(function (d) {
	var contem = (Estalo.filtros.filtro_ud.indexOf(""+d.codigo) >= 0);
	if(controle.indexOf(d.codigo) >= 0){
	  options.push('<option value="' + d.codigo + '" ' + ((grupo === "Controle" || contem) ? " selected" : "") + '>' + d.nome + '</option>');
	}
  });
  options.push('</optgroup>');

  options.push('<optgroup label="Piloto">');
  ud.forEach(function (d) {
	var contem = (Estalo.filtros.filtro_ud.indexOf(""+d.codigo) >= 0);
	if(piloto.indexOf(d.codigo) >= 0){
	  options.push('<option value="' + d.codigo + '" ' + ((grupo === "Piloto" || contem) ? " selected" : "") + '>' + d.nome + '</option>');
	}
  });
  options.push('</optgroup>');*/

	if (refresh === true) {
		view.find("select.filtro-ud").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-ud").html(options.join());
	}

	//Selecionar grupo de dígitos
	for (var i = 0; i < 2; i++) {
		$($('div>div.container > ul > li >>div> div.btn-group.filtro-ud .dropdown-menu.inner li dt')[i]).click(
			function () {
				//console.log($(this).context.textContent +" "+typeof $(this).context.textContent);
				carregaDigitos(view, true, $(this).context.textContent);
			}
		);
	}
}

function carregaDoisDigitos(view, refresh, grupo) {

	//Popula lista de últimos digitos
	var options = [];

	//var doisdigitos = [];

	i = 0;

	options.push('<optgroup label="Dois Dígitos">');
	while (i <= 99) {
		//doisdigitos.push(i);
		if (i <= 9) {
			options.push('<option value="0' + i + '" ' + ((grupo === "Dois Dígitos") ? " selected" : "") + '>0' + i + '</option>');
		} else {
			options.push('<option value="' + i + '" ' + ((grupo === "Dois Dígitos") ? " selected" : "") + '>' + i + '</option>');
		}
		i++;
	}
	options.push('</optgroup>');


	/*options.push('<optgroup label="Dois Dígitos">');
  ud.forEach(function (d) {
	var contem = (Estalo.filtros.filtro_ud.indexOf(""+d.codigo) >= 0);
    if(controle.indexOf(d.codigo) >= 0){
    }
  });
  options.push('</optgroup>');*/

	if (refresh === true) {
		view.find("select.filtro-udd").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-udd").html(options.join());
	}

	//Selecionar grupo de dígitos
	for (var i = 0; i < 2; i++) {
		$($('div>div.container > ul > li >>div> div.btn-group.filtro-udd .dropdown-menu.inner li dt')[i]).click(
			function () {
				//console.log($(this).context.textContent +" "+typeof $(this).context.textContent);
				carregaDoisDigitos(view, true, $(this).context.textContent);
			}
		);
	}
}

function carregaFalhas(view, refresh) {
	
	if(falhas.length === undefined){
		falhas = [
			{
				codigo: "Falhas",
				nome: "Falhas"
			},
			{
				codigo: "ErroAp",
				nome: "Erro Aplicação"
			},
			{
				codigo: "ErroCtg",
				nome: "Contingência"
			},
			{
				codigo: "SemPmt",
				nome: "Sem Prompt"
			},
			{
				codigo: "SemDig",
				nome: "Sem Interação"
			},
			{
				codigo: "ErroVendas",
				nome: "Erro Vendas"
			}
		];
	}
	
	view.find("select.filtro-falhas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Falhas'
        });

	//Popula lista de falhas
	var options = [];
	falhas.forEach(function (f) {
		if (f.nome === "Falhas") {
			options.push('<option data-hidden="true" value="' + f.codigo + '">' + f.nome + '</option>');
		} else {
			options.push('<option value="' + f.codigo + '">' + f.nome + '</option>');
		}

	});
	if (refresh === true) {
		view.find("select.filtro-falhas").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-falhas").html(options.join());
	}
}

function carregaFalhas2(view, refresh) {
	
	if(falhas.length === undefined){
		falhas = [
			{
				codigo: "Falhas",
				nome: "Falhas"
			},
			{
				codigo: "ErroAp",
				nome: "Erro Aplicação"
			},
			{
				codigo: "ErroCtg",
				nome: "Contingência"
			},
			{
				codigo: "SemPmt",
				nome: "Sem Prompt"
			},
			{
				codigo: "SemDig",
				nome: "Sem Interação"
			},
			{
				codigo: "ErroVendas",
				nome: "Erro Vendas"
			}
		];
	}
	
	view.find("select.filtro-falhas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Falhas'
        });

	//Popula lista de falhas
	var options = [];
	falhas.forEach(function (f) {
		if (f.nome === "Falhas") {
			options.push('<option data-hidden="true" value="' + f.codigo + '">' + f.nome + '</option>');
		} else {
			options.push('<option value="' + f.codigo + '">' + f.nome + '</option>');
		}

	});
	if (refresh === true) {
		view.find("select.filtro-falhas").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-falhas").html(options.join());
	}
}

function carregaTipo(view, refresh) {

	//Popula lista de falhas
	var options = [];
	tipo_transferid.forEach(function (f) {
		if (f.nome === "Tipo") {
			options.push('<option data-hidden="true" value="' + f.codigo + '">' + f.nome + '</option>');
		} else {
			options.push('<option value="' + f.codigo + '">' + f.nome + '</option>');
		}

	});
	if (refresh === true) {
		view.find("select.filtro-tipo").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-tipo").html(options.join());
	}
}

function carregaClusters(view, refresh) {

	//Popula lista de clustes
	var options = [];
	clusters.forEach(function (c) {
		if (c.nome === "Cluster") {
			options.push('<option data-hidden="true" value="' + c.codigo + '">' + c.nome + '</option>');
		} else {
			options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');
		}

	});
	if (refresh === true) {
		view.find("select.filtro-cluster").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-cluster").html(options.join());
	}
}

function carregaMotivos(view, refresh) {

	//Popula lista de motivos
	var options = [];
	motivos.forEach(function (c) {
		//if (c.nome === "Motivo") {
		options.push('<option data-hidden="true" value="' + c.codigo + '">' + c.nome + '</option>');
		//} else {
		options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');
		//}
	});
	if (refresh === true) {
		view.find("select.filtro-motivo").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-motivo").html(options.join());
	}
}

function carregaNotas(view, refresh) {

	//Popula lista de lista_notas
	var options = [];
	lista_notas.forEach(function (c) {
		options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');		
	});
	if (refresh === true) {
		view.find("select.filtro-nota").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-nota").html(options.join());
	}
}

function carregaProdutosPesquisa(view, refresh) {

	//Popula lista de lista_produtos
	var options = [];
	lista_produtos.forEach(function (c) {
		options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');

	});
	if (refresh === true) {
		view.find("select.filtro-produto-pesquisa").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-produto-pesquisa").html(options.join());
	}
}

function carregaEntradas(view, refresh) {

	//Popula lista de entradas
	var options = [];
	entradas.forEach(function (c) {
		//if (c.nome === "Entrada") {
		options.push('<option data-hidden="true" value="' + c.codigo + '">' + c.nome + '</option>');
		//} else {
		options.push('<option value="' + c.codigo + '">' + c.nome + '</option>');
		//}
	});
	if (refresh === true) {
		view.find("select.filtro-entrada").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-entrada").html(options.join());
	}
}

function carregaResultados(view, scop, refresh) {
	var idview = view.attr('id');
	//Popula lista de resultados
	var options = status_vendas.map(function (array) {
		var contem = (Estalo.filtros.filtro_resultados.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.codigo + " - " + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-status").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-status").html(options.join());
	}
}

function carregaResultadosNaoAbordados(view, scop, refresh) {
	var idview = view.attr('id');
	//Popula lista de resultados
	var options = status_nao_abordado.map(function (array) {
		var contem = (Estalo.filtros.filtro_nao_abordado.indexOf("" + array.codigo) >= 0);
		return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.nome + '</option>';
	});
	if (refresh === true) {
		view.find("select.filtro-status-nao-abordado").html(options.join()).selectpicker('refresh');
	} else {
		view.find("select.filtro-status-nao-abordado").html(options.join());
	}
}

function carregaErros(view, refresh) {
	
	function testaeErros(){
		if(cache.erros.length === 0){
			cache.carregaErros(function(){ populaErro();});			
		}else{
			populaErro();	
		}
	}
	function populaErro(){
			// Popula lista de erros
			var arrays = cache.erros;

			arrays.sort(function (primeiro, segundo) {
				return +primeiro.codigo - +segundo.codigo;
			});

			var indices = unique(arrays.map(function (array) {
				return array.codigo;
			}));

			var newArray = [];
			indices.forEach(function (indice) {
				var nome = "";
				arrays.forEach(function (array) {
					if (array.codigo === indice) {
						nome += array.nome + "/";
					}
				});
				nome = nome.substring(0, nome.length - 1);
				var n = unique(nome.split('/'));
				n = n.join("/");
				newArray.push({
					codigo: indice,
					nome: n
				});
			});


			var options = newArray.map(function (array) {
				var contem = (Estalo.filtros.filtro_erros.indexOf("" + array.codigo) >= 0);
				var nome = array.nome.substring(0, 47) + (array.nome.length > 47 ? "..." : "");
				return '<option value="' + array.codigo + '"' + (contem ? " selected" : "") + '>' + array.codigo + " - " + nome + '</option>';

			});

			var unicos = unique(options);

			if (refresh === true) {
				view.find("select.filtro-erro").html(options.join()).selectpicker('refresh');
			} else {
				view.find("select.filtro-erro").html(unicos.join());
			}

			//16/04/2014 Gabriel - Popula lista de erros
			//      var arrays = [$scope.erros.IN, $scope.erros.M4U];
			//      var options7 = arrays.map(function (array) {
			//        return array.map(function (err) {
			//          var contem = (Estalo.filtros.filtro_erros.indexOf(""+err.codigo) >= 0);
			//          var nome = err.nome.substring(0, 47) + (err.nome.length > 47 ? "..." : "");
			//          return '<option value="' + err.codigo + '"' + (contem ? " selected" : "") + '>' + err.codigo + " - " + nome + '</option>';
			//        });
			//      });
	//      var unicos = unique(options7[0].concat(options7[1]));
	//         .find("select.filtro-erro").html(unicos.join());
	}
	testaeErros();
}

function carregaProdutosPorGrupo(scop, view, inicia) {

	testaIndices();

	var $sel_produtos = view.find("select.filtro-produto");
	var apls = view.find("select.filtro-aplicacao").val() || [];
	var prods = [];

	if (apls.length > 0) {
		apls.map(function (a) {
			return cache.aplicacoes[a].produtos.map(function (prod) {
				prods.push(prod.codigo)
			})
		});
	}

	var filtro_produtos = {};
	($sel_produtos.val() || []).forEach(function (codigo) {
		filtro_produtos[codigo] = true;
	});

	var filtro_grupo_produtos = $("select.filtro-grupo-produto").val() || [];
	if (filtro_grupo_produtos.length === 0) {
		$sel_produtos
			.html("")
			.prop("disabled", true)
			.selectpicker('refresh');
		$('#alinkProd').attr('disabled', 'disabled'); //Bernardo 20-02-2014
		return;
	}

	var filtro_produtos = {};
	($sel_produtos.val() || []).forEach(function (codigo) {
		filtro_produtos[codigo] = true;
	});

	view.find("select.filtro-produto").selectpicker({
		selectedTextFormat: 'count',
		countSelectedText: '{0} Produtos'
	});

	var options = [];
	var v = [];

	if (Estalo.filtros.filtro_grupos_produtos.length > 0 && inicia === true) {
		Estalo.filtros.filtro_grupos_produtos.forEach(function (codigo) {
			v = v.concat(cache.grupos_produtos_vendas[cache.grupos_produtos.indexOf(codigo)]['produtos'] || []);
		});
	} else {
		filtro_grupo_produtos.forEach(function (codigo) {
			v = v.concat(cache.grupos_produtos_vendas[cache.grupos_produtos.indexOf(codigo)]['produtos'] || []);
		});
	}

	unique(v).forEach((function (filtro_produtos) {
		return function (codigo) {
			var prod = cache.produtos_vendas.indice[codigo];
			var contem = (Estalo.filtros.filtro_produtos.indexOf("" + prod.codigo) >= 0);
			if (prods.length > 0 && prods.indexOf(codigo) >= 0) {
				options.push('<option value="' + prod.codigo + '"' + (contem ? " selected" : "") + '>' + prod.nome + '</option>');
			}
		};
	})(filtro_produtos));
	$sel_produtos.html(options.join());

	$sel_produtos
		.prop("disabled", false)
		.selectpicker('refresh');
	$('#alinkProd').removeAttr('disabled'); //Bernardo 20-02-2014
}
//}