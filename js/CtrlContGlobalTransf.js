function CtrlContGlobalTransf($scope, $globals) {

    win.title="Contagem Global de Transferências"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-contGlobalTransf", "Contagem Global de Transferencias");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
        
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013
        sites: [],
        segmentos: [],
        isHFiltroED: false,
        porDia: false,
        porDDD: false
    };

    // Filtro: ddd
    $scope.ddds = cache.ddds;

    $scope.porDDD = null;

    //02/06/2014 Flag
    $scope.porRegEDDD = $globals.agrupar;

    $scope.aba = 2;


    var empresas = cache.empresas.map(function(a){return a.codigo});

    $scope.valor = "empresa";



    function geraColunas(){
      var array = [];


      
      array.push(
      { field: "DatReferencia", displayName: "Data", width: 150, pinned: true },
         // { field: "NumPassos", displayName: "Qtd de Chamadas Transferidas", width: 50 },
           { field: "QtdChamadas", displayName: "Transferidas", width: 110 , pinnable : false },
             //{ field: "QtdChamadas0", displayName: "Op 0", width: 95 , pinnable : false },
               { field: "QtdChamadas1", displayName: "Op 1", width: 95 , pinnable : false },
                 { field: "QtdChamadas2", displayName: "Op 2", width: 95 , pinnable : false },
                   { field: "QtdChamadas3", displayName: "Op 3", width: 55 , pinnable : false },
                     { field: "QtdChamadas4", displayName: "Op 4", width: 55 , pinnable : false },
                       { field: "QtdChamadas5", displayName: "Op 5", width: 55 , pinnable : false },
                         { field: "QtdChamadas6", displayName: "Op 6", width: 55 , pinnable : false },
                           { field: "QtdChamadas7", displayName: "Op 7", width: 55 , pinnable : false },
                             { field: "QtdChamadas8", displayName: "Op 8", width: 55 , pinnable : false },
                               { field: "QtdChamadas9", displayName: "Op 9", width: 55 , pinnable : false },
                                 { field: "QtdChamadas10", displayName: "Op 10", width: 55 , pinnable : false },
                                   { field: "QtdChamadas11OuMais", displayName: "Op 11+", width: 70 , pinnable : false },
                                    //{ field: "perc0", displayName: "% Op 0", width: 80 , pinnable : false },
                                     { field: "perc1", displayName: "% Op 1", width: 80 , pinnable : false },
                                      { field: "perc2", displayName: "% Op 2+", width: 80 ,  pinnable : false }
           // { field: "Percent", displayName: "% Chamadas", width: 100 }
       );

       
             // if ($(".filtro-empresa").val() ==null){
        // array.push({ field: "AeC", displayName: "AeC", width: 150, pinned: true },
        // { field: "BTCC", displayName: "BTCC", width: 150, pinned: true },
        // { field: "CONTAX", displayName: "CONTAX", width: 150, pinned: true });
       // }
       
       // if (($(".filtro-empresa").val() || []).indexOf("AeC")>=0){
        // array.push({ field: "AeC", displayName: "AeC", width: 150, pinned: true });
       // }
        // if (($(".filtro-empresa").val() || []).indexOf("BTCC")>=0){
        // array.push({ field: "BTCC", displayName: "BTCC", width: 150, pinned: true });
       // }
        // if (($(".filtro-empresa").val() || []).indexOf("CONTAX")>=0){
      // array.push({ field: "CONTAX", displayName: "CONTAX", width: 150, pinned: true });
       // }
             

      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-contGlobalTransf");
      // treeView('#pag-contGlobalTransf #chamadas', 15,'Chamadas','dvchamadas');
      // treeView('#pag-contGlobalTransf #repetidas', 35,'Repetidas','dvRepetidas');
      // treeView('#pag-contGlobalTransf #ed', 65,'ED','dvED');
      // treeView('#pag-contGlobalTransf #ic', 95,'IC','dvIC');
      // treeView('#pag-contGlobalTransf #tid', 115,'TID','dvTid');
      // treeView('#pag-contGlobalTransf #vendas', 145,'Vendas','dvVendas');
      // treeView('#pag-contGlobalTransf #falhas', 165,'Falhas','dvFalhas');
      // treeView('#pag-contGlobalTransf #extratores', 195,'Extratores','dvExtratores');
      // treeView('#pag-contGlobalTransf #parametros', 225,'Parametros','dvParam');
      // treeView('#pag-contGlobalTransf #admin', 255,'Administrativo','dvAdmin');
      // treeView('#pag-contGlobalTransf #monitoracao', 275, 'Monitoração', 'dvReparo');
      // treeView('#pag-contGlobalTransf #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
      // treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');
        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');*/


       $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});


        $(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
        $(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
        $('.navbar-inner').css('height','70px');
        $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

        //minuteStep: 5

      //19/03/2014
      componenteDataHora ($scope,$view);

        carregaRegioes($view);
        carregaAplicacoes($view,false,false,$scope);
        carregaSites($view);
        carregaEmpresas($view);
        carregaRegras($view);
        carregaSkills($view);



        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


      carregaDDDsPorAplicacao($scope,$view,true);
      // Popula lista de  DDDs a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function(){ carregaDDDsPorAplicacao($scope,$view)});


      $view.on("change","input:radio[name=radioB]",function() {

        if($(this).val() === "empresa"){
          $('#porDDD').attr('disabled', 'disabled');
          $('#porDDD').prop('checked',false);
        }else{
          $('#porDDD').attr('disabled', false);
        }

      });


        /*$view.find(".selectpicker").selectpicker({
        //noneSelectedText: 'Nenhum item selecionado',
        countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-ed").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} EDs'
        });


        $view.find("select.filtro-empresa").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Empresas'
        });

        $view.find("select.filtro-regra").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Regras'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-ddd", function () {
            var filtro_ddds = $(this).val() || [];
            Estalo.filtros.filtro_ddds = filtro_ddds;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

      //2014-11-27 transição de abas
      var abas = [2,3,4];

      $view.on("click", "#alinkAnt", function () {

        if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
        if($scope.aba > abas[0]){
          $scope.aba--;
          mudancaDeAba();
        }
      });

      $view.on("click", "#alinkPro", function () {

        if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

        if($scope.aba < abas[abas.length-1]){
          $scope.aba++;
          mudancaDeAba();
        }

      });

      function mudancaDeAba(){
        abas.forEach(function(a){
          if($scope.aba === a){
            $('.nav.aba'+a+'').fadeIn(500);
          }else{
            $('.nav.aba'+a+'').css('display','none');
          }
        });
      }


        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        // Marca todos os empresas
        $view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});

        // Marca todos os regras
        $view.on("click", "#alinkRegra", function(){ marcaTodosIndependente($('.filtro-regra'),'regra')});

        // Marca todos os skills
        $view.on("click", "#alinkSkill", function(){ marcaTodosIndependente($('.filtro-skill'),'skill')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        // GILBERTO 18/02/2014



      // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.btn-group.filtro-ddd>div>ul').css({'max-height':'600px','overflow-y':'auto','min-height':'1px'});
          }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
            $('div.btn-group.filtro-segmento').addClass('open');
            $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.dropdown-menu.open').css({ 'margin-left':'-110px' });
            $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.dropdown-menu.open').css({ 'margin-left':'0px' });
            $('div.btn-group.filtro-ddd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
            $('div.btn-group.filtro-empresa').addClass('open');

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
            $('div.btn-group.filtro-empresa').removeClass('open');

        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseover(function () {
            $('div.btn-group.filtro-regra').addClass('open');
            $('div.btn-group.filtro-regra>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseout(function () {
            $('div.btn-group.filtro-regra').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseover(function () {
            $('div.btn-group.filtro-skill').addClass('open');
            $('div.btn-group.filtro-skill>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseout(function () {
            $('div.btn-group.filtro-skill').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ed').addClass('open');
                $('div.btn-group.filtro-ed>div>ul').css(
                  { 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' }
                );
            }
        });



        // OCULTAR AO TIRAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseout(function () {
            $('div.btn-group.filtro-ed').removeClass('open');
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-contGlobalTransf");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-contGlobalTransf");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });







        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
      $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };








    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_ed = $view.find("select.filtro-ed").val() || [];
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_regras = $view.find("select.filtro-regra").val() || [];
        var filtro_skills = $view.find("select.filtro-skill").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
        if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
        if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }
        if (filtro_regras.length > 0) { $scope.filtros_usados += " Regras: " + filtro_regras; }
        if (filtro_skills.length > 0) {

          var filtro_skills_mod = [];
          for(var i = 0; i < filtro_skills.length; i++){
            var lsRegExp = /%/g;
            filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp,'&lt;'));
          }
          $scope.filtros_usados += " Skills: " + filtro_skills_mod;
        }

        if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
          var options = [];
          var v = [];
          filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
          unique(v).forEach((function (filtro_ddds) {
            return function (codigo) {
              var ddd = cache.ddds.indice[codigo];
              filtro_ddds.push(codigo);
            };
          })(filtro_ddds));
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
        }else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
          $scope.filtros_usados += " Regiões: " + filtro_regioes;
          $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }


        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };


      empSelect = "";
      empSelectIn = "";
      empObjParts = "";
      $scope.QtdCONTAX = 0;
       $scope.QtdAEC = 0;
        $scope.QtdBTCC = 0;
      

      if($scope.valor === "empresa"){

      for(var i= 0; i<empresas.length;i++){ empSelect+="["+empresas[i]+"] as "+empresas[i]+"," }
      for(var i= 0; i<empresas.length;i++){ empSelectIn+="["+empresas[i]+"]," }
      for(var i= 0; i<empresas.length;i++){ empObjParts+= empresas[i]+ ":" +empresas[i]+"," }

      empSelect = empSelect.substring(0,empSelect.length-1);
      empSelectIn = empSelectIn.substring(0,empSelectIn.length-1);
      empObjParts = empObjParts.substring(0,empObjParts.length-1);

      
       // CONSULTA SQL 1.0
      // stmt = db.use
      // + " SELECT NumPassos, SUM(QtdChamadas) AS QtdChamadas"
      // + " FROM ResumoECHPassosDia"
      // + " WHERE 1 = 1"
      // + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      // + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
      // stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      // stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      // stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
      // + "GROUP BY NumPassos"
      // + " ORDER BY NumPassos";
      
       // CONSULTA SQL 1.1
         // stmt = db.use
      // + " SELECT NumPassos, ISNULL([AeC], 0)+ISNULL([BTCC], 0)+ISNULL([CONTAX], 0) AS QtdChamadas, ISNULL([AeC], 0) as AeC, ISNULL([BTCC], 0)  as BTCC,ISNULL([CONTAX], 0) as CONTAX"
      // + " FROM ("
      // + "SELECT NumPassos, QtdChamadas, CodEmpresa , DatReferencia FROM ResumoECHPassosDia "
      // + "WHERE 1 = 1 "
      // +" AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      // + " AND DatReferencia <= '" + formataDataHora(data_fim) +  "'"

      // stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      // stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      // stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
      // +")"
      // +"p PIVOT ( SUM (qtdchamadas) FOR codempresa IN ( [AeC],[BTCC],[CONTAX] )) AS pvt ORDER BY DatReferencia;";
     
      var tabelaConsultar = "ResumoECHPassos";
      if ($scope.filtros.porDia) {
        tabelaConsultar = "ResumoECHPassosDia";
      }
        // CONSULTA SQL 1.1
            // stmt = db.use
      // + " SELECT DatReferencia, NumPassos, ISNULL([AeC], 0)+ISNULL([BTCC], 0)+ISNULL([CONTAX], 0) AS QtdChamadas, ISNULL([AeC], 0) as AeC, ISNULL([BTCC], 0)  as BTCC,ISNULL([CONTAX], 0) as CONTAX"
      // + " FROM ("
      // + "SELECT NumPassos, QtdChamadas, CodEmpresa , DatReferencia FROM " +tabelaConsultar+ " "
      // + "WHERE 1 = 1 "
      // +" AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      // + " AND DatReferencia <= '" + formataDataHora(data_fim) +  "'"

      // stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
      // stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      // stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
      // +")"
      // +"p PIVOT ( SUM (qtdchamadas) FOR codempresa IN ( [AeC],[BTCC],[CONTAX] )) AS pvt ORDER BY DatReferencia;";
      
      
      
      
                  stmt = db.use
      + " SELECT DatReferencia, "
      + " SUM(QtdChamadas) as QtdChamadas,"
      + "  sum(case when NumPassos = 0 then QtdChamadas else 0 end) as QtdChamadas0,"
      + " sum(case when NumPassos = 1 then QtdChamadas else 0 end) as QtdChamadas1,"
      + "  sum(case when NumPassos = 2 then QtdChamadas else 0 end) as QtdChamadas2,"
      + "  sum(case when NumPassos = 3 then QtdChamadas else 0 end) as QtdChamadas3,"
      + "   sum(case when NumPassos = 4 then QtdChamadas else 0 end) as QtdChamadas4,"
      + "  sum(case when NumPassos = 5 then QtdChamadas else 0 end) as QtdChamadas5,"
      + "  sum(case when NumPassos = 6 then QtdChamadas else 0 end) as QtdChamadas6,"
      + "  sum(case when NumPassos = 7 then QtdChamadas else 0 end) as QtdChamadas7,"
      + " sum(case when NumPassos = 8 then QtdChamadas else 0 end) as QtdChamadas8,"
      + " sum(case when NumPassos = 9 then QtdChamadas else 0 end) as QtdChamadas9,"
      + "  sum(case when NumPassos = 10 then QtdChamadas else 0 end) as QtdChamadas10,"
      + "  sum(case when NumPassos >= 11 then QtdChamadas else 0 end) as QtdChamadas11OuMais "
      + " from  " + tabelaConsultar+ " "
      + " WHERE 1 = 1 "
      +" AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
      + " AND DatReferencia <= '" + formataDataHora(data_fim) +  "'"
      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
	  stmt += restringe_consulta("CodSite", filtro_sites, true)
      stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
      stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
      +" GROUP BY DatReferencia; ";
          
      // SELECT NumPassos, ISNULL([AeC], 0)+ISNULL([BTCC], 0)+ISNULL([CONTAX], 0) AS QtdChamadas, ISNULL([AeC], 0) as AeC, ISNULL([BTCC], 0)  as BTCC,ISNULL([CONTAX], 0) as CONTAX  FROM 
// (
// SELECT NumPassos, QtdChamadas, CodEmpresa , DatReferencia 
// FROM ResumoECHPassosDia 
// WHERE 1 = 1 AND DatReferencia >= '2016-06-07 00:00:00' AND DatReferencia <= '2016-12-05 23:59:59'
// )
 // p PIVOT ( SUM (qtdchamadas) FOR codempresa IN ( [AeC],[BTCC],[CONTAX] )) AS pvt ORDER BY DatReferencia 
      
      // SELECT NumPassos, QtdChamadas
      // FROM ResumoECHPassosDia
      // ORDER BY NumPassos;
      }

try{
        executaQuery = executaQuery2;
        log(stmt);
}
catch(err){
  console.log(err);
}

        function formataData2(data) {
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        
        $scope.QtdChamadasTotal = 0;
        $scope.vetorQtdChamadas = [];
         var  NumPassos =0;
          var  QtdChamadas =0; 
          var QtdAEC =0;
          var QtdBTCC =0;
          var QtdCONTAX =0;
        function executaQuery2(columns){

                // GERA DADOS COLUNAS  
                DatReferencia = columns["DatReferencia"] !== undefined ? columns["DatReferencia"].value : undefined;
                // NumPassos = columns["NumPassos"] !== undefined ? columns["NumPassos"].value : undefined,
                QtdChamadas = columns["QtdChamadas"] !== undefined ? columns["QtdChamadas"].value : undefined;
                // QtdAEC = columns["AeC"] !== undefined ? columns["AeC"].value : undefined;
                // QtdBTCC = columns["BTCC"] !== undefined ? columns["BTCC"].value : undefined;
                // QtdCONTAX = columns["CONTAX"] !== undefined ? columns["CONTAX"].value : undefined;
                QtdChamadas0 = columns["QtdChamadas0"] !== undefined ? columns["QtdChamadas0"].value : undefined;
                QtdChamadas1 = columns["QtdChamadas1"] !== undefined ? columns["QtdChamadas1"].value : undefined;
                QtdChamadas2 = columns["QtdChamadas2"] !== undefined ? columns["QtdChamadas2"].value : undefined;
                QtdChamadas3 = columns["QtdChamadas3"] !== undefined ? columns["QtdChamadas3"].value : undefined;
                QtdChamadas4 = columns["QtdChamadas4"] !== undefined ? columns["QtdChamadas4"].value : undefined;
                QtdChamadas5 = columns["QtdChamadas5"] !== undefined ? columns["QtdChamadas5"].value : undefined;
                QtdChamadas6 = columns["QtdChamadas6"] !== undefined ? columns["QtdChamadas6"].value : undefined;
                QtdChamadas7 = columns["QtdChamadas7"] !== undefined ? columns["QtdChamadas7"].value : undefined;
                QtdChamadas8 = columns["QtdChamadas8"] !== undefined ? columns["QtdChamadas8"].value : undefined;
                QtdChamadas9 = columns["QtdChamadas9"] !== undefined ? columns["QtdChamadas9"].value : undefined;
                QtdChamadas10 = columns["QtdChamadas10"] !== undefined ? columns["QtdChamadas10"].value : undefined;
                QtdChamadas11OuMais = columns["QtdChamadas11OuMais"] !== undefined ? columns["QtdChamadas11OuMais"].value : undefined;
                
    
                var perc0 = 0;
                 var perc1 = 0;
                 var perc2 = 0;
                 var tempT = QtdChamadas2+QtdChamadas3+QtdChamadas4+QtdChamadas5+QtdChamadas6+QtdChamadas7+QtdChamadas8+QtdChamadas9+QtdChamadas10+QtdChamadas11OuMais;
     
              perc0 = ((QtdChamadas0/QtdChamadas)*100).toFixed(1);
                  perc1 = ((QtdChamadas1/QtdChamadas)*100).toFixed(1);
                   perc2 = (((QtdChamadas-(QtdChamadas0+QtdChamadas1))/QtdChamadas)*100).toFixed(1);
                
                 $scope.vetorQtdChamadas.push(perc2);
                 $scope.QtdChamadasTotal += QtdChamadas;   
                 
                    if (!$scope.filtros.porDia) { DatReferencia = formataDataHoraBR(DatReferencia);} else{DatReferencia = formataDataBR(DatReferencia); }
                
                      $scope.dados.push({
                        DatReferencia : DatReferencia,
                        NumPassos: NumPassos,
                         QtdChamadas: QtdChamadas,
                        QtdChamadas0: QtdChamadas0,
                        QtdChamadas1: QtdChamadas1,
                       QtdChamadas2: QtdChamadas2,
                       QtdChamadas3: QtdChamadas3,
                       QtdChamadas4: QtdChamadas4,
                       QtdChamadas5: QtdChamadas5,
                       QtdChamadas6: QtdChamadas6,
                       QtdChamadas7: QtdChamadas7,
                       QtdChamadas8: QtdChamadas8,
                       QtdChamadas9: QtdChamadas9,
                       QtdChamadas10: QtdChamadas10,
                       QtdChamadas11OuMais: QtdChamadas11OuMais,
                        perc0 : perc0,
                         perc1 : perc1,
                          perc2 : perc2
                        // PercentTotal :PercentTotal
                    });
                 

                    // FIM GERA DADOS COLUNAS
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
           if($scope.dados.length%1000===0){
                $scope.$apply();
            }
          }

                db.query(stmt,executaQuery, function (err, num_rows) {
                  userLog(stmt, "Período "+data_ini+" até "+data_fim , 2, err);

                  if($scope.porRegEDDD===true){
                    $('#pag-contGlobalTransf table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1170px','margin-bottom':'100px'});
                  }else{
                    $('#pag-contGlobalTransf table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1280px','margin-bottom':'100px'});
                  }
                    console.log("Executando query-> "+stmt+" "+num_rows);


                  var datFim = "";
                  if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1>limiteHoras/24){
                    // datFim = " Botão transpor habilitado para intervalos de "+limiteHoras/24+" dias.";
                                        datFim = "";
                  }
                  // CALCULA PORCENTAGEM COLUNA
                  var PercentTotal =0;
                  $scope.naoDerivadas=0;
                  $scope.Derivadas=0;
                  for(var i=0;i<$scope.dados.length;i++){
                
                        $scope.dados[i].Percent =(($scope.vetorQtdChamadas[i]/$scope.QtdChamadasTotal)*100).toFixed(2);
                         if($scope.dados[i].NumPassos==0){
                            $scope.naoDerivadas = $scope.dados[i].Percent;
                         }
                           if($scope.dados[i].NumPassos==1){
                            $scope.Derivadas = $scope.dados[i].Percent;
                         }
                  }
                  $scope.Transferidas = 100 - $scope.naoDerivadas - $scope.Derivadas;
                  $scope.Transferidas = $scope.Transferidas.toFixed(2);
                  // $scope.dados.push(
                   // {'NumPassos' : ' ', 'QtdChamadas' : " " },
                    // {'NumPassos' : 'Total Geral :', 'QtdChamadas' :  " "+$scope.QtdChamadasTotal+" " },
                     // {'NumPassos' : 'Não Encontradas :', 'QtdChamadas' :  " "+ $scope.naoDerivadas+" % "   },
                      // {'NumPassos' : 'Derivadas :', 'QtdChamadas' :  " "+$scope.Derivadas +" %" },
                       // {'NumPassos' : 'Transferidas :', 'QtdChamadas' :  " "+$scope.Transferidas +" %" }
                  
                  // );
                
                  //FIM Gera Grid
                    retornaStatusQuery(num_rows, $scope, datFim);
                    $btn_gerar.button('reset');
                  if(num_rows>0){
                    $btn_exportar.prop("disabled", false);
                  }

                  //if(executaQuery !== executaQuery2){
                    $('.btn.btn-pivot').prop("disabled","false");
                    if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1<=limiteHoras/24 && num_rows>0){
                        $('.btn.btn-pivot').button('reset');
                    }
                  //}

                  if($scope.valor === "regra"){
                    // Preencher com zero datas inexistentes em cada erro
                $scope.dados.forEach(function (dado) {
                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = { qtd_entrantes: 0 };
                    }
                })
            });


            $scope.datas.sort(function (a, b) { return a.data < b.data ? -1 : 1; });
            $scope.datas.forEach(function (d) {
                $scope.colunas.push({ field: "totais." + d.data + ".qtd_entrantes", displayName: d.data_BR, width: $scope.filtros.porDia ? 100 : 88 });
            });

            $scope.colunas.push({ field: "totais.geral.qtd_entrantes", displayName: "TOTAL", width: $scope.filtros.porDia ? 100 : 88 });
            $scope.dados.push({ cod_ic: "", nome_ic: "TOTAL", totais: $scope.totais });
                  }
                });
            //});

            // GILBERTOOOOOO 17/03/2014
            $view.on("mouseup", "tr.resumo", function () {
                var that = $(this);
                $('tr.resumo.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });
    };

// Exportar planilha XLSX
       $scope.exportaXLSX = function () {
       var $btn_exportar = $(this);
       $btn_exportar.button('loading');
       var linhas  = [];

        linhas.push([
        {value:'Data' ,hAlign:'center'},
        {value:'Qtd de Chamadas Transferidas' ,hAlign:'center'},
        //{value:'Op 0' ,hAlign:'center'},
        {value:'Op 1' ,hAlign:'center'},
        {value:'Op 2' ,hAlign:'center'},
        {value:'Op 3'  ,hAlign:'center'},
        {value:'Op 4' ,hAlign:'center'},
        {value:'Op 5' ,hAlign:'center'},
        {value:'Op 6' ,hAlign:'center'},
        {value:'Op 7' ,hAlign:'center'},
        {value:'Op 8' ,hAlign:'center'},
        {value:'Op 9' ,hAlign:'center'},
        {value:'Op 10' ,hAlign:'center'},
        {value:'Op 11+' ,hAlign:'center'},
        //{value:'% Op 0' ,hAlign:'center'},
        {value:'% Op 1' ,hAlign:'center'},
        {value:'% Op 2+' ,hAlign:'center'}
        ]);
          $scope.dados.map(function (dado) {
            return linhas.push([

            {value:dado.DatReferencia, autoWidth:true, hAlign:'center'},
            {value:dado.QtdChamadas, autoWidth:true, hAlign:'center'},  
             //{value:dado.QtdChamadas0, hAlign:'center'},  
             {value:dado.QtdChamadas1, hAlign:'center'},  
             {value:dado.QtdChamadas2, hAlign:'center'},  
             {value:dado.QtdChamadas3, hAlign:'center'},  
             {value:dado.QtdChamadas4, hAlign:'center'},  
             {value:dado.QtdChamadas5, hAlign:'center'},  
             {value:dado.QtdChamadas6, hAlign:'center'},  
             {value:dado.QtdChamadas7, hAlign:'center'},  
             {value:dado.QtdChamadas8, hAlign:'center'},  
             {value:dado.QtdChamadas9, hAlign:'center'},  
             {value:dado.QtdChamadas10, hAlign:'center'},  
             {value:dado.QtdChamadas11OuMais, hAlign:'center'},  
             //{value:dado.perc0, hAlign:'center'},
             {value:dado.perc1, hAlign:'center'},
             {value:dado.perc2, hAlign:'center'}
            ]);
          });
       
       // linhas.push([
       // {value:"Chamadas: " + $scope.QtdChamadasTotal, autoWidth:true},
       // {value:"Não antendidas: "+$scope.naoDerivadas+"%", autoWidth:true},
       // {value:"Derivadas: " +$scope.Derivadas+"%", autoWidth:true},
       // {value:" ", autoWidth:true}, 
       // {value:" ", autoWidth:true},
       // {value:" ", autoWidth:true},
       // {value:"Transferidas: "+$scope.Transferidas+"%", autoWidth:true}
       // ]);


        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{ name: 'CGT', data: linhas, table: true }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: { first: 1 }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'contagemglobaldetransferencias_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
       };

      }

      CtrlContGlobalTransf.$inject = ['$scope', '$globals'];
