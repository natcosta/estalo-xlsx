function CtrlUraAtiva($scope, $globals) {
  win.title="Ura Ativa";
  $scope.versao = versao;
  $scope.rotas = rotas;
  $scope.limite_registros = 0;
  $scope.incrementoRegistrosExibidos = 100;
  $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

  travaBotaoFiltro(0, $scope, "#pag-ura-ativa", "Ura Ativa");

  //Alex 24/02/2014
  $scope.status_progress_bar = 0;
  $scope.aplicacoes = []; // FIXME: copy

  // Filtros
  $scope.filtros = {
    grupos: [],
    porMes: false,
  };

  $scope.gridOptions = {
    columnDefs: [],
    rowData: []
  };

  $scope.grupos = [];
  $scope.arrDatas = [];
  $scope.dadosQuery = [];
  $scope.objDadosPorData = {};
  $scope.objDadosPorGrupo = {};

	//Alex 02/08/2017
	$scope.intervalo = false;
	var p = cache.aclbotoes.map(function(b){ return b.idview; }).indexOf('pag-ura-ativa');
	if(p >= 0){
		var teste = cache.aclbotoes[p].permissoes.split(',');
		for (var i = 0; i < teste.length; i++){
			if(teste[i] === 'intervalo'){
				$scope.intervalo = true;
			}
		}
	}

  // Filtros: data e hora
  var agora = new Date();

  //var dat_consoli = new Date(teste_data);
  //Alex 03/03/2014
  var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = precisaoHoraIni(Estalo.filtros.filtro_data_hora[0]) : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = precisaoHoraFim(Estalo.filtros.filtro_data_hora[1]) : fim = ontemFim();

  $scope.periodo = {
    inicio:inicio,
    fim: fim,
    min: new Date(2013, 11, 1),
    max: agora // FIXME: atualizar ao virar o dia
    /*min: ontem(dat_consoli),
    max: dat_consoli*/
  };

  var $view;

  $scope.$on('$viewContentLoaded', function () {
    $view = $("#pag-ura-ativa");
    $(".aba2").css({'position':'fixed','left':'120px','top':'42px'});
    $('.navbar-inner').css('height','70px');
    $(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
    //minuteStep: 5

    //19/03/2014
    componenteDataMaisHora ($scope,$view);

    // Marcar todos grupos
    $view.on("click", "#alinkGru", function(){ marcaTodasAplicacoes($('.filtro-grupo'),$view,$scope)});

    /* $view.on("click", ".btn-exportar", function () {
      $scope.exportaXLSX.apply(this);
    }); */

    $view.on("click", ".btn-exportarCSV-interface", function () {
      $scope.exportaCSV.apply(this);
    });

    // Limpa filtros Alex 03/03/2014
    $view.on("click", ".btn-limpar-filtros", function () {
      // $scope.porRegEDDD = false;
      $scope.limparFiltros.apply(this);
    });

    $scope.limparFiltros = function () {
      iniciaFiltros();
      componenteDataMaisHora ($scope,$view,true);

      var partsPath = window.location.pathname.split("/");
      var part  = partsPath[partsPath.length-1];

      var $btn_limpar = $view.find(".btn-limpar-filtros");
      $btn_limpar.prop("disabled", true);
      $btn_limpar.button('loading');

      setTimeout(function(){
        $btn_limpar.button('reset');
        //window.location.href = part + window.location.hash +'/';
      },500);
    }

    // Botão agora Alex 03/03/2014
    $view.on("click", ".btn-agora", function () {
      $scope.agora.apply(this);
    });

    $scope.agora = function () {
      iniciaAgora($view,$scope);
    }

    // Botão abortar Alex 23/05/2014
    $view.on("click", ".abortar", function () {
      $scope.abortar.apply(this);
    });

    //Alex 23/05/2014
    $scope.abortar = function () {
      abortar($scope, "#pag-ura-ativa");
    }

    // Lista chamadas conforme filtros
    $view.on("click", ".btn-gerar", function () {
      limpaProgressBar($scope, "#pag-ura-ativa");
      //22/03/2014 Testa se data início é maior que a data fim
      var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
      var testedata = testeDataMaisHora(data_ini, data_fim);
      if (testedata !== "") {
        setTimeout(function () {
          atualizaInfo($scope, testedata);
          effectNotification();
          $view.find(".btn-gerar").button('reset');
        }, 500);
        return;
      }
      $scope.listaDados.apply(this);
    });

    $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  });

  // Exibe mais registros
  $scope.exibeMaisRegistros = function () {
    $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
  };

  // Lista chamadas conforme filtros
  $scope.listaDados = function () {
    $globals.numeroDeRegistros = 0;
    // var $btn_exportar = $view.find(".btn-exportar");
    var $btn_exportar = $view.find(".btn-exportarCSV-interface");
    $btn_exportar.prop("disabled", true);

    var $btn_gerar = $(this);
    $btn_gerar.button('loading');

    var data_ini = $scope.periodo.inicio,
      data_fim = $scope.periodo.fim;

    var filtro_aplicacoes =  [1];
    var filtro_grupos = $scope.gruposSelecionados
    $scope.grupos = $scope.gruposSelecionados
    if (!filtro_grupos) {
      atualizaInfo($scope, '<font color = "white">Selecione pelo menos um grupo.</font>');
      $btn_gerar.button('reset');
      return;
    }

    //filtros usados
    $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
    + " até " + formataDataHoraBR($scope.periodo.fim);
    if (filtro_grupos.length > 0) { $scope.filtros_usados += " Grupos: " + filtro_grupos; }

    var stmt = "";
    var executaQuery = "";

    // if($scope.filtros.porDia) {
    //   var data1 = " CONVERT(VARCHAR, DatReferencia, 3) AS Data "
    //   var data2ini = formataData(data_ini) + " 00:00:00 ";
    //   var data2fim = formataData(data_fim) + " 23:59:59 ";
    // } else {
    //   var data1 = " DatReferencia AS Data "
      var data2ini = formataDataHora(data_ini);
      var data2fim = formataDataHora(data_fim);
    // }

    if ($scope.chkMensal) {
      stmt = db.use
      + " SELECT CAST(YEAR(DATA) AS CHAR(4)) + RIGHT('0' + CAST(MONTH(DATA) AS VARCHAR(2)),2) AS DATA, "
      + " GRUPO, "
      + " ISNULL(SUM(EFETUADAS), 0) AS EFETUADAS, "
      + " ISNULL(SUM(ATENDIDAS), 0) AS ATENDIDAS, "
      + " ISNULL(SUM(SUCESSO), 0) AS SUCESSO "
      + " FROM UraAtiva "
      + " WHERE 1=1 "
      + restringe_consulta("Grupo", filtro_grupos, true)
      + " AND DATA >= '" + data2ini + "'"
      + " AND DATA <= '" + data2fim + "'"
      + " GROUP BY CAST(YEAR(DATA) AS CHAR(4)) + RIGHT('0' + CAST(MONTH(DATA) AS VARCHAR(2)),2), "
      + " GRUPO "
      + " ORDER BY CAST(YEAR(DATA) AS CHAR(4)) + RIGHT('0' + CAST(MONTH(DATA) AS VARCHAR(2)),2) "
    } else {
    // CONSULTA SQL 1.1
    stmt = db.use
      + " SELECT DATA, "
      + " GRUPO, "
      + " ISNULL(SUM(EFETUADAS), 0) AS EFETUADAS, "
      + " ISNULL(SUM(ATENDIDAS), 0) AS ATENDIDAS, "
      + " ISNULL(SUM(SUCESSO), 0) AS SUCESSO "
      + " FROM UraAtiva "
      + " WHERE 1=1 "
      + restringe_consulta("Grupo", filtro_grupos, true)
      + " AND DATA >= '" + data2ini + "'"
      + " AND DATA <= '" + data2fim + "'"
      + " GROUP BY DATA, "
      + " GRUPO "
      + " ORDER BY DATA"
    }

    $scope.arrDatas = [];
    $scope.dadosQuery = [];
    $scope.objDadosPorData = {};
    $scope.objDadosPorGrupo = {};
    $scope.gridOptions.rowData = [];
    $scope.gridOptions.columnDefs = [
      {
        headerName: "Grupo",
        field: "grupo",
        pinned: 'left',
        cellStyle: function(params) {
          if (params.node.rowIndex % 3 === 0) {
            return { 
              'border-top': '#04b2d9 solid 2px',
              'border-right': '0',
              'border-left': '0',
              'background': '#f5f5f5'
            }
          } else {
            return { 
              // 'border-top': 'f5f5f5',
              'border-right': '0',
              'border-left': '0',
              'background': '#f5f5f5'
            }
          }
        }
      },
      {
        headerName: "Status",
        field: "status",
        pinned: 'left',
        width: 100,
        cellStyle: function(params) {
          if (params.node.rowIndex % 3 === 0) {
            if (params.node.rowIndex % 2 === 0) {
              return { 
                'border-top': '#04b2d9 solid 2px',
                'border-right': '0',
                'border-left': '0',
                'background': '#f2f2f2'
              }
            } else {
              return { 
                'border-top': '#04b2d9 solid 2px',
                'border-right': '0',
                'border-left': '0',
                'background': '#e2e2e2'
              }
            }
          } else if (params.node.rowIndex % 2 === 0) {
            return { 
              'border-top': '#f5f5f5 solid',
              'border-right': '0',
              'border-left': '0',
              'background': '#f2f2f2'
            } 
          } else {
            return { 
              'border-top': '#f5f5f5 solid',
              'border-right': '0',
              'border-left': '0',
              'background': '#e2e2e2'
            }
          }
        }
      }
    ];
    $scope.gridOptions.api.setColumnDefs([]);
    $scope.gridOptions.api.setRowData([]);

    executaQuery = executaQuery2;
    log(stmt);

    var stmtCountRows = stmtContaLinhas(stmt);

    //Contador de linhas para auxiliar progress bar
    function contaLinhas(columns) {
      $globals.numeroDeRegistros = columns[0].value;
    }

    $scope.totalEfetuadas = {};
    $scope.totalAtendidas = {};
    $scope.totalSucesso = {};

    function executaQuery2(columns){
      var grupoQuery = columns['GRUPO'].value;
      var dataQuery = $scope.chkMensal ? formataMesString(columns['DATA'].value) : formataDataString(columns['DATA'].value);
      var efetuadasQuery = columns['EFETUADAS'].value ? columns['EFETUADAS'].value.toLocaleString('pt-BR') : "0";
      var atendidasQuery = columns['ATENDIDAS'].value ? columns['ATENDIDAS'].value.toLocaleString('pt-BR') : "0";
      var sucessoQuery = columns['SUCESSO'].value ? columns['SUCESSO'].value.toLocaleString('pt-BR') : "0";
      
      $scope.dadosQuery.push({
        grupo: grupoQuery,
        data: dataQuery,
        efetuadas: efetuadasQuery,
        atendidas: atendidasQuery,
        sucesso: sucessoQuery
      });

      if ($scope.totalAtendidas[dataQuery]) {
        $scope.totalEfetuadas[dataQuery] += columns['EFETUADAS'].value;
        $scope.totalAtendidas[dataQuery] += columns['ATENDIDAS'].value;
        $scope.totalSucesso[dataQuery] += columns['SUCESSO'].value;
      } else {
        $scope.totalEfetuadas[dataQuery] = columns['EFETUADAS'].value
        $scope.totalAtendidas[dataQuery] = columns['ATENDIDAS'].value
        $scope.totalSucesso[dataQuery] = columns['SUCESSO'].value
      }
      // if ($scope.dados.length % 1000 === 0) {
      //   $scope.$apply();

      // }
    }

    function formataDataString(valor) {
      var ano = valor.substring(0,4);
      var mes = valor.substring(5,7);
      var dia = valor.substring(8,10);
      return dia + '/' + mes + '/' + ano;
    }

    function formataMesString(valor) {
      var ano = valor.substring(0,4);
      var mes = valor.substring(4);
      return mes + '/' + ano;
    }

    function criaArrayDatas(dados) {
      var seen = {};
      var out = [];
      var len = dados.length;
      var j = 0;
      for(var i = 0; i < len; i++) {
           var item = dados[i].data;
           if(seen[item] !== 1) {
                 seen[item] = 1;
                //  out[j++] = {[item] : {}}
                 out[j++] = item;
           }
      }
      return out;
    }

    db.query(stmt, executaQuery, function(err, num_rows){
      //userLog(stmt, 'Carrega dados', 2, err);
      console.log('Executando query-> ' + stmt + ' ' + num_rows);
      retornaStatusQuery(num_rows,$scope);
      $btn_gerar.button('reset');
      if(num_rows>0){

        $scope.arrDatas = criaArrayDatas($scope.dadosQuery);

        for (var i = 0 ; i < $scope.dadosQuery.length ; i++) {
          $scope.objDadosPorData[$scope.arrDatas[i]] = $scope.dadosQuery.filter( function(el) {
            return el.data == $scope.arrDatas[i];
          })
        }
        for (var i = 0 ; i < $scope.grupos.length ; i++) {
          $scope.objDadosPorGrupo[$scope.grupos[i]] = {
            Efetuadas: [],
            Atendidas: [],
            Sucesso: []
          }
          for (var data in $scope.objDadosPorData) {
            var filtraGrupoPorData = $scope.objDadosPorData[data].filter(function(el) {
              return el.grupo == $scope.grupos[i];
            })
            if (filtraGrupoPorData[0]) {
              $scope.objDadosPorGrupo[$scope.grupos[i]].Efetuadas.push(filtraGrupoPorData[0]['efetuadas']);
              $scope.objDadosPorGrupo[$scope.grupos[i]].Atendidas.push(filtraGrupoPorData[0]['atendidas']);
              $scope.objDadosPorGrupo[$scope.grupos[i]].Sucesso.push(filtraGrupoPorData[0]['sucesso']);
            }
          }
        }

        for( var i = 0 ; i < $scope.arrDatas.length ; i++) {
          $scope.gridOptions.columnDefs.push({
            headerName: $scope.arrDatas[i],
            width: 110,
            field: $scope.arrDatas[i],
            cellStyle: function(params) {
              if (params.node.rowIndex % 3 === 0) {
                if (params.node.rowIndex % 2 === 0) {
                  return {
                    'border-top': '#04b2d9 solid 2px',
                    'background': '#f2f2f2',
                    'border-right': '0',
                    'border-left': '0',
                    textAlign: 'center'
                  }
                } else {
                  return {
                    'border-top': '#04b2d9 solid 2px',
                    'background': '#e2e2e2',
                    'border-right': '0',
                    'border-left': '0',
                    textAlign: 'center'
                  }
                }
              } else  if (params.node.rowIndex % 2 === 0) {
                return {
                  borderTop: '#f5f5f5 solid',
                  background: '#f2f2f2',
                  textAlign: 'center',
                  'border-right': '0',
                  'border-left': '0',
                }
              } else {
                return { 
                  borderTop: '#f5f5f5 solid',
                  background: '#e2e2e2',
                  textAlign: 'center',
                  'border-right': '0',
                  'border-left': '0',
                }
              }
            }
          })
        }

        for ( var grupo in $scope.objDadosPorGrupo ) {
          for ( var status in $scope.objDadosPorGrupo[grupo]) {
            var grupoFormatado = status == 'Atendidas' ? grupo : '';
            var tempObj = {
              grupo: grupoFormatado,
              status: status
            }
            for (var i = 0 ; i < $scope.arrDatas.length ; i++) {
              tempObj[$scope.arrDatas[i]] = $scope.objDadosPorGrupo[grupo][status][i] || 0
            }
            $scope.gridOptions.rowData.push(tempObj)
          }
        }

        var rowTotaisEfetuadas = {
          grupo: '',
          status: 'Efetuadas'
        }
        var rowTotaisAtendidas = {
          grupo: 'Total',
          status: 'Atendidas'
        }
        var rowTotaisSucesso = {
          grupo: '',
          status: 'Sucesso'
        }

        for ( var dataDeConsulta in $scope.totalAtendidas ) {
          rowTotaisEfetuadas[dataDeConsulta] = $scope.totalEfetuadas[dataDeConsulta].toLocaleString('pt-BR');
          rowTotaisAtendidas[dataDeConsulta] = $scope.totalAtendidas[dataDeConsulta].toLocaleString('pt-BR');
          rowTotaisSucesso[dataDeConsulta] = $scope.totalSucesso[dataDeConsulta].toLocaleString('pt-BR');
        }
        $scope.gridOptions.rowData.push(rowTotaisEfetuadas)
        $scope.gridOptions.rowData.push(rowTotaisAtendidas)
        $scope.gridOptions.rowData.push(rowTotaisSucesso)

        $scope.gridOptions.api.setColumnDefs($scope.gridOptions.columnDefs);
        $scope.gridOptions.api.setRowData($scope.gridOptions.rowData)
        $btn_exportar.prop("disabled", false);
      }
    });

    // GILBERTOOOOOO 17/03/2014
    $view.on("mouseup", "tr.resumo", function () {
      var that = $(this);
      $('tr.resumo.marcado').toggleClass('marcado');
      $scope.$apply(function () {
        that.toggleClass('marcado');
      });
    });
  };

  // Exportar planilha XLSX
  /* $scope.exportaXLSX = function () {
    var $btn_exportar = $(this);
    $btn_exportar.button('loading');
    var linhas  = [];

    var header = [];
    for ( var i = 0 ; i < $scope.gridOptions.columnDefs.length ; i++ ) {
      header.push({
        value: $scope.gridOptions.columnDefs[i].headerName, hAlign: 'center', autoWidth: true
      })
    }
    linhas.push(header)
    for ( var i = 0 ; i < $scope.gridOptions.rowData.length ; i++ ) {
      var linha = [];
      linha.push({ 
        value: $scope.gridOptions.rowData[i]['grupo'], 
        hAlign: 'center'
      },{ 
        value: $scope.gridOptions.rowData[i]['status'], 
        hAlign: 'center'
      })
      for ( var j = 0 ; j < $scope.arrDatas.length ; j++ ) {
        linha.push({ 
          value: $scope.gridOptions.rowData[i][$scope.arrDatas[j]], 
          hAlign:'center'
        })
      }
      linhas.push(linha)
    }

    for( var i = 0 ; i < linhas.length ; i++) {
      for ( var j = 1 ; j < linhas[i].length ; j++ ) {
        for ( var obj in linhas[i][j] ) {
          if( linhas[i][j]['value'] !== '' || linhas[i][j]['value'] !== 'Sucesso' || linhas[i][j]['value'] !== 'Atendidas' || linhas[i][j]['value'] !== 'Efetuadas' ) {
            linhas[i][j]['value'] = linhas[i][j]['value'].replace('.','')
          }
        }
      }
    }

    var planilha = {
      creator: "Estalo",
      lastModifiedBy: $scope.username || "Estalo",
      worksheets: [{ name: 'Ura Ativa', data: linhas, table: true }],
      autoFilter: false,
      // Não incluir a linha do título no filtro automático
      dataRows: { first: 1 }
    };

    var xlsx = frames["xlsxjs"].window.xlsx;
    planilha = xlsx(planilha, 'binary');

    var milis = new Date();
    var file = 'UraAtiva_' + formataDataHoraMilis(milis) + '.xlsx';

    if (!fs.existsSync(file)) {
      fs.writeFileSync(file, planilha.base64, 'base64');
      childProcess.exec(file);
    }


    setTimeout(function () {
      $btn_exportar.button('reset');
    }, 500);
  }; */

  // Exporta CSV
  $scope.exportaCSV = function () {
    var colunas = [];
    var linhas  = [];

    var header = [];
    for ( var i = 0 ; i < $scope.gridOptions.columnDefs.length ; i++ ) {
      header.push({
        displayName: $scope.gridOptions.columnDefs[i].headerName, hAlign: 'center', autoWidth: true
      })
    }
    $scope.gridOptions.columnDefs.forEach(function (col) {
      colunas.push({
        displayName: col.headerName,
        field: col.field
      });
    });
    
    exportaCSV(colunas, $scope.gridOptions.rowData, "UraAtiva_");
  };
}

CtrlUraAtiva.$inject = ['$scope', '$globals'];
