function CtrlResumoPesqServ($scope, $globals) {

    win.title="Atendimento Eletrônico"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    travaBotaoFiltro(0, $scope, "#pag-resumo-pesq-serv", "Atendimento Eletrônico");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.dadosPivot = [];

    $scope.pivot = false;

    $scope.colunas = [];
    $scope.csv = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };


    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;
    $scope.agrupaApls = false;
    $scope.servico = true;
    $scope.item = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013
        sites: [],
        segmentos: [],
        isHFiltroED: false
    };

    // Filtro: ddd
    $scope.ddds = cache.ddds;

    $scope.porDDD = null;

    $scope.valor = "h";
    $scope.valor2 = "s";

    //02/06/2014 Flag
    $scope.porRegEDDD = $globals.agrupar;

    $scope.aba = 2;




    function geraColunas(){
      var array = [];
      var testeNps = ($('.filtro-aplicacao').val() || []).join(';').match(/ussd[0-9a-zA-Z]*/gi)!== null ? $('.filtro-aplicacao').val().join(';').match(/ussd[0-9a-zA-Z]*/gi).length === $('.filtro-aplicacao').val().length : false;
      if(testeNps){
        array.push(
          { field: "datReferencia", displayName: "Data e hora", width: 150, pinned: true }
        );

        if(!$scope.agrupaApls){
          array.push(
            { field: "codaplicacao", displayName: "Aplicação", width: 80, pinned: true }
          );
        }

        if($scope.servico){
          array.push(
            { field: "codservico", displayName: "Serviço", width: 150, pinned: true }
          );
        }

        if($scope.item){
          array.push(
            { field: "codic", displayName: "IC", width: 70, pinned: true },
            { field: "qtd", displayName: "QTD", width: 70, pinned: true }
          );
        }


        if($scope.valor2 === "s"){
          array.push(
            { field: "zeroS", displayName: "N0", width: 40, pinned: true },
            { field: "umS", displayName: "N1", width: 40, pinned: true },
            { field: "doisS", displayName: "N2", width: 40, pinned: true },
            { field: "tresS", displayName: "N3", width: 40, pinned: true },
            { field: "quatroS", displayName: "N4", width: 40, pinned: true },
            { field: "cincoS", displayName: "N5", width: 40, pinned: true },
            { field: "seisS", displayName: "N6", width: 40, pinned: true },
            { field: "seteS", displayName: "N7", width: 40, pinned: true },
            { field: "oitoS", displayName: "N8", width: 40, pinned: true },
            { field: "noveS", displayName: "N9", width: 40, pinned: true },
            { field: "dezS", displayName: "N10", width: 40, pinned: true },
            { field: "qtdNIS", displayName: "Inválidas", width: 70, pinned: true },
            { field: "qtdAS", displayName: "Abstenções", width: 70, pinned: true },
            { field: "NotaMediaS", displayName: "Média", width: 70, pinned: true }
          );
        }else if($scope.valor2 === "r"){
          array.push(
            { field: "zeroR", displayName: "N0", width: 40, pinned: true },
            { field: "umR", displayName: "N1", width: 40, pinned: true },
            { field: "doisR", displayName: "N2", width: 40, pinned: true },
            { field: "tresR", displayName: "N3", width: 40, pinned: true },
            { field: "quatroR", displayName: "N4", width: 40, pinned: true },
            { field: "cincoR", displayName: "N5", width: 40, pinned: true },
            { field: "seisR", displayName: "N6", width: 40, pinned: true },
            { field: "seteR", displayName: "N7", width: 40, pinned: true },
            { field: "oitoR", displayName: "N8", width: 40, pinned: true },
            { field: "noveR", displayName: "N9", width: 40, pinned: true },
            { field: "dezR", displayName: "N10", width: 40, pinned: true },
            { field: "qtdNIR", displayName: "Inválidas", width: 70, pinned: true },
            { field: "qtdAR", displayName: "Abstenções", width: 70, pinned: true },
            { field: "NotaMediaR", displayName: "Média", width: 70, pinned: true }
          );
        }

      }else{
        array.push(
          { field: "datReferencia", displayName: "Data e hora", width: 150, pinned: true },
          { field: "codaplicacao", displayName: "Aplicação", width: 150, pinned: true },
          { field: "codservico", displayName: "Serviço", width: 230, pinned: true },
          { field: "um", displayName: "Nota 1", width: 80, pinned: true },
          { field: "dois", displayName: "Nota 2", width: 80, pinned: true },
          { field: "tres", displayName: "Nota 3", width: 80, pinned: true },
          { field: "quatro", displayName: "Nota 4", width: 80, pinned: true },
          { field: "cinco", displayName: "Nota 5", width: 80, pinned: true },
          { field: "qtdNI", displayName: "Inválidas", width: 100, pinned: true },
          { field: "qtdA", displayName: "Abstenções", width: 100, pinned: true },
          { field: "Nota0a100", displayName: "Nota", width: 80, pinned: true }
        );
      }
      return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

        $scope.periodo = {
            inicio:inicio,
            fim: fim,
            min: new Date(2013, 11, 1),
            max: agora // FIXME: atualizar ao virar o dia
            /*min: ontem(dat_consoli),
            max: dat_consoli*/
        };

    var $view;


    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-resumo-pesq-serv");


      //19/03/2014
      componenteDataHora ($scope,$view);

        carregaRegioes($view);
        carregaAplicacoes($view,false,false,$scope);
        carregaSites($view);
        carregaServicos($view,true);


        carregaSegmentosPorAplicacao($scope,$view,true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});

        carregaDDDsPorRegiao($scope,$view,true);
        // Popula lista de  ddds a partir das regioes selecionadas
        $view.on("change", "select.filtro-regiao", function(){
          $('#alinkApl').removeAttr('disabled');
          $(".filtro-aplicacao").attr('multiple', 'multiple');
          $('#HFiltroED').prop('checked',false);
          $scope.filtros.isHFiltroED = false;
          $('#oueED').attr('disabled', 'disabled');
          $('#oueED').prop('checked',false);
          //$('#iit').attr('disabled', false);
          var $sel_eds = $view.find("select.filtro-ed");
          $sel_eds.
          val([])
          .selectpicker('refresh');
          $sel_eds
          .prop("disabled", "disabled")
          .selectpicker('refresh');
          carregaDDDsPorRegiao($scope,$view);
        });



        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-ed").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} EDs'
        });
        $view.find("select.filtro-servico").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Serviços'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-ddd", function () {
            var filtro_ddds = $(this).val() || [];
            Estalo.filtros.filtro_ddds = filtro_ddds;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });


      $('input[type=radio][name=nps]').change(function() {
        if (this.value == 's') {
          $scope.valor2 = 's';
          $scope.colunas = geraColunas();
        } else if (this.value == 'r') {
          $scope.valor2 = 'r';
          $scope.colunas = geraColunas();
        }
      });

        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

        // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

        // Marca todos os Servicos
        $view.on("click", "#alinkPesq", function(){ marcaTodosIndependente($('.filtro-servico'),'servicos')});

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


        // GILBERTO 18/02/2014

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
          //11/07/2014 não mostrar filtros desabilitados
          if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.dropdown-menu.open').css({ 'margin-left':'-110px' });
            $('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
          }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.dropdown-menu.open').css({ 'margin-left':'0px' });
            $('div.btn-group.filtro-ddd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ed').addClass('open');
                $('div.btn-group.filtro-ed>div>ul').css(
                  { 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' }
                );
            }
        });

        // OCULTAR AO TIRAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseout(function () {
            $('div.btn-group.filtro-ed').removeClass('open');
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora ($scope,$view,true);

            var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function(){
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            },500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view,$scope);
            /*var partsPath = window.location.pathname.split("/");
            var part  = partsPath[partsPath.length-1];

            var $btn_agora = $view.find(".btn-agora");
            $btn_agora.prop("disabled", true);
            $btn_agora.button('loading');

            setTimeout(function(){
            window.location.href = part + window.location.hash +'/';
            },500);            */
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
        abortar($scope, "#pag-resumo-pesq-serv");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

          $scope.pivot = false;
          limpaProgressBar($scope, "#pag-resumo-pesq-serv");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini,data_fim);
            if(testedata!==""){
                setTimeout(function(){
                    atualizaInfo($scope,testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
            }
			
			function apenasUSSD($filtro) {
				var filtro_aplicacoes = $filtro.val() || [];
				if (typeof filtro_aplicacoes === 'string') { filtro_aplicacoes = [ filtro_aplicacoes ]; }
				return filtro_aplicacoes.every(function (apl) { return apl.match(/^(USSD|PUSH)/); });
			}


          if(!(($('.filtro-aplicacao').val() || []).join(';').match(/ussd[0-9a-zA-Z]*/gi)!== null ? $('.filtro-aplicacao').val().join(';').match(/ussd[0-9a-zA-Z]*/gi).length === $('.filtro-aplicacao').val().length : false) && !(($('.filtro-aplicacao').val() || []).join(';').match(/EMPUNIF*/gi)!== null ? $('.filtro-aplicacao').val().join(';').match(/EMPUNIF*/gi).length === $('.filtro-aplicacao').val().length : false)){
            setTimeout(function(){

                    atualizaInfo($scope,"Combinação não permitida");
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                },500);
                return;
          }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        //Alex 27/02/2014
        $view.on("click", ".btn-pivot", function () {


          if($scope.pivot === true){
            $scope.pivot = false;
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);

          }else{
            limpaProgressBar($scope, "#pag-resumo-dia");
            $('.chkMensal').prop('disabled',true);
            $scope.pivot = true;
            $scope.colunas = geraColunas();
            $scope.listaDadosPivot.apply(this);
          }


        });







        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    nps = "";

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportarCSV");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_servicos = $view.find("select.filtro-servico").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_ed = $view.find("select.filtro-ed").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR(dataConsoliReal($scope.periodo.fim));
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
        if (filtro_servicos.length > 0) { $scope.filtros_usados += " Serviços: " + filtro_servicos; }

        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";

        nps = filtro_aplicacoes.join(';').match(/ussd[0-9a-zA-Z]*/gi)!== null ? filtro_aplicacoes.join(';').match(/ussd[0-9a-zA-Z]*/gi).length === filtro_aplicacoes.length : false;

      stmt = db.use;

      if ($scope.item && nps) stmt += " with a as( ";
      stmt += " SELECT  ";

      if($scope.valor === 'd'){
        stmt += "CONVERT(DATE,RP.DatReferencia) as DatReferencia";
      }else if($scope.valor === 'm'){
        stmt += "CONVERT(CHAR(4), RP.DatReferencia, 120) +'-'+ CONVERT(CHAR(2), RP.DatReferencia, 101) as DatReferencia";
      }else{
        stmt += "RP.DatReferencia";
      }

      stmt += " "+(!$scope.agrupaApls ? ",codaplicacao" : "")+""+(($scope.servico || !nps) ? ",codservico" : "")+""+(($scope.item && nps) ? ",CodIc" : "")+",";

      if(nps){
        stmt += " sum(QtdNotas0S) as zeroS, sum(QtdNotas1S) as umS, sum(QtdNotas2S) as doisS, sum(QtdNotas3S) as tresS,"
        + " sum(QtdNotas4S) as quatroS, sum(QtdNotas5S) as cincoS,  sum(QtdNotas6S) as seisS,  sum(QtdNotas7S) as seteS,"
        + " sum(QtdNotas8S) as oitoS, sum(QtdNotas9S) as noveS,  sum(QtdNotas10S) as dezS,"
        + " sum(QtdNotasInvalidasS) as qtdNIS, sum(QtdAbstencoesS) AS qtdAS,"
        + " sum(QtdNotas0R) as zeroR, sum(QtdNotas1R) as umR, sum(QtdNotas2R) as doisR, sum(QtdNotas3R) as tresR,"
        + " sum(QtdNotas4R) as quatroR, sum(QtdNotas5R) as cincoR,  sum(QtdNotas6R) as seisR,  sum(QtdNotas7R) as seteR,"
        + " sum(QtdNotas8R) as oitoR, sum(QtdNotas9R) as noveR,  sum(QtdNotas10R) as dezR,"
        + " sum(QtdNotasInvalidasR) as qtdNIR, sum(QtdAbstencoesR) AS qtdAR,";
      }else{
        stmt += " sum(QtdNotas1) as um, sum(QtdNotas2) as dois, sum(QtdNotas3) as tres,"
        + " sum(QtdNotas4) as quatro, sum(QtdNotas5) as cinco,"
        + " sum(QtdNotasInvalidas) as qtdNI, sum(QtdAbstencoes) AS qtdA,";
      }

      if($scope.valor !=='h'){
        if(nps){
          stmt += " (CONVERT([decimal](12,1), case when ((((((((((sum([QtdNotas0S])+sum([QtdNotas1S]))+sum([QtdNotas2S]))+sum([QtdNotas3S]))+sum([QtdNotas4S]))+sum([QtdNotas5S]))+sum([QtdNotas6S]))+sum([QtdNotas7S]))+sum([QtdNotas8S]))+sum([QtdNotas9S]))+sum([QtdNotas10S]))=(0) then (0) else ((((((((((sum([QtdNotas0S])+(1.0)*sum([QtdNotas1S]))+(2.0)*sum([QtdNotas2S]))+(3.0)*sum([QtdNotas3S]))+(4.0)*sum([QtdNotas4S]))+(5.0)*sum([QtdNotas5S]))+(6.0)*sum([QtdNotas6S]))+(7.0)*sum([QtdNotas7S]))+(8.0)*sum([QtdNotas8S]))+(9.0)*sum([QtdNotas9S]))+(10)*sum([QtdNotas10S]))/((((((((((sum([QtdNotas0S])+sum([QtdNotas1S]))+sum([QtdNotas2S]))+sum([QtdNotas3S]))+sum([QtdNotas4S]))+sum([QtdNotas5S]))+sum([QtdNotas6S]))+sum([QtdNotas7S]))+sum([QtdNotas8S]))+sum([QtdNotas9S]))+sum([QtdNotas10S])) end,0))AS NotaMediaS, (CONVERT([decimal](12,1), case when ((((((((((sum([QtdNotas0R])+sum([QtdNotas1R]))+sum([QtdNotas2R]))+sum([QtdNotas3R]))+sum([QtdNotas4R]))+sum([QtdNotas5R]))+sum([QtdNotas6R]))+sum([QtdNotas7R]))+sum([QtdNotas8R]))+sum([QtdNotas9R]))+sum([QtdNotas10R]))=(0) then (0) else ((((((((((sum([QtdNotas0R])+(1.0)*sum([QtdNotas1R]))+(2.0)*sum([QtdNotas2R]))+(3.0)*sum([QtdNotas3R]))+(4.0)*sum([QtdNotas4R]))+(5.0)*sum([QtdNotas5R]))+(6.0)*sum([QtdNotas6R]))+(7.0)*sum([QtdNotas7R]))+(8.0)*sum([QtdNotas8R]))+(9.0)*sum([QtdNotas9R]))+(10)*sum([QtdNotas10R]))/((((((((((sum([QtdNotas0R])+sum([QtdNotas1R]))+sum([QtdNotas2R]))+sum([QtdNotas3R]))+sum([QtdNotas4R]))+sum([QtdNotas5R]))+sum([QtdNotas6R]))+sum([QtdNotas7R]))+sum([QtdNotas8R]))+sum([QtdNotas9R]))+sum([QtdNotas10R])) end,0))AS NotaMediaR";
        }else{
          stmt += " (CONVERT([decimal](2,1),case when ((((sum([QtdNotas1])+sum([QtdNotas2]))+sum([QtdNotas3]))+sum([QtdNotas4]))+sum([QtdNotas5]))=(0) then (0) else (((((1.0)*sum([QtdNotas1])+(2.0)*sum([QtdNotas2]))+(3.0)*sum([QtdNotas3]))+(4.0)*sum([QtdNotas4]))+(5.0)*sum([QtdNotas5]))/((((sum([QtdNotas1])+sum([QtdNotas2]))+sum([QtdNotas3]))+sum([QtdNotas4]))+sum([QtdNotas5])) end,0)) AS NotaMedia,(CONVERT([decimal](3,0),case when ((((sum([QtdNotas1])+sum([QtdNotas2]))+sum([QtdNotas3]))+sum([QtdNotas4]))+sum([QtdNotas5]))=(0) then (0) else (25.0)*((((((1.0)*sum([QtdNotas1])+(2.0)*sum([QtdNotas2]))+(3.0)*sum([QtdNotas3]))+(4.0)*sum([QtdNotas4]))+(5.0)*sum([QtdNotas5]))/((((sum([QtdNotas1])+sum([QtdNotas2]))+sum([QtdNotas3]))+sum([QtdNotas4]))+sum([QtdNotas5]))-(1)) end,0)) AS Nota0a100";
        }
      }else{
        if(nps){
          stmt += " (CONVERT([decimal](12,1), case when ((((((((((sum([QtdNotas0S])+sum([QtdNotas1S]))+sum([QtdNotas2S]))+sum([QtdNotas3S]))+sum([QtdNotas4S]))+sum([QtdNotas5S]))+sum([QtdNotas6S]))+sum([QtdNotas7S]))+sum([QtdNotas8S]))+sum([QtdNotas9S]))+sum([QtdNotas10S]))=(0) then (0) else ((((((((((sum([QtdNotas0S])+(1.0)*sum([QtdNotas1S]))+(2.0)*sum([QtdNotas2S]))+(3.0)*sum([QtdNotas3S]))+(4.0)*sum([QtdNotas4S]))+(5.0)*sum([QtdNotas5S]))+(6.0)*sum([QtdNotas6S]))+(7.0)*sum([QtdNotas7S]))+(8.0)*sum([QtdNotas8S]))+(9.0)*sum([QtdNotas9S]))+(10)*sum([QtdNotas10S]))/((((((((((sum([QtdNotas0S])+sum([QtdNotas1S]))+sum([QtdNotas2S]))+sum([QtdNotas3S]))+sum([QtdNotas4S]))+sum([QtdNotas5S]))+sum([QtdNotas6S]))+sum([QtdNotas7S]))+sum([QtdNotas8S]))+sum([QtdNotas9S]))+sum([QtdNotas10S])) end,0))AS NotaMediaS, (CONVERT([decimal](12,1), case when ((((((((((sum([QtdNotas0R])+sum([QtdNotas1R]))+sum([QtdNotas2R]))+sum([QtdNotas3R]))+sum([QtdNotas4R]))+sum([QtdNotas5R]))+sum([QtdNotas6R]))+sum([QtdNotas7R]))+sum([QtdNotas8R]))+sum([QtdNotas9R]))+sum([QtdNotas10R]))=(0) then (0) else ((((((((((sum([QtdNotas0R])+(1.0)*sum([QtdNotas1R]))+(2.0)*sum([QtdNotas2R]))+(3.0)*sum([QtdNotas3R]))+(4.0)*sum([QtdNotas4R]))+(5.0)*sum([QtdNotas5R]))+(6.0)*sum([QtdNotas6R]))+(7.0)*sum([QtdNotas7R]))+(8.0)*sum([QtdNotas8R]))+(9.0)*sum([QtdNotas9R]))+(10)*sum([QtdNotas10R]))/((((((((((sum([QtdNotas0R])+sum([QtdNotas1R]))+sum([QtdNotas2R]))+sum([QtdNotas3R]))+sum([QtdNotas4R]))+sum([QtdNotas5R]))+sum([QtdNotas6R]))+sum([QtdNotas7R]))+sum([QtdNotas8R]))+sum([QtdNotas9R]))+sum([QtdNotas10R])) end,0))AS NotaMediaR";
            }else{
            stmt += " NotaMedia, Nota0a100";
            }
          }




          stmt += " FROM " + db.prefixo + ""+(nps ? ($scope.item ? "ResumoIC_NPS" : "ResumoNPS")  : "resumopesquisa" )+" as RP"
          + " WHERE 1 = 1"
          + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
          + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
          stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
          stmt += restringe_consulta("CodSite", filtro_sites, true)
          stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
          stmt += restringe_consulta("codservico", filtro_servicos, true)
          stmt += " GROUP BY "+(!$scope.agrupaApls ? "codaplicacao," : "")+" "+(($scope.servico || !nps) ? "codservico," : "")+" "+(($scope.item && nps) ? "CodIc," : "")+"";


          if($scope.valor === 'd'){
            stmt += "CONVERT(DATE,RP.DatReferencia)";
          }else if($scope.valor === 'm'){
            stmt += "CONVERT(CHAR(4), RP.DatReferencia, 120) +'-'+ CONVERT(CHAR(2), RP.DatReferencia, 101)";
          }else{
            if(nps){
              stmt += "RP.DatReferencia";
            }else{
              stmt += "RP.DatReferencia, notamedia, Nota0a100";
            }

          }


      if($scope.item && nps){
                stmt +=" )"
                + " ,b as("
                + " SELECT ";
                if($scope.valor === 'd'){
                  //stmt += "CONVERT(DATE,DatReferencia) as DatReferenciaB";
                  stmt += "DatReferencia as DatReferenciaB";
                }else if($scope.valor === 'm'){
                  stmt += "CONVERT(CHAR(4), DatReferencia, 120) +'-'+ CONVERT(CHAR(2), DatReferencia, 101) as DatReferenciaB";
                }else{
                  stmt += "DatReferencia as DatReferenciaB";
                }

                stmt +=" , sum(qtdChamadas) as qtd,"
                + " CodIC as CodIcB"
                + " FROM " + db.prefixo + "ResumoIC"+($scope.valor === 'd' ? "Dia":"")+""
				//+ " FROM " + db.prefixo + "ResumoIC"
                + " WHERE 1 = 1"
                + " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
                + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
                stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
                stmt += restringe_consulta("CodSite", filtro_sites, true)
                stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)

                stmt += " group by ";
          if($scope.valor === 'd'){
            //stmt += "CONVERT(DATE,DatReferencia)";
            stmt += "DatReferencia";
          }else if($scope.valor === 'm'){
            stmt += "CONVERT(CHAR(4), DatReferencia, 120) +'-'+ CONVERT(CHAR(2), DatReferencia, 101)";
          }else{
            stmt += "DatReferencia";
          }

                stmt += " ,CodIC";
                stmt += " ) "
                + " SELECT * from a"
                + " left outer join b"
                + " on a.DatReferencia = b.DatReferenciaB"
                + " and a.CodIC = b.CodIcB";


              }else{

          stmt += " ORDER BY "+(!$scope.agrupaApls ? "codaplicacao," : "")+" "+(($scope.servico || !nps) ? "codservico," : "")+" "+(($scope.item && nps) ? "CodIc," : "")+"";

          if($scope.valor === 'd'){
            stmt += "CONVERT(DATE,RP.DatReferencia)";
          }else if($scope.valor === 'm'){
            stmt += "CONVERT(CHAR(4), RP.DatReferencia, 120) +'-'+ CONVERT(CHAR(2), RP.DatReferencia, 101)";
          }else{
            if(nps){
              stmt += "RP.DatReferencia";
            }else{
              stmt += "RP.DatReferencia, notamedia, Nota0a100";
            }
          }
              }

        executaQuery = executaQuery2;
        log(stmt);

        function executaQuery2(columns){

          var datahorateste = "";
          if(columns["DatReferencia"].value.length === 10){
             datahorateste = new Date(columns["DatReferencia"].value + " 00:00:00");
          }else if(columns["DatReferencia"].value.length === 7){
            datahorateste = new Date(columns["DatReferencia"].value + "-01 00:00:00");
          }else{
            datahorateste = new Date(columns["DatReferencia"].value);
          }

          var datahorateste2 = "";
          if($scope.valor === 'd'){
            datahorateste2 = formataDataBR(datahorateste);
          }else if($scope.valor === 'm'){
            datahorateste2 = formataDataBR(datahorateste,"mes");
          }else{
            datahorateste2 = formataDataHoraBR(datahorateste);
          }


          if(nps){

            var
              data_hora = datahorateste,
              datReferencia = datahorateste2,
              codaplicacao = columns["codaplicacao"] !== undefined ? columns["codaplicacao"].value : "",
              codservico = columns["codservico"] !== undefined ? obtemNomePesquisa(columns["codservico"].value) : "",
              codic = columns["CodIc"] !== undefined ? columns["CodIc"].value : "",
              qtd = columns["qtd"] !== undefined ? columns["qtd"].value : "",
              zeroS = columns["zeroS"].value,
              umS = columns["umS"].value,
              doisS = columns["doisS"].value,
              tresS = columns["tresS"].value,
              quatroS = columns["quatroS"].value,
              cincoS = columns["cincoS"].value,
              seisS = columns["seisS"].value,
              seteS = columns["seteS"].value,
              oitoS = columns["oitoS"].value,
              noveS = columns["noveS"].value,
              dezS = columns["dezS"].value,
              qtdNIS = columns["qtdNIS"].value,
              qtdAS = columns["qtdAS"].value,
              NotaMediaS = columns["NotaMediaS"].value,
              zeroR = columns["zeroR"].value,
              umR = columns["umR"].value,
              doisR = columns["doisR"].value,
              tresR = columns["tresR"].value,
              quatroR = columns["quatroR"].value,
              cincoR = columns["cincoR"].value,
              seisR = columns["seisR"].value,
              seteR = columns["seteR"].value,
              oitoR = columns["oitoR"].value,
              noveR = columns["noveR"].value,
              dezR = columns["dezR"].value,
              qtdNIR = columns["qtdNIR"].value,
              qtdAR = columns["qtdAR"].value,
              NotaMediaR = columns["NotaMediaR"].value;


          }else{


          var
              data_hora = datahorateste,
              datReferencia = datahorateste2,
              codaplicacao = columns["codaplicacao"].value,
              codservico = obtemNomePesquisa(columns["codservico"].value),
              um = columns["um"].value,
              dois = columns["dois"].value,
              tres = columns["tres"].value,
              quatro = columns["quatro"].value,
              cinco = columns["cinco"].value,
              qtdNI = columns["qtdNI"].value,
              qtdA = columns["qtdA"].value,
              NotaMedia = columns["NotaMedia"].value,
              Nota0a100 = columns["Nota0a100"].value;
          }


          if(nps){

            $scope.dados.push({
                    data_hora: data_hora,
                    datReferencia: datReferencia,
                    codaplicacao: codaplicacao,
                    codservico: codservico,
                    codic: codic,
                    qtd: qtd,
                    zeroS: zeroS,
                    umS: umS,
                    doisS: doisS,
                    tresS: tresS,
                    quatroS: quatroS,
                    cincoS: cincoS,
                    seisS: seisS,
                    seteS: seteS,
                    oitoS: oitoS,
                    noveS: noveS,
                    dezS: dezS,
                    qtdNIS: qtdNIS,
                    qtdAS:  qtdAS,
                    NotaMediaS:  NotaMediaS,
                    zeroR: zeroR,
                    umR: umR,
                    doisR: doisR,
                    tresR: tresR,
                    quatroR: quatroR,
                    cincoR: cincoR,
                    seisR: seisR,
                    seteR:seteR,
                    oitoR: oitoR,
                    noveR: noveR,
                    dezR: dezR,
                    qtdNIR: qtdNIR,
                    qtdAR:  qtdAR,
                    NotaMediaR:  NotaMediaR
            });

          }else{



            $scope.dados.push({
                    data_hora: data_hora,
                    datReferencia: datReferencia,
                    codaplicacao: codaplicacao,
                    codservico: codservico,
                    um: um,
                    dois: dois,
                    tres: tres,
                    quatro: quatro,
                    cinco: cinco,
                    qtdNI: qtdNI,
                    qtdA:  qtdA,
                    NotaMedia:  NotaMedia,
                    Nota0a100:  Nota0a100
            });
          }




           if($scope.dados.length%1000===0){
                $scope.$apply();
              }
        }



                db.query(stmt,executaQuery, function (err, num_rows) {
					
					
					var complemento = "";				
					
				
				if(nps) complemento += " NPS";


                  if($scope.porRegEDDD===true){
                    $('#pag-resumo-pesq-serv table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1170px','margin-bottom':'100px'});
                  }else{
                    $('#pag-resumo-pesq-serv table').css({'margin-left':'auto','margin-right':'auto','margin-top':'100px','max-width':'1280px','margin-bottom':'100px'});
                  }
                    console.log("Executando query-> "+stmt+" "+num_rows);


                  var datFim = "";
                  if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1>limiteHoras/24){
                    datFim = " Botão transpor habilitado para intervalos de "+limiteHoras/24+" dias.";
                  }


                    retornaStatusQuery(num_rows, $scope, datFim);
                    $btn_gerar.button('reset');
                  if(num_rows>0){
                    $btn_exportar.prop("disabled", false);
                  }

                  //if(executaQuery !== executaQuery2){
                    $('.btn.btn-pivot').prop("disabled","false");
                    if(moment($scope.periodo.fim).diff(moment($scope.periodo.inicio), "days")+1<=limiteHoras/24 && num_rows>0){
                        $('.btn.btn-pivot').button('reset');
                    }
					
					
          userLog(stmt, "Consulta "+complemento, 2, err);
          
                  $scope.dados.forEach(function (dado) {
                    $scope.csv.push($scope.colunas.filterMap(function (col) {
                      return dado[col.field] ? dado[col.field] : "0";
                    }));
                  });

                });


            // GILBERTOOOOOO 17/03/2014
            $view.on("mouseup", "tr.resumo", function () {
                var that = $(this);
                $('tr.resumo.marcado').toggleClass('marcado');
                $scope.$apply(function () {
                    that.toggleClass('marcado');
                });
            });
    };




// Exportar planilha XLSX
    $scope.exportaXLSX = function () {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var template = "";

      (($('.filtro-aplicacao').val() || []).join(';').match(/ussd[0-9a-zA-Z]*/gi)!== null ? $('.filtro-aplicacao').val().join(';').match(/ussd[0-9a-zA-Z]*/gi).length === $('.filtro-aplicacao').val().length : false) ? template =  "tResumoPesqServNps"+$scope.valor2+""+($scope.agrupaApls ? "Apl" : "")+""+($scope.item ? "IC" : "")+""+(!$scope.servico ? "S" : "")+"" : template =  "tResumoPesqServ";

   //Alex 15-02-2014 - 26/03/2014 TEMPLATE
   var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='"+template+"'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


          var milis = new Date();
          var baseFile = 'tResumoPesqServ'+$scope.valor2+'.xlsx';



    var buffer = toBuffer(toArrayBuffer(arquivo));

      fs.writeFileSync(baseFile, buffer, 'binary');

   var file = 'tResumoPesqServ'+$scope.valor2+'_'+formataDataHoraMilis(milis)+'.xlsx';

   var newData;


   fs.readFile(baseFile, function(err, data) {
   // Create a template
    var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                  filtros: $scope.filtros_usados,
                  planDados: $scope.dados
                });

    // Get binary data
    newData = t.generate();


    if(!fs.existsSync(file)){
        fs.writeFileSync(file, newData, 'binary');
        childProcess.exec(file);
    }
   });

    setTimeout(function(){
      $btn_exportar.button('reset');
    },500);



          }, function (err, num_rows) {
			  var complemento = "";
			  if(nps) complemento = " NPS";
			  userLog(stmt, "Exportaçao"+complemento, 5, err);
            //?
          });

    };

}
CtrlResumoPesqServ.$inject = ['$scope', '$globals'];
