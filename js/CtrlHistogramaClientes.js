function CtrlHistogramaClientes($scope, $globals) {

    win.title = "Histograma de Repetidas";//Brunno 01/10/2019
    var idView = "pag-histograma-clientes";
    var pagina = "#pag-histograma-clientes"
    $scope.versao = versao;
    $scope.rotas = rotas;
    var busca = ""
    var pause = false
    var excel = false
    var stmts = []
    $scope.valor = "24h";

    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    var $view;
    travaBotaoFiltro(0, $scope, "#pag-histograma-clientes", win.title);
    //Grafico

    var resetCanvas = function () {
        $('#myChart').remove(); // this is my <canvas> element
        $('#canvas-container').append('<canvas id="myChart"><canvas>');
        canvas = document.getElementById('myChart');
        ctx = canvas.getContext('2d');
        ctx.canvas.width = $('#graph').width(); // resize to parent width
        ctx.canvas.height = $('#graph').height(); // resize to parent height
        var x = canvas.width / 2;
        var y = canvas.height / 2;
        ctx.font = '10pt Verdana';
        ctx.textAlign = 'center';
        ctx.fillText('This text is centered on the canvas', x, y);
    };
    function restringe_aplicacao(valores) {
        if (valores.length === 0) {
            return console.log("valor 0");
        } else {
            var aplicacao = "";
            for (i = 0; i < valores.length; i++) {
                aplicacao = aplicacao + "'" + valores[i] + "'"
                if (i < valores.length - 1) aplicacao += ","
            }

            return aplicacao;
        }
    }

    ctx = document.getElementById('myChart').getContext('2d');

    // Filtros: data e hora
    var agora = new Date();
    var inicio = Estalo.filtros.filtro_data_hora[0] || hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] || hojeFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };
    var $view;
    $scope.$on('$viewContentLoaded', function () {
        $view = $("#" + idView);
        $view.find(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        // function componenteDataMesSO1(scop, view, refresh) {
        //     function ontemMes() {
        //         date = new Date();
            
        //         return new Date(date.getFullYear(), date.getMonth() -1, 01, 00, 00, 0);
        //     }
        //     if (refresh === true) {
        //         scop.periodo.inicio = ontemMes() 
        //     }
        //     var $Dat_Referencia = view.find(".datahora-mes");
        //     $Dat_Referencia.datetimepicker({
        //         language: 'pt-BR',
        //         pickSeconds: false,
        //         pickTime: false,
        //         startDate: formataDataBR(scop.periodo.min),
        //         value: formataDataBR(scop.periodo.inicio),
        //         format: "MM/yyyy",
        //         viewMode: "months",
        //         minViewMode: "months"
        //     });
        
        //     $Dat_Referencia.data('datetimepicker').setLocalDate(scop.periodo.inicio);
        
        //     //Método executado quando se altera a data
        //     $Dat_Referencia.on('changeDate', function (ev) {
        //         var datahora = new Date(ev.localDate);
        //         scop.$apply((function (datadia) {
        //             return function () {
        //                 scop.periodo.inicio = datahora;
        //                 if(view.attr('id') !== "pag-consolidado-ed"){
        //                     relsVendas.indexOf(view.attr('id')) >= 0 ? carregaAplicacoes(view, true, true, scop) : carregaAplicacoes(view, true, false, scop);
        //                 }
        //             };
        //         })(datahora));
        //         //02/03/2014 escondendo o calendario e mostrando o outro
        //         $(this).data("datetimepicker").hide();
        //         Estalo.filtros.filtro_data_hora[0] = scop.periodo.inicio;
        //     });
        // }


        
        componenteDataMaisHora($scope,$view,true)
        //componenteDataMesSO1 ($scope,$view,true);
        // $('input').click(function() {
        //         if($scope.valor === 'mes'){
        //             document.getElementById("Dias").style.display = "none";
        //             document.getElementById("Meses").style.display = "block"; 
                    
        //         }
        //         if($scope.valor === 'dia'){
        //             document.getElementById("Meses").style.display = "none";
        //             document.getElementById("Dias").style.display = "block";   
                                   
        //         }
              
        //   })
        carregaAplicacoes($view, false, false, $scope);
        carregaSegmentosPorAplicacao($scope, $view)
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });

        // Carregar filtros
        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
            $view.find("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $view.find("div.datahora-fim.input-append.date").data("datetimepicker").hide();
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#" + idView);

            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length > 0) {

                $scope.listarTempo.apply(this);
            } else {
                $scope.filtros_usados = "";
                retornaStatusQuery("erro: tem que selecionar uma aplicação", $scope);
            }


        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this)
            
        });

        // Botão abortar
        $view.on("click", ".abortar", function () {
            killThreads();
            $scope.abortar.apply(this);

        });

        $scope.abortar = function () {
            abortar($scope, pagina, db);
            $view.find(".btn-gerar").button('reset');
            $view.find(".btn-exportar").button('reset');
            $view.find('.ampulheta').hide();
            pause = true;
            counter = stmts.length;
        };

       

    });

    $scope.listarTempo = function () {
        var tabela = ''
        var qtdClientes = []
        var chamadas = []
        $scope.filtros_usados = ""
        resetCanvas();
        console.log('Lista Consolidado')

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var data_ini = precisaoHoraIni($scope.periodo.inicio)
        var data_fim = precisaoHoraIni($scope.periodo.fim)

        $scope.filtros_usados += " Data inicio: " + formataDataHora(data_ini)
        $scope.filtros_usados += " Aplicações: " + restringe_aplicacao(filtro_aplicacoes)
        var $btn_gerar_import = $(this);
        $btn_gerar_import.button("loading");
        
        if ($scope.valor === '24h' || $scope.valor === '2h' || $scope.valor === '1h'|| $scope.valor === '45min' 
        || $scope.valor === '30min'|| $scope.valor === '15min'){
            tabela = 'HistogramaRepetidas' + $scope.valor;
        }
        var stmtTempo = "with cte as ( Select SUM(Qtd) as quantidade from "+tabela+
        " where DatReferencia >= '" + formataDataHora(data_ini) +"'"+
        "	and DatReferencia <= '" + formataDataHora(data_fim) +"'"+
            "   and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ") "
            if (filtro_segmentos.length > 0){
                stmtTempo += "   and CodSegmento in (" + restringe_aplicacao(filtro_segmentos) + ") "
            }else{
                stmtTempo += "   and CodSegmento in ('')"
            }
            
           stmtTempo += " and Categoria = '*' and Repetidas >= 16"
        +") "+
        "Select str(Repetidas) as Repetidas ,SUM(Qtd) as quantidade from " +tabela+
            "	where DatReferencia >= '" + formataDataHora(data_ini) +"'"+
            "	and DatReferencia <= '" + formataDataHora(data_fim) +"'"+
            "   and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ") "
            if (filtro_segmentos.length > 0){
                stmtTempo += "   and CodSegmento in (" + restringe_aplicacao(filtro_segmentos) + ") "
            }else{
                stmtTempo += "   and CodSegmento in ('')"
            }
            
           stmtTempo += " and Categoria = '*' and Repetidas <= 15"+
            " Group by Repetidas union all select '16+' as Repetidas, quantidade from cte"
           ;

        log(stmtTempo);

        db.query(stmtTempo, function (columns) {


            chamadas.push(columns[0].value + "x")
            qtdClientes.push(columns[1].value)


        }, function (err, num_rows) {
            if (err) {
                console.log('Erro ao buscar Importa: ' + err);
                $btn_gerar_import.button("reset");
                limpaProgressBar($scope, "pag-histograma-clientes");
            } else {
                var $btn_exportar = $view.find(".btn-exportar");
                var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
                var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
                var $btn_exportar_jpg = $view.find(".btn-gerar-jpg");

                $btn_exportar.prop("disabled", false);
                $btn_exportar_jpg.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
                console.log('Registros encontrados: ' + num_rows);
                retornaStatusQuery(chamadas.length, $scope);
                $btn_gerar_import.button("reset");

                //limpaProgressBar($scope, "pag-acesso-usuarios");
                //Gráfico
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: chamadas,
                        datasets: [
                            {
                                type: 'bar',
                                label: '',
                                backgroundColor: '#c2a7db',
                                borderColor: '#c2a7db',
                                borderWidth: 1,
                                hidden: false,
                                data: qtdClientes,
                                datalabels: {
                                    anchor: 'end',
                                    align: 'end',
                                    color: 'black',
                                    display: 'auto',
                                    formatter: function (value) {
                                        if (value < 1000) {

                                            return value
                                        }
                                        return formataNumGrande(value);
                                    },
                                },

                                yAxisID: 'client',
                            }
                        ]
                    },
                    options: {

                        legend: {
                            position: false,

                        },
                        scales: {
                            yAxes: [{
                                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                id: 'client',
                                stacked: true,

                            },

                            ],
                            xAxes: [{
                                stacked: false,
                                ticks: {
                                    beginAtZero: true,
                                    suggestedMax: 50
                                },
                                display: true
                            }],
                        },

                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            datalabels: {
                                align: 'end',
                                anchor: 'end',
                                display: function (ctx) {
                                    return ctx
                                }
                            }
                        },
                        hover: {
                            animationDuration: 1
                        },
                        animation: {
                            duration: 100,
                            onComplete: function () {
                                var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;
                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function (bar, index) {
                                        var data = dataset.data[index];
                                        if (data !== 0) {
                                            if (bar._model.y < 15.5) {
                                                ctx.fillStyle = "#FFFFFF";
                                                ctx.fillText(data, bar._model.x, bar._model.y + 15);
                                            }

                                            else {
                                                ctx.fillStyle = "#000000";
                                                if (data >= 1000) {
                                                    modificado = formataNumGrande(data)
                                                } else {
                                                    modificado = data
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                        },

                    },

                });

              
            }
        });
    }

    $scope.listarDetalhado = function () {
        var tabela = ''
        var dados = [];
        $scope.filtros_usados = ""
        console.log('Lista Consolidado')

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var data_ini = precisaoHoraIni($scope.periodo.inicio)
        var data_fim = precisaoHoraIni($scope.periodo.fim)

        $scope.filtros_usados += " Data inicio: " + formataDataHora(data_ini)
        $scope.filtros_usados += " Aplicações: " + restringe_aplicacao(filtro_aplicacoes)
        var $btn_gerar_import = $(this);
        $btn_gerar_import.button("loading");
        
        if ($scope.valor === '24h' || $scope.valor === '2h' || $scope.valor === '1h'|| $scope.valor === '45min' 
        || $scope.valor === '30min'|| $scope.valor === '15min'){
            tabela = 'HistogramaRepetidas' + $scope.valor;
        }
        segmentos = (filtro_segmentos.length > 0) ? ", CodSegmento " : "";
        var stmtTempo = "Select Cast(DatReferencia as date),CodAplicacao, Repetidas,Qtd "+segmentos+" from " +tabela+
            "	where DatReferencia >= '" + formataDataHora(data_ini) +"'"+
            "	and DatReferencia <= '" + formataDataHora(data_fim) +"'"+
            "   and CodAplicacao in (" + restringe_aplicacao(filtro_aplicacoes) + ") "
            if (filtro_segmentos.length > 0){
                stmtTempo += "   and CodSegmento in (" + restringe_aplicacao(filtro_segmentos) + ") "
            }else{
                stmtTempo += "   and CodSegmento in ('')"
            }
            
           stmtTempo += " and Categoria = '*' "
           ;

        log(stmtTempo);

        db.query(stmtTempo, function (columns) {

            
                dados.push({
                    data: formataDataBRString(columns[0].value),
                    aplicacao: columns[1].value,
                    repetidas: columns[2].value,
                    qtd: columns[3].value,
                    segmentos: (filtro_segmentos.length > 0) ?  columns[4].value : ""
                }
                    
                )
           

        }, function (err, num_rows) {
            if (err) {
                console.log('Erro ao buscar Importa: ' + err);
                $btn_gerar_import.button("reset");
                limpaProgressBar($scope, "pag-histograma-clientes");
            } else {
                var $btn_exportar = $view.find(".btn-exportar");
                var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
                var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
                var $btn_exportar_jpg = $view.find(".btn-gerar-jpg");

                $btn_exportar.prop("disabled", false);
                $btn_exportar_jpg.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
                console.log('Registros encontrados: ' + num_rows);
                $btn_gerar_import.button("reset");
                if (dados.length > 0) {
                
                    separator = ";";
                    colunas = "";
					var data = ""
                    file_name = tempDir3() + "export_" + Date.now();
                    
                    if(filtro_segmentos.length > 0){
                        colunas = 'Data;Aplicação;Segmento;Repetida;Quantidade;'
                }else{
                    colunas = 'Data;Aplicação;Repetida;Quantidade;'
    
                }
                    colunas = colunas + "\n"
    
                    for (var i = 0; i < dados.length; i++) {
                        data = dados[i].data + separator+dados[i].aplicacao+separator+dados[i].repetidas+separator+dados[i].qtd+separator
                        colunas += data + "\n";
                        }
                    // for (i = 0; i < $scope.gridDados.rowData.length; i++) {
                    //     colunas = colunas + $scope.gridDados.rowData[i].join(separator) + "\n"
                    // }
                    $(".dropdown-exportar").toggle();
                    exportaTXT(colunas, file_name, 'csv', true);
                    $scope.csv = [];
                    $view.find(".btn-exportar").button('click');
               
            }
            }
        });
    }
   
    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
        $scope.listarDetalhado.apply(this);   
        
    };

}

CtrlHistogramaClientes.$inject = ['$scope', '$globals'];