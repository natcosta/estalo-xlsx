function CtrlChamadasCTG2($scope, $globals) {
  win.title = "Detalhamento de chamadas CTG";
var limite = 50000;
  $scope.versao = versao;
  //Alex 08/02/2014	
  travaBotaoFiltro(0, $scope, "#pag-chamadas-ctg", "Detalhamento de chamadas CTG 2 - Limite para exportação xlsx: "+limite+" registros.");
  $scope.dados = [];
  $scope.csv = [];

  $scope.colunas = [];
  $scope.gridDados = {
      data: "dados",
      columnDefs: "colunas",
      enableColumnResize: true,
      enablePinning: true
  };

  $scope.aplicacoes = []; // FIXME: copy
  $scope.sites = [];
  $scope.segmentos = [];
  $scope.ordenacao = ['DATA_HORA_INICIO_PASSO1'];
  $scope.decrescente = false;

  var tempData = "";	

  var novaMacro;
var csvExportado = '';


  // Filtros
  $scope.filtros = {
      aplicacoes: [],
      // ALEX - 29/01/2013
      sites: [],
      segmentos: [],
      isHFiltroED: false,
      porDia: false,
      porDDD: false
  };

  var exibeNLU = false;
  var exibeFibraRep = false;

  //Alex 02/08/2017
  $scope.intervalo = testaPermissoesAclBotoes('pag-chamadas-ctg');

  function geraColunas() {
      var array = [];
  
      array.push(
        {
          field: "DATA_HORA_INICIO_PASSO1",
          displayName: "Data hora (Operação 1)",
          width: 150,
          pinnable: true,
          pinned: true
        },
        {
          field: "DataHora_Inicio",
          displayName: "Data hora (URA)",
          width: 150,
          pinnable: false
        }
      );
  
      if ($scope.intervalo) {
        array.push({
          field: "intervalo",
          displayName: "Intervalo (Segundos)",
          width: 50,
          pinnable: false
        });
      }
  
      array.push(
        {
          field: "CodAplicacao",
          displayName: "Aplicação",
          width: 120,
          pinnable: false
        },
        {
          field: "CodSegmento",
          displayName: "Segmento",
          width: 75,
          pinnable: false
        },
        {
          field: "NumANI",
          displayName: "Chamador",
          width: 100,
          pinnable: false
        },
        {
          field: "ClienteTratado",
          displayName: "Tratado",
          width: 100,
          pinnable: false
        },
        {
          field: "UltimoED",
          displayName: "Último estado",
          width: 120,
          pinnable: false
        },
        {
          field: "TransferID",
          displayName: "TransferID",
          width: 120,
          pinnable: false
        },
        {
          field: "CodRegra",
          displayName: "Regra",
          width: 120,
          pinnable: false
        },
        {
          field: "CtxID",
          displayName: "CTXID",
          width: 120,
          pinnable: false
        },
        {
          field: "CALL_DISP",
          displayName: "CALL_DISP",
          width: 120,
          pinnable: false
        },
        {
          field: "FL_TRANSF_EXT",
          displayName: "Transf. Ext",
          width: 120,
          pinnable: false
        },
        {
          field: "FL_TRANSFERENCIA",
          displayName: "Transf.",
          width: 120,
          pinnable: false
        }
      );
  
      if (exibeNLU || exibeFibraRep)
        array.push(
          {
            field: "confianca",
            displayName: "Confiança",
            width: 120,
            pinnable: false
          },
          {
            field: "elocucao",
            displayName: "Elocução",
            width: 120,
            pinnable: false
          },
          {
            field: "resultado",
            displayName: "Resultado",
            width: 120,
            pinnable: false
          },
          {
            field: "reconhecimento",
            displayName: "Reconhecimento",
            width: 120,
            pinnable: false
          },
          {
            field: "score",
            displayName: "Score",
            width: 120,
            pinnable: false
          },
          {
            field: "tipo",
            displayName: "Tipo de Chamada",
            width: 142,
            pinnable: false
          }
        );
      array.push(
        {
          field: "OPERACAO_FINAL_PASSO1",
          displayName: "Operação 1",
          width: 90,
          pinnable: false
        },
        {
          field: "EMPRESA_PASSO1",
          displayName: "Empresa 1",
          width: 90,
          pinnable: false
        },
        {
          field: "operacao2",
          displayName: "Operação 2",
          width: 90,
          pinnable: false
        },
        {
          field: "empresa2",
          displayName: "Empresa 2",
          width: 90,
          pinnable: false
        },
        {
          field: "operacao3",
          displayName: "Operação 3",
          width: 90,
          pinnable: false
        },
        {
          field: "empresa3",
          displayName: "Empresa 3",
          width: 90,
          pinnable: false
        },
        {
          field: "operacao4",
          displayName: "Operação 4",
          width: 90,
          pinnable: false
        },
        {
          field: "empresa4",
          displayName: "Empresa 4",
          width: 90,
          pinnable: false
        },
        {
          field: "operacao5",
          displayName: "Operação 5",
          width: 90,
          pinnable: false
        },
        {
          field: "empresa5",
          displayName: "Empresa 5",
          width: 90,
          pinnable: false
        },
        {
          field: "operacao6",
          displayName: "Operação 6",
          width: 90,
          pinnable: false
        },
        {
          field: "empresa6",
          displayName: "Empresa 6",
          width: 90,
          pinnable: false
        },
        {
          field: "SessaoRecVoz",
          displayName: "Sessão",
          width: 90,
          pinnable: false
        },
        {
          field: "Assunto",
          displayName: "Assunto",
          width: 120,
          pinnable: false
        },
        {
          field: "DuracaoURA",
          displayName: "Tempo atend.",
          width: 120,
          pinnable: false
        },
        {
          field: "DATA_HORA_FIM",
          displayName: "Data hora fim (HUMANO)",
          width: 120,
          pinnable: false
        },
        {
          field: "BDAberto",
          displayName: "BDAberto",
          width: 120,
          pinnable: false
        },
        {
          field: "OSAberta",
          displayName: "OSAberta",
          width: 120,
          pinnable: false
        },
        {
          field: "URAResgateAnatel",
          displayName: "URAResgateAnatel",
          width: 120,
          pinnable: false
        },
        {
          field: "URAProduto",
          displayName: "URAProduto",
          width: 120,
          pinnable: false
        },
        {
          field: "URAOpcao",
          displayName: "URAOpcao",
          width: 120,
          pinnable: false
        },
        {
          field: "COD_SKILL_PASSO2",
          displayName: "COD_SKILL_PASSO2",
          width: 120,
          pinnable: false
        },
  
        {
          field: "TAG_RECVOZ",
          displayName: "TAG_RECVOZ",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BDABERTODENTRODOPRAZO",
          displayName: "URA_BDABERTODENTRODOPRAZO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BDABERTOFORADOPRAZO",
          displayName: "URA_BDABERTOFORADOPRAZO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BDVELOXABERTODENTRODOPRAZO",
          displayName: "URA_BDVELOXABERTODENTRODOPRAZO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BDVELOXABERTOFORADOPRAZO",
          displayName: "URA_BDVELOXABERTOFORADOPRAZO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BERCARIO",
          displayName: "URA_BERCARIO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BLACKLISTCREP",
          displayName: "URA_BLACKLISTCREP",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BLOQUEIO_FIXA",
          displayName: "URA_BLOQUEIO_FIXA",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_CLIENTE_FIBRA",
          displayName: "URA_CLIENTE_FIBRA",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_HORARIOCREP",
          displayName: "URA_HORARIOCREP",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_MAILING_R2",
          displayName: "URA_MAILING_R2",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_OS",
          displayName: "URA_OS",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_PILOTOCREP",
          displayName: "URA_PILOTOCREP",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_QUARENTENACREP",
          displayName: "URA_QUARENTENACREP",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_REPETIDACREP",
          displayName: "URA_REPETIDACREP",
          width: 120,
          pinnable: false
        },
  
        {
          field: "URA_RESGATEANATELST",
          displayName: "URA_RESGATEANATELST",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_TIPOBLOQUEIO",
          displayName: "URA_TIPOBLOQUEIO",
          width: 120,
          pinnable: false
        },
        {
          field: "VIP_FLAG",
          displayName: "VIP_FLAG",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_BAIRRO",
          displayName: "URA_BAIRRO",
          width: 120,
          pinnable: false
        },
        {
          field: "URA_CIDADE",
          displayName: "URA_CIDADE",
          width: 120,
          pinnable: false
        }
      );
  
      return array;
    }

  // Filtros: data e hora
  var agora = new Date();

  //var dat_consoli = new Date(teste_data);

  //Alex 03/03/2014
  var inicio, fim;
  Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
  Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

  $scope.periodo = {
      inicio: inicio,
      fim: fim,
      min: new Date(2013, 11, 1),
      max: agora // FIXME: atualizar ao virar o dia

  };

  var $view;


  $scope.$on('$viewContentLoaded', function() {
      $view = $("#pag-chamadas-ctg");

      //19/03/2014
      componenteDataHora($scope, $view);
      carregaRegioes($view);
      carregaAplicacoes($view, false, false, $scope);
      carregaOperacoesECH($view);
      carregaSites($view);
      carregaEmpresas($view);
      carregaRegras($view);
      carregaSkills($view);

      carregaSegmentosPorAplicacao($scope, $view, true);
      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function() {
          carregaSegmentosPorAplicacao($scope, $view)
      });

      carregaDDDsPorAplicacao($scope, $view, true);
      // Popula lista de  DDDs a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function() {
          carregaDDDsPorAplicacao($scope, $view)
      });


      //GILBERTOO - change de segmentos
      $view.on("change", "select.filtro-segmento", function() {
          Estalo.filtros.filtro_segmentos = $(this).val();
      });

      // Marca todos os operacoes
      $view.on("click", "#alinkOper", function() {
          marcaTodosIndependente($('.filtro-operacao'), 'operacoes')
      });

      // Marca todos os edstransf
      $view.on("click", "#alinkTid", function() {
          marcaTodosIndependente($('.filtro-transferid'), 'transferid')
      });

      // Marca todos os edstransf
      $view.on("click", "#alinkEDT", function() {
          marcaTodosIndependente($('.filtro-edstransf'), 'edstransf')
      });

      // Marca todos os sites
      $view.on("click", "#alinkSite", function() {
          marcaTodosIndependente($('.filtro-site'), 'sites')
      });

      // Marca todos os segmentos
      $view.on("click", "#alinkSeg", function() {
          marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
      });

      // Marca todos os empresas
      $view.on("click", "#alinkEmp", function() {
          marcaTodosIndependente($('.filtro-empresa'), 'empresa')
      });

      // Marca todos os regras
      $view.on("click", "#alinkRegra", function() {
          marcaTodosIndependente($('.filtro-regra'), 'regra')
      });

      // Marca todos os skills
      $view.on("click", "#alinkSkill", function() {
          marcaTodosIndependente($('.filtro-skill'), 'skill')
      });

      //Marcar todas aplicações
      $view.on("click", "#alinkApl", function() {
          marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
      });


      $view.on("click", ".btn-exportar", function() {
          $scope.exportaXLSX.apply(this);
      });

      // Limpa filtros Alex 03/03/2014
      $view.on("click", ".btn-limpar-filtros", function() {
          $scope.porRegEDDD = false;
          $scope.limparFiltros.apply(this);
      });

      $scope.limparFiltros = function() {
          iniciaFiltros();
          componenteDataHora($scope, $view, true);

          var partsPath = window.location.pathname.split("/");
          var part = partsPath[partsPath.length - 1];

          var $btn_limpar = $view.find(".btn-limpar-filtros");
          $btn_limpar.prop("disabled", true);
          $btn_limpar.button('loading');

          setTimeout(function() {
              $btn_limpar.button('reset');
              //window.location.href = part + window.location.hash +'/';
          }, 500);
      }

      // Botão agora Alex 03/03/2014
      $view.on("click", ".btn-agora", function() {
          $scope.agora.apply(this);
      });

      $scope.agora = function() {
          iniciaAgora($view, $scope);
      }
  
  $view.on("click", ".btn-exportarCSV-interface", function() {
    var newName = formataDataHoraMilis(new Date()).replace(/[^0-9]/g,'');
    if(csvExportado === ''){
      fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData,'ascii');				
      fs.renameSync(tempDir3() + 'dados_ctg.csv',	tempDir3() + 'export_'+newName+'.csv');                
              childProcess.exec("start excel " + tempDir3() + 'export_'+ newName +'.csv', 
              function (error, stdout, stderr) {
                  if (error) {
                      console.error("exec error: " + error);
                      alert("Parece que o seu sistema não tem o Excel instalado.");
                      childProcess.exec(tempDir3() + 'export_'+newName+'.csv');
                      return;
                  }                        
                  
              });
              csvExportado = tempDir3() + 'export_'+newName+'.csv';               
             
    }else{
      alert("Dado já foi exportado("+csvExportado+").");
    }
    
  });

      // Botão abortar Alex 23/05/2014
      $view.on("click", ".abortar", function() {
          $scope.abortar.apply(this);
		var $btn_exportar = $view.find(".btn-exportar");
		var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
		var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
		$btn_exportar.prop("disabled", false);
		$btn_exportar_csv.prop("disabled", false);
		$btn_exportar_dropdown.prop("disabled", false);
    /*if ($scope.dados.length > 0) {
      fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData,'ascii');						
      childProcess.exec(tempDir3() + 'dados_ctg.csv');

    }*/
      });

      //Alex 23/05/2014
      $scope.abortar = function() {
          abortar($scope, "#pag-chamadas-ctg");
      }

      // Lista chamadas conforme filtros
      $view.on("click", ".btn-gerar", function() {

          limpaProgressBar($scope, "#pag-chamadas-ctg");
          //22/03/2014 Testa se data início é maior que a data fim
          var data_ini = $scope.periodo.inicio,
              data_fim = $scope.periodo.fim;
          var testedata = testeDataMaisHora(data_ini, data_fim);
          if (testedata !== "") {
              setTimeout(function() {
                  atualizaInfo($scope, testedata);
                  effectNotification();
                  $view.find(".btn-gerar").button('reset');
              }, 500);
              return;
          }
          $scope.colunas = geraColunas();
          $scope.listaDados.apply(this);
      });

      $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
  
  $view.on("change", 'input:radio[name=tela]:checked', function() {
  if ($("input[name='tela']:checked").val() == 'exibeNLU') {			
      exibeNLU = true;
  }else{
      exibeNLU = false;
  }
  if ($("input[name='tela']:checked").val() == 'exibeFibraRep') {			
    exibeFibraRep = true;
  }else{
    exibeFibraRep = false;
  }
  });  
  $('input:radio[name=tela]').css('margin','1px 3px 3px 3px');
});
  
  
  


  // Lista chamadas conforme filtros
  $scope.listaDados = function() {		
  csvExportado = '';
  
  //var totalGeral = 0;
  
  if (fs.existsSync(tempDir3() + 'dados_ctg.csv')) fs.unlinkSync(tempDir3() + 'dados_ctg.csv');			
  var header = '';
  for (var item in $scope.colunas) {
    if(typeof($scope.colunas[item]) === "object") header += $scope.colunas[item].displayName +';';                
  }			
  header += ';\r\n';
  tempData = header;
      $scope.csv = [];
      var datasComDados = {};
      $globals.numeroDeRegistros = 0;

      var $btn_exportar = $view.find(".btn-exportar");
      var $btn_exportar_csv = $view.find(".btn-exportarCSV-interface");
      var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
      $btn_exportar.prop("disabled", true);
      $btn_exportar_csv.prop("disabled", true);
      $btn_exportar_dropdown.prop("disabled", true);

      var $btn_gerar = $(this);
      $btn_gerar.button('loading');

      var data_ini = $scope.periodo.inicio,
          data_fim = $scope.periodo.fim;

      var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

      if (filtro_aplicacoes.length === 0) {
          atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
          $btn_gerar.button('reset');
          return;
      }
      var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
      var filtro_sites = $view.find("select.filtro-site").val() || [];
      var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
      var filtro_ed = $view.find("select.filtro-ed").val() || [];
      var filtro_edt = garanteVetor($(".filtro-edstransf").val());
      if (filtro_edt.length > 0) {
          filtro_edt = filtro_edt.join(",").split(",");
      }
      var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
      var filtro_regras = $view.find("select.filtro-regra").val() || [];
      var filtro_skills = $view.find("select.filtro-skill").val() || [];
      var filtro_transferid = $view.find("select.filtro-transferid").val() || [];


      //filtros usados
      $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
          " até " + formataDataHoraBR($scope.periodo.fim);
      if (filtro_aplicacoes.length > 0) $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;        
      if (filtro_operacoes.length > 0) $scope.filtros_usados += " Operações: " + filtro_operacoes;        
      if (filtro_sites.length > 0) $scope.filtros_usados += " Sites: " + filtro_sites;        
      if (filtro_segmentos.length > 0) $scope.filtros_usados += " Segmentos: " + filtro_segmentos;     
      if (filtro_ed.length > 0) $scope.filtros_usados += " EDs: " + filtro_ed;        
      if (filtro_edt.length > 0) $scope.filtros_usados += " EDsTransf: " + filtro_edt;        
      if (filtro_empresas.length > 0) $scope.filtros_usados += " Empresas: " + filtro_empresas;
      if (filtro_regras.length > 0) $scope.filtros_usados += " Regras: " + filtro_regras;
      if (filtro_transferid.length > 0) $scope.filtros_transferid += " TransferId: " + filtro_transferid;
      if (filtro_skills.length > 0) {
          var filtro_skills_mod = [];
          for (var i = 0; i < filtro_skills.length; i++) {
              var lsRegExp = /%/g;
              filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp, '&lt;'));
          }
          $scope.filtros_usados += " Skills: " + filtro_skills_mod;
      }

      $scope.dados = [];
      var stmt = "";
      //var executaQuery = "";


      var tabelaConsultar = "CruzamentoECH2";

      camposAdicionais = "";
      if (exibeNLU) {
          camposAdicionais = ", confiancarecvoz, elocucaorecvoz, resultadorecvoz, reconhecimentorecvoz, scorerecvoz, tipo_chamada";
          tabelaConsultar = "CruzamentoECH2RecVoz";
      }else if(exibeFibraRep){
        camposAdicionais = ", confiancarecvoz, elocucaorecvoz, resultadorecvoz, reconhecimentorecvoz, scorerecvoz, tipo_chamada";
        tabelaConsultar = "CruzamentoECH2FibraRep";
      }



      // CONSULTA SQL 1.1
      stmt =
    db.use +
    " SELECT DATA_HORA_INICIO_PASSO1,DataHora_Inicio,CodAplicacao,CodSegmento,NumANI,ClienteTratado,ED.Nom_Estado AS UltimoED,ucid,TEL_BINADO, CALL_DISP, FL_TRANSF_EXT, FL_TRANSFERENCIA" +
    camposAdicionais +
    " ,TransferID,CtxID,CodRegra,isnull(SessaoRecVoz,'') as SessaoRecVoz ,OPERACAO_FINAL_PASSO1,EMPRESA_PASSO1,Transferencias,Assunto,DuracaoURA,DATA_HORA_FIM, BDAberto, OSAberta, URAResgateAnatel, URAProduto, URAOpcao, VariaveisJSON" +
    " FROM " +
    tabelaConsultar +
    " AS RE" +
    " LEFT OUTER JOIN ESTADO_DIALOGO AS ED" +
    " ON RE.UltimoED = ED.Cod_Estado AND RE.CodAplicacao =  ED.Cod_Aplicacao" +
    " WHERE 1 = 1" +
    " AND DATA_HORA_INICIO_PASSO1 >= '" +
    formataDataHora(data_ini) +
    "'" +
    " AND DATA_HORA_INICIO_PASSO1 <= '" +
    formataDataHora(data_fim) +
    "'";
  stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
  stmt += restringe_consulta("TransferId", filtro_transferid, true);
  stmt += restringe_consulta("OPERACAO_FINAL_PASSO1", filtro_operacoes, true);
  stmt += restringe_consulta("CodSite", filtro_sites, true);
  stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
  stmt += restringe_consulta("EMPRESA_PASSO1", filtro_empresas, true);
  stmt += restringe_consulta("UltimoED", filtro_edt, true);
  stmt += " ORDER BY DATA_HORA_INICIO_PASSO1 ASC";
  
      try {
          //executaQuery = executaQuery2;
          log(stmt);
      } catch (err) {
          console.log(err);
      }

      var stmtCountRows = stmtContaLinhasChamadasCTG(stmt);

      // Alex 24/02/2014 Contador de linhas 
      function contaLinhas(columns) {
    //var item = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'": "'+_.size(datasComDados)+'"}';
    var item1 = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'4": "'+columns[1].value+'0:00'+'|'+columns[1].value+'4:59'+'"}';
    var item2 = '{"'+columns[1].value.replace(/[^0-9]/g,'')+'9": "'+columns[1].value+'5:00'+'|'+columns[1].value+'9:59'+'"}';
    jsonConcat(datasComDados,JSON.parse(item1));	
    jsonConcat(datasComDados,JSON.parse(item2));	
          $globals.numeroDeRegistros = columns[0].value;
      }
  
  var objColunasTabela = {};
  
  function executaQuery1(columns) {
    
    executaQuery(columns);
    // Insere no grid
    $scope.dados.push(objColunasTabela);
    var a = [];			
    for (var item in $scope.colunas) {				
      a.push(objColunasTabela[$scope.colunas[item].field]);
    }
    $scope.csv.push(a);	
    
    // FIM GERA DADOS COLUNAS
          //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
          if ($scope.dados.length % 1000 === 0) {		
              fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData,'ascii');
              tempData = "";
              $scope.$apply();

          }
  }
  
  function executaQuery2(columns) {
    
    executaQuery(columns);
    if (tempData.length > 240000) {	
      fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData,'ascii');
      tempData = "";
    }
  }
  
  function executaQuery(columns) {
    //totalGeral++;
    objColunasTabela = {};
          
          // GERA DADOS COLUNAS
          // DETALHAMENTO INICIO
          objColunasTabela["DATA_HORA_INICIO_PASSO1"] = columns["DATA_HORA_INICIO_PASSO1"] !== undefined ? columns["DATA_HORA_INICIO_PASSO1"].value : undefined;
          objColunasTabela["DataHora_Inicio"] = columns["DataHora_Inicio"] !== undefined ? columns["DataHora_Inicio"].value : undefined;
          objColunasTabela["intervalo"] = (new Date(objColunasTabela["DATA_HORA_INICIO_PASSO1"]) - new Date(objColunasTabela["DataHora_Inicio"])) / 1000;
          objColunasTabela["CodAplicacao"] = columns["CodAplicacao"] !== undefined ? columns["CodAplicacao"].value : undefined;
          objColunasTabela["UltimoED"] = columns["UltimoED"] !== undefined ? columns["UltimoED"].value : undefined;
          objColunasTabela["CodAplicacao"] = obtemNomeAplicacao(objColunasTabela["CodAplicacao"]);
          objColunasTabela["CodSegmento"] = columns["CodSegmento"] !== undefined ? columns["CodSegmento"].value : undefined;
          objColunasTabela["CodSegmento"] = obtemNomeSegmento(objColunasTabela["CodSegmento"]);
          objColunasTabela["NumANI"] = columns["NumANI"] !== undefined ? columns["NumANI"].value : undefined;
          objColunasTabela["ClienteTratado"] = columns["ClienteTratado"] !== undefined ? columns["ClienteTratado"].value : undefined;
          objColunasTabela["TransferID"] = columns["TransferID"] !== undefined ? columns["TransferID"].value : undefined;
          objColunasTabela["CtxID"] = columns["CtxID"] !== undefined ? (columns["CtxID"].value).toString(): undefined;
          objColunasTabela["UCID"] = columns["UCID"] !== undefined ? columns["UCID"].value : undefined;
    
          objColunasTabela["CALL_DISP"] = columns["CALL_DISP"] !== undefined ? columns["CALL_DISP"].value : undefined;
          objColunasTabela["FL_TRANSF_EXT"] = columns["FL_TRANSF_EXT"] !== undefined ? columns["FL_TRANSF_EXT"].value : undefined;
          objColunasTabela["FL_TRANSFERENCIA"] = columns["FL_TRANSFERENCIA"] !== undefined ? columns["FL_TRANSFERENCIA"].value : undefined;
    
    // NLU
          if (exibeNLU || exibeFibraRep) {
              objColunasTabela["confianca"] = columns["confiancarecvoz"] !== undefined ? columns["confiancarecvoz"].value : undefined;
              objColunasTabela["elocucao"] = columns["elocucaorecvoz"] !== undefined ? columns["elocucaorecvoz"].value : undefined;
              objColunasTabela["resultado"] = columns["resultadorecvoz"] !== undefined ? columns["resultadorecvoz"].value : undefined;
              objColunasTabela["reconhecimento"] = columns["reconhecimentorecvoz"] !== undefined ? columns["reconhecimentorecvoz"].value : undefined;
              objColunasTabela["score"] = columns["scorerecvoz"] !== undefined ? columns["scorerecvoz"].value : undefined;
              objColunasTabela["tipo"] = columns["tipo_chamada"] !== undefined ? columns["tipo_chamada"].value : undefined;
          }

          objColunasTabela["TEL_DIGITADO"] = columns["TEL_DIGITADO"] !== undefined ? columns["TEL_DIGITADO"].value : undefined;
          objColunasTabela["TEL_BINADO"] = columns["TEL_BINADO"] !== undefined ? columns["TEL_BINADO"].value : undefined;
          objColunasTabela["CLITRATADO"] = columns["CLITRATADO"] !== undefined ? columns["CLITRATADO"].value : undefined;
          objColunasTabela["CHAM_ATENDIDA"] = columns["CHAM_ATENDIDA"] !== undefined ? columns["CHAM_ATENDIDA"].value : undefined;
          objColunasTabela["CHAM_ABAND"] = columns["CHAM_ABAND"] !== undefined ? columns["CHAM_ABAND"].value : undefined;

          objColunasTabela["DATA_HORA_FIM_PASSO1"] = columns["DATA_HORA_FIM_PASSO1"] !== undefined ? columns["DATA_HORA_FIM_PASSO1"].value : undefined;
          objColunasTabela["DURACAO_PASSO1"] = columns["DURACAO_PASSO1"] !== undefined ? columns["DURACAO_PASSO1"].value : undefined;
          objColunasTabela["TEMPO_FILA_PASSO1"] = columns["TEMPO_FILA_PASSO1"] !== undefined ? columns["TEMPO_FILA_PASSO1"].value : undefined;
          objColunasTabela["MATRICULA_AGENTE_PASSO1"] = columns["MATRICULA_AGENTE_PASSO1"] !== undefined ? columns["MATRICULA_AGENTE_PASSO1"].value : undefined;
          objColunasTabela["COD_SKILL_PASSO1"] = columns["COD_SKILL_PASSO1"] !== undefined ? columns["COD_SKILL_PASSO1"].value : undefined;
          objColunasTabela["ID_ECADOP_PASSO1"] = columns["ID_ECADOP_PASSO1"] !== undefined ? columns["ID_ECADOP_PASSO1"].value : undefined;
          objColunasTabela["DAC_PASSO1"] = columns["DAC_PASSO1"] !== undefined ? columns["DAC_PASSO1"].value : undefined;
          objColunasTabela["OPERACAO_FINAL_PASSO1"] = columns["OPERACAO_FINAL_PASSO1"] !== undefined ? columns["OPERACAO_FINAL_PASSO1"].value : undefined;
          objColunasTabela["EMPRESA_PASSO1"] = columns["EMPRESA_PASSO1"] !== undefined ? columns["EMPRESA_PASSO1"].value : undefined;
          objColunasTabela["CodRegra"] = columns["CodRegra"] !== undefined ? columns["CodRegra"].value : undefined;
          objColunasTabela["SessaoRecVoz"] = columns["SessaoRecVoz"] !== undefined ? columns["SessaoRecVoz"].value : undefined;
          objColunasTabela["Assunto"] = columns["Assunto"] !== undefined ? columns["Assunto"].value : undefined;
    objColunasTabela["DuracaoURA"] = columns["DuracaoURA"] !== undefined ? columns["DuracaoURA"].value : undefined;
    objColunasTabela["DATA_HORA_FIM"] = columns["DATA_HORA_FIM"] !== undefined ? columns["DATA_HORA_FIM"].value : undefined;
          // DETALHAMENTO FIM PT1

          // PARSE JSON OBJ TRANSFERENCIAS
          objColunasTabela["Transferencias"] = columns["Transferencias"] !== undefined ? columns["Transferencias"].value : undefined;
          
          // Nova coluna inserida ES-4403 25/02/2019 - Paulo Raoni
          var COD_SKILL_PASSO2 = objColunasTabela["Transferencias"] ? JSON.parse(objColunasTabela["Transferencias"]) : null;
          COD_SKILL_PASSO2 = COD_SKILL_PASSO2.passos && COD_SKILL_PASSO2.passos.length > 0 ? (COD_SKILL_PASSO2.passos[0].COD_SKILL ? COD_SKILL_PASSO2.passos[0].COD_SKILL : null ) : null;
          //console.log("Transferências: ",objColunasTabela["Transferencias"]);
          //console.log("Transferências parsed: ", JSON.parse(objColunasTabela["Transferencias"]));
          //console.log("COD_SKILL_PASSO2: ", COD_SKILL_PASSO2);

          objColunasTabela["COD_SKILL_PASSO2"] = COD_SKILL_PASSO2 ? COD_SKILL_PASSO2 : null;

           // Novas colunas inseridas ES-4403 21/03/2019 - Paulo Raoni
    objColunasTabela["VariaveisJSON"] =
    columns["VariaveisJSON"] !== undefined
      ? columns["VariaveisJSON"].value
      : undefined;
      var variaveisJSONParsed = objColunasTabela["VariaveisJSON"] !== undefined
      ? JSON.parse(objColunasTabela["VariaveisJSON"])
      : undefined;
       
      // console.log(variaveisJSONParsed);

  // console.log("VariaveisJSONParsed: ", variaveisJSONParsed);

  const variaveisArray = [
    ["eg","TAG_RECVOZ"],
    ["c8","URA_BDABERTODENTRODOPRAZO"],
    ["c9","URA_BDABERTOFORADOPRAZO"],
    ["ca", "URA_BDVELOXABERTODENTRODOPRAZO"],
    ["cb", "URA_BDVELOXABERTOFORADOPRAZO"],
    ["cc","URA_BERCARIO"],
    ["e6","URA_BLACKLISTCREP"],
    ["ed","URA_BLOQUEIO_FIXA"],
    ["ee","URA_CLIENTE_FIBRA"],
    ["e7","URA_HORARIOCREP"],
    ["d3","URA_MAILING_R2"],
    ["d6","URA_OS"],
    ["e5","URA_PILOTOCREP"],
    ["e4","URA_QUARENTENACREP"],
    ["e3","URA_REPETIDACREP"],
    ["e8","URA_RESGATEANATELST"],
    ["ec","URA_TIPOBLOQUEIO"],
    ["ef","VIP_FLAG"],
    ["eh","URA_BAIRRO"],
    ["ei","URA_CIDADE"]
    ];


    function insertVariaveisJSONParsedIntoGrid() {

      variaveisArray.forEach(function(el) {      
        var key = el[0];
        var value = el[1];
        if (!variaveisJSONParsed) {
          objColunasTabela[value] = undefined;
          return;
        }
        if(variaveisJSONParsed)  {            
            var valueJSON = variaveisJSONParsed[key] ? variaveisJSONParsed[key] : null;
            objColunasTabela[value] = valueJSON ? valueJSON : null;
        } 
         
    });    

      // var valuesNotBoolean = ["TAG_RECVOZ", "URA_MAILING_R2", "URA_OS"];
      // var valuesNotBoolean = ["eg", "d3", "d6", "eh", "ei"];     
    
      // variaveisArray.forEach(function(x) {
      //   var key = x[0];
      //   var value = x[1];
        
        
      //   if (!variaveisJSONParsed) {
      //     objColunasTabela[value] = valuesNotBoolean.indexOf(key) > -1 ? null : false;
      //     return;
      //   }
      //   if(key in variaveisJSONParsed) {
      //     var valueJSON = variaveisJSONParsed[key];
      //     if(!valueJSON || valueJSON == "N" || valueJSON == "F") {
      //       objColunasTabela[value] = false;
      //       return;
      //     }
      //     if(valueJSON == true || valueJSON == "S" || valueJSON == "T" || valueJSON == "V") {
      //       objColunasTabela[value] = true;
      //       return;
      //     }
      //     objColunasTabela[value] = valueJSON ? valueJSON : null;
      //     return;

      //   } else {
      //     objColunasTabela[value] = valuesNotBoolean.indexOf(key) > -1 ? null : false;
      //   }
       
      // });    
    }


    insertVariaveisJSONParsedIntoGrid();
          try {
              if (typeof objColunasTabela["Transferencias"] !== "object") objColunasTabela["Transferencias"] = JSON.parse(objColunasTabela["Transferencias"]);
              var ultimo = objColunasTabela["Transferencias"].passos.length;
              for (var i = 0; i < ultimo; i++) {
                  objColunasTabela["operacao" + (i + 2)] = objColunasTabela["Transferencias"].passos[i].OPERACAO_FINAL;
                  objColunasTabela["empresa" + (i + 2)] = objColunasTabela["Transferencias"].passos[i].EMPRESA;
              }

          } catch (ex) {
              objColunasTabela["operacao2"] = "";
              objColunasTabela["empresa2"] = "";
              objColunasTabela["operacao3"] = "";
              objColunasTabela["empresa3"] = "";
              objColunasTabela["operacao4"] = "";
              objColunasTabela["empresa4"] = "";
              objColunasTabela["operacao5"] = "";
              objColunasTabela["empresa5"] = "";
              objColunasTabela["operacao6"] = "";
              objColunasTabela["empresa6"] = "";
              //console.log(ex);
          }
          // FIM PARSE JSON OBJ TRANSFERENCIAS
          // DETALHAMENTO FIM PT2     
    if(objColunasTabela["DATA_HORA_FIM"] !== null)	objColunasTabela["DATA_HORA_FIM"] = formataDataHoraBR(objColunasTabela["DATA_HORA_FIM"]);
      objColunasTabela["DataHora_Inicio"] = formataDataHoraBR(objColunasTabela["DataHora_Inicio"]);			
      objColunasTabela["DATA_HORA_INICIO_PASSO1"] = formataDataHoraBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]);
          
          // Novos dados inseridos ES-4181 01/02/2019 - Paulo Raoni
          objColunasTabela["BDAberto"] = columns["BDAberto"] !== null ? columns["BDAberto"].value : null;
          objColunasTabela["OSAberta"] = columns["OSAberta"] !== null ? columns["OSAberta"].value : null;
          objColunasTabela["URAResgateAnatel"] = columns["URAResgateAnatel"] !== null ? columns["URAResgateAnatel"].value : null;
          objColunasTabela["URAProduto"] = columns["URAProduto"] !== null ? columns["URAProduto"].value : null;
          objColunasTabela["URAOpcao"] = columns["URAOpcao"] !== null ? columns["URAOpcao"].value : null;
      

          tempData += objColunasTabela.DATA_HORA_INICIO_PASSO1 + ';' + 
      objColunasTabela.DataHora_Inicio + ';' +
              objColunasTabela.CodAplicacao + ';' + 
      objColunasTabela.CodSegmento + ';' +
              objColunasTabela.NumANI + ';' +
              objColunasTabela.ClienteTratado + ';' +
              objColunasTabela.UltimoED + ';' +
              objColunasTabela.TransferID + ';' +
              objColunasTabela.CodRegra + ';' +
              (objColunasTabela.CtxID !== undefined ? "'"+objColunasTabela.CtxID : "") + ';' +                
      (objColunasTabela.CALL_DISP !== undefined ? objColunasTabela.CALL_DISP : "") + ';' +
      (objColunasTabela.FL_TRANSFERENCIA !== undefined ? objColunasTabela.FL_TRANSFERENCIA : "") + ';' +
              (objColunasTabela.FL_TRANSF_EXT !== undefined ? objColunasTabela.FL_TRANSF_EXT : "") + ';';                
          
    if(exibeNLU || exibeFibraRep){
      tempData +=(objColunasTabela.confianca !== undefined ? objColunasTabela.confianca : "") + ';' +
              (objColunasTabela.elocucao !== undefined ? objColunasTabela.elocucao : "") + ';' +
              (objColunasTabela.resultado !== undefined ? objColunasTabela.resultado : "") + ';' +
              (objColunasTabela.reconhecimento !== undefined ? objColunasTabela.reconhecimento : "") + ';' +
              (objColunasTabela.score !== undefined ? objColunasTabela.score : "") + ';' +
      (objColunasTabela.tipo !== undefined ? objColunasTabela.tipo : "") + ';';
    }
              
    tempData +=	(objColunasTabela.OPERACAO_FINAL_PASSO1 !== undefined ? objColunasTabela.OPERACAO_FINAL_PASSO1 : "") + ';' +
              (objColunasTabela.EMPRESA_PASSO1 !== undefined ? objColunasTabela.EMPRESA_PASSO1 : "") + ';' +
              (objColunasTabela.operacao2 !== undefined ? objColunasTabela.operacao2 : "") + ';' +
              (objColunasTabela.empresa2 !== undefined ? objColunasTabela.empresa2 : "") + ';' +
              (objColunasTabela.operacao3 !== undefined ? objColunasTabela.operacao3 : "") + ';' +
              (objColunasTabela.empresa3 !== undefined ? objColunasTabela.empresa3 : "") + ';' +
              (objColunasTabela.operacao4 !== undefined ? objColunasTabela.operacao4 : "") + ';' +
              (objColunasTabela.empresa4 !== undefined ? objColunasTabela.empresa4 : "") + ';' +
              (objColunasTabela.operacao5 !== undefined ? objColunasTabela.operacao5 : "") + ';' +
              (objColunasTabela.empresa5 !== undefined ? objColunasTabela.empresa5 : "") + ';' +
              (objColunasTabela.operacao6 !== undefined ? objColunasTabela.operacao6 : "") + ';' +
              (objColunasTabela.empresa6 !== undefined ? objColunasTabela.empresa6 : "") + ';' +
              (objColunasTabela.SessaoRecVoz !== undefined ? objColunasTabela.SessaoRecVoz : "") + ';' +
              (objColunasTabela.Assunto !== undefined ? objColunasTabela.Assunto : "") + ';' +
              (objColunasTabela.DuracaoURA !== null ? objColunasTabela.DuracaoURA : "") + ';' +
              (objColunasTabela.DATA_HORA_FIM !== null ? objColunasTabela.DATA_HORA_FIM : "") + ';' +
              (objColunasTabela.BDAberto !== undefined ? objColunasTabela.BDAberto : "") + ';' +
              (objColunasTabela.OSAberta !== undefined ? objColunasTabela.OSAberta : "") + ';' +
              (objColunasTabela.URAResgateAnatel !== undefined ? objColunasTabela.URAResgateAnatel : "") + ';' +
              (objColunasTabela.URAProduto !== undefined ? objColunasTabela.URAProduto : "") + ';' +
              (objColunasTabela.URAOpcao !== undefined ? objColunasTabela.URAOpcao : "") + ';' + 
              //Novas colunas abaixo - Paulo Raoni - última edição 26/03/2019
              (objColunasTabela.COD_SKILL_PASSO2 !== null ? objColunasTabela.COD_SKILL_PASSO2 : "") + ';'  +
              (objColunasTabela.TAG_RECVOZ !== undefined ? objColunasTabela.TAG_RECVOZ : "") + ';' +
              (objColunasTabela.URA_BDABERTODENTRODOPRAZO !== undefined ? objColunasTabela.URA_BDABERTODENTRODOPRAZO : "") + ';' +
              (objColunasTabela.URA_BDABERTOFORADOPRAZO !== undefined ? objColunasTabela.URA_BDABERTOFORADOPRAZO : "") + ';' +
              (objColunasTabela.URA_BDVELOXABERTODENTRODOPRAZO !== undefined ? objColunasTabela.URA_BDVELOXABERTODENTRODOPRAZO : "") + ';' +
              (objColunasTabela.URA_BDVELOXABERTOFORADOPRAZO !== undefined ? objColunasTabela.URA_BDVELOXABERTOFORADOPRAZO : "") + ';' +
              (objColunasTabela.URA_BERCARIO !== undefined ? objColunasTabela.URA_BERCARIO : "") + ';' +
              (objColunasTabela.URA_BLACKLISTCREP !== undefined ? objColunasTabela.URA_BLACKLISTCREP : "") + ';' +
              (objColunasTabela.URA_BLOQUEIO_FIXA !== undefined ? objColunasTabela.URA_BLOQUEIO_FIXA : "") + ';' +
              (objColunasTabela.URA_CLIENTE_FIBRA !== undefined ? objColunasTabela.URA_CLIENTE_FIBRA : "") + ';' +
              (objColunasTabela.URA_HORARIOCREP !== undefined ? objColunasTabela.URA_HORARIOCREP : "") + ';' +
              (objColunasTabela.URA_MAILING_R2 !== undefined ? objColunasTabela.URA_MAILING_R2 : "") + ';' +
              (objColunasTabela.URA_OS !== undefined ? objColunasTabela.URA_OS : "") + ';' +
              (objColunasTabela.URA_PILOTOCREP !== undefined ? objColunasTabela.URA_PILOTOCREP : "") + ';' +
              (objColunasTabela.URA_QUARENTENACREP !== undefined ? objColunasTabela.URA_QUARENTENACREP : "") + ';' +
              (objColunasTabela.URA_REPETIDACREP !== undefined ? objColunasTabela.URA_REPETIDACREP : "") + ';' +
              (objColunasTabela.URA_RESGATEANATELST !== undefined ? objColunasTabela.URA_RESGATEANATELST : "") + ';' +
              (objColunasTabela.URA_TIPOBLOQUEIO !== undefined ? objColunasTabela.URA_TIPOBLOQUEIO : "") + ';' +
              (objColunasTabela.VIP_FLAG !== undefined ? objColunasTabela.VIP_FLAG : "") + ';'+ 
              (objColunasTabela.URA_BAIRRO !== undefined ? objColunasTabela.URA_BAIRRO : "") + ';'+
              (objColunasTabela.URA_CIDADE !== undefined ? objColunasTabela.URA_CIDADE : "") + ';'+'\r\n';  

              //COD_SKILL_PASSO2            
      }
  
  function retornaQuery(indice){	
    var obj = Object.keys(datasComDados);
    var stmtMod = stmt;
    var ini = stmtMod.match(/>= '(.*?)'/)[1];
    var fim = stmtMod.match(/<= '(.*?)'/)[1];
    if(indice > 0)stmtMod = stmtMod.replace(ini,datasComDados[obj[indice]].split('|')[0]);
    if(indice < obj.length-1) stmtMod = stmtMod.replace(fim,datasComDados[obj[indice]].split('|')[1]);
    return stmtMod;
  }


      var controle = 0;
      var total = 0;
  

      function proximaHora(stmt) {

          function comOuSemDados(err, c, txt) {
              $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, Object.keys(datasComDados).length) + " Encontrados: " + total + " "+txt).selectpicker('refresh');
              controle++;
              $scope.$apply();

              //Executa dbquery enquanto array de querys menor que length-1
              if (controle < Object.keys(datasComDados).length) {
                  proximaHora(retornaQuery(controle));
                  if (c === undefined) console.log(controle);
              }

              //Evento fim
              if (controle === Object.keys(datasComDados).length) {
                  if (c === undefined) console.log("fim " + controle +" "+new Date());
                  if (err) {
                      retornaStatusQuery(undefined, $scope);
                  } else {
                      retornaStatusQuery($scope.dados.length, $scope);
                  }

                  $btn_gerar.button('reset');
                  if ($scope.dados.length > 0) {
                      fs.appendFileSync(tempDir3() + 'dados_ctg.csv', tempData,'ascii');
          tempData = "";
                      $btn_exportar.prop("disabled", false);
                      $btn_exportar_csv.prop("disabled", false);
                      $btn_exportar_dropdown.prop("disabled", false);
                  } else {
                      $btn_exportar.prop("disabled", true);
                      $btn_exportar_csv.prop("disabled", true);
                      $btn_exportar_dropdown.prop("disabled", true);
                  }
              }
          }
              db.query(stmt,($scope.dados.length < limite ? executaQuery1 : executaQuery2), function(err, num_rows) {
                  console.log("Executando query-> " + stmt + " " + num_rows);
                  num_rows !== undefined ? total += num_rows : total += 0;
                  comOuSemDados(err,undefined,($scope.dados.length < limite ? "" : "Inserindo em arquivo..."));

              });
          
      }



      db.query(stmtCountRows, contaLinhas, function(err, num_rows) {
          console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);

          if ($globals.numeroDeRegistros === 0) {
              retornaStatusQuery(0, $scope);
              $btn_gerar.button('reset');
          } else {
              //Disparo inicial
              proximaHora(retornaQuery(0));
          }

      });




      //});

      // GILBERTOOOOOO 17/03/2014
      $view.on("mouseup", "tr.resumo", function() {
          var that = $(this);
          $('tr.resumo.marcado').toggleClass('marcado');
          $scope.$apply(function() {
              that.toggleClass('marcado');
          });
      });
  };

  // Exportar planilha XLSX
  $scope.exportaXLSX = function() {

      var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }

      // var colunasFinal = [colunas];

      console.log(colunas);

      var dadosExcel = [colunas].concat($scope.csv);

      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'tChamadasCTGMore_' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);

      // if ($scope.dados.length > 10000) {

      //     novaMacro = formataDataHoraMilis(new Date());

      //     // TEMPLATE
      //     var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo" +
      //         " FROM " + db.prefixo + "ArquivoRelatorios" +
      //         " WHERE NomeRelatorio='tChamadasCTGMore'";
      //     log(stmt);

      //     db.query(stmt, function(columns) {
      //         var dataAtualizacao = columns[0].value,
      //             nomeRelatorio = columns[1].value,
      //             arquivo = columns[2].value;

      //         var milis = new Date();
      //         var baseFile = 'tChamadasCTGMore_' + novaMacro + '.xlsm';
      //         var buffer = toBuffer(toArrayBuffer(arquivo));
      //         fs.writeFileSync(tempDir3() + baseFile, buffer, 'binary');
      //     }, function(err, num_rows) {

      //         childProcess.exec(tempDir3() + 'tChamadasCTGMore_' + novaMacro + '.xlsm');

      //     });
      // } else {
    
  // 	// TEMPLATE
      //     var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo" +
      //         " FROM " + db.prefixo + "ArquivoRelatorios" +
      //         " WHERE NomeRelatorio='tChamadasCTG'";
      //     log(stmt);
      //     db.query(stmt, function(columns) {
      //         var dataAtualizacao = columns[0].value,
      //             nomeRelatorio = columns[1].value,
      //             arquivo = columns[2].value;

      //         var milis = new Date();
      //         var baseFile = 'tChamadasCTG.xlsx';
      //         var buffer = toBuffer(toArrayBuffer(arquivo));
      //         fs.writeFileSync(baseFile, buffer, 'binary');
      //         var file = 'detalhamentoChamadasCTG_' + formataDataHoraMilis(milis) + '.xlsx';

      //         var newData;

      //         fs.readFile(baseFile, function(err, data) {
        
  // 			$scope.dados.map(function (array) {
          
  // 				for (var item in $scope.colunas) {
  // 					if(array[$scope.colunas[item].field] === undefined) array[$scope.colunas[item].field] = "";							
  // 				}	
  // 				if(!exibeNLU){
  // 					if(array.confianca === undefined) array.confianca = "";
  // 					if(array.elocucao === undefined) array.elocucao = "";							
  // 					if(array.resultado === undefined) array.resultado = "";
  // 					if(array.reconhecimento === undefined) array.reconhecimento = "";
  // 					if(array.score === undefined) array.score = "";
  // 				}
  // 				return array;
      //             });
                  
      //             if (XLSX) {
      //                 console.log('CHECANDO XLSX NO LIB ------------------');
      //                 console.log(XLSX);
      //                 console.log('CHECANDO XLSX NO LIB ------------------');
      //             } else {
      //                 console.log('CHECANDO XLSX NO LIB ------------------');
      //                 console.log(XLSX);
      //                 console.log('CHECANDO XLSX NO LIB ------------------');
      //             }
        
      //             // Create a template
      //             var t = new XlsxTemplate(data);

      //             // Perform substitution
      //             t.substitute(1, {
      //                 filtros: $scope.filtros_usados,
      //                 planDados: $scope.dados
      //             });

      //             // Get binary data
      //             newData = t.generate();

      //             if (!fs.existsSync(file)) {
      //                 fs.writeFileSync(file, newData, 'binary');
      //                 childProcess.exec(file);
      //             }
      //         });
            
      //     }, function(err, num_rows) {                
      //         //?
      //     });
      // }

      setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);
  };
}

CtrlChamadasCTG2.$inject = ['$scope', '$globals'];
