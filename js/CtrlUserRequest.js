function CtrlUserRequest($scope, $globals) {

	win.title="Solicitação de cadastro"; //Matheus 28/02/2017
	$scope.versao = versao;
	
	$scope.step1 = "active";
	$scope.step2 = "disabled";
	$scope.step3 = "disabled";
  $scope.login = loginUsuario;
  $scope.dominio = dominioUsuario;
  $scope.globalPerms = rotasInit; // Permissões globais extraidas do rotasInit
  $scope.dataGridPerms = [];
	
	//* Tree view responsável pelo menu de navegação

    var $view;
    
    $(".registerTelefone").maskbrphone({  
            useDdd           : true,
            useDddParenthesis: true,
            dddSeparator     : ' ',
            numberSeparator  : '-'
     });
	//Colunas do grid de permissões
	$scope.permissions = [
		{ field: "detalhes", displayName: "Relatórios",  width: '90%', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm row{{row.rowIndex+1}}">{{row.entity.detalhes}}</span></div>'},
		{ field: "checked", displayName: "Habilitado", width: '10%', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm" ><input type="checkbox"  class="PermissionCheckBox" ng-model="forms" ng-click="requestPerms()" id="{{row.entity.form}}"></span></div>' }
	];

	//Grid de permissões
	/*$scope.gridPerms = {
		data: $scope.dataGridPerms,
		columnDefs: $scope.permissions,
		enableColumnResize: false,
		rowHeight: 60
	};*/
  
	//Grid de permissões
	$scope.gridPerms = {
		data: "dataGridPerms",
		columnDefs: "permissions",
		enableColumnResize: false,
		// enablePinning: false,
		// filterOptions: $scope.permFiltros,
		// showFilter: true,
		rowHeight: 60
	};
    
	function RequestAlert(texto, html){
			
		if(html){
			$(".RequestAlert").html(texto);
			return;
		}		
		
		if(texto!=null&&texto!=undefined){
			texto = 
			$(".RequestAlert").html("<strong>"+texto+"</strong>");	
			return;
		}else{
			$(".RequestAlert").text("");		
			return;
		}
	
	};
  
	// Função vinculada ao botão cancelar, utilizada para limpar dados importados 
	$scope.emptyRequest = function(){
    
		var string = "<strong>Atenção!</strong> seu nome de usuário e domínio não estão registrados. Para utilizar a ferramenta preencha a solicitação de cadastro ou entre em contato com <strong>suporte@versatec.com.br</strong>. ";
		RequestAlert(string, true);
		
		$scope.aplicacoes = [];
    $(".filtro-aplicacao").val("");
    $(".filtro-aplicacao").selectpicker('refresh');
    //$('body').resize();
		$scope.step1 = "active";
		$scope.step2 = "disabled";
		$scope.step3 = "disabled";
	};
  
  $scope.startRequest = function(){
    $scope.step1 = "completed";
    $scope.step2 = "active";
		var string = "<strong>Atenção </strong> preencha os dados do formulário para confirmar a sua solicitação de cadastro, estes dados serão verificados.";//<strong>Atenção!</strong> seu nome de usuário e domínio não estão registrados. Para utilizar a ferramenta preencha a solicitação de cadastro ou entre em contato com <strong>suporte@versatec.com.br</strong>. ";
		RequestAlert(string, true);
    console.log("RequestAlert: String: "+string);
  }
	// Fim da função vinculada ao botão cancelar, utilizada para limpar dados importados 
  
  $scope.changeRequest = function(){
    $scope.step1 = "completed";
    $scope.step2 = "completed";
    $scope.step3 = "active";
    var string = "Marque os relatórios que deseja utilizar na ferramenta Estalo, as alterações são salvas no clique.";
    RequestAlert(string, true); 
    $("body").resize();
  }
  
  $scope.confirmRequest = function(){
    $scope.step1 = "completed";
    $scope.step2 = "completed";
    $scope.step3 = "completed";
    $("body").resize();
    RequestAlert("<strong>Aguarde!</strong> o seu cadastro está na lista de espera, você receberá uma notificação por email quando seu cadastro for verificado."
                      +"<br/> Qualquer dúvida ou solicitação, entre em contato com suporte@versatec.com.br.", true);
  }
  
  $scope.saveRequest = function (){
      console.log("SaveRequest!")
			var sLogin = $scope.login;
			var sNome = $scope.Nome;
			var sDominio = $scope.dominio;
			var sEmail = $scope.Email;
			var sEmpresa = $scope.Empresa;
			var sTelefone = $scope.Telefone;

		function validForm(){

			console.log("Login registrado: "+sLogin+" Email: "+sEmail);
        if(
          ((sLogin==""||sLogin==undefined)||
            (sNome==""||sNome==undefined)||
              (sDominio==""||sDominio==undefined)||
                (sEmail==""||sEmail==undefined)||
                  (sEmpresa==""||sEmpresa==undefined))
          ){
          var resultstring = "Favor preencha ";
          
          if(sLogin==""||sLogin==undefined){
            resultstring = resultstring+"login, ";
            sLogin="";
          }
          if(sNome==""||sNome==undefined){
            resultstring = resultstring+"nome, ";
            sNome="";
          }
          if(sDominio==""||sDominio==undefined){
            resultstring = resultstring+"domínio, ";
            sDominio="";
          }
          if(sEmail==""||sEmail==undefined){
            resultstring = resultstring+"email, ";
            sEmail="";
          }
          if(sEmpresa==""||sEmpresa==undefined){
            resultstring = resultstring+"empresa, ";
            sEmpresa="";
          }
          /*if($scope.Cpf==""||$scope.Cpf==undefined){
            $scope.Cpf="";
          }*/

          resultstring = resultstring+" e tente novamente.";
          console.log("Error: "+resultstring);
          RequestAlert(resultstring);
          return false;
        }
        if(!$scope.UserController.$valid){
          console.log("Form USERCONTROLLED Not VALID")
          requestAlert("Formulário Inválido. Verifique os campos!");
          return false;
        }
        
        return true;
    }

		if(validForm()){
				//checkLoginToRegister(sLogin, sDominio, sEmpresa, sNome, sEmail, sTelefone, $scope)
        solicitaUser();
      $('body').resize();
      
      console.log("Usuário solicitando permissões: "+$scope.login+" de origem do domínio: "+$scope.dominio);
      console.log("Tamanho da lista de Perm: "+$scope.dataGridPerms.length);
			
    }else{
      console.log("ValidForm IS NOT VALID!");
    }
    
	}
  
  function solicitaUser(){
    
     
			var Login = $scope.login;
			var Nome = $scope.Nome;
			var Dominio = $scope.dominio;
			var Email = $scope.Email;
			var Empresa = $scope.Empresa;
			var Telefone = $scope.Telefone;
      
      Login = Login.replace(/(?:\r\n|\r|\n)/g, '');
      Login = Login.replace(/[']/g, "''") 
      Login = Login.replace(/["]/g, "''") 
      Empresa = Empresa.replace(/(?:\r\n|\r|\n)/g, '');
      Empresa = Empresa.replace(/[']/g, "''") 
      Empresa = Empresa.replace(/["]/g, "''") 
      Nome = Nome.replace(/(?:\r\n|\r|\n)/g, '');
      Nome = Nome.replace(/[']/g, "''") 
      Nome = Nome.replace(/["]/g, "''") 
      if(Email==undefined||Email==null){
        Email="";
      }
      
      if(Telefone==undefined||Telefone==null){
        Telefone="";
      }
			
			stmt = "Select LoginUsuario from SolicitacaoUsuarios where LoginUsuario = '"+Login+"' and Dominio = '"+Dominio+"'";				
			log(stmt);
			db.query(stmt, function () { }, function (err, num_rows) {
			    userLog(stmt, 'Carrega dados', 2, err)
					if(err){
						console.log("Erro na sintaxe: "+err);
					}
					
					if(num_rows>=1){
						console.log("Existe no Banco "+Login+" Rows: "+num_rows);
						console.log("Usuário "+Login+" com domínio "+Dominio+" já solicitado.");
					}
					
					if(num_rows==0){
						console.log("Não existe no banco solicitacao para "+Login);
						  var stmt2 = db.use
						  + "INSERT " + db.prefixo + "solicitacaoUsuarios(LoginUsuario,Dominio,Empresa,Nome,EmailUsuario,telefoneUsuario)"
						  +" VALUES "
						  +"('"+Login+"','"+Dominio+"','"+Empresa+"',"
						  +"'"+Nome+"','"+Email+"','"+Telefone+"')"

		
				// Cadastra o Usuário
           db.query(stmt2, function () { }, function (err, num_rows) {
               //userLog(stmt2, 'Cadastra Usuario', 1, err)
						console.log("Executando query-> "+stmt);
							if(err){
									console.log("Erro ao registrar: "+err);
                  
                /*$scope.step1 = "completed";
                $scope.step2 = "active";
                $scope.step3 = "disabled";*/
								RequestAlert("Falha ao registrar usuário, erro ao cadastrar no banco de dados. "+err);
                  
							}else{
								
								
                $scope.step1 = "completed";
                $scope.step2 = "completed";
                $scope.step3 = "active";
				$scope.$apply();
       
              }
					});
				// Fim do cadastra usuário
					}
					
			});
		}
  
  	// Verifica as permissões do usuário a ser editado (Chamado pelo editUser)
	$scope.listaPerms = function(){
			var perms = $scope.globalPerms;

				function checkNomeByForm(perms, form){
					var nomes="";
					//console.log("perms: "+perms+" form: "+form);
					for(x=0;x<perms.length;x++){
						//console.log("s "+x+": "+perms[x].nome+" para "+form);
						if(perms[x].form == form){
							if(nomes==""){
								nomes = perms[x].nome;
							}else{
								nomes = perms[x].nome+", "+nomes;
							}
						}else{
							//console.log(perms[x].nome+" FORM +"+perms[x].form);
						}
					}

					if(nomes){
						return nomes;
					}else{
						return false;
					}
				}

				var formNames = []; // ex: frmChamUraVoz
				var nomes = []; // ex: Detalhamento de chamadas

				for(i=0; i<perms.length;i++){
					if(formNames.indexOf(perms[i].form)==-1){
						nomes.push(checkNomeByForm(perms, perms[i].form));
						formNames.push(perms[i].form);
					}else{
						//console.log(perms[i].form+" na posição "+formNames.indexOf(perms[i].form)+" I: "+i);
					}
				}
				var arrayDeObjetos = [];

				//console.log("Nomes: "+nomes.length+" Perms: "+formNames.length);

				for(i=0; i<nomes.length;i++){
						var objeto = {detalhes: '', form: ''};
						objeto.detalhes = nomes[i];
						objeto.form = formNames[i];
						$scope.dataGridPerms.push(objeto);
				}
					console.log("Array De Objetos Size: "+arrayDeObjetos.length);
          reqPermissions();
				/*for(i=0;i<arrayDeObjetos.length;i++){
					console.log(i+" detalhe: "+arrayDeObjetos[i].detalhes+" form: "+arrayDeObjetos[i].form);
				}*/

				//$scope.dataGridPerms = arrayDeObjetos;
	};
  
		function reqPermissions(){
       console.log("Searching permissions.");
			$(".PermissionCheckBox").prop('checked', false);

			// Desmarca as CheckBox da classe PermissionCheckBox

			$scope.userPerms = []; // Limpou array responsável por guardar as permissões do usuário em edição
			function permInputCheck(columns){
					var perm = columns[0].value;
           // if(perm!=null&&perm!=undefined&&perm!=""){}
            console.log("Permissoes encontradas:  "+perm+" DENTRO DE PERM INPUT CHECK!!!!");
           //perm = JSON.parse("["+perm+"]");
            if(perm!=null&&perm!=""&&perm!=undefined){
            perm = perm.split(",");
           
           //perm = perm.split(",");
           console.log("Perm APOS SPLIT: "+perm+" Length: "+perm.length+" String: "+perm.toString());
            //console.log("Perm checada: #"+perm);
            i=0;
            while(i<=perm.length){
              var permElem = $('#'+perm[i]);
              if(permElem.length){
                permElem.prop('checked', true);
                console.log("PermElement CHECKED:  "+permElem+" Perm I: "+perm[i]);
              }
              i++;              
            }
              $scope.userPerms = perm; // Adiciona a permissão marcada ao UserPerms
           }
              $scope.confirmRequest();
          }
			    
      
			stmt = "select permissoes from solicitacaoUsuarios where loginUsuario = '"+$scope.login+"' and dominio = '"+$scope.dominio+"';";
      //log(stmt);
      db.query(stmt, permInputCheck, function (err, num_rows) {
          //userLog(stmt, 'Analisa permissoes', 2, err)
				if(err){
					console.log("Erro na consulta "+stmt+" \n Erro: "+err);
				}else{
					console.log("Permission Selecionadas: "+$scope.userPerms.toString());
				}
			});

		}

   
   $scope.requestPerms = function(){
		var userPerms = $scope.userPerms; // Abriu com essas permissões
		var savedPerms = []; // Usuário está com essas permissões em execução
		var nowPerms = [];
		var deletePerms = [];
		var insertPerms = [];


		//var deleteArray = [];
		var Login = $scope.login.trim();
		var Dominio = $scope.dominio.trim();

      if(Login==undefined||Login==""){
        console.log(" LOG FS");
        return false;
      }

      if(Dominio==undefined||Dominio==""){
        console.log(" DOM FS");
        return false;
      }
    
      function updatePermissions(){
      var permChecks = $("#pag-registerRequest .PermissionCheckBox:checked");
      var i=0;
      console.log("Lenght: "+permChecks.length);
      while(i<permChecks.length){
          savedPerms.push(""+permChecks[i].id+"");
          console.log("Perm: "+i+" Perm.length: "+permChecks.length);
          /*if(i==0){
            savedPerms = " ' "+permChecks[i].id+" ' ";
          }else{
            savedPerms = savedPerms+",'"+permChecks[i].id+" ' ";
          }*/
        i++;
      }

      console.log("Saved Perms: "+savedPerms.length);
     /* i=0;
      while(i<savedPerms.length){
        //console.log("Perm "+i+": "+savedPerms[i]);
        i++;
      }*/
        
          console.log("isSqlBusy? Should be!"+$scope.sqlBusy);
           //var insertStmt = db.use + "INSERT " + db.prefixo + " into solicitacaoUsuarios(permissoes) VALUES";
           var updateStmt = db.use + "UPDATE "+db.prefixo+" solicitacaoUsuarios set permissoes = '";
           var stmtPerms = savedPerms;//.replace("'", "''");
            
          var where =  "where LoginUsuario = '"+Login+"' AND Dominio = '"+Dominio+"'";           
          //var insertStmt = insertStmt + "("+stmtPerms+")"+where;
          var updateStmt = updateStmt + stmtPerms+"' "+where;
          console.log("UpdateSTMT: "+updateStmt);
          // console.log("insertStmt");
          
        if(Login==""||Login==undefined||Dominio==""||Dominio==undefined){
          insertStmt="",updateStmt="";
        }
          //console.log("Saved Perms: ""+savedPerms);
          //var stmtPerms = savedPerms.toString();

          // Permissões totais antes da remoção e da inserção
            //nowPerms = unique(nowPerms.concat(userPerms)); //Total de Valores presentes e valores a serem deletados
            $(".PermissionCheckBox").prop("disabled", true);
             //log(updateStmt);
             db.query(updateStmt, function () { }, function (err, num_rows) {
                 //userLog(stmt, 'Atualisa permissoes', 3, err)
                if(err){
                  console.log("Erro na sintaxe: "+err);
                  RequestAlert("Falha ao solicitar permissões de "+Login);
                  console.log("Permissao não pode ser realizado: "+updateStmt);
                  $scope.sqlBusy = false;
                  //$(".PermissionCheckBox").prop("disabled", false);
                  $('#pag-registerRequest .loadingButton').hide();
                  //$("#pag-registerRequest #modal-view").modal('hide');

                }else{
                  $scope.sqlBusy = false;
                  $(".PermissionCheckBox").prop("disabled", false);
                  
                }
            });
            //Fim DB.QUERY
      }

		console.log("isSqlBusy? BEFORE FUNCTION "+$scope.sqlBusy);
		if(!$scope.sqlBusy){
			$scope.sqlBusy = true;
			$('#pag-registerRequest .loadingButton').show();
			console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
			updatePermissions(); // Realiza as queries
			//Fim da updatePerms
		}
	}
	
        $scope.listaPerms($scope.globalPerms); //Executa a listagem de permissões baseado em informações do init.json

  
  
    $scope.$on('$viewContentLoaded', function () {
      $view = $("#pag-registerRequest");
      $("#pag-registerRequest .botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});
	});

}
  
  
  
  CtrlUserRequest.$inject = ['$scope','$globals'];

