var CtrlResumoTransferenciasOperacao = geraCtrlResumoTransferenciasOrigem({
  codigo: "operacao",
  titulo: "Operação",
  tituloPlural: "Operações",
  tituloGrid: "Operação 1",
  tabela: "ResumoECHTransferID",
  coluna: "CodOperacao1",
  largura: 200
});

var CtrlResumoTransferenciasTransferID = geraCtrlResumoTransferenciasOrigem({
  codigo: "transferid",
  titulo: "TransferID",
  tituloPlural: "TransferIDs",
  tituloGrid: "TransferID",
  tabela: "ResumoECHTransferID",
  coluna: "TransferID",
  largura: 200
});

var CtrlResumoTransferenciasED = geraCtrlResumoTransferenciasOrigem({
  codigo: "ultimo-ed",
  titulo: "Último ED",
  tituloPlural: "EDs",
  tituloGrid: "ED",
  tabela: "ResumoECHUltimoED",
  coluna: "CodED",
  largura: 200
});

function geraCtrlResumoTransferenciasOrigem(tipoOrigem) {
  var ctrl = function ($scope, $globals) {
    var idView = "pag-resumo-transf-" + (tipoOrigem.codigo === "ultimo-ed" ? "ed" : tipoOrigem.codigo);

    $scope.titulo = "Resumo de transferências por " + tipoOrigem.titulo.substring(0, 1).toLowerCase() + tipoOrigem.titulo.substring(1);
    win.title = $scope.titulo;

    $scope.nomeExportacao = "resumoTransf" + tipoOrigem.codigo; // TODO: kebab-case -> CamelCase

    $scope.versao = versao;
    $scope.gruposRelatorios = gruposRelatorios;

    travaBotaoFiltro(0, $scope, "#" + idView, $scope.titulo);

    $scope.colunas = [];
    $scope.dados = [];

    $scope.gridDados = {
      data: "dados",
      columnDefs: "colunas",
      /*sortInfo: {
        fields: [ 'datahora', 'origem' ],
        directions: [ 'asc', 'asc' ]
      },*/
      enableColumnResize: true,
      enablePinning: true
    };

    // Filtros
    $scope.filtros = {
      aplicacao: [],
	  site: [],
      segmento: [],
      operacao: [],
      transferid: [],
      ultimoed: [],
      porDia: false,
      porOper2: false
    };

    // TODO: RxJS
    $scope.listarPorData = $scope.filtros.porDia;
    $scope.listarPorOrigem = $scope.filtros[tipoOrigem.codigo.replace("-", "")].length === 0;
    //$scope.listarPorDestino = !$scope.listarPorOrigem;
    $scope.listarPorDestino = $scope.filtros.porOper2;

    $scope.colunas1 = [
      { nome: "datahora", titulo: "Data", largura: 100, exibir: function(){}/*, sort: { priority: 0 }*/ },
      { nome: "datahoraBR", titulo: "Data", largura: 100, exibir: function () { return $scope.listarPorData; }, fixar: true },
      { nome: "origem", titulo: tipoOrigem.tituloGrid, largura: tipoOrigem.largura, exibir: function () { return $scope.listarPorOrigem; }, fixar: true/*, sort: { direction: uiGridConstants.ASC, priority: 1 }*/ },
      { nome: "qtdOper1", titulo: "Qtd Operação 1", largura: 130, numero: true, fixar: true },
      { nome: "qtdOper2", titulo: " Qtd Operação 2", largura: 130, numero: true, fixar: true },
      { nome: "percentTransf", titulo: "% Transferências", largura: 140, numero: true, fixar: true }
    ];

    function geraColunas(colunas) {
      var numberTemplate = '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>';
      return colunas.map(function (col) {
        var o = { field: col.nome, displayName: col.titulo, width: col.largura, pinned: col.fixar !== undefined ? col.fixar : false };
        if (col.numero) {
          o.cellTemplate = numberTemplate;
        }
        if (col.exibir !== undefined && !col.exibir()) {
          o.visible = false;
        }
        return o;
      });
    }

    // Filtros: data e hora
    var inicio = Estalo.filtros.filtro_data_hora[0] || ontemInicio();
    var fim = Estalo.filtros.filtro_data_hora[1] || ontemFim();
    var agora = new Date();
    $scope.periodo = {
      inicio: inicio,
      fim: fim,
      min: new Date(2016, 8, 1),
      max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
      $view = $("#" + idView);

      // Exibir notificação
      $view.find('div.logoCliente').mouseover(function () {
        $view.find('div.notification').fadeIn(500);
      });

      // Ocultar a notificação
      $view.find('button.close').click(function () {
        $view.find('div.notification').fadeOut(500);
      });

      function exibeCalendario(qual) {
        if (qual === 'inicio') {
          $view.find("div.data-inicio").data("datetimepicker").show();
          $view.find("div.data-fim").data("datetimepicker").hide();
        } else if (qual === 'fim') {
          $view.find("div.data-inicio").data("datetimepicker").hide();
          $view.find("div.data-fim").data("datetimepicker").show();
        } else {
          $view.find("div.data-inicio").data("datetimepicker").hide();
          $view.find("div.data-fim").data("datetimepicker").hide();
        }
      }

      $view.find('.menu-relatorios > .dropdown-toggle').dropdownHover().dropdown();

      $view.find('.menu-relatorios > .dropdown-toggle').mouseover(function () {
        exibeCalendario(false);
      });

      $view.find('div.data-inicio span.add-on').mouseover(function () {
        exibeCalendario('inicio');
      });

      $view.find('div.data-fim span.add-on').mouseover(function () {
        exibeCalendario('fim');
      });

      $view.find("div.bootstrap-datetimepicker-widget.dropdown-menu[style^=d]").mouseleave(function () {
        $(this).hide();
      });

      $view.find(".topLine").mouseover(function () {
        exibeCalendario(false);
      });
      $view.find(".bottomLine").mouseover(function () {
        exibeCalendario(false);
      });

      $scope.gruposRelatorios.forEach(function (grupo) {
       // treeView("#" + idView + " ." + grupo.codigo, grupo.pos, grupo.titulo, grupo.div, "#" + idView + " .menu-relatorios");
      });

      componenteDataHora($scope, $view);
      carregaAplicacoes($view, false, false, $scope);
	  carregaSites($view);
      carregaSegmentosPorAplicacao($scope, $view, true);

      // Popula lista de segmentos a partir das aplicações selecionadas
      $view.on("change", "select.filtro-aplicacao", function () {
        var valores = $(this).val() || [];
        $scope.filtros.aplicacao = valores;
        carregaSegmentosPorAplicacao($scope, $view);
      });

      $view.find("select.filtro-aplicacao").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} aplicações',
        showSubtext: true
      });
	  
	  $view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});

      $view.on("change", "select.filtro-segmento", function () {
        var valores = $(this).val() || [];
        $scope.filtros.segmento = valores;
        Estalo.filtros.filtro_segmentos = valores;
      });

      // Marcar todas aplicações
      $view.on("click", ".alinkApl", function () { marcaTodasAplicacoes($view.find('.filtro-aplicacao'), $view, $scope); });

      // Marcar todos os segmentos
      $view.on("click", ".alinkSeg", function () { marcaTodosIndependente($view.find('.filtro-segmento'), 'segmentos'); });
	  
	  // Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

      $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
        exibeCalendario(false);
        $(this).addClass('open');
        $(this).find('>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
      });

      $view.find('div.btn-group.filtro-aplicacao').mouseout(function () {
        $(this).removeClass('open');
      });

      /*$view.find('div.btn-group.filtro-segmento:not-contains[.btn.disabled]').mouseover(function () {
        $(this).addClass('open');
        // div.btn-group.filtro-segmento.open > div > ul { max-height: 500px; overflow-y: auto; min-height: 1px; max-width: 350px; }
      });*/

      $view.find('div.btn-group.filtro-segmento').mouseout(function () {
        $(this).removeClass('open');
      });

      $view.on("click", ".btn-exportar", function () {
        $scope.exportaXLSX.apply(this);
      });

      // Limpa filtros
      $view.on("click", ".btn-limpar-filtros", function () {
        $scope.porDia = false;
        $scope.limpaFiltros();
      });

      $scope.limpaFiltros = function () {
        iniciaFiltros();
        componenteDataHora($scope, $view, true);

        var $btn_limpar = $view.find(".btn-limpar-filtros");
        $btn_limpar.prop("disabled", true);
        $btn_limpar.button('loading');

        setTimeout(function () {
          $btn_limpar.button('reset');
        }, 500);
      }

      // Botão agora
      $view.on("click", ".btn-agora", function () {
        $scope.agora();
      });

      $scope.agora = function () {
        iniciaAgora($view, $scope);
      }

      // Botão abortar
      $view.on("click", ".abortar", function () {
        $scope.abortar();
      });

      $scope.abortar = function () {
        abortar($scope, "#" + idView);
      }

      // Lista chamadas conforme filtros
      $view.on("click", ".btn-gerar", function () {
        limpaProgressBar($scope, "#" + idView);

        if (!validaFiltros()) {
          return;
        }

        $scope.listaDados.apply(this);
      });

      $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    function validaFiltros() {
      // Testa se data início é maior que a data fim
      var data_ini = $scope.periodo.inicio,
          data_fim = $scope.periodo.fim;
      var testedata = testeDataMaisHora(data_ini,data_fim);
      if (testedata !== "") {
        setTimeout(function () {
          atualizaInfo($scope, testedata);
          effectNotification();
          $view.find(".btn-gerar").button('reset');
        }, 500);
        return false;
      }
      return true;
    }

    // Lista dados conforme filtros
    $scope.listaDados = function () {
      var $btn_exportar = $view.find(".btn-exportar");
      $btn_exportar.prop("disabled", true);

      var $btn_gerar = $(this);
      $btn_gerar.button('loading');

      var data_ini = $scope.periodo.inicio,
          data_fim = $scope.periodo.fim;

      var filtro_aplicacao = $view.find("select.filtro-aplicacao").val() || [];
	  var filtro_sites = $view.find("select.filtro-site").val() || [];
      var filtro_segmento = $view.find("select.filtro-segmento").val() || [];
      var filtro_empresa = $view.find("select.filtro-empresa").val() || [];
      var filtro_origem = $view.find("select.filtro-" + tipoOrigem.codigo).val() || [];

      // Notificação de filtros usados
      $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + " até " + formataDataHoraBR($scope.periodo.fim)
        + (filtro_aplicacao.length > 0 ? " Aplicações: " + filtro_aplicacao : "")
		+ (filtro_sites.length > 0 ? " Sites: " + filtro_sites : "") 
        + (filtro_segmento.length > 0 ? " Segmentos: " + filtro_segmento : "")
        + (filtro_origem.length > 0 ? " " + tipoOrigem.tituloPlural + ": " + filtro_origem : "");

      $scope.dados = [];
      $scope.pivot = [];
      $scope.pivot._colunas = {};

      // TODO: RxJS
      $scope.listarPorData = $scope.filtros.porDia;
      $scope.listarPorOrigem = $scope.filtros[tipoOrigem.codigo.replace("-", "")].length === 0;
      //$scope.listarPorDestino = !$scope.listarPorOrigem;
      $scope.listarPorDestino = $scope.filtros.porOper2;

      $scope.colunas = geraColunas($scope.colunas1);

      $scope.totais = {};
      $scope.colunas1.filter(function (col) { return col.nome.match(/^qtd/); }).forEach(function (col) {
        $scope.totais[col.nome] = 0;
      });

      function _join(values, sep) {
        var list = [];
        for (var i = 0; i < 3; i++) {
          if (values[i]) {
            list.push(values[i]);
          }
        }
        return list.join(sep);
      }

      var tabela = tipoOrigem.tabela + "Dia";
      var colunaOrigem = tipoOrigem.coluna;
      //var colunaDataHora = $scope.listarPorData ? "CONVERT(varchar, DatReferencia, 3)" : "LEFT(CONVERT(varchar, DatReferencia, 120), 16)";
      var chave = _join([
        $scope.listarPorData ? "LEFT(CONVERT(varchar,DatReferencia,120),10) AS DatReferencia, CONVERT(varchar,DatReferencia,3) AS DatReferenciaBR" : "",
        $scope.listarPorOrigem ? colunaOrigem : "",
        $scope.listarPorDestino ? "CodOperacao2" : ""
      ], ", ");
      var ordem = _join([
        $scope.listarPorData ? "DatReferencia" : "",
        $scope.listarPorOrigem ? colunaOrigem : ""
      ], ", ");

      if ($scope.listarPorDestino) {
        var stmt = db.use
          + " WITH cte AS ("
          + "   SELECT" // TODO: dummy
          + "     " + ($scope.listarPorData ? "DatReferencia" : "''") + " AS DatReferencia,"
          + "     " + ($scope.listarPorOrigem ? colunaOrigem : "''") + " AS " + colunaOrigem + ","
          + "     CodOperacao2,"
          + "     SUM(QtdTransferidas) AS QtdTransferidas"
          + "   FROM " + tabela
          + "   WHERE DatReferencia >= '" + formataDataHora(data_ini) + "'"
          + "     AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
          + "     AND CodAplicacao <> ''"
          + "     AND CodOperacao2 <> ''"
          + restringe_consulta("CodAplicacao", filtro_aplicacao, true)
		  + restringe_consulta("CodSite", filtro_sites, true)
          + restringe_consulta("CodSegmento", filtro_segmento, true)
          + restringe_consulta("CodEmpresa", filtro_empresa, true)
          + restringe_consulta(colunaOrigem, filtro_origem, true)
          + "   GROUP BY " + ordem + ", CodOperacao2" // TODO: dummy
          + " ), cte2 AS ("
          + "   SELECT" // TODO: dummy
          + "     " + ($scope.listarPorData ? "DatReferencia" : "''") + " AS DatReferencia,"
          + "     " + ($scope.listarPorOrigem ? colunaOrigem : "''") + " AS " + colunaOrigem + ","
          + "     SUM(QtdDerivadas) AS QtdDerivadas"
          + "   FROM " + tabela
          + "   WHERE DatReferencia >= '" + formataDataHora(data_ini) + "'"
          + "     AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
          + "     AND CodAplicacao <> ''"
          + restringe_consulta("CodAplicacao", filtro_aplicacao, true)
		  + restringe_consulta("CodSite", filtro_sites, true)
          + restringe_consulta("CodSegmento", filtro_segmento, true)
          + restringe_consulta("CodEmpresa", filtro_empresa, true)
          + restringe_consulta(colunaOrigem, filtro_origem, true)
          + "   GROUP BY " + ordem // TODO: dummy
          + " )"
          + " SELECT " + chave.replace(",DatReferencia", ",ISNULL(cte.DatReferencia,cte2.DatReferencia)").replace(",DatReferencia", ",ISNULL(cte.DatReferencia,cte2.DatReferencia)").replace(colunaOrigem, "ISNULL(cte." + colunaOrigem + ",cte2." + colunaOrigem + ") AS " + colunaOrigem) + ","
          + "   QtdDerivadas, QtdTransferidas"
          + " FROM cte"
          + " LEFT OUTER JOIN cte2 ON"
          + "     " + _join([$scope.listarPorOrigem ? "cte." + colunaOrigem + " = cte2." + colunaOrigem : "", $scope.listarPorData ? "cte.DatReferencia = cte2.DatReferencia" : ""], " AND ")
          + " UNION ALL"
          + " SELECT " + chave.replace(",DatReferencia", ",ISNULL(cte.DatReferencia,cte2.DatReferencia)").replace(",DatReferencia", ",ISNULL(cte.DatReferencia,cte2.DatReferencia)").replace(colunaOrigem, "ISNULL(cte." + colunaOrigem + ",cte2." + colunaOrigem + ") AS " + colunaOrigem).replace("CodOperacao2", "'' AS CodOperacao2") + ","
          + "   QtdDerivadas, 0 AS QtdTransferidas"
          + " FROM cte2"
          + " LEFT OUTER JOIN cte ON"
          + "     " + _join([$scope.listarPorOrigem ? "cte." + colunaOrigem + " = cte2." + colunaOrigem : "", $scope.listarPorData ? "cte.DatReferencia = cte2.DatReferencia" : ""], " AND ")
          + " WHERE cte." + colunaOrigem + " IS NULL" // TODO: dummy
          + "   AND cte2.QtdDerivadas > 0"
          + " ORDER BY " + ordem;
      } else {
        var stmt = db.use
          + " SELECT " + chave + ","
          + "   SUM(QtdDerivadas) AS QtdDerivadas, SUM(QtdTransferidas) AS QtdTransferidas"
          + " FROM " + tabela
          + " WHERE DatReferencia >= '" + formataDataHora(data_ini) + "'"
          + "   AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
          + "   AND CodAplicacao <> ''"
          + "   AND QtdDerivadas > 0"
          + restringe_consulta("CodAplicacao", filtro_aplicacao, true)
		  + restringe_consulta("CodSite", filtro_sites, true)
          + restringe_consulta("CodSegmento", filtro_segmento, true)
          + restringe_consulta("CodEmpresa", filtro_empresa, true)
          + restringe_consulta(colunaOrigem, filtro_origem, true)
          + " GROUP BY " + ordem
          + " ORDER BY " + ordem;
      }
      log(stmt);

      function processaRegistro(columns) {
        var i = 0;
        var datahora, datahoraBR, origem, operacao2;
        if ($scope.listarPorData) { datahora = columns[i].value; i += 1; datahoraBR = columns[i].value; i += 1; }
        if ($scope.listarPorOrigem) { origem = columns[i].value; i += 1; }
        if ($scope.listarPorDestino) { operacao2 = columns[i].value; i += 1; }
        var qtdOper1 = +columns[i].value; i += 1;
        var qtdOper2 = +columns[i].value; i += 1;

        if (tipoOrigem.codigo === "ultimo-ed" && origem) {
          for (var codAplicacao in cache.aplicacoes) {
            var nomeED = obtemNomeEstado(codAplicacao, origem);
            if (nomeED !== origem) {
              origem = nomeED;
            }
          }
        }

        var d = {
          datahora: datahora || "",
          datahoraBR: datahoraBR || "",
          origem: origem || "",
          operacao2: operacao2 || "",
          qtdOper1: qtdOper1,
          qtdOper2: qtdOper2,
          percentTransf: (100.0 * qtdOper2 / qtdOper1).toFixed(2)
        };

        if (!$scope.listarPorDestino) {
          $scope.dados.push(d);
        }

        $scope.colunas1.filter(function (col) { return col.nome.match(/^qtd/); }).forEach(function (col) {
          $scope.totais[col.nome] += d[col.nome];
        });

        if ($scope.listarPorDestino && d.operacao2 !== "") {
          var id = d.datahora + "/" + d.origem;
          var _d = $scope.pivot[id];
          if (_d === undefined) {
            _d = { datahora: d.datahora, datahoraBR: d.datahoraBR, origem: d.origem, qtdOper1: d.qtdOper1, qtdOper2: 0 };
            $scope.pivot.push(_d);
            $scope.pivot[id] = _d;
          }
          _d.qtdOper2 += d.qtdOper2;
          /*_d.qtdOper2_[d.operacao2] = d.qtdOper2;*/
          _d["qtdOper2_" + d.operacao2] = d.qtdOper2;
          $scope.pivot._colunas[d.operacao2] = true;
          //$scope.pivot._totais.qtdOper2_[d.operacao2] = ($scope.pivot._totais.qtdOper2_[d.operacao2] || 0) + d.qtdOper2;
        }
      }

      db.query(stmt, processaRegistro, function (err, numRows) {
          log(numRows + " registros recebidos");
          userLog(stmt, 'Processa registro', 2, err)

        if ($scope.listarPorDestino) {
          $scope.pivot.forEach(function (d) {
            Object.keys($scope.pivot._colunas).forEach(function (s) {
              d["qtdOper2_" + s] = d["qtdOper2_" + s] || 0;
            });
            d.percentTransf = (100.0 * d.qtdOper2 / d.qtdOper1).toFixed(2);
          });
          $scope.dados = $scope.pivot;

          var extra = Object.keys($scope.pivot._colunas).sort();
          $scope.colunas = geraColunas($scope.colunas1.concat(extra.map(function (s) {
            return { nome: "qtdOper2_" + s, titulo: s, largura: 70, numero: true };
          })));
        }

        $view.find('table').css({ 'margin-left': 'auto', 'margin-right': 'auto', 'margin-top': '100px', 'margin-bottom': '100px', 'max-width': '1280px' });

        retornaStatusQuery(numRows, $scope, "");
        $btn_gerar.button('reset');
        if (numRows > 0) {
          $btn_exportar.prop("disabled", false);
        }

        $scope.$apply();
      });
    };

    // Exporta planilha XLSX
    $scope.exportaXLSX = function () {
      var $btn_exportar = $(this);

      $btn_exportar.button('loading');

      var linhas  = [];
      linhas.push($scope.colunas.filterMap(function (col) { if (col.visible || col.visible === undefined) { return col.displayName; } }));
      $scope.dados.forEach(function (dado) {
        linhas.push($scope.colunas.filterMap(function (col) { if (col.visible || col.visible === undefined) { return dado[col.field]; } }));
      });

      exportaXLSX($scope.titulo, linhas, $scope.nomeExportacao, $scope.username);
      
      setTimeout(function () {
        $btn_exportar.button('reset');
      }, 500);
    };
  };
  ctrl.$inject = ['$scope', '$globals'];
  return ctrl;
}
