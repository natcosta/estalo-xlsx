/*
 * CtrlTransferID
 */
var mudarTID = false;

function CtrlTransferID($scope, $globals) {

    win.title = "Resumo por TransferID";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //var teste_data = dataUltimaConsolidacao("ResumoTID");
    travaBotaoFiltro(0, $scope, "#pag-transferID", "Resumo por TransferID");

    $scope.status_progress_bar = 0;

    $scope.dados = [];
    $scope.csv = [];

    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    $scope.datas = [];
    $scope.totais = {};

    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = 'transferid';
    //$scope.ordenacao = ['transferid', 'data'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        sites: [],
        segmentos: [],
        porDdd: false,
        porIit: false,
        porTlv: false,
        porDia: false
    };
    $scope.tlv = false;
    // Filtro: ddd
    $scope.ddds = cache.ddds;

    $scope.aba = 2;

    function geraColunas(tipoDer) {
        var array = [];

        var tipo = tipoDer == "pontoderivacao" ? "Ponto Derivação" : "Transfer Id";

        array.push({
            field: "transferid",
            displayName: tipo,
            width: 300,
            pinned: true
        });

        if ($scope.filtros.porDdd) {
            array.push({
                field: "ddd",
                displayName: "DDD",
                width: 100,
                pinned: true
            });
        }
        array.push({
            field: "chamadas",
            displayName: "Chamadas",
            width: 100,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "tma",
            displayName: "TMA",
            width: 100,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | currency:""}}</span></div>'
        });


        return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-transferID");

        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '18px'
        });


        //19/03/2014
        componenteDataHora($scope, $view);

        carregaDDDs($view);
        carregaAplicacoes($view, false, false, $scope);
        carregaSites($view);
        carregaTipo($view);
        carregaSegmentosPorAplicacao($scope, $view, true);


        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-segmento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} segmentos'
        });

        $view.find("select.filtro-tipo").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Tipo'
        });

        $("#pag-transferID").on("click", ".filtro-tipo div.dropdown-menu.open ul li", function () {
            if (mudarTID === false) {

                $('select.filtro-tipo').val('').selectpicker('refresh');
            }
            mudarTID = false;
        });


        //2014-11-27 transição de abas
        var abas = [2, 3, 4];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }

        // Marca todos os DDDs
        $view.on("click", "#alinkDDD", function () {
            marcaTodosIndependente($('.filtro-ddd'), 'ddds')
        });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function () {
            marcaTodosIndependente($('.filtro-site'), 'sites')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        //Marcar todas aplicaçãos
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });



        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val();
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-tipo", function () {
            mudarTID = true;
            var filtro_tipo = $(this).val() || [];

        });
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
            if ($('.filtro-aplicacao').val().indexOf("103") > -1 ||
                $('.filtro-aplicacao').val().indexOf("TLV") > -1 || $('.filtro-aplicacao').val() == "OIDEALER") {
                $('select.filtro-tipo').html('').selectpicker('refresh')
                carregaTipo($view);
                $view.find("select.filtro-tipo").prop("disabled", false);
                $('select.filtro-tipo').selectpicker('refresh')


            } else {

                var tipo = retornaTipo($('select.filtro-aplicacao').val());
                var nome;
                if (tipo == "trnid") {
                    nome = "Trn Id"
                    $view.find("select.filtro-tipo").prop("disabled", false);
                    $('select.filtro-tipo').html('').selectpicker('refresh')
                    $('select.filtro-tipo').append('<option value="' + tipo + '">' + nome + '</option>').selectpicker('refresh')
                } else if (tipo == "pontoderivacao") {
                    nome = "Ponto de Derivação"
                    $view.find("select.filtro-tipo").prop("disabled", false);
                    $('select.filtro-tipo').html('').selectpicker('refresh')
                    $('select.filtro-tipo').append('<option value="' + tipo + '">' + nome + '</option>').selectpicker('refresh')

                } else {
                    $('select.filtro-tipo').html('')
                    $view.find("select.filtro-tipo").prop("disabled", true);
                    $('select.filtro-tipo').selectpicker('refresh')
                }



            }

        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-ddd').removeClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            $('div.btn-group.filtro-ddd').addClass('open');
            $('div.btn-group.filtro-ddd>div>ul').css({
                'max-height': '600px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '400px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });

        $('div.btn-group.filtro-tipo').mouseover(function () {
            // Não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-tipo .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-tipo').addClass('open');
                $('div.btn-group.filtro-tipo>div>ul').css({
                    'max-width': '500px',
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE Falhas
        $('div.btn-group.filtro-tipo').mouseout(function () {
            $('div.btn-group.filtro-tipo').removeClass('open');
        });


        // Exibe mais registros
        $scope.exibeMaisRegistros = function () {
            $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
        };

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-transferID");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            //22/03/2014 Testa se uma aplicação foi selecionada
            var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Selecione uma aplicação');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            if ($('input.pordia').prop('checked') && $('input.porddd').prop('checked')) {
                setTimeout(function () {
                    atualizaInfo($scope, 'Opção não disponível (Checkboxes <b>DDD</b> e <b>porDia</b> na mesma consulta)');
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }


            $scope.listaDados.apply(this);

        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-transferID");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');

    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        limpaProgressBar($scope, $globals.numeroDeRegistros, "#pag-transferID");
        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_tipo = $view.find("select.filtro-tipo").val() || "";

        $scope.colunas = geraColunas(filtro_tipo);

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);

        if (filtro_aplicacoes.length > 0 && filtro_aplicacoes[0] != null) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        } else {
            setTimeout(function () {
                atualizaInfo($scope, '<font color = "Red">Selecione uma aplicação</font>');
                effectNotification();
                $view.find(".btn-gerar").button('reset');
                $view.find(".btn-exportar").prop("disabled", false);
            }, 500);
            return;
        }

        if (filtro_tipo == "tipo") {

            setTimeout(function () {
                atualizaInfo($scope, '<font color = "Red">Selecione um tipo de TransferID</font>');
                effectNotification();
                $view.find(".btn-gerar").button('reset');
                $view.find(".btn-exportar").prop("disabled", false);
            }, 500);
            return;
        }

        //if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }

        if ($scope.filtros.porDia) {
            $scope.colunas = $scope.colunas.slice(0, 2);
            $scope.datas = [];
            $scope.totais = {
                geral: {
                    chamadas: 0
                }
            };
        }
        $scope.dados = [];
        var campo = [];
        stmt = "";

        $scope.filtros.porDia ? campo[0] = 'CONVERT(DATE,r.DatReferencia) as datReferencia, ' : campo[0] = '';
        $scope.filtros.porDia ? campo[1] = 'CONVERT(DATE,r.DatReferencia), ' : campo[1] = '';
        $scope.filtros.porDia ? $scope.porDia = true : $scope.porDia = false;


        var tabelas = tabelasParticionadas($scope, 'ResumoTransferID', false);
        var datfim = tabelas.nomes.length > 1 ? "< '" + tabelas.data + " 23:59:59'" : "<= '" + formataDataHora(data_fim) + "'";

        //colocar aqui o if para apontar qual tabela deverá ler
        if (formataDataHora(data_ini) >= '2016-12-23' && formataDataHora(data_fim) >= '2016-12-23') {
            // tabela nova
            table = filtro_tipo == "trnid" ? "ResumoTrnID" : "ResumoPontoDerivacao";
        } else if (formataDataHora(data_ini) < '2016-12-23' && formataDataHora(data_fim) < '2016-12-23') {
            //tabela antiga
            table = "ResumoTransferID";
            if (bloqueiaConsultaTrnApl(filtro_aplicacoes, filtro_tipo)) {
                atualizaInfo($scope, '<font>Nenhum resultado encontrado.</font>');
                effectNotification();
                $view.find(".btn-gerar").button('reset');
                $view.find(".btn-exportar").prop("disabled", false);
                return;
            }
        } else {
            //ambas	
            //alert('mix de tabelas');
            table = filtro_tipo == "trnid" ? "ResumoTrnID" : "ResumoPontoDerivacao";
            if (!bloqueiaConsultaTrnApl(filtro_aplicacoes, filtro_tipo))
                table = "ResumoTransferID";

        }


        var indicDelig = "        AND (IndicDelig = 'TRN' OR IndicDelig is NULL)";
        if ($scope.filtros.porIit) indicDelig = "        AND (IndicDelig = 'IIT' OR IndicDelig = 'TRN' OR IndicDelig is NULL)";
        if ($scope.tlv) indicDelig = "        AND (IndicDelig = 'TLV' OR IndicDelig is NULL)";
        if ($scope.filtros.porIit && $scope.tlv) indicDelig = "        AND (IndicDelig = 'IIT' OR IndicDelig = 'TRN' OR IndicDelig = 'TLV' OR IndicDelig is NULL)";
        console.log("INDIC DELIG: " + indicDelig);
        if ($scope.filtros.porDdd === true) {

            stmt += "SELECT" + testaLimite($scope.limite_registros) +
                " " + campo[0] + " r.transferid as ti, r.ddd as ddd, sum(r.qtdchamadas) as chamadas, sum(r.qtdsegsduracao) as quantidade" +
                "       FROM " + db.prefixo + table + " r" +
                " INNER JOIN MapeamentoAplicacoes m on m.codaplicacao = r.codaplicacao" +
                "      WHERE 1 = 1" +
                "        AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                "        AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
                //+ "        AND (IndicDelig = 'TRN' OR IndicDelig is NULL)"
                +
                indicDelig
            stmt += restringe_consulta("r.CodAplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("CodSite", filtro_sites, true)
            stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
            stmt += restringe_consulta("DDD", filtro_ddds, true)

            stmt += " GROUP BY " + campo[1] + " transferid, ddd";
            stmt += " ORDER BY transferid, ddd";

            log(stmt);
            //}

        } else {

            stmt += "SELECT" + testaLimite($scope.limite_registros) +
                " " + campo[0] + " r.transferid as ti, sum(r.qtdchamadas) as chamadas, sum(r.qtdsegsduracao) as quantidade" +
                "       FROM " + db.prefixo + table + " r" +
                " INNER JOIN MapeamentoAplicacoes m on m.codaplicacao = r.codaplicacao" +
                "      WHERE 1 = 1" +
                "        AND DatReferencia >= '" + formataDataHora(data_ini) + "'" +
                "        AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
                //+ "        AND (IndicDelig = 'TRN' OR IndicDelig is NULL)"
                +
                indicDelig
            stmt += restringe_consulta("r.CodAplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("CodSite", filtro_sites, true)
            stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
            stmt += restringe_consulta("DDD", filtro_ddds, true)
            stmt += " GROUP BY " + campo[1] + " transferid";
            stmt += " ORDER BY transferid";

            log(stmt);
            // }
        }

        var stmtCountRows = stmtContaLinhas(stmt);

        //Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function formataData2(data) {
            var ano = data.substring(0, 4),
                mes = data.substring(5, 7),
                dia = data.substring(8, 10);
            var ndt = new Date(ano, mes - 1, dia);

            var y = ndt.getFullYear(),
                m = ndt.getMonth() + 1,
                d = ndt.getDate();
            var newdt = y + _02d(m) + _02d(d);
            return newdt;
        }

        function formataDataBR2(data, granularidade) {
            granularidade = granularidade || "dia";

            var ano = data.substring(0, 4),
                mes = data.substring(5, 7),
                dia = data.substring(8, 10);
            var ndt = new Date(ano, mes - 1, dia);

            var y = ndt.getFullYear(),
                m = ndt.getMonth() + 1,
                d = ndt.getDate();
            if (granularidade === "dia") {
                return _02d(d) + "/" + _02d(m) + "/" + y;
            } else if (granularidade === "mes") {
                return _02d(m) + "/" + y;
            } else if (granularidade === "ano") {
                return "" + y;
            }
        }

        function executaQuery(columns) {
            //db.query(stmt, function (columns) {

            $scope.query = "";

            if ($scope.filtros.porDdd === true) {

                var
                    data_hora = columns["datReferencia"] !== undefined ? columns["datReferencia"].value : undefined,
                    datReferencia = data_hora !== undefined ? formataDataBRString(data_hora) : undefined,
                    transferid = columns["ti"].value,
                    ddd = columns["ddd"] !== undefined ? columns["ddd"].value : undefined,
                    chamadas = +columns["chamadas"].value,
                    tma = columns["quantidade"] !== undefined ? (+columns["quantidade"].value / chamadas).toFixed(2) : undefined,

                    tma = +(+columns["quantidade"].value / chamadas).toFixed(2);

                $scope.dados.push({
                    data_hora: data_hora,
                    datReferencia: datReferencia,
                    transferid: transferid,
                    ddd: ddd,
                    chamadas: chamadas,
                    tma: tma
                });

                $scope.csv.push([
                    transferid,
                    ddd,
                    chamadas,
                    tma
                ]);
            } else {

                if ($scope.filtros.porDia === true) {
                    var
                        transferid = columns["ti"].value,
                        data = columns["datReferencia"].value === null ? null : "_" + formataData2(columns["datReferencia"].value),
                        data_BR = columns["datReferencia"].value === null ? null : formataDataBR2(columns["datReferencia"].value),
                        chamadas = +columns["chamadas"].value;
                    /*,
                                                tma = +(+columns["quantidade"].value / chamadas).toFixed(2);*/

                    if (data != null) {
                        var d = $scope.datas[data];
                        if (d === undefined) {
                            d = {
                                data: data,
                                data_BR: data_BR
                            };
                            $scope.datas.push(d);
                            $scope.datas[data] = d;
                            $scope.totais[data] = {
                                chamadas: 0
                            };

                        }
                    }

                    var dado = $scope.dados[transferid];
                    if (dado === undefined) {
                        dado = {
                            transferid: transferid,
                            chamadas: chamadas,
                            //tma: tma,
                            totais: {
                                geral: {
                                    chamadas: 0
                                }
                            }
                        };
                        $scope.dados.push(dado);
                        $scope.dados[transferid] = dado;
                    }

                    if (data != null) {
                        dado.totais[data] = {
                            chamadas: chamadas
                        };
                        dado.totais.geral.chamadas += chamadas;
                        $scope.totais[data].chamadas += chamadas;
                        $scope.totais.geral.chamadas += chamadas;
                    }

                    if ($scope.dados.length % 1000 === 0) {
                        $scope.$apply();
                    }


                } else {
                    var
                        data_hora = columns["datReferencia"] !== undefined ? columns["datReferencia"].value : undefined,
                        datReferencia = data_hora !== undefined ? formataDataBRString(data_hora) : undefined,
                        transferid = columns["ti"].value,
                        chamadas = +columns["chamadas"].value,
                        tma = +(+columns["quantidade"].value / chamadas).toFixed(2);

                    $scope.dados.push({
                        data_hora: data_hora,
                        datReferencia: datReferencia,
                        transferid: transferid,
                        chamadas: chamadas,
                        tma: tma
                    });

                    $scope.csv.push([
                        transferid,
                        chamadas,
                        tma
                    ]);
                }
            }
        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }

            if ($scope.porDia && $scope.dados.length > 0) {

                $scope.dados.forEach(function (dado) {
                    $scope.datas.forEach(function (d) {
                        if (dado.totais[d.data] === undefined) {
                            dado.totais[d.data] = {
                                chamadas: 0
                            };
                        }
                    })
                });

                if ($scope.colunas.length > 1) {
                    $scope.colunas.pop();
                }

                $scope.datas.sort(function (a, b) {
                    return a.data < b.data ? -1 : 1;
                });
                $scope.datas.forEach(function (d) {
                    $scope.colunas.push({
                        field: "totais." + d.data + ".chamadas",
                        displayName: d.data_BR,
                        width: 100
                    });
                });

                $scope.colunas.push({
                    field: "totais.geral.chamadas",
                    displayName: "TOTAL",
                    width: 100
                });
                $scope.dados.push({
                    transferid: "TOTAL",
                    totais: $scope.totais
                });
                $scope.dados.forEach(function (dado) {
                    var linha = [];
                    linha.push(dado.transferid);
                    $scope.datas.forEach(function (d) {
                        linha.push(dado.totais[d.data].chamadas);
                    });
                    linha.push(dado.totais.geral.chamadas);
                    $scope.csv.push(linha);
                });
            }




            $scope.$apply();

        });
        //});

        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        if (!$scope.porDia) {

            //Alex 15-02-2014 TEMPLATE
            var tName = "";


            if ($scope.filtros.porDdd === true) {
                tName = "tResumoTIDDDD";
            } else {
                tName = "tResumoTIDSimples";
            }

            //Alex 15-02-2014 - 26/03/2014 TEMPLATE
            var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
                " WHERE NomeRelatorio='" + tName + "'";
            log(stmt);
            db.query(stmt, function (columns) {
                var dataAtualizacao = columns[0].value,
                    nomeRelatorio = columns[1].value,
                    arquivo = columns[2].value;

                var milis = new Date();
                var baseFile = '' + tName + '.xlsx';
                var file = 'r' + (tName.substring(2)).substring(0, tName.length - 7) + '_' + formataDataHoraMilis(milis) + '.xlsx';
                var buffer = toBuffer(toArrayBuffer(arquivo));

                fs.writeFileSync(baseFile, buffer, 'binary');

                var newData;

                fs.readFile(baseFile, function (err, data) {
                    // Create a template
                    var t = new XlsxTemplate(data);

                    // Perform substitution
                    t.substitute(1, {
                        filtros: $scope.filtros_usados,
                        planDados: $scope.dados
                    });

                    // Get binary data
                    newData = t.generate();

                    if (!fs.existsSync(file)) {
                        fs.writeFileSync(file, newData, 'binary');
                        childProcess.exec(file);
                    }
                });

                setTimeout(function () {
                    $btn_exportar.button('reset');
                }, 500);
            }, function (err, num_rows) {
                userLog(stmt, 'Exportar XLSX', 2, err)
                //?
            });
        } else {
            // Criar cabeçalho
            var cabecalho = $scope.datas.map(function (d) {
                return {
                    value: d.data_BR,
                    bold: 1,
                    hAlign: 'center',
                    autoWidth: true
                };
            });

            // Inserir primeira coluna: item de controle
            var tipo = $view.find("select.filtro-tipo").val() == "pontoderivacao" ? "Ponto Derivação" : "Transfer Id";
            cabecalho.unshift({
                value: tipo,
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            });

            // Inserir última coluna: total por linha
            cabecalho.push({
                value: "Total",
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            });

            var linhas = $scope.dados.map(function (dado) {
                var linha = $scope.datas.map(function (d) {
                    try {
                        return {
                            value: dado.totais[d.data].chamadas || 0
                        };
                    } catch (ex) {
                        return 0;
                    }
                });

                // Inserir primeira coluna: item de controle
                linha.unshift({
                    value: dado.transferid
                });

                // Inserir última coluna: total por linha
                linha.push({
                    value: dado.totais.geral.chamadas
                });

                return linha;
            });

            // Criar última linha, com os totais por data
            var linha_totais = $scope.datas.map(function (d) {
                try {
                    return {
                        value: $scope.totais[d.data].chamadas || 0
                    };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna
            linha_totais.unshift({
                value: "Total",
                bold: 1
            });

            // Inserir última coluna: total geral
            //linha_totais.push({ value: $scope.totais.geral.chamadas });

            // Inserir primeira linha: cabeçalho
            linhas.unshift(cabecalho);

            // Inserir última linha: totais por data
            //linhas.push(linha_totais);

            var planilha = {
                creator: "Estalo",
                lastModifiedBy: $scope.username || "Estalo",
                worksheets: [{
                    name: 'Resumo por TransferID',
                    data: linhas,
                    table: true
                }],
                autoFilter: false,
                // Não incluir a linha do título no filtro automático
                dataRows: {
                    first: 1
                }
            };

            var xlsx = frames["xlsxjs"].window.xlsx;
            planilha = xlsx(planilha, 'binary');


            var milis = new Date();
            var file = 'resumoTransferID_' + formataDataHoraMilis(milis) + '.xlsx';


            if (!fs.existsSync(file)) {
                fs.writeFileSync(file, planilha.base64, 'base64');
                childProcess.exec(file);
            }

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);
        }
    };

    function bloqueiaConsultaTrnApl(codAplicacao, tipo) {
        if (tipo == "pontoderivacao") {
            if (codAplicacao.indexOf("EMPUNIF") > -1 || codAplicacao == "MULTPROD" ||
                codAplicacao == "NOVAOITV" || codAplicacao.indexOf("TLV") > -1) {
                return true;
            }

        } else if (tipo == "trnid") {
            if (codAplicacao.indexOf("103") > -1 || codAplicacao == "OCTUNIF" ||
                codAplicacao == "NGRPRE" || codAplicacao == "POSPAGO" ||
                codAplicacao == "OICANCEL") {
                return true;
            }
        }
        return false;
    }



    function retornaTipo(codAplicacao) {

        if (codAplicacao.indexOf("EMPUNIF") > -1 || codAplicacao == "MULTPROD" ||
            codAplicacao == "NOVAOITV" || codAplicacao == "EMPUNIF2") {
            return "trnid";


        } else if (codAplicacao == "OCTUNIF" ||
            codAplicacao == "NGRPRE" || codAplicacao == "POSPAGO" ||
            codAplicacao == "OICANCEL") {
            return "pontoderivacao";

        } else {
            return "sem transferid"
        }

    }


    $scope.abreAjuda = function (link) {
        abreAjuda(link);
    }
}
CtrlTransferID.$inject = ['$scope', '$globals'];