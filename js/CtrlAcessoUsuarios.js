function CtrlAcessoUsuarios($scope, $globals) {

    win.title = "Monitor de acesso";//Brunno 16/01/2019
    var idView = "pag-acesso-usuarios";
    $scope.versao = versao;
    $scope.rotas = rotas;
    var data = [];
    var dado = [];
    $scope.localeGridText = {
            contains: 'Contém',
            notContains: 'Não contém',
            equals: 'Igual',
            notEquals: 'Diferente',
            startsWith: 'Começa com',
            endsWith: 'Termina com'
    }

    var $view;
 //Grafico
 
 var resetCanvas = function(){
  $('#myChart').remove(); // this is my <canvas> element
  $('#canvas-container').append('<canvas id="myChart"><canvas>');
  canvas = document.getElementById('myChart');
  ctx = canvas.getContext('2d');
  ctx.canvas.width = $('#graph').width(); // resize to parent width
  ctx.canvas.height = $('#graph').height(); // resize to parent height
  var x = canvas.width/2;
  var y = canvas.height/2;
  ctx.font = '10pt Verdana';
  ctx.textAlign = 'center';
  ctx.fillText('This text is centered on the canvas', x, y);
};

 ctx = document.getElementById('myChart').getContext('2d');
    // Filtros: data e hora
    var agora = new Date();
    var inicio = Estalo.filtros.filtro_data_hora[0] || hoje();
    var fim = Estalo.filtros.filtro_data_hora[1] || hojeFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };

    var $view;
    setInterval(function(){ if(win.title === "Monitor de acesso"){
        $scope.gridImporta.rowData = [];    
        $scope.listarImporta.apply(this);
        } }, 120000);
    
    $scope.$on('$viewContentLoaded', function () {
        $view = $("#" + idView);

       

        
        

        $view.find(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });

        componenteDataMaisHora($scope, $view);

        // Carregar filtros
        

        // Popula lista de segmentos a partir das aplicações selecionadas
        

        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($view.find('.filtro-site'), 'sites'); });

       

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-aplicacao').mouseover(function () {
            $view.find("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $view.find("div.datahora-fim.input-append.date").data("datetimepicker").hide();
        });

        // EXIBIR AO PASSAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseover(function () {
            $view.find('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $view.find('div.btn-group.filtro-site').mouseout(function () {
            $view.find('div.btn-group.filtro-site').removeClass('open');
        });

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#" + idView);
            
            // Testar se a data inicial é maior que a data final
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            // Testar se uma aplicação foi selecionada
            // var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
            // if (filtro_aplicacoes.length === 0 || filtro_aplicacoes[0] === null) {
            //     setTimeout(function () {
            //         atualizaInfo($scope, 'Selecione no mínimo uma aplicação');
            //         effectNotification();
            //         $view.find(".btn-gerar").button('reset');
            //     }, 500);
            //     return;
            // }
            
    
            $scope.gridExporta.rowData = [];
            $scope.listarExporta.apply(this); 
                
        
                    

    //Grafico
        });
    });
    
    $scope.gridExporta = {
            rowSelection: 'single',
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
            },
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
            overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
           
            columnDefs: [  
                { headerName: 'Empresa', field: 'procEmpresa',width: 130 },
                { headerName: 'Consultas', field: 'proxQuantidade' ,width: 150},
                    
            ],
            
            rowData: [],
            localeText: $scope.localeGridText,
            suppressHorizontalScroll: true
    };
//     $scope.gridExporta.getRowStyle = function(params) {
//         if (params.data.proxExecStatus === "Normal") {
//             return { background: 'rgb(223,240,216)' }
//         }else{
//             return { background: '#EFF692', color: 'red' } 
//         }
//     }
	

    $scope.gridImporta = {
        rowSelection: 'single',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        onGridReady: function(params) {
                params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado encontrado.</span>',
        columnDefs: [
                { headerName: 'Usuário', field: 'procUsuario', width: 310 },
                { headerName: 'Empresa', field: 'procEmpresa' },
                { headerName: 'Relátorio', field: 'procRelatorio', width: 300},				
				
        ],
        rowData: [],
	angularCompileRows: true,
        localeText: $scope.localeGridText,
        suppressHorizontalScroll: true
        };
		

	function linkClicked(val) {
        // alert("val clicked: " + val);
		gui.Shell.openExternal(val);
    }
		
		
	function linkCellRendererFunc(params) {	
		params.$scope.linkClicked = linkClicked;
        return '<button ng-click="linkClicked(data.url)">Doc</button>';
    }
	


    $scope.$on('$viewContentLoaded', function () {
            $view = $("pag-acesso-usuarios");

            limpaProgressBar($scope, "pag-acesso-usuarios");
            
            
            $scope.gridImporta.rowData = [];
            $scope.listarImporta.apply(this);

            
            // Botão abortar Alex 23/05/2014
            $view.on("click", ".abortar", function() {
                    $scope.abortar.apply(this);
            });			
			
            //Alex 23/05/2014
            $scope.abortar = function() {
                    abortar($scope, "pag-acesso-usuarios");
            }
           
    });

    $scope.listarExporta = function() {
		resetCanvas();
        console.log('Buscando Empresas')
        var $btn_gerar_exporta = $(this);
        $btn_gerar_exporta.button("loading");
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
        var stmtExporta = 
        "select replace(upper(u.empresa),'','') as empresa, "+
        "count(*) as acessos "+
        "from usuariosestatura u "+
        "inner join logestaloView l "+
        "on l.login = u.loginusuario "+
        "and l.dominio = u.dominio  "+ 
        "where statususer is not null "+
        "and u.empresa not like 'vers%' "+
        "and u.dominio not like '%erver%' "+
        "and l.login not like '%vers%' "+
        "and relatorio not like 'Estalo%' "+
        "and l.datahora >= '"+ formataDataHora(data_ini) + "' AND l.datahora < '" + formataDataHora(data_fim) +"' "+ 
        "group by u.empresa "+
        "order by  acessos desc  ";
        
        db.query(stmtExporta, function(columns) {
                    $scope.gridExporta.rowData.push({
                         
                        procEmpresa: columns[0].value,
                        proxQuantidade: columns[1].value,  
                    });
                data.push({
                        pData:columns[0].value});
            }, function(err, num_rows) {
                    if (err) {
                            console.log('Erro ao buscar exportação: ' + err);
                            $btn_gerar_exporta.button("reset");
                            limpaProgressBar($scope, "pag-acesso-usuarios");
                    } else {
							limpaProgressBar($scope, "pag-acesso-usuarios");
                            //console.log('Registros encontrados: ' + num_rows);
                            retornaStatusQuery($scope.gridExporta.rowData.length, $scope);

                            $scope.gridExporta.api.setRowData($scope.gridExporta.rowData);
                            $scope.gridExporta.api.refreshCells({force: true});
                            //$scope.gridImporta.rowData = [];
                            //$scope.listarImporta.apply(this)
                            $btn_gerar_exporta.button("reset");
                            dado = [];
                         $scope.listarTempo.apply(this);    
                            //console.log('row data ', $scope.gridExporta.rowData.map(function(obj){return obj.proxQuantidade}));
                            							
                    }
            });
    };

    
    $scope.listarImporta = function() {
        console.log('Lista numero de pessoas acesso estalo')
        
        var $btn_gerar_import = $(this);
        $btn_gerar_import.button("loading");
        var stmtMailing = " SELECT distinct (nome) , relatorio, replace(upper(Empresa),'','') as Empresa from logestaloView "+
        "inner join usuariosestatura on [login] = LoginUsuario where datahora >= DATEADD(minute,-2,GETDATE()) "+
        "and relatorio not like 'Estalo%' and Empresa not like '%ers%til' and Empresa not in ('VERSATEC','Versatil','Versátil') "+
        "order by nome asc"

        db.query(stmtMailing, function(columns) {

                $scope.gridImporta.rowData.push({
                        procUsuario: columns[0].value,
                        procEmpresa: columns[2].value,
                        procRelatorio: columns[1].value,
						
                });

        }, function(err, num_rows) {
                if (err) {
                        console.log('Erro ao buscar Importa: ' + err);
                        $btn_gerar_import.button("reset");
                        limpaProgressBar($scope, "pag-acesso-usuarios");
                } else {
                        console.log('Registros encontrados: ' + num_rows);
                        retornaStatusQuery($scope.gridImporta.rowData.length, $scope);
                        console.log($scope.gridImporta.rowData.length)
                        if ($scope.gridImporta.rowData.length !== 0){
                        $scope.gridImporta.api.setRowData($scope.gridImporta.rowData);
                        $scope.gridImporta.api.refreshCells({force: true});
                        }
                        $btn_gerar_import.button("reset");

                        limpaProgressBar($scope, "pag-acesso-usuarios");
                      
                }
        });
}

$scope.listarTempo = function() {
        console.log('Lista de tempo')
        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;
        var $btn_gerar_import = $(this);
        $btn_gerar_import.button("loading");
        var stmtTempo = " SELECT convert(date,datahora) as [data],count(distinct [login]) as qtd from logestaloView "+
        "inner join usuariosestatura on [login] = LoginUsuario where datahora >= '"+ formataDataHora(data_ini) + "' AND datahora < '" + formataDataHora(data_fim) +"' "+
        "and relatorio not like 'Estalo%' and Empresa not like '%ers%til' and Empresa not in ('VERSATEC','Versatil','Versátil') "+
        "group by convert(date,datahora) order by convert(date,datahora) asc"

        db.query(stmtTempo, function(columns) {

                dado.push({
                        procData: columns[0].value,
                        procCount: columns[1].value,						
                });

        }, function(err, num_rows) {
                if (err) {
                        console.log('Erro ao buscar Importa: ' + err);
                        $btn_gerar_import.button("reset");
                        limpaProgressBar($scope, "pag-acesso-usuarios");
                } else {
                        console.log('Registros encontrados: ' + num_rows);
                        //retornaStatusQuery($scope.gridImporta.rowData.length, $scope);
                        $btn_gerar_import.button("reset");

                        //limpaProgressBar($scope, "pag-acesso-usuarios");

                        myChart = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                        labels: dado.map(function(obj){return obj.procData}),
                                        datasets: [
                                         {
                                        type: 'bar',
                                        label: '',
                                        data: dado.map(function(obj){return obj.procCount}),
                                        backgroundColor: 'rgba(255, 99, 132, 0.2)',
                                        yAxisID: 'client',
                                        }                
                                        ]
                                },
                                options: {
                                        scales: {
                                        yAxes: [{
                                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                        display: true,
                                        id: 'client',
                                        }, 
                                //         {
                                //         type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                //         display: true,
                                //         id: 'pareto',
                                //     // grid line settings
                                //         gridLines: {
                                //         drawOnChartArea: false, // only want the grid lines for one axis to show up
                                //         },
                                //         // ticks: {
                                //         // callback: function(value, index, values) {
                                //         //         return (Math.round(value * 100)) + '%';
                                //         // }
                                //         // }
                                //         }
                                
                                        ]
                                        },
                                        tooltips: {
                                        callbacks: {
                                        label: function(tooltipItem, data) {
                                        if (tooltipItem.datasetIndex === 1) {
                                                return (Math.round(tooltipItem.yLabel * 100)) + '%';
                                        } else {
                                                return tooltipItem.yLabel;
                                        }
                                        }
                                        }
                                        },
                                        responsive:true,
                                        maintainAspectRatio: false,
                                },
                                });
                      
                }
        });
}

}

CtrlAcessoUsuarios.$inject = ['$scope', '$globals'];