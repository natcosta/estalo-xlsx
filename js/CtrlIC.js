function CtrlIC($scope, $globals) {

    win.title = "Cadastro de IC"; //Matheus 2017.1
    $scope.versao = versao;

    //$scope.usuario = {};
    $scope.ICData = []; // Dados do grid de usuários
    $scope.ICData2 = []; // Dados Repetidos
    $scope.aplicacoes = []; //
    $scope.grupoAplicacoes = []; // Grupos de Aplicacoes

    $scope.aplicacoes = "";
    $scope.ICCods = [];
    $scope.ICRepetidos = [];

    $scope.ICs = [
        { field: "codIC", displayName: "Cod. IC", width: '25%' },// cellClass:"span1", width: 150,
        { field: "desc", displayName: "Descrição", width: '75%' }//, cellClass:"span2", width: 80,
        //{ field: "checked", displayName: "Hab.", width: '5%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellRepetida" ><input type="radio" name="{{row.entity.codIC}}" value="{{row.entity.desc}}" class="repeatedRadioBox" ng-model="form" ng-change="updateReplics({{row.entity.codIC}}, {{row.entity.desc}})"></span></div>' }
        //{ field: "fluxo", displayName: "Fluxo", cellClass: "grid-align", pinned: true },//  cellClass:"span1", width: 80,
        //{ field: "aplicacao", displayName: "Aplicação", cellClass: "grid-align", pinned: true }
    ];


    $scope.gridIC = {
        data: $scope.ICData,
        columnDefs: $scope.ICs,
        rowHeight: 55,
        enableColumnResizing: true
    };

    //* Tree view responsável pelo menu de navegação

    var $viewctrlic;
    var workbook = "";


    var files = document.getElementById('ICInput');

    //Função responsável pelo texto em baixo do footsteps
    function ICAlert(texto, html) {
        $(".confirmBtn").prop("disabled", false);
        if (html) {
            $(".ICAlert").html(texto);
            return;
        }

        if (texto != null && texto != undefined) {
            texto =
                $(".ICAlert").html("<strong>" + texto + "</strong>");
            return;
        } else {
            $(".ICAlert").text("");
            return;
        }

    };

    //Fim da função responsável pelo texto em baixo do footsteps
    /* fixdata and rABS are defined in the drag and drop example */
    function handleFile(e) {
        //ICAlert();
        console.log("Scopo Apl: " + $scope.aplicacoes);
        if ($scope.aplicacoes.length <= 0 || $scope.aplicacoes == false) {
            $scope.aplicacoes = $(".filtro-aplicacao").val();
        }

        //console.log("Filtro: "+$scope.aplicacoes+" Array: "+$(".filtro-aplicacao").val());
        /*if($scope.aplicacoes==null||$scope.aplicacoes.length<=0){
        ICAlert("Selecione um filtro para prosseguir o cadastro.");
        //notificate("Selecione um filtro para prosseguir o cadastro de IC's.", $scope);
        return;
        }else{ */

        $scope.ICCods = [];
        $scope.ICRepetidos = [];
        $scope.ICData = [];
        $scope.ICData2 = [];
        var fname = $("#ICInput").val();
        var fextension = fname.substr((~-fname.lastIndexOf(".") >>> 0) + 2);
        console.log("Fextension: " + fextension);

        var startIndex = (fname.indexOf('\\') >= 0 ? fname.lastIndexOf('\\') : fname.lastIndexOf('/'));
        var filename = fname.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }

        if (fextension.toLowerCase() != "xlsx" && fextension.toLowerCase() != "xls") {
            alerte("Tipo de arquivo não suportado, utilize o template de ICs no formato XLSX!", "Falha ao enviar arquivo");
            //notificate(filename+" possui tipo de arquivo não suportado (."+fextension+"), utilize o template Versátil com o formato .xlsx.", $scope);
            return false;
        }


        var files = e.target.files;
        var i, f;
        var rABS = true;
        for (i = 0; i != files.length; ++i) {
            f = files[i];
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                var data = e.target.result;

                var workbook;
                try {
                    if (rABS) {
                        /* if binary string, read with type 'binary' */
                        workbook = XLSX.read(data, { type: 'binary' });
                    } else {
                        /* if array buffer, convert to base64 */
                        var arr = fixdata(data);
                        workbook = XLSX.read(btoa(arr), { type: 'base64' });
                    }
                } catch (error) {
                    //notificate("Arquivo não suportado ("+error+"), favor utilizar o template Versátil", $scope);
                    $scope.gridIC.data = $scope.ICData;
                }
                //$scope.workbook = workbook;
                console.log("Workbook: " + workbook);


                prepareWorkbook(workbook, filename, fextension);
                //}
                /* DO SOMETHING WITH workbook HERE */
            };
            reader.readAsBinaryString(f);
        }
    }

    $scope.listaAplicacoes = function () {
        $scope.grupoAplicacoes = [];
        stmt = "Select CodGrupo, CodAplicacao, NomeGrupo from GrupoAplCadastroValores";

        //console.log("Consulta GrupoAplCadastroValores STMT: "+stmt);
        log(stmt);
        db.query(stmt, populateAplicacoes, function (err, num_rows) {

            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            console.log("" + num_rows);
        });
    }

    function populateAplicacoes(columns) {
        //console.log("Columns: "+columns);
        var codGrupo = columns[0].value,
            codAplicacao = columns[1].value,
            NomeGrupo = columns[2].value;

        dado = {
            codAplicacao: codAplicacao,
            groupName: NomeGrupo,
            codGroup: codGrupo
        };

        //console.log("Pushando dados para DataGridGroups "+NomeGrupo+" cod: "+codGrupo);

        $scope.grupoAplicacoes.push(dado);
    }

    // Função responsável por tratar a planilha
    function prepareWorkbook(workbook, filename, fextension) {

        function sugereGrupo(apl) {
            console.log("APL Sugerida: " + apl);
            var x = $scope.grupoAplicacoes.filter(function (o) { return o.codAplicacao === apl; });
            if (x.length === 0) return;
            var grupo = x[0].groupName;
            var aplicacoes = $scope.grupoAplicacoes.filter(function (o) { return o.groupName === grupo; }).map(function (o) { return o.codAplicacao; });
            console.log("Grupo retornado: " + grupo);
            return { groupName: grupo, aplicacoes: aplicacoes };
        }

        function adicionaGrupos(ret) {
            console.log("Original Aplicacoes: " + $scope.aplicacoes);
            console.log("Ret Aplicacoes: " + ret.aplicacoes);
            $scope.aplicacoes = $scope.aplicacoes.concat(ret.aplicacoes);
            console.log("Aplicacoes depois do concat:" + $scope.aplicacoes);
            $scope.aplicacoes = unique($scope.aplicacoes);
            console.log("Scope Unique: " + $scope.aplicacoes);
        }

        $scope.ICCods = [];
        $scope.ICRepetidos = [];
        $scope.ICData = [];
        $scope.ICData2 = [];
        $scope.retotal = [];
        grupos = [];

        console.log("File Name: " + filename + " file extension: " + fextension);

        var first_sheet_name = workbook.SheetNames[0]; // Sheet 0 = IC
        var i = 8; // Célula inicial de conteúdo para IC = D8
        var j = 0; // Conta vazias
        var totalCells = 0;

        /* Get worksheet */
        var worksheet = workbook.Sheets[first_sheet_name];
        var sheet_name_list = workbook.SheetNames;

        if (worksheet["F7"] == undefined || worksheet["F7"] == null || worksheet["E7"] == undefined || worksheet["E7"] == null) {
            console.log("Cabeçalhos de código e conteúdo não encontrados, planilha padrão não está sendo utilizada");
            alerte("Falha ao carregar arquivo, utilize o template da Versátil para o tratamento de ICs.", "Falha ao carregar arquivo");
            //notificate("Falha ao carregar, por favor utilize o <a href='http://www.versatec.com.br/downloads/bases/TemplateIC.xlsx'>template</a> da Versátil para inclusão, exclusão e alteração de ICs.", $scope);
            return false;
        }


        if (!worksheet["F7"].v.includes("IC") || !(worksheet["E7"].v == "Descrição" || worksheet["E7"].v == "Descricão")) {
            console.log("IC: \r\n" + worksheet["F7"].v + " \r\n Conteudo: " + worksheet["E7"].v);
            alerte("Falha ao carregar arquivo, utilize o template da Versátil para o tratamento de ICs.", "Falha ao carregar arquivo");
            //notificate("Falha ao carregar, por favor utilize o template da Versátil para inclusão, exclusão e alteração de ICs.", $scope);
            return false;
        }

        var temicnastring = true;
        while (j < 10) {//&&$scope.ICData2.length<=0

            var address_of_cell = 'E' + i; // Célula inicial de conteúdo para IC = E8
            /* Find desired cell */
            var desired_cell = worksheet[address_of_cell];

            if (desired_cell == null || desired_cell == "") {//||desired_value==undefined){
                console.log("Celula Vazia N " + j)
                j++;
            } else {
                var codigoIC = "";

                var fluxoIC = "";
                /*if(worksheet["D"+i]==undefined||worksheet["D"+i]==null){
                codigoIC="";
                }else{*/
                codigoIC = "" + worksheet["F" + i].v + "";
                codigoIC = codigoIC.trim();
                codigoIC = codigoIC.replace(/(?:\r\n|\r|\n)/g, '');
                codigoIC = codigoIC.replace(" ", "");
                if (!(codigoIC.match("IC") || codigoIC.match("ic"))) {
                    temicnastring = false;
                    //console.log("ENCONTRADO IC SEM 'ic' NA STRING!!!!!!!1111ONZE")
                    alerte("Por favor, verifique os valores inseridos para código IC, não utilize formatação na planilha.", "Erros na planilha");
                }

                //codigoIC = codigoIC.replace(".wav", "");
                //}

                if (worksheet["G" + i] == undefined || worksheet["G" + i] == null) {
                    fluxoIC = "";
                } else {
                    fluxoIC = worksheet["G" + i].v;
                }

                var codAplicacao = "";
                if (worksheet["C5"] == undefined || worksheet["C5"] == null) { } else {
                    var codAplicacao = worksheet["C5"].v;
                }
                var linha = "";
                /* Get the value */
                var desired_value = desired_cell.v;
                /*desired_value = desired_value.replace(/(?:\r\n|\r|\n)/g, '');
                desired_value = desired_value.replace(/[']/g, "''") 
                desired_value = desired_value.unquote();*/
                desired_value = mysql_real_escape_string(desired_value);

                //console.log("Valor i: "+i+" J: "+j+" desired value: "+desired_value);//Codigo IC: "+codigoIC+"  Fluxo: "+fluxoIC);
                if (codigoIC != "" && $scope.ICCods.indexOf(codigoIC) == -1) {
                    $scope.ICCods.push(codigoIC);
                    linha = { codIC: codigoIC, desc: desired_value, fluxo: fluxoIC, aplicacao: codAplicacao }
                    $scope.ICData.push(linha);
                    totalCells++;
                    //console.log("ICs em memória: "+$scope.ICCods.toString());
                } else {

                    if (codigoIC == "" || codigoIC == undefined || codigoIC == null || codigoIC.match(" ")) {
                        console.log("Código IC com erros!: " + codigoIC + " Na posição :" + i + " ");
                        //ICAlert("Falha ao carregar, por favor utilize o <a href='http://www.versatec.com.br/downloads/bases/TemplateIC.xlsx'>template</a> da Versátil para inclusão, exclusão e alteração de ICs.", true);
                        alerte("Os códigos IC foram escritos de maneira que não é suportada pelo sistema, por favor escreva manualmente e remova os espaços.", "Falha ao cadastrar");
                        $scope.emptyData();
                        break;
                        return false;
                    }


                    if (codigoIC != "" && $scope.ICCods.indexOf(codigoIC) > -1) {
                        //console.log("Comparando Linhas para o código: "+codigoIC);
                        linha = { codIC: codigoIC, desc: desired_value, fluxo: fluxoIC, aplicacao: codAplicacao };
                        $scope.ICData.map(function (linhas) {
                            if (linhas.codIC == linha.codIC) {
                                console.log("codICA: " + linhas.codIC + " codICB: " + linha.codIC);
                                console.log("DescriptionA: " + linhas.desc + " DescriptionB: " + linha.desc);
                                if (linhas.desc != linha.desc) {
                                    console.log("As descrições são diferentes: " + linhas.desc + " codigo:" + linhas.codIC);
                                    //alerte("O IC "+linhas.codIC+" foi inserido duas ou mais vezes com descrições diferentes, corrija e tente novamente", "Falha ao cadastrar");
                                    $scope.ICData2.push(linhas);
                                    $scope.ICData2.push(linha)
                                    //j = 10;
                                }
                                if (linhas.desc == linha.desc) {
                                    $scope.ICRepetidos.push(codigoIC);
                                }
                            }
                        });

                        //console.log("ICData2: "+$scope.ICData2.toString());						
                    }
                }
            }

            // if($scope.ICData2.length>0||j>=10){
            // console.log("Length PD2: "+$scope.ICData2.length+" J: "+j);
            // ICAlert("O IC "+$scope.ICData2[0].codIC+" é repetido com diferentes valores na planilha, upload cancelado!");
            // $scope.emptyData();
            // }

            i++;
        }

        if (j >= 10) { //&& $scope.ICData2.length <= 0

            /*var solicitante = "";
            if (worksheet["C4"] == undefined || worksheet["C4"] == null) {
                solicitante = "";
            } else {
                solicitante = " solicitada por " + worksheet["C4"].v;
            }*/



            ///Tratamento de aplicações selecionadas e grupos a quais pertencem
            i = 0;
            $scope.retotal = [];
            var ret = [];
            while (i < $scope.aplicacoes.length) {
                //console.log("Comparando: "+$scope.aplicacoes[i]);
                var ret = sugereGrupo($scope.aplicacoes[i]);//sugereGrupo($scope.grupoAplicacoes[i].codAplicacao);

                if (ret !== undefined) {
                    $scope.retotal.push(ret);
                    //console.log("Seu tapado, não esqueceu nada não?");
                    //console.log(ret.aplicacoes);
                } else {
                    //console.log("Partiu cadastro então")
                    //console.log("RET: "+ret);
                }
                i++;
            }
            if ($scope.ICData.length >= 0) {


                $scope.gridIC.data = $scope.ICData2.length > 0 ? $scope.ICData2 : $scope.ICData;
                $('.grid').resize();
                $scope.step1 = "completed";
                $scope.step2 = "active";


                if ($scope.ICData2.length > 0) {
                    ICAlert("Alguns ICs aparecem na lista com <font color='red'>descrições diferentes</font>, corrija a planilha. Upload <font color='red'>cancelado!</font>", true);
                    $(".confirmBtn").attr("disabled", true)
                    //$scope.emptyData();
                } else {
                    ICAlert("<strong>" + $scope.ICData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + ".", true);
                }

                if ($scope.retotal.length > 0 && $scope.ICData2.length <= 0) {

                    console.log("Antes do unique $scope.retotal Length: " + $scope.retotal.length + " $scope.retotal String: " + $scope.retotal.toString() + " $scope.retotal: " + $scope.retotal);
                    $scope.retotal = uniqueApl($scope.retotal);
                    i = 0, grupos = "";
                    console.log("$scope.retotal Length: " + $scope.retotal.length + " $scope.retotal String: " + $scope.retotal.toString() + " $scope.retotal: " + $scope.retotal);
                    
                    while (i < $scope.retotal.length) {
                        //console.log("$scope.retotal I:"+i+" grupo: "+$scope.retotal[i].groupName);
                        if (i == 0) {
                            grupos = $scope.retotal[i].groupName;
                        } else {
                            grupos = $scope.retotal[i].groupName + ", " + grupos;
                        }
                        i++;
                    }

                    confirme("Você selecionou " + $scope.aplicacoes + ", gostaria de inserir em todas as aplicações " + grupos + "?", "Confirme", "Sim", function (realize) {
                        if (realize) { //do something
                            i = 0;
                            while (i < $scope.retotal.length) {
                                //console.log("Ret de I:"+i+" RET: "+$scope.retotal[i]);
                                adicionaGrupos($scope.retotal[i]);
                                i++;
                            }
                            if (!temicnastring) {
                                alerte("Por favor, verifique os valores inseridos para código IC. Lembre-se de não utilizar variáveis na planilha, os valores devem ser escritos manualmente.", "Planilha com variáveis");
                            } else {
                                //alerte("ICS OK!", "Planilha ok");
                            }

                            console.log("Scopo Final Aplicacoes: " + $scope.aplicacoes);
                        } else {
                            console.log("Não realizou a adição de aplicacoes!");
                        }
                    });

                    if ($scope.ICRepetidos.length >= 1) {
                        ICAlert("<strong>" + $scope.ICData.length + " linhas encontradas </strong>, a inserir em: " + $scope.aplicacoes + " com " + $scope.ICRepetidos.length + " duplicadas.", true);
                        console.log("\n ICs Repetidos: " + $scope.ICRepetidos);
                    } 
                }

                ///Fim do tratamento de aplicações selecionadas e grupos a quais pertencem
            } else {
                /*console.log("$SCOPE.ICDATA2 <=0 = false ? " + $scope.ICData2.length)
                console.log("$SCOPE.RETOTAL <=0 = false ? " + $scope.retotal.length)*/
                $scope.emptyData();
                ICAlert("A planilha carregada <font color='red'>NÃO</font> possui informações, cadastro caneclado!");

            }
        }
    }

    //Fim da Função responsável por tratar a planilha 
    /*$scope.step1 = "active";
    $scope.step2 = "disabled";
    $scope.step3 = "disabled";
    */
    // Função vinculada ao botão cancelar, utilizada para limpar dados importados 
    $scope.emptyData = function () {
        $scope.ICData = [];
        $scope.gridIC.data = [];
        $scope.step1 = "active";
        $scope.step2 = "disabled";
        $scope.step3 = "disabled";
        $("#ICInput").val('');
        var string = "<strong>Atenção!</strong> utilize o <a href='http://www.versatec.com.br/downloads/bases/TemplateIC.xlsx'>template <i class='fa fa-download'></i></a> da Versátil para utilizar a tela cadastro de ICs.";
        ICAlert(string, true);
        console.log("Dados de GRID e IC esvaziados! FIles:" + $("#ICInput").val(''));

        $scope.aplicacoes = [];
        $(".filtro-aplicacao").val("");
        //$('body').resize();
        $(".filtro-aplicacao").resize();
        $(".filtro-aplicacao").selectpicker('refresh');
        $(".confirmBtn").prop("disabled", false);
    };
    // Fim da função vinculada ao botão cancelar, utilizada para limpar dados importados 


    $scope.registraICs = function () {
        var mergestmt = "MERGE dbo.itemdecontrole as A USING tempdb..#itemdecontroleTC as B ON A.Cod_Item = B.Cod_Item And A.Cod_Aplicacao = B. Cod_Aplicacao " +
            "WHEN MATCHED THEN UPDATE SET descricao = B.Descricao WHEN NOT MATCHED THEN INSERT (Cod_Item,Descricao,Cod_aplicacao) VALUES(B.Cod_Item,B.Descricao,B.Cod_aplicacao);";
        $(".confirmBtn").prop("disabled", true);

        var apl = 0, j = 0, fiveh = 1;
        var insert = "INSERT INTO tempdb..#itemdecontroleTC(Cod_Item, Descricao, Cod_aplicacao) VALUES ";
        var insertstmt = insert;

        console.log("Aplength: " + $scope.aplicacoes.length + " ICsLength: " + $scope.ICData.length);
        while (apl < $scope.aplicacoes.length) {
            codAplicacao = $scope.aplicacoes[apl];
            i = 0;
            while (i < $scope.ICData.length) {
                /*if(i==0&&apl==0){
                    insert = insert+"('"+$scope.ICData[i].codIC+"', '"+$scope.ICData[i].desc+"', '"+codAplicacao+"'),";
                */
                if ((j / 500) < fiveh) {
                    if ((i + 1 == $scope.ICData.length && (apl + 1 == $scope.aplicacoes.length)) || ((j + 1) / 500 == fiveh)) {
                        //console.log("CodIC: "+$scope.ICData[i].codIC+" I: "+i+" J: "+j);
                        insert = insert + "('" + $scope.ICData[i].codIC + "', '" + $scope.ICData[i].desc + "', '" + codAplicacao + "');";
                    } else {
                        insert = insert + "('" + $scope.ICData[i].codIC + "', '" + $scope.ICData[i].desc + "', '" + codAplicacao + "'),";
                    }
                }
                if (j / 500 == fiveh) {
                    //console.log("i/500=fiveh CodIC: "+$scope.ICData[i].codIC+" I: "+i+" J: "+j);
                    insert = insert + " " + insertstmt + "('" + $scope.ICData[i].codIC + "', '" + $scope.ICData[i].desc + "', '" + codAplicacao + "'),";

                    fiveh++;
                }
                i++; //Contador dessa aplicação
                j++; ///Contador total
            }
            apl++;
        }
        /* Before 500 update // Matheus 13/04
        while(apl<$scope.aplicacoes.length){
        codAplicacao = $scope.aplicacoes[apl];
        i=0;
        while(i<$scope.ICData.length){
        if((i+1==$scope.ICData.length)&&(apl+1==$scope.aplicacoes.length)){
        insert = insert+"('"+$scope.ICData[i].codIC+"', '"+$scope.ICData[i].desc+"', '"+codAplicacao+"')";
        }else{
        insert = insert+"('"+$scope.ICData[i].codIC+"', '"+$scope.ICData[i].desc+"', '"+codAplicacao+"'),";
        }
                            	
        i++;
        }
        apl++;
        }*/

        //console.log("Insert Final: "+insert);

        //console.log("Registra ICs....");
        if (!$scope.aplicacoes) {
            alerte("Nenhuma aplicação foi selecionada para o cadastro de IC's, por favor inicie o processo novamente.", "Falha ao confirmar cadastro.");
            $scope.emptyData();
            $(".confirmBtn").prop("disabled", false);
            return false;
        }

        stmt = "if OBJECT_ID('tempdb..#itemdecontroleTC') is null" +
            " CREATE TABLE #itemdecontroleTC(" +
            "[Cod_Item] [varchar](100) NOT NULL," +
            "[Descricao] [varchar](200) NOT NULL," +
            "[Cod_Aplicacao] [varchar](100) NOT NULL)" +
            " else truncate table #itemdecontroleTC " + insert + " " + mergestmt;

        console.log("Complete Stmt: " + stmt);

        log(stmt);
        db.query(stmt, function () { }, function (err, num_rows) {
            console.log("Realizou query!");

            userLog(stmt, "Insert de ICs ", 1, err);
            if (err) {
                console.log("Erro na sintaxe: " + err);
                ICAlert("Não foi possível realizar o cadastro da planilha, por favor solicite-o a suporte@versatec.com.br");
            } else {

                //console.log("End Step1: "+$scope.step1+" Step2: "+$scope.step2+" Scope3: "+$scope.step3);
                $scope.step1 = "completed";
                $scope.step2 = "completed";
                $scope.step3 = "completed";
                ICAlert("<strong>" + $scope.ICData.length + " IC's </strong> cadastrados.", true);
                //console.log("Done Step1: "+$scope.step1+" Step2: "+$scope.step2+" Scope3: "+$scope.step3);
                $("body").resize();
                /*$scope.$apply(function () {
                                            	
                                            	
                });*/
                /*setTimeout(function(){
                alerte("Dados cadastrados com sucesso nas aplicações "+$scope.aplicacoes.toString()+".", "Dados cadastrados");
                $scope.emptyData();	
                }, 1000);*/
            }
            console.log("" + num_rows);
        });


    };

    $scope.listaAplicacoes();

    /// Função responsável por carregar as funções quando o conteúdo estiver carregado
    $scope.$on('$viewContentLoaded', function () {
        //console.log("ICData: "+$scope.ICData+" Length: "+$scope.ICData.length);
        $view = $("#pag-ICs");


        carregaListaAplicacoes($view, false, $scope);
        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        //carregaRotas($view);

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-ICs");
        }
        //var files = document.getElementById('ICInput');
        if (files.addEventListener) files.addEventListener('change', handleFile, false);

    });
}

CtrlIC.$inject = ['$scope', '$globals'];
