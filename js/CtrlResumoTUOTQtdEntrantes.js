function CtrlResumoTUOTQtdEntrantes($scope, $globals) {
	
    win.title = "Detalhamento dos serviços por variáveis (Oi Total)"; //Fabio 28/05/2019
    $scope.versao = versao;

    //Alex 08/02/2014
    
    travaBotaoFiltro(0, $scope, "#pag-resumo-tuot-qtd-entrantes", "Quantidade de chamadas atendidas TU que poparam na operação (Entrantes)");

    $scope.dados = [];
    $scope.csv = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
   // $scope.segmentos = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;

    // Filtros
    $scope.filtros = {        
        empresas: [],
        produtos: []        
    };	
	//$scope.operador = false;

    function geraColunas() {
        var array = [];

        array.push(
            {
                field: "produto",
                displayName: "Produto TU",
                width: 150,
                pinned: true
            }, 
            {
                field: "codempresa",
                displayName: "Empresa",
                width: 150,
                pinned: true
            },
            {
                field: "dataHora",
                displayName: "Dia",
                width: 150,
                pinned: true
            },
            {
                field: "qtd",
                displayName: "Total",
                width: 150,
                pinned: true
            },
            {
                field: "perc",
                displayName: "%",
                width: 150,
                pinned: true
            }
        );

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-tuot-qtd-entrantes");


        //19/03/2014
        componenteDataHora($scope, $view);        
        carregaEmpresas($view);
        carregaProdutosTU($view,true);
		
        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {    
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {           
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-tuot-qtd-entrantes");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {          
  

            limpaProgressBar($scope, "#pag-resumo-tuot-qtd-entrantes");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.colunas = geraColunas();            
            $scope.listaDados.apply(this);
        });
        
    });


    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_produtotu = $view.find("select.filtro-produtotu").val() || [];

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }

        if (filtro_produtotu.length > 0) {
            $scope.filtros_usados += " Produtos TU: " + filtro_produtotu;
        }
    
        
        $scope.dados = [];
        $scope.csv = [];
        var stmt = "";
        
        
        
       
        var filtro_empresas_join = (filtro_empresas.length > 0) ? "''"+filtro_empresas.join("'',''")+"''" : '';
        var filtro_produtostu_join = (filtro_produtotu.length > 0 ) ? "''"+filtro_produtotu.join("'',''")+"''" : '';
        
        stmt = db2.use +   
        "Exec uspTUOiTotalQtdEntrantes" + 
        " @dataIni = '" + formataDataHora(data_ini) +
        "', @dataFim = '" + formataDataHora(data_fim) + "'" +
        (filtro_empresas_join ? ", @eps = '" + filtro_empresas_join + "'" : '')+
        (filtro_produtostu_join ? ", @produto = '" + filtro_produtostu_join + "'" : '');
          
        log(stmt);
      
        function executaQuery2(columns) {
            
            var data_hora = columns["DataHora"].value ? columns["DataHora"].value : undefined,
            produto = columns["TU_APPL_PRODUTO"].value ? columns["TU_APPL_PRODUTO"].value : undefined,
            codempresa = columns["EPS"].value ? columns["EPS"].value : undefined,            
            qtd = columns["totalep"].value.toString() ? columns["totalep"].value : undefined,
            perc= columns["PercTotal"].value.toString() ? columns["PercTotal"].value.toFixed(2) : undefined,
			datReferencia = formataDataBRString(data_hora);			
			
			
			var s = {                
                produto : produto,
                codempresa: codempresa,
                dataHora: datReferencia,                
                qtd: qtd,
                perc: perc
            }
					
            $scope.dados.push(s);

            var a = [];
            for (var item in $scope.colunas) {
                a.push(s[$scope.colunas[item].field]);
            }
            $scope.csv.push(a);
            
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }            
        }

        db2.query(stmt, executaQuery2, function (err, num_rows) {            
            num_rows = $scope.dados.length;
            console.log("Executando query-> " + stmt + " " + num_rows);
            var datFim = "";
            
            retornaStatusQuery(num_rows, $scope, datFim);
            $btn_gerar.button('reset');
            if ( num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
      
        });
        
    };

    // Exportar planilha XLSX
  $scope.exportaXLSX = function() {
	  
	  var $btn_exportar = $(this);
      $btn_exportar.button('loading');

      var colunas = [];
      for (i = 0; i < $scope.colunas.length; i++) {
          colunas.push($scope.colunas[i].displayName);
      }
	  
      var dadosExcel = [colunas].concat($scope.csv);
      var dadosComFiltro = [['Filtros:' + $scope.filtros_usados]].concat(dadosExcel);

      var ws = XLSX.utils.aoa_to_sheet(dadosComFiltro);
      var wb = XLSX.utils.book_new();
      var milis = new Date();
      var arquivoNome = 'ResumoTUOTServVariaveis' + formataDataHoraMilis(milis) + '.xlsx';
      XLSX.utils.book_append_sheet(wb, ws)
      console.log(wb);
      XLSX.writeFile(wb, tempDir3() + arquivoNome, {
          compression: true
      });
      console.log('Arquivo escrito');
      childProcess.exec(tempDir3() + arquivoNome);
	        setTimeout(function() {
          $btn_exportar.button('reset');
      }, 500);

  };

}

CtrlResumoTUOTQtdEntrantes.$inject = ['$scope', '$globals'];