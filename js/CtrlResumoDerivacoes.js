function CtrlResumoDerivacoes($scope, $globals) {

  win.title = "Resumo por Derivações";
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    travaBotaoFiltro(0, $scope, "#pag-resumo-der", "Resumo por Derivações");
    $scope.valor = "d";
    $scope.dados = [];
	$scope.xlsx = [];
	$scope.colunas = [{
            field: "cod_ic",
            displayName: "Código",
            width: 80,
            pinned: true
        },
        {
            field: "nome_ic",
            displayName: "Descrição",
            width: 300,
            pinned: true
        }
    ];

    $scope.columnDefs = [{
            headerName: 'Código',
            field: 'codIC'
        },
        {
            headerName: 'Descrição',
            field: 'descricao'
        }
    ];
	
	$scope.columnDefs = [{
            headerName: 'Código',
            field: 'codIC'
        },
        {
            headerName: 'Descrição',
            field: 'descricao'
        }
    ];


    $scope.localeGridText = {
        contains: 'Contém',
        notContains: 'Não contém',
        equals: 'Igual',
        notEquals: 'Diferente',
        startsWith: 'Começa com',
        endsWith: 'Termina com'
    }

    $scope.gridDados = {
        rowSelection: 'single',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        // onRowClicked: criarInstancia,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        },
        overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
        overlayNoRowsTemplate: '<span class="ag-overlay-loading-center">Nenhum dado foi encontrado.</span>',
        columnDefs: $scope.columnDefs,
        rowData: [],
        localeText: $scope.localeGridText
    };
	
    $scope.datas = [];
    $scope.totais = {};
    $scope.ordenacao = 'nome_ic';
    $scope.decrescente = true;
    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
	$scope.dddsSel = false;
	
	$scope.xlsxCabecalho = ["Descrição"]
    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };
    
    $scope.iit = false;

    $scope.ocultar = {
        totais_linha: false,
        totais_coluna: false
    };

    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        segmentos: [],
        sites: [],
		fontes: []
    };
	

    // Filtros: data e hora
    var agora = new Date();

    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
    };


    var $view;

    $scope.$on('$viewContentLoaded', function () {
		
		
        $view = $("#pag-resumo-der");
		$scope.gridDados.api.hideOverlay();
	
        //19/03/2014
        componenteDataMaisHora($scope, $view);

        //ALEX - Carregando filtros
        carregaAplicacoesGrupo($view,false,false,$scope,"Cadastro e Migração");
        //carregaAplicacoesComData($view);
        carregaSites($view);
        carregaDDDs($view,true);
        carregaFontes($view);    
		
		
		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});
		
        $view.on("change", "select.filtro-ddd", function () {
            Estalo.filtros.filtro_ddds = $(this).val() || [];
        });

        carregaSegmentosPorAplicacao($scope, $view, true);
        //GILBERTO - change de filtros

        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
          carregaSegmentosPorAplicacao($scope, $view);          
        });	
    

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        

        //Marcar todos
        // Marca todos os sites
        $view.on("click", "#alinkSite", function () { marcaTodosIndependente($('.filtro-site'), 'sites') });
		
        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope) });
		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});
  

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });


        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-derivacoes");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
;
				//$scope.colunas = geraColunas();				
                $scope.listaDados.apply(this);
          

        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });
		
		$view.on("click", ".btn-exportarxlsx-interface", function() {
          
            if ($scope.gridDados.rowData.length > 0) {
                separator = ";"
                colunas = "";
                file_name = tempDir3() + "export_" + Date.now();
                for (i = 0; i < $scope.colunas.length-1; i++) {
                    colunas = colunas + $scope.columnDefs[i].headerName + separator
                }
                colunas = colunas + "\n"

                for (var i = 0; i < $scope.gridDados.rowData.length; i++) {
                    for (var v in $scope.gridDados.rowData[i]) {
                        colunas = colunas + $scope.gridDados.rowData[i][v] + separator;
                    }
                    colunas = colunas + "\n";
                }
            
                $(".dropdown-exportar").toggle();
                exportaTXT(colunas, file_name, 'xlsx', true);
                $scope.xlsx = [];
            } else {
                $('.notification .ng-binding').text('Recurso indisponível.').selectpicker('refresh');
            }
        });

        // Limpa filtros
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
			
            componenteDataMaisHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-der");
        }

        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
		$('select.filtro-ddd').removeAttr( "disabled" ).selectpicker('refresh');
		$('#alinkDDD').removeAttr( "disabled" ).selectpicker('refresh');

    });

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;
		$scope.filtros_usados = "";
		
		var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_xlsx = $view.find(".btn-exportarXLSX");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("enable", true);
        $btn_exportar_xlsx.prop("disabled", true);
        $btn_exportar_dropdown.prop("enable", true);


        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
 

        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataBR($scope.periodo.inicio) + ""
        + " até " + formataDataBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
	   
        $scope.colunas = $scope.colunas.slice(0, 2);
        $scope.dados = [];
		$scope.xlsx = [];
        var stmt = "";
        $scope.ICs = [];
        $scope.datas = [];
        $scope.totais = { geral: { qtd_entrantes: 0 } };
		
		var sufTab = "";
		if(filtro_ddds.length > 0) sufTab = "DDD";
		
        var iit = "";
        if($scope.iit) iit = " - QtdTransfURA"
        
        var fator  =   $scope.valor === "m" ? "month" : "day";
        if ($scope.valor === "h") {
            dia = ' DatReferencia '
        }else{
            dia = " DATEADD("+fator+", DATEDIFF("+fator+", 0, DatReferencia), 0) "
        }
	  
			stmt +=  " SELECT cast(r.CodCombinacao  as varchar(20)) + '_' as Cod_Item ,"+dia+" ,"
			+ " SUM(QtdChamadas "+iit+") AS Qtd, Descricao"
			+ " FROM " + db.prefixo + "ResumoGenerico"+sufTab+" r"
			+ " join CriteriosConsolGenerica c on r.CodAplicacao = c.CodAplicacao and r.CodCombinacao = c.CodCombinacao"
			+ " WHERE 1 = 1 and CodProjeto = 'ResumoDerivacoes'"
			+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
            + " AND DatReferencia <= '" + formataDataHora(data_fim) + "'";
			stmt += restringe_consulta("r.CodAplicacao", filtro_aplicacoes, true);
			if (filtro_segmentos.length > 0) {
			  stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
			}
			stmt += restringe_consulta("DDD", filtro_ddds, true);
			stmt += restringe_consulta("CodSite", filtro_sites, true);
			stmt += " group by "+dia+", r.CodAplicacao, Descricao, r.CodCombinacao"
			+ " order by "+dia+", r.CodAplicacao, Descricao";




        log(stmt);

      
        function formataData2(data) {
			
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
            return y + _02d(m) + _02d(d);
        }

        function formataData3(data) {
			
            var y = data.getFullYear(),
              m = data.getMonth() + 1,
              d = data.getDate();
              h = data.getHours();
              m = data.getMinutes()
              s= data.getSeconds();
            return y + _02d(m) + _02d(d)+_02d(h)+m+s;
        }

        function executaQuery(columns) {
			
			if ($.isNumeric(columns[0].value) == false) {
				  var cod_ic = columns[0].value,
				  data = $scope.valor === "d" ||  $scope.valor === "m"? columns[1].value === null ? null : "_" + formataData2(columns[1].value) : columns[1].value === null ? null : "_" + formataData3(columns[1].value)
				  //data_BR = $scope.valor === "m" ? columns[1].value === null ? null : formataDataBR(columns[1].value,"mes") : formataDataBR3(columns[1].value)
                  if ($scope.valor === "m" && columns[1].value !== null){
                    data_BR = formataDataBR(columns[1].value,"mes")
                  }else if($scope.valor === "d" && columns[1].value !== null) 
                  {
                    data_BR = formataDataBR(columns[1].value,"dia")
                  }else if($scope.valor === "h" && columns[1].value !== null){
                    data_BR = formataDataBR3(columns[1].value)
                  }else if (columns[1].value === null){
                    data_BR = null
                  }
                
                  qtd_entrantes = +columns[2].value,
				  descricao = columns[3].value
                  
					if (data != null) {
						var d = $scope.datas[data];
						if (d === undefined) {
							d = { data: data, data_BR: data_BR };
							$scope.datas.push(d);
							$scope.datas[data] = d;
							$scope.totais[data] = { qtd_entrantes: 0 };
						}
					}

					var dado = $scope.dados[cod_ic];
					if (dado === undefined) {
						dado = {
							cod_ic: cod_ic,
							nome_ic: descricao,
							totais: { geral: { qtd_entrantes: 0 } }
						};
						$scope.dados.push(dado);
						$scope.dados[cod_ic] = dado;
					}

					if (data != null) {
						dado.totais[data] = { qtd_entrantes: qtd_entrantes };
						dado.totais.geral.qtd_entrantes += qtd_entrantes;
						$scope.totais[data].qtd_entrantes += qtd_entrantes;
						$scope.totais.geral.qtd_entrantes += qtd_entrantes;
					}

					
			}	
			
			if ($scope.dados.length % 1000 === 0) {
				$scope.$apply();
			}

        }

        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_xlsx.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
                $scope.gridDados.api.showNoRowsOverlay();
            } else {
                $scope.gridDados.api.hideOverlay();
            }

            $scope.datas.sort(function (a, b) {
                return a.data < b.data ? -1 : 1;
            });

            $scope.datas.forEach(function (d) {
                $scope.columnDefs.push({ headerName: d.data_BR, field: d.data });
            });

            $scope.columnDefs.push({ headerName: 'Total', field: 'dataTotal' });

            $scope.gridDados.api.setColumnDefs($scope.columnDefs);

            // Preencher com zero datas inexistentes em cada IC
            $scope.dados.forEach(function (dado) {
                linhaxlsx = [dado.nome_ic.replace(/(\n|\r)/g,' ').replace(/\&.+\;/g, '')]
                rowData = { descricao: dado.nome_ic.replace(/(\n|\r)/g,' ').replace(/\&.+\;/g, '') }

                $scope.datas.forEach(function (d) {
                    if (dado.totais[d.data] === undefined) {
                        dado.totais[d.data] = {
                            qtd_entrantes: 0
                        };
                        linhaxlsx.push(0);
                        rowData[d.data] = 0;
                    } else {
                        linhaxlsx.push(dado.totais[d.data].qtd_entrantes);
                        rowData[d.data] = dado.totais[d.data].qtd_entrantes;
                    }
                })
                linhaxlsx.push(dado.totais.geral.qtd_entrantes);
                rowData['dataTotal'] = dado.totais.geral.qtd_entrantes;
                $scope.xlsx.push(linhaxlsx);
                $scope.gridDados.rowData.push(rowData);
            });
            linhaxlsxTotais = ['', 'TOTAL']

            $scope.gridDados.api.setRowData($scope.gridDados.rowData);
            $scope.gridDados.api.refreshCells({force: true});

            $scope.datas.forEach(function (d) {
                if ($scope.xlsxCabecalho.indexOf(d.data_BR) == -1){
                    $scope.xlsxCabecalho.push(d.data_BR)
                }
                $scope.colunas.push({
                    field: "totais." + d.data + ".qtd_entrantes",
                    displayName: d.data_BR,
                    width: 100
                });
                linhaxlsxTotais.push($scope.totais[d.data].qtd_entrantes)
            });
            linhaxlsxTotais.push($scope.totais.geral.qtd_entrantes)
            $scope.xlsx.push(linhaxlsxTotais)
            $scope.colunas.push({
                field: "totais.geral.qtd_entrantes",
                displayName: "TOTAL",
                width: 100
            });
            $scope.dados.push({                
                nome_ic: "TOTAL",
                totais: $scope.totais
            });
            if ($scope.xlsxCabecalho.indexOf("TOTAL") == -1){
                $scope.xlsxCabecalho.push("TOTAL")
            }
            $scope.$apply();
        });


        $btn_gerar.button('reset');
        $btn_exportar.prop("disabled", false);
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function() {
	  
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho = $scope.datas.map(function (d) {
            return {
                value: d.data_BR,
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            };
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({
            value: "Descrição",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        // Inserir última coluna: total por linha
        cabecalho.push({
            value: "Total",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        var linhas = $scope.dados.map(function (dado) {
            var linha = $scope.datas.map(function (d) {
                try {
                    return {
                        value: dado.totais[d.data].qtd_entrantes || 0
                    };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            linha.unshift({
                value: dado.nome_ic
            });

            // Inserir última coluna: total por linha
            linha.push({
                value: dado.totais.geral.qtd_entrantes
            });

            return linha;
        });

        // Criar última linha, com os totais por data
        var linha_totais = $scope.datas.map(function (d) {
            try {
                return {
                    value: $scope.totais[d.data].qtd_entrantes || 0
                };
            } catch (ex) {
                return 0;
            }
        });

        // Inserir primeira coluna
        linha_totais.unshift({
            value: "Total",
            bold: 1
        });

        // Inserir última coluna: total geral
        linha_totais.push({
            value: $scope.totais.geral.qtd_entrantes
        });

        // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);

        // Inserir última linha: totais por data
        linhas.push(linha_totais);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: 'Resumo Derivação',
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoDerivacao_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}
CtrlResumoDerivacoes.$inject = ['$scope', '$globals'];
