function CtrlCriaGra($scope, $http, $globals) {

    win.title = "Cadastro de GRAs"; //Bruno 13/02/2017
    $scope.versao = versao;

    $scope.criaGraData = []; // Dados do grid 

    //Colunas do grid 
    $scope.colunas = [
        { field: "idgra", enableFiltering: true, displayName: "IDGRA", cellClass: "grid-align", width: '22,5%', pinned: false },//  cellClass:"span1", width: 80,
        { field: "gra", enableFiltering:true, displayName: "GRA", cellClass: "grid-align", width: '22,5%', pinned: false },//  cellClass:"span1", width: 80,
        { field: "ddd", enableFiltering: true, displayName: "DDD", cellClass: "grid-align", width: '22,5%', pinned: false },// cellClass:"span2",width: 80,
        { field: "uf", enableFiltering: true, displayName: "UF", cellClass: "grid-align", width: '22,5%', pinned: false },// cellClass:"span1", width: 150,
        { field: "edita", enableFiltering: false, displayName: "Edição", cellClass: "grid-align", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-view" ng-click="grid.appScope.Egra(row.entity)" title="Editar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];

    $scope.filtros = {
        filterText: ""
    };

    // Filtro 
    $scope.filterUser = function () {
        var filterText = $scope.userFilter;
        if (filterText !== '') {
            $scope.filtros.filterText = filterText;
        } else {
            $scope.filtros.filterText = '';
        }
    };
    // Fim do Filtro

    // Grid 
    $scope.gridDados = {
        data: $scope.criaGraData,
        columnDefs: $scope.colunas,
        enableColumnResize: false,
        enablePining: true,
        enableFiltering: true,
        exporterMenuPdf: false,
        filterOptions: $scope.filtros,
        enableGridMenu: true,
        enableSelectAll: true,
        exporterCsvFilename: 'Gra.csv',
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-CriaGra_Gra");
        treeView('#pag-CriaGra #chamadas', 15, 'Chamadas', 'dvchamadas');
        treeView('#pag-CriaGra #repetidas', 35, 'Repetidas', 'dvRepetidas');
        treeView('#pag-CriaGra #ed', 65, 'ED', 'dvED');
        treeView('#pag-CriaGra #ic', 95, 'IC', 'dvIC');
        treeView('#pag-CriaGra #tid', 115, 'TID', 'dvTid');
        treeView('#pag-CriaGra #vendas', 145, 'Vendas', 'dvVendas');
        treeView('#pag-CriaGra #falhas', 165, 'Falhas', 'dvFalhas');
        treeView('#pag-CriaGra #extratores', 195, 'Extratores', 'dvExtratores');
        treeView('#pag-CriaGra #parametros', 225, 'Parametros', 'dvParam');
        treeView('#pag-CriaGra #admin', 255, 'Administrativo', 'dvAdmin');
        treeView('#pag-CriaGra #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-CriaGra #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');

        $("#pag-CriaGra .botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });



        $view.find("select.filtro-rotas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Permissões',
            showSubtext: true
        });


        $("#pag-CriaGra #modal-header").on('hidden', function () {
            $("#pag-CriaGra .registerAlert").hide();
            $("#pag-CriaGra .registerAlertText").text("");
            $("#pag-CriaGra .container button").blur();
            console.log("Blurred buttons");
        });

        $("#pag-CriaGra #modal-view").on('hidden', function () {
            $("#pag-CriaGra .registerAlert").hide();
            $("#pag-CriaGra .registerAlertText").text("");
            $("#pag-CriaGra .container button").blur();
            console.log("Blurred buttons");
        });

        //preenchendo list com os relatorios

        var options2 = [];
        cache.relatorios.forEach(function (form) {

            options2.push('<option value="' + form.gra + '">');

        });

        $scope.listaGra();
    });



    function populateGraGrid(columns) {

        var
            gridIDGra = columns[0].value
            gridGra = columns[1].value,
            gridDDD = columns[2].value,
            gridUF = columns[3].value

        dado = {
            idgra: gridIDGra,
            gra: gridGra,
            ddd: gridDDD,
            uf: gridUF

        };

        $scope.criaGraData.push(dado);
        $scope.gridDados.data = $scope.criaGraData;
    }

  
    // Função responsável por popular o grid 
    $scope.listaGra = function () {

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');
        $scope.criaGraData = [];

        console.log("Listando Gras");

        // Fim da permissão responsavel por popular o grid 

        listaGra(populateGraGrid, $scope);

        $btn_gerar.button('reset');

    };

    // Função responsável por REGISTRAR  gra no banco de dados 

    $scope.registraCriaGra = function criaGra() {
        $scope.exist = false;

        function validForm() {
            $("#pag-CriaGra .registerAlertText").text("");
            if (
                (($scope.gra == "" || $scope.gra == undefined) ||
                ($scope.ddd == "" || $scope.ddd == undefined) ||
                ($scope.uf == "" || $scope.uf == undefined)
                )
                ) {
                var resultstring = "Favor preencha ";

                console.log("Valor de GRA: " + $scope.gra);
                console.log("Valor de DDD: " + $scope.ddd);
                console.log("Valor de UF: " + $scope.uf);

                if ($scope.gra == "" || $scope.gra == undefined) {
                    resultstring = resultstring + "gra, ";
                    $scope.gra = "";
                }
               
                if ($scope.ddd == "" || $scope.ddd == undefined) {
                    resultstring = resultstring + "DDD, ";
                    $scope.ddd = "";
                }
                if ($scope.uf == "" || $scope.uf == undefined) {
                    resultstring = resultstring + "UF";
                    $scope.uf = "";
                }

                resultstring = resultstring + " e tente novamente.";
                console.log("Error: " + resultstring);
                $("#pag-CriaGra .registerAlertText").text(resultstring);
                $("#pag-CriaGra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);
                return false;
            }

            return true;
        }



        console.log("Connected? " + db.isConnected());

        if (validForm()) {
            console.log("Teste de Repetido")
            
            checkLoginToRegisterCriaGra($scope.gra, $scope.ddd, $scope.uf, $scope);
            /*if (checkLoginToRegisterCriaGra($scope.gra, $scope.ddd, $scope.uf, $scope)) {
                console.log(" já existe no banco de dados " + $scope.gra);
                $("#pag-para_gra .registerAlertText").text(" já existe no banco de dados " + $scope.gra);
                $("#pag-para_gra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);
            } else {
                $("#pag-CriaGra #modal-gra-registro").modal('hide');
                $("#pag-CriaGra #modal-gra-registro input").val("");
            }*/

        } else {

        }

    };
    // Fim da função responsável por registrar  gra

    function listaGra(callBackGra, scopo) {
        array = [];
        pgra = "Select idgra,gra,ddd,uf From gras2";

        db.query(pgra, callBackGra, function (err, num_rows) {
            userLog(pgra, 'visualiza Gra' , 2 , err)
            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            retornaStatusQuery(num_rows, scopo);
            console.log("" + num_rows);
        });
    }

    /* Inserções do Bruno para função de Gra */
    function registrarGra(gra, ddd, uf, scope) {
      
      
        var stmt = db.use
        + "INSERT " + db.prefixo + "gras2 (gra,ddd,uf)"
        + " VALUES "
        + "('" + gra + "','" + ddd + "','" + uf + "')"


        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        // Cadastra o Gra
        db.query(stmt, function () { }, function (err, num_rows) {
            console.log("Executando query-> " + stmt);
            userLog(stmt, 'Registra Gra ' + gra, 1, err)
            if (err) {
                console.log("Erro ao registrar: " + err);
                $("#pag-CriaGra .registerAlertText").text("Erro ao registrar: " + err);
                $("#pag-CriaGra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);
            } else {
                console.log("Sem erros na db.query!");
                $("#pag-CriaGra .registerAlertText").text("GRA registrado com sucesso");
                $("#pag-CriaGra .registerAlert").show();
                document.getElementById("FormCriaGra").reset();

                if (scope.listaGra != undefined && scope.listaGra != "") {
                    console.log("Scopo Lista Gra");
                    scope.listaGra();
                }
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);
            }
        });
        // Fim do cadastra Gra

    }

    // Função utilizada para conferir se  existe no banco de dados
    function checkLoginToRegisterCriaGra( gra, ddd, uf, scope) {
        //console.log("Before replace: " + gra + " " + ddd + " " + uf);

        
        gra = replaceDiaritics(gra);
        ddd = replaceDiaritics(ddd);
        uf = replaceDiaritics(uf);

        //console.log("After replace:  " + gra + " " + ddd + " " + uf);


        //stmt = "Select count(*) from gras2 where GRA = '" + gra + "' and DDD = '" + ddd + "'and UF = '" + uf + "'";
        stmt = "Select * from gras2 where GRA = '" + gra + "' and DDD = '" + ddd + "'and UF = '" + uf + "'";
        var coluna = false;
        var executou = false;

        function getRetorno(columns) {
            var total = columns[0].value;
            //colunay = columns[1].value;
            executou = true;
            if (total > 0) {
                console.log("Este GRA já existe no banco de dados!!");
            }
        }

        db.query(stmt, getRetorno, function (err, num_rows) {
            
            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            if (num_rows >= 1) {
                coluna = true;
                console.log("Existe no Banco **** " + gra + " Rows: " + num_rows);
                //notificate("Parâmetro Gra já cadastrado.", scope);
                $("#pag-CriaGra .registerAlertText").text("Já existe um GRA cadastrado com o valor : " + gra + "");
                $("#pag-CriaGra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);

            }

            if (num_rows == 0) {
                console.log("Não existe no banco " + gra + " Rows: " + num_rows);
                registrarGra(gra, ddd, uf, scope);
                $("#pag-CriaGra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-CriaGra .registerAlert").hide();
                }, 3000);
            }

            console.log("Valor de retorno: " + coluna);
        });
    }
    // Fim da função utilizada para conferir se login existe no banco de dados

    // Inicio da função responsável por salvar a edição de gra
    $scope.row = ""; // Variável responsável pela linha em edição no array de gra
    $scope.Egra = function log (row) {
        $scope.exist = false;
       
        console.log("Edita Gra Valores: " + row + " Row: " + row);
        //var editidgra = row.idgra;
        //var editgra = row.gra;
        //var editddd = row.ddd;
        //var edituf = row.uf;

        $scope.editidgra = row.idgra;
        $scope.editgra = row.gra;
        $scope.editddd = row.ddd;
        $scope.edituf = row.uf;

        console.log("IDGra: " + $scope.editidgra);
        console.log("Gra: " + $scope.editgra);
        console.log("DDD: " + $scope.editddd);
        console.log("Uf: " + $scope.edituf);
        $scope.row = row;
      

    
    };
 
    $scope.verifGRA = function () {
        console.log('Verificação de Repetidos');
        $("#pag-CriaGra .registerAlertText").text("");
        if (
            (($scope.editidgra == "" || $scope.editidgra == undefined) ||
            ($scope.editgra == "" || $scope.editgra == undefined) ||
             ($scope.editddd == "" || $scope.editddd == undefined) ||
            ($scope.edituf == "" || $scope.edituf == undefined)
            )
            ) {
            var resultstring = "Favor preencha ";

            console.log("Valor de GRA: " + $scope.editgra);
            console.log("Valor de DDD: " + $scope.editddd);
            console.log("Valor de UF: " + $scope.edituf);


            if ($scope.editgra == "" || $scope.editgra == undefined) {
                resultstring = resultstring + "GRA, ";
                $scope.editgra = "";
            }
            
            if ($scope.editddd == "" || $scope.editddd == undefined) {
                resultstring = resultstring + "DDD, ";
                $scope.editddd = "";
            }
            if ($scope.edituf == "" || $scope.edituf == undefined) {
                resultstring = resultstring + "UF, ";
                $scope.edituf = "";
            }

            resultstring = resultstring + " e tente novamente.";
            console.log("Error: " + resultstring);
            $("#pag-CriaGra .registerAlertText").text(resultstring);
            $("#pag-CriaGra .registerAlert").show();
            setTimeout(function () {
                $("#pag-CriaGra .registerAlert").hide();
            }, 3000);
            return false;
        }
        return true;
    }

    $scope.Edgra = function () {
        row = $scope.row;
        $scope.exist = false;

        var editidgra = row.idgra;
        var editgra = row.gra;
        var editddd = row.ddd;
        var edituf = row.uf;

        var stmt = db.use
+ "UPDATE " + db.prefixo + "gras2 SET gra= '" + $scope.editgra + "', ddd='" + $scope.editddd + "',  uf='" + $scope.edituf + "' WHERE idgra='" + editidgra + "'and  gra='" + editgra + "'and  ddd='" + editddd + "'and  uf='" + edituf + "'";

        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        if ($scope.verifGRA()) {
            //edita o Gra
            db.query(stmt, function () { }, function (err, num_rows) {
                console.log("Executando query-> " + stmt);
                userLog(stmt, 'Atualiza Gra' + editgra, 3, err)
                if (err) {
                    console.log("Erro ao editar: " + err);
                    $("#pag-CriaGra .registerAlertText").text("Erro ao editar: " + err);
                    $("#pag-CriaGra .registerAlert").show();
                    setTimeout(function () {
                        $("#pag-CriaGra .registerAlert").hide();
                    }, 3000);
                } else {
                    console.log("Sem erros na db.query!");
                    $("#pag-CriaGra .registerAlertText").text("GRA editado com sucesso");
                    $("#pag-CriaGra .registerAlert").show();

                    if ($scope.listaGra != undefined && $scope.listaGra != "") {
                        console.log("Scopo Lista Gra");
                        $scope.listaGra();
                    }
                    setTimeout(function () {
                        $("#pag-CriaGra .registerAlert").hide();
                    }, 3000);
                }
            });


            // Fim do edita Gra
        }
    }
}
///// Fim Bruno
CtrlCriaGra.$inject = ['$scope', '$http', '$globals'];
