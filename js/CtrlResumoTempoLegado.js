/*
 * CtrlResumoTempoLegado
 */
function CtrlResumoTempoLegado($scope, $globals) {

    //Alex 10/05/2014 Andamento da extração
    intervaloExtratorStatus = undefined;
    intervaloExtratorStatusAux = undefined;

    intervaloExtratorStatusAux = setInterval(function () {
        if (extratorStatus !== "") {
            $scope.status_extrator = extratorStatus;
            $scope.$apply();
        }
    }, 500);

    win.title = "Resumo de Tempos Legados"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;



    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoVS"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-legado", "Resumo de Tempos Legados");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    //$scope.limite_registros = 3000;

    $scope.vendas = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "vendas",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };


    $scope.ordenacao = ['data_hora', 'legado'];
    $scope.decrescente = false;

    $scope.extrator = false;
    $scope.status_extrator;

    $scope.log = [];





    //filtros_usados
    $scope.filtros_usados = "";


    $scope.filtro_aplicacoes = [];

    $scope.valor = "legado";
    $scope.viewFormat = null;

    // Filtro: incentivo
    $scope.filtro_legados = [];

    /*cache.produtos_vendas.indice = geraIndice(cache.produtos_vendas);
    cache.grupos_produtos_vendas.indice = geraIndice(cache.grupos_produtos_vendas);*/


    $scope.aba = 2;


    function geraColunas() {
        var array = [];


        array.push({ field: "data_hora_BR", displayName: "Data", width: 200, pinned: false },
                   { field: "nome", displayName: "Variável", width: 250, pinned: false },
                   { field: "max", displayName: "Tempo Max", width: 150, pinned: false },
                   { field: "medio", displayName: "Tempo Médio", width: 150, pinned: false},
                   { field: "min", displayName: "Tempo Min", width: 150, pinned: false }

                  );
        return array;
    }


    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = hoje();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = horaAnterior();
    Estalo.filtros.filtro_legados = [];

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: mesAnterior(dat_consoli),
        max: dat_consoli*/
    };

    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-resumo-legado");
      treeView('#pag-resumo-legado #chamadas', 15,'Chamadas','dvchamadas');
      treeView('#pag-resumo-legado #repetidas', 35,'Repetidas','dvRepetidas');
      treeView('#pag-resumo-legado #ed', 65,'ED','dvED');
      treeView('#pag-resumo-legado #ic', 95,'IC','dvIC');
      treeView('#pag-resumo-legado #tid', 115,'TID','dvTid');
      treeView('#pag-resumo-legado #vendas', 145,'Vendas','dvVendas');
      treeView('#pag-resumo-legado #falhas', 165,'Falhas','dvFalhas');
      treeView('#pag-resumo-legado #extratores', 195,'Extratores','dvExtratores');
      treeView('#pag-resumo-legado #parametros', 225,'Parametros','dvParam');
      treeView('#pag-resumo-legado #admin', 255,'Administrativo','dvAdmin');
	      treeView('#pag-resumo-legado #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-pag-resumo-legado #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');
		treeView('#uracadastro', 325, 'Cadastro e Migração', 'dvCadastro');

        /*$('.nav.aba3').css('display','none');
        $('.nav.aba4').css('display','none');
        $('.nav.aba5').css('display','none');*/

        $(".aba3").css({ 'position': 'fixed', 'left': '60px', 'top': '40px' });
        $(".aba4").css({ 'position': 'fixed', 'left': '377px', 'top': '40px' });
        $(".botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });


        //19/03/2014
        componenteDataHora($scope, $view);

        carregaAplicacoes($view, false, true, $scope);
        carregaSegmentosPorAplicacao($scope, $view, true);
        $view.on("change", "select.filtro-aplicacao", function () { carregaSegmentosPorAplicacao($scope, $view) });

        carregaLegados($view, true);
        // Popula lista de  grupo de produtos a partir das aplicações selecionadas
        //$view.on("change", "select.filtro-aplicacao", function () { carregaGruposProdutoPorAplicacao($scope, $view) });

        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        $view.on("change", "select.filtro-legado", function () {
            Estalo.filtros.filtro_legados = $(this).val();
        });

        //2014-11-27 transição de abas
        var abas = [2, 3, 4, 5];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }


        // Marca todos os produtos
        $view.on("click", "#alinkLeg", function () { marcaTodosIndependente($('.filtro-legado'), 'legados') });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () { marcaTodosIndependente($('.filtro-segmento'), 'segmentos') });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () { marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope) });






        /*  $view.find("select.filtro-regiao").selectpicker({
        selectedTextFormat: 'count',
        countSelectedText: '{0} regiÃµes',
        showSubtext: true
        });*/


        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });


        $view.find("select.filtro-legado").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} legados'
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {
            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });





        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-legado').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-legado .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-legado').addClass('open');
                $('div.btn-group.filtro-legado>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-legado').mouseout(function () {
            $('div.btn-group.filtro-legado').removeClass('open');
        });




        // Lista vendas conforme filtros
        $view.on("click", ".btn-gerar", function () {
            limpaProgressBar($scope, "#pag-resumo-legado");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });

        $view.on("click", ".btn-baixarT", function () {

            limpaProgressBar($scope, "#pag-resumo-legado");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }
            if (_02d(data_ini.getMonth() + 1) !== _02d(data_fim.getMonth() + 1)) {
                setTimeout(function () {
                    atualizaInfo($scope, "Operação válida somente para o mesmo mês.");
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }

            $scope.consultaConsolidadoT.apply(this);
        });

        $view.on("click", ".btn-baixarB", function () {
            limpaProgressBar($scope, "#pag-resumo-legado");
            $scope.consultaConsolidadoB.apply(this);
        });

        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, "venda");

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        //Alex 02/04/2014
        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }


        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-legado");
        }

        $view.find(".iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });






    // Lista dados conforme filtros
    $scope.listaDados = function () {

        $globals.numeroDeRegistros = 0;

        $scope.filtros_usados = "";
        //if (!connection) {
        //  db.connect(config, $scope.listaVendas);
        //  return;
        //}

        var $btn_exportar = $view.find(".btn-exportar");
        $btn_exportar.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var aplicacao = $scope.aplicacao;

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_legados = $view.find("select.filtro-legado").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
        + " até " + formataDataHoraBR(dataConsoliReal(data_fim));
        if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
        if (filtro_legados.length > 0) { $scope.filtros_usados += " Variáveis: " + filtro_legados; }
        if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }


        $scope.vendas = [];
        var stmt = "";
        //var executaQuery = "";
        var sufixo = "";
        var campo;

        if ($('#chkDia').prop("checked")) {
            campo = "convert(date,r.dataHora)";
        }else{
            campo = "r.dataHora";
        }


        stmt = db.use + "select " + campo + ",v.nomeVariavel,max(r.tempoMax),sum(r.tempoTotal)/sum(QtdAcessos) as tempoMedio,min(r.tempoMin)"
            + " from ResumoTemposLegado R  "
            + " inner join VariavelCalllog2 V on v.codVariavel = r.codVariavel"
            + " where r.dataHora >= '" + formataDataHora(data_ini) + "'"
            + " AND r.dataHora <= '" + formataDataHora(dataConsoliReal(data_fim)) + "'";

            stmt += restringe_consulta("r.codVariavel", filtro_legados, true)
            stmt += restringe_consulta("r.codaplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("r.codsegmento", filtro_segmentos, true)


            stmt += " group by " + campo + ",v.nomeVariavel ";
            stmt += " order by " + campo ;



            log(stmt);

        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }

        function executaQuery(columns) {

            var datahora = $('#chkDia').prop("checked") ? formataDataBRString(columns[0].value) : formataDataHoraBR(columns[0].value),
                nome = columns[1].value,
                max = toFixed (+columns[2].value,3).replace(",","."),  //valor = toFixed(columns[2].value, 2), //Não ordena
                medio = toFixed(+columns[3].value, 3).replace(",", "."),
                min = toFixed(+columns[4].value, 3).replace(",", ".");


            $scope.vendas.push({
                data_hora_BR: datahora,
                nome : nome,
                max: max,
                medio: medio,
                min: min
            });

            console.log($scope.vendas);
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.vendas.length,$scope,"#pag-resumo-vendas");
            //console.log($scope.vendas.length+" "+$globals.numeroDeRegistros);
            if ($scope.vendas.length % 1000 === 0) {
                $scope.$apply();
            }

        }



        //db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
        //console.log("Executando query-> "+stmtCountRows+" "+$globals.numeroDeRegistros);
        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log(err);
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');



            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
            }
        });
        //});

        // GILBERTOOOOOO 17/03/2014



        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });

    };


    /*// Consulta consolidado resumo venda TEMPLATE
    $scope.consultaConsolidadoT = function () {

        var $btn_baixarT = $(this);
        $btn_baixarT.button('loading');

        var data_ini = $scope.periodo.inicio,
        data_fim = $scope.periodo.fim;

        var vendasDetalhada= [];
        var stmt = db.use + "SELECT GrupoProduto, CodAplicacao,DatReferencia, CodDDD, PU.CodProduto, PU.Descricao, Resultado, codErro,"
        + " ValorVendido, QtdVendas from " + db.prefixo + "ResumoVendaProduto as RVP"
        + " left outer join " + db.prefixo + "ProdutosURA as PU on RVP.CodProduto = PU.CodProduto WHERE 1 = 1"
        + " AND RVP.DatReferencia >= '" + formataDataHora(data_ini) + "'"
        + " AND RVP.DatReferencia <= '" + formataDataHora(data_fim) + "'"
        + " order by GrupoProduto ASC";

        log(stmt);
        $scope.info_status = "Aguarde esta consulta pode demorar alguns minutos.";
                    $scope.$apply(function () {
                    });

        db.query(stmt, function (columns) {
            var codAplicacao = columns[0].value,
                datReferencia = formataDataBR(columns[1].value),
                codDDD = columns[2].value,
                codProduto = columns[3].value,
                descricao = columns[4].value,
                grupoProduto = columns[5].value,
                resultado = columns[6].value,
                codErro = columns[7].value,
                valorVendido = columns[8].value,
                qtdVendas = columns[9].value;

            vendasDetalhada.push({
                codAplicacao: codAplicacao,
                datReferencia: datReferencia,
                codDDD: codDDD,
                codProduto: codProduto,
                descricao: descricao,
                grupoProduto: grupoProduto,
                resultado: resultado,
                codErro: codErro,
                valorVendido: valorVendido,
                qtdVendas: qtdVendas
                });

        }, function (err, num_rows) {
            $btn_baixarT.button('reset');
            retornaStatusQuery(num_rows,$scope);
            $scope.info_status += " Processando arquivo, aguarde...";
                    $scope.$apply(function () {
                    });

            var file = 'resumoVendas_'+formataData($scope.periodo.inicio)+'.xlsx';

            var newData;

            fs.readFile('templates/tResumoVS2.xlsx', function(err, data) {

            // Create a template
            var t = new XlsxTemplate(data);
                // Perform substitution
                t.substitute(1, {

                    planDados: vendasDetalhada,

                });
                // Get binary data
                newData = t.generate();
                $scope.info_status += " Concluído.";
                $scope.$apply(function () {
                });

                regraParaGerarBinario(file,newData,$scope,0);

            });

        });
        return true;

    };*/










    /*// Consulta consolidado resumo venda BD
    $scope.consultaConsolidadoB = function () {


        var $btn_baixarB = $(this);
        $btn_baixarB.button('loading');

        var data_ini = $scope.periodo.inicio;

        var file = "";


        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM "+ db.prefixo + "ArquivoRelatorios"
        + " WHERE NomeRelatorio='VENDAS'";
        //+ " AND CONVERT(VARCHAR(25), DataAtualizacao, 126) LIKE '" + formataData(data_ini) + "%'";
        log(stmt);

        var emissor = new (require("events").EventEmitter);

        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;
          var milis = new Date();

          file = 'resumoVendas_'+formataData(dataAtualizacao)+'_gerado_'+formataDataHoraMilis(milis)+'.xlsx';

          var arqDb = toArrayBuffer(arquivo);

          emissor.on('fim', function(){
            var buffer = toBuffer(arqDb);
            regraParaGerarBinario(file,buffer,$scope,5000);
            atualizaProgressBar(arquivo.length, arquivo.length,$scope,"#pag-resumo-vendas");
          });



        }, function (err, num_rows) {

            $scope.filtros_usados = "";
            $btn_baixarB.button('reset');
            retornaStatusQuery(num_rows,$scope);
            emissor.emit('fim');
        });
        return true;
    };*/

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var template = "tResumoTempoLegado";


        //Alex 15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios"
             + " WHERE NomeRelatorio='" + template + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = 'tResumoTempoLegado.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = 'resumoTempoLegado_' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.vendas
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
          
             userLog(stmt, 'Exportar XLSX', 2, err)
        });

    };
    /*$scope.celula = function (cod_status) {
        return (['success', 'danger', 'warning'])[cod_status];
    };*/
}
CtrlResumoTempoLegado.$inject = ['$scope', '$globals'];
