function CtrlChamadasCTGNaoCruzadas2($scope, $globals) {


    win.title = "Detalhamento de chamadas CTG não cruzadas com ECH"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-chamadas-ctg-naocruzadas", "Detalhamento de chamadas CTG não cruzadas com ECH");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;

    //$scope.limite_registros = 500;

    $scope.dados = [];
    $scope.csv = [];
    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };
    //$scope.ordenacao = 'data_hora';
    //$scope.decrescente = true;

    $scope.log = [];
    $scope.aplicacoes = []; // FIXME: copy
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.ordenacao = ['DATA_HORA_INICIO_PASSO1'];
    $scope.decrescente = false;
    $scope.iit = false;
    $scope.parcial = false;


    // Filtros
    $scope.filtros = {
        aplicacoes: [],
        // ALEX - 29/01/2013
        sites: [],
        segmentos: [],
        isHFiltroED: false,
        porDia: false,
        porDDD: false
    };

    // Filtro: ddd
    $scope.ddds = cache.ddds;

    $scope.porDDD = null;

    //02/06/2014 Flag
    $scope.porRegEDDD = $globals.agrupar;

    $scope.aba = 2;


    var empresas = cache.empresas.map(function (a) {
        return a.codigo
    });

    $scope.valor = "empresa";

    //Alex 02/08/2017
    $scope.intervalo = false;
    var p = cache.aclbotoes.map(function (b) {
        return b.idview;
    }).indexOf('pag-chamadas-ctg-naocruzadas');
    if (p >= 0) {
        var teste = cache.aclbotoes[p].permissoes.split(',');
        for (var i = 0; i < teste.length; i++) {
            if (teste[i] === 'intervalo') {
                $scope.intervalo = true;
            }
        }
    }



    function geraColunas() {
        var array = [];



        array.push({
            field: "DataHora_Inicio",
            displayName: "Data hora (URA)",
            width: 150,
            pinnable: false
        });

        array.push({
                field: "CodAplicacao",
                displayName: "Aplicação",
                width: 120,
                pinnable: false
            }, {
                field: "CodSegmento",
                displayName: "Segmento",
                width: 75,
                pinnable: false
            }, {
                field: "NumANI",
                displayName: "Chamador",
                width: 100,
                pinnable: false
            }, {
                field: "ClienteTratado",
                displayName: "Tratado",
                width: 100,
                pinnable: false
            }, {
                field: "UltimoED",
                displayName: "Último estado",
                width: 280,
                pinnable: false
            }, {
                field: "TransferID",
                displayName: "TransferID",
                width: 280,
                pinnable: false
            }, {
                field: "CodRegra",
                displayName: "Regra",
                width: 280,
                pinnable: false
            }, {
                field: "CtxID",
                displayName: "CTXID",
                width: 280,
                pinnable: false
            }, {
                field: "CodEmpresa",
                displayName: "Empresa",
                width: 280,
                pinnable: false
            }, {
                field: "Assunto",
                displayName: "Assunto",
                width: 120,
                pinnable: false
            }

        );

        return array;
    }

    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: ontem(dat_consoli),
        max: dat_consoli*/
    };

    var $view;


    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-chamadas-ctg-naocruzadas");


        $(".aba2").css({
            'position': 'fixed',
            'left': '47px',
            'top': '42px'
        });


        $(".aba4").css({
            'position': 'fixed',
            'left': '750px',
            'top': '40px'
        });
        $(".aba5").css({
            'position': 'fixed',
            'left': '55px',
            'right': 'auto',
            'margin-top': '35px',
            'z-index': '1'
        });
        $('.navbar-inner').css('height', '70px');
        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });

        //minuteStep: 5

        //19/03/2014
        componenteDataHora($scope, $view);

        carregaRegioes($view);
        carregaAplicacoes($view, false, false, $scope);
        carregaSites($view);
        carregaEmpresas($view);
        carregaRegras($view);




        carregaSegmentosPorAplicacao($scope, $view, true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });


        carregaDDDsPorAplicacao($scope, $view, true);
        // Popula lista de  DDDs a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaDDDsPorAplicacao($scope, $view)
        });


        $view.on("change", "input:radio[name=radioB]", function () {

            if ($(this).val() === "empresa") {
                $('#porDDD').attr('disabled', 'disabled');
                $('#porDDD').prop('checked', false);
            } else {
                $('#porDDD').attr('disabled', false);
            }

        });



        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });


        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-ddd").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} DDDs',
            showSubtext: true
        });

        $view.find("select.filtro-regiao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} regiões',
            showSubtext: true
        });

        $view.find("select.filtro-transferid").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} transferids'
        });

        $view.find("select.filtro-ed").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} EDs'
        });

        $view.find("select.filtro-edstransf").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} EDs'
        });


        $view.find("select.filtro-empresa").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Empresas'
        });

        $view.find("select.filtro-regra").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Regras'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

        $view.on("change", "select.filtro-ddd", function () {
            var filtro_ddds = $(this).val() || [];
            Estalo.filtros.filtro_ddds = filtro_ddds;
        });

        $view.on("change", "select.filtro-regiao", function () {
            var filtro_regioes = $(this).val() || [];
            Estalo.filtros.filtro_regioes = filtro_regioes;
        });

        //GILBERTOO - change de segmentos
        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });

        //2014-11-27 transição de abas
        var abas = [2, 3, 4];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }


        // Marca todos os ddds
        $view.on("click", "#alinkDDD", function () {
            marcaTodosIndependente($('.filtro-ddd'), 'ddds')
        });

        //Bernardo 20-02-2014 Marcar todas as regiões
        $view.on("click", "#alinkReg", function () {
            marcaTodasRegioes($('.filtro-regiao'), $view, $scope)
        });


        // Marca todos os edstransf
        $view.on("click", "#alinkTid", function () {
            marcaTodosIndependente($('.filtro-transferid'), 'transferid')
        });

        // Marca todos os edstransf
        $view.on("click", "#alinkEDT", function () {
            marcaTodosIndependente($('.filtro-edstransf'), 'edstransf')
        });

        // Marca todos os sites
        $view.on("click", "#alinkSite", function () {
            marcaTodosIndependente($('.filtro-site'), 'sites')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        // Marca todos os empresas
        $view.on("click", "#alinkEmp", function () {
            marcaTodosIndependente($('.filtro-empresa'), 'empresa')
        });


        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });


        // GILBERTO 18/02/2014



        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.dropdown-menu.open').css({
                    'margin-left': '0px'
                });
                $('div.btn-group.filtro-ddd>div>ul').css({
                    'max-height': '600px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.btn-group.filtro-ddd').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.data-inicio.input-append.date").data("datetimepicker").hide();
            $("div.data-fim.input-append.date").data("datetimepicker").hide();
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseover(function () {
            $('div.btn-group.filtro-operacao').addClass('open');
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-operacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseout(function () {
            $('div.btn-group.filtro-operacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-transferid').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-transferid').addClass('open');
                $('div.dropdown-menu.open').css({
                    'margin-left': '-230px'
                });
                $('div.btn-group.filtro-transferid>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-transferid').mouseout(function () {
            $('div.btn-group.filtro-transferid').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-edstransf').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-edstransf').addClass('open');
                $('div.dropdown-menu.open').css({
                    'margin-left': '0px'
                });
                $('div.btn-group.filtro-edstransf>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-edstransf').mouseout(function () {
            $('div.btn-group.filtro-edstransf').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {

            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.dropdown-menu.open').css({
                    'margin-left': '0px'
                });
                $('div.btn-group.filtro-segmento>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ddd').addClass('open');
                $('div.dropdown-menu.open').css({
                    'margin-left': '-110px'
                });
                $('div.btn-group.filtro-ddd>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '400px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-ddd').removeClass('open');
        });



        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-regiao').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
            $('div.btn-group.filtro-regiao').removeClass('open');
        });


        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
            $('div.dropdown-menu.open').css({
                'margin-left': '0px'
            });
            $('div.btn-group.filtro-empresa').addClass('open');

        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
            $('div.btn-group.filtro-empresa').removeClass('open');

        });





        // EXIBIR AO PASSAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ed').addClass('open');
                $('div.btn-group.filtro-ed>div>ul').css({
                    'max-width': '500px',
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }
        });



        // OCULTAR AO TIRAR O MOUSE estado de dialogo
        $('div.btn-group.filtro-ed').mouseout(function () {
            $('div.btn-group.filtro-ed').removeClass('open');
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.porRegEDDD = false;
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {
            iniciaFiltros();
            componenteDataHora($scope, $view, true);

            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash +'/';
            }, 500);
        }

        // Botão agora Alex 03/03/2014
        $view.on("click", ".btn-agora", function () {
            $scope.agora.apply(this);
        });

        $scope.agora = function () {
            iniciaAgora($view, $scope);
        }

        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-chamadas-ctg-naocruzadas");
        }

        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {


            limpaProgressBar($scope, "#pag-chamadas-ctg-naocruzadas");
            //22/03/2014 Testa se data início é maior que a data fim
            var data_ini = $scope.periodo.inicio,
                data_fim = $scope.periodo.fim;
            var testedata = testeDataMaisHora(data_ini, data_fim);
            if (testedata !== "") {
                setTimeout(function () {
                    atualizaInfo($scope, testedata);
                    effectNotification();
                    $view.find(".btn-gerar").button('reset');
                }, 500);
                return;
            }



            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });







        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });


    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };








    // Lista chamadas conforme filtros
    $scope.listaDados = function () {


        var datasComDados = [];


        $globals.numeroDeRegistros = 0;



        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.fim;

        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];

        if (filtro_aplicacoes.length === 0) {
            atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
            $btn_gerar.button('reset');
            return;
        }


        var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
        var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
        var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
        var original_ddds = $view.find("select.filtro-ddd").val() || [];
        var filtro_edt = garanteVetor($(".filtro-edstransf").val());
        if (filtro_edt.length > 0) {
            filtro_edt = filtro_edt.join(",").split(",");
        }
        var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
        var filtro_regras = $view.find("select.filtro-regra").val() || [];

        var filtro_transferid = $view.find("select.filtro-transferid").val() || [];


        //filtros usados
        $scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + "" +
            " até " + formataDataHoraBR($scope.periodo.fim);
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }
        if (filtro_ddds.length > 0) {
            $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }
        if (filtro_edt.length > 0) {
            $scope.filtros_usados += " EDsTransf: " + filtro_edt;
        }
        if (filtro_empresas.length > 0) {
            $scope.filtros_usados += " Empresas: " + filtro_empresas;
        }
        if (filtro_regras.length > 0) {
            $scope.filtros_usados += " Regras: " + filtro_regras;
        }
        if (filtro_transferid.length > 0) {
            $scope.filtros_transferid += " TransferId: " + filtro_transferid;
        }


        if (filtro_regioes.length > 0 && filtro_ddds.length === 0) {
            var options = [];
            var v = [];
            filtro_regioes.forEach(function (codigo) {
                v = v.concat(unique2(cache.ddds.map(function (ddd) {
                    if (ddd.regiao === codigo) {
                        return ddd.codigo
                    }
                })) || []);
            });
            unique(v).forEach((function (filtro_ddds) {
                return function (codigo) {
                    var ddd = cache.ddds.indice[codigo];
                    filtro_ddds.push(codigo);
                };
            })(filtro_ddds));
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
        } else if (filtro_regioes.length > 0 && filtro_ddds.length !== 0) {
            $scope.filtros_usados += " Regiões: " + filtro_regioes;
            $scope.filtros_usados += " DDDs: " + filtro_ddds;
        }


        $scope.dados = [];
        var stmt = "";
        var executaQuery = "";
        $scope.datas = [];
        $scope.totais = {
            geral: {
                qtd_entrantes: 0
            }
        };


        empSelect = "";
        empSelectIn = "";
        empObjParts = "";
        $scope.QtdCONTAX = 0;
        $scope.QtdAEC = 0;
        $scope.QtdBTCC = 0;



        if ($scope.valor === "empresa") {

            for (var i = 0; i < empresas.length; i++) {
                empSelect += "[" + empresas[i] + "] as " + empresas[i] + ","
            }
            for (var i = 0; i < empresas.length; i++) {
                empSelectIn += "[" + empresas[i] + "],"
            }
            for (var i = 0; i < empresas.length; i++) {
                empObjParts += empresas[i] + ":" + empresas[i] + ","
            }

            empSelect = empSelect.substring(0, empSelect.length - 1);
            empSelectIn = empSelectIn.substring(0, empSelectIn.length - 1);
            empObjParts = empObjParts.substring(0, empObjParts.length - 1);

            var tabelaConsultar = "NaoCruzamentoECH2";

            // CONSULTA SQL 1.1
            stmt = db.use +
                " SELECT DataHora_Inicio,CodAplicacao,CodSegmento,NumANI,ClienteTratado,ED.Nom_Estado AS UltimoED" +
                " ,TransferID,CtxID,CodRegra,CodEmpresa,Assunto" +
                " FROM " + tabelaConsultar + " AS RE" +
                " LEFT OUTER JOIN ESTADO_DIALOGO AS ED" +
                " ON RE.UltimoED = ED.Cod_Estado AND RE.CodAplicacao =  ED.Cod_Aplicacao" +
                " WHERE 1 = 1" +
                " AND DataHora_Inicio >= '" + formataDataHora(data_ini) + "'" +
                " AND DataHora_Inicio <= '" + formataDataHora(data_fim) + "'"
            stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
            stmt += restringe_consulta("TransferId", filtro_transferid, true)
            stmt += restringe_consulta("CodSite", filtro_sites, true)
            stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
            stmt += restringe_consulta("UltimoED", filtro_edt, true)
            stmt += " ORDER BY DataHora_Inicio ASC";


        }



        try {
            executaQuery = executaQuery2;
            log(stmt);
        } catch (err) {
            console.log(err);
        }

        function formataData2(data) {
            var y = data.getFullYear(),
                m = data.getMonth() + 1,
                d = data.getDate();
            return y + _02d(m) + _02d(d);
        }


        $scope.QtdChamadasTotal = 0;
        $scope.vetorQtdChamadas = [];

        var objTransferencias = {};


        var stmtCountRows = stmtContaLinhasChamadasCTGNaoCruzadas(stmt);



        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {

            datasComDados.push(columns[1].value);
            //if(datainicio_mod === "") datainicio_mod = columns[1].value +":00:00";
            //datafim_mod = columns[1].value +":59:59";
            //console.log(columns[1].value);
            $globals.numeroDeRegistros = columns[0].value;
        }


        function executaQuery2(columns) {
            var objColunasTabela = {};
            // GERA DADOS COLUNAS
            // DatReferencia = columns["DatReferencia"] !== undefined ? columns["DatReferencia"].value : undefined,
            // NumPassos = columns["NumPassos"] !== undefined ? columns["NumPassos"].value : undefined,
            // QtdChamadas = columns["QtdChamadas"] !== undefined ? columns["QtdChamadas"].value : undefined;
            // QtdAEC = columns["AeC"] !== undefined ? columns["AeC"].value : undefined;
            // QtdBTCC = columns["BTCC"] !== undefined ? columns["BTCC"].value : undefined;
            // QtdCONTAX = columns["CONTAX"] !== undefined ? columns["CONTAX"].value : undefined;

            // DETALHAMENTO INICIO
            //objColunasTabela["DATA_HORA_INICIO_PASSO1"] = columns["DATA_HORA_INICIO_PASSO1"] !== undefined ? columns["DATA_HORA_INICIO_PASSO1"].value : undefined;
            objColunasTabela["DataHora_Inicio"] = columns["DataHora_Inicio"] !== undefined ? columns["DataHora_Inicio"].value : undefined;
            //objColunasTabela["intervalo"] =  (new Date(objColunasTabela["DATA_HORA_INICIO_PASSO1"]) - new Date(objColunasTabela["DataHora_Inicio"]))/1000;
            objColunasTabela["CodAplicacao"] = columns["CodAplicacao"] !== undefined ? columns["CodAplicacao"].value : undefined;
            //objColunasTabela["CodSite"] = columns["CodSite"] !== undefined ? columns["CodSite"].value : undefined;
            objColunasTabela["UltimoED"] = columns["UltimoED"] !== undefined ? columns["UltimoED"].value : undefined;
            //objColunasTabela["UltimoED"] = obtemNomeEstado(objColunasTabela["CodAplicacao"],objColunasTabela["UltimoED"]);
            objColunasTabela["CodAplicacao"] = obtemNomeAplicacao(objColunasTabela["CodAplicacao"]);
            objColunasTabela["CodSegmento"] = columns["CodSegmento"] !== undefined ? columns["CodSegmento"].value : undefined;
            objColunasTabela["CodSegmento"] = obtemNomeSegmento(objColunasTabela["CodSegmento"]);
            objColunasTabela["NumANI"] = columns["NumANI"] !== undefined ? columns["NumANI"].value : undefined;
            objColunasTabela["ClienteTratado"] = columns["ClienteTratado"] !== undefined ? columns["ClienteTratado"].value : undefined;
            objColunasTabela["TransferID"] = columns["TransferID"] !== undefined ? columns["TransferID"].value : undefined;
            objColunasTabela["CtxID"] = columns["CtxID"] !== undefined ? columns["CtxID"].value : undefined;
            objColunasTabela["CodEmpresa"] = columns["CodEmpresa"] !== undefined ? columns["CodEmpresa"].value : undefined;
            //objColunasTabela["CodRegra"] = columns["CodRegra"] !== undefined ? columns["CodRegra"].value : undefined;
            //objColunasTabela["UCID"] = columns["UCID"] !== undefined ? columns["UCID"].value : undefined;
            //objColunasTabela["IDCONTATO"] = columns["IDCONTATO"] !== undefined ? columns["IDCONTATO"].value : undefined;
            //objColunasTabela["TEL_DIGITADO"] = columns["TEL_DIGITADO"] !== undefined ? columns["TEL_DIGITADO"].value : undefined;
            //objColunasTabela["TEL_BINADO"] = columns["TEL_BINADO"] !== undefined ? columns["TEL_BINADO"].value : undefined;
            //objColunasTabela["CLITRATADO"] = columns["CLITRATADO"] !== undefined ? columns["CLITRATADO"].value : undefined;
            //objColunasTabela["CHAM_ATENDIDA"] = columns["CHAM_ATENDIDA"] !== undefined ? columns["CHAM_ATENDIDA"].value : undefined;
            // objColunasTabela["CHAM_ABAND"] = columns["CHAM_ABAND"] !== undefined ? columns["CHAM_ABAND"].value : undefined;

            //objColunasTabela["DATA_HORA_FIM_PASSO1"] = columns["DATA_HORA_FIM_PASSO1"] !== undefined ? columns["DATA_HORA_FIM_PASSO1"].value : undefined;
            //objColunasTabela["DURACAO_PASSO1"] = columns["DURACAO_PASSO1"] !== undefined ? columns["DURACAO_PASSO1"].value : undefined;
            //objColunasTabela["TEMPO_FILA_PASSO1"] = columns["TEMPO_FILA_PASSO1"] !== undefined ? columns["TEMPO_FILA_PASSO1"].value : undefined;
            //objColunasTabela["MATRICULA_AGENTE_PASSO1"] = columns["MATRICULA_AGENTE_PASSO1"] !== undefined ? columns["MATRICULA_AGENTE_PASSO1"].value : undefined;
            //objColunasTabela["COD_SKILL_PASSO1"] = columns["COD_SKILL_PASSO1"] !== undefined ? columns["COD_SKILL_PASSO1"].value : undefined;
            //objColunasTabela["ID_ECADOP_PASSO1"] = columns["ID_ECADOP_PASSO1"] !== undefined ? columns["ID_ECADOP_PASSO1"].value : undefined;
            // objColunasTabela["DAC_PASSO1"] = columns["DAC_PASSO1"] !== undefined ? columns["DAC_PASSO1"].value : undefined;
            //objColunasTabela["OPERACAO_FINAL_PASSO1"] = columns["OPERACAO_FINAL_PASSO1"] !== undefined ? columns["OPERACAO_FINAL_PASSO1"].value : undefined;
            // objColunasTabela["EMPRESA_PASSO1"] = columns["EMPRESA_PASSO1"] !== undefined ? columns["EMPRESA_PASSO1"].value : undefined;
            objColunasTabela["CodRegra"] = columns["CodRegra"] !== undefined ? columns["CodRegra"].value : undefined;
            objColunasTabela["Assunto"] = columns["Assunto"] !== undefined ? columns["Assunto"].value : undefined;
            //objColunasTabela["SessaoRecVoz"] = columns["SessaoRecVoz"] !== undefined ? columns["SessaoRecVoz"].value : undefined;
            // DETALHAMENTO FIM PT1


            // PARSE JSON OBJ TRANSFERENCIAS
            //objColunasTabela["Transferencias"] = columns["Transferencias"] !== undefined ? columns["Transferencias"].value : undefined;
            //console.log(objColunasTabela["Transferencias"]);

            // FIM PARSE JSON OBJ TRANSFERENCIAS
            // DETALHAMENTO FIM PT2

            // $scope.vetorQtdChamadas.push(QtdChamadas);
            // $scope.QtdChamadasTotal += QtdChamadas;

            //if (!$scope.filtros.porDia) { objColunasTabela["DATA_HORA_INICIO_PASSO1"] = formataDataHoraBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]);} else{DatReferencia = formataDataBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]); }
            //if (!$scope.filtros.porDia) { objColunasTabela["DATA_HORA_FIM_PASSO1"] = formataDataHoraBR(objColunasTabela["DATA_HORA_FIM_PASSO1"]);} else{DatReferencia = formataDataBR(objColunasTabela["DATA_HORA_FIM_PASSO1"]); }

            if (!$scope.filtros.porDia) {
                objColunasTabela["DataHora_Inicio"] = formataDataHoraBR(objColunasTabela["DataHora_Inicio"]);
            } else {
                DatReferencia = formataDataBR(objColunasTabela["DataHora_Inicio"]);
            }
            //if (!$scope.filtros.porDia) { objColunasTabela["DATA_HORA_INICIO_PASSO1"] = formataDataHoraBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]);} else{DatReferencia = formataDataBR(objColunasTabela["DATA_HORA_INICIO_PASSO1"]); }



            //DADOS PRO SCOPE
            $scope.dados.push(objColunasTabela);

            var a = [];
			for (var item in objColunasTabela) a.push(objColunasTabela[item]);
			$scope.csv.push(a);

            // PercentTotal :PercentTotal

            //FIM DADOS PRO SCOPE


            // FIM GERA DADOS COLUNAS
            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }
        }


        //13/06/2014 Query de hora em hora
        //var stmts = arrayHorasQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);
        //13/06/2014 Query de 5 em 5 minutos
        var stmts = arrayCincoMinutosQuery(stmt, $scope.periodo.inicio, $scope.periodo.fim);


        //13/06/2014 Data final do penúltimo array com base na hora final do filtro, último array é uma flag
        //stmts[stmts.length -2] = [stmts[stmts.length -2].toString().replace(formataDataHora(precisaoMinutoFim(data_fim)),formataDataHora(data_fim))];
        stmts[stmts.length - 2] = [stmts[stmts.length - 2].toString().replace(/DATA_HORA_INICIO_PASSO1 <= \'.+\' /g, "DATA_HORA_INICIO_PASSO1 <= \'" + formataDataHora(new Date(data_fim)) + "\' ")];



        var controle = 0;
        var total = 0;

        function proximaHora(stmt) {


            var pularQuery = false;

            for (var i = 0; i < datasComDados.length; i++) {
                var d = new RegExp('DataHora_Inicio >= \'' + datasComDados[i] + '(.*?)\'');
                if (stmt.match(d) === null) {
                    pularQuery = true;
                } else {
                    pularQuery = false;
                    datasComDados.splice(0, i - 1);
                    break;
                }
            }

            function comOuSemDados(err, c) {
                $('.notification .ng-binding').text("Aguarde... " + atualizaProgressExtrator(controle, stmts.length) + " Encontrados: " + total).selectpicker('refresh');
                controle++;
                $scope.$apply();

                //Executa dbquery enquanto array de querys menor que length-1
                if (controle < stmts.length - 1) {
                    proximaHora(stmts[controle].toString());
                    if (c === undefined) console.log(controle);
                }

                //Evento fim
                if (controle === stmts.length - 1) {
                    if (c === undefined) console.log("fim " + controle);
                    if (err) {
                        retornaStatusQuery(undefined, $scope);
                    } else {
                        retornaStatusQuery($scope.dados.length, $scope);
                    }

                    $btn_gerar.button('reset');
                    if ($scope.dados.length > 0) {
                        $btn_exportar.prop("disabled", false);
                        $btn_exportar_csv.prop("disabled", false);
                        $btn_exportar_dropdown.prop("disabled", false);
                    } else {
                        $btn_exportar.prop("disabled", true);
                        $btn_exportar_csv.prop("disabled", true);
                        $btn_exportar_dropdown.prop("disabled", true);
                    }
                }
            }


            if (pularQuery) {
                comOuSemDados(false, "console");
            } else {

                //console.log("uararrr ->"+stmt);

                db.query(stmt, executaQuery, function (err, num_rows) {
                    console.log("Executando query-> " + stmt + " " + num_rows);
                    num_rows !== undefined ? total += num_rows : total += 0;
                    comOuSemDados(err);

                });
            }
        }



        db.query(stmtCountRows, contaLinhas, function (err, num_rows) {
            console.log("Executando query conta linhas -> " + stmtCountRows + " " + $globals.numeroDeRegistros);

            if ($globals.numeroDeRegistros === 0) {
                retornaStatusQuery(0, $scope);
                $btn_gerar.button('reset');
            } else {
                //Disparo inicial
                proximaHora(stmts[0].toString());
            }

        });





        //});

        // GILBERTOOOOOO 17/03/2014
        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };

    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {
        var $btn_exportar = $(this);
        $btn_exportar.button('loading');
        var linhas = [];



        linhas.push([

            {
                value: "Data hora (URA)",
                hAlign: 'center'
            },
            {
                value: "Aplicação",
                hAlign: 'center'
            },
            {
                value: "Segmento",
                hAlign: 'center'
            },
            {
                value: "Chamador",
                hAlign: 'center'
            },
            {
                value: "Tratado",
                hAlign: 'center'
            },
            {
                value: "último estado",
                hAlign: 'center'
            },
            {
                value: "TransferID",
                hAlign: 'center'
            },
            //Edson vetou
            {
                value: "CtxID",
                hAlign: 'center'
            },
            {
                value: "CodEmpresa",
                hAlign: 'center'
            },
            {
                value: "Regra",
                hAlign: 'center'
            },
            {
                value: "Assunto",
                hAlign: 'center'
            }
            //{ value: "UCID",hAlign:'center'},
            // { value: "UCID______________________" ,hAlign:'center'},
            //{ value: "IDCONTATO"  ,hAlign:'center'},
            //Edson vetou
            //{ value: "TEL_BINADO"  ,hAlign:'center'},
            //{ value: "TEL_DIGITADO"  ,hAlign:'center'},
            //{ value: "CLITRATADO"  ,hAlign:'center'},
            //{ value: "CHAM_ATENDIDA"  ,hAlign:'center'},
            //{ value: "CHAM_ABAND"  ,hAlign:'center'},

            //{ value: "DATA_HORA_FIM_PASSO1"  ,hAlign:'center'},
            //{ value: "DURACAO_PASSO1"  ,hAlign:'center'},
            //{ value: "TEMPO_FILA_PASSO1"  ,hAlign:'center'},
            //{ value: "MATRICULA_AGENTE_PASSO1"  ,hAlign:'center'},
            //{ value: "COD_SKILL_PASSO1"  ,hAlign:'center'},
            //{ value: "ID_ECADOP_PASSO1"  ,hAlign:'center'},
            //{ value: "DAC_PASSO1"  ,hAlign:'center'},



        ]);
        $scope.dados.map(function (dado) {



            return linhas.push([

                {
                    value: dado["DataHora_Inicio"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["CodAplicacao"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["CodSegmento"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["NumANI"],
                    autoWidth: true,
                    formatCode: '0',
                    hAlign: 'center'
                },
                {
                    value: dado["ClienteTratado"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["UltimoED"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["TransferID"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                //Edson vetou
                {
                    value: dado["CtxID"],
                    autoWidth: true,
                    formatCode: '0',
                    hAlign: 'center'
                },
                {
                    value: dado["CodEmpresa"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["CodRegra"],
                    autoWidth: true,
                    hAlign: 'center'
                },
                {
                    value: dado["Assunto"],
                    autoWidth: true,
                    hAlign: 'center'
                }
                //{ value:dado["UCID"],   autoWidth:true, formatCode: '0',hAlign:'center',  autoWidth:true },
                //{ value:dado["IDCONTATO"],   autoWidth:true, formatCode: '0' ,hAlign:'center' },
                //Edson vetou
                //{ value:dado["TEL_BINADO"],   autoWidth:true, formatCode: '0' ,hAlign:'center' },
                //{ value:dado["TEL_DIGITADO"],   autoWidth:true, formatCode: '0' ,hAlign:'center' },
                //{ value:dado["CLITRATADO"],   autoWidth:true , formatCode: '0' ,hAlign:'center'},
                //{ value:dado["CHAM_ATENDIDA"],   autoWidth:true,hAlign:'center' },
                // { value:dado["CHAM_ABAND"],   autoWidth:true,hAlign:'center' },

                //{ value:dado["DATA_HORA_FIM_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["DURACAO_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["TEMPO_FILA_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["MATRICULA_AGENTE_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["COD_SKILL_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["ID_ECADOP_PASSO1"],   autoWidth:true,hAlign:'center' },
                //{ value:dado["DAC_PASSO1"],   autoWidth:true,hAlign:'center' },


            ]);
        });





        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: 'Detalhamento',
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'detalhamentodechamadasCTGNaoCruzadas_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };

}

CtrlChamadasCTGNaoCruzadas2.$inject = ['$scope', '$globals'];