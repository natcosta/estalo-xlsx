var changeFalhas = false;

function CtrlResumoFalhas($scope, $globals) {

    win.title = "Resumo de Falhas por Servidor"; //Alex 27/02/2014
    $scope.versao = versao;

    $scope.rotas = rotas;

    $scope.limite_registros = 0;
    $scope.incrementoRegistrosExibidos = 100;
    $scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

    //Alex 08/02/2014
    //var teste_data = dataUltimaConsolidacao("ResumoDD"); //$scope, relatorio
    travaBotaoFiltro(0, $scope, "#pag-resumo-falhas", "Resumo de Falhas");

    //Alex 24/02/2014
    $scope.status_progress_bar = 0;


    $scope.dados = [];
    $scope.csv = [];

    $scope.colunas = [];
    $scope.gridDados = {
        data: "dados",
        columnDefs: "colunas",
        enableColumnResize: true,
        enablePinning: true
    };

    $scope.log = [];
    $scope.aplicacoes = [];
    $scope.sites = [];
    $scope.segmentos = [];
    $scope.uras = [];
    $scope.falhas = [];
    $scope.ordenacao = ['data_hora'];
    $scope.decrescente = false;

    // Filtros
    // criando o objeto filtros
    $scope.filtros = {
        aplicacoes: [],
        uras: [],
        falhas: [],
        porDia: false,
        sites: [],
        segmentos: []
    };


    $scope.aba = 2;


    function geraColunas() {
        var array = [];


        array.push({
            field: "hostname",
            displayName: "Hostname",
            width: 130,
            pinned: true
        }, {
            field: "hora0",
            displayName: "0",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora1",
            displayName: "1",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora2",
            displayName: "2",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora3",
            displayName: "3",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora4",
            displayName: "4",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora5",
            displayName: "5",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora6",
            displayName: "6",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora7",
            displayName: "7",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora8",
            displayName: "8",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora9",
            displayName: "9",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora10",
            displayName: "10",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora11",
            displayName: "11",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora12",
            displayName: "12",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora13",
            displayName: "13",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora14",
            displayName: "14",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora15",
            displayName: "15",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora16",
            displayName: "16",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora17",
            displayName: "17",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora18",
            displayName: "18",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora19",
            displayName: "19",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora20",
            displayName: "20",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora21",
            displayName: "21",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora22",
            displayName: "22",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "hora23",
            displayName: "23",
            width: 45,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        }, {
            field: "total",
            displayName: "Total",
            width: 55,
            pinned: true,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.getProperty(col.field) | number}}</span></div>'
        });


        return array;
    }



    // Filtros: data e hora
    var agora = new Date();

    //var dat_consoli = new Date(teste_data);



    //var dat_consoli = new Date();

    //Alex 03/03/2014
    var inicio, fim;
    Estalo.filtros.filtro_data_hora[0] !== undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
    Estalo.filtros.filtro_data_hora[1] !== undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

    $scope.periodo = {
        inicio: inicio,
        fim: fim,
        min: new Date(2013, 11, 1),
        max: agora // FIXME: atualizar ao virar o dia
        /*min: ontem(dat_consoli),
        max: dat_consoli*/
    };
    console.log('data' + $scope.periodo.inicio);


    var $view;
    //define que o div principal carregara a página resumo-falhas
    $scope.$on('$viewContentLoaded', function () {
        $view = $("#pag-resumo-falhas");



        $(".botoes").css({
            'position': 'fixed',
            'left': 'auto',
            'right': '25px',
            'margin-top': '35px'
        });


        //19/03/2014
        componenteDataMaisHora($scope, $view, true);


        carregaAplicacoes($view, false, false, $scope);
        carregaSites($view);
        carregaFalhas($view,true);
        carregaUras($view,true);
        carregaSegmentosPorAplicacao($scope, $view, true);
        // Popula lista de segmentos a partir das aplicações selecionadas
        $view.on("change", "select.filtro-aplicacao", function () {
            carregaSegmentosPorAplicacao($scope, $view)
        });

        /*$view.find(".selectpicker").selectpicker({
        //noneSelectedText: 'Nenhum item selecionado',
        countSelectedText: '{0} itens selecionados'
        });*/

        $view.find("select.filtro-aplicacao").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} aplicações',
            showSubtext: true
        });

        $view.find("select.filtro-site").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Sites/POPs',
            showSubtext: true
        });

        $view.find("select.filtro-segmento").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} segmentos'
        });

        $view.find("select.filtro-falhas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Falhas'
        });

        $view.find("select.filtro-ura").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Uras'
        });

        $view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });



        $view.on("change", "select.filtro-falhas", function () {

            changeFalhas = true;
            var filtro_falhas = $(this).val() || [];
            Estalo.filtros.filtro_falhas = filtro_falhas;


        });

        $("#pag-resumo-falhas").on("click", ".filtro-falhas div.dropdown-menu.open ul li", function () {

            if (changeFalhas === false) {
                Estalo.filtros.filtro_falhas = [];
                $('select.filtro-falhas').val('').selectpicker('refresh');
            }
            changeFalhas = false;

        });


        //GILBERTO - change de uras
        $view.on("change", "select.filtro-ura", function () {
            var filtro_uras = $(this).val() || [];
            Estalo.filtros.filtro_uras = filtro_uras;
        });

        //2014-11-27 transição de abas
        var abas = [2, 3];

        $view.on("click", "#alinkAnt", function () {

            if ($scope.aba === abas[0]) $scope.aba = abas[abas.length - 1] + 1;
            if ($scope.aba > abas[0]) {
                $scope.aba--;
                mudancaDeAba();
            }
        });

        $view.on("click", "#alinkPro", function () {

            if ($scope.aba === abas[abas.length - 1]) $scope.aba = abas[0] - 1;

            if ($scope.aba < abas[abas.length - 1]) {
                $scope.aba++;
                mudancaDeAba();
            }

        });

        function mudancaDeAba() {
            abas.forEach(function (a) {
                if ($scope.aba === a) {
                    $('.nav.aba' + a + '').fadeIn(500);
                } else {
                    $('.nav.aba' + a + '').css('display', 'none');
                }
            });
        }


        // Marca todos os sites
        $view.on("click", "#alinkSite", function () {
            marcaTodosIndependente($('.filtro-site'), 'sites')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkSeg", function () {
            marcaTodosIndependente($('.filtro-segmento'), 'segmentos')
        });

        // Marca todos os segmentos
        $view.on("click", "#alinkUra", function () {
            marcaTodosIndependente($('.filtro-ura'), 'uras')
        });

        //Marcar todas aplicações
        $view.on("click", "#alinkApl", function () {
            marcaTodasAplicacoes($('.filtro-aplicacao'), $view, $scope)
        });


        $view.on("change", "select.filtro-segmento", function () {
            Estalo.filtros.filtro_segmentos = $(this).val();
        });


        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

            $("div.datahora-inicio.input-append.date").data("datetimepicker").hide();
            $('div.btn-group.filtro-aplicacao').addClass('open');
            $('div.btn-group.filtro-aplicacao>div>ul').css({
                'max-height': '500px',
                'overflow-y': 'auto',
                'min-height': '1px',
                'max-width': '350px'
            });
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
            $('div.btn-group.filtro-aplicacao').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div > ul > li >> div> div.btn-group.filtro-segmento').mouseover(function () {
            if (!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-segmento').addClass('open');
                $('div.btn-group.filtro-segmento>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div > ul > li >> div> div.btn-group.filtro-segmento').mouseout(function () {
            $('div.btn-group.filtro-segmento').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-ura').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-ura .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-ura').addClass('open');
                $('div.btn-group.filtro-ura>div>ul').css({
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px',
                    'max-width': '350px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-ura').mouseout(function () {
            $('div.btn-group.filtro-ura').removeClass('open');
        });

        // EXIBIR AO PASSAR O MOUSE falhas
        $('div.btn-group.filtro-falhas').mouseover(function () {
            //11/07/2014 não mostrar filtros desabilitados
            if (!$('div.btn-group.filtro-falhas .btn').hasClass('disabled')) {
                $('div.btn-group.filtro-falhas').addClass('open');
                $('div.btn-group.filtro-falhas>div>ul').css({
                    'max-width': '500px',
                    'max-height': '500px',
                    'overflow-y': 'auto',
                    'min-height': '1px'
                });
            }
        });

        // OCULTAR AO TIRAR O MOUSE Falhas
        $('div.btn-group.filtro-falhas').mouseout(function () {
            $('div.btn-group.filtro-falhas').removeClass('open');
        });



        // Lista chamadas conforme filtros
        $view.on("click", ".btn-gerar", function () {

            limpaProgressBar($scope, "#pag-resumo-falhas");
            //22/03/2014 Testa se data início é maior que a data fim
            /*var testedata = testeDataMaisHora($scope.periodo.inicio,$scope.periodo.fim);
              if(testedata!==""){
                  setTimeout(function(){
                      atualizaInfo($scope,testedata);
                      effectNotification();
                      $view.find(".btn-gerar").button('reset');
                  },500);
                  return;
              }*/
            $scope.colunas = geraColunas();
            $scope.listaDados.apply(this);
        });


        $view.on("click", ".btn-exportar", function () {
            $scope.exportaXLSX.apply(this);
        });

        // Limpa filtros Alex 03/03/2014
        $view.on("click", ".btn-limpar-filtros", function () {
            $scope.limparFiltros.apply(this);
        });

        $scope.limparFiltros = function () {

            iniciaFiltros();
            componenteDataHora($scope, $view, true);


            var partsPath = window.location.pathname.split("/");
            var part = partsPath[partsPath.length - 1];

            var $btn_limpar = $view.find(".btn-limpar-filtros");
            $btn_limpar.prop("disabled", true);
            $btn_limpar.button('loading');

            setTimeout(function () {
                $btn_limpar.button('reset');
                //window.location.href = part + window.location.hash + '/';
            }, 500);
        }



        // Botão abortar Alex 23/05/2014
        $view.on("click", ".abortar", function () {
            $scope.abortar.apply(this);
        });

        //Alex 23/05/2014
        $scope.abortar = function () {
            abortar($scope, "#pag-resumo-falhas");
        }




        $view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
    });

    // Exibe mais registros
    $scope.exibeMaisRegistros = function () {
        $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
    };

    // Lista chamadas conforme filtros
    $scope.listaDados = function () {



        $globals.numeroDeRegistros = 0;

        var $btn_exportar = $view.find(".btn-exportar");
        var $btn_exportar_csv = $view.find(".btn-exportarCSV");
        var $btn_exportar_dropdown = $view.find(".btn-exportar-dropdown-form");
        $btn_exportar.prop("disabled", true);
        $btn_exportar_csv.prop("disabled", true);
        $btn_exportar_dropdown.prop("disabled", true);

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');

        var data_ini = $scope.periodo.inicio,
            data_fim = $scope.periodo.inicio;


        var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
        var filtro_sites = $view.find("select.filtro-site").val() || [];
        var filtro_uras = $view.find("select.filtro-ura").val() || [];
        var filtro_falhas = $view.find("select.filtro-falhas").val() || [];
        var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];

        if (filtro_aplicacoes.length === 0) {
            atualizaInfo($scope, '<font color = "white">Selecione uma aplicação.</font>');
            $btn_gerar.button('reset');
            return;
        }
        if (filtro_falhas === "Falhas") {
            atualizaInfo($scope, '<font color = "white">Selecione um tipo de falha.</font>');
            $btn_gerar.button('reset');
            return;
        }



        //filtros usados
        $scope.filtros_usados = " Período: " + formataData($scope.periodo.inicio) + "" +
            " até " + formataData($scope.periodo.fim).replace("00:00:00", "23:59:59");
        if (filtro_aplicacoes.length > 0) {
            $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes;
        }
        if (filtro_sites.length > 0) {
            $scope.filtros_usados += " Sites: " + filtro_sites;
        }
        if (filtro_uras.length > 0) {
            $scope.filtros_usados += " Uras: " + filtro_uras;
        }
        if (filtro_falhas != "Falhas") {
            $scope.filtros_usados += " Falha: " + filtro_falhas;
        }
        if (filtro_segmentos.length > 0) {
            $scope.filtros_usados += " Segmentos: " + filtro_segmentos;
        }



        $scope.dados = [];
        $scope.csv = [];



        //02/06/2014 Visão por ddd(s) ou por região(ões)
        // Apenas fazer o join com clientes únicos se sites e segmentos forem todos selecionados,
        // e aplicações forem uma ou todas selecionadas


        // Nenhuma aplicação selecionada (= todas as aplicações)
        // Exibir total de clientes únicos idependente de aplicação (CodAplicacao = '')

        var tipoFalha, tipoServ;
        if ($scope.visaoUra) {
            tipoServ = "'U'"
        } else {
            tipoServ = "'A'"
        }
        switch (filtro_falhas) {
            case "ErroAp":
                tipoFalha = "Sum(QtdErroAplic - QtdErroAplicIIT)";
                break;
            case "ErroCtg":
                tipoFalha = "Sum(QtdErroConting - QtdErroContingIIT)";
                break;
            case "SemPmt":
                tipoFalha = "Sum(QtdSemPrompt - QtdSemPromptIIT)";
                break;
            case "SemDig":
                tipoFalha = "Sum(QtdSemDig - QtdSemDigIIT)";
                break;
            case "ErroVendas":
                tipoFalha = "Sum(QtdErroVendas - QtdErroVendasIIT)";
                break;
        }

        var stmt = db.use

        stmt += "with tab1 as (" +
            "SELECT Hostname," +
            "case when DATEPART(hour,DatReferencia) = 0 then " + tipoFalha + " else 0 end as hora0," +
            "case when DATEPART(hour,DatReferencia) = 1 then " + tipoFalha + " else 0 end as hora1," +
            "case when DATEPART(hour,DatReferencia) = 2 then " + tipoFalha + " else 0 end as hora2," +
            "case when DATEPART(hour,DatReferencia) = 3 then " + tipoFalha + " else 0 end as hora3," +
            "case when DATEPART(hour,DatReferencia) = 4 then " + tipoFalha + " else 0 end as hora4," +
            "case when DATEPART(hour,DatReferencia) = 5 then " + tipoFalha + " else 0 end as hora5," +
            "case when DATEPART(hour,DatReferencia) = 6 then " + tipoFalha + " else 0 end as hora6," +
            "case when DATEPART(hour,DatReferencia) = 7 then " + tipoFalha + " else 0 end as hora7," +
            "case when DATEPART(hour,DatReferencia) = 8 then " + tipoFalha + " else 0 end as hora8," +
            "case when DATEPART(hour,DatReferencia) = 9 then " + tipoFalha + " else 0 end as hora9," +
            "case when DATEPART(hour,DatReferencia) = 10 then " + tipoFalha + " else 0 end as hora10," +
            "case when DATEPART(hour,DatReferencia) = 11 then " + tipoFalha + " else 0 end as hora11," +
            "case when DATEPART(hour,DatReferencia) = 12 then " + tipoFalha + " else 0 end as hora12," +
            "case when DATEPART(hour,DatReferencia) = 13 then " + tipoFalha + " else 0 end as hora13," +
            "case when DATEPART(hour,DatReferencia) = 14 then " + tipoFalha + " else 0 end as hora14," +
            "case when DATEPART(hour,DatReferencia) = 15 then " + tipoFalha + " else 0 end as hora15," +
            "case when DATEPART(hour,DatReferencia) = 16 then " + tipoFalha + " else 0 end as hora16," +
            "case when DATEPART(hour,DatReferencia) = 17 then " + tipoFalha + " else 0 end as hora17," +
            "case when DATEPART(hour,DatReferencia) = 18 then " + tipoFalha + " else 0 end as hora18," +
            "case when DATEPART(hour,DatReferencia) = 19 then " + tipoFalha + " else 0 end as hora19," +
            "case when DATEPART(hour,DatReferencia) = 20 then " + tipoFalha + " else 0 end as hora20," +
            "case when DATEPART(hour,DatReferencia) = 21 then " + tipoFalha + " else 0 end as hora21," +
            "case when DATEPART(hour,DatReferencia) = 22 then " + tipoFalha + " else 0 end as hora22," +
            "case when DATEPART(hour,DatReferencia) = 23 then " + tipoFalha + " else 0 end as hora23," +
            "" + tipoFalha + " as total " +
            "FROM ResumoFalhas2 WHERE 1 = 1 AND TipoServidor = " + tipoServ + " "

            +
            "AND DatReferencia >= '" + formataData(data_ini) + "' AND DatReferencia <= '" + formataDataHora(precisaoHoraFim(data_fim)) + "'";
        stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
        //stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
        //stmt += restringe_consulta("Cod_Site", filtro_sites, true);
        stmt += " GROUP BY Hostname, DatReferencia"
        //+ "), tab2 as("
        //+ "select distinct Hostname FROM ResumoFalhas2 WHERE 1 = 1 AND TipoServidor = " + tipoServ + " "
        //+ "AND DatReferencia >= '" + formataDataHora(data_ini) + "' AND DatReferencia <= '" + formataDataHora(data_fim).replace("00:00:00", "23:59:59") + "'";
        //      stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true);
        //stmt += restringe_consulta("CodSegmento", filtro_segmentos, true);
        //stmt += restringe_consulta("Cod_Site", filtro_sites, true);
        stmt += ") SELECT tab1.hostname,max(hora0) as hora0,max(hora1) as hora1,max(hora2) as hora2,max(hora3) as hora3,max(hora4) as hora4," +
            "max(hora5) as hora5,max(hora6) as hora6,max(hora7) as hora7,max(hora8) as hora8,max(hora9) as hora9," +
            "max(hora10) as hora10,max(hora11) as hora11,max(hora12) as hora12,max(hora13) as hora13,max(hora14) as hora14," +
            "max(hora15) as hora15,max(hora16) as hora16,max(hora17) as hora17,max(hora18) as hora18,max(hora19) as hora19," +
            "max(hora20) as hora20,max(hora21) as hora21,max(hora22) as hora22,max(hora23) as hora23,sum(tab1.total) as total "
            //+ "FROM tab2 inner join tab1 on tab2.hostname = tab1.hostname "
            +
            "FROM tab1 " +
            "group by tab1.Hostname ";

        log(stmt);



        var stmtCountRows = stmtContaLinhas(stmt);

        // Alex 24/02/2014 Contador de linhas para auxiliar progress bar
        function contaLinhas(columns) {
            $globals.numeroDeRegistros = columns[0].value;
        }


        function executaQuery(columns) {

            var hostname = columns["hostname"].value,

                hora0 = +columns["hora0"].value,
                hora1 = +columns["hora1"].value,
                hora2 = +columns["hora2"].value,
                hora3 = +columns["hora3"].value,
                hora4 = +columns["hora4"].value,
                hora5 = +columns["hora5"].value,
                hora6 = +columns["hora6"].value,
                hora7 = +columns["hora7"].value,
                hora8 = +columns["hora8"].value,
                hora9 = +columns["hora9"].value,
                hora10 = +columns["hora10"].value,
                hora11 = +columns["hora11"].value,
                hora12 = +columns["hora12"].value,
                hora13 = +columns["hora13"].value,
                hora14 = +columns["hora14"].value,
                hora15 = +columns["hora15"].value,
                hora16 = +columns["hora16"].value,
                hora17 = +columns["hora17"].value,
                hora18 = +columns["hora18"].value,
                hora19 = +columns["hora19"].value,
                hora20 = +columns["hora20"].value,
                hora21 = +columns["hora21"].value,
                hora22 = +columns["hora22"].value,
                hora23 = +columns["hora23"].value,
                total = +columns["total"].value;



            $scope.dados.push({
                hostname: hostname,
                total: total,
                hora0: hora0,
                hora1: hora1,
                hora2: hora2,
                hora3: hora3,
                hora4: hora4,
                hora5: hora5,
                hora6: hora6,
                hora7: hora7,
                hora8: hora8,
                hora9: hora9,
                hora10: hora10,
                hora11: hora11,
                hora12: hora12,
                hora13: hora13,
                hora14: hora14,
                hora15: hora15,
                hora16: hora16,
                hora17: hora17,
                hora18: hora18,
                hora19: hora19,
                hora20: hora20,
                hora21: hora21,
                hora22: hora22,
                hora23: hora23,

            });

            $scope.csv.push([
                hostname,
                total,
                hora0,
                hora1,
                hora2,
                hora3,
                hora4,
                hora5,
                hora6,
                hora7,
                hora8,
                hora9,
                hora10,
                hora11,
                hora12,
                hora13,
                hora14,
                hora15,
                hora16,
                hora17,
                hora18,
                hora19,
                hora20,
                hora21,
                hora22,
                hora23,
            ]);

            //atualizaProgressBar($globals.numeroDeRegistros, $scope.dados.length,$scope,"#pag-resumo-dia");
            if ($scope.dados.length % 1000 === 0) {
                $scope.$apply();
            }
        }





        db.query(stmt, executaQuery, function (err, num_rows) {
            userLog(stmt, 'Carrega dados', 2, err)
            console.log("Executando query-> " + stmt + " " + num_rows);
            retornaStatusQuery(num_rows, $scope);
            $btn_gerar.button('reset');
            if (num_rows > 0) {
                $btn_exportar.prop("disabled", false);
                $btn_exportar_csv.prop("disabled", false);
                $btn_exportar_dropdown.prop("disabled", false);
            }
        });




        $view.on("mouseup", "tr.resumo", function () {
            var that = $(this);
            $('tr.resumo.marcado').toggleClass('marcado');
            $scope.$apply(function () {
                that.toggleClass('marcado');
            });
        });
    };





    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        // Criar cabeçalho
        var cabecalho = $scope.datas.map(function (d) {
            return {
                value: d.data_BR,
                bold: 1,
                hAlign: 'center',
                autoWidth: true
            };
        });

        // Inserir primeira coluna: item de controle
        cabecalho.unshift({
            value: "Item de controle",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        // Inserir última coluna: total por linha
        cabecalho.push({
            value: "Total",
            bold: 1,
            hAlign: 'center',
            autoWidth: true
        });

        var linhas = $scope.dados.map(function (dado) {
            var linha = $scope.datas.map(function (d) {
                try {
                    return {
                        value: dado.totais[d.data].qtd_entrantes || 0
                    };
                } catch (ex) {
                    return 0;
                }
            });

            // Inserir primeira coluna: item de controle
            linha.unshift({
                value: dado.cod_ic + " - " + dado.nome_ic
            });

            // Inserir última coluna: total por linha
            linha.push({
                value: dado.totais.geral.qtd_entrantes
            });

            return linha;
        });

        // Criar última linha, com os totais por data
        var linha_totais = $scope.datas.map(function (d) {
            try {
                return {
                    value: $scope.totais[d.data].qtd_entrantes || 0
                };
            } catch (ex) {
                return 0;
            }
        });

        // Inserir primeira coluna
        linha_totais.unshift({
            value: "Total",
            bold: 1
        });

        // Inserir última coluna: total geral
        linha_totais.push({
            value: $scope.totais.geral.qtd_entrantes
        });

        // Inserir primeira linha: cabeçalho
        linhas.unshift(cabecalho);

        // Inserir última linha: totais por data
        linhas.push(linha_totais);

        var planilha = {
            creator: "Estalo",
            lastModifiedBy: $scope.username || "Estalo",
            worksheets: [{
                name: 'Resumo por IC',
                data: linhas,
                table: true
            }],
            autoFilter: false,
            // Não incluir a linha do título no filtro automático
            dataRows: {
                first: 1
            }
        };

        var xlsx = frames["xlsxjs"].window.xlsx;
        planilha = xlsx(planilha, 'binary');


        var milis = new Date();
        var file = 'resumoItemControle_' + formataDataHoraMilis(milis) + '.xlsx';


        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, planilha.base64, 'base64');
            childProcess.exec(file);
        }

        setTimeout(function () {
            $btn_exportar.button('reset');
        }, 500);
    };



    // Exportar planilha XLSX
    $scope.exportaXLSX = function () {

        var $btn_exportar = $(this);
        $btn_exportar.button('loading');

        var template = 'tResumoFalhas';




        //Alex 15-02-2014 - 26/03/2014 TEMPLATE
        var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
            " WHERE NomeRelatorio='" + template + "'";
        log(stmt);
        db.query(stmt, function (columns) {
            var dataAtualizacao = columns[0].value,
                nomeRelatorio = columns[1].value,
                arquivo = columns[2].value;


            var milis = new Date();
            var baseFile = 'tResumoFalhas.xlsx';



            var buffer = toBuffer(toArrayBuffer(arquivo));

            fs.writeFileSync(baseFile, buffer, 'binary');

            var file = 'resumoFalhas_' + formataDataHoraMilis(milis) + '.xlsx';

            var newData;


            fs.readFile(baseFile, function (err, data) {
                // Create a template
                var t = new XlsxTemplate(data);

                // Perform substitution
                t.substitute(1, {
                    filtros: $scope.filtros_usados,
                    planDados: $scope.dados
                });

                // Get binary data
                newData = t.generate();


                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, newData, 'binary');
                    childProcess.exec(file);
                }
            });

            setTimeout(function () {
                $btn_exportar.button('reset');
            }, 500);



        }, function (err, num_rows) {
            userLog(stmt, 'Exportar XLSX', 2, err);
            //?
        });

    };

}
CtrlResumoFalhas.$inject = ['$scope', '$globals'];