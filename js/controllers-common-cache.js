var cache_querys = [];
var aplicacaoic = [];
//{cache
function Cache(db) {
	
	var self = this;

	this.aplicacoes = {};
	this.segmentos = [];
	this.gruposMeta = [];
	this.transferid = [];
	this.fontes = [];
	this.sites = [];
	this.errosRouting = [];
	this.reconhecimentos = [];
	this.empresasRecarga = [];
	this.variaveis2 = [];
	this.legados = [];
	this.varjson = [];
	this.reparos = [];
	this.incentivos = [];
	this.produto_prepago = [];
	this.telcallflow = [];
	this.dominios = [];
	this.uras = [];
	this.parametros = [];
	this.parametrosOiTV = [];
	this.usuarios = [];
	this.grupoaplicacoes = [];
	this.erros = [];
	this.icsRepetidas = [];
	this.icsRecVoz = [];
	this.ddds = [];
	this.dddsApl = [];
	this.ufs = [];
	this.gras = [];
	this.performanceINs = [];
	this.apls = [];
	this.segs = [];
	this.segsPorApl = [];
	this.gruposSegs = [];
	this.gruposSegsPorApl = [];
	this.aplsVenda = [];
	this.produtos = [];
	this.produtos_vendas = [];
	this.grupos_produtos = [];
	this.grupos_produtos_vendas = [];
	this.datsUltConsolis = [];
	this.datsUltConsolIc = [];
	this.datsUltMesDetCham = new Date();
	this.datsUltMesCallLog = new Date();
	this.datsUltMesCallLogM4U = new Date();
	this.datsUltMesCallLogNVP = new Date();
	this.relatorios = [];
	this.valoresVar = [];
	this.servicos = [];
	this.bases = [];
	this.empresas = [];
	this.operacoes = [];
	this.regras = [];
	this.skills = [];
	this.releaseEstaloData = "";
	this.releaseEstaloLink = "";
	this.releaseEstaloHtml = "";
	this.releaseEstaloEXE = "";
	this.toolInfos = [];
	this.aclapls = [];
	this.aclbotoes = [];
	this.encerramentos = [];
	this.exibicaoCondicionalColunas = [];
	this.assuntos = [];
	this.criteriosConsolGenerica = [];
	this.produtosTu = [];
	this.tiposAutomacoes = [];
	this.canaisM4U = [];
	
	
	//Alex 12/08/2015 - Carrega extrator padrão
	this.carregaExtratorPadrao = function (callback) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='extratorDiaADiaZip'";
		console.log("EXTRATOR PADRÃO -> " + stmt);
		statusInfo("Carregando extrator...");
		db.query(stmt, function (columns) {
			var dataAtualizacao = columns[0].value,
				nomeRelatorio = columns[1].value,
				arquivoB = columns[2].value;		
				var baseFileZip = 'extratorDiaADia.js';
				var buffZip = toBuffer(toArrayBuffer(arquivoB));								
				  fs.writeFileSync(baseFileZip +"_temp", buffZip, 'binary');	
				  require('zlib').unzip(new Buffer(fs.readFileSync(baseFileZip +"_temp")), function(err, buf) {      
				  if (err) console.log(err);
				  fs.writeFileSync(baseFileZip, buf, 'binary');
					  try{
						fs.unlinkSync(baseFileZip +"_temp");
					  }catch(ex){
					  }
				  });	
		
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();			

		});
	};	

	//Alex 12/08/2015 - Carrega node.exe padrão
	this.carregaEstaloExe = function (callback) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='Estalo.exe'";
		console.log("ESTALO.EXE -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var dataAtualizacao = result[i][0].value,
						nomeRelatorio = result[i][1].value,
						arquivoB = result[i][2].value;
														
					if(nomeRelatorio !==""){			
						
						var baseFileZip = nomeRelatorio;
						var buffZip = toBuffer(toArrayBuffer(arquivoB));								
					  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
					  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + baseFileZip +"_temp")), function(err, buf) {      
					  if (err) console.log(err);
					  fs.writeFileSync(tempDir3() + baseFileZip, buf, 'binary');
						  try{
							fs.unlinkSync(tempDir3() + baseFileZip +"_temp");
						  }catch(ex){
						  }
					  });							  
										
					}
				}
				if (callback) callback();
			}
		});	
	
	};

	//Alex 12/08/2015 - Carrega node.exe padrão
	this.carregaNodeExe = function (callback) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='NODE.EXE'";
		console.log("NODE.EXE PADRÃO -> " + stmt);
		statusInfo("Carregando interpretador...");

		if (fs.existsSync(tempDir3() + 'node.exe')) {
			cache_querys.push(stmt);
			atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			callback();
		} else {
			db.query(stmt, function (columns) {
				var dataAtualizacao = columns[0].value,
					nomeRelatorio = columns[1].value,
					arquivoB = columns[2].value;
				var baseFileZip = 'node.exe';
				var buffZip = toBuffer(toArrayBuffer(arquivoB));								
				  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
				  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + baseFileZip +"_temp")), function(err, buf) {      
				  if (err) console.log(err);
				  fs.writeFileSync(tempDir3() + baseFileZip, buf, 'binary');
					  try{
						fs.unlinkSync(tempDir3() + baseFileZip +"_temp");
					  }catch(ex){
					  }
				  });	
	
			}, function (err, num_rows) {
				if (err) {
					console.log(err);
					
				}

				if (num_rows > 0) {
					cache_querys.push(stmt);
					atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
				}

				if (callback) callback();				

			});

		}
	};
	
	
	//Alex 02/07/2019 - Tocar prompts
	this.carregaPromptsTocar = function (callback) {		
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio in ('ffmpeg.dll','close.png','play.png','aguarde.gif')";
		console.log("TOCAR PROMPTS -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var dataAtualizacao = result[i][0].value,
						nomeRelatorio = result[i][1].value,
						arquivoB = result[i][2].value;
											
					if(nomeRelatorio !==""){			
						if(nomeRelatorio.match('.png|.gif') !== null){
							var baseFile = nomeRelatorio;
							var buff = toBuffer(toArrayBuffer(arquivoB));
							if (!fs.existsSync(__dirname + 'img/')) fs.mkdirSync(__dirname + 'img/');
							fs.writeFileSync(tempDir3()+'img/' + baseFile, buff, 'binary');
						}else{
							var baseFileZip = nomeRelatorio;
							var buffZip = toBuffer(toArrayBuffer(arquivoB));								
						  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
						  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + baseFileZip +"_temp")), function(err, buf) {      
						  if (err) console.log(err);
						  fs.writeFileSync(tempDir3() + baseFileZip, buf, 'binary');
							  try{
								fs.unlinkSync(tempDir3() + baseFileZip +"_temp");
							  }catch(ex){
							  }
						  });							  
						}				
					}
				}
				if (callback) callback();
			}
		});	
		//}
	};
	
	//Alex 25/09/2019 - Reconhecimento  de voz
	this.carregaRecVoz = function (callback) {
		if (fs.existsSync(tempDir3() + 'curl.exe') && fs.existsSync(tempDir3() + 'ffmpeg.exe')) {
			return;
		}else{
			var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
				" WHERE NomeRelatorio in ('ffmpeg.exe','curl.exe')";
			console.log("Reconhecimento  de voz -> " + stmt);
			
			function executaUnZip (name){
			  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + name +"_temp")), function(err, buf) {      
			  if (err) console.log(err);
			  fs.writeFileSync(tempDir3() + name, buf, 'binary');
				  try{
					fs.unlinkSync(tempDir3() + name +"_temp");
				  }catch(ex){
				  }
			  });
			}
			
			mssqlQueryTedious(stmt, function (erro, result) {
				if (result.length > 0) {			
					for (var i = 0; i < result.length; i++){
						var dataAtualizacao = result[i][0].value,
							nomeRelatorio = result[i][1].value,
							arquivoB = result[i][2].value;
												
						if(nomeRelatorio !==""){									
							var baseFileZip = nomeRelatorio;
							var buffZip = toBuffer(toArrayBuffer(arquivoB));								
						  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
						  executaUnZip(baseFileZip);				  
						}				
					}
				}
				if (callback) callback();			
			});	
		}
	};

	//Alex 01/11/2017 - Carrega pscp.exe
	this.carregaPscpExe = function (callback) {
		var stmt = db.use + "SELECT DataAtualizacao, NomeRelatorio, Arquivo FROM " + db.prefixo + "ArquivoRelatorios" +
			" WHERE NomeRelatorio='PSCP.EXE'";
		console.log("PSCP.EXE PADRÃO -> " + stmt);
		statusInfo("Carregando utilitário ftp...");

		if (fs.existsSync(tempDir3() + 'pscp.exe')) {
			cache_querys.push(stmt);
			atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			callback();
		} else {

			db.query(stmt, function (columns) {
				var dataAtualizacao = columns[0].value,
					nomeRelatorio = columns[1].value,
					arquivoB = columns[2].value;
					
					var baseFileZip = 'pscp.exe';
				var buffZip = toBuffer(toArrayBuffer(arquivoB));								
				  fs.writeFileSync(tempDir3() + baseFileZip +"_temp", buffZip, 'binary');	
				  require('zlib').unzip(new Buffer(fs.readFileSync(tempDir3() + baseFileZip +"_temp")), function(err, buf) {      
				  if (err) console.log(err);
				  fs.writeFileSync(tempDir3() + baseFileZip, buf, 'binary');
					  try{
						fs.unlinkSync(tempDir3() + baseFileZip +"_temp");
					  }catch(ex){
					  }
				  });	

			}, function (err, num_rows) {
				if (err) {
					console.log(err);
					
				}

				if (num_rows > 0) {
					cache_querys.push(stmt);
					atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
				}

				if (callback) callback();
			});
		}
	};

	//Bernardo 12/08/2015 - Carrega uras
	this.carregaUras = function (callback) {
		var stmt = db.use + "SELECT CodIVRHW" +
			" FROM " + db.prefixo + "IVRHW where TipoServidor = 'U' and CodAplicacao in " +
			" (SELECT CodAplicacao from MapeamentoAplicacoes where DataFim < getdate()) " +
			" ORDER BY CodIVRHW;"
		console.log("URAS -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.parametros = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodIVRHW = result[i]["CodIVRHW"].value;
						
					if(CodIVRHW !==""){
						self.uras.push({
							codigo: CodIVRHW,
							nome: CodIVRHW
						});
					}
				}
				if (callback) callback();
			}
		});	
	};

	//Alex 13/07/2017 - Carrega ACLAplicacoes
	this.carregaACLAplicacoes = function (callback) {
		var stmt = db.use + "SELECT Dominio, idView, Tipo, Aplicacoes" +
			" FROM " + db.prefixo + "ACLAplicacoes where dominio = '" + dominioUsuario + "';"

		console.log("ACLAplicacoes -> " + stmt);
		statusInfo("Carregando regras de whitelist e blacklist...");
		db.query(stmt, function (columns) {
			var Dominio = columns["Dominio"].value,
				idView = columns["idView"].value,
				Tipo = columns["Tipo"].value,
				Aplicacoes = columns["Aplicacoes"].value;

			self.aclapls.push({
				dominio: Dominio,
				idview: idView,
				tipo: Tipo,
				aplicacoes: Aplicacoes
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}	
			cache_querys.push(stmt);
			atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			if (callback) callback();
		});
	};

	//18/07/2017 - Carrega ACLBotoes
	this.carregaACLBotoes = function (callback) {
		var stmt = db.use + "SELECT Login, Dominio, idView, Permissoes" +
			" FROM " + db.prefixo + "ACLBotoes where dominio = '" + dominioUsuario + "' and login = '" + loginUsuario + "';"
		console.log("ACLBotoes -> " + stmt);
		statusInfo("Carregando regras para exibir botões...");
		db.query(stmt, function (columns) {
			var Login = columns["Login"].value,
				Dominio = columns["Dominio"].value,
				idView = columns["idView"].value,
				Permissoes = columns["Permissoes"].value;

			self.aclbotoes.push({
				login: Login,
				dominio: Dominio,
				idview: idView,
				permissoes: Permissoes
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			cache_querys.push(stmt);
			atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			if (callback) callback();
		});
	};

	//12/08/2015 - Carrega Parametros
	this.carregaParametros = function (callback) {
		var stmt = db.use + "SELECT codigo,nome,prazo,frase1,frase2 " +
			" FROM " + db.prefixo + "ParametrosGra order by id";
		console.log("ParametrosGra -> " + stmt);		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.parametros = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var codigo = result[i]["codigo"].value;
					var nome = result[i]["nome"].value;
					var prazo = result[i]["prazo"].value;
					var frase1 = ((result[i]["frase1"].value == undefined) ? "" : result[i]["frase1"].value);
					var frase2 = ((result[i]["frase2"].value == undefined) ? "" : result[i]["frase2"].value);
						
					if(codigo !==""){
						self.parametros.push({
							codigo: codigo,
							nome: nome,
							prazo: prazo,
							frase1: frase1,
							frase2: frase2
						});
					}
				}
				if (callback) callback();
			}
		});
	};
	
	//12/08/2015 - Carrega grupo aplicacoes
	this.carregaGrupoaAplicacoes = function (callback) {
		var stmt = db.use + "select grupoapl " +
			" FROM " + db.prefixo + "GrupoAplicacoes";
		console.log("Grupo aplicações -> " + stmt);
		statusInfo("Carregando grupos de aplicações...");

		db.query(stmt, function (columns) {
			var combinacao = columns["grupoapl"].value;
			self.grupoaplicacoes.push(combinacao);

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();
		});
	};

	//Carrega produtos
	this.carregaProdutos = function (callback) {

		var stmt = db.use + "SELECT CodAplicacao as ca, MPA.CodProduto as cp, GrupoProduto as gp" +
			" FROM " + db.prefixo + "ProdutosPorAplicacao AS MPA" +
			" INNER JOIN ProdutosURA AS PU ON MPA.CodProduto = PU.CodProduto" +
			" WHERE 1 = 1" +
			" AND MPA.CodAplicacao IN (" + cache.apls.map(function (apl) {
				return "'" + apl.codigo + "'";
			}).join(",") + ")" +
			" ORDER BY MPA.CodAplicacao";
		console.log("PRODUTOS POR APLICAÇÃO -> " + stmt);
		statusInfo("Carregando produtos por aplicação...");
		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["ca"].value,
				cod_produto = columns["cp"].value,
				cod_grupo_produto = columns["gp"].value;

			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});

			apl.produtos || (apl.produtos = []);
			apl.produtos.push({
				codigo: cod_produto,
				grupo: cod_grupo_produto
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	//07/02/2014 - Carrega grupos produtos
	var produtos = [];
	this.carregaGruposProdutos = function (callback) {
		var stmt = db.use + "SELECT CodProduto,Descricao,GrupoProduto,CodCategoria" +
			" FROM " + db.prefixo + "ProdutosURA;"
		console.log("PRODUTOS -> " + stmt);
		statusInfo("Carregando produtos por aplicação..");
		db.query(stmt, function (columns) {
			var CodProduto = columns["CodProduto"].value,
				Descricao = columns["Descricao"].value,
				GrupoProduto = columns["GrupoProduto"].value,
				CodCategoria = columns["CodCategoria"].value;

			self.produtos_vendas.push({
				codigo: CodProduto,
				nome1: Descricao,
				nome: Descricao,
				grupo: GrupoProduto
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			for (var i = 0; i < self.produtos_vendas.length; i++) {
				self.grupos_produtos.push(self.produtos_vendas[i]['grupo']);
			}
			self.grupos_produtos = unique(self.grupos_produtos);

			for (var j = 0; j < self.grupos_produtos.length; j++) {
				for (var i = 0; i < self.produtos_vendas.length; i++) {
					if (self.produtos_vendas[i]['grupo'] === self.grupos_produtos[j]) {
						produtos.push(self.produtos_vendas[i]['codigo']);
					}
				}
				self.grupos_produtos_vendas.push({
					codigo: self.grupos_produtos[j],
					nome: self.grupos_produtos[j],
					produtos: produtos
				});
				produtos = [];

			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();
		});
	};

	//12/08/2014 25/08/2016 - Carrega aplicações
	this.carregaAplicacoes = function (callback) {

		disparoInicial = new Date();
		var stmt = db.use + "SELECT CodAplicacao,descricao,NGR,NVP,Venda,datainicio,datafim,grupo,cpf,contrato,operador"
		+" FROM " + db.prefixo + "MapeamentoAplicacoes where Datafim > getdate() order by grupo,descricao;"

		console.log("APLICAÇÕES -> " + stmt);
		statusInfo("Carregando aplicações...");
		db.query(stmt, function (columns) {
			var CodAplicacao = columns["CodAplicacao"].value,
				Descricao = columns["descricao"].value,
				NGR = columns["NGR"].value,
				NVP = columns["NVP"].value,
				venda = columns["Venda"].value,
				inicio = columns["datainicio"] !== undefined ? precisaoMesInicio(columns["datainicio"].value) : "",
				fim = columns["datafim"] !== undefined ? precisaoMesFim(columns["datafim"].value) : "",
				grupo = columns["grupo"].value,
				cpf = columns["cpf"].value,
				contrato = columns["contrato"].value,
				operador = columns["operador"].value;


			self.apls.push({
				codigo: CodAplicacao,
				nome: Descricao,
				ngr: NGR,
				nvp: NVP,
				inicio: inicio,
				fim: fim,
				grupo: grupo,
				cpf: cpf,
				contrato: contrato,
				operador: operador
			});
			if (venda) {
				self.aplsVenda.push(CodAplicacao);
			}

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
				alert("Erro de conexão ao banco de dados\n" + "usuário: "+ loginUsuario + " dominio: "+ dominioUsuario + "\n ");
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();;
		});
	};

	//Grupo Metas Brunno 12/11/2019
		this.carregaGruposMeta = function (callback) {

		disparoInicial = new Date();
		var stmt = db.use + "SELECT grupo,descricao,datainicio,datafim"
		+" FROM " + db.prefixo + "GruposMeta where Datafim > getdate() order by grupo,descricao;"

		console.log("GRUPOS METAS -> " + stmt);
		statusInfo("Carregando Grupo Metas...");
		mssqlQueryTedious(stmt, function (erro, result) {
			self.gruposMeta = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
			var Descricao = result[i]["descricao"].value,
				inicio = result[i]["datainicio"] !== undefined ? precisaoMesInicio(result[i]["datainicio"].value) : "",
				fim = result[i]["datafim"] !== undefined ? precisaoMesFim(result[i]["datafim"].value) : "",
				grupo = result[i]["grupo"].value;
					
					if(grupo !==""){
						self.gruposMeta.push({
							codigo: grupo,
							nome: Descricao,
							inicio: inicio,
							fim: fim
						});
					}
				}
				if (callback) callback();		
			}
		});
	};

	//12/08/2014 - Carrega erros
	this.carregaErros = function (callback) {
		var stmt = db.use + "SELECT CodErro,DescErro,GrupoProduto" +
			" FROM " + db.prefixo + "ErrosVendas;"
		console.log("ERROS VENDAS -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.erros = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodErro = result[i]["CodErro"].value,
					DescErro = result[i]["DescErro"].value,
					GrupoProduto = result[i]["GrupoProduto"].value;
									
					if(CodErro !==""){
						self.erros.push({
							codigo: CodErro,
							nome: DescErro,
							grupo: GrupoProduto
						});
					}
				}
				if (callback) callback();	
			}
		});
	};

	this.carregaIcsRepetidas = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao,CodIc,Descricao,RelatorioTag" +
			" FROM " + db.prefixo + "ICRepetidas;"
		console.log("ICS REPETIDAS -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.icsRepetidas = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodAplicacao = result[i]["CodAplicacao"].value,
					CodIc = result[i]["CodIc"].value,
					Descricao = result[i]["Descricao"].value,
					RelatorioTag = result[i]["RelatorioTag"].value;
									
					if(CodIc !==""){
						self.icsRepetidas.push({
							CodIc: CodIc,
							CodAplicacao : CodAplicacao,
							Descricao: Descricao,
							RelatorioTag: RelatorioTag
						});
					}
				}
				if (callback) callback();	
			}
		});
	};

	this.carregaIcsRecVoz = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao,CodIc,Descricao,Perfil" +
			" FROM " + db.prefixo + "ICRecVoz;"
		console.log("ICS RECVOZ -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.icsRecVoz = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodAplicacao = result[i]["CodAplicacao"].value,
					CodIc = result[i]["CodIc"].value,
					Descricao = result[i]["Descricao"].value,
					Perfil = result[i]["Perfil"].value;
									
					if(CodIc !==""){
						self.icsRecVoz.push({
							CodIc: CodIc,
							CodAplicacao : CodAplicacao,
							Descricao: Descricao,
							Perfil: Perfil
						});
					}
				}
				if (callback) callback();	
			}
		});
	};

	//18/08/2014 - Carrega ddds
	this.carregaDDDs = function (callback) {
		var stmt = db.use + "SELECT UF,DDD,Regiao,Descricao" +
			" FROM " + db.prefixo + "UF_DDD ORDER BY DDD ASC;"
		console.log("DDDS -> " + stmt);
		mssqlQueryTedious(stmt, function (erro, result) {
			self.ddds = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var UF = result[i]["UF"].value,
						DDD = result[i]["DDD"].value,
						regiao = result[i]["Regiao"].value,
						descricao = result[i]["Descricao"].value;
					
					if(UF !==""){
						self.ddds.push({
							codigo: DDD,
							nome: descricao,
							uf: UF,
							regiao: regiao
						});
					}
				}
				var ufs = [];
				self.ddds.forEach(function (ddd) {
					ufs.push(ddd.uf);
				});
				ufs = unique(ufs);
				ufs.forEach(function (uf) {
					self.ufs.push({
						codigo: uf,
						nome: uf
					});
				});
				if (callback) callback();	
			}
		});
	};

	//18/08/2014 - Carrega gras
	this.carregaGras = function (callback) {
		var stmt = db.use + "SELECT GRA, CODGRA, DDD, UF" +
			" FROM " + db.prefixo + "gras2 ORDER BY GRA ASC;"
		console.log("GRAS -> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.gras = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var GRA = result[i]["GRA"].value,
					CODGRA = result[i]["CODGRA"].value,
					DDD = result[i]["DDD"].value,
					UF = result[i]["UF"].value;					
									
					if(GRA !==""){
						self.gras.push({
							codigo: GRA,
							nome: GRA,
							codigo2: CODGRA,
							ddd: DDD,
							uf: UF
						});
					}
				}
				if (callback) callback();
			}
		});
	};

	//18/08/2014 - Carrega sites
	this.carregaSites = function (callback) {
		var stmt = db.use + "SELECT CodSite, TituloSite" +
			" FROM " + db.prefixo + "sites where ativo = 1 ORDER BY TituloSite ASC;"
		console.log("SITES uar-> " + stmt);
		statusInfo("Carregando sites...");
		db.query(stmt, function (columns) {
			var CodSite = columns["CodSite"].value,
				TituloSite = columns["TituloSite"].value;

			self.sites.push({
				codigo: CodSite,
				nome: TituloSite
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();	
		});
	};

	this.carregaEmpresasRecarga = function (callback) {
		var stmt = db.use + "SELECT CodEmpresa" +
			" FROM " + db.prefixo + "EmpresaRecarga"
		console.log("empresas recarga-> " + stmt);		
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.empresasRecarga = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodEmpresa = result[i]["CodEmpresa"].value,
						Titulo = result[i]["CodEmpresa"].value;
					
					if(CodEmpresa !==""){
						self.empresasRecarga.push({
							codigo: CodEmpresa,
							nome: Titulo
						});
					}
				}
				if (callback) callback();		
			}
		});
	};

	this.carregaErrosRouting = function (callback) {
		var stmt = db.use + "SELECT CodErro" +
			" FROM " + db.prefixo + "ErroRoteamento"
		console.log("erros pre routing-> " + stmt);
		statusInfo("Carregando erros routing...");
		db.query(stmt, function (columns) {
			var CodErro = columns["CodErro"].value,
				Titulo = columns["CodErro"].value;

			self.errosRouting.push({
				codigo: CodErro,
				nome: Titulo
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();			
		});
	};

	this.carregaReconhecimentos = function (callback) {
		var stmt = db.use + "SELECT Resultado" +
			" FROM " + db.prefixo + "ResultadosReconhecimentoVoz"
		console.log("ResultadosReconhecimentoVoz-> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.reconhecimentos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var Codigo = result[i]["Resultado"].value,
						Titulo = result[i]["Resultado"].value;
						
					if(Codigo !==""){
						self.reconhecimentos.push({
						codigo: Codigo,
						nome: Titulo
					});
					}
				}
				if (callback) callback();
			}
		});
	};

	// Carrega fontes
	this.carregaFontes = function (callback) {
		var stmt = db.use + "SELECT distinct Parametro" +
			" FROM " + db.prefixo + "Parametros_Ics p " +
			" where relatorio = 'Fontes' and sucesso = 'Sim' order by parametro";
		console.log("Fontes-> " + stmt);
		statusInfo("Carregando fontes...");
		db.query(stmt, function (columns) {
			var Codic = columns["Parametro"].value,
				Descricao = columns["Parametro"].value;

			self.fontes.push({
				codigo: Codic,
				nome: Descricao
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();
		});
	};

	// Carrega Incentivos
	this.carregaIncentivos = function (callback) {
		var stmt = db.use + "SELECT CodIncentivo, Descricao" +
			" FROM " + db.prefixo + "Incentivo order by Descricao;"
		console.log("Incentivos-> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.incentivos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var Codigo = result[i]["CodIncentivo"].value,
					Descricao = result[i]["Descricao"].value;					
					if(Codigo !==""){
						self.incentivos.push({
						codigo: Codigo,
						nome: Descricao
					});
					}
				}
				if (callback) callback();
			}
		});
		
	};

	// Carrega Produtos Pre Pago
	this.carregaProdutoPrepago = function (callback) {
		var stmt = db.use + "SELECT codigo, Produto" +
			" FROM " + db.prefixo + "ProdutoPrePago order by Produto;"
		console.log("ProdutoPrePago-> " + stmt);
		mssqlQueryTedious(stmt, function (erro, result) {
			self.incentivos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var Codigo = result[i]["codigo"].value,
					Produto = result[i]["Produto"].value;						
					if(Codigo !==""){
						self.produto_prepago.push({
							codigo: Codigo,
							nome: Produto
						})
					}
				}
				if (callback) callback();
			}
		});
	};

	// Carrega TelefonesCallFlow
	this.carregaTelCallFlow = function (callback) {
		var stmt = db.use + "SELECT numAni" +
			" FROM " + db.prefixo + "TelefonesCallFlow;"
		console.log("TelCallFlow-> " + stmt);
		statusInfo("Carregando telefones callflow...");
		db.query(stmt, function (columns) {
			var Codigo = columns["numAni"].value,
				Descricao = true;

			self.telcallflow.push({
				codigo: Codigo,
				nome: Descricao
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();

		});
	};	

	//12/08/2015 - Carrega uras
	this.carregaUras = function (callback) {
		var stmt = db.use + "SELECT CodIVRHW" +
			" FROM " + db.prefixo + "IVRHW where TipoServidor = 'U' and CodAplicacao in " +
			" (SELECT CodAplicacao from MapeamentoAplicacoes where DataFim < getdate()) " +
			" ORDER BY CodIVRHW;"
		console.log("URAS -> " + stmt);
		statusInfo("Carregando informações de URAs...");
		db.query(stmt, function (columns) {
			var CodIVRHW = columns["CodIVRHW"].value;
			self.uras.push({
				codigo: CodIVRHW,
				nome: CodIVRHW
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();
		});
	};

	//18/08/2014 - Carrega ddds por aplicação
	this.carregaDDDsPorAplicacao = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao,CodDDD" +
			" FROM " + db.prefixo + "DDDsPorAplicacao;"
		console.log("DDDS POR APLICAÇÃO -> " + stmt);		
		statusInfo("Carregando DDDs...");

		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				cod_ddd = columns["CodDDD"].value;

			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});

			apl.dddsApl || (apl.dddsApl = []);
			apl.dddsApl.push(cod_ddd);

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
	
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();	
		});
	};

	// Carrega segmentos
	this.carregaSegmentos = function (callback) {

		var stmt = db.use + "SELECT MPA.CodAplicacao as ca, MPA.CodSegmento as cs, MS.CodGrupoSegmento as cgs" +
			" FROM " + db.prefixo + "SegmentosPorAplicacao AS MPA" +
			" INNER JOIN Segmentos AS MS ON MPA.CodSegmento = MS.CodSegmento" +
			" WHERE 1 = 1" +
			" AND MPA.CodAplicacao IN (" + cache.apls.map(function (apl) {
				return "'" + apl.codigo + "'";
			}).join(",") + ")" +
			" ORDER BY MPA.CodAplicacao";

		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["ca"].value,
				cod_segmento = columns["cs"].value,
				cod_grupo_segmento = columns["cgs"].value;

			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});

			apl.segmentos || (apl.segmentos = []);
			apl.segmentos.push({
				codigo: cod_segmento,
				codigo2: cod_grupo_segmento
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	// Carrega tid
	this.carregaTransferIds = function (callback) {

		var stmt = db.use + "SELECT CodAplicacao, TransferId" +
			" FROM " + db.prefixo + "TransferIdsporAplicacao " +
			" WHERE 1 = 1" +
			" AND CodAplicacao IN (" + cache.apls.map(function (apl) {
				return "'" + apl.codigo + "'";
			}).join(",") + ")" +
			" ORDER BY CodAplicacao,TransferId";
		console.log("TRANSFERID POR APLICAÇÃO -> " + stmt);
		statusInfo("Carregando transferIds...");
	
		
		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				transferid = columns["TransferId"].value;

			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});

			apl.transferid || (apl.transferid = []);
			apl.transferid.push({
				codigo: transferid
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	// Carrega eds transferência
	this.carregaEDsTransf = function (callback) {

		var stmt = db.use + "SELECT EDT.CodAplicacao, EDT.CodED, ED.Nom_Estado" +
			" FROM " + db.prefixo + "EDTransferencia AS EDT " +
			" INNER JOIN Estado_Dialogo AS ED " +
			" ON EDT.CodAplicacao = ED.Cod_Aplicacao " +
			" AND EDT.CodED = ED.Cod_Estado";

		console.log("EDsTranf POR APLICAÇÃO -> " + stmt);
		statusInfo("Carregando EDs de transferência...");
		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				cod_ed = columns["CodED"].value,
				nome = columns["Nom_Estado"].value;


			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});

			apl.EDsTransf || (apl.EDsTransf = []);
			apl.EDsTransf.push({
				codigo: cod_ed,
				apl: cod_aplicacao,
				nome: nome
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	//13/08/2014 - Carrega grupos segmentos
	this.carregaGruposSegmentos = function (callback) {

		var segmentos = [];

		var stmt = db.use + "SELECT CodSegmento,DescSegmento,MGS.CodGrupoSegmento,MGS.DescGrupoSegmento, MS.DataCriacao, MS.DataExpiracao, MS.nomeFiltro" +
			" FROM " + db.prefixo + "Segmentos as MS INNER JOIN GrupoSegmentos as MGS on MS.CodGrupoSegmento =  MGS.CodGrupoSegmento;"
		console.log("GRUPOS SEGMENTOS -> " + stmt);
		statusInfo("Carregando grupos de segmentos...");
		db.query(stmt, function (columns) {
			var CodSegmento = columns["CodSegmento"].value,				
				DescSegmento = columns["DescSegmento"].value,
				CodGrupoSegmento = columns["CodGrupoSegmento"].value,
				DescGrupoSegmento = columns["DescGrupoSegmento"].value,
				cod_data_criacao = columns["DataCriacao"].value,
				cod_data_expiracao = columns["DataExpiracao"].value,
				nomeFiltro = columns["nomeFiltro"].value;


			self.segs.push({
				codigo: CodSegmento,
				nome: DescSegmento,
				grupo: CodGrupoSegmento,
				nome2: DescGrupoSegmento,
				datacriacao: cod_data_criacao,
				dataexpiracao: cod_data_expiracao,
				nomeFiltro: nomeFiltro
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			self.segs.indice = geraIndice(self.segs);

			var grupos = unique(self.segs.map(function (grupo) {
				return [grupo.grupo, grupo.nome2];
			}));

			for (var j = 0; j < grupos.length; j++) {

				for (var i = 0; i < self.segs.length; i++) {
					if (self.segs[i]['grupo'] === grupos[j][0]) {
						segmentos.push(self.segs[i]['codigo']);
					}
				}
				self.gruposSegs.push({
					codigo: grupos[j][0],
					nome: grupos[j][1],
					segmentos: segmentos
				});
				segmentos = [];
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}

			if (callback) callback();
		});
	};

	//07/02/2014
	this.carregaDatUltConsolis = function (callback) {
		var stmt = db.use + "SELECT minima,maxima,aplicacao,relatorio" +
			" FROM " + db.prefixo + "consolidacoes";

		console.log("CONSOLIDAÇÕES -> " + stmt);
		statusInfo("Carregando dados de consolidação...");
		var controle = 0;
		db.query(stmt, function (columns) {
			var minima = columns[0].value,
				maxima = columns[1].value,
				aplicacao = columns[2].value,
				relatorio = columns[3].value;

			var apl = self.aplicacoes[aplicacao] || (self.aplicacoes[aplicacao] = {});

			apl.datsUltConsolis || (apl.datsUltConsolis = []);
			apl.datsUltConsolis.push({
				min: minima,
				max: maxima,
				view: relatorio
			});

		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	//07/02/2014
	this.ultimoMesDetalhamentoDeChamadas = function (callback) {
		var dataAtual = new Date();
		var podeMudar = true;
		var mesAtual = dataAtual.getMonth() + 1; // formatado para usar na query.
		mesAtual = mesAtual < 10 ? mesAtual = "0" + mesAtual : mesAtual;

		var stmt = db.use + "SELECT TOP 1 DataHora_Inicio FROM IVRCDR"; //ORDER BY DataHora_Inicio ASC;";

		console.log("CHAMADAS DATA CONSOLIDAÇÃO -> " + stmt);
		statusInfo("Carregando status de consolidação de chamadas...");


		db.query(stmt, function (columns) {
			var DatReferencia = columns[0].value !== null ? columns[0].value : new Date();
			self.datsUltMesDetCham = formataDataHora(DatReferencia);

		}, function (err, num_rows) {
			if (err) console.log(err);
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	this.ultimoMesCallLogs = function (callback) {


		var stmt = db.use + "select top 1 datahorapasta from calllog union all select top 1 datahorapasta from calllogm4u union all select top 1 datahorapasta from calllognvp;";


		console.log("CallLogs datas -> " + stmt);
		statusInfo("Carregando informações de log original...");
		db.query(stmt, function (columns) {
			self.datsUltMesCallLog = columns[0].value,
				self.datsUltMesCallLogM4U = columns[0].value,
				self.datsUltMesCallLogNVP = columns[0].value;

		}, function (err, num_rows) {
			if (err) console.log(err);
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	//07/02/2014
	this.releaseEstalo = function (callback) {
		var stmt = db.use + "select top 1 * from ReleasesEstalo where tipo = '" + tipoVersao + "' order by DataAtualizacao desc";
		console.log("INFORMAÇÕES DE RELEASE -> " + stmt);
		statusInfo("Carregando informações de release...");

		db.query(stmt, function (columns) {
			self.releaseEstaloData = "Release: " + formataDataBR2(columns[0].value);
			self.releaseEstaloLink = columns[1].value;
			self.releaseEstaloHtml = columns[2].value;
			self.releaseEstaloMsgInicial = columns[4].value;

			console.log(self.releaseEstaloMsgInicial);
		}, function (err, num_rows) {
			if (err) console.log(err);
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	// Carrega variáveis -- 23/01/2014 - retornar também, a descrição das variáveis.
	this.carregaVariaveis = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao, Nome, CodXML, Descricao" +
			" FROM " + db.prefixo + "VariaveisCallLog" +
			" WHERE CodAplicacao <> ''";
		console.log("VARIÁVEIS -> " + stmt);
		statusInfo("Carregando variáveis do log original...");

		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				nome_variavel = columns["Nome"].value,
				cod_xml = columns["CodXML"].value,
				descricao = columns["Descricao"].value,
				tipo = "";
			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});
			apl.variaveis || (apl.variaveis = []);
			
			
				if(columns["CodXML"].value.split('.').length > 1){
					cod_xml = columns["CodXML"].value.split('.')[0];
					tipo = columns["CodXML"].value.split('.')[1];
				}
	
			apl.variaveis.push({
				codigo: cod_xml,
				nome: nome_variavel,
				descricao: descricao,
				tipo: tipo
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	// Carrega variáveis -- 23/01/2014 - retornar também, a descrição das variáveis.
	this.carregaVariaveis2 = function (callback) {
		var stmt = db.use + "SELECT CodVariavel, NomeVariavel" +
			" FROM " + db.prefixo + "VariavelCallLog2";

		console.log("VARIÁVEIS2-> " + stmt);
		db.query(stmt, function (columns) {
			var nome_variavel = columns["NomeVariavel"].value,
				cod_xml = columns["CodVariavel"].value;
			self.variaveis2.push({
				codigo: cod_xml,
				nome: nome_variavel
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}	
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();

		});
	};

	// Carrega legados
	this.carregaLegados = function (callback) {
		var stmt = db.use + "SELECT CodVariavel, NomeVariavel" +
			" FROM " + db.prefixo + "VariavelCallLog2 where tipoVariavel = 'TEMPO'";

		console.log("LEGADOS-> " + stmt);
		db.query(stmt, function (columns) {
			var nome_variavel = columns["NomeVariavel"].value,
				cod_xml = columns["CodVariavel"].value;
			self.legados.push({
				codigo: cod_xml,
				nome: nome_variavel
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();

		});
	};

	// Carrega VariavelJSon
	this.carregaVarJSon = function (callback) {
		var stmt = db.use + "SELECT CodVariavel, NomeVariavel" +
			" FROM " + db.prefixo + "VariavelJSon";

		console.log("VariavelJSon-> " + stmt);
		db.query(stmt, function (columns) {
			var nome_variavel = columns["NomeVariavel"].value,
				cod_xml = columns["CodVariavel"].value;
			self.varjson.push({
				codigo: cod_xml,
				nome: nome_variavel
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			if (callback) callback();
		});
	};

	// Carrega reparos
	this.carregaReparos = function (callback) {
		var stmt = db.use + "SELECT idreparo, nome" +
			" FROM " + db.prefixo + "Reparos";

		console.log("REPAROS-> " + stmt);
		mssqlQueryTedious(stmt, function (erro, result) {
			self.reparos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["nome"].value,
						codigo = result[i]["idreparo"].value;
							
					if(codigo !==""){
						self.reparos.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();
			}
		});
	};
	
	// Carrega bases
	this.carregaBases = function (callback) {
		var stmt = db.use + "SELECT distinct nomebase from " + db.prefixo + " ParametrosFontesCadastro";
		console.log("Serviços bases-> " + stmt);
		mssqlQueryTedious(stmt, function (erro, result) {
			self.bases = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["nomebase"].value,
						codigo = result[i]["nomebase"].value;
				
					if(codigo !==""){
						self.bases.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();		
			}
		});
	};

	// Carrega servicos
	this.carregaServicos = function (callback) {
		var stmt = db.use + "SELECT CodServico, nomeServico" +
			" FROM " + db.prefixo + "ServicoPesquisa";

		console.log("Serviços pesquisa-> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.servicos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["nomeServico"].value,
					codigo = result[i]["CodServico"].value;					
						
					if(codigo !==""){
						self.servicos.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();
			}
		});
	};

	// Carrega empresas
	this.carregaEmpresas = function (callback) {
		var stmt = db.use + "SELECT CodEmpresa, Valor" +
			" FROM " + db.prefixo + "EmpresaRoteamento";

		console.log("Empresas callcenter-> " + stmt);
		//statusInfo("Carregando empresas callcenter...");
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.empresas = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["CodEmpresa"].value,
						codigo = result[i]["Valor"].value;
					if(codigo !==""){
						self.empresas.push({
							codigo: codigo.toUpperCase(),
							nome: nome
						});
					}
				}
				if (callback) callback();
			}
		});
	};

	// Carrega produtos TU
	this.carregaProdutosTU = function (callback) {
		var stmt = db.use + "SELECT CodProduto,Descricao" +
			" FROM " + db.prefixo + "ProdutosTelaUnica";

		console.log("Produtos Tela Única-> " + stmt);
		//statusInfo("Carregando produtos Tela Única...");
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.produtosTu = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["Descricao"].value,
						codigo = result[i]["CodProduto"].value;					
					if(codigo !==""){
						self.produtosTu.push({
							codigo: codigo.toUpperCase(),
							nome: nome
						});
					}					
				}
				if (callback) callback();		
			}
		});
	};

	// Carrega tipos automações
	this.carregaTiposAutomacoes = function (callback) {
		var stmt = db.use + "SELECT TipoAutomacao" +
			" FROM " + db.prefixo + "CadastroTipoAutomacoesTU";

		console.log("Tipos Automações Tela Única-> " + stmt);
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.tiposAutomacoes = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var automacao = result[i]["TipoAutomacao"].value;						
					if(automacao !==""){
						self.tiposAutomacoes.push({
							tipoAutomacao: automacao
						});
					}					
				}
				if (callback) callback();		
			}
		});
	};

	// Carrega Canais M4U
	this.carregaCanaisM4U = function (callback) {
		var stmt = db.use + "SELECT DISTINCT Canal " +
			" FROM " + db.prefixo + "ResumoRecargaM4U";

		console.log("Canais de Recarga M4U -> " + stmt);
		statusInfo("Carregando Canais de Recarga M4U...");	

		mssqlQueryTedious(stmt, function (erro, result) {
			self.canaisM4U = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var canal = result[i]["Canal"].value;						
					if(canal !==""){
						self.canaisM4U.push({
							canal: canal
						});
					}					
				}
				if (callback) callback();		
			}
		});
	};

	// Carrega operações
	this.carregaOperacoesECH = function (callback) {	
		
		var stmt = db.use + "SELECT ISNULL(case CodOperacao WHEN '' THEN NULL ELSE CodOperacao END ,'VAZIO') AS CodOperacao" +
			" FROM " + db.prefixo + "OperacaoEch";

		console.log("Operações-> " + stmt);
		mssqlQueryTedious(stmt, function (erro, result) {
			self.operacoes = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["CodOperacao"].value,
					codigo = result[i]["CodOperacao"].value;
					
					if(codigo !==""){
						self.operacoes.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();	
			}
		});
	};
	
	// Carrega Assuntos
	this.carregaAssuntos = function (callback) {	
		
		var stmt = db.use + "SELECT ISNULL(case Assunto WHEN '' THEN NULL ELSE Assunto END ,'VAZIO') AS Assunto" +
			" FROM " + db.prefixo + "Assuntos";

		console.log("Assuntos-> " + stmt);
		//statusInfo("Carregando assuntos...");
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.assuntos = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["Assunto"].value,
				codigo = result[i]["Assunto"].value;
					
					if(codigo !==""){
						self.assuntos.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();
			}
		});
	};
	
	// Carrega critérios de consolidação genérica
	this.carregaCriterios = function (callback) {
		var stmt = db.use + "SELECT * " +
			" FROM " + db.prefixo + "criteriosConsolGenerica WHERE ATIVO = 1 order by ordem asc";

		console.log("Critérios-> " + stmt);
		//statusInfo("Carregando critérios de consolidação...");
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.criteriosConsolGenerica = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var CodProjeto = result[i]["CodProjeto"].value,
					CodCombinacao = result[i]["CodCombinacao"].value,
					Descricao = result[i]["Descricao"].value,
					CodAplicacao = result[i]["CodAplicacao"].value,
					Criterios = result[i]["Criterios"].value;
					
					if(CodProjeto !==""){
						self.criteriosConsolGenerica.push({
							CodProjeto: CodProjeto,
							CodCombinacao: CodCombinacao,
							Descricao: Descricao,
							CodAplicacao: CodAplicacao,
							Criterios: Criterios
						});
					}
				}
				if (callback) callback();
			}
		});
	};


	// Carrega regras
	this.carregaRegras = function (callback) {
		var stmt = db.use + "SELECT CodRegra" +
			" FROM " + db.prefixo + "RegraRoteamento";

		console.log("Regras roteamento-> " + stmt);
		//statusInfo("Carregando regras roteamento...");
		
		mssqlQueryTedious(stmt, function (erro, result) {
			self.regras = [];
			if (result.length > 0) {
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["CodRegra"].value,
						codigo = result[i]["CodRegra"].value;
					if(codigo !==""){
						self.regras.push({
							codigo: codigo,
							nome: nome
						});
					}
				}
				if (callback) callback();			
			}
		});
	};

	// Carrega skills
	this.carregaSkills = function (callback) {
		var stmt = db.use + "SELECT CodDestino" +
			" FROM " + db.prefixo + "DestinoRoteamento";
		console.log("Skills roteamento-> " + stmt);		
		mssqlQueryTedious(stmt, function (erro, result) {
			if (result.length > 0) {
				self.skills = [];
				for (var i = 0; i < result.length; i++){
					var nome = result[i]["CodDestino"].value,
						codigo = result[i]["CodDestino"].value.toString().replace('<', '%');
					if(codigo !==""){
						self.skills.push({
							codigo: codigo,
							nome: nome
						});
					}
				}	
				if (callback) callback();			
			}
		});
	};
	
	//Brunno 13/09/2019
	this.carregaAplicacaoIC = function (callback) {
		var stmt = "select distinct CodAplicacao from ICRepetidas";	
		mssqlQueryTedious(stmt, function (erro, result) {
			aplicacaoic = [];
			if (result.length > 0) {			
				for (var i = 0; i < result.length; i++){
					var codAplicacao = result[i]["CodAplicacao"].value;
					if(codAplicacao !==""){
						aplicacaoic.push(
							codAplicacao
						);
					}
				}	
				if (callback) callback();		
			}
		});
	};
	// Carrega operações
	this.carregaOperacoes = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao, CodGrupo, DescOperacao, Modalidade" +
			" FROM " + db.prefixo + "GrupoOperacaoBkp2" +
			" WHERE 1 = 1" +
			" AND CodAplicacao IN (" + cache.apls.map(function (apl) {
				return "'" + apl.codigo + "'";
			}).join(",") + ")"
			//+ " AND Modalidade = '3'"
			+
			" ORDER BY DescOperacao";
		console.log("OPERAÇÕES -> " + stmt);
		statusInfo("Carregando operações...");

		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				cod_operacao = columns["CodGrupo"].value,
				nome_operacao = columns["DescOperacao"].value,
				modalidade = columns["Modalidade"].value;
			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});
			apl.operacoes || (apl.operacoes = []);
			apl.operacoes.push({
				codigo: cod_operacao,
				nome: nome_operacao,
				modalidade: modalidade
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	// Carrega promoções
	this.carregaPromocoes = function (callback) {
		var stmt = db.use + "SELECT CodAplicacao, Nome" +
			" FROM " + db.prefixo + "Promocoes" +
			" WHERE 1 = 1 AND Status='A'" +
			" AND CodAplicacao IN (" + cache.apls.map(function (apl) {
				return "'" + apl.codigo + "'";
			}).join(",") + ")" +
			" ORDER BY Nome";
		console.log("PROMOÇÕES -> " + stmt);
		statusInfo("Carregando promoções...");
		db.query(stmt, function (columns) {
			var cod_aplicacao = columns["CodAplicacao"].value,
				nome = columns["Nome"].value;
			var apl = self.aplicacoes[cod_aplicacao] || (self.aplicacoes[cod_aplicacao] = {});
			apl.promocoes || (apl.promocoes = []);
			var codigo, modNome;
			if (+nome.substring(0, 5)) {
				codigo = +nome.substring(0, 5);
				modNome = nome.substring(6, nome.length);
			} else {
				codigo = "ND";
				modNome = nome;
			}
			apl.promocoes.push({
				codigo: codigo,
				nome: modNome
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}		
			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};

	// Carrega encerramentos
	this.carregaEncerramentos = function (callback) {
		var stmt = db.use + "SELECT codigo, nome, tipo" +
			" FROM " + db.prefixo + "Encerramentos";

		console.log("ENCERRAMENTOS -> " + stmt);
		statusInfo("Carregando encerramentos...");
		db.query(stmt, function (columns) {
			var codigo = columns["codigo"].value,
				nome = columns["nome"].value,
				tipo = columns["tipo"].value;
			self.encerramentos.push({
				codigo: codigo,
				nome: nome,
				tipo: tipo
			});
		}, function (err, num_rows) {
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);

			}
			if (callback) callback();
		});
	};


	this.carregaItensDeControleEstadosDeDialogo = function (callback) {
		console.log("Verificando IC ED cache");
		var pastaIcs = __dirname + 'ics/';
		var pastaEds = __dirname + 'eds/';
		if (!fs.existsSync(__dirname + 'ics/') || !fs.existsSync(__dirname + 'eds/')) {
			fs.mkdirSync(__dirname + 'ics/');
			fs.mkdirSync(__dirname + 'eds/');
		}
		mssqlQueryTedious('select  m.CodAplicacao as CodAplicacao, 	c.tamanhoArquivo as tamanhoArquivo, c.dataArquivo as dataArquivo, c.tipo as tipo from MapeamentoAplicacoes m left outer join cacheEstalo c 	on m.CodAplicacao = c.codAplicacao	where 1=1 AND DataFim > GETDATE()', function (err, resultado) {
			if (err) console.log(err);
			var arrAplicacaoTemp = [];
			for (var i = 0; i < resultado.length; i++) {
				arrAplicacaoTemp.push(
					{ 
						apl: resultado[i].CodAplicacao.value,
						result: resultado[i].tamanhoArquivo.value,
						dataArq: resultado[i].dataArquivo.value,
						tipo: resultado[i].tipo.value
					}
				);
			}
			// EDS
			if (fs.existsSync(pastaEds + '10331UNIF.csv.gz')) {						
				//if (formataData(new Date()) === formataData(new Date(tempAplFile.mtime))) { //ATUALIZADO, LER Todos
				    statusInfo("Carregando ICs e EDs...");
					arrAplicacaoTemp.forEach(function (aplicacaoTemp, index, arrayTemp) {
						//FIM
						if (index == arrAplicacaoTemp.length-1) {						
							cache_querys.push('');
							atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
							if (callback) callback();					
							return;
						}
						
						
						var tempAplFile = {};
						var tempAplFile2 = {};
						
						try{
							tempAplFile = fs.statSync(pastaEds + aplicacaoTemp.apl +'.csv.gz');
						}catch(ex){
							tempAplFile.ctime = 0;
						}
						try{
							tempAplFile2 = fs.statSync(pastaIcs + aplicacaoTemp.apl +'.csv.gz');
						}catch(ex){
							tempAplFile2.ctime = 0;
						}
							if(aplicacaoTemp.tipo === "IC" && new Date(aplicacaoTemp.dataArq) > new Date(tempAplFile2.mtime)){						
								baixarTodosICs([aplicacaoTemp], 0, function () {					
									carregaEmMemoria(aplicacaoTemp.apl,aplicacaoTemp.tipo);
									console.log(aplicacaoTemp.tipo+"S da Aplicação "+aplicacaoTemp.apl+" atualizados.");
								});
							}else if(aplicacaoTemp.tipo === "ED" && new Date(aplicacaoTemp.dataArq) > new Date(tempAplFile.mtime)){
								baixarTodosEDs([aplicacaoTemp], 0, function () {					
									carregaEmMemoria(aplicacaoTemp.apl,aplicacaoTemp.tipo);
									console.log(aplicacaoTemp.tipo+"S da Aplicação "+aplicacaoTemp.apl+" atualizados.");
								});								
							}else{
								carregaEmMemoria(aplicacaoTemp.apl,aplicacaoTemp.tipo);
							}
						
						function carregaEmMemoria(apl,tipo){
							// EDS
							if (fs.existsSync(pastaEds + apl + '.csv.gz') && tipo === "ED") {
									zlib.unzip(new Buffer(fs.readFileSync(pastaEds + apl + '.csv.gz')), function (err, buffer) {
										var tempArquivoArray = buffer.toString().split('\r\n');							;
										var arrayTemporarioDownload = [];
										for (var i = 0; i < tempArquivoArray.length; i++) {
											var linha = tempArquivoArray[i].split('\t');
											var trash = linha[4] !== undefined ? (linha[4] === "0") : true;
											var volume = linha[5] !== undefined ? (linha[5] === "0") : true;
											
											arrayTemporarioDownload.push({
												codigo: linha[1],
												nome: linha[2],
												aplicacao: linha[0],
												exibir: (linha[3] === "S"),
												trash: trash,
												volume: volume
											});
											if (i == 0) {
												apl = linha[0];
											}
										}					
										try {
											cache.aplicacoes[apl]['estados'] = [];
											cache.aplicacoes[apl]['estados'] = (arrayTemporarioDownload);
										} catch (err) {								
										}
									});							
							}
							// ICS
							if (fs.existsSync(pastaIcs + apl + '.csv.gz') && tipo === "IC") {
								zlib.unzip(new Buffer(fs.readFileSync(pastaIcs + apl + '.csv.gz')), function (err, buffer) {
									var tempArquivoArray = buffer.toString().split('\r\n');			
									var arrayTemporarioDownload = [];
									for (var i = 0; i < tempArquivoArray.length; i++) {
										var linha = tempArquivoArray[i].split('\t');
										arrayTemporarioDownload.push({
											codigo: linha[1],
											descricao: linha[2],
											aplicacao: linha[0]
										});
										if (i == 0) {
											apl = linha[0];
										}
									}
									try {
										cache.aplicacoes[apl]['itens_controle'] = [];
										cache.aplicacoes[apl]['itens_controle'] = (arrayTemporarioDownload);
									} catch (err) {						
									}
								});
							}
						}
						
					});
				
			} else {
				statusInfo("Download de ICs e EDs...");
				baixarTodosICsEDs(arrAplicacaoTemp, 0, function () {					
					cache_querys.push('');
					atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
					if (callback) callback();					

				});
			}
		});
	};

	this.carregaExibicaoCondicionalColunas = function (callback){
		stmt = "select * from ExibicaoCondicionalColunas";
		exibicao = []
		db.query(stmt,function(columns){
			codrelatorio = columns[0].value
			codaplicacao = columns[1].value
			coluna = columns[2].value

			if(exibicao[codrelatorio] == undefined){
				exibicao[codrelatorio] = []
			}
			if(exibicao[codrelatorio][codaplicacao] == undefined){
				exibicao[codrelatorio][codaplicacao] = []
			}
			exibicao[codrelatorio][codaplicacao].push(coluna)
			
			
		}, function(err, num_rows){
			if (err) {
				console.log(err);
				
			}

			if (num_rows > 0) {
				cache_querys.push(stmt);
				atualizaProgressCache(cacheEstimado, cache_querys.length, "", defaultPage);
			}
			log("Cache carregado em aproximadamente " + Math.round((new Date() - disparoInicial) / 1000) + "s");
		})

		self.exibicaoCondicionalColunas = exibicao
		if (callback) callback();
	}
} //}fim do CACHE

var cache = new Cache(db);
db2.connect(configProc);

function carregaCache(err) {
		
	call_async([
		cache.carregaAplicacoes,
		cache.carregaSegmentos, //Depende de aplicações
		cache.carregaVariaveis, //Depende de aplicações
		cache.carregaDDDsPorAplicacao, //Depende de aplicações
		cache.carregaOperacoes, //Depende de aplicações
		cache.carregaSites, //Independente
		cache.carregaVariaveis2,
		cache.carregaLegados,
		cache.carregaVarJSon,				
		cache.carregaTelCallFlow,							
		cache.carregaGruposSegmentos, //Independente
		cache.carregaTransferIds,
		cache.carregaEDsTransf,
		cache.carregaProdutos, //Independente
		cache.carregaGruposProdutos, //Independente
		cache.carregaDatUltConsolis, //Independente
		cache.ultimoMesDetalhamentoDeChamadas, //Independente
		cache.ultimoMesCallLogs, //Independente
		cache.releaseEstalo,				
		cache.carregaGrupoaAplicacoes,
		cache.carregaNodeExe,
		cache.carregaPscpExe,				
		cache.carregaEncerramentos,
		cache.carregaItensDeControleEstadosDeDialogo,
		cache.carregaExtratorPadrao,
		cache.carregaACLAplicacoes,
		cache.carregaACLBotoes,
		cache.carregaExibicaoCondicionalColunas,
		cache.carregaGruposMeta	
	]);	
	if (err) return;

}

function miniQuery(apl){
var tempQuery = "select Cod_Item,Descricao from ItemDeControle WHERE DataInclusao>='" + new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + "01' AND cod_aplicacao='" + apl + "'";
   mssqlQueryTedious(tempQuery, function (err, resultado) {
	 if (err) console.log(err);
	 for (var i = 0; i < resultado.length; i++) {
		 if(cache.aplicacoes[apl]['itens_controle'] !== undefined){
			 cache.aplicacoes[apl]['itens_controle'].push(
			 {
			 codigo: resultado[i].Cod_Item.value,
			 descricao: resultado[i].Descricao.value,
			 aplicacao: apl
			 }
			 );
			 try{
				 cache.aplicacoes[apl]['itens_controle'].indice[resultado[i].Cod_Item.value] = {
				 codigo: resultado[i].Cod_Item.value,
				 descricao: resultado[i].Descricao.value,
				 aplicacao: apl
				 };
			 }catch(ex){			
			 }			 
		 }
	 }

	 var tempQuery = "select Cod_Estado,Nom_Estado, Indic_Exibe from Estado_Dialogo WHERE DataInclusao>='" + new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + "01' AND cod_aplicacao='" + apl + "'";
	 mssqlQueryTedious(tempQuery, function (err, resultado) {
	   if (err) console.log(err);
	   for (var i = 0; i < resultado.length; i++) {
		   if(cache.aplicacoes[apl]['estados'] !== undefined){
			   cache.aplicacoes[apl]['estados'].push({
			   codigo: resultado[i].Cod_Estado.value,
			   nome: resultado[i].Nom_Estado.value,
			   aplicacao: apl,
			   exibir : (resultado[i].Indic_Exibe.value === "S")
				});
				
			try{				
			 cache.aplicacoes[apl]['estados'].indice[resultado[i].Cod_Estado.value] = {
			 codigo: resultado[i].Cod_Estado.value,
			   nome: resultado[i].Nom_Estado.value,
			   aplicacao: apl,
			   exibir : (resultado[i].Indic_Exibe.value === "S")
			 };
			}catch(ex){
				//console.log(ex);
			}
		   }
	   }
	 });
   });
}

function statusInfo(msg){
	$('.textoInfo').text(msg);
	$('.textoInfo').css({"color":"white","position":"relative","margin-top":"400px"});
}