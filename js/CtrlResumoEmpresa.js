function CtrlResumoEmpresa($scope, $globals) {

	win.title="Resumo por empresa"; //Alex 27/02/2014
	$scope.versao = versao;

	$scope.rotas = rotas;

	$scope.limite_registros = 0;
	$scope.incrementoRegistrosExibidos = 100;
	$scope.numRegistrosExibidos = $scope.incrementoRegistrosExibidos;

	//Alex 08/02/2014
	//var teste_data = dataUltimaConsolidacao("ResumoPH"); //$scope, relatorio
	travaBotaoFiltro(0, $scope, "#pag-resumo-empresa", "Resumo por empresa");

	//Alex 24/02/2014
	$scope.status_progress_bar = 0;

	//$scope.limite_registros = 500;

	$scope.dados = [];
	$scope.dadosPivot = [];

	$scope.pivot = false;

	$scope.colunas = [];

	$scope.gridDados = {
		headerTemplate: base + 'header-template.html',
		category: $scope.category,
		data: $scope.dados,
		columnDefs: $scope.colunas,
		enableRowHeaderSelection: false,
		enableSelectAll: true,
		multiSelect : true,
		enableRowSelection : true,
		enableFullRowSelection : true,
		multiSelect : true,
		modifierKeysToMultiSelect : true
	};


  $scope.total_qtdDerivadas;
  $scope.total_qtdTransferidas;
  $scope.total_qtdNEncontradas;
  $scope.total_totalGeral;

	$scope.log = [];
	$scope.aplicacoes = []; // FIXME: copy
	$scope.operacoes = [];
	$scope.segmentos = [];
	$scope.ordenacao = ['data_hora'];
	$scope.decrescente = false;
	$scope.iit = false;
	$scope.parcial = false;

	// Filtros
	$scope.filtros = {
		aplicacoes: [],
		// ALEX - 29/01/2013
		operacoes: [],
		segmentos: [],
		isHFiltroED: false,
		porDia: false,
		porDDD: false
	};

	// Filtro: ddd
	$scope.ddds = cache.ddds;

	$scope.porDDD = null;

	//02/06/2014 Flag
	$scope.porRegEDDD = $globals.agrupar;

	$scope.aba = 2;

	var distinctEmpresas = [];
	var empresas = cache.empresas.map(function(a){return a.codigo});

	$scope.valor = "regra";



	function geraColunas(){
	  var array = [];

	  var selectEmpresas = distinctEmpresas;

	  array.push({ field: "datReferencia", displayName: "Data", pinnedLeft:true, width: 150 });


		//cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{(row.getProperty(col.name)|| 0) | number}}</span></div>',

	array.push(
		  { field: "qtdDerivadas_TOTAL", displayName: "Qtd Op1", width: 95,  category: "TOTAL", cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>'},
		  { field: "qtdTransferidas_TOTAL", displayName: "Qtd Op2", width: 95,  category: "TOTAL", cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' },
		  { field: "qtdPercTransferEPS_TOTAL", displayName: "% Op2", width: 95,  category: "TOTAL", cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' }
		);



	  for(var i= 0; i<selectEmpresas.length;i++){
		array.push(
		  { field: "qtdDerivadas_"+selectEmpresas[i]+"", displayName: "Qtd Op1", width: 95, category: ""+obtemNomeEmpresa(selectEmpresas[i]), cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' },
		  { field: "qtdTransferidas_"+selectEmpresas[i]+"", displayName: "Qtd Op2", width: 95, category: ""+obtemNomeEmpresa(selectEmpresas[i]), cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' },
		  { field: "qtdPercTransferEPS_"+selectEmpresas[i]+"", displayName: "% Op2", width: 95, category: ""+obtemNomeEmpresa(selectEmpresas[i]), cellTemplate: '<div class="ui-grid-cell-contents">{{(COL_FIELD || 0) | number}}</div>' }
		);
	  }
	  return array;
	}

	function geraCategorias(){
		var array = [];
		var selectEmpresas = distinctEmpresas;
		array.push({name: "TOTAL", visible: true});
		for(var i= 0; i<selectEmpresas.length;i++){
			array.push({name: obtemNomeEmpresa(selectEmpresas[i]), visible: true});
		}
		return array;
	}

	// Filtros: data e hora
	var agora = new Date();

	//var dat_consoli = new Date(teste_data);

	//Alex 03/03/2014
	var inicio, fim;
	Estalo.filtros.filtro_data_hora[0]!==undefined ? inicio = Estalo.filtros.filtro_data_hora[0] : inicio = ontemInicio();
	Estalo.filtros.filtro_data_hora[1]!==undefined ? fim = Estalo.filtros.filtro_data_hora[1] : fim = ontemFim();

		$scope.periodo = {
			inicio:inicio,
			fim: fim,
			min: new Date(2013, 11, 1),
			max: agora // FIXME: atualizar ao virar o dia
			/*min: ontem(dat_consoli),
			max: dat_consoli*/
		};

	var $view;


	$scope.$on('$viewContentLoaded', function () {



	  $view = $("#pag-resumo-empresa");
	  


	   $(".aba2").css({'position':'fixed','left':'47px','top':'42px'});


		$(".aba4").css({'position':'fixed','left':'750px','top':'40px'});
		$(".aba5").css({'position':'fixed','left':'55px','right':'auto','margin-top':'35px','z-index':'1'});
		$('.navbar-inner').css('height','70px');
		$(".botoes").css({'position':'fixed','left':'auto','right':'25px','margin-top':'35px'});

		//minuteStep: 5

	  //19/03/2014
	  componenteDataHora ($scope,$view);

		carregaRegioes($view);
		carregaAplicacoes($view,false,false,$scope);
		carregaOperacoesECH($view);
		carregaSites($view);
		carregaEmpresas($view);
		carregaRegras($view);
		carregaSkills($view);



		carregaSegmentosPorAplicacao($scope,$view,true);
		// Popula lista de segmentos a partir das aplicações selecionadas
		$view.on("change", "select.filtro-aplicacao", function(){ carregaSegmentosPorAplicacao($scope,$view)});


	  carregaDDDsPorAplicacao($scope,$view,true);
	  // Popula lista de  DDDs a partir das aplicações selecionadas
	  $view.on("change", "select.filtro-aplicacao", function(){ carregaDDDsPorAplicacao($scope,$view)});


	  $view.on("change","input:radio[name=radioB]",function() {

		if($(this).val() === "empresa"){
		  $('#porDDD').attr('disabled', 'disabled');
		  $('#porDDD').prop('checked',false);
		}else{
		  $('#porDDD').attr('disabled', false);
		}

	  });


		

		$view.find("select.filtro-aplicacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Aplicações',
			showSubtext: true
		});

		$view.find("select.filtro-operacao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Operações',
			showSubtext: true
		});

		$view.find("select.filtro-ddd").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} DDDs',
			showSubtext: true
		});

		$view.find("select.filtro-regiao").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Regiões',
			showSubtext: true
		});

		$view.find("select.filtro-ed").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} EDs'
		});


		$view.find("select.filtro-empresa").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Empresas'
		});

		$view.find("select.filtro-regra").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Regras'
		});

		$view.find("select.filtro-skill").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Skills'
		});
		
		$view.find("select.filtro-site").selectpicker({
			selectedTextFormat: 'count',
			countSelectedText: '{0} Sites/POPs',
			showSubtext: true
		});
		
		
		$view.on("change", "select.filtro-site", function () {
            var filtro_sites = $(this).val() || [];
            Estalo.filtros.filtro_sites = filtro_sites;
        });

		$view.on("change", "select.filtro-operacao", function () {
			var filtro_operacoes = $(this).val() || [];
			Estalo.filtros.filtro_operacoes = filtro_operacoes;
		});

		$view.on("change", "select.filtro-ddd", function () {
			var filtro_ddds = $(this).val() || [];
			Estalo.filtros.filtro_ddds = filtro_ddds;
		});

		$view.on("change", "select.filtro-regiao", function () {
			var filtro_regioes = $(this).val() || [];
			Estalo.filtros.filtro_regioes = filtro_regioes;
		});

		//GILBERTOO - change de segmentos
		$view.on("change", "select.filtro-segmento", function () {
			Estalo.filtros.filtro_segmentos = $(this).val();
		});

	  //2014-11-27 transição de abas
	  var abas = [2,3,4];

	  $view.on("click", "#alinkAnt", function () {

		if($scope.aba === abas[0]) $scope.aba = abas[abas.length-1] + 1;
		if($scope.aba > abas[0]){
		  $scope.aba--;
		  mudancaDeAba();
		}
	  });

	  $view.on("click", "#alinkPro", function () {

		if($scope.aba === abas[abas.length-1])  $scope.aba = abas[0] - 1;

		if($scope.aba < abas[abas.length-1]){
		  $scope.aba++;
		  mudancaDeAba();
		}

	  });

	  function mudancaDeAba(){
		abas.forEach(function(a){
		  if($scope.aba === a){
			$('.nav.aba'+a+'').fadeIn(500);
		  }else{
			$('.nav.aba'+a+'').css('display','none');
		  }
		});
	  }


		// Marca todos os ddds
		$view.on("click", "#alinkDDD", function(){ marcaTodosIndependente($('.filtro-ddd'),'ddds')});

		//Bernardo 20-02-2014 Marcar todas as regiões
		$view.on("click", "#alinkReg", function(){ marcaTodasRegioes($('.filtro-regiao'),$view,$scope)});

		// Marca todos os operacoes
		$view.on("click", "#alinkOper", function(){ marcaTodosIndependente($('.filtro-operacao'),'operacoes')});
		
		// Marca todos os sites
        $view.on("click", "#alinkSite", function(){ marcaTodosIndependente($('.filtro-site'),'sites')});

		// Marca todos os segmentos
		$view.on("click", "#alinkSeg", function(){ marcaTodosIndependente($('.filtro-segmento'),'segmentos')});

		// Marca todos os empresas
		$view.on("click", "#alinkEmp", function(){ marcaTodosIndependente($('.filtro-empresa'),'empresa')});

		// Marca todos os regras
		$view.on("click", "#alinkRegra", function(){ marcaTodosIndependente($('.filtro-regra'),'regra')});

		// Marca todos os skills
		$view.on("click", "#alinkSkill", function(){ marcaTodosIndependente($('.filtro-skill'),'skill')});

		//Marcar todas aplicações
		$view.on("click", "#alinkApl", function(){ marcaTodasAplicacoes($('.filtro-aplicacao'),$view,$scope)});


		// GILBERTO 18/02/2014



	  // EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
			//11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
			$('div.btn-group.filtro-ddd').addClass('open');
			$('div.btn-group.filtro-ddd>div>ul').css({'max-height':'600px','overflow-y':'auto','min-height':'1px'});
		  }

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
			$('div.btn-group.filtro-ddd').removeClass('open');
		});


		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseover(function () {

			$("div.data-inicio.input-append.date").data("datetimepicker").hide();
			$("div.data-fim.input-append.date").data("datetimepicker").hide();
			$('div.btn-group.filtro-aplicacao').addClass('open');
			$('div.btn-group.filtro-aplicacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-aplicacao').mouseout(function () {
			$('div.btn-group.filtro-aplicacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseover(function () {
			$('div.btn-group.filtro-operacao').addClass('open');
			$('div.btn-group.filtro-operacao>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-operacao').mouseout(function () {
			$('div.btn-group.filtro-operacao').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseover(function () {
		  //11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-segmento .btn').hasClass('disabled')){
			$('div.btn-group.filtro-segmento').addClass('open');
			$('div.btn-group.filtro-segmento>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '350px' });
		  }
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-segmento').mouseout(function () {
			$('div.btn-group.filtro-segmento').removeClass('open');
		});
		
		// EXIBIR AO PASSAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseover(function () {
            $('div.btn-group.filtro-site').addClass('open');
        });

        // OCULTAR AO TIRAR O MOUSE
        $('div.container > ul > li >>div> div.btn-group.filtro-site').mouseout(function () {
            $('div.btn-group.filtro-site').removeClass('open');
        });

		// EXIBIR AO PASSAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseover(function () {
		  //11/07/2014 não mostrar filtros desabilitados
		  if(!$('div.btn-group.filtro-ddd .btn').hasClass('disabled')){
			$('div.btn-group.filtro-ddd').addClass('open');
			$('div.dropdown-menu.open').css({ 'margin-left':'-110px' });
			$('div.btn-group.filtro-ddd>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
		  }
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div > ul > li >>div> div.btn-group.filtro-ddd').mouseout(function () {
			$('div.dropdown-menu.open').css({ 'margin-left':'0px' });
			$('div.btn-group.filtro-ddd').removeClass('open');
		});



		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseover(function () {
			$('div.btn-group.filtro-regiao').addClass('open');
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regiao').mouseout(function () {
			$('div.btn-group.filtro-regiao').removeClass('open');
		});


		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseover(function () {
			$('div.btn-group.filtro-empresa').addClass('open');

		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-empresa').mouseout(function () {
			$('div.btn-group.filtro-empresa').removeClass('open');

		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseover(function () {
			$('div.btn-group.filtro-regra').addClass('open');
			$('div.btn-group.filtro-regra>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-regra').mouseout(function () {
			$('div.btn-group.filtro-regra').removeClass('open');
		});

		// EXIBIR AO PASSAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseover(function () {
			$('div.btn-group.filtro-skill').addClass('open');
			$('div.btn-group.filtro-skill>div>ul').css({ 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px', 'max-width': '400px' });
		});

		// OCULTAR AO TIRAR O MOUSE
		$('div.container > ul > li >>div> div.btn-group.filtro-skill').mouseout(function () {
			$('div.btn-group.filtro-skill').removeClass('open');
		});


		// EXIBIR AO PASSAR O MOUSE estado de dialogo
		$('div.btn-group.filtro-ed').mouseover(function () {
			//11/07/2014 não mostrar filtros desabilitados
			if (!$('div.btn-group.filtro-ed .btn').hasClass('disabled')) {
				$('div.btn-group.filtro-ed').addClass('open');
				$('div.btn-group.filtro-ed>div>ul').css(
				  { 'max-width': '500px', 'max-height': '500px', 'overflow-y': 'auto', 'min-height': '1px' }
				);
			}
		});



		// OCULTAR AO TIRAR O MOUSE estado de dialogo
		$('div.btn-group.filtro-ed').mouseout(function () {
			$('div.btn-group.filtro-ed').removeClass('open');
		});


		$view.on("click", ".btn-exportar", function () {
			$scope.exportaXLSX.apply(this);
		});

		// Limpa filtros Alex 03/03/2014
		$view.on("click", ".btn-limpar-filtros", function () {
		  $scope.porRegEDDD = false;
		  $scope.limparFiltros.apply(this);
		});

		$scope.limparFiltros = function () {
			iniciaFiltros();
			componenteDataHora ($scope,$view,true);

			var partsPath = window.location.pathname.split("/");
			var part  = partsPath[partsPath.length-1];

			var $btn_limpar = $view.find(".btn-limpar-filtros");
			$btn_limpar.prop("disabled", true);
			$btn_limpar.button('loading');

			setTimeout(function(){
				$btn_limpar.button('reset');
				//window.location.href = part + window.location.hash +'/';
			},500);
		}

		// Botão agora Alex 03/03/2014
		$view.on("click", ".btn-agora", function () {
			$scope.agora.apply(this);
		});

		$scope.agora = function () {
			iniciaAgora($view,$scope);
		}

		// Botão abortar Alex 23/05/2014
		$view.on("click", ".abortar", function () {
			$scope.abortar.apply(this);
		});

		//Alex 23/05/2014
		$scope.abortar = function () {
		abortar($scope, "#pag-resumo-empresa");
		}

		// Lista chamadas conforme filtros
		$view.on("click", ".btn-gerar", function () {

		  $scope.pivot = false;
		  limpaProgressBar($scope, "#pag-resumo-empresa");
			//22/03/2014 Testa se data início é maior que a data fim
			var data_ini = $scope.periodo.inicio, data_fim = $scope.periodo.fim;
			var testedata = testeDataMaisHora(data_ini,data_fim);
			if(testedata!==""){
				setTimeout(function(){
					atualizaInfo($scope,testedata);
					effectNotification();
					$view.find(".btn-gerar").button('reset');
				},500);
				return;
			}
			$scope.listaDados.apply(this);
		});







		$view.find("div.iframe").html('<iframe name="xlsxjs" src="' + base + 'xlsx.html" frameborder="0" scrolling="no" />');
	});


	// Exibe mais registros
	$scope.exibeMaisRegistros = function () {
	  $scope.numRegistrosExibidos += $scope.incrementoRegistrosExibidos;
	};








	// Lista chamadas conforme filtros
	$scope.listaDados = function () {

		distinctEmpresas = [];



		$globals.numeroDeRegistros = 0;



		var $btn_exportar = $view.find(".btn-exportar");
		$btn_exportar.prop("disabled", true);

		var $btn_gerar = $(this);
		$btn_gerar.button('loading');

		var data_ini = $scope.periodo.inicio,
		data_fim = $scope.periodo.fim;

		var filtro_aplicacoes = $view.find("select.filtro-aplicacao").val() || [];
		var filtro_operacoes = $view.find("select.filtro-operacao").val() || [];
		var filtro_sites = $view.find("select.filtro-site").val() || [];
		var filtro_segmentos = $view.find("select.filtro-segmento").val() || [];
		var filtro_regioes = $view.find("select.filtro-regiao").val() || [];
		var filtro_ddds = $view.find("select.filtro-ddd").val() || [];
		var original_ddds = $view.find("select.filtro-ddd").val() || [];
		var filtro_ed = $view.find("select.filtro-ed").val() || [];
		var filtro_empresas = $view.find("select.filtro-empresa").val() || [];
		var filtro_regras = $view.find("select.filtro-regra").val() || [];
		var filtro_skills = $view.find("select.filtro-skill").val() || [];


		//filtros usados
		$scope.filtros_usados = " Período: " + formataDataHoraBR($scope.periodo.inicio) + ""
		+ " até " + formataDataHoraBR($scope.periodo.fim);
		if (filtro_aplicacoes.length > 0) { $scope.filtros_usados += " Aplicações: " + filtro_aplicacoes; }
		if (filtro_operacoes.length > 0) { $scope.filtros_usados += " Operações: " + filtro_operacoes; }
		if (filtro_sites.length > 0) { $scope.filtros_usados += " Sites: " + filtro_sites; }
		if (filtro_segmentos.length > 0) { $scope.filtros_usados += " Segmentos: " + filtro_segmentos; }
		if (filtro_ddds.length > 0) { $scope.filtros_usados += " DDDs: " + filtro_ddds; }
		if (filtro_ed.length > 0) { $scope.filtros_usados += " EDs: " + filtro_ed; }
		if (filtro_empresas.length > 0) { $scope.filtros_usados += " Empresas: " + filtro_empresas; }
		if (filtro_regras.length > 0) { $scope.filtros_usados += " Regras: " + filtro_regras; }
		if (filtro_skills.length > 0) {

		  var filtro_skills_mod = [];
		  for(var i = 0; i < filtro_skills.length; i++){
			var lsRegExp = /%/g;
			filtro_skills_mod.push(filtro_skills[i].replace(lsRegExp,'&lt;'));
		  }
		  $scope.filtros_usados += " Skills: " + filtro_skills_mod;
		}

		if(filtro_regioes.length > 0 && filtro_ddds.length === 0){
		  var options = [];
		  var v = [];
		  filtro_regioes.forEach(function (codigo) { v = v.concat(unique2(cache.ddds.map(function(ddd){ if(ddd.regiao === codigo){ return ddd.codigo } }))  || []); });
		  unique(v).forEach((function (filtro_ddds) {
			return function (codigo) {
			  var ddd = cache.ddds.indice[codigo];
			  filtro_ddds.push(codigo);
			};
		  })(filtro_ddds));
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		}else if(filtro_regioes.length > 0 && filtro_ddds.length !== 0){
		  $scope.filtros_usados += " Regiões: " + filtro_regioes;
		  $scope.filtros_usados += " DDDs: " + filtro_ddds;
		}


		$scope.dados = [];
		var stmt = "";
		var executaQuery = "";
		$scope.datas = [];
		$scope.totais = { geral: { qtd_entrantes: 0 } };




		stmt = db.use
		+ " SELECT "+($scope.filtros.porDia ? "convert(date,DatReferencia) as DatReferencia" : "DatReferencia")+""

		//+ ", CodAplicacao, CodSegmento, CodEmpresa, CodOperacao"
		+ ", CodEmpresa"
		+ ", SUM(QtdDerivadas) as qtdDer"
		+ ", SUM(QtdTransferidas) qtdTransf"
		+ ", SUM(qtdchamadas) as qtd"
		+ " FROM ResumoECH"
		+ " WHERE 1 = 1"
		+ " AND DatReferencia >= '" + formataDataHora(data_ini) + "'"
		+ " AND DatReferencia <= '" + formataDataHora(data_fim) + "'"
		+ " AND CodEmpresa <> ''"
		stmt += restringe_consulta("CodAplicacao", filtro_aplicacoes, true)
		stmt += restringe_consulta("CodOperacao", filtro_operacoes, true)
		stmt += restringe_consulta("CodSite", filtro_sites, true)
		stmt += restringe_consulta("CodSegmento", filtro_segmentos, true)
		stmt += restringe_consulta("CodEmpresa", filtro_empresas, true)
		stmt += restringe_consulta("CodRegra", filtro_regras, true)
		stmt += restringe_consulta("ddd", filtro_ddds, true)
		stmt += restringe_consulta_like2("CodSkill", filtro_skills)
		stmt += " GROUP BY "+($scope.filtros.porDia ? "convert(date,DatReferencia)" : "DatReferencia")+", CodEmpresa"
		stmt += " ORDER BY "+($scope.filtros.porDia ? "convert(date,DatReferencia)" : "DatReferencia")+", CodEmpresa";








		executaQuery = executaQuery2;
		log(stmt);


		function formataData2(data) {
			var y = data.getFullYear(),
			  m = data.getMonth() + 1,
			  d = data.getDate();
			return y + _02d(m) + _02d(d);
		}





		function executaQuery2(columns){


		  var data_hora = new Date(columns["DatReferencia"].value.length === 10 ? columns["DatReferencia"].value + " 00:00:00" : columns["DatReferencia"].value),
			  datReferencia = $scope.filtros.porDia ? formataDataBR(data_hora) : formataDataHoraBR(data_hora);


		  var empresa = (columns["CodEmpresa"].value).toUpperCase(),
			  qtdDerivadas = +columns["qtdDer"].value,
			  qtdTransferidas = +columns["qtdTransf"].value;//,
			  //totalGeral = qtdDerivadas + qtdTransferidas;

		  var  qtdPercTransferEPS = 0;


		  if(distinctEmpresas.indexOf(empresa)< 0 && (filtro_empresas.indexOf(empresa)>=0 || filtro_empresas.length === 0)) distinctEmpresas.push(empresa);

		  if (qtdDerivadas > 0){
			qtdPercTransferEPS = (100 * qtdTransferidas / qtdDerivadas).toFixed(1);
		  }
			$scope.total_qtdDerivadas = $scope.total_qtdDerivadas + qtdDerivadas;
			//$scope.total_totalGeral = $scope.total_totalGeral + totalGeral;



			var obj = { data_hora: data_hora, datReferencia: datReferencia };
			obj["qtdDerivadas_"+empresa] = qtdDerivadas;
			obj["qtdTransferidas_"+empresa] = qtdTransferidas;
			obj["qtdPercTransferEPS_"+empresa] = qtdPercTransferEPS;
			$scope.dados.push(obj);
			/*var obj = {};
			eval("var obj = '{ \"data_hora\": \"" + data_hora + "\", \"datReferencia\": \"" + datReferencia + "\", \"qtdDerivadas_"+empresa+"\": \"" + qtdDerivadas + "\", \"qtdTransferidas_"+empresa+"\": \"" + qtdTransferidas + "\", \"qtdPercTransferEPS_"+empresa+"\": \"" + qtdPercTransferEPS + "\" }'");
			$scope.dados.push(JSON.parse(obj));*/

		  if($scope.dados.length%1000===0){
			$scope.$apply();
		  }
		}


	$scope.total_qtdDerivadas = 0;
	$scope.total_qtdTransferidas = 0;
	//$scope.total_totalGeral = 0;


				db.query(stmt,executaQuery, function (err, num_rows) {
				    userLog(stmt, 'Carrega dados', 2, err)

				  var objetoAgrupado = _.groupBy($scope.dados,'datReferencia');
				  dadosTemp = [];

				  for (m in objetoAgrupado){
					var item = {};
					var item2 = {};
					for (var i = 0; i < objetoAgrupado[m].length; i++) {
					  jQuery.extend(item, objetoAgrupado[m][i]);
					}


		  var totalDer = 0;
		  var totalTrans = 0;
		  var totalTransferEPS = 0;

		  for (var i = 0; i < distinctEmpresas.length; i++) {
					  totalDer += parseInt(item['qtdDerivadas_'+distinctEmpresas[i]+'']) || 0;
			totalTrans += parseInt(item['qtdTransferidas_'+distinctEmpresas[i]+'']) || 0;
					}

		  if(totalDer > 0) totalTransferEPS = (100 * totalTrans / totalDer).toFixed(1);
		  item2 = {qtdDerivadas_TOTAL: totalDer, qtdTransferidas_TOTAL: totalTrans, qtdPercTransferEPS_TOTAL: totalTransferEPS};
					dadosTemp.push(jsonConcat(item,item2));
					//console.log(item);
					item = {};

				  }

				  $scope.dados = dadosTemp;
				  $($('.ngHeaderContainer')[0]).css('height','30px');
				  retornaStatusQuery(num_rows, $scope);
				  $btn_gerar.button('reset');

				  if(num_rows>0){
					$scope.colunas = geraColunas();
					$scope.category =  geraCategorias();
					$scope.gridDados.data = $scope.dados;
					$scope.gridDados.columnDefs = $scope.colunas;
					$scope.gridDados.category = $scope.category;
					$scope.$apply();
					  
					$btn_exportar.prop("disabled", false);
				  }else{
					$($('.ngHeaderContainer')[0]).css('height','0px');
				  }

				});


			// GILBERTOOOOOO 17/03/2014
			$view.on("mouseup", "tr.resumo", function () {
			  var that = $(this);
			  $('tr.resumo.marcado').toggleClass('marcado');
			  $scope.$apply(function () {
				that.toggleClass('marcado');
			  });
			});
	};




	// Exportar planilha XLSX
	$scope.exportaXLSX = function () {

		var $btn_exportar = $(this);

		$btn_exportar.button('loading');

	  var linhas  = [];


	  var header1 = distinctEmpresas;
	  var header2 = ['Qtd Op1','Qtd Op2','% Op2'];

	  var h =[];

	  h.push({value:  ""});

	h.push({value: 'TOTAL' , colSpan : 3, hAlign: 'center'});

		for(var i= 0; i<header1.length;i++){
		  h.push({value: obtemNomeEmpresa(header1[i]) , colSpan : 3, hAlign: 'center'});
		}

		linhas.push(h);

	      var h2 = [];

		  h2.push({value:  "Data", autoWidth:true});

		  h2.push({value: 'Qtd Op1', autoWidth:true});
		  h2.push({value: 'Qtd Op2' , autoWidth:true});
		  h2.push({value: '% Op2' , autoWidth:true});

		for(var i= 0; i<header1.length;i++){
		  h2.push({value: 'Qtd Op1', autoWidth:true});
		  h2.push({value: 'Qtd Op2' , autoWidth:true});
		  h2.push({value: '% Op2' , autoWidth:true});
		}
		linhas.push(h2);



		$scope.dados.map(function (dado) {
		  var l = [];
		  l.push({value:dado.datReferencia, autoWidth:true});

			l.push({value:(dado['qtdDerivadas_TOTAL'] === undefined ? 0 : dado['qtdDerivadas_TOTAL'])});
			l.push({value:(dado['qtdTransferidas_TOTAL'] === undefined ? 0 : dado['qtdTransferidas_TOTAL'])});
			l.push({value:(dado['qtdPercTransferEPS_TOTAL'] === undefined ? 0 : dado['qtdPercTransferEPS_TOTAL'])});

		  for(var i= 0; i<header1.length;i++){
			l.push({value:(dado['qtdDerivadas_'+header1[i]+''] === undefined ? 0 : dado['qtdDerivadas_'+header1[i]+''])});
			l.push({value:(dado['qtdTransferidas_'+header1[i]+''] === undefined ? 0 : dado['qtdTransferidas_'+header1[i]+''])});
			l.push({value:(dado['qtdPercTransferEPS_'+header1[i]+''] === undefined ? 0 : dado['qtdPercTransferEPS_'+header1[i]+''])});
		  }
		  return linhas.push(l);
		});








		var planilha = {
			creator: "Estalo",
			lastModifiedBy: $scope.username || "Estalo",
			worksheets: [{ name: 'ResEmp', data: linhas, table: false }],
			autoFilter: false,
			// Não incluir a linha do título no filtro automático
			dataRows: { first: 1 }
		};

		var xlsx = frames["xlsxjs"].window.xlsx;
		planilha = xlsx(planilha, 'binary');


		var milis = new Date();
		var file = 'resumoEmpresa_' + formataDataHoraMilis(milis) + '.xlsx';


		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, planilha.base64, 'base64');
			childProcess.exec(file);
		}

		setTimeout(function () {
			$btn_exportar.button('reset');
		}, 500);
	};
  //empresas = cache.empresas.map(function(a){return a.codigo});

}

CtrlResumoEmpresa.$inject = ['$scope', '$globals'];
