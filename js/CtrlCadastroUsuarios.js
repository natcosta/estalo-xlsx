function CtrlCadastroUsuarios($scope, $globals) {
  win.title = "Cadastro de Usuários";
  $scope.versao = versao;

  $scope.usuarioSelecionado = {};
  $scope.usuarioSemEdicao = {};
  $scope.usuarioPermissoes = {};
  $scope.usuarioPermissoesAposEdicao = {};
  $scope.rotas = rotasInit;
  $scope.permissoes = {};
  $scope.arrPermissoesUsuario = [];
  $scope.loginCopiaPermissoes = "";
  $scope.loginCopiaPermissoesNovo = "";
  $scope.novoUsuario = {};
  $scope.mostrarInputCopia = false;
  $scope.grupos = [];

  $scope.globalPerms = rotasInit || rotas; // Permissões globais extraidas do rotasInit
  $scope.dataGridPerms = []; // Dados do grid de permissões trabalhado em listaPerms

  // Arrays com informações do usuário aberto
  $scope.userPerms = []; // Permissões do usuário em edição
  $scope.newPerms = [];
  $scope.userGroups = []; // Grupos do usuário em edição
  $scope.solicitacoes = [];
  $scope.requestPerms = []; // Permissões requisitadas
  //Fim da lista de Arrays com informações do usuário aberto

  //requests
  $scope.reqTelefone = "";
  $scope.reqEmail = "";
  $scope.reqLogin = "";
  $scope.reqDominio = "";
  $scope.reqEmpresa = "";
  $scope.reqNome = "";
  $scope.request = false; //Determina se é uma solicitação de cadastro do estalo ou não
  $scope.userTarget = "";
  $scope.domainTarget = "";
  $scope.userOrigem = "";
  $scope.domainOrigem = "";
  //fim dos requests

  //Início Grids
  $scope.gridUsuarios = {
    rowSelection: "single",
    enableColResize: true,
    enableSorting: true,
    enableFilter: true,
    onRowClicked: abreModalEdicaoUsuario,
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
    },
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    overlayNoRowsTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    columnDefs: [
      // { headerName: '', width: 30, checkboxSelection: true, supressSorting: true, supressMenu: true, pinned: true },
      { headerName: "Nome", field: "nome" },
      { headerName: "Login", field: "login" },
      { headerName: "Domínio", field: "dominio" },
      { headerName: "Empresa", field: "empresa" },
      { headerName: "Email", field: "email" },
      { headerName: "Telefone", field: "telefone" }
    ],
    rowData: [],
    localeText: {
      contains: "Contém",
      notContains: "Não contém",
      equals: "Igual",
      notEquals: "Diferente",
      startsWith: "Começa com",
      endsWith: "Termina com"
    }
  };

  $scope.gridPermissoes = {
    rowSelection: "multiple",
    enableColResize: true,
    enableSorting: true,
    enableRowSelection: true,
    suppressRowClickSelection: true,
    suppressCellSelection: true,
    onRowClicked: permissaoSelecionada,
    rowHeight: 50,
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    overlayNoRowsTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
    },
    columnDefs: [
      { headerName: "Habilitado", checkboxSelection: true, width: 37 },
      { headerName: "Grupo", field: "grupo", width: 150 },
      { headerName: "Relatório", field: "relatorio", width: 719 },
      { headerName: "Form", field: "form", width: 1, hide: true }
    ],
    rowData: []
  };

  //Colunas do grid de permissões
  $scope.reqPermissions = [
    {
      field: "detalhes",
      displayName: "Relatórios",
      cellClass: "grid-align",
      width: "90%",
      pinned: false,
      cellTemplate:
        '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm row{{row.rowIndex+1}}">{{row.getProperty("detalhes")}}</span></div>'
    },
    {
      field: "checked",
      displayName: "Habilitado",
      cellClass: "grid-align habilitado",
      width: "10%",
      pinned: false,
      cellTemplate:
        '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="ngCellPerm" ><input type="checkbox" id="{{row.getProperty(\'form\')}}" class="PermissionCheckBox RequestCheckBox" ng-model="form" ng-change=""></span></div>'
    }
    // cellClass:"span2", width: 80,
  ];

  //Grid de permissões
  $scope.gridReqPerms = {
    data: "dataGridPerms",
    columnDefs: "reqPermissions",
    enableColumnResize: false,
    enablePinning: false,
    rowHeight: 60
  };

  $scope.requests = [
    {
      field: "Nome",
      displayName: "Nome",
      cellClass: "grid-align",
      pinned: true
    }, // cellClass:"span1", width: 150,
    {
      field: "LoginUsuario",
      displayName: "Login",
      cellClass: "grid-align",
      pinned: true
    }, // cellClass:"span2", width: 80,
    {
      field: "Dominio",
      displayName: "Domínio",
      cellClass: "grid-align",
      pinned: true
    }, //  cellClass:"span1", width: 80,
    {
      field: "Empresa",
      displayName: "Empresa",
      cellClass: "grid-align",
      pinned: true
    }, // cellClass:"span2",width: 80,
    {
      field: "EmailUsuario",
      displayName: "Email",
      cellClass: "grid-align",
      pinned: false
    }, //  cellClass:"span4", width: 120,
    //{ field: "Cpf", displayName: "CPF", cellClass: "grid-align", pinned: false},//  cellClass:"span4", width: 120,
    {
      field: "Tools",
      displayName: "Funções",
      cellClass: "grid-align",
      width: 80,
      pinned: false,
      cellTemplate:
        '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-userRequest" ng-click="reqUser(row.entity)" ng-model="reqUserButton" title="Verificar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>'
    } // cellClass:"span2", width: 80,
  ];

  $scope.gridRequests = {
    data: "solicitacoes",
    columnDefs: "requests",
    enableColumnResize: false,
    rowHeight: 60
  };

  function permissaoSelecionada(event) {
    if (event.node.isSelected()) {
      event.node.setSelected(false, false);
    } else {
      event.node.setSelected(true);
    }
  }

  $scope.gridPermissoesNovoUsuario = {
    rowSelection: "multiple",
    enableColResize: true,
    enableSorting: true,
    enableRowSelection: true,
    suppressRowClickSelection: true,
    suppressCellSelection: true,
    onRowClicked: permissaoSelecionada,
    rowHeight: 50,
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    overlayNoRowsTemplate:
      '<span class="ag-overlay-loading-center">Aguarde enquanto os dados são carregados.</span>',
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
    },
    columnDefs: [
      { headerName: "Habilitado", checkboxSelection: true, width: 37 },
      { headerName: "Grupo", field: "grupo", width: 150 },
      { headerName: "Relatório", field: "relatorio", width: 719 },
      { headerName: "Form", field: "form", width: 1, hide: true }
    ],
    rowData: []
  };
  //Fim Grids

  $scope.abreModalNovoUsuario = function() {
    $scope.formataPermissoes();
    $scope.loginCopiaPermissoesNovo = "";
    $scope.gridPermissoesNovoUsuario.rowData = [];
    $scope.gridPermissoesNovoUsuario.api.setRowData([]);
    $scope.novoUsuario = {
      nome: "",
      dominio: "",
      login: "",
      empresa: "",
      email: "",
      telefone: "",
      permissoes: []
    };
    for (var form in $scope.permissoes) {
      $scope.gridPermissoesNovoUsuario.rowData.push({
        relatorio: $scope.permissoes[form].nomes.join(),
        grupo: $scope.permissoes[form].grupo,
        form: form
      });
    }
    $scope.gridPermissoesNovoUsuario.api.setRowData(
      $scope.gridPermissoesNovoUsuario.rowData
    );
    $scope.gridPermissoesNovoUsuario.api.forEachNode(function(node) {
      if (
        node.data.form == "frmPesqChamURAVoz" ||
        node.data.form == "frmVisaoHoraGeral" ||
        node.data.form == "FrmDiaaDiaGeral"
      ) {
        node.setSelected(true);
      }
    });
    $scope.gridPermissoesNovoUsuario.api.refreshCells({ force: true });
    $("#modal-novo-usuario").modal("show");
  };

  $scope.salvaNovoUsuario = function() {
    $scope.novoUsuario.permissoes = [];
    var permissoesSelecionadas = null;
    if (!$scope.novoUsuario.login || !$scope.novoUsuario.dominio) {
      alerte("Login e Domínio são obrigatórios", "Erro");
      return;
    }
    var permissoesSelecionadas = $scope.gridPermissoesNovoUsuario.api.getSelectedRows();
    for (var i = 0; i < permissoesSelecionadas.length; i++) {
      $scope.novoUsuario.permissoes.push(permissoesSelecionadas[i].form);
    }

    var permissoesAlteradas = $scope.getMudancasDePermissao(
      [],
      $scope.novoUsuario.permissoes
    );
    if (permissoesAlteradas.insert.length) {
      var queryInsertPermissoes =
        "INSERT INTO PermissoesUsuariosEstatura VALUES ";
      for (var i = 0; i < permissoesAlteradas.insert.length; i++) {
        queryInsertPermissoes +=
          "('" +
          $scope.novoUsuario.login +
          "','" +
          $scope.novoUsuario.dominio +
          "','" +
          permissoesAlteradas.insert[i] +
          "'),";
      }
      queryInsertPermissoes =
        queryInsertPermissoes.slice(0, queryInsertPermissoes.length - 1) + ";";
    }

    if (permissoesAlteradas.delete.length) {
      var queryDeletePermissoes =
        "DELETE FROM PermissoesUsuariosEstatura " +
        "WHERE LoginUsuario = '" +
        $scope.novoUsuario.login +
        "' AND Dominio = '" +
        $scope.novoUsuario.dominio +
        "' AND Formulario IN (";
      for (var i = 0; i < permissoesAlteradas.delete.length; i++) {
        queryDeletePermissoes += "'" + permissoesAlteradas.delete + "',";
      }
      queryDeletePermissoes =
        queryDeletePermissoes.slice(0, queryDeletePermissoes.length - 1) + ");";
    }

    var queryInsertUsuario =
      "INSERT INTO UsuariosEstatura VALUES ('" +
      $scope.novoUsuario.login +
      "','" +
      $scope.novoUsuario.dominio +
      "','" +
      ($scope.novoUsuario.empresa || "") +
      "','" +
      ($scope.novoUsuario.nome || "") +
      "'," +
      "NULL,'N',GETDATE(),'" + //CPF, TipoUsuario, DataCadastro
      ($scope.novoUsuario.email || "") +
      "','" +
      ($scope.novoUsuario.telefone || "") +
      "'," +
      "NULL);"; //StatusUser

    // var queryInsertPermissoesNovosUsuarios =
    //   "INSERT INTO PermissoesUsuariosEstatura VALUES (";
    // for (var i = 0; i < $scope.novoUsuario.permissoes.length; i++) {
    //   queryInsertPermissoesNovosUsuarios +=
    //     "'" + $scope.novoUsuario.permissoes[i] + "',";
    // }
    // queryInsertPermissoesNovosUsuarios =
    //   queryInsertPermissoesNovosUsuarios.slice(
    //     0,
    //     queryInsertPermissoesNovosUsuarios.length - 1
    //   ) + ");";

    mssqlQueryTedious(queryInsertUsuario, function(err, res) {
      var erro = false;
      if (err) {
        erro = true;
        console.log("erro query insert usuário", err);
        alerte("Erro ao inserir novo usuário.", "Erro");
      }
      // mssqlQueryTedious(queryInsertPermissoesNovosUsuarios, function(err, res) {
      //   if (err) {
      //     erro = true;
      //     console.log("erro query insert permissões", err);
      //     alerte("Erro ao inserir permissões de novos usuários.", "Erro");
      //   }
      // });
      mssqlQueryTedious(queryDeletePermissoes, function(err, res) {
        if (err) {
          erro = true;
          console.log("erro query delete permissoes", err);
          alerte("Erro ao excluir permissões", "Erro");
        }
        mssqlQueryTedious(queryInsertPermissoes, function(err, res) {
          if (err) {
            erro = true;
            console.log(
              "erro query insert permissoes",
              queryInsertPermissoes
            );
            alerte("Erro ao inserir permissões", "Erro");
          }
        });
      });
      if (erro == false)
        alerte(
          "Usuário " +
            $scope.novoUsuario.nome +
            " cadastrado com o login " +
            $scope.novoUsuario.login +
            ".",
          "Sucesso"
        );
    });
    $("#modal-novo-usuario").modal("hide");
    // setTimeout($scope.getUsuarios(), 5000);
    $scope.getUsuarios();
    $scope.gridUsuarios.api.refreshCells({ force: true });
  };

  function abreModalEdicaoUsuario() {
    $scope.formataPermissoes();
    $scope.loginCopiaPermissoes = "";
    $scope.usuarioSelecionado = {};
    $scope.gridPermissoes.rowData = [];
    $scope.gridPermissoes.api.setRowData([]);
    $scope.loginCopiaPermissoes = "";

    var linhaSelecionada = $scope.gridUsuarios.api.getSelectedRows();

    $scope.usuarioSelecionado = {
      nome: linhaSelecionada[0].nome,
      dominio: linhaSelecionada[0].dominio,
      login: linhaSelecionada[0].login,
      empresa: linhaSelecionada[0].empresa,
      email: linhaSelecionada[0].email,
      telefone: linhaSelecionada[0].telefone,
      permissoes: []
    };
    $scope.usuarioSemEdicao = angular.copy($scope.usuarioSelecionado);
    $scope.usuarioPermissoes = {};

    queryPermissoes =
      " SELECT DISTINCT Formulario " +
      " FROM PermissoesUsuariosEstatura " +
      " WHERE LoginUsuario = '" +
      $scope.usuarioSelecionado.login +
      "'" +
      " AND Dominio = '" +
      $scope.usuarioSelecionado.dominio +
      "'" +
      " AND Formulario IS NOT NULL;";

    mssqlQueryTedious(queryPermissoes, function(err, res) {
      if (err) {
        console.log("erro query permissoes", err);
        alerte("Permissões não foram cadastradas.", "Erro");
      }
      $scope.usuarioPermissoes = $scope.getPermissoesDeUsuario(res);
      $scope.usuarioPermissoesAposEdicao = angular.copy(
        $scope.usuarioPermissoes
      );
      console.log("per", $scope.usuarioPermissoes);

      for (var form in $scope.usuarioPermissoes) {
        $scope.gridPermissoes.rowData.push({
          relatorio: $scope.usuarioPermissoes[form].nomes.join(),
          grupo: $scope.permissoes[form].grupo,
          form: form
        });
      }

      $scope.gridPermissoes.api.setRowData($scope.gridPermissoes.rowData);

      $scope.gridPermissoes.api.forEachNode(function(node) {
        var verificaPermissao = $scope.arrPermissoesUsuario.filter(function(
          el
        ) {
          return node.data.form == el;
        });
        if (verificaPermissao.length > 0) {
          $scope.usuarioSelecionado.permissoes.push(node.data.form);
          node.setSelected(true);
        }
      });
      $scope.usuarioSemEdicao.permissoes = angular.copy(
        $scope.usuarioSelecionado.permissoes
      );
      $scope.gridPermissoes.api.refreshCells();
    });
    $scope.$apply();
    $("#modal-edicao").modal("show");
  }

  $scope.getPermissoesDeUsuario = function(objQueryPermissoes) {
    var objNovoPermissoes = angular.copy($scope.permissoes);

    $scope.arrPermissoesUsuario = [];

    for (var i = 0; i < objQueryPermissoes.length; i++) {
      if (objNovoPermissoes[objQueryPermissoes[i][0].value]) {
        // verifica se o form existe nas rotas, já que há permissoes concedidas
        // a forms que não existem, provavelmente devido a erro de digitação
        objNovoPermissoes[objQueryPermissoes[i][0].value].habilitado = true;
      }
      $scope.arrPermissoesUsuario.push(objQueryPermissoes[i][0].value);
    }
    return objNovoPermissoes;
  };

  $scope.getUsuarios = function() {
    $scope.gridUsuarios.rowData = [];
    $scope.gridUsuarios.api.setRowData([]);
    var queryUsuarios =
      "select * from UsuariosEstatura order by datacadastro desc";
    mssqlQueryTedious(queryUsuarios, function(err, res) {
      if (err) {
        console.log("erro query usuários", err);
        alerte("Erro ao consultar usuários cadastrados", "Erro");
      }
      for (var i = 0; i < res.length; i++) {
        $scope.gridUsuarios.rowData.push({
          nome: res[i].Nome.value,
          login: res[i].LoginUsuario.value,
          dominio: res[i].Dominio.value,
          empresa: res[i].Empresa.value,
          email: res[i].EmailUsuario.value,
          telefone: res[i].telefoneUsuario.value
        });
      }
      $scope.gridUsuarios.api.setRowData($scope.gridUsuarios.rowData);
      $scope.gridUsuarios.api.refreshCells({ force: true });
    });
  };

  $scope.formataPermissoes = function() {
    console.log("rotas", $scope.rotas);

    $scope.permissoes = {};
    for (var i = 0; i < $scope.rotas.length; i++) {
      if ($scope.permissoes[$scope.rotas[i].form]) {
        $scope.permissoes[$scope.rotas[i].form].nomes.push(
          $scope.rotas[i].nome
        );
      } else {
        $scope.permissoes[$scope.rotas[i].form] = {
          nomes: [$scope.rotas[i].nome],
          grupo: [$scope.rotas[i].grupo],
          habilitado: false
        };
      }
    }
  };

  $scope.atualizarUsuario = function() {
    var cadastroAntigo = {
      login: $scope.usuarioSemEdicao.login,
      dominio: $scope.usuarioSemEdicao.dominio,
      permissoes: $scope.usuarioSemEdicao.permissoes,
      nome: $scope.usuarioSemEdicao.nome,
      empresa: $scope.usuarioSemEdicao.empresa,
      email: $scope.usuarioSemEdicao.email,
      telefone: $scope.usuarioSemEdicao.telefone
    };

    var cadastroNovo = {
      login: $scope.usuarioSelecionado.login,
      dominio: $scope.usuarioSelecionado.dominio,
      nome: $scope.usuarioSelecionado.nome,
      empresa: $scope.usuarioSelecionado.empresa,
      email: $scope.usuarioSelecionado.email,
      telefone: $scope.usuarioSelecionado.telefone,
      permissoes: []
    };

    var permissoesSelecionadas = $scope.gridPermissoes.api.getSelectedRows();
    for (var i = 0; i < permissoesSelecionadas.length; i++) {
      cadastroNovo.permissoes.push(permissoesSelecionadas[i].form);
    }
    $scope.salvaAlteracoes(cadastroAntigo, cadastroNovo);
  };

  $scope.getMudancasDeUsuario = function(usuarioAntigo, usuarioNovo) {
    var obj = {};

    if (usuarioAntigo.nome !== usuarioNovo.nome) {
      obj["Nome"] = usuarioNovo.nome;
    }

    if (usuarioAntigo.login !== usuarioNovo.login) {
      obj["LoginUsuario"] = usuarioNovo.login;
    }

    if (usuarioAntigo.dominio !== usuarioNovo.dominio) {
      obj["Dominio"] = usuarioNovo.dominio;
    }

    if (usuarioAntigo.empresa !== usuarioNovo.empresa) {
      obj["Empresa"] = usuarioNovo.empresa;
    }

    if (usuarioAntigo.email !== usuarioNovo.email) {
      obj["EmailUsuario"] = usuarioNovo.email;
    }

    if (usuarioAntigo.telefone !== usuarioNovo.telefone) {
      obj["telefoneUsuario"] = usuarioNovo.telefone;
    }

    return obj;
  };

  $scope.getMudancasDePermissao = function(permissoesAntigas, permissoesNovas) {
    var obj = {
      insert: [],
      delete: []
    };

    obj.insert = permissoesNovas.filter(function(el) {
      return permissoesAntigas.indexOf(el) == -1;
    });

    obj.delete = permissoesAntigas.filter(function(el) {
      return permissoesNovas.indexOf(el) == -1;
    });
    return obj;
  };

  $scope.salvaAlteracoes = function(antigo, novo) {
    var dadosUsuarioAlterados = $scope.getMudancasDeUsuario(antigo, novo);
    var permissoesAlteradas = $scope.getMudancasDePermissao(
      antigo.permissoes,
      novo.permissoes
    );

    if (Object.keys(dadosUsuarioAlterados).length) {
      var queryUpdateUsuario = "UPDATE UsuariosEstatura SET ";
      for (var dado in dadosUsuarioAlterados) {
        queryUpdateUsuario +=
          dado + " = '" + dadosUsuarioAlterados[dado] + "',";
      }
      queryUpdateUsuario =
        queryUpdateUsuario.slice(0, queryUpdateUsuario.length - 1) +
        " WHERE LoginUsuario = '" +
        antigo.login +
        "' AND Dominio = '" +
        antigo.dominio +
        "';";

      if (dadosUsuarioAlterados.LoginUsuario || dadosUsuarioAlterados.Dominio) {
        var queryUpdateUsuarioTabelaPermissoes =
          "UPDATE PermissoesUsuariosEstatura " +
          "SET LoginUsuario = '" +
          novo.login +
          "', " +
          "Dominio = '" +
          novo.dominio +
          "' " +
          "WHERE LoginUsuario = '" +
          antigo.login +
          "' " +
          "AND Dominio = '" +
          antigo.dominio +
          ";";
      }
    }

    if (permissoesAlteradas.insert.length) {
      var queryInsertPermissoes =
        "INSERT INTO PermissoesUsuariosEstatura VALUES ";
      for (var i = 0; i < permissoesAlteradas.insert.length; i++) {
        queryInsertPermissoes +=
          "('" +
          novo.login +
          "','" +
          novo.dominio +
          "','" +
          permissoesAlteradas.insert[i] +
          "'),";
      }
      queryInsertPermissoes =
        queryInsertPermissoes.slice(0, queryInsertPermissoes.length - 1) + ";";
    }

    if (permissoesAlteradas.delete.length) {
      var queryDeletePermissoes =
        "DELETE FROM PermissoesUsuariosEstatura " +
        "WHERE LoginUsuario = '" +
        novo.login +
        "' AND Dominio = '" +
        novo.dominio +
        "' AND Formulario IN (";
      for (var i = 0; i < permissoesAlteradas.delete.length; i++) {
        queryDeletePermissoes += "'" + permissoesAlteradas.delete + "',";
      }
      queryDeletePermissoes =
        queryDeletePermissoes.slice(0, queryDeletePermissoes.length - 1) + ");";
    }

    mssqlQueryTedious(queryUpdateUsuario, function(err, res) {
      var erro = false;
      if (err) {
        erro = true;
        console.log("erro query update usuario", err);
        alerte("Erro ao atualizar usuário", "Erro");
      }
      mssqlQueryTedious(queryUpdateUsuarioTabelaPermissoes, function(err, res) {
        if (err) {
          erro = true;
          console.log("erro query update usuario tab permissoes", err);
          alerte("Erro ao atualizar permissões", "Erro");
        }
        mssqlQueryTedious(queryDeletePermissoes, function(err, res) {
          if (err) {
            erro = true;
            console.log("erro query delete permissoes", err);
            alerte("Erro ao excluir permissões", "Erro");
          }
          mssqlQueryTedious(queryInsertPermissoes, function(err, res) {
            if (err) {
              erro = true;
              console.log(
                "erro query insert permissoes",
                queryInsertPermissoes
              );
              alerte("Erro ao inserir permissões", "Erro");
            }
          });
        });
      });
      if (erro == false) $scope.updateRegisterSuccess();
    });

    $scope.updateRegisterSuccess = function() {
      alerte("Cadastro do usuário atualizado com sucesso.", "Sucesso");
    };

    $("#modal-edicao").modal("hide");
    $scope.getUsuarios();
    $scope.gridUsuarios.api.refreshCells({ force: true });
  };

  $scope.copiarPermissoes = function(login) {
    var querySelectPermissoes =
      "SELECT DISTINCT Formulario FROM PermissoesUsuariosEstatura WHERE LoginUsuario = '" +
      login +
      "' AND Formulario IS NOT NULL;";
    mssqlQueryTedious(querySelectPermissoes, function(err, res) {
      if (err) {
        console.log("erro query select permissoes para copia", err);
        alerte("erro ao copiar permissoes", "Erro");
      }
      var arrTeste = [];
      if (res.length == 0) {
        alerte("Usuário não localizado", "Erro");
        return;
      }
      for (var i = 0; i < res.length; i++) {
        arrTeste.push(res[i][0].value);
      }

      $scope.gridPermissoes.api.forEachNode(function(node) {
        var y = arrTeste.filter(function(el) {
          return el == node.data.form;
        });

        if (y.length) {
          node.setSelected(true);
        } else {
          node.setSelected(false);
        }
      });
      $scope.gridPermissoes.api.refreshCells({ force: true });

      var queryNomeUsuario =
        "SELECT NOME FROM UsuariosEstatura WHERE LoginUsuario = '" +
        login +
        "';";
      mssqlQueryTedious(queryNomeUsuario, function(err, res) {
        if (err) console.log("erro query nome usuario", err);
        alerte(
          "Permissoes do usuário " + res[0].NOME.value + " copiadas.",
          "Permissões copiadas"
        );
      });
    });
  };

  $scope.copiarPermissoesNovoUsuario = function(login) {
    var arrPermissoesParaCopia = [];
    var querySelectPermissoes =
      "SELECT DISTINCT Formulario" +
      "FROM PermissoesUsuariosEstatura " +
      "WHERE LoginUsuario = '" +
      login +
      "' AND Formulario IS NOT NULL;";

    mssqlQueryTedious(querySelectPermissoes, function(err, res) {
      if (err) console.log("erro query select permissoes para copia", err);
      var arrTeste = [];
      for (var i = 0; i < res.length; i++) {
        arrTeste.push(res[i][0].value);
      }

      $scope.gridPermissoesNovoUsuario.api.forEachNode(function(node) {
        arrTeste.filter(function(el) {
          return el == node.data.form;
        });

        if (arrTeste.length) {
          node.setSelected(true);
        } else {
          node.setSelected(false);
        }
      });

      $scope.gridPermissoesNovoUsuario.api.refreshCells();

      var queryNomeUsuario =
        "SELECT NOME FROM UsuariosEstatura WHERE LoginUsuario = '" +
        login +
        "';";
      mssqlQueryTedious(queryNomeUsuario, function(err, res) {
        if (err) console.log("erro query nome usuario", err);
        alerte(
          "Permissoes do usuário " + res[0].NOME.value + " copiadas.",
          "Permissões copiadas"
        );
      });
    });
  };

  /* NOVAS FUNÇÕES TRAZIDAS DA OUTRA TELA */
  $scope.reqUser = function(row) {
    $scope.reqLogin = row.LoginUsuario;
    $scope.reqDominio = row.Dominio;

    if (row.Empresa != undefined && row.Empresa != "") {
      $scope.reqEmpresa = row.Empresa;
    }

    if (row.EmailUsuario != undefined && row.EmailUsuario != "") {
      $scope.reqEmail = row.EmailUsuario;
    }

    $scope.reqNome = row.Nome;
    $scope.reqTelefone = row.TelefoneUsuario;

    //checkPermissions(row.LoginUsuario, row.Dominio); Substituido por reqPermissions
    function reqPermissions() {
      //console.log("Searching permissions.");
      $(".PermissionCheckBox").prop("checked", false);

      // Desmarca as CheckBox da classe PermissionCheckBox

      $scope.userPerms = []; // Limpou array responsável por guardar as permissões do usuário em edição
      function permInputCheck(columns) {
        var perm = columns[0].value;
        //console.log("Permissoes encontradas:  "+perm+" DENTRO DE PERM INPUT CHECK!!!!");
        if (perm != null && perm != "" && perm != undefined) {
          perm = perm.split(",");
          //console.log("Perm APOS SPLIT: "+perm+" Length: "+perm = ""+" String: "+perm.toString());
          i = 0;

          while (i <= perm.length) {
            var permElem = $("#" + perm[i]);
            if (permElem.length) {
              permElem.prop("checked", true);
              //console.log("PermElement CHECKED:  "+permElem+" Perm I: "+perm[i]);
            }
            i++;
          }

          $scope.userPerms = perm; // Adiciona a permissão marcada ao UserPerms
          //$scope.confirmRequest();
		  $scope.$apply();
        }
      }
	  
	  stmt =
        "select permissoes from solicitacaoUsuarios where loginUsuario = '" +
        $scope.reqLogin +
        "' and dominio = '" +
        $scope.reqDominio +
        "';";
      //log(stmt);
      db.query(stmt, permInputCheck, function(err, num_rows) {
        //userlog(stmt, "Verificar permissões solicitadas ", 2, err);
        if (err) {
          //console.log("Erro na consulta "+stmt+" \n Erro: "+err);
        } else {
          //console.log("Permission Selecionadas: "+$scope.userPerms.toString());
        }
      });
      
    }
	reqPermissions();
	
    //$("#modal-userRequest").show();
    //$("body").resize();
  };

  function populateUserGrid(columns) {
    var gridLoginUsuario = columns[0].value,
      gridDominio = columns[1].value,
      gridEmpresa = columns[2].value,
      gridNome = columns[3].value,
      gridEmailUsuario = columns[4].value,
      gridTelefoneUsuario = columns[5].value;
    //gridCpf = columns[5].value,

    dado = {
      LoginUsuario: gridLoginUsuario,
      Dominio: gridDominio,
      Empresa: gridEmpresa,
      Nome: gridNome,
      EmailUsuario: gridEmailUsuario,
      TelefoneUsuario: gridTelefoneUsuario
    };

    $scope.userData.push(dado);
  }

  function populateGroupGrid(columns) {
    var groupCodigo = columns[0].value,
      groupName = columns[1].value;

    dado = {
      groupName: groupName,
      groupCodigo: groupCodigo
    };

    //console.log("Pushando dados para DataGridGroups "+groupName+" cod: "+groupCodigo);

    $scope.dataGridGroups.push(dado);
  }

  // Função ListaGrupos responsável por popular grid de grupos
  $scope.listaGroups = function() {
    $scope.dataGridGroups = [];
    //console.log("Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n Listando Groups \n ");
    listaGroups(populateGroupGrid, $scope);

    // Função responsável pelo grid dos grupos (Registro e edição de usuario)
  };

  // Função responsável por popular o grid
  $scope.listaUsers = function() {
    $scope.userData = [];
    listaUsers(0, populateUserGrid, $scope);
  };

  // Função responsável por popular o grid de solicitações
  $scope.listaSolicitacoes = function() {
    /*// var$btn_gerar = $(this);
		//$btn_gerar.button('loading');*/
    $scope.solicitacoes = [];

    //console.log("Carregando solicitações.. ");
    stmt = "Select * from solicitacaoUsuarios ORDER BY DataSolicitacao DESC";
    //log(stmt);
    db.query(stmt, populateRequestGrid, function(err, num_rows) {
      if (err) {
        console.log("Erro ao carregar solicitações");
      } else {
        $scope.listaUsers();
      }
    });
  };

  $scope.updatePerms = function() {
    var userPerms = $scope.userPerms; // Abriu com essas permissões
    var savedPerms = []; // Usuário está com essas permissões em execução
    var nowPerms = [];
    var deletePerms = [];
    var insertPerms = [];

    //var deleteArray = [];
    var Login = $scope.editLogin.trim();
    var Dominio = $scope.editDominio.trim();

    if (Login == undefined || Login == "") {
      return false;
    }

    if (Dominio == undefined || Dominio == "") {
      return false;
    }

    //console.log("User Perms length: "+userPerms = "");

    var permChecks = $("#pag-permissoes .PermissionCheckBox:checked");
    var i = 0;

    while (i < permChecks.length) {
      savedPerms.push(permChecks[i].id);
      i++;
    }

    //console.log("Saved Perms: "+savedPerms = "");
    i = 0;
    while (i < savedPerms.length) {
      //console.log("Perm "+i+": "+savedPerms[i]);
      i++;
    }

    //console.log("B4 ALL \n NowPerms: "+nowPerms.toString()+" \n userPerms: "+userPerms.toString());
    //console.log("Saved Perms Total: "+savedPerms);
    //console.log("User Perms Total: "+userPerms);

    // Função vai comparar os arrays e inserir os novos, deletar os que foram removidos

    i = userPerms.length - 1;
    while (i >= 0) {
      // //console.log("Comparando: "+i+" "+userPerms[i]);
      if (savedPerms.indexOf(userPerms[i]) == -1) {
        //console.log("Ausente no segundo: "+userPerms[i]);
      } else {
        //console.log("Presente nos dois: "+userPerms[i]);
        nowPerms.push(userPerms[i]);
        savedPerms.splice(savedPerms.indexOf(userPerms[i]), 1);
        userPerms.splice(i, 1);
      }

      i--;
    }

    function updatePermissions() {
      var insertStmt =
        db.use +
        "INSERT " +
        db.prefixo +
        " into PermissoesUsuariosEstatura(LoginUsuario, Dominio, Formulario) VALUES";
      var stmtPerms = "";

      i = 0;
      while (i < savedPerms.length) {
        if (i + 1 == savedPerms.length) {
          stmtPerms =
            stmtPerms +
            "('" +
            Login +
            "','" +
            Dominio +
            "','" +
            savedPerms[i] +
            "')";
        } else {
          stmtPerms =
            stmtPerms +
            "('" +
            Login +
            "','" +
            Dominio +
            "','" +
            savedPerms[i] +
            "'), ";
        }
        i++;
      }

      var deleteStmt =
        db.use +
        "DELETE " +
        db.prefixo +
        " from PermissoesUsuariosEstatura where loginUsuario = '" +
        Login +
        "' AND Dominio = '" +
        Dominio +
        "' AND (";
      var deletePerms = "";

      i = 0;
      while (i < userPerms.length) {
        if (i + 1 == userPerms.length) {
          deletePerms = deletePerms + " Formulario = '" + userPerms[i] + "')";
          //deletePerms = deletePerms + "('"+Login+"','"+Dominio+"','"+userPerms[i]+"')";
        } else {
          deletePerms = deletePerms + " Formulario = '" + userPerms[i] + "' OR";
        }
        i++;
      }
      insertStmt = insertStmt + stmtPerms;
      deleteStmt = deleteStmt + deletePerms;

      if (deletePerms == "" || deletePerms == undefined) {
        deleteStmt = "";
      }
      if (stmtPerms == "" || stmtPerms == undefined) {
        insertStmt = "";
      }

      if (
        Login == "" ||
        Login == undefined ||
        Dominio == "" ||
        Dominio == undefined
      ) {
        insertStmt = "";
        deleteStmt = "";
      }
      //console.log("isSqlBusy? Should be!"+$scope.sqlBusy);
      // Permissões totais antes da remoção e da inserção
      nowPerms = unique(nowPerms.concat(userPerms)); //Total de Valores presentes e valores a serem deletados
      $(".PermissionCheckBox").prop("disabled", true);

      //$scope.sqlBusy = true;
      //log(insertStmt);
      db.query(
        insertStmt,
        function() {},
        function(err, num_rows) {
          //userlog(insertStmt, "Inserção de permissão", 1, err);
          if (err) {
            //console.log("Erro na sintaxe: "+err);
            alerte(
              "Falha ao remover permissões de " +
                Login +
                ", por favor tente novamente mais tarde.",
              "Falha em execução."
            );
            //console.log("insert não pode ser realizado: "+insertStmt);
            //$scope.sqlBusy = false;
            $(".PermissionCheckBox").prop("disabled", false);
            $("#pag-permissoes .loadingButton").hide();
            //$("#pag-permissoes #modal-view").modal('hide');
          } else {
            nowPerms = unique(nowPerms.concat(savedPerms)); //Adiciona os valores inseridos ao array
            db.query(
              deleteStmt,
              function() {},
              function(err, num_rows) {
                if (err) {
                  //console.log("Erro na sintaxe: "+err);
                  alerte(
                    "Falha ao remover permissões de " +
                      Login +
                      ", por favor tente novamente mais tarde.",
                    "Falha em execução."
                  );
                  //console.log("Delete não pode ser realizado: "+deleteStmt);
                } else {
                  //console.log("Now Perms: "+nowPerms);
                  nowPerms = $(nowPerms)
                    .not(userPerms)
                    .get();
                  alerte(
                    "Permissões alteradas com sucesso para o usuário " +
                      Login +
                      ".",
                    "Ação realizada."
                  );

                  setTimeout(function() {
                    $("#bootstrap-alert-box-modal").modal("hide");
                  }, 2000);
                }
                //userlog(deleteStmt, "Remoção de permissão", 4, err);

                //console.log("isSqlBusy? FimDelete "+$scope.sqlBusy);
                //$scope.sqlBusy = false;
                //console.log("isSqlBusy? Should Be Deactivated! "+$scope.sqlBusy+" scopo: "+$scope.sqlBusy);
                $scope.userPerms = nowPerms; // Atualiza o scopo com as permissões que o usuário atualmente possui
                $(".PermissionCheckBox").prop("disabled", false);
                $("#pag-permissoes .loadingButton").hide(); //button('reset');
                //$("#pag-permissoes #modal-view").modal('hide');
              }
            );
          }
        }
      );

      //Fim da função
    }
    function tryAgainInTime() {
      //console.log("Try Again In Time");!$scope.sqlBusy||
      if (db.isIdle()) {
        //$scope.sqlBusy = true;
        $("#pag-permissoes .loadingButton").show();
        //console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
        updatePermissions(); // Realiza as queries
      } else {
        setTimeout(tryAgainInTime, 1000);
      }
    }

    //console.log("isSqlBusy? BEFORE FUNCTION "+$scope.sqlBusy);!$scope.sqlBusy&&
    if (db.isIdle()) {
      //console.log(" IsSQLBUSY?"+$scope.sqlBusy+" dbIdle: "+db.isIdle());
      $("#pag-permissoes .loadingButton").show();
      //console.log("Scope SQLBUSY should be true;"+$scope.sqlBusy);
      updatePermissions(); // Realiza as queries baseado nas checkbox da tela de permissões //Matheus 2017.1
    } else {
      setTimeout(tryAgainInTime, 1000);
    }
  };

  $scope.updateUser = function(row) {
    // Informações guardadas ao abrir edição de usuário
    var oldLogin = $scope.openLogin;
    var oldDomain = $scope.openDominio;

    // Informações possivelmente alteradas
    var newLogin = $scope.editLogin;
    var newDomain = $scope.editDominio;
    var newEmpresa = $scope.editEmpresa || "";
    var newNome = $scope.editNome || "";
    var newEmail = $scope.editEmail || "";
    var newTel = $scope.editTelefone || "";

    if (!newLogin || !newDomain) {
      alerte(
        "Por favor, preencha as informações em branco para que as alterações sejam salvas.",
        "Falha ao atualizar usuário."
      );
      return false;
    }

    var updateStmt =
      db.use +
      "UPDATE " +
      db.prefixo +
      " usuariosEstatura set LoginUsuario = '" +
      newLogin +
      "', Dominio = '" +
      newDomain +
      "', Empresa = '" +
      newEmpresa +
      "', Nome = '" +
      newNome +
      "', emailUsuario = '" +
      newEmail +
      "', telefoneUsuario = '" +
      newTel +
      "' where LoginUsuario = '" +
      oldLogin +
      "' AND dominio = '" +
      oldDomain +
      "' ";
    var updatePerms =
      "update permissoesUsuariosEstatura set LoginUsuario = '" +
      newLogin +
      "', Dominio = '" +
      newDomain +
      "' where LoginUsuario = '" +
      oldLogin +
      "' AND dominio = '" +
      oldDomain +
      "' ";
    //console.log("Trying to updateUser");

    log(updateStmt);
    db.query(
      updateStmt,
      function() {},
      function(err, num_rows) {
        if (err) {
          //console.log("Erro ao atualizar usuário: "+err);

          alerte(
            "Falha ao atualizar dados do usuário " +
              oldLogin +
              " do domínio " +
              oldDomain +
              ".",
            "Falha em execução."
          );
          setTimeout(function() {
            $("#bootstrap-alert-box-modal").modal("hide");
          }, 2000);
        } else {
          db.query(
            updateStmt,
            function() {},
            function(err, num_rows) {
              if (err) {
                alerte(
                  "Falha ao atualizar permissões do usuário " +
                    oldLogin +
                    " do domínio " +
                    oldDomain +
                    ", entre em contato com suporte@versatec",
                  "Falha em execução"
                );
              }
            }
          );
          alerte(
            "Usuário " +
              oldLogin +
              " do domínio " +
              oldDomain +
              " atualizado com sucesso",
            "Ação executada."
          );
          setTimeout(function() {
            $("#bootstrap-alert-box-modal").modal("hide");
          }, 2000);
          $("#modal-view").modal("hide");
          //$scope.listaUsers();
          $scope.listaSolicitacoes();
        }
      }
    );
  };

  //Função para negar o cadastro do usuário
  $scope.denyRequest = function() {
    var Login = $scope.reqLogin;
    var Dominio = $scope.reqDominio;
    var Nome = $scope.reqNome;
    var Empresa = $scope.reqEmpresa;
    var Telefone = $scope.reqTelefone;
    var Email = $scope.reqEmail;

    var deletestmt =
      "delete from solicitacaoUsuarios where LoginUsuario = '" +
      $scope.reqLogin +
      "' AND Dominio = '" +
      $scope.reqDominio +
      "'";
    var emailStmt =
      "insert into FilaNotificacaoEmail(Assunto, Para, Cc, Cco, Mensagem) values ('Solicitação de cadastro no Estalo', '<" +
      $scope.reqEmail +
      ">'," +
      " '<suporte@versatec.com.br>', '<jorge.esteves@versatec.com.br>', " +
      "'A solicitação de acesso do usuário " +
      Nome +
      " ao Estalo foi recusada." +
      " Por favor, entre em contato através do e-mail suporte@versatec.com.br para maiores informações.')";

    mssqlQueryTedious(deletestmt, function(err, result) {
      if (err) console.log("Erro ao deletar solicitacaoUSuarios: " + err);
      mssqlQueryTedious(emailStmt, function(err, result) {
        if (err) console.log(err);
        setTimeout(
          alerte(
            "Usuário " +
              Login.trim() +
              " com domínio " +
              Dominio.trim() +
              " teve registro negado.",
            "Ação executada."
          ),
          500
        );
        $scope.listaSolicitacoes(); //Atualiza o grid de usuários
        $("#modal-userRequest").modal("hide");
        $("#modal-requests").modal("hide");
      });
    });
  };

  //Função para permitir o cadastro do usuário
  $scope.allowRequest = function() {
    sLogin = $scope.reqLogin;
    sDominio = $scope.reqDominio;
    sEmpresa = $scope.reqEmpresa;
    sNome = $scope.reqNome;
    sEmail = $scope.reqEmail;
    sTelefone = $scope.reqTelefone;
    $scope.request = true; // Confirma que a solicitação foi feita pelo estalo

    $scope.requestPerms = [];
    var reqChecks = $(".RequestCheckBox:checked");
    var i = 0;

    while (i < reqChecks.length) {
      //console.log("Permission Request Checked: "+reqChecks[i].id);
      $scope.requestPerms.push(reqChecks[i].id);
      i++;
    }

    //console.log("Permissões totais: "+$scope.requestPerms.toString());
    checkLoginToRegister(
      sLogin,
      sDominio,
      sEmpresa,
      sNome,
      sEmail,
      sTelefone,
      $scope
    );
    $scope.gridUsuarios.api.refreshCells({ force: true });
  };

  // Verifica as permissões do usuário a ser editado (Chamado pelo editUser)
  $scope.listaPerms = function() {
    var perms = $scope.globalPerms;

    function checkNomeByForm(perms, form) {
      var nomes = "";
      //console.log("perms: "+perms+" form: "+form);
      for (x = 0; x < perms.length; x++) {
        //console.log("s "+x+": "+perms[x].nome+" para "+form);
        if (perms[x].form == form) {
          if (nomes == "") {
            nomes = perms[x].nome;
          } else {
            nomes = perms[x].nome + ", " + nomes;
          }
        } else {
          //console.log(perms[x].nome+" FORM +"+perms[x].form);
        }
      }

      if (nomes) {
        return nomes;
      } else {
        return false;
      }
    }

    var formNames = []; // ex: frmChamUraVoz
    var nomes = []; // ex: Detalhamento de chamadas

    for (i = 0; i < perms.length; i++) {
      if (formNames.indexOf(perms[i].form) == -1) {
        nomes.push(checkNomeByForm(perms, perms[i].form));
        formNames.push(perms[i].form);
      } else {
        //console.log(perms[i].form+" na posição "+formNames.indexOf(perms[i].form)+" I: "+i);
      }
    }
    var arrayDeObjetos = [];

    //console.log("Nomes: "+nomes = ""+" Perms: "+formNames = "");

    for (i = 0; i < nomes.length; i++) {
      var objeto = { detalhes: "", form: "" };
      objeto.detalhes = nomes[i];
      objeto.form = formNames[i];
      arrayDeObjetos.push(objeto);
    }
    //console.log("Data Grid Perms Size: "+arrayDeObjetos = "");

    $scope.dataGridPerms = arrayDeObjetos;
  };

  function populateRequestGrid(columns) {
    var gridLoginUsuario = columns[0].value,
      gridDominio = columns[1].value,
      gridEmpresa = columns[2].value,
      gridNome = columns[3].value,
      gridEmailUsuario = columns[5].value,
      gridTelefoneUsuario = columns[6].value,
      reqPerms = columns[7].value;

    dado = {
      LoginUsuario: gridLoginUsuario,
      Dominio: gridDominio,
      Empresa: gridEmpresa,
      Nome: gridNome,
      EmailUsuario: gridEmailUsuario,
      TelefoneUsuario: gridTelefoneUsuario,
      Permissoes: reqPerms
      //Cpf: gridCpf
    };

    $scope.solicitacoes.push(dado);
  }

  /*FIM NOVAS FUNÇÕES TRAZIDAS DA OUTRA TELA */

  // $scope.abreModalGrupos = function() {
  //   $scope.grupos = [];
  //   var querySelectGrupos = "SELECT DISTINCT CodGrupo FROM PermissoesGruposEstaturaB"

  //   mssqlQueryTedious(querySelectGrupos, function(err,res) {
  //     if (err) {
  //       console.log('erro query select grupos', err);
  //       alerte("Erro ao pesquisar grupos","Erro");
  //     };

  //     for (var i=0;i<res.length;i++) {
  //       $scope.grupos.push(res[i][0].value);
  //     }
  //   })
  //   $('#modal-grupo').modal('show');
  // };

  $scope.$on("$viewContentLoaded", function() {
    $view = $("#pag-cadastro-usuarios");

    $("#pag-permissoes #modal-register").on("hidden", function() {
      $("#pag-permissoes .registerAlert").hide();
      $("#pag-permissoes .registerAlertText").text("");
      $("#pag-permissoes .container button").blur();
      //console.log("Blurred buttons");
    });

    // Inflam o grid dentro do modal
    $('#pag-permissoes a[href="#groupTab"]').on("shown", function() {
      //console.log('Clicou em groupTab');
      $("#groupTab").resize();
    });

    $('#pag-permissoes a[href="#permTab"]').on("shown", function() {
      $("#permTab").resize();
    });

    $("#pag-permissoes .modal-view").on("shown", function() {
      $("#permTab").resize();
    });

    $("#modal-requests").on("shown", function() {
      $("body").resize();
    });

    $("#modal-userRequest").on("shown", function() {
      $("body").resize();
      $("#modal-requests").modal("hide");
    });

    $("#modal-userRequest").on("hidden", function() {
      $("#modal-requests").modal("show");
      $scope.getUsuarios();
      $scope.gridUsuarios.api.refreshCells({ force: true });
    });

    $view.on("click", ".close", function() {
      $(document).focus();
      //console.log("Clicou no fechar modal");
    });

    $(".registerTelefone").maskbrphone({
      useDdd: true,
      useDddParenthesis: true,
      dddSeparator: " ",
      numberSeparator: "-"
    });

    $scope.listaSolicitacoes();
    $scope.listaPerms($scope.globalPerms); //Executa a listagem de permissões baseado em informações do init.json
    $scope.getUsuarios();
  });
}
CtrlCadastroUsuarios.$inject = ["$scope", "$globals"];
