function verificaRepetidoArrayObjectos(arr, prop) {
	var arr2 = [];
	for (var i = 0; i < arr.length; i++) {
		if (i == 0) {
			arr2.push(arr[i]);
			continue;
		} else {
			var temValor = false;
			for (var j = 0; j < arr2.length; j++) {
				if (arr2[j][prop] == arr[i][prop]) {
					temValor = true;
				}
			}
			if (!temValor) {
				arr2.push(arr[i]);
			}
		}
	}
	return arr2;
}

function mssqlQueryTedious(query, callback) {
	//console.log("[Carregando] " + query);
	var mssqlConfig = {
		server: config.server,
		userName: "ESTUPD",
		password: "sql2008@ESTU42",
		options: {
			database: "Estatura",
			port: config.options.port,
			rowCollectionOnRequestCompletion: true,
			rowCollectionOnDone: true,
			connectTimeout: 60000,
			requestTimeout: 60000
		}
	};
	var connection = new Connection(mssqlConfig);
	connection.on('connect', function (err) {
		// If no error, then good to go...
		console.log("[Conectou] " + query);
		if (err) callback(err, []);
		mssqlQueryTediousExecuteStatement(query, connection, callback);
	});
	connection.on('error', function (text) {
		console.log(text);
	});
}

function mssqlQueryTedious2(query, callback) {
	//console.log("[Carregando] " + query);
	var mssqlConfig = {
		server: config.server,
		userName: "ESTUPD",
		password: "sql2008@ESTU42",
		options: {
			database: "Estatura",
			port: 55759,
			rowCollectionOnRequestCompletion: true,
			rowCollectionOnDone: true,
			connectTimeout: 5000,
			requestTimeout: 5000
		}
	};
	var connection = new Connection(mssqlConfig);
	connection.on('connect', function (err) {
		// If no error, then good to go...
		console.log("[Conectou] " + query);
		if (err) callback(err, []);
		mssqlQueryTediousExecuteStatement(query, connection, callback);
	});
	connection.on('error', function (text) {
		console.log(text);
	});
}

function mssqlQueryTedious3(query, callback) {
	//console.log("[Carregando] " + query);
	var mssqlConfig = {
		server: config.server,
		userName: "SDBM6009",
		password: "BD00%06S",
		options: {
			database: "Estatura",
			port: config.options.port,
			rowCollectionOnRequestCompletion: true,
			rowCollectionOnDone: true,
			connectTimeout: 5000,
			requestTimeout: 5000
		}
	};
	var connection = new Connection(mssqlConfig);
	connection.on('connect', function (err) {
		// If no error, then good to go...
		console.log("[Conectou] " + query);
		if (err) callback(err, []);
		mssqlQueryTediousExecuteStatement(query, connection, callback);
	});
	connection.on('error', function (text) {
		console.log(text);
	});
}


function mssqlQueryTediousExecuteStatement(query, connection, callback) {
	var arrResultadoQuery = [];
	request = new Request(query, function (err, rowCount) {
		if (err) {
			//console.log(err, []);
		} else {
			callback(false, arrResultadoQuery);
			connection.close();
		}
		connection.close();
	});
	request.on('row', function (columns) {
		// console.log(columns);
		arrResultadoQuery.push(columns);
	});
	connection.execSql(request);
}

function downloadFileHttp(url, dest, callback, tipo, aplicacaoTemp, callback2) {
	//console.log('Baixando ' + aplicacaoTemp);
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", url, true);
	rawFile.onreadystatechange = function () {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				if (rawFile.responseText !== "") {
					var tempArquivoArray = rawFile.response.toString().split('\r\n');
					if (tipo == 1) {
						try {
							cache.aplicacoes[aplicacaoTemp]['itens_controle'] = [];
						} catch (err) {
							//console.log(err);
						}
						var arrayTemporarioDownload = [];
						for (var i = 0; i < tempArquivoArray.length; i++) {
							var linha = tempArquivoArray[i].split('\t');
							arrayTemporarioDownload.push({
								codigo:  "" +linha[1],
								descricao:  "" +linha[2],
								aplicacao:  "" +aplicacaoTemp
							});
						}
						// console.log(arrayTemporarioDownload);
						// console.log(cache.aplicacoes[aplicacaoTemp]['itens_controle']);
						try {
							cache.aplicacoes[aplicacaoTemp]['itens_controle'] = (arrayTemporarioDownload);
						} catch (err) {
							//console.log(err);
						}
						// console.log(cache.aplicacoes[aplicacaoTemp]['itens_controle']);
					} else {
						try {
							cache.aplicacoes[aplicacaoTemp]['estados'] = [];
						} catch (err) {
							//console.log(err);
						}
						var arrayTemporarioDownload = [];
						for (var i = 0; i < tempArquivoArray.length; i++) {
							var linha = tempArquivoArray[i].split('\t');
							var trash = linha[4] !== undefined ? (linha[4] === "0") : true;
							var volume = linha[5] !== undefined ? (linha[5] === "0") : true;
							arrayTemporarioDownload.push({
								codigo:  "" +linha[1],
								nome:  "" +linha[2],
								aplicacao:  "" +aplicacaoTemp,
								exibir: (linha[3] === "S"),
								trash: trash,
								volume: volume
							});
						}
						try {
							cache.aplicacoes[aplicacaoTemp]['estados'] = (arrayTemporarioDownload);
						} catch (err) {
							//console.log(err);
						}
					}
					fs.writeFileSync(dest + '.temp', rawFile.response);
					var inp = fs.createReadStream(dest + '.temp');
					var out = fs.createWriteStream(dest);
					var gzip = zlib.createGzip();

					inp.pipe(gzip).pipe(out);
					out.on('close', function () {
						if(fs.existsSync(dest + '.temp')){
						fs.unlinkSync(dest + '.temp');
						callback();
						}
						else{
							callback();
						}
						// if(contadorTemporarioDownloadFileHttp>59){
						// 	callback2();
						// }
						//  console.log(dest + 'CONCCLUIDO');
					});
				} else {
					//console.log("Vazio");
					callback();
				}
			}	else { 
				//console.log("Falha ao baixar "+aplicacaoTemp);
				callback();
			}
		} else { 
			//console.log("não ready");
		}
	}
	
	rawFile.send(null);

}
// };
// { Função apaga pasta não vazia
function deleteFolderRecursive (path) {
	if (fs.existsSync(path)) {
	  fs.readdirSync(path).forEach(function(file, index){
		var curPath = path + "/" + file;
		if (fs.lstatSync(curPath).isDirectory()) { // recurse
		  deleteFolderRecursive(curPath);
		} else { // delete file
		  fs.unlinkSync(curPath);
		}
	  });
	  fs.rmdirSync(path);
	}
  };
// } Fim função apaga pasta não vazia


function baixarTodosICsEDs(arrAplicacaoTemp, index, callback) {
	index = (index) ? index : 0;
	if (index != arrAplicacaoTemp.length) {
		// ICS
		downloadFileHttp('http://www.versatec.com.br/downloads/bases/ics/' + arrAplicacaoTemp[index].apl + '.csv.gz', tempDir3() + 'ics/' + arrAplicacaoTemp[index].apl + '.csv.gz', function (resposta) {
			index++;
			baixarTodosICsEDs(arrAplicacaoTemp, index, callback);
		}, 1, arrAplicacaoTemp[index].apl, function () {
		});
		//EDS
		downloadFileHttp('http://www.versatec.com.br/downloads/bases/eds/' + arrAplicacaoTemp[index].apl + '.csv.gz', tempDir3() + 'eds/' + arrAplicacaoTemp[index].apl + '.csv.gz', function (resposta) {
		}, 0, arrAplicacaoTemp[index].apl);
	} else {
		callback();
	}
}


function baixarTodosICs(arrAplicacaoTemp, index, callback) {
	index = (index) ? index : 0;
	if (index != arrAplicacaoTemp.length) {
		// ICS
		downloadFileHttp('http://www.versatec.com.br/downloads/bases/ics/' + arrAplicacaoTemp[index].apl + '.csv.gz', tempDir3() + 'ics/' + arrAplicacaoTemp[index].apl + '.csv.gz', function (resposta) {
			index++;
			baixarTodosICs(arrAplicacaoTemp, index, callback);
		}, 1, arrAplicacaoTemp[index].apl, function () {
		});
		
	} else {
		callback();
	}
}

function baixarTodosEDs(arrAplicacaoTemp, index, callback) {
	index = (index) ? index : 0;
	if (index != arrAplicacaoTemp.length) {
		// EDS
		downloadFileHttp('http://www.versatec.com.br/downloads/bases/eds/' + arrAplicacaoTemp[index].apl + '.csv.gz', tempDir3() + 'eds/' + arrAplicacaoTemp[index].apl + '.csv.gz', function (resposta) {
			index++;
			baixarTodosEDs(arrAplicacaoTemp, index, callback);
		}, 1, arrAplicacaoTemp[index].apl, function () {
		});
		
	} else {
		callback();
	}
}
