function CtrlParametroGra($scope, $http, $globals) {

    win.title = "Cadastro de Parametros GRA"; //Bruno 13/02/2017
    $scope.versao = versao;

    $scope.graData = []; // Dados do grid 

    //Colunas do grid 
    $scope.colunas = [
      { field: "codigo", enableFiltering: true, displayName: "Código", cellClass: "grid-align", width: '15%', pinned: false },//  cellClass:"span1", width: 80,
        { field: "nome", enableFiltering: true, displayName: "Nome", cellClass: "grid-align", width: '15%', pinned: false },// cellClass:"span2",width: 80,
        { field: "prazo", enableFiltering: true, displayName: "Prazo", cellClass: "grid-align", width: '8%', pinned: false },// cellClass:"span2",width: 80,
        { field: "Frase1", enableFiltering: true, displayName: "Frase 1", cellClass: "grid-align", width: '25%', pinned: false },// cellClass:"span1", width: 150,
        { field: "Frase2", enableFiltering: true, displayName: "Frase 2", cellClass: "grid-align", width: '25%', pinned: false },//  cellClass:"span4", width: 120,
        { field: "Edição", enableFiltering: false, displayName: "Edição", cellClass: "grid-align", width: '10%', pinned: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#modal-view" ng-click="grid.appScope.EPgra(row.entity)" title="Editar" data-toggle="modal" class="icon-edit" target="_self"></a></span></div>' }// cellClass:"span2", width: 80,
    ];

    // Grid 
    $scope.gridDados = {
        data: $scope.graData,
        columnDefs: $scope.colunas,
        enableColumnResize: false,
        enablePining: true,
        enableFiltering: true,
        exporterMenuPdf: false,
        filterOptions: $scope.filtros,
        showFilter: true,
        enableGridMenu: true,
        enableSelectAll: true,
        exporterCsvFilename: 'ParametroGra.csv',
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };


    $scope.filtros = {
        filterText: "",
        useExternalFilter: true
    };
    // Filtro 
    $scope.filterUser = function () {
        var filterText = $scope.userFilter;
        if (filterText !== '') {
            $scope.filtros.filterText = filterText;
        } else {
            $scope.filtros.filterText = '';
        }
    };
    // Fim do Filtro
    var $view;

    $scope.$on('$viewContentLoaded', function () {

        $view = $("#pag-para_gra_Gra");
        treeView('#pag-para_gra #chamadas', 15, 'Chamadas', 'dvchamadas');
        treeView('#pag-para_gra #repetidas', 35, 'Repetidas', 'dvRepetidas');
        treeView('#pag-para_gra #ed', 65, 'ED', 'dvED');
        treeView('#pag-para_gra #ic', 95, 'IC', 'dvIC');
        treeView('#pag-para_gra #tid', 115, 'TID', 'dvTid');
        treeView('#pag-para_gra #vendas', 145, 'Vendas', 'dvVendas');
        treeView('#pag-para_gra #falhas', 165, 'Falhas', 'dvFalhas');
        treeView('#pag-para_gra #extratores', 195, 'Extratores', 'dvExtratores');
        treeView('#pag-para_gra #parametros', 225, 'Parametros', 'dvParam');
        treeView('#pag-para_gra #admin', 255, 'Administrativo', 'dvAdmin');
        treeView('#pag-para_gra #monitoracao', 275, 'Monitoração', 'dvReparo');
        treeView('#pag-para_gra #cradleToGrave', 295, 'Cradle to Grave', 'dvcontGlobalTransf');

        $("#pag-para_gra .botoes").css({ 'position': 'fixed', 'left': 'auto', 'right': '25px', 'margin-top': '35px' });



        $view.find("select.filtro-rotas").selectpicker({
            selectedTextFormat: 'count',
            countSelectedText: '{0} Permissões',
            showSubtext: true
        });


        $("#pag-para_gra #modal-gra-registro").on('hidden', function () {
            $("#pag-para_gra .registerAlert").hide();
            $("#pag-para_gra .registerAlertText").text("");
            $("#pag-para_gra .container button").blur();
            console.log("Blurred buttons");
        });

        //preenchendo list com os relatorios

        var options2 = [];
        cache.relatorios.forEach(function (form) {

            options2.push('<option value="' + form.codigo + '">' + form.nome + '</option>');

        });

        $scope.listaGra();
    });



    function populateGraGrid(columns) {

        var
            gridCodigo = columns[1].value,
            gridNome = columns[2].value,
            gridPrazo = columns[3].value,
            gridFrase1 = columns[4].value,
            gridFrase2 = columns[5].value;

        dado = {

            codigo: gridCodigo,
            nome: gridNome,
            prazo: gridPrazo,
            Frase1: gridFrase1,
            Frase2: gridFrase2

        };
        $scope.graData.push(dado);
        $scope.gridDados.data = $scope.graData;
    }


    // Função responsável por popular o grid 
    $scope.listaGra = function () {

        var $btn_gerar = $(this);
        $btn_gerar.button('loading');
        $scope.graData = [];

        console.log("Listando Gras");

        // Fim da permissão responsavel por popular o grid 

        listaGra(populateGraGrid, $scope);

        $btn_gerar.button('reset');

    };

    //função para criar csv//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //var app = angular.module('app', ['ngAnimate', 'ngTouch', 'ui.grid', 'ui.grid.selection', 'ui.grid.exporter']);

    //app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
    //           $http.get('/data/100.json')
    //    .success(function (data) {
    //        $scope.gridOptions.data = data;
    //    });

    //}]);

    // Função responsável por REGISTRAR parametro gra no banco de dados 

    $scope.registraParaGra = function paraGra() {
        $scope.exist = false;

        function validForm() {
            $("#pag-para_gra .registerAlertText").text("");
            if (
                (($scope.codigo == "" || $scope.codigo == undefined) ||
                    ($scope.nome == "" || $scope.nome == undefined) ||
                ($scope.prazo == "" || $scope.prazo == undefined)
                //||
                //        ($scope.Frase1 == "" || $scope.Frase1 == undefined) ||
                //            ($scope.Frase2 == "" || $scope.Frase2 == undefined)
                )
                ) {
                var resultstring = "Favor preencha ";

                console.log("Valor de Código: " + $scope.codigo);
                console.log("Valor de Nome: " + $scope.nome);
                console.log("Valor de Prazo: " + $scope.prazo);

                if ($scope.codigo == "" || $scope.codigo == undefined) {
                    resultstring = resultstring + "codigo, ";
                    $scope.codigo = "";
                }
                if ($scope.nome == "" || $scope.nome == undefined) {
                    resultstring = resultstring + "nome, ";
                    $scope.nome = "";
                }
                if ($scope.prazo == "" || $scope.prazo == undefined) {
                    resultstring = resultstring + "prazo, ";
                    $scope.prazo = "";
                }

                resultstring = resultstring + " e tente novamente.";
                console.log("Error: " + resultstring);
                $("#pag-para_gra .registerAlertText").text(resultstring);
                $("#pag-para_gra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-para_gra .registerAlert").hide();
                }, 3000);
                return false;
            }

            return true;
        }



        console.log("Connected? " + db.isConnected());

        if (validForm()) {

            if (checkLoginToRegisterGra($scope.codigo, $scope.nome, $scope.prazo, $scope.Frase1, $scope.Frase2, $scope)) {
                console.log(" já existe no banco de dados " + $scope.codigo);
                $("#pag-para_gra .registerAlertText").text(" já existe no banco de dados " + $scope.codigo);
                $("#pag-para_gra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-para_gra .registerAlert").hide();
                }, 3000);
            } else {
                $("#pag-para_gra #modal-gra-registro").modal('hide');
                $("#pag-para_gra #modal-gra-registro input").val("");
            }

        } else {

        }

    };
    // Fim da função responsável por registrar parametro gra

    function listaGra(callBackGra, scopo) {
        array = [];
        pgra = "Select id,codigo,nome,prazo,Frase1,Frase2 From parametrosgra";

        db.query(pgra, callBackGra, function (err, num_rows) {
            userLog(pgra, 'visualiza Parametro Gra', 2, err)
            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            retornaStatusQuery(num_rows, scopo);
            console.log("" + num_rows);
        });
    }

    /* Inserções do Bruno para função de Parametro Gra */
    function registraParaGra(editCodigo, editNome, editPrazo, editFrase1, editfrase2, scope) {
        var stmt = db.use
        + "INSERT " + db.prefixo + "parametrosgra(codigo,nome,prazo,Frase1,Frase2)"
        + " VALUES "
        + "('" + editCodigo + "','" + editNome + "','" + editPrazo + "','" + editFrase1 + "',"
        + "'" + editfrase2 + "')"


        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        // Cadastra o parametro Gra
        db.query(stmt, function () { }, function (err, num_rows) {
            userLog(stmt, 'Registra Parametro Gra ' + editCodigo, 1, err)
            console.log("Executando query-> " + stmt);
            if (err) {
                console.log("Erro ao registrar: " + err);
                $("#pag-para_gra .registerAlertText").text("Erro ao registrar parâmetro Gra" + err);
                $("#pag-para_gra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-para_gra .registerAlert").hide();
                }, 3000);
            } else {
                console.log("Sem erros na db.query!");
                $("#pag-para_gra .registerAlertText").text("Parâmetro GRA registrado com sucesso");
                $("#pag-para_gra .registerAlert").show();
                document.getElementById("FormParaGra").reset();

                if (scope.listaGra != undefined && scope.listaGra != "") {
                    console.log("Scopo Lista Gra");
                    scope.listaGra();
                }
                setTimeout(function () {
                    $("#pag-para_gra .registerAlert").hide();
                }, 3000);
                //$("#pag-para_gra .registerAlert").delay(5000).hide();
                //return true;
            }
        });
        // Fim do cadastra parametro Gra

    }

    // Função utilizada para conferir se login existe no banco de dados
    function checkLoginToRegisterGra(codigo, nome, prazo, Frase1, Frase2, scope) {
        console.log("Before replace: " + codigo + " " + nome + " " + prazo);

        if (Frase1 == undefined) {
            Frase1 = ""
        }
        if (Frase2 == undefined) {
            Frase2 = ""
        }

        codigo = replaceDiaritics(codigo);
        nome = replaceDiaritics(nome);
        prazo = replaceDiaritics(prazo);
        Frase1 = replaceDiaritics(Frase1);
        Frase2 = replaceDiaritics(Frase2);


        console.log("After replace: " + codigo + " " + nome + " " + prazo);



        stmt = "Select codigo from parametrosgra where Codigo = '" + codigo + "' and Nome = '" + nome + "'and Prazo = '" + prazo + "'";
        var coluna = false;
        var executou = false;

        function getRetorno(columns) {
            colunax = columns[0].value;
            executou = true;
            if (colunax == codigo) {
                console.log("Este Parâmetro Gra já existe no banco de dados!!");
            }
        }

        db.query(stmt, getRetorno, function (err, num_rows) {
            if (err) {
                console.log("Erro na sintaxe: " + err);
            }

            if (num_rows >= 1) {
                coluna = true;
                console.log("Existe no Banco " + codigo + " Rows: " + num_rows);
                //notificate("Parâmetro Gra já cadastrado.", scope);
                $("#pag-para_gra .registerAlertText").text("Já existe um Parametro com o Código : " + codigo + "");
                $("#pag-para_gra .registerAlert").show();
                setTimeout(function () {
                    $("#pag-para_gra .registerAlert").hide();
                }, 3000);

            }

            if (num_rows == 0) {
                console.log("Não existe no banco " + codigo + " Rows: " + num_rows);
                registraParaGra(codigo, nome, prazo, Frase1, Frase2, scope);
            }

            console.log("Valor de retorno: " + coluna);
        });
    }
    // Fim da função utilizada para conferir se login existe no banco de dados


    // Inicio da função responsável por salvar a edição de gra
    $scope.row = ""; // Variável responsável pela linha em edição no array de gra
    $scope.EPgra = function (row) {
        $scope.exist = false;

        console.log("Edita Gra Valores: " + row + " Row: " + row);


        var editcodigo = row.codigo;
        var editnome = row.nome;
        var editprazo = row.prazo;
        var editFrase1 = row.Frase1;
        var editFrase2 = row.Frase2;

        $scope.editcodigo = row.codigo;
        $scope.editnome = row.nome;
        $scope.editprazo = row.prazo;
        $scope.editFrase1 = row.Frase1;
        $scope.editFrase2 = row.Frase2;

        console.log("Código: " + editcodigo);
        console.log("Nome: " + editnome);
        console.log("Prazo: " + editprazo);
        console.log("Frase 1: " + editFrase1);
        console.log("Frase 2: " + editFrase2);
        $scope.row = row;



        return true;
    };

    $scope.verifPGRA = function () {
        console.log('Verificação de Repetidos');
        $("#pag-para_gra .registerAlertText").text("");
        if (
            (($scope.editcodigo == "" || $scope.editcodigo == undefined) ||
                ($scope.editnome == "" || $scope.editnome == undefined) ||
            ($scope.editprazo == "" || $scope.editprazo == undefined)
            )
            ) {
            var resultstring = "Favor preencha ";

            console.log("Valor de Código: " + $scope.editcodigo);
            console.log("Valor de Nome: " + $scope.editnome);
            console.log("Valor de Prazo: " + $scope.editprazo);

            if ($scope.editcodigo == "" || $scope.editcodigo == undefined) {
                resultstring = resultstring + "codigo, ";
                $scope.editcodigo = "";
            }
            if ($scope.editnome == "" || $scope.editnome == undefined) {
                resultstring = resultstring + "nome, ";
                $scope.editnome = "";
            }
            if ($scope.editprazo == "" || $scope.editprazo == undefined) {
                resultstring = resultstring + "prazo, ";
                $scope.editprazo = "";
            }

            resultstring = resultstring + " e tente novamente.";
            console.log("Error: " + resultstring);
            $("#pag-para_gra .registerAlertText").text(resultstring);
            $("#pag-para_gra .registerAlert").show();
            setTimeout(function () {
                $("#pag-para_gra .registerAlert").hide();
            }, 3000);
            return false;
        }
        return true;
    }
    $scope.EdPgra = function () {
        row = $scope.row;
        $scope.exist = false;
        var editcodigo = row.codigo;
        var editnome = row.nome;
        var editprazo = row.prazo;
        var editFrase1 = row.Frase1;
        var editFrase2 = row.Frase2;

        var stmt = db.use
+ "UPDATE " + db.prefixo + "parametrosgra SET codigo= '" + $scope.editcodigo + "', nome='" + $scope.editnome + "', prazo='" + $scope.editprazo + "',  Frase1='" + $scope.editFrase1 + "', Frase2='" + $scope.editFrase2 + "' WHERE codigo='" + editcodigo + "'";

        console.log("Connected: " + db.isConnected() + " Idle: " + db.isIdle());

        if ($scope.verifPGRA()) {
            // Edita o parametro Gra
            db.query(stmt, function () { }, function (err, num_rows) {
                console.log("Executando query-> " + stmt);
                userLog(stmt, 'Atualiza Parametro Gra ' + editcodigo, 3, err)
                if (err) {
                    console.log("Erro ao editar: " + err);
                    $("#pag-para_gra .registerAlertText").text("Erro ao editar parâmetro Gra");
                    $("#pag-para_gra .registerAlert").show();
                    setTimeout(function () {
                        $("#pag-para_gra .registerAlert").hide();
                    }, 3000);
                } else {
                    console.log("Sem erros na db.query!");
                    $("#pag-para_gra .registerAlertText").text("Parâmetro GRA editado com sucesso");
                    $("#pag-para_gra .registerAlert").show();

                    if ($scope.listaGra != undefined && $scope.listaGra != "") {
                        console.log("Scopo Lista Gra");
                        $scope.listaGra();
                    }
                    setTimeout(function () {
                        $("#pag-para_gra .registerAlert").hide();
                    }, 3000);
                }
                if (num_rows == 0) {
                    console.log("Não existe no banco " + editcodigo + " Rows: " + num_rows);
                }


            });

        }
    };
    ///// Fim Bruno
    CtrlParametroGra.$inject = ['$scope', '$http', '$globals'];

}