::APAGAR CACHE
tzutil /s "E. South America Standard Time
set caminho=%appdata%
set modified=%caminho:Roaming=Local\Estalo%
@RD /S /Q %modified%
set caminho=%appdata%
set modified=%caminho:Roaming=Local\Temp\%
@RD /S /Q %modified%
TASKKILL /F /IM Estalo.exe /T
TASKKILL /F /IM node.exe /T
TASKKILL /F /IM tool.exe /T
del extratorDiaADia*.csv
del extratorDiaADia*.js
del extratorDiaADia*.txt
del *_ItemDeControle.txt
del *_ItemDeControleOnDemand.txt
del *_Prompts.txt
del *.tmp
del *.xlsx